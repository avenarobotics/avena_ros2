# change_detect

This ROS2 package removes robot links from the scene point cloud and detects whether there was a change (e.g. movement) in the scene between two ROS2 actions calls
