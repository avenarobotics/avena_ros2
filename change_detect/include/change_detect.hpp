#ifndef CHANGE_DETECT_HPP
#define CHANGE_DETECT_HPP

// ___CPP___
#include <functional>
#include <memory>
#include <string>

// __ROS__
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <std_msgs/msg/float32.hpp>

// __PCL__
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/octree/octree_pointcloud_changedetector.h>
#include <pcl/filters/crop_box.h>

// ___AVENA___
#include "custom_interfaces/msg/change_detect.hpp"
#include "custom_interfaces/msg/robot_self_filter.hpp"
#include "custom_interfaces/msg/merged_ptcld_filtered.hpp"
#include "visibility_control.h"
#include "helpers_vision/helpers_vision.hpp"
#include "helpers_commons/helpers_commons.hpp"

namespace change_detect
{
  using json = nlohmann::json;
  using Timer = helpers::Timer;

  struct Area
  {
    Eigen::Vector4f point_min;
    Eigen::Vector4f point_max;
  };

  struct SecurityArea
  {
    Eigen::Vector4f point_min_inter;
    Eigen::Vector4f point_max_inter;
    Eigen::Vector4f point_min_exter;
    Eigen::Vector4f point_max_exter;
  };

  class ChangeDetect : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    COMPOSITION_PUBLIC
    explicit ChangeDetect(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());
    ~ChangeDetect();

    void initNode() override;
    void shutDownNode() override;

  private:
    helpers::Watchdog::SharedPtr _watchdog;
    void _getChangedPointsIndices(const pcl::PointCloud<pcl::PointXYZ>::Ptr prev_cloud, const pcl::PointCloud<pcl::PointXYZ>::Ptr changed_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr out_diff);
    void _filterTablePointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
    void _zoneMovementDetection(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_diff, std::map<std::string, int64_t> &zones_detection);
    void _robotSelfFilterCallback(const custom_interfaces::msg::RobotSelfFilter::SharedPtr robot_self_filter_data);
    void _mergedPtcldFilteredCallback(const custom_interfaces::msg::MergedPtcldFiltered::SharedPtr merged_ptcld_filtered_data);
    custom_interfaces::msg::ChangeDetect::UniquePtr _prepareOutputMsg(const custom_interfaces::msg::RobotSelfFilter::SharedPtr robot_self_filter_data);
    // TableArea _getTableAreaFromParametersServer();
    void _countPointsInArea(Eigen::Vector4f &min_pt, Eigen::Vector4f &max_pt, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_diff, int64_t &size);
    void _getParametersFromServer();
    pcl::PointCloud<pcl::PointXYZ>::Ptr _previous_cloud;
    rclcpp::Subscription<custom_interfaces::msg::RobotSelfFilter>::SharedPtr _sub_robot_self_filter;
    rclcpp::Subscription<custom_interfaces::msg::MergedPtcldFiltered>::SharedPtr _sub_merged_ptcld_filtered;
    rclcpp::Publisher<custom_interfaces::msg::ChangeDetect>::SharedPtr _pub_change_detect;
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr _pub_scene_change_value;
    void joinThread();

    rclcpp::TimerBase::SharedPtr _timer;
    bool _param_server_read;
    std::thread _param_server_thread;

    // TableArea _table_area;
    std::map<std::string, Area> _areas;
    SecurityArea _security_area;

    float _octree_resolution;
    bool _valid_scene_data;
    bool _debug;

    const float DEFAULT_OCTREE_RESOLUTION = 0.01;
  };

} // namespace change_detect

#endif
