// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef CHANGE_DETECT_DEBUG__COMPONENT_HPP_
#define CHANGE_DETECT_DEBUG__COMPONENT_HPP_

#include "visibility_control.h"
#include "rclcpp/rclcpp.hpp"
#include <tf2_ros/transform_listener.h>
#include <mysql_connector.h>
#include <assert.h>
#include <sstream>

// #include "custom_interfaces/msg/scene.hpp"
#include "custom_interfaces/msg/change_detect.hpp"
#include "helpers_vision/helpers_vision.hpp"

#include <memory>

namespace ros2mysql
{
  using json = nlohmann::json;
  class ChangeDetect : public rclcpp::Node
  {
  public:
    COMPOSITION_PUBLIC
    explicit ChangeDetect(const rclcpp::NodeOptions &options);

  private:
    COMPOSITION_PUBLIC
    void _configure();

    COMPOSITION_PUBLIC
    void _initializeParameters();

    COMPOSITION_PUBLIC
    std::vector<json> _getTransforms(const builtin_interfaces::msg::Time &timestamp);

    std::unique_ptr<MysqlConnector> db_;
    std::string host_;
    std::string port_;
    std::string db_name_;
    std::string username_;
    std::string password_;
    bool debug_;
    uint8_t frames;
    rclcpp::Subscription<custom_interfaces::msg::ChangeDetect>::SharedPtr sub_;
    rclcpp::TimerBase::SharedPtr timer_;
    std::unique_ptr<tf2_ros::TransformListener> _transform_listener;
    std::unique_ptr<tf2_ros::Buffer> _transforms_buffer;
  };

} // namespace ros2mysql

#endif // DETECT_DEBUG__COMPONENT_HPP_
