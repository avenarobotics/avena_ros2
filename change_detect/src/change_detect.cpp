#include "change_detect.hpp"

namespace change_detect
{
    ChangeDetect::ChangeDetect(const rclcpp::NodeOptions &options)
        : Node("change_detect_server", options),
          _previous_cloud(new pcl::PointCloud<pcl::PointXYZ>),
          _octree_resolution(DEFAULT_OCTREE_RESOLUTION),
          _valid_scene_data(false),
          _debug(false)
    {
        RCLCPP_INFO(this->get_logger(), "Initialization of change detect module.");
        helpers::commons::setLoggerLevelFromParameter(this);

        this->declare_parameter<float>("octree_resolution", DEFAULT_OCTREE_RESOLUTION);

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        _sub_robot_self_filter = this->create_subscription<custom_interfaces::msg::RobotSelfFilter>("robot_self_filter", qos_settings, std::bind(&ChangeDetect::_robotSelfFilterCallback, this, std::placeholders::_1));
        _pub_change_detect = this->create_publisher<custom_interfaces::msg::ChangeDetect>("change_detect", qos_settings);
        _pub_scene_change_value = create_publisher<std_msgs::msg::Float32>("scene_change_value", qos_settings);

        _sub_merged_ptcld_filtered = this->create_subscription<custom_interfaces::msg::MergedPtcldFiltered>("merged_ptcld_filtered", qos_settings, std::bind(&ChangeDetect::_mergedPtcldFilteredCallback, this, std::placeholders::_1));
        std::cout << "start reading " << std::endl;
        _param_server_thread = std::thread(std::bind(&ChangeDetect::_getParametersFromServer, this));
        // _getParametersFromServer();
        std::cout << "end reading " << std::endl;

        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        status = custom_interfaces::msg::Heartbeat::STOPPED;

        _timer = this->create_wall_timer(std::chrono::milliseconds(100), std::bind(&ChangeDetect::joinThread, this));

        RCLCPP_INFO(this->get_logger(), "...done");
    }

    void ChangeDetect::initNode()
    {
        status = custom_interfaces::msg::Heartbeat::STARTING;

        status = custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void ChangeDetect::shutDownNode()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;

        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    ChangeDetect::~ChangeDetect()
    {
        _param_server_read = true;
        joinThread();
    }

    void ChangeDetect::joinThread()
    {
        if (_param_server_read && _param_server_thread.joinable())
            _param_server_thread.join();
    }

    custom_interfaces::msg::ChangeDetect::UniquePtr ChangeDetect::_prepareOutputMsg(const custom_interfaces::msg::RobotSelfFilter::SharedPtr robot_self_filter_data)
    {
        // helpers::Timer timer(__func__, get_logger());
        custom_interfaces::msg::ChangeDetect::UniquePtr change_detect_data(new custom_interfaces::msg::ChangeDetect);
        change_detect_data->cam1_rgb = robot_self_filter_data->cam1_rgb;
        change_detect_data->cam2_rgb = robot_self_filter_data->cam2_rgb;
        change_detect_data->cam1_depth = robot_self_filter_data->cam1_depth;
        change_detect_data->cam2_depth = robot_self_filter_data->cam2_depth;
        // change_detect_data->cam1_ptcld = robot_self_filter_data->cam1_ptcld;
        // change_detect_data->cam2_ptcld = robot_self_filter_data->cam2_ptcld;
        // change_detect_data->cam1_ptcld_trans = robot_self_filter_data->cam1_ptcld_trans;
        // change_detect_data->cam2_ptcld_trans = robot_self_filter_data->cam2_ptcld_trans;
        change_detect_data->merged_ptcld = robot_self_filter_data->merged_ptcld;
        change_detect_data->merged_ptcld_filtered = robot_self_filter_data->merged_ptcld_filtered;
        return change_detect_data;
    }

    void ChangeDetect::_mergedPtcldFilteredCallback(const custom_interfaces::msg::MergedPtcldFiltered::SharedPtr merged_ptcld_filtered_data)
    {
        if (status != custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
            return;
        }

        // helpers::Timer timer(__func__, get_logger());
        RCLCPP_INFO(this->get_logger(), "Scene callback with merged point cloud filtered");
        _valid_scene_data = true;
        _previous_cloud->clear();
        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(merged_ptcld_filtered_data->merged_ptcld_filtered, _previous_cloud);
    }

    void ChangeDetect::_robotSelfFilterCallback(const custom_interfaces::msg::RobotSelfFilter::SharedPtr robot_self_filter_data)
    {
        if (status != custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
            return;
        }

        if (!_param_server_read)
            return;
        // RCLCPP_DEBUG(get_logger(), "--------------------------------------------START-------------------------------------------");
        helpers::Timer timer(__func__, get_logger());
        this->get_parameter_or("octree_resolution", _octree_resolution, DEFAULT_OCTREE_RESOLUTION);

        pcl::PointCloud<pcl::PointXYZ>::Ptr current_cloud(new pcl::PointCloud<pcl::PointXYZ>);

        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(robot_self_filter_data->merged_ptcld_filtered, current_cloud);

        std::thread t1(std::bind(&ChangeDetect::_filterTablePointCloud, this, _previous_cloud));
        std::thread t2(std::bind(&ChangeDetect::_filterTablePointCloud, this, current_cloud));

        t1.join();
        t2.join();

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_diff_total(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_diff_temp(new pcl::PointCloud<pcl::PointXYZ>);

        std::thread t3(std::bind(&ChangeDetect::_getChangedPointsIndices, this, current_cloud, _previous_cloud, cloud_diff_total));
        std::thread t4(std::bind(&ChangeDetect::_getChangedPointsIndices, this, _previous_cloud, current_cloud, cloud_diff_temp));

        t3.join();
        t4.join();

        *cloud_diff_total += *cloud_diff_temp;

        std::map<std::string, int64_t> zones_detection;
        _zoneMovementDetection(cloud_diff_total, zones_detection);
        custom_interfaces::msg::ChangeDetect::UniquePtr msg = _prepareOutputMsg(robot_self_filter_data);
        std_msgs::msg::Float32::UniquePtr scene_change_value_msg(new std_msgs::msg::Float32);
        msg->header.frame_id = "world";
        msg->header.stamp = now();
        json areas_change = zones_detection;
        msg->areas_change = areas_change.dump();
        msg->timestamp = now();
        if (!_valid_scene_data)
            msg->scene_change_percentage = 1.0;
        else if (current_cloud->size() > 0 && _previous_cloud->size() == 0)
            msg->scene_change_percentage = static_cast<double>(cloud_diff_total->size()) / current_cloud->size();
        else if (current_cloud->size() == 0 && _previous_cloud->size() > 0)
            msg->scene_change_percentage = static_cast<double>(cloud_diff_total->size()) / _previous_cloud->size();
        else if (current_cloud->size() == 0 && _previous_cloud->size() == 0)
            msg->scene_change_percentage = 0.0;
        else
            msg->scene_change_percentage = static_cast<double>(cloud_diff_total->size()) / current_cloud->size();

        helpers::converters::pclToRosPtcld<pcl::PointXYZ>(cloud_diff_total, msg->diff_ptcld);

        msg->diff_ptcld.header.frame_id = "world";
        msg->diff_ptcld.header.stamp = now();

        if (msg->scene_change_percentage < 0)
            msg.reset(new custom_interfaces::msg::ChangeDetect);
        else
            scene_change_value_msg->data = msg->scene_change_percentage;

        _pub_change_detect->publish(*msg);

        _pub_scene_change_value->publish(*scene_change_value_msg);
    }

    void ChangeDetect::_getChangedPointsIndices(const pcl::PointCloud<pcl::PointXYZ>::Ptr prev_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr changed_cloud, pcl::PointCloud<pcl::PointXYZ>::Ptr out_diff)
    {
        // helpers::Timer timer(__func__, get_logger());

        pcl::octree::OctreePointCloudChangeDetector<pcl::PointXYZ> octree(_octree_resolution);
        octree.setInputCloud(prev_cloud);
        octree.addPointsFromInputCloud();
        octree.switchBuffers();
        octree.setInputCloud(changed_cloud);
        octree.addPointsFromInputCloud();
        std::vector<int> new_point_idx_vector;
        octree.getPointIndicesFromNewVoxels(new_point_idx_vector);
        helpers::vision::extract(new_point_idx_vector, false, changed_cloud, out_diff);

        return;
    }

    void ChangeDetect::_zoneMovementDetection(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_diff, std::map<std::string, int64_t> &zones_detection)
    {
        // helpers::Timer timer(__func__, get_logger());
        std::thread t1(std::bind(&ChangeDetect::_countPointsInArea, this, _security_area.point_min_inter, _security_area.point_max_inter, cloud_diff, zones_detection["security_area_change_points"]));
        std::vector<std::thread> threads;

        for (auto const &[key, val] : _areas)
            threads.push_back(std::thread(std::bind(&ChangeDetect::_countPointsInArea, this, val.point_min, val.point_max, cloud_diff, zones_detection[key + "_change_points"])));

        t1.join();
        //there is no need to crop cloud on external points becouse its already cropped like that in ptcld transformer
        zones_detection["security_area_change_points"] = cloud_diff->points.size() - zones_detection["security_area_change_points"];
        for (auto &th : threads)
            th.join();
    }

    void ChangeDetect::_countPointsInArea(Eigen::Vector4f &min_pt, Eigen::Vector4f &max_pt, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_diff, int64_t &size)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr temp_cloud(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::CropBox<pcl::PointXYZ> crop_box;
        crop_box.setInputCloud(cloud_diff);
        crop_box.setMin(min_pt);
        crop_box.setMax(max_pt);
        crop_box.filter(*temp_cloud);
        size = temp_cloud->points.size();
    }

    void ChangeDetect::_filterTablePointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
    {
        // helpers::Timer timer(__func__, get_logger());
        float min = _areas["table_area"].point_min.z() + 0.005;
        float max = _areas["table_area"].point_max.z();
        helpers::vision::passThroughFilter(cloud, "z", min, max);
    }

    void ChangeDetect::_getParametersFromServer()
    {
        // helpers::Timer timer(__func__, get_logger());

        try
        {
            while (rclcpp::ok())
            {

                auto parameters = helpers::commons::getParameters({"areas", "security_area", "change_detect"});
                if (parameters.empty())
                    continue;

                auto get_vector4f_from_json = [](const json &point) -> Eigen::Vector4f
                {
                    return Eigen::Vector4f(point["x"].get<float>(), point["y"].get<float>(), point["z"].get<float>(), 1);
                };

                json areas_parameters = parameters["areas"];
                for (auto &area_json : areas_parameters.items())
                {
                    const std::string area_name = area_json.key();
                    const json area_info = area_json.value();
                    Area area;
                    area.point_min = get_vector4f_from_json(area_info["min"]);
                    area.point_max = get_vector4f_from_json(area_info["max"]);
                    _areas[area_name] = area;
                }

                json security_area = parameters["security_area"];
                _security_area.point_min_exter = get_vector4f_from_json(security_area["min_exter"]);
                _security_area.point_max_exter = get_vector4f_from_json(security_area["max_exter"]);
                _security_area.point_min_inter = get_vector4f_from_json(security_area["min_inter"]);
                _security_area.point_max_inter = get_vector4f_from_json(security_area["max_inter"]);

                json change_detect_params = parameters["change_detect"];
                _octree_resolution = change_detect_params["octree_resolution"].get<float>();
                _param_server_read = true;
                break;
            }
        }
        catch (const json::exception &e)
        {
            std::string error_message = "bbb Error occured while parsing parameter from the server. Message: " + std::string(e.what()) + ". Exiting...";
            RCLCPP_FATAL(get_logger(), error_message);
            rclcpp::shutdown();
            throw std::runtime_error(error_message);
        }
    }

} // namespace change_detect

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(change_detect::ChangeDetect)
