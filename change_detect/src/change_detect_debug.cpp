#include "change_detect_debug.hpp"

#include <iostream>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include <rclcpp/parameter.hpp>

namespace ros2mysql
{

    ChangeDetect::ChangeDetect(const rclcpp::NodeOptions &options)
        : Node("change_detect_debug", options)
    {
        // Declare parameters.
        this->_initializeParameters();

        this->_configure();

        _transforms_buffer = std::make_unique<tf2_ros::Buffer>(get_clock(), tf2::Duration(std::chrono::seconds(5)));
        _transform_listener = std::make_unique<tf2_ros::TransformListener>(*_transforms_buffer);

        this->frames = 0;
        try
        {
            this->db_ = std::make_unique<MysqlConnector>(this->host_, this->port_, this->db_name_, this->username_, this->password_, this->debug_);
        }
        catch (const network_exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
        }

        auto callback = [this](const typename custom_interfaces::msg::ChangeDetect::SharedPtr msg) -> void {
            this->frames++;
            // RCLCPP_ERROR(this->get_logger(), get_name());

            // Validate input message
            if (msg->header.stamp == builtin_interfaces::msg::Time())
            {
                try
                {
                    db_->clearTable("change_detect");
                }
                catch (const network_exception &e)
                {
                    RCLCPP_ERROR(this->get_logger(), e.what());
                }
                catch (const query_exception &e)
                {
                    RCLCPP_ERROR(this->get_logger(), e.what());
                }
                return;
            }

            // change_detect
            change_detect_t change_detect;
            cv::Mat camera_1_rgb;
            cv::Mat camera_1_depth;
            cv::Mat camera_2_rgb;
            cv::Mat camera_2_depth;
            helpers::converters::rosImageToCV(msg->cam1_rgb, camera_1_rgb);
            helpers::converters::imageToStream(camera_1_rgb, change_detect.camera_1_rgb);
            helpers::converters::rosImageToCV(msg->cam1_depth, camera_1_depth);
            helpers::converters::imageToStream(camera_1_depth, change_detect.camera_1_depth);
            helpers::converters::rosImageToCV(msg->cam2_rgb, camera_2_rgb);
            helpers::converters::imageToStream(camera_2_rgb, change_detect.camera_2_rgb);
            helpers::converters::rosImageToCV(msg->cam2_depth, camera_2_depth);
            helpers::converters::imageToStream(camera_2_depth, change_detect.camera_2_depth);

            pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_camera_1(new pcl::PointCloud<pcl::PointXYZ>);
            helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(msg->cam1_ptcld, pcl_camera_1);
            helpers::converters::pointcloudToStream<pcl::PointXYZ>(pcl_camera_1, change_detect.pcl_camera_1);
            pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_camera_2(new pcl::PointCloud<pcl::PointXYZ>);
            helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(msg->cam2_ptcld, pcl_camera_2);
            helpers::converters::pointcloudToStream<pcl::PointXYZ>(pcl_camera_2, change_detect.pcl_camera_2);

            // std::vector<json> robot_transforms(msg->robot_transforms.size());
            // for (size_t i = 0; i < msg->robot_transforms.size(); i++)
            // {
            //     Eigen::Affine3f link_pose;
            //     helpers::converters::geometryToEigenAffine(msg->robot_transforms[i].transform, link_pose);
            //     Eigen::Vector3f vec = link_pose.translation();
            //     Eigen::Quaternionf quat(link_pose.rotation());
            //     json link;
            //     link["translation"] = helpers::commons::assignJsonFromPosition(vec);
            //     link["rotation"] = helpers::commons::assignJsonFromQuaternion(quat);
            //     robot_transforms[i] = link;
            // }
            
            change_detect.robot_transforms = _getTransforms(msg->merged_ptcld.header.stamp);

            pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_camera_1_transformed(new pcl::PointCloud<pcl::PointXYZ>);
            helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(msg->cam1_ptcld_trans, pcl_camera_1_transformed);
            helpers::converters::pointcloudToStream<pcl::PointXYZ>(pcl_camera_1_transformed, change_detect.pcl_camera_1_transformed);

            pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_camera_2_transformed(new pcl::PointCloud<pcl::PointXYZ>);
            helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(msg->cam2_ptcld_trans, pcl_camera_2_transformed);
            helpers::converters::pointcloudToStream<pcl::PointXYZ>(pcl_camera_2_transformed, change_detect.pcl_camera_2_transformed);

            pcl::PointCloud<pcl::PointXYZ>::Ptr merged_pcl(new pcl::PointCloud<pcl::PointXYZ>);
            helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(msg->merged_ptcld, merged_pcl);
            helpers::converters::pointcloudToStream<pcl::PointXYZ>(merged_pcl, change_detect.merged_pcl);

            pcl::PointCloud<pcl::PointXYZ>::Ptr merged_pcl_filtered(new pcl::PointCloud<pcl::PointXYZ>);
            helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(msg->merged_ptcld_filtered, merged_pcl_filtered);
            helpers::converters::pointcloudToStream<pcl::PointXYZ>(merged_pcl_filtered, change_detect.merged_pcl_filtered);

            change_detect.scene_change_percentage = msg->scene_change_percentage;
            
            // change_detect.areas_change = msg->areas_change;

            // change_detect.tools_area = msg->tools_area;
            // change_detect.items_area = msg->items_area;
            // change_detect.security_area = msg->security_area;
            // change_detect.operating_area = msg->operating_area;

            pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_diff(new pcl::PointCloud<pcl::PointXYZ>);
            helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(msg->diff_ptcld, pcl_diff);
            helpers::converters::pointcloudToStream<pcl::PointXYZ>(pcl_diff, change_detect.pcl_diff);

            try
            {
                // clear database table
                db_->clearTable("change_detect");

                // storing data
                db_->setChangeDetect(&change_detect);
            }
            catch (const network_exception &e)
            {
                RCLCPP_ERROR(this->get_logger(), e.what());
            }
            catch (const query_exception &e)
            {
                RCLCPP_ERROR(this->get_logger(), e.what());
            }
        };

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        sub_ = create_subscription<custom_interfaces::msg::ChangeDetect>("change_detect", qos_settings, callback);

        auto check_freqency =
            [this]() -> void {
            // msg_ = std::make_unique<std_msgs::msg::String>();
            // msg_->data = "Hello World: " + std::to_string(count_++);
            // RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", msg_->data.c_str());
            // // Put the message into a queue to be processed by the middleware.
            // // This call is non-blocking.
            // pub_->publish(std::move(msg_));
            RCLCPP_INFO(this->get_logger(), "%.1f frames per second.", ((float)(this->frames)) / 60.0);
            this->frames = 0;
        };

        // Use a timer to schedule periodic message publishing.
        timer_ = this->create_wall_timer(60s, check_freqency);
    }

    std::vector<json> ChangeDetect::_getTransforms(const builtin_interfaces::msg::Time &timestamp)
    {
        std::vector<json> transforms;
        std::vector<std::string> frame_names = _transforms_buffer->getAllFrameNames();
        for (auto frame_name : frame_names)
        {
            try
            {
                json tf;
                tf["frame_id"] = "world";
                tf["child_frame_id"] = frame_name;
                
                auto time = rclcpp::Time(timestamp);
                auto transform_stamped = _transforms_buffer->lookupTransform("world", frame_name, time);
                tf["translation"]["x"] = transform_stamped.transform.translation.x;
                tf["translation"]["y"] = transform_stamped.transform.translation.y;
                tf["translation"]["z"] = transform_stamped.transform.translation.z;

                tf["rotation"]["x"] = transform_stamped.transform.rotation.x;
                tf["rotation"]["y"] = transform_stamped.transform.rotation.y;
                tf["rotation"]["z"] = transform_stamped.transform.rotation.z;
                tf["rotation"]["w"] = transform_stamped.transform.rotation.w;
                
                transforms.push_back(tf);
            }
            catch (const tf2::TransformException &err)
            {
                RCLCPP_DEBUG(get_logger(), err.what());
                continue;
            }
        }
        return transforms;
    }

    void ChangeDetect::_configure()
    {
        this->get_parameter<std::string>("host", host_);
        this->get_parameter<std::string>("port", port_);
        this->get_parameter<std::string>("db_name", db_name_);
        this->get_parameter<std::string>("username", username_);
        this->get_parameter<std::string>("password", password_);
        this->get_parameter<bool>("debug", debug_);
    }

    void ChangeDetect::_initializeParameters()
    {
        rcl_interfaces::msg::ParameterDescriptor host_descriptor;
        host_descriptor.name = "host";
        host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("host", "", host_descriptor);

        rcl_interfaces::msg::ParameterDescriptor port_descriptor;
        port_descriptor.name = "port";
        port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("port", "", port_descriptor);

        rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
        db_name_descriptor.name = "db_name";
        db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("db_name", "", db_name_descriptor);

        rcl_interfaces::msg::ParameterDescriptor username_descriptor;
        username_descriptor.name = "username";
        username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("username", "", username_descriptor);

        rcl_interfaces::msg::ParameterDescriptor password_descriptor;
        password_descriptor.name = "password";
        password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("password", "", password_descriptor);

        rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
        debug_descriptor.name = "debug";
        debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
        this->declare_parameter("debug", false, debug_descriptor);
    }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::ChangeDetect)
