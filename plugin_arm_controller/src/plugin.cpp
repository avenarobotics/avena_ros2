#include "plugin.h"

namespace arm_controller
{
    void ArmController::onStart()
    {
        using namespace std::chrono_literals;

        if (!registerScriptStuff())
            throw std::runtime_error("script stuff initialization failed");

        _node = rclcpp::Node::make_shared("arm_controller");

        setExtVersion("Plugin for publishing arm joint states");
        setBuildDate(BUILD_DATE);
        rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local();

        _sub_trajectory_point = _node->create_subscription<trajectory_msgs::msg::JointTrajectoryPoint>("trajectory_point", latching_qos, std::bind(&ArmController::_setJointStateCallback, this, std::placeholders::_1));
        _pub_joint_state = _node->create_publisher<sensor_msgs::msg::JointState>("arm_joint_states", latching_qos);
        _timer = _node->create_wall_timer(10ms, std::bind(&ArmController::_broadcastJointState, this));
        // _watchdog = std::make_shared<helpers::Watchdog>(_node.get(), "system_monitor");
    }

    void ArmController::onInstancePass(const sim::InstancePassFlags &flags, bool first)
    {
        rclcpp::spin_some(_node);
    }

    void ArmController::_setJointStateCallback(const trajectory_msgs::msg::JointTrajectoryPoint::SharedPtr trajectory_point_msg)
    {
        // create joint_handles
        std::vector<int> joint_handles;
        for (int i = 1; i <= _joint_number; i++)
        {
            std::string name = "left_joint_" + std::to_string(i);
            joint_handles.push_back(simGetObjectHandle(name.c_str()));
        }
        // check if message is empty and simulation is stopped - avoid starting from wrong position
        if (simGetSimulationState() == 0 && trajectory_point_msg->positions.size() == 0)
        {
            RCLCPP_INFO(_node->get_logger(), "Message is empty. Setting home position");
            std::vector<float> home_state = {0, 0, 0, 0, 0, 0};
            for (size_t i = 0; i < _joint_number; i++)
                simSetJointPosition(joint_handles[i], home_state[i]);
        }
        else if (trajectory_point_msg->positions.size() == _joint_number)
        {

            int joint_index = 0;
            for (auto position : trajectory_point_msg->positions)
            {
                // RCLCPP_INFO(_node->get_logger(), "Setting pos %f", position);

                simSetJointPosition(joint_handles[joint_index++], position);
                // simSetJointForce(joint_handles[1], 5);
                // simSetJointTargetVelocity(joint_handles[joint_index++], position);
            }
            // for (auto position : trajectory_point_msg->positions)
            //     simSetJointPosition(joint_handles[joint_index++], position);
        }
    }

    void ArmController::_broadcastJointState()
    {
        sensor_msgs::msg::JointState::SharedPtr joint_state_msg(new sensor_msgs::msg::JointState);

        // add names to message
        std::vector<std::string> joint_names;
        for (size_t i = 1; i <= _joint_number; i++)
            joint_names.push_back("left_joint_" + std::to_string(i));
        joint_state_msg->name = joint_names;

        // get positions and add to message
        joint_state_msg->position.resize(_joint_number);
        for (size_t i = 0; i < _joint_number; i++)
        {
            float position;
            int joint_handle = simGetObjectHandle(joint_names[i].c_str());
            simGetJointPosition(joint_handle, &position);
            joint_state_msg->position[i] = position;
        }

        joint_state_msg->header.stamp = _node->now();
        _pub_joint_state->publish(*joint_state_msg);
    }
}

SIM_PLUGIN("ArmController", 1, arm_controller::ArmController)
#include "stubsPlusPlus.cpp"
