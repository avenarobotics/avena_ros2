#include "plugin.h"

namespace place
{
    // // __________Coppelia plugin____________________
    PlacePlugin::PlacePlugin()
        : _parameters_read(false)
    {
    }
    PlacePlugin::~PlacePlugin() {}

    void PlacePlugin::onStart()
    {
        _node = rclcpp::Node::make_shared("place"); // TODO: Hasan Farag, remove node
        RCLCPP_INFO(_node->get_logger(), "Initialization of place server.");

        _place_server = rclcpp_action::create_server<PlaceAction>(
            _node->get_node_base_interface(),
            _node->get_node_clock_interface(),
            _node->get_node_logging_interface(),
            _node->get_node_waitables_interface(),
            "place", // TODO: hasan Farag , replace with template
            std::bind(&PlacePlugin::_handle_goal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&PlacePlugin::_handle_cancel, this, std::placeholders::_1),
            std::bind(&PlacePlugin::_handle_accepted, this, std::placeholders::_1));

        auto qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local().reliable();
        _sub_manager = std::make_shared<helpers::SubscriptionsManager>(_node->get_node_topics_interface());

        _sub_manager->createSubscription(Topics::occupancy_grid.name, Topics::occupancy_grid.type, qos_settings);
        _sub_manager->createSubscription(Topics::items.name, Topics::items.type, qos_settings);

        qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        _place_pub = _node->create_publisher<PlaceMsg>(Topics::place.name, qos_settings);

        _getParams();
        RCLCPP_INFO(_node->get_logger(), "...Place server initialization is done");
    }
    void PlacePlugin::onInstancePass(const sim::InstancePassFlags &flags, bool first)
    {
        rclcpp::spin_some(_node);
    }

    //________Avena Data Validation___________

    int PlacePlugin::_validateMsg(const std::shared_ptr<const InputMsg> input_msg)
    {
        if (!_parameters_read)
        {
            RCLCPP_WARN(_node->get_logger(), "Areas data is not loaded yet");
            return Status_e::INVALID;
        }
        if (!input_msg)
        {
            RCLCPP_WARN(_node->get_logger(), "Invalid \"input\" message.");
            return Status_e::INVALID;
        }
        if (input_msg->occ_grid_msg.header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(_node->get_logger(), "Invalid header in \"occupancy_grid\" message.");
            return Status_e::INVALID;
        }
        if (input_msg->items_msg.header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(_node->get_logger(), "Invalid header in \"estimate_shape\" message.");
            return Status_e::INVALID;
        }
        return Status_e::SUCCESS;
    }
    int PlacePlugin::_validateGoal(const Goal &goal)
    {
        if (goal.place_location == "area")
            for (auto &area_label : goal.selected_areas_labels)
            {
                if (_params.table_areas_coords.find(area_label) != _params.table_areas_coords.end())
                    continue;
                else
                {
                    RCLCPP_WARN(_node->get_logger(), " Wrong area label '%s' ", area_label.c_str());
                    return Status_e::INVALID;
                }
            }
        if (goal.search_shift < _MinSearchShift)
        {
            RCLCPP_WARN(_node->get_logger(), "wrong distance parameter passed, Goal Failed");
            return Status_e::INVALID;
        }
        if (goal.no_of_required_place_positions < _MinNoOfRequiredPlacePositions)
        {
            RCLCPP_WARN(_node->get_logger(), "Number of required place poses cant be lower than 1  :  %i ", goal.no_of_required_place_positions);
            RCLCPP_WARN(_node->get_logger(), "wrong  parameter passed, Goal Failed");
            return Status_e::INVALID;
        }
        if (goal.no_of_required_place_positions > _MaxNoOfRequiredPlacePositions)
        {
            RCLCPP_WARN(_node->get_logger(), "Number of required place poses cant be higher than 6  : %i ", goal.no_of_required_place_positions);
            RCLCPP_WARN(_node->get_logger(), "wrong  parameter passed, Goal Failed");
            return Status_e::INVALID;
        }
        return Status_e::SUCCESS;
    }
    int PlacePlugin::_validateOutput(const std::shared_ptr<const PlaceMsg> output_msg)
    {
        if (output_msg->place_poses.size() != 0)
        {
            return Status_e::SUCCESS;
        }
        else
        {
            RCLCPP_ERROR(_node->get_logger(), "Failed");
            return Status_e::FAILURE;
        }
    }

    void PlacePlugin::_terminate(const std::shared_ptr<GoalHandlePlaceAction> goal_handle)
    {
        _place_pub->publish(PlaceMsg());
        goal_handle->abort(std::make_shared<PlaceAction::Result>());
        RCLCPP_ERROR(_node->get_logger(), "Goal Failed");
    }
    //__________Place PLugin Main Fucntion_________
    void PlacePlugin::_execute(const std::shared_ptr<GoalHandlePlaceAction> goal_handle)
    {
        // helpers::Timer(__func__);
        RCLCPP_INFO(_node->get_logger(), "Place Executing goal");
        //-------------read parameters if needed----------------
        if (_getParams() != Status_e::SUCCESS)
        {
            RCLCPP_WARN(_node->get_logger(), "Not able to read parameters from the server");
            _terminate(goal_handle);
            return;
        }
        //-------------input data----------------
        auto items_msg = _sub_manager->getData<Items>(Topics::items.name);
        auto occ_grid_msg = _sub_manager->getData<OccGrid>(Topics::occupancy_grid.name);
        if (!items_msg)
        {
            RCLCPP_WARN(_node->get_logger(), "There is not data in items message");
            _terminate(goal_handle);
            return;
        }
        if (!occ_grid_msg)
        {
            RCLCPP_WARN(_node->get_logger(), "There is not data in occupancy grid message");
            _terminate(goal_handle);
            return;
        }
        auto input_msg = std::make_shared<place::InputMsg>();
        input_msg->items_msg = *items_msg;
        input_msg->occ_grid_msg = *occ_grid_msg;
        if (_validateMsg(input_msg) != Status_e::SUCCESS)
        {
            _terminate(goal_handle);
            return;
        }
        //--------goal----------------------
        if (_validateGoal(_goal) != Status_e::SUCCESS)
        {
            _terminate(goal_handle);
            return;
        }
        // --------------------algorithm---------------------------------
        auto place = std::make_unique<Place>(_node->get_logger(), true);
        auto place_msg = std::make_shared<PlaceMsg>();
        if (place->execute(input_msg, _params, _goal, place_msg) != Status_e::SUCCESS)
        {
            _terminate(goal_handle);
            return;
            RCLCPP_INFO(_node->get_logger(), "Output Msg");
        }
        //-----------Output Msg-----------------
        if (_validateOutput(place_msg) != Status_e::SUCCESS)
        {
            _terminate(goal_handle);
            return;
        }
        //----------------------publish---------------------
        _setMsg(_goal, place_msg);
        _place_pub->publish(*place_msg);
        //-----------------------result----------------
        auto result = std::make_shared<PlaceAction::Result>();
        result->place_poses = place_msg->place_poses;
        goal_handle->succeed(result);
    }

    // ___________ data getters______________________
    // void PlacePlugin::_getMsg(const std::shared_ptr<InputMsg> input_msg)
    // {
    //     helpers::Timer(__func__);
    //     // PLEASE do not dereference pointer if you are not sure whether there is nullptr or not
    //     input_msg->items_msg = *(_sub_manager->getData<Items>(Topics::items.name));
    //     input_msg->occ_grid_msg = *(_sub_manager->getData<OccGrid>(Topics::occupancy_grid.name));
    // }
    // ___________ data setters______________________
    void PlacePlugin::_setMsg(const Goal &goal,
                              std::shared_ptr<PlaceMsg> output_msg)
    {
        helpers::Timer(__func__);
        output_msg->header.frame_id = "world";
        output_msg->header.stamp.sec = _node->now().seconds();
        output_msg->header.stamp.nanosec = _node->now().nanoseconds();
        //---passi->gg unecessay data for control team---//
        output_msg->grasp_poses = goal.grasp_msg.grasp_poses;
        output_msg->selected_item_id = goal.selected_item_id;
    }

    // __________ ROS Action Functions_______________
    rclcpp_action::GoalResponse PlacePlugin::_handle_goal(
        const rclcpp_action::GoalUUID &uuid,
        std::shared_ptr<const PlaceAction::Goal> goal)
    {
        _goal.grasp_msg.grasp_poses.resize(1);
        helpers::Timer(__func__);
        RCLCPP_INFO(_node->get_logger(), "Received goal ");
        try
        {
            _goal.search_shift = std::stof(goal->search_shift);
            _goal.no_of_required_place_positions = std::stoi(goal->place_positions_required);
            _goal.grasp_msg.grasp_poses[0] = goal->grasp_pose;
            _goal.selected_item_id = goal->selected_item_id;

            for (auto area : goal->selected_area_label)
            {
                if ((area.find("bowl") != area.npos || area.find("plate") != area.npos || area.find("cutting_board") != area.npos))
                {
                    int brain_id;
                    if (_getBrainId(area, brain_id))
                    {
                        return rclcpp_action::GoalResponse::REJECT;
                    }
                    _goal.selected_container_id = brain_id;
                    _goal.place_location = "container";
                    RCLCPP_INFO(_node->get_logger(), "selected_container_id  :  '%i'", _goal.selected_container_id);

                    // std::cout << "_goal.selected_container_id : " << _goal.selected_container_id << std::endl;
                    // std::cout << "_goal.place_location : " << _goal.place_location << std::endl;
                }
                else
                {
                    _goal.selected_areas_labels = goal->selected_area_label;
                    _goal.selected_area_operator = goal->selected_area_operator;
                    _goal.place_location = "area";
                    break;
                }
            }
        }
        // check here if container
        catch (const json::exception &e)
        {
            RCLCPP_FATAL_STREAM(_node->get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
            return rclcpp_action::GoalResponse::REJECT;
        }
        RCLCPP_INFO(_node->get_logger(), "search_shift :  '%f'", _goal.search_shift);
        RCLCPP_INFO(_node->get_logger(), "selected_areas_labels : ");
        for (auto &area_label : _goal.selected_areas_labels)
        {
            RCLCPP_INFO(_node->get_logger(), "   '%s' ", area_label.c_str());
        }
        RCLCPP_INFO(_node->get_logger(), "selected_area_operator :  '%s'", _goal.selected_area_operator.c_str());
        RCLCPP_INFO(_node->get_logger(), "place_positions_required :  '%i'", _goal.no_of_required_place_positions);
        (void)uuid;
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse PlacePlugin::_handle_cancel(
        const std::shared_ptr<GoalHandlePlaceAction> goal_handle)
    {
        RCLCPP_INFO(_node->get_logger(), "Received request to cancel goal");
        (void)goal_handle;
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void PlacePlugin::_handle_accepted(const std::shared_ptr<GoalHandlePlaceAction> goal_handle)
    {
        _execute(goal_handle);
        // std::thread{std::bind(&PlacePlugin::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    //___________ ROS Params___________
    int PlacePlugin::_getParams()
    {
        helpers::Timer(__func__);
        RCLCPP_INFO_ONCE(_node->get_logger(), "Reading parameters from the server");
        // TODO: Hasan Farag - Read from param srvu
        _params.item_rotation_angle = M_PI / 6.0;

        if (_parameters_read)
            return Status_e::SUCCESS;

        try
        {
            auto parameters = helpers::commons::getParameters({"place", "get_occupancy_grid", "security_area", "areas"});
            if (parameters.empty())
                return Status_e::FAILURE;
            json place_parameters = parameters["place"];
            json occ_grid = parameters["get_occupancy_grid"];
            json security_areas = parameters["security_area"];
            json areas = parameters["areas"];

            // Place specific parameters
            _params.number_of_positions_on_table = place_parameters["place_position_on_the_table"];
            RCLCPP_INFO_STREAM(_node->get_logger(), "Place will return place positions equal too : " << _params.number_of_positions_on_table);

            // Occupancy grid parameter
            _params.occ_grid_leaf_size = occ_grid["occ_grid_leaf_size"];
            RCLCPP_INFO_STREAM(_node->get_logger(), "grid_size[occ_grid_leaf_size]" << _params.occ_grid_leaf_size);

            // Security area parameters
            auto &JsonToPosition = helpers::vision::assignPositionFromJson;
            _params.table_areas_coords["security_area"] = {
                {"min_exter", JsonToPosition(security_areas["min_exter"])},
                {"max_exter", JsonToPosition(security_areas["max_exter"])},
                {"min_inter", JsonToPosition(security_areas["min_inter"])},
                {"max_inter", JsonToPosition(security_areas["max_inter"])}};

            // Areas parameters
            for (const auto &area : areas.items())
            {
                json area_name = area.key();
                json area_data = area.value();
                if (area_data.contains("min") && area_data.contains("max"))
                {
                    _params.table_areas_coords[area_name] = {{"min", JsonToPosition(area_data["min"])},
                                                             {"max", JsonToPosition(area_data["max"])}};
                }
            }
        }
        catch (const json::exception &e)
        {
            RCLCPP_FATAL_STREAM(_node->get_logger(), "JSON error with ID: " << e.id << "; message: " << e.what());
            return Status_e::FAILURE;
        }
        catch (const std::exception &e)
        {
            RCLCPP_FATAL_STREAM(_node->get_logger(), "Other error with message: " << e.what());
            return Status_e::FAILURE;
        }
        
        _parameters_read = true;
        return Status_e::SUCCESS;
    }
    int PlacePlugin::_getBrainId(std::string container_brain_name, int32_t &container_id)
    {
        std::vector<size_t> positions;
        size_t pos = container_brain_name.find("_");
        container_id = 0;
        while (pos != std::string::npos)
        {
            positions.push_back(pos);
            pos = container_brain_name.find("_", pos + 1);
        }

        if (pos == 0)
        {
            container_id = 0;
            RCLCPP_INFO(_node->get_logger(), "Container with name %s does not exist in coppelia brain", container_brain_name);
            return 1;
        }
        else
        {
            for (auto position : positions)
            {
                std::string item_id = container_brain_name.substr(position + 1);
                std::istringstream(item_id) >> container_id;
                if (container_id == 0)
                    continue;
                return 0;
            }
        }
        RCLCPP_INFO(_node->get_logger(), "Container name contains invalid or no id ");
        return 1;
    }

} // namespace place

SIM_PLUGIN("PlacePlugin", 1, place::PlacePlugin)
#include "stubsPlusPlus.cpp"
