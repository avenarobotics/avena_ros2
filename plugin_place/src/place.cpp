#include "place.hpp"
#include <opencv2/imgproc.hpp>
using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;
namespace place
{
    Place::Place(rclcpp::Logger logger, bool debug) : _logger(logger), _debug(debug) {}
    //______Algorithm_________________
    int Place::execute(const std::shared_ptr<const InputMsg> input_msg, const Params &params, const Goal &goal, std::shared_ptr<PlaceMsg> output_msg)
    {
        // helpers::TimerROS(__func__,_logger);
        cv::Mat occ_grid;
        helpers::converters::rosImageToCV(input_msg->occ_grid_msg.occupancy_grid_ros, occ_grid);
        std::vector<std::vector<Eigen::Vector3f>> target_areas_coords;
        if (goal.place_location == "area")
        {
            if (_getTargetAreasCoords(params.table_areas_coords,
                                      goal.selected_areas_labels,
                                      goal.selected_area_operator,
                                      target_areas_coords) != Status_e::SUCCESS)
            {
                RCLCPP_WARN(_logger, "Failed to obtain target area coordinates");
                return Status_e::FAILURE;
            }
        }
        // get container coords and modify the selected container at occ grid
        if (goal.place_location == "container")
        {
            if (_getContainerCoords(params.table_areas_coords, occ_grid, params.occ_grid_leaf_size, goal.selected_container_id, input_msg->items_msg.items, target_areas_coords) != Status_e::SUCCESS)
            {
                RCLCPP_WARN(_logger, "Failed to obtain container coordinates");
                return Status_e::FAILURE;
            }
        }
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_in_hand_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
        if (_getItemInHandPtcld(input_msg->items_msg.items,
                                goal.selected_item_id,
                                item_in_hand_ptcld) != Status_e::SUCCESS)
        {
            return Status_e::FAILURE;
        }
        Eigen::Vector3f initial_item_positon;
        Eigen::Vector2f initial_obb_dims;
        _computeObbDimsAndPosition(item_in_hand_ptcld, initial_obb_dims, initial_item_positon);
        // if (_loadSceneData() != Status_e::SUCCESS)
        // {
        //     return Status_e::FAILURE;
        // }
        std::string item_in_hand_label;
        for (auto &item : input_msg->items_msg.items)
        {
            if (item.id == goal.selected_item_id)
            {
                item_in_hand_label = item.label;
                break;
            }
        }
        int place_proposals_amount;
        place_proposals_amount = M_PI * 2 / params.item_rotation_angle;
        output_msg->place_poses.clear();
        // pcl::visualization::PCLVisualizer::Ptr debug(new pcl::visualization::PCLVisualizer);
        pcl::PointCloud<pcl::PointXYZ>::Ptr rotated_item_in_hand_ptcld_visualizer(new pcl::PointCloud<pcl::PointXYZ>);

        // for each grasp: compute specific no of place positions
        // static int img_idx = 0;
        for (size_t grasp_pose_idx = 0; grasp_pose_idx < goal.grasp_msg.grasp_poses.size(); grasp_pose_idx++)
        {
            // for each area: search for set of place poses
            for (auto &target_area_coords : target_areas_coords)
            {
                Eigen::Vector3f target_area_min(target_area_coords[0]), target_area_max(target_area_coords[1]);
                auto [padded_height, padded_width] = _getSearchLimits(target_area_coords, goal.search_shift, initial_obb_dims);
                float top_corner_x = target_area_max.x(); // top
                float top_corner_y;
                for (int idx_x = 0; idx_x < padded_height; ++idx_x)
                {
                    top_corner_y = target_area_max.y(); // left
                    for (int idx_y = 0; idx_y < padded_width; ++idx_y)
                    {
                        std::vector<Eigen::Affine3f> place_poses;
                        custom_interfaces::msg::PlaceData place_pose_saving;
                        for (int i = 0; i < place_proposals_amount; i++)
                        {
                            pcl::PointCloud<pcl::PointXYZ>::Ptr rotated_item_in_hand_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
                            rotated_item_in_hand_ptcld = _rotateItem(item_in_hand_ptcld, i * params.item_rotation_angle);
                            Eigen::Vector3f obb_center_position;
                            Eigen::Vector2f obb_dims;
                            if (_computeObbDimsAndPosition(rotated_item_in_hand_ptcld, obb_dims, obb_center_position) != Status_e::SUCCESS)
                            {
                                return Status_e::FAILURE;
                            }
                            Eigen::Vector3f place_position = {top_corner_x - obb_dims.x() / 2, top_corner_y - obb_dims.y() / 2, 0.0};
                            Eigen::Affine3f transform(Eigen::Affine3f::Identity());
                            transform.translation() = place_position;
                            pcl::transformPointCloud(*rotated_item_in_hand_ptcld, *rotated_item_in_hand_ptcld, transform);
                            *rotated_item_in_hand_ptcld_visualizer = *rotated_item_in_hand_ptcld;
                            cv::Mat item_img(cv::Mat::zeros(occ_grid.rows, occ_grid.cols, CV_8UC1));
                            _getImageFromPtcld(params, *rotated_item_in_hand_ptcld, params.occ_grid_leaf_size, item_img);
                            // debug = helpers::vision::visualize({rotated_item_in_hand_ptcld_visualizer}, {place_poses}, debug, "Place affines");
                            cv::Mat item_collsion_view;
                            cv::Mat item_collsion_view_or;
                            cv::bitwise_and(item_img, occ_grid, item_collsion_view);
                            // cv::bitwise_or(item_img, occ_grid, item_collsion_view_or);

                            // cv::Rect border(cv::Point((int)(place_position.y()) + 120, (int)(place_position.x())), item_collsion_view_or.size());
                            // cv::Rect border(cv::Point(59, 67 + 120),(3,3));

                            // cv::imwrite("/root/ros2_ws/images/item_img" + std::to_string(img_idx) + ".png", item_img);
                            // cv::imwrite("/root/ros2_ws/images/occ_grid" + std::to_string(img_idx) + ".png", occ_grid);
                            // cv::imwrite("/root/ros2_ws/images/item_collsion" + std::to_string(img_idx) + ".png", item_collsion_view);
                            // cv::imwrite("/root/ros2_ws/images/item_collsion_or" + std::to_string(img_idx) + ".png", item_collsion_view_or);

                            // if (cv::countNonZero(item_collsion_view) > 0)
                            // {
                            //     std::cout << "Failure " << img_idx << std::endl;
                            // }
                            // else
                            // {
                            //     std::cout << "Succes " << img_idx << std::endl;
                            // }
                            // img_idx++;
                            // if (_checkFreeSpace(item_img, place_position, obb_dims, occ_grid, params.occ_grid_leaf_size) == Status_e::SUCCESS)
                            if (cv::countNonZero(item_collsion_view) == 0)
                            {
                                // RCLCPP_INFO_STREAM(_logger, "proposed grid place position is free of collision");
                                Eigen::Affine3f place_pose = _rotateGripperToPlace(i * params.item_rotation_angle, goal.grasp_msg.grasp_poses[grasp_pose_idx].grasp_pose, place_position, initial_item_positon);
                                // for collision with table
                                place_pose.translation().z() = 0.0;
                                if (goal.place_location == "area")
                                    place_position.z() = goal.grasp_msg.grasp_poses[grasp_pose_idx].grasp_pose.position.z;
                                if (goal.place_location == "container")
                                    place_position.z() = goal.grasp_msg.grasp_poses[grasp_pose_idx].grasp_pose.position.z + target_area_max.z() + 0.01;
                                // return place pose from world frame
                                place_pose.translation() += place_position;
                                // RCLCPP_INFO_STREAM(_logger, "proposed gripper place position in world frame: " << place_pose.translation());
                                if (_checkGrasp(place_pose, goal.selected_item_id, goal.grasp_msg.grasp_poses[grasp_pose_idx]) == Status_e::SUCCESS)
                                {
                                    place_poses.push_back(place_pose);
                                }
                            }

                            // else
                            //     RCLCPP_WARN_STREAM(_logger, "proposed grid place position is in collision");
                        }
                        if (place_poses.size() >= static_cast<size_t>(goal.no_of_required_place_positions))
                        {
                            RCLCPP_INFO_STREAM(_logger, "place trial for: X " << idx_x << "<Y: " << idx_y << " passed with " << place_poses.size());

                            for (auto &pose : place_poses)
                            {

                                geometry_msgs::msg::Pose ros_pose;
                                helpers::converters::eigenAffineToGeometry(pose, ros_pose);
                                place_pose_saving.poses.push_back(ros_pose);
                            }
                        }
                        if (place_pose_saving.poses.size() >= static_cast<size_t>(goal.no_of_required_place_positions))
                        {
                            output_msg->place_poses.push_back(place_pose_saving);
                        }
                        if (output_msg->place_poses.size() >= static_cast<size_t>(params.number_of_positions_on_table))
                        {
                            RCLCPP_INFO(_logger, "Found  %i place positions ", output_msg->place_poses.size());
                            return Status_e::SUCCESS;
                        } // end of place proposals
                        place_poses.clear();
                        top_corner_y -= goal.search_shift;
                    } // target area y search
                    top_corner_x -= goal.search_shift;
                } // target area x search

                RCLCPP_INFO(_logger, "Goal failed for current area, proceeding to next one");
            }
            if (output_msg->place_poses.size() >= static_cast<size_t>(params.number_of_positions_on_table))
            {
                RCLCPP_INFO(_logger, "Found  %i place positions ", output_msg->place_poses.size());
                return Status_e::SUCCESS;
            }
            // end of areas
            // // only one grasp: No need for for loop, it is left for future for list
            // return Status_e::FAILURE;
        } // end of grasps
        return Status_e::FAILURE;
    }
    int Place::_getTargetAreasCoords(const std::map<std::string,
                                                    std::map<std::string, Eigen::Vector3f>>
                                         table_areas_coords,
                                     const std::vector<std::string> selected_areas_labels,
                                     const std::string selected_area_operator,
                                     std::vector<std::vector<Eigen::Vector3f>> &out_target_areas_coords)
    {
        helpers::Timer(__func__);
        RCLCPP_INFO_STREAM(_logger, __func__ << " started");
        if (selected_area_operator == _TableAreasOperators.at(AreaOperator_e::INCLUDE))
        {
            for (auto &area_label : selected_areas_labels)
            {
                if (area_label == _WholeTable)
                {
                    out_target_areas_coords.push_back({table_areas_coords.at("security_area").at("min_inter"), table_areas_coords.at("security_area").at("max_inter")});
                    return Status_e::SUCCESS;
                }
                else if (area_label != _TableAreasNames.at(Area_e::SECURITY_AREA))
                {
                    out_target_areas_coords.push_back({table_areas_coords.at(area_label).at("min"), table_areas_coords.at(area_label).at("max")});
                }
                else
                {
                    RCLCPP_WARN(_logger, "You should not pass security area as target area");
                }
            }
        }
        else if (selected_area_operator == _TableAreasOperators.at(AreaOperator_e::EXCLUDE))
        {
            std::map<std::string,
                     std::map<std::string, Eigen::Vector3f>>
                available_table_areas_coords(table_areas_coords);

            available_table_areas_coords.erase(_TableAreasNames.at(Area_e::SECURITY_AREA));

            for (auto &area_label : selected_areas_labels)
            {
                if (available_table_areas_coords.size() != 0)
                    available_table_areas_coords.erase(area_label);
            }

            if (available_table_areas_coords.size() != 0)
            {
                for (auto &area_coords : available_table_areas_coords)
                {
                    out_target_areas_coords.push_back({area_coords.second["min"], area_coords.second["max"]});
                }
            }
            else
            {
                RCLCPP_ERROR(_logger, "Area vector is empty(You probably excluded all areas), Goal Failed");
                return Status_e::INVALID;
            }
        }
        else
        {
            RCLCPP_ERROR(_logger, "Unsupported area operator");
            return Status_e::INVALID;
        }
        if (out_target_areas_coords.size() != 0)
            return Status_e::SUCCESS;
        else
            return Status_e::FAILURE;
    }
    int Place::_getItemInHandPtcld(const std::vector<Item> &items, const int &item_in_hand_id, pcl::PointCloud<pcl::PointXYZ>::Ptr out_item_in_hand_ptcld)
    {
        // helpers::Timer(__func__);
        // RCLCPP_INFO_STREAM(_logger, __func__ << " started");
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_element_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
        auto item_result_it = std::find_if(items.begin(), items.end(),
                                           [item_in_hand_id](custom_interfaces::msg::Item item)
                                           {
                                               return item.id == item_in_hand_id;
                                           });
        if (item_result_it != items.end())
        {
            for (auto &item : items)
            {
                if (item.id == item_in_hand_id)
                {
                    for (auto &item_element : item.item_elements)
                    {
                        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.merged_ptcld, item_element_ptcld);
                        *out_item_in_hand_ptcld += *item_element_ptcld;
                        item_element_ptcld->clear();
                    }
                }
            }
            helpers::vision::passThroughFilter(out_item_in_hand_ptcld, "z", -0.005, 0.005, true);
            helpers::vision::statisticalOutlierRemovalFilter(out_item_in_hand_ptcld);
            if (out_item_in_hand_ptcld->points.size() == 0)
            {
                RCLCPP_WARN(_logger, "Item data pointcloud is empty ");
                return Status_e::INVALID;
            }
            return Status_e::SUCCESS;
        }
        else
        {
            RCLCPP_ERROR(_logger, "Item in hand does exist in input message");
            return Status_e::FAILURE;
        }
    }
    int Place::_checkFreeSpace(const cv::Mat &item_img, const Eigen::Vector3f &place_position, const Eigen::Vector2f &obb_dims, const cv::Mat &occ_grid, const float &leaf_size)
    {
        // helpers::Timer(__func__);
        // RCLCPP_INFO_STREAM(_logger, __func__ << " started");
        // transform aabb from world to image frame and get index in image
        size_t bottom_x = floor((place_position.x() - obb_dims.x() / 2) / leaf_size);
        size_t bottom_y = std::abs((1 + (place_position.y() - obb_dims.y() / 2)) / leaf_size);
        size_t top_x = floor((place_position.x() + obb_dims.x() / 2) / leaf_size);
        size_t top_y = floor((1 + (place_position.y() + obb_dims.y() / 2)) / leaf_size);

        // _drawBorder(occ_grid, bottom_x, bottom_y, top_x, top_y);
        size_t zero = 0;
        auto clip = [](const size_t n, const size_t lower, const size_t upper)
        {
            return std::max(lower, std::min(n, upper));
        };
        bottom_x = clip(bottom_x, zero, static_cast<size_t>(occ_grid.rows));
        bottom_y = clip(bottom_y, zero, static_cast<size_t>(occ_grid.cols));
        top_x = clip(top_x, zero, static_cast<size_t>(occ_grid.rows));
        top_y = clip(top_y, zero, static_cast<size_t>(occ_grid.cols));
        cv::Rect roi(bottom_y, bottom_x, top_y - bottom_y, top_x - bottom_x);
        cv::Mat place_img = occ_grid(roi);
        cv::Mat item_in_place_img = item_img(roi);
        cv::Mat item_collsion;
        cv::bitwise_and(place_img, item_in_place_img, item_collsion);
        // cv::namedWindow("_checkFreeSpace",cv::WINDOW_GUI_EXPANDED);
        // cv::imshow("_checkFreeSpace",item_collsion);
        // cv::waitKey(0);
        // cv::destroyAllWindows();
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/place_img" + std::to_string(img_idx) + ".png", place_img);
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/item_img" + std::to_string(img_idx) + ".png", item_img);
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/item_in_place_img" + std::to_string(img_idx) + ".png", item_in_place_img);
        // cv::imwrite("/root/ros2_ws/src/avena_coppelia_addons/plugin_place/images/item_collsion" + std::to_string(img_idx) + ".png", item_collsion);

        // _showManyImages(__func__, 5, occ_grid, place_img, item_img, item_in_place_img, item_collsion);
        if (cv::countNonZero(item_collsion) > 0)
        {
            return Status_e::FAILURE;
        }
        else
        {
            return Status_e::SUCCESS;
        }
    }
    int Place::_computeObbDimsAndPosition(pcl::PointCloud<pcl::PointXYZ>::Ptr item_in_hand_ptcld, Eigen::Vector2f &out_obb_dims, Eigen::Vector3f &out_obb_center_position)
    {
        // helpers::Timer(__func__);
        out_obb_center_position = Eigen::Vector3f::Zero();
        out_obb_dims = Eigen::Vector2f::Zero();
        // helpers::Timer(__func__);
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid(*item_in_hand_ptcld, centroid);
        Eigen::Matrix3f covariance;
        computeCovarianceMatrixNormalized(*item_in_hand_ptcld, centroid, covariance);
        Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigen_solver(covariance, Eigen::ComputeEigenvectors);
        Eigen::Matrix3f eigDx = eigen_solver.eigenvectors();
        eigDx.col(2) = eigDx.col(0).cross(eigDx.col(1));
        Eigen::Matrix4f p2w(Eigen::Matrix4f::Identity());
        p2w.block<3, 3>(0, 0) = eigDx.transpose();
        p2w.block<3, 1>(0, 3) = -1.f * (p2w.block<3, 3>(0, 0) * centroid.head<3>());
        pcl::PointCloud<pcl::PointXYZ> cPoints;
        pcl::transformPointCloud(*item_in_hand_ptcld, cPoints, p2w);
        pcl::PointXYZ min_pt, max_pt;
        pcl::getMinMax3D(cPoints, min_pt, max_pt);
        const Eigen::Vector3f mean_diag = 0.5f * (max_pt.getVector3fMap() + min_pt.getVector3fMap());
        // final transform
        const Eigen::Quaternionf qfinal(eigDx);
        const Eigen::Vector3f tfinal = eigDx * mean_diag + centroid.head<3>();

        out_obb_dims = {abs(max_pt.y - min_pt.y) + 0.02, abs(max_pt.z - min_pt.z) + 0.02};
        out_obb_center_position = tfinal;

        return Status_e::SUCCESS;
    }
    Eigen::Affine3f Place::_rotateGripperToPlace(const float &rotation, const geometry_msgs::msg::Pose &grasp_pose, const Eigen::Vector3f &place_position, const Eigen::Vector3f &initial_item_position)
    {
        Eigen::Affine3f grasp_pose_mat;
        helpers::converters::geometryToEigenAffine(grasp_pose, grasp_pose_mat);
        Eigen::Vector3f grasp_in_item_frame_vec = grasp_pose_mat.translation() - initial_item_position;
        Eigen::Affine3f grasp_pose_item_frame = Eigen::Translation3f(grasp_in_item_frame_vec) * Eigen::Quaternionf(grasp_pose_mat.rotation());
        Eigen::Vector3f grasp_in_item_frame_vec_flattended = grasp_in_item_frame_vec;
        grasp_in_item_frame_vec_flattended(2) = 0.0;
        Eigen::Quaternionf grasp_to_place_angle;
        grasp_to_place_angle.setFromTwoVectors(grasp_in_item_frame_vec_flattended, place_position);
        Eigen::Affine3f initial_place = grasp_pose_item_frame;
        initial_place = grasp_to_place_angle * initial_place;
        return Eigen::Quaternionf(helpers::vision::assignRotationMatrixAroundZ(M_PI)) * Eigen::Quaternionf(helpers::vision::assignRotationMatrixAroundZ(rotation)) * initial_place;
    }
    int Place::_checkGrasp(Eigen::Affine3f &place_pose, const int &selected_item_id, const custom_interfaces::msg::GraspData &grasp)
    {
        auto parameters = helpers::commons::getParameters({"robot"});
        if (parameters.empty())
            return 1;
        json robot_property = parameters["robot"];

        const std::string working_side = robot_property["working_side"];
        auto robot_info = helpers::commons::getRobotInfo(working_side);
        helpers::commons::RobotInfo collision_robot_info;
        if (working_side == "right")
            collision_robot_info = helpers::commons::getRobotInfo("left");
        else
            collision_robot_info = helpers::commons::getRobotInfo("right");

        int target = simGetObjectHandle((robot_info.robot_prefix + "_target").c_str());
        int world = simGetObjectHandle(CoppeliaScene::world_name.c_str());
        int ik_handle = simGetIkGroupHandle((robot_info.robot_prefix + "_ik").c_str());
    
        const std::string robot_collection_name = robot_info.robot_prefix + "_collection";
        int robot_handle = simGetCollectionHandle(robot_collection_name.c_str());
        const std::string collision_robot_collection_name = collision_robot_info.robot_prefix + "_collection";
        int collision_robot_handle = simGetCollectionHandle(collision_robot_collection_name.c_str());
        int scene_collection_handle = simGetCollectionHandle("scene_collection");

        std::vector<int> panda_collision;
        panda_collision.push_back(robot_handle);
        panda_collision.push_back(scene_collection_handle);
        panda_collision.push_back(robot_handle);
        panda_collision.push_back(collision_robot_handle);

        std::vector<int> joint_names;                         
        for (auto joint_name : robot_info.joint_names)
        {
            // std::string joint_name = "left_joint_" + std::to_string(joint_idx) + "_ghost";
            int joint = simGetObjectHandle(joint_name.c_str());
            joint_names.emplace_back(joint);
        }
        std::pair<Eigen::Affine3f, int> pair;
        std::vector<float> joint_positions(robot_info.nr_joints);
        Eigen::Vector3f vec = place_pose.translation();
        Eigen::Quaternionf quat(place_pose.rotation());
        Eigen::Quaternionf quat_pick(grasp.grasp_pose.orientation.w,
                                     grasp.grasp_pose.orientation.x,
                                     grasp.grasp_pose.orientation.y,
                                     grasp.grasp_pose.orientation.z);

        // pick target
        std::vector<float> position_pick = {static_cast<float>(grasp.grasp_pose.position.x),
                                            static_cast<float>(grasp.grasp_pose.position.y),
                                            static_cast<float>(grasp.grasp_pose.position.z)};
        std::vector<float> orientation_pick = {quat_pick.w(), quat_pick.x(), quat_pick.y(), quat_pick.z()};

        // place target
        std::vector<float> position_place = {vec.x(), vec.y(), vec.z() + 0.01f};
        std::vector<float> orientation_place = {quat.w(), quat.x(), quat.y(), quat.z()};

        // set pick target
        simSetObjectPosition(target, world, position_pick.data());
        simSetObjectQuaternion(target + sim_handleflag_wxyzquaternion, world, orientation_pick.data());

        // find config for pick pose
        int ik_result = simGetConfigForTipPose(ik_handle, joint_names.size(), joint_names.data(), 0.2, 50,
                                               joint_positions.data(), nullptr, panda_collision.size() / 2, panda_collision.data(),
                                               nullptr, nullptr, nullptr, nullptr);

        // if ok go to that pose
        if (ik_result == IKResult_e::IK_SUCCESS)
        {
            for (size_t joint_idx = 0; joint_idx < joint_names.size(); joint_idx++)
                simSetJointPosition(joint_names[joint_idx], joint_positions[joint_idx]);
        }
        else
        {
            RCLCPP_ERROR(_logger, "Cannot find pose for grasp!");
            return Status_e::FAILURE;
        }

        // get object handle id
        int _hand = simGetObjectHandle(robot_info.connection.c_str());
        int _parent_handle = simGetObjectHandle(CoppeliaScene::collision.c_str());
        int g_selected_item_id = selected_item_id;
        int g_selected_item_handle = 0;
        std::string str_look_for = "_" + std::to_string(g_selected_item_id);
        int object_count;
        int *returned_handles = simGetObjectsInTree(_parent_handle, sim_handle_all, 2, &object_count);
        std::vector<int> objects = std::vector<int>(returned_handles, returned_handles + object_count);
        for (size_t i = 0; i < objects.size(); i++)
        {
            std::string name = simGetObjectName(objects[i]);
            size_t res = name.find(str_look_for);
            if (res != std::string::npos)
            {
                g_selected_item_handle = objects[i];
            }
        }
        if (g_selected_item_handle == 0)
        {
            RCLCPP_ERROR(_logger, "Invalid item handle, aborting");
            return Status_e::FAILURE;
        }

        // save object matrix to restore after place calculation
        std::vector<float> g_item_matrix(16);
        simGetObjectMatrix(g_selected_item_handle, world, g_item_matrix.data());

        // attach the object for calcualtions
        int result = simSetObjectParent(g_selected_item_handle, _hand, true);
        if (result == -1)
        {
            RCLCPP_ERROR(_logger, "Object attach failed");
        }

        // set place target
        simSetObjectPosition(target, world, position_place.data());
        simSetObjectQuaternion(target + sim_handleflag_wxyzquaternion, world, orientation_place.data());

        // find config for place target
        int place_result = simGetConfigForTipPose(ik_handle, joint_names.size(), joint_names.data(), 0.2, 50,
                                                  joint_positions.data(), nullptr, panda_collision.size() / 2, panda_collision.data(),
                                                  nullptr, nullptr, nullptr, nullptr);

        // if ok go to that pose
        if (place_result == IKResult_e::IK_SUCCESS)
        {
            for (size_t joint_idx = 0; joint_idx < joint_names.size(); joint_idx++)
                simSetJointPosition(joint_names[joint_idx], joint_positions[joint_idx]);
        }

        // detach object
        result = simSetObjectParent(g_selected_item_handle, _parent_handle, true);
        if (result == -1)
        {
            RCLCPP_INFO(_logger, "Detaching failed ");
        }
        // restore object
        result = simSetObjectMatrix(g_selected_item_handle, world, g_item_matrix.data());
        if (result == -1)
        {
            RCLCPP_INFO(_logger, "Restoring item failed ");
        }

        // return success
        if (place_result == IKResult_e::IK_SUCCESS)
            return Status_e::SUCCESS;
        else
            return Status_e::FAILURE;

        return Status_e::FAILURE;
    }
    int Place::_loadSceneData()
    {
        // // helpers::Timer(__func__);
        // // RCLCPP_INFO_STREAM(_logger, __func__ << " started");
        // std::vector<int> joints;
        // for (size_t joint_idx = 1; joint_idx <= joint_names.size(); joint_idx++)
        // {
        //     std::string joint_name = "left_joint_" + std::to_string(joint_idx) + "_ghost";
        //     int joint = simGetObjectHandle(joint_name.c_str());
        //     joints.push_back(joint);
        // }
        // if (joints.size() != 0)
        // {
        //     return Status_e::SUCCESS;
        // }
        // else
        // {
        //     return Status_e::FAILURE;
        // }
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr Place::_rotateItem(const pcl::PointCloud<pcl::PointXYZ>::Ptr item_in_hand_ptcld, const float &rotation_angle)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr rotated_item_in_hand_ptcld(item_in_hand_ptcld);
        Eigen::Affine3f transform(Eigen::Affine3f::Identity());
        Eigen::Vector4f centroid(Eigen::Vector4f::Zero());
        pcl::compute3DCentroid(*rotated_item_in_hand_ptcld, centroid);
        transform.translation() = -centroid.head<3>();
        pcl::transformPointCloud(*rotated_item_in_hand_ptcld, *rotated_item_in_hand_ptcld, transform);
        // helpers::vision::visualize({rotated_item_in_hand_ptcld});
        transform = Eigen::Affine3f::Identity();
        Eigen::Matrix3f rotation(Eigen::AngleAxisf(rotation_angle, Eigen::Vector3f::UnitZ()));
        transform.rotate(rotation);
        pcl::transformPointCloud(*rotated_item_in_hand_ptcld, *rotated_item_in_hand_ptcld, transform);
        // helpers::vision::visualize({rotated_item_in_hand_ptcld});
        return rotated_item_in_hand_ptcld;
    }
    void Place::_getImageFromPtcld(Params params, const pcl::PointCloud<pcl::PointXYZ> &item_in_hand_ptcld, const float &occ_grid_leaf_size, cv::Mat &out_item_img)
    {
        out_item_img = cv::Mat::zeros(out_item_img.size(), out_item_img.type());
        Eigen::Vector3f max_point;
        max_point = params.table_areas_coords["table_area"].at("min");
        for (auto const &pt : item_in_hand_ptcld.points)
        {
            size_t x_img = floor(pt.x / occ_grid_leaf_size);
            size_t y_img = floor((std::abs(max_point.y()) + pt.y) / occ_grid_leaf_size);
            out_item_img.at<uint8_t>(x_img, y_img) = OccGrid_e::OCCUPIED;
        }

        cv::Mat kernel = cv::getStructuringElement(cv::MorphShapes::MORPH_ELLIPSE, cv::Size(3, 3));
        cv::dilate(out_item_img, out_item_img, kernel);
    }
    std::tuple<int, int> Place::_getSearchLimits(const std::vector<Eigen::Vector3f> &target_area, const float &search_shift, const Eigen::Vector2f &obb_dims)
    {
        Eigen::Vector3f target_area_max(target_area[1]), target_area_min(target_area[0]);

        float height = abs(target_area_max.x() - target_area_min.x());
        float width = abs(target_area_max.y() - target_area_min.y());

        float shift_y = search_shift;
        float shift_x = search_shift;
        // in image
        int padded_height = floor((height - obb_dims.x()) / shift_x);
        int padded_width = floor((width - obb_dims.y()) / shift_y);
        return {padded_height, padded_width};
    }
    int Place::_getContainerCoords(const std::map<std::string,
                                                  std::map<std::string, Eigen::Vector3f>>
                                       table_areas_coords,
                                   cv::Mat &occ_grid, const float &occ_grid_leaf_size, const int &selected_container_id, const std::vector<Item> &items, std::vector<std::vector<Eigen::Vector3f>> &out_target_areas_coords)
    {
        // only one container
        out_target_areas_coords.resize(1);
        std::vector<Eigen::Vector3f> container_coords;
        std::vector<int> container_x, container_y;
        auto item_result_it = std::find_if(items.begin(), items.end(),
                                           [selected_container_id](custom_interfaces::msg::Item item)
                                           {
                                               return item.id == selected_container_id;
                                           });
        if (item_result_it != items.end()) // container exists in the scene
        {

            for (int x = 0; x < occ_grid.rows; x++)
            {
                for (int y = 0; y < occ_grid.cols; y++)
                {
                    // Accesssing values of each pixel
                    if (occ_grid.at<uint8_t>(x, y) == selected_container_id)
                    {
                        container_x.emplace_back(x);
                        container_y.emplace_back(y);
                        // free container (not contents) from occ_grid to check against for selected item
                        occ_grid.at<uint8_t>(x, y) = 0;
                    }
                    else
                    {
                        occ_grid.at<uint8_t>(x, y) = 255;
                    }
                }
            }
            Eigen::Vector3f max_point;
            Eigen::Vector3f min_point;
            max_point = table_areas_coords.at("table_area").at("max");
            min_point = table_areas_coords.at("table_area").at("min");
            auto min_x = std::min_element(container_x.begin(), container_x.end());
            auto max_x = std::max_element(container_x.begin(), container_x.end());
            auto min_y = std::min_element(container_y.begin(), container_y.end());
            auto max_y = std::max_element(container_y.begin(), container_y.end());

            Eigen::Vector3f container_min{*min_x * occ_grid_leaf_size, *min_y * occ_grid_leaf_size - std::abs(min_point.y()), 0.0};
            // std::cout << "container_min values" << std::endl;
            // std::cout << "container_min.x()" << container_min.x() << std::endl;
            // std::cout << "container_min.y)" << container_min.y() << std::endl;
            // std::cout << "container_min.z()" << container_min.z() << std::endl;
            container_coords.emplace_back(container_min);
            float container_height(0.0);
            // get container height
            for (auto &item : items)
            {
                if (item.id == selected_container_id)
                {
                    json item_primitive = json::parse(item.item_elements[0].parts_description[0]);
                    try
                    {
                        if (item.label == "cutting_board")
                        {
                            container_height = item_primitive["board"]["dims"]["x"];
                        }

                        else if (item.label == "bowl" || "plate")
                        {
                            container_height = item_primitive["ramp"]["dims"]["height"];
                        }

                        else
                        {
                            RCLCPP_WARN_STREAM(_logger, "container is not supported");
                        }
                    }
                    catch (const json::exception &e)
                    {
                        RCLCPP_FATAL_STREAM(_logger, "Item Height JSON Exception with ID: " << e.id << "; message: " << e.what());
                    }
                    break;
                }
            }
            Eigen::Vector3f container_max{*max_x * occ_grid_leaf_size, *max_y * occ_grid_leaf_size - std::abs(max_point.y()), container_height};
            // std::cout << "container_max values" << std::endl;
            // std::cout << "container_max.x()" << container_max.x() << std::endl;
            // std::cout << "container_max.y)" << container_max.y() << std::endl;
            // std::cout << "container_max.z()" << container_max.z() << std::endl;
            // table limits check
            if (container_min.x() >= min_point.x() && container_max.x() <= max_point.x() &&
                container_min.y() >= min_point.y() && container_max.y() <= max_point.y())
            {
                container_coords.emplace_back(container_max);
                out_target_areas_coords[0] = container_coords;
                // cv::namedWindow("container_data", cv::WINDOW_GUI_EXPANDED);
                // cv::imshow("container_data", occ_grid);
                // cv::waitKey(0);
                return Status_e::SUCCESS;
            }
            else
                return Status_e::FAILURE;
        }
        else
        {
            RCLCPP_WARN_STREAM(_logger, "container does not exist in the scene");
            return Status_e::INVALID;
        }
    }
} // namespace place
