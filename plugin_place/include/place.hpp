#ifndef PLACE_HPP
#define PLACE_HPP
//_________Structs And Enums______
#include "structs_enums.hpp"

//____Coppelia Plugin_____
#include "config.h"
#include "simPlusPlus/Plugin.h"
#include "stubs.h"

//_____CPP_______
#include <vector>
#include <memory>
#include <algorithm>
#include <chrono>
//____PCL_____
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/features/moment_of_inertia_estimation.h>

// ______ROS______
//__loggger___
#include <rclcpp/rclcpp.hpp>

//__macros__
#include "rclcpp_components/register_node_macro.hpp"
//___________Avena______________
#include "helpers_vision/helpers_vision.hpp"
#include "custom_interfaces/msg/grasp.hpp"
#include <geometry_msgs/msg/pose.hpp>
#include <helpers_commons/helpers_commons.hpp>
#include <helpers_vision/helpers_vision.hpp>

namespace place
{
    using json = nlohmann::json;

    //__________________ place______________
    class Place
    {

    public:
        // _________local declarations___________
        using Item = custom_interfaces::msg::Item;

        // ________place________
        explicit Place(rclcpp::Logger logger, bool debug = false);
        ~Place(){};
        int execute(const std::shared_ptr<const InputMsg> input_msg, const Params &params, const Goal &goal, std::shared_ptr<PlaceMsg> output_msg);

    private:
        // _________________constants__________________
        const std::map<size_t, std::string> _TableAreasNames = {{Area_e::SECURITY_AREA, "security_area"},
                                                                {Area_e::TOOLS_AREA, "tools_area"},
                                                                {Area_e::ITEM_AREA, "item_area"},
                                                                {Area_e::OPERATING_AREA, "operating_area"}};

        const std::map<size_t, std::string> _TableAreasOperators = {{AreaOperator_e::INCLUDE, "inside"},
                                                                    {AreaOperator_e::EXCLUDE, "outside"}};
        const std::string _WholeTable = "whole";

        // _________________logger__________________
        rclcpp::Logger _logger;
        bool _debug;
        // ___________algorthim_____________
        int _getTargetAreasCoords(const std::map<std::string,
                                                 std::map<std::string, Eigen::Vector3f>>
                                      table_areas_coords,
                                  const std::vector<std::string> selected_areas_labels,
                                  const std::string selected_area_operator,
                                  std::vector<std::vector<Eigen::Vector3f>> &out_target_areas_coords);
        int _getItemInHandPtcld(const std::vector<Item> &items,
                                const int &item_in_hand_id,
                                pcl::PointCloud<pcl::PointXYZ>::Ptr out_item_in_hand_ptcld);
        int _checkFreeSpace(const cv::Mat &item_img, const Eigen::Vector3f &place_position, const Eigen::Vector2f &obb_dims, const cv::Mat &occ_grid, const float &leaf_size);
        int _computeObbDimsAndPosition(pcl::PointCloud<pcl::PointXYZ>::Ptr item_in_hand_ptcld, Eigen::Vector2f &out_obb_dims, Eigen::Vector3f &out_obb_center_position);
        Eigen::Affine3f _rotateGripperToPlace(const float  &rotation,const geometry_msgs::msg::Pose &grasp_pose, const Eigen::Vector3f &place_position, const Eigen::Vector3f &initial_item_position);
        pcl::PointCloud<pcl::PointXYZ>::Ptr _rotateItem(const pcl::PointCloud<pcl::PointXYZ>::Ptr item_in_hand_ptcld, const float &rotation_angle);
        void _getImageFromPtcld(Params params,const pcl::PointCloud<pcl::PointXYZ> &item_in_hand_ptcld, const float &occ_grid_leaf_size, cv::Mat &out_item_img);
        std::tuple<int, int> _getSearchLimits(const std::vector<Eigen::Vector3f> &target_area, const float &search_shift, const Eigen::Vector2f &obb_dims);
        // _________________Containers_________________--
        int _getContainerCoords(const std::map<std::string,
                                                    std::map<std::string, Eigen::Vector3f>>
                                         table_areas_coords,cv::Mat &occ_grid, const float &occ_grid_leaf_size, const int &selected_container_id, const std::vector<Item> &items, std::vector<std::vector<Eigen::Vector3f>> &out_target_areas_coords);
        // ________________coppelia scene data getters __________________
        int _loadSceneData();
        int _checkGrasp(Eigen::Affine3f &place_pose, const int &selected_item_id, const custom_interfaces::msg::GraspData &grasp_pose);
    };

} // namespace place
#endif
