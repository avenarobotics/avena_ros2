#ifndef PLACE_PLUGIN_HPP
#define PLACE_PLUGIN_HPP
//_____CPP_______
#include <chrono>

// ______ROS______
//___client libs___
#include <rclcpp_action/rclcpp_action.hpp>
// __visibility__
#include "visibility_control.h"
// __msgs__
#include <geometry_msgs/msg/pose_array.hpp>
//___AVENA
//__subscription__
#include "helpers_commons/subscriptions_manager.hpp"
// __msgs__
#include "custom_interfaces/action/place_action.hpp"

// ___________algorithm________
#include "place.hpp"

#define PLUGIN_NAME "AvenaPlace"
#define PLUGIN_VERSION 1

namespace place
{
    struct TopicInfo
    {
        std::string name;
        std::string type;
    };
    struct Topics
    {
        inline static const TopicInfo occupancy_grid = {"occupancy_grid", "custom_interfaces/OccupancyGrid"};
        inline static const TopicInfo items = {"merged_items", "custom_interfaces/Items"};
        inline static const TopicInfo place = {"place", "custom_interfaces/Place"};
    };

    //__________________ Plugin______________
    class PlacePlugin : public sim::Plugin
    {
    public:
        // _________declarations___________
        using PlaceAction = custom_interfaces::action::PlaceAction;
        using GoalHandlePlaceAction = rclcpp_action::ServerGoalHandle<PlaceAction>;
        using json = nlohmann::json;

        // __________Coppelia plugin____________________
        PLACE_PLUGIN_PUBLIC
        PlacePlugin();
        PLACE_PLUGIN_PUBLIC
        virtual ~PlacePlugin();
        PLACE_PLUGIN_PUBLIC
        void onStart() override;
        PLACE_PLUGIN_PUBLIC
        void onInstancePass(const sim::InstancePassFlags &flags, bool first) override;
        Goal _goal;

    private:
        //_________________constants__________________-
        const uint8_t _MinNoOfRequiredPlacePositions = 1;
        const uint8_t _MaxNoOfRequiredPlacePositions = 6;
        const float _MinSearchShift = 0.01;

        // const std::map<size_t, std::string> _TableAreasNames = {{Area_e::SECURITY_AREA, "security_area"},
        //                                                         {Area_e::TOOLS_AREA, "tools_area"},
        //                                                         {Area_e::ITEM_AREA, "item_area"},
        //                                                         {Area_e::OPERATING_AREA, "operating_area"}};
        //_______ROS param srv_______________
        Params _params;
        bool _parameters_read;

        PLACE_PLUGIN_LOCAL
        int _getParams();

        //_____ROS node____
        rclcpp::Node::SharedPtr _node;

        // __________ subscription_______________
        helpers::SubscriptionsManager::SharedPtr _sub_manager;

        // __________ publisher _______________
        rclcpp::Publisher<custom_interfaces::msg::Place>::SharedPtr _place_pub;

        // _________Action ___________
        rclcpp_action::Server<PlaceAction>::SharedPtr _place_server;

        PLACE_PLUGIN_LOCAL
        rclcpp_action::GoalResponse _handle_goal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const PlaceAction::Goal> goal);
        PLACE_PLUGIN_LOCAL
        rclcpp_action::CancelResponse _handle_cancel(const std::shared_ptr<GoalHandlePlaceAction> goal_handle);
        PLACE_PLUGIN_LOCAL
        void _handle_accepted(const std::shared_ptr<GoalHandlePlaceAction> goal_handle);

        //__________Place PLugin Main Fucntion_________
        PLACE_PLUGIN_LOCAL
        void _execute(const std::shared_ptr<GoalHandlePlaceAction> goal_handle);

        //________data validation___________

        PLACE_PLUGIN_LOCAL
        int _validateMsg(const std::shared_ptr<const InputMsg> input_msg);
        PLACE_PLUGIN_LOCAL
        int _validateOutput(const std::shared_ptr<const PlaceMsg> output_msg);
        PLACE_PLUGIN_LOCAL
        int _validateGoal(const Goal &goal);

        //________data getters___________

        // PLACE_PLUGIN_LOCAL
        // void _getMsg(const std::shared_ptr<InputMsg> input_msg);
        PLACE_PLUGIN_LOCAL
        void _setMsg(const Goal &goal,
                     std::shared_ptr<PlaceMsg>
                         output_msg);
        // DEVELOP ??????
        // void _setMsg(const std::shared_ptr<const InputMsg> input_msg, const Goal &goal,
        //                                                                   std::shared_ptr<PlaceMsg>
        //                                                                       output_msg);
        // ___ error handlers______
        PLACE_PLUGIN_LOCAL
        void _terminate(const std::shared_ptr<GoalHandlePlaceAction> goal_handle);
        int _getBrainId(std::string container_brain_name, int32_t &container_id);
    };
} // namespace place

#endif // PLACE_PLUGIN_HPP
