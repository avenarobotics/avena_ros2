#ifndef PLACE__VISIBILITY_CONTROL_H_
#define PLACE__VISIBILITY_CONTROL_H_

#ifdef __cplusplus
extern "C"
{
#endif

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define PLACE_PLUGIN_EXPORT __attribute__ ((dllexport))
    #define PLACE_PLUGIN_IMPORT __attribute__ ((dllimport))
  #else
    #define PLACE_PLUGIN_EXPORT __declspec(dllexport)
    #define PLACE_PLUGIN_IMPORT __declspec(dllimport)
  #endif
  #ifdef PLACE_PLUGIN_BUILDING_DLL
    #define PLACE_PLUGIN_PUBLIC PLACE_PLUGIN_EXPORT
  #else
    #define PLACE_PLUGIN_PUBLIC PLACE_PLUGIN_IMPORT
  #endif
  #define PLACE_PLUGIN_PUBLIC_TYPE PLACE_PLUGIN_PUBLIC
  #define PLACE_PLUGIN_LOCAL
#else
  #define PLACE_PLUGIN_EXPORT __attribute__ ((visibility("default")))
  #define PLACE_PLUGIN_IMPORT
  #if __GNUC__ >= 4
    #define PLACE_PLUGIN_PUBLIC __attribute__ ((visibility("default")))
    #define PLACE_PLUGIN_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define PLACE_PLUGIN_PUBLIC
    #define PLACE_PLUGIN_LOCAL
  #endif
  #define PLACE_PLUGIN_PUBLIC_TYPE
#endif

#ifdef __cplusplus
}
#endif

#endif  // DETECT__VISIBILITY_CONTROL_H_
