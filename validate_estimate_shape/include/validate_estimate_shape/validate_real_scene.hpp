#include "custom_interfaces/msg/item.hpp"
#include "helpers_vision/helpers_vision.hpp"
#include "nlohmann/json.hpp"
#include "rclcpp/rclcpp.hpp"
#include "validate_estimate_shape/utils.hpp"
#include <Eigen/Geometry>
#include <chrono>
#include <geometry_msgs/msg/pose.hpp>
#include <helpers_commons/parameters.hpp>
namespace validate_estimate_shape
{
    using namespace std::chrono_literals;
    class RealSceneValidator
    {
    public:
        RealSceneValidator(const std::vector<custom_interfaces::msg::Item> &estiamted_items, const nlohmann::json &real_scene_description, std::map<std::string, nlohmann::json> &points, const std::string &input_json_filename);
        bool runValidation();
        void validateSingleItem(const custom_interfaces::msg::Item &estimated_item);
        void setMaxErrorThresholds(float position, float orientation, float size);
        int getErrorCode();
        int FixHeightOffset(std::string item_label, float &z_offset);


    private:
        void _parseRealSceneDescription();
        int _saveResults(std::vector<nlohmann::json> result_vector);
        int _saveResultsCsv(std::string data);
        int _generateItemRaport(const custom_interfaces::msg::Item &estimated_item,const nlohmann::json real_scene_item, const nlohmann::json position_error, const nlohmann::json orientation_error, float distance);

        nlohmann::json _findNearestRealSceneItem(const custom_interfaces::msg::Item &estimated_item, float &out_distance);
        float _getDistanceError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _nlohGetDistanceError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);

        nlohmann::json _getOrangeSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getCucumberSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getCarrotSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getMilkSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getLiptonSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getBowlSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getPlateSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);

        nlohmann::json _getCucumberOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getCarrotOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getMilkOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getLiptonOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getPlateOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);
        nlohmann::json _getBowlOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item);

        std::vector<custom_interfaces::msg::Item> _estimated_items;
        std::vector<nlohmann::json> _real_scene_items;
        std::vector<nlohmann::json> _raports_to_save;
        nlohmann::json _labels_data;
        nlohmann::json _raport;
        float _max_position_error;
        float _max_orientation_error;
        float _max_size_error;
        std::map<std::string, nlohmann::json> _real_scene_points;
        std::string _input_json_filename;
        std::stringstream _csv_raport;


        int _error_code;
    };
}