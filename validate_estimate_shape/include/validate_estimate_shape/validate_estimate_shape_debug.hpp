#pragma once

#include "rclcpp/rclcpp.hpp"
#include <mysql_connector.h>
#include <sstream>
#include <memory>
#include "validate_estimate_shape/visibility_control.h"
#include "custom_interfaces/msg/validate_data.hpp"

namespace ros2mysql
{
    class ValidateEstimateShape : public rclcpp::Node
    {
    public:
        COMPOSITION_PUBLIC
        explicit ValidateEstimateShape(const rclcpp::NodeOptions &options);

    private:
        /**;
         * Configures component.
         *
         * Declares parameters and configures video capture.
         */
        COMPOSITION_PUBLIC
        void
        configure();

        /**;
         * Declares the parameter using rcl_interfaces.
         */
        COMPOSITION_PUBLIC
        void
        initialize_parameters();

        std::unique_ptr<MysqlConnector> db_;
        std::string host_;
        std::string port_;
        std::string db_name_;
        std::string username_;
        std::string password_;
        bool debug_;
        rclcpp::Subscription<custom_interfaces::msg::ValidateData>::SharedPtr sub_;
    };
}
