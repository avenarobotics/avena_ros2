#pragma once
#include <geometry_msgs/msg/point.hpp>
#include <geometry_msgs/msg/vector3.hpp>
#include <geometry_msgs/msg/quaternion.hpp>
#include <Eigen/Geometry>

namespace utils
{

    float getDistance(const geometry_msgs::msg::Point &p1, const geometry_msgs::msg::Point &p2);

    float getAngle(const geometry_msgs::msg::Vector3 &v1, const geometry_msgs::msg::Vector3 &v2);

    Eigen::Matrix3f quaternionToMatrix(const geometry_msgs::msg::Quaternion &q1);

    std::vector<geometry_msgs::msg::Vector3> extractVectorAxes(const Eigen::Matrix3f &matrix);

    float angleError(float angle, bool only_direction);

}
