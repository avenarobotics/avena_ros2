#pragma once

#include "rclcpp/rclcpp.hpp"
#include "custom_interfaces/srv/get_items_info.hpp"
#include <memory>
#include <nlohmann/json.hpp>
#include "custom_interfaces/msg/items.hpp"
#include <iomanip>
#include <rclcpp_action/rclcpp_action.hpp>
#include "rclcpp_components/register_node_macro.hpp"
#include "custom_interfaces/action/validate_estimate_shape.hpp"
#include <Eigen/Geometry>
#include "validate_estimate_shape/utils.hpp"
#include "custom_interfaces/msg/validate_data.hpp"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"
#include "validate_estimate_shape/validate_real_scene.hpp"

using namespace std::chrono_literals;

namespace validate_estimate_shape
{

    enum class ItemLabel_e
    {
        ORANGE,
        LIPTON,
        MILK,
        CUCUMBER,
        BROCCOLI,
        ONION,
        BOWL,
        BANANA,
        PLATE,
        CARROT,
        KNIFE,
        SPATULA,
        CUTTING_BOARD
    };

    class ValidateEstimateShapeServer : public rclcpp::Node
    {
    public:
        using ValidateEstimateShape = custom_interfaces::action::ValidateEstimateShape;
        using GoalHandleValidateEstimateShape = rclcpp_action::ServerGoalHandle<ValidateEstimateShape>;

        explicit ValidateEstimateShapeServer(const rclcpp::NodeOptions &options);

    private:
        std::vector<custom_interfaces::msg::CoppeliaItemInfo> _harvestCoppeliaData(std::string coppelia_name);

        float _getDistanceError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);

        nlohmann::json _getOrangeSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getCucumberSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getCarrotSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getMilkSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getLiptonSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getBowlSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getPlateSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getSpatulaSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getKnifeSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);

        nlohmann::json _getCucumberOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getCarrotOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getMilkOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getLiptonOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getPlateOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getBowlOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getSpatulaOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);
        nlohmann::json _getKnifeOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item);


        // std::map<int, std::string> _readLabels();

        ItemLabel_e _getItemLabel(std::string coppelia_name);

        custom_interfaces::msg::CoppeliaItemInfo _getNearestCameraItem(custom_interfaces::msg::Item &estimated_item, float &out_distance);

        void topic_callback(const custom_interfaces::msg::Items::SharedPtr msg);

        rclcpp_action::GoalResponse _handle_goal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const ValidateEstimateShape::Goal> goal);

        rclcpp_action::CancelResponse _handle_cancel(const std::shared_ptr<GoalHandleValidateEstimateShape> goal_handle);

        void _handle_accepted(const std::shared_ptr<GoalHandleValidateEstimateShape> goal_handle);

        void _execute(const std::shared_ptr<GoalHandleValidateEstimateShape> goal_handle);

        void _getAreasParameter();

        rclcpp_action::Server<ValidateEstimateShape>::SharedPtr _action_server;
        rclcpp::Subscription<custom_interfaces::msg::Items>::SharedPtr _subscription;
        // std::map<int, std::string> _labels;
        std::vector<custom_interfaces::msg::CoppeliaItemInfo> _coppelia_camera_items;
        std::map<std::string, ItemLabel_e> _string_to_label_assignment;
        std::vector<custom_interfaces::msg::Item> _estimated_items;
        rclcpp::Publisher<custom_interfaces::msg::ValidateData>::SharedPtr _validate_data_publisher;
        std::map<std::string, nlohmann::json> _real_scene_points; //<point_name, {x,y,z}>

        float max_position_error;
        float max_orientation_error;
        float max_size_error;

        int error_code;
    };
}
