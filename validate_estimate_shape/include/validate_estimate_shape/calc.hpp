#pragma once

#include "nlohmann/json.hpp"
#include "custom_interfaces/msg/coppelia_item_info.hpp"
#include "validate_estimate_shape/utils.hpp"
#include "validate_estimate_shape/data_harvester.hpp"

enum class OrientationsToCheck
{
    X,
    Y,
    Z,
    XY,
    XZ,
    YZ,
    XYZ
};

enum class ItemLabel_e
{
    ORANGE,
    LIPTON,
    MILK,
    CUCUMBER,
    BROCCOLI,
    ONION,
    BOWL,
    BANANA,
    PLATE,
    CARROT,
    KNIFE,
    SPATULA,
    CUTTING_BOARD
};

class ErrorCalculator
{
private:
    float _getDistanceError(custom_interfaces::msg::CoppeliaItemInfo& coppelia_camera_item, custom_interfaces::msg::CoppeliaItemInfo& coppelia_brain_item);
    nlohmann::json _getSizeError(custom_interfaces::msg::CoppeliaItemInfo& coppelia_camera_item, custom_interfaces::msg::CoppeliaItemInfo& coppelia_brain_item);
    nlohmann::json _getOrientationError(custom_interfaces::msg::CoppeliaItemInfo& coppelia_camera_item, custom_interfaces::msg::CoppeliaItemInfo& coppelia_brain_item, OrientationsToCheck orientations);
    ItemLabel_e _getItemLabel(std::string coppelia_name);
    custom_interfaces::msg::CoppeliaItemInfo _getNearestBrainItem(custom_interfaces::msg::CoppeliaItemInfo& camera_item, float& out_distance);

    std::map<std::string, ItemLabel_e> _string_to_label_assignment;
    std::vector<custom_interfaces::msg::CoppeliaItemInfo> _camera_items;
    std::vector<custom_interfaces::msg::CoppeliaItemInfo> _brain_items;
public:
    explicit ErrorCalculator(std::shared_ptr<rclcpp::Node>& node);

    nlohmann::json getOrangeError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getLiptonError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getMilkError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getCucumberError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getBroccoliError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getOnionError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getBowlError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getBananaError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getPlateError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getCarrotError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getKnifeError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getSpatulaError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
    nlohmann::json getCuttingBoardError(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);

    void run();

    void routeCalculationMethod(custom_interfaces::msg::CoppeliaItemInfo& camera_item, custom_interfaces::msg::CoppeliaItemInfo& brain_item);
};