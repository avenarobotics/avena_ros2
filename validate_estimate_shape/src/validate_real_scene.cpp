#include "validate_estimate_shape/validate_real_scene.hpp"

namespace validate_estimate_shape
{
    RealSceneValidator::RealSceneValidator(const std::vector<custom_interfaces::msg::Item> &estiamted_items, const nlohmann::json &real_scene_description, std::map<std::string, nlohmann::json> &points, const std::string &input_json_filename)
        : _estimated_items(estiamted_items)
    {
        _max_position_error = 0;
        _max_orientation_error = 0;
        _max_size_error = 0;
        _error_code = 0;
        for (const auto &item : real_scene_description["items"])
        {
            _real_scene_items.push_back(item);
        }
        _real_scene_points = points;
        _input_json_filename = input_json_filename;
        _raports_to_save.clear();
        _labels_data = helpers::commons::getParameter("labels");
    }

    void RealSceneValidator::setMaxErrorThresholds(float position, float orientation, float size)
    {
        _max_position_error = position;
        _max_orientation_error = orientation;
        _max_size_error = size;
    }

    float RealSceneValidator::_getDistanceError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
        Eigen::Vector3f position(estimated_item.pose.position.x, estimated_item.pose.position.y, estimated_item.pose.position.z);
        nlohmann::json estimated_item_position = helpers::vision::assignJsonFromPosition(position);
        nlohmann::json real_scene_item_position;
        if (real_scene_item.contains("grid_point_id"))
        {
            real_scene_item_position = _real_scene_points[real_scene_item["grid_point_id"]];
        }
        else
        {
            RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "Real scene item is missing or do not contain grid_point_id");
            return std::numeric_limits<float>::quiet_NaN();
        }

        float real_x = real_scene_item_position["x"], real_y = real_scene_item_position["y"], real_z = real_scene_item_position["z"];
        float estimated_x = estimated_item_position["x"], estimated_y = estimated_item_position["y"], estimated_z = estimated_item_position["z"];
        if (FixHeightOffset(estimated_item.label, real_z))
        {
            RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "Using default point height as reference point (Z = 0.0 )");
        }

        return sqrt(
            pow((real_x - estimated_x), 2) +
            pow((real_y - estimated_y), 2) +
            pow((real_z - estimated_z), 2));
    }
    nlohmann::json RealSceneValidator::_nlohGetDistanceError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
        float distance = _getDistanceError(real_scene_item, estimated_item);
        Eigen::Vector3f position(estimated_item.pose.position.x, estimated_item.pose.position.y, estimated_item.pose.position.z);
        nlohmann::json estimated_item_position = helpers::vision::assignJsonFromPosition(position);
        nlohmann::json real_scene_item_position;
        if (real_scene_item.contains("grid_point_id"))
        {
            real_scene_item_position = _real_scene_points[real_scene_item["grid_point_id"]];
            float z_offset;
            if (!FixHeightOffset(estimated_item.label, z_offset))
                real_scene_item_position["z"] = z_offset;
        }
        else
        {
            Eigen::Vector3f invalid_position(std::numeric_limits<float>::quiet_NaN(), std::numeric_limits<float>::quiet_NaN(), std::numeric_limits<float>::quiet_NaN());
            real_scene_item_position = helpers::vision::assignJsonFromPosition(invalid_position);
        }
        nlohmann::json result = {
            {"absolute_error",
             {{"Error", distance}}},
            {"units", "meters"},
            {"gt_position", real_scene_item_position},
            {"estimated_position", estimated_item_position}};

        return result;
    }

    nlohmann::json RealSceneValidator::_findNearestRealSceneItem(const custom_interfaces::msg::Item &estimated_item, float &out_distance)
    {
        float min = std::numeric_limits<float>::max();
        nlohmann::json chosen_real_scene_item;
        float current_distance;

        for (const auto &real_scene_item : _real_scene_items)
        {
            if (estimated_item.label != real_scene_item["label"])
                continue;
            current_distance = _getDistanceError(real_scene_item, estimated_item);
            if (current_distance < min)
            {
                min = current_distance;
                chosen_real_scene_item = real_scene_item;
            }
        }

        out_distance = min;
        return chosen_real_scene_item;
    }

    void RealSceneValidator::validateSingleItem(const custom_interfaces::msg::Item &estimated_item)
    {
        float distance;
        nlohmann::json real_scene_item;
        nlohmann::json size_error, orientation_error, position_error;

        if (estimated_item.label == "orange")
        {
            real_scene_item = _findNearestRealSceneItem(estimated_item, distance);
            size_error = _getOrangeSizeError(real_scene_item, estimated_item);
            position_error = _nlohGetDistanceError(real_scene_item, estimated_item);
            if (!real_scene_item.contains("grid_point_id"))
                real_scene_item["grid_point_id"] = "invalid";
            if (!real_scene_item.contains("data_type"))
                real_scene_item["data_type"] = "missing";
            _raport["orange_" + std::string(real_scene_item["grid_point_id"])] = {
                {"id", estimated_item.id},
                {"label", estimated_item.label},
                {"distance", distance},
                {"size", size_error},
                {"position", position_error}};
            if (_generateItemRaport(estimated_item, real_scene_item, position_error, orientation_error, distance))
            {
                RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), " Error while generating raport" + estimated_item.label + " \n");
            }
        }
        else if (estimated_item.label == "bowl")
        {
            real_scene_item = _findNearestRealSceneItem(estimated_item, distance);
            size_error = _getBowlSizeError(real_scene_item, estimated_item);
            orientation_error = _getBowlOrientationError(real_scene_item, estimated_item);
            position_error = _nlohGetDistanceError(real_scene_item, estimated_item);
            if (!real_scene_item.contains("grid_point_id"))
                real_scene_item["grid_point_id"] = "invalid";
            if (!real_scene_item.contains("data_type"))
                real_scene_item["data_type"] = "missing";
            _raport["bowl_" + std::string(real_scene_item["grid_point_id"])] = {
                {"id", estimated_item.id},
                {"label", estimated_item.label},
                {"point_id", real_scene_item["grid_point_id"]},
                {"distance", distance},
                {"size", size_error},
                {"orientation", orientation_error},
                {"position", position_error}};
            if (_generateItemRaport(estimated_item, real_scene_item, position_error, orientation_error, distance))
            {
                RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), " Error while generating raport " + estimated_item.label + " \n");
            }
        }
        else if (estimated_item.label == "plate")
        {
            real_scene_item = _findNearestRealSceneItem(estimated_item, distance);
            size_error = _getPlateSizeError(real_scene_item, estimated_item);
            orientation_error = _getPlateOrientationError(real_scene_item, estimated_item);
            position_error = _nlohGetDistanceError(real_scene_item, estimated_item);
            if (!real_scene_item.contains("grid_point_id"))
                real_scene_item["grid_point_id"] = "invalid";
            if (!real_scene_item.contains("data_type"))
                real_scene_item["data_type"] = "missing";
            _raport["plate_" + std::string(real_scene_item["grid_point_id"])] = {
                {"id", estimated_item.id},
                {"label", estimated_item.label},
                {"point_id", real_scene_item["grid_point_id"]},
                {"distance", distance},
                {"size", size_error},
                {"orientation", orientation_error},
                {"position", position_error}};
            if (_generateItemRaport(estimated_item, real_scene_item, position_error, orientation_error, distance))
            {
                RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), " Error while generating raport " + estimated_item.label + " \n");
            }
        }

        if (distance > _max_position_error)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), estimated_item.label + " has distance above threshold\n");
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), std::to_string(distance));
            _error_code = EXIT_FAILURE;
        }
    }

    int RealSceneValidator::_generateItemRaport(const custom_interfaces::msg::Item &estimated_item, const nlohmann::json real_scene_item, const nlohmann::json position_error, const nlohmann::json orientation_error, float distance)
    {

        _csv_raport << _input_json_filename << "\t" << real_scene_item["data_type"] << "\t" << estimated_item.label << "\t" << estimated_item.id << "\t" << real_scene_item["grid_point_id"].get<std::string>() << "\t ";
        float degrees_conversion = (180.0 / 3.141592653589793238463);
        float distance_conversion = 1000; // from meters to mm
        if (position_error.contains("estimated_position") && position_error.contains("gt_position"))
        {
            _csv_raport << position_error["estimated_position"]["x"].get<float>() * distance_conversion << "\t" << position_error["estimated_position"]["y"].get<float>() * distance_conversion << "\t" << position_error["estimated_position"]["z"].get<float>() * distance_conversion << "\t\t"
                        << position_error["gt_position"]["x"].get<float>() * distance_conversion << "\t" << position_error["gt_position"]["y"].get<float>() * distance_conversion << "\t" << position_error["gt_position"]["z"].get<float>() * distance_conversion << "\t\t"
                        << (position_error["gt_position"]["x"].get<float>() - position_error["estimated_position"]["x"].get<float>()) * distance_conversion << "\t"
                        << (position_error["gt_position"]["y"].get<float>() - position_error["estimated_position"]["y"].get<float>()) * distance_conversion << "\t"
                        << (position_error["gt_position"]["z"].get<float>() - position_error["estimated_position"]["z"].get<float>()) * distance_conversion << "\t"
                        << distance * distance_conversion << "\t\t";
        }
        else
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Estimated item position or real position is missing \n");
            return 1;
        }
        if (!orientation_error.is_null())
            if (orientation_error.contains("estimated_orientation") && orientation_error.contains("gt_orientation"))
            {
                _csv_raport << orientation_error["estimated_orientation"]["w"] << "\t" << orientation_error["estimated_orientation"]["x"] << "\t" << orientation_error["estimated_orientation"]["y"] << "\t" << orientation_error["estimated_orientation"]["z"] << "\t\t"
                            << orientation_error["gt_orientation"]["w"] << "\t" << orientation_error["gt_orientation"]["x"] << "\t" << orientation_error["gt_orientation"]["y"] << "\t" << orientation_error["gt_orientation"]["z"] << "\t\t"
                            << orientation_error["absolute_error"]["x"].get<float>() * degrees_conversion << "\t" << orientation_error["absolute_error"]["y"].get<float>() * degrees_conversion << "\t" << orientation_error["absolute_error"]["z"].get<float>() * degrees_conversion << "\n";
            }
            else
            {
                RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "One of orientations is missing\n");
                // return 1;
            }
        return 0;
    }

    bool RealSceneValidator::runValidation()
    {
        int item_number = 0;

        if (_estimated_items.size() <= 0)
        {
            _error_code = 1;
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Estiamtion vector is empty\n");
            return false;
        }
        std::cout << "items ammount: " << _estimated_items.size() << std::endl;
        std::for_each(
            _estimated_items.begin(),
            _estimated_items.end(),
            [this, &item_number](const custom_interfaces::msg::Item &estimated_item)
            {
                std::cout << "run validation for item : " << estimated_item.label << " with item id : " << estimated_item.id << std::endl;
                this->validateSingleItem(estimated_item);
            });
        _saveResults({_raport});
        _saveResultsCsv(_csv_raport.str());
        _raport.clear();
        _csv_raport.clear();
        return true;
    }

    int RealSceneValidator::getErrorCode()
    {
        return _error_code;
    }

    int RealSceneValidator::FixHeightOffset(std::string item_label, float &z_offset)
    {
        for (auto label : _labels_data)
        {
            if (label["label"] == item_label)
            {
                if (label.contains("item_description"))
                {
                    if (label["item_description"].contains("z_axis_offset"))
                    {
                        z_offset = label["item_description"]["z_axis_offset"].get<float>();
                        RCLCPP_DEBUG_STREAM(rclcpp::get_logger("rclcpp"), "Fixed height offset for item with label " << item_label.c_str() << " to :" << z_offset);
                        return 0;
                    }
                }
            }
        }
        return 1;
    }

    nlohmann::json RealSceneValidator::_getOrangeSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
        nlohmann::json result;
        nlohmann::json estimated_orange_shape = nlohmann::json::parse(estimated_item.item_elements[0].parts_description[0]);
        float estimated_radius = 0;
        if (estimated_orange_shape.contains("dims"))
            if (estimated_orange_shape["dims"].contains("radius"))
                estimated_radius = estimated_orange_shape["dims"]["radius"].get<float>();
            else
            {
                RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), " Estimated  data does not contain radius ");
            }
        float real_radius = 0;
        if (real_scene_item.contains("size"))
            if (real_scene_item["size"].contains("radius"))
                real_radius = (real_scene_item["size"]["radius"].get<float>()) / 2;
            else
            {
                RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), " Real data does not contain real size radius ");
            }

        float size_error = (estimated_radius - real_radius);
        float percent_of_error = abs(size_error / real_radius * 100);

        result = {
            {"absolute_error", size_error},
            {"percent_of_error", percent_of_error},
            {"units", "meters"}};

        if (abs(size_error) > _max_size_error)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Orange has size error above threshold\n");
            RCLCPP_ERROR_STREAM(rclcpp::get_logger("rclcpp"), result);
            _error_code = 1;
        }
        return result;
    }

    nlohmann::json RealSceneValidator::_getCucumberSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
    }

    nlohmann::json RealSceneValidator::_getCarrotSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
    }

    nlohmann::json RealSceneValidator::_getMilkSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
    }

    nlohmann::json RealSceneValidator::_getLiptonSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
    }

    nlohmann::json RealSceneValidator::_getBowlSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
        return {"fixed_size"};
    }

    nlohmann::json RealSceneValidator::_getPlateSizeError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
        return {"fixed_size"};
    }

    nlohmann::json RealSceneValidator::_getCucumberOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
    }

    nlohmann::json RealSceneValidator::_getCarrotOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
    }

    nlohmann::json RealSceneValidator::_getMilkOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
    }

    nlohmann::json RealSceneValidator::_getLiptonOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
    }

    nlohmann::json RealSceneValidator::_getPlateOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
        custom_interfaces::msg::ItemElement estimated_element = estimated_item.item_elements[0];

        geometry_msgs::msg::Quaternion real_scene_item_quaternion;
        if (real_scene_item.contains("orientation"))
        {
            real_scene_item_quaternion.w = static_cast<float>(real_scene_item["orientation"]["w"]);
            real_scene_item_quaternion.x = static_cast<float>(real_scene_item["orientation"]["x"]);
            real_scene_item_quaternion.y = static_cast<float>(real_scene_item["orientation"]["y"]);
            real_scene_item_quaternion.z = static_cast<float>(real_scene_item["orientation"]["z"]);
        }
        else
        {
            RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "Real scene item is missing or do not contain valid orientation");
            Eigen::Quaternionf data = Eigen::Quaternionf::Identity();
            data.w() = 0;
            nlohmann::json result = {
                {"absolute_error",
                 {{"x", std::numeric_limits<float>::quiet_NaN()}, {"y", std::numeric_limits<float>::quiet_NaN()}, {"z", std::numeric_limits<float>::quiet_NaN()}}},
                {"percent_of_error",
                 {{"x", std::numeric_limits<float>::quiet_NaN()}, {"y", std::numeric_limits<float>::quiet_NaN()}, {"z", std::numeric_limits<float>::quiet_NaN()}}},
                {"units", "radians"},
                {"gt_orientation", helpers::vision::assignJsonFromQuaternion(data)},
                {"estimated_orientation", helpers::vision::assignJsonFromQuaternion(data)}};
            return result;
        }
        // nlohmann::json estimated_orientation = nlohmann::json::parse(estimated_element.orientation);
        Eigen::Quaternionf orient(estimated_item.pose.orientation.w, estimated_item.pose.orientation.x, estimated_item.pose.orientation.y, estimated_item.pose.orientation.z);
        nlohmann::json estimated_orientation = helpers::vision::assignJsonFromQuaternion(orient);

        geometry_msgs::msg::Quaternion estimated_item_quaternion;
        estimated_item_quaternion.w = static_cast<float>(estimated_orientation["w"]);
        estimated_item_quaternion.x = static_cast<float>(estimated_orientation["x"]);
        estimated_item_quaternion.y = static_cast<float>(estimated_orientation["y"]);
        estimated_item_quaternion.z = static_cast<float>(estimated_orientation["z"]);

        Eigen::Matrix3f real_item_matrix = utils::quaternionToMatrix(real_scene_item_quaternion);
        Eigen::Matrix3f estimated_matrix = utils::quaternionToMatrix(estimated_item_quaternion);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(real_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(estimated_matrix);

        float absolute_angle_x = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float absolute_angle_y = utils::getAngle(camera_item_axes[1], brain_item_axes[1]);
        float absolute_angle_z = utils::getAngle(camera_item_axes[2], brain_item_axes[2]);

        float percent_of_error_x = abs(utils::angleError(absolute_angle_x, false) * 100);
        float percent_of_error_y = abs(utils::angleError(absolute_angle_y, false) * 100);
        float percent_of_error_z = abs(utils::angleError(absolute_angle_z, false) * 100);

        Eigen::Quaternionf real_q(real_item_matrix);
        Eigen::Quaternionf estimated_q(estimated_matrix);

        nlohmann::json result = {
            {"absolute_error",
             {{"x", absolute_angle_x}, {"y", absolute_angle_y}, {"z", absolute_angle_z}}},
            {"percent_of_error",
             {{"x", percent_of_error_x}, {"y", percent_of_error_y}, {"z", percent_of_error_z}}},
            {"units", "radians"},
            {"gt_orientation", helpers::vision::assignJsonFromQuaternion(real_q)},
            {"estimated_orientation", helpers::vision::assignJsonFromQuaternion(estimated_q)}};

        if ((percent_of_error_z) > _max_orientation_error)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Bowl has orientation error above threshold\n");
            RCLCPP_ERROR_STREAM(rclcpp::get_logger("rclcpp"), result);
            _error_code = EXIT_FAILURE;
        }

        return result;
    }

    nlohmann::json RealSceneValidator::_getBowlOrientationError(const nlohmann::json &real_scene_item, const custom_interfaces::msg::Item &estimated_item)
    {
        custom_interfaces::msg::ItemElement estimated_element = estimated_item.item_elements[0];

        geometry_msgs::msg::Quaternion real_scene_item_quaternion;
        if (real_scene_item.contains("orientation"))
        {
            real_scene_item_quaternion.w = static_cast<float>(real_scene_item["orientation"]["w"]);
            real_scene_item_quaternion.x = static_cast<float>(real_scene_item["orientation"]["x"]);
            real_scene_item_quaternion.y = static_cast<float>(real_scene_item["orientation"]["y"]);
            real_scene_item_quaternion.z = static_cast<float>(real_scene_item["orientation"]["z"]);
        }
        else
        {
            RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "Real scene item is missing or do not contain valid orientation");
            Eigen::Quaternionf data = Eigen::Quaternionf::Identity();
            data.w() = 0;
            nlohmann::json result = {
                {"absolute_error",
                 {{"x", std::numeric_limits<float>::quiet_NaN()}, {"y", std::numeric_limits<float>::quiet_NaN()}, {"z", std::numeric_limits<float>::quiet_NaN()}}},
                {"percent_of_error",
                 {{"x", std::numeric_limits<float>::quiet_NaN()}, {"y", std::numeric_limits<float>::quiet_NaN()}, {"z", std::numeric_limits<float>::quiet_NaN()}}},
                {"units", "radians"},
                {"gt_orientation", helpers::vision::assignJsonFromQuaternion(data)},
                {"estimated_orientation", helpers::vision::assignJsonFromQuaternion(data)}};
            return result;
        }
        // nlohmann::json estimated_orientation = nlohmann::json::parse(estimated_element.orientation);
        geometry_msgs::msg::Quaternion estimated_item_quaternion = estimated_item.pose.orientation;
        // estimated_item_quaternion.w = static_cast<float>(estimated_orientation["w"]);
        // estimated_item_quaternion.x = static_cast<float>(estimated_orientation["x"]);
        // estimated_item_quaternion.y = static_cast<float>(estimated_orientation["y"]);
        // estimated_item_quaternion.z = static_cast<float>(estimated_orientation["z"]);

        Eigen::Matrix3f real_item_matrix = utils::quaternionToMatrix(real_scene_item_quaternion);
        Eigen::Matrix3f estimated_matrix = utils::quaternionToMatrix(estimated_item_quaternion);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(real_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(estimated_matrix);

        float absolute_angle_x = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float absolute_angle_y = utils::getAngle(camera_item_axes[1], brain_item_axes[1]);
        float absolute_angle_z = utils::getAngle(camera_item_axes[2], brain_item_axes[2]);

        float percent_of_error_x = abs(utils::angleError(absolute_angle_x, false) * 100);
        float percent_of_error_y = abs(utils::angleError(absolute_angle_y, false) * 100);
        float percent_of_error_z = abs(utils::angleError(absolute_angle_z, false) * 100);

        Eigen::Quaternionf real_q(real_item_matrix);
        Eigen::Quaternionf estimated_q(estimated_matrix);

        nlohmann::json result = {
            {"absolute_error",
             {{"x", absolute_angle_x}, {"y", absolute_angle_y}, {"z", absolute_angle_z}}},
            {"percent_of_error",
             {{"x", percent_of_error_x}, {"y", percent_of_error_y}, {"z", percent_of_error_z}}},
            {"units", "radians"},
            {"gt_orientation", helpers::vision::assignJsonFromQuaternion(real_q)},
            {"estimated_orientation", helpers::vision::assignJsonFromQuaternion(estimated_q)}};

        if ((percent_of_error_z) > _max_orientation_error)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Bowl has orientation error above threshold\n");
            RCLCPP_ERROR_STREAM(rclcpp::get_logger("rclcpp"), result);
            _error_code = EXIT_FAILURE;
        }

        return result;
    }

    int RealSceneValidator::_saveResults(std::vector<nlohmann::json> result_vector)

    {
        int index = _input_json_filename.find_last_of("/\\");
        std::string input_filename_ext = _input_json_filename.substr(index + 1);
        std::string input_trace_path = _input_json_filename.substr(0, index);
        size_t sep = input_filename_ext.find_last_of(".");
        std::string input_trace_filename = input_filename_ext.substr(0, sep);
        std::cout << "input_trace_filename:  " << input_trace_filename << std::endl;
        std::cout << "Saving raport to :" << input_trace_path + "/" + input_trace_filename + "_results.json" << std::endl;
        std::ofstream json_results_file(input_trace_path + "/" + input_trace_filename + "_results.json");
        std::cout << "result_vector.size() : " << result_vector.size() << std::endl;
        for (auto result : result_vector)
        {
            json_results_file << result << std::endl;
            std::cout << std::setw(4) << result << std::endl;
        }
        json_results_file.close();
        return 0;
    }

    int RealSceneValidator::_saveResultsCsv(std::string data)

    {
        int index = _input_json_filename.find_last_of("/\\");
        std::string input_filename_ext = _input_json_filename.substr(index + 1);
        std::string input_trace_path = _input_json_filename.substr(0, index);
        size_t sep = input_filename_ext.find_last_of(".");
        std::string input_trace_filename = input_filename_ext.substr(0, sep);

        std::cout << "Saving raport to :" << input_trace_path + "/" + input_trace_filename + "_results.csv" << std::endl;
        std::ofstream item_data(input_trace_path + "/" + input_trace_filename + "_results.csv");
        // item_data.open(input_trace_path + "/" + input_trace_filename + "_results.csv");
        if (item_data.is_open())
        {
            item_data << data << std::endl;
        }
        item_data.close();
        return 0;
    }
} // namespace validate_estimate_shape
