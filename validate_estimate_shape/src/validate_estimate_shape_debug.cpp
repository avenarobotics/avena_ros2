#include "validate_estimate_shape/validate_estimate_shape_debug.hpp"

namespace ros2mysql
{
    ValidateEstimateShape::ValidateEstimateShape(const rclcpp::NodeOptions &options)
        : Node("validate_estimate_shape_debug", options)
    {
        this->initialize_parameters();

        this->configure();

        try
        {
            this->db_ = std::make_unique<MysqlConnector>(this->host_, this->port_, this->db_name_, this->username_, this->password_, this->debug_);
        }
        catch (const network_exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
        }

        auto callback = [this](const typename custom_interfaces::msg::ValidateData::SharedPtr msg) -> void {
            // for (auto &validation_error_ros_str : msg->data)
            // {
            //     json validation_error = json::parse(validation_error_ros_str.data);
            //     std::stringstream ss;
            //     ss << std::endl << std::setw(4) << validation_error << std::endl << "---";
            //     RCLCPP_WARN(get_logger(), ss.str());
            // }

            std::vector<item_element_t> db_rows;
            std::vector<uint32_t> item_elements_ids = this->db_->getRowsId("item_element");

            for (const auto &item_element_id : item_elements_ids)
            {
                item_element_t item_element_data;
                this->db_->getItemElement(&item_element_data, item_element_id);

                for (const auto &item_element_raport : msg->data)
                {
                    nlohmann::json validation_json = nlohmann::json::parse(item_element_raport.data);
                    if (item_element_data.id != validation_json["id"])
                        continue;
                    item_element_data.estimation_error = validation_json;
                }
                this->db_->setItemElement(&item_element_data);
            }
        };

        rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        sub_ = create_subscription<custom_interfaces::msg::ValidateData>("validate_data", latching_qos, callback);
    }

    void ValidateEstimateShape::configure()
    {
        this->get_parameter<std::string>("host", host_);
        this->get_parameter<std::string>("port", port_);
        this->get_parameter<std::string>("db_name", db_name_);
        this->get_parameter<std::string>("username", username_);
        this->get_parameter<std::string>("password", password_);
        this->get_parameter<bool>("debug", debug_);
    }

    void ValidateEstimateShape::initialize_parameters()
    {
        rcl_interfaces::msg::ParameterDescriptor host_descriptor;
        host_descriptor.name = "host";
        host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("host", "", host_descriptor);

        rcl_interfaces::msg::ParameterDescriptor port_descriptor;
        port_descriptor.name = "port";
        port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("port", "", port_descriptor);

        rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
        db_name_descriptor.name = "db_name";
        db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("db_name", "", db_name_descriptor);

        rcl_interfaces::msg::ParameterDescriptor username_descriptor;
        username_descriptor.name = "username";
        username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("username", "", username_descriptor);

        rcl_interfaces::msg::ParameterDescriptor password_descriptor;
        password_descriptor.name = "password";
        password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("password", "", password_descriptor);

        rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
        debug_descriptor.name = "debug";
        debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
        this->declare_parameter("debug", false, debug_descriptor);
    }
}

#include "rclcpp_components/register_node_macro.hpp"

RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::ValidateEstimateShape)