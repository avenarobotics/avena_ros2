#include "validate_estimate_shape/validate_estimate_shape.hpp"

namespace validate_estimate_shape
{

    ValidateEstimateShapeServer::ValidateEstimateShapeServer(const rclcpp::NodeOptions &options)
        : Node("validate_estimate_shape", options)
    {
        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local().reliable();
        _subscription = this->create_subscription<custom_interfaces::msg::Items>(
            "merged_items", qos_settings, std::bind(&ValidateEstimateShapeServer::topic_callback, this, std::placeholders::_1));

        // _labels = _readLabels();

        this->_action_server = rclcpp_action::create_server<ValidateEstimateShape>(
            this,
            "validate_estimate_shape",
            std::bind(&ValidateEstimateShapeServer::_handle_goal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&ValidateEstimateShapeServer::_handle_cancel, this, std::placeholders::_1),
            std::bind(&ValidateEstimateShapeServer::_handle_accepted, this, std::placeholders::_1));

        qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        _validate_data_publisher = this->create_publisher<custom_interfaces::msg::ValidateData>("validate_data", qos_settings);
        error_code = 0;
        _getAreasParameter();
    }

    std::vector<custom_interfaces::msg::CoppeliaItemInfo> ValidateEstimateShapeServer::_harvestCoppeliaData(std::string coppelia_name)
    {
        std::string service_name = "/control_" + coppelia_name + "/get_items_info";

        rclcpp::Client<custom_interfaces::srv::GetItemsInfo>::SharedPtr client =
            shared_from_this()->create_client<custom_interfaces::srv::GetItemsInfo>(service_name);

        auto request = std::make_shared<custom_interfaces::srv::GetItemsInfo::Request>();
        while (!client->wait_for_service(1s))
        {
            if (!rclcpp::ok())
            {
                RCLCPP_ERROR(this->get_logger(), "Interrupted while waiting for the service. Exiting.");
                throw std::runtime_error(std::string("Interrupted while waiting for the service. Exiting."));
            }
            RCLCPP_INFO(this->get_logger(), "service not available, waiting again...");
        }

        auto result = client->async_send_request(request);
        auto future_status = result.wait_for(std::chrono::seconds(5));
        if (future_status != std::future_status::ready)
            throw(std::runtime_error(std::string("error while loading coppelia items")));

        return result.get().get()->coppelia_items;
    }

    float ValidateEstimateShapeServer::_getDistanceError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        Eigen::Vector3f coppelia_camera_item_position(coppelia_camera_item.pose.position.x, coppelia_camera_item.pose.position.y, coppelia_camera_item.pose.position.z);
        Eigen::Vector3f coppelia_brain_item_position(coppelia_brain_item.pose.position.x, coppelia_brain_item.pose.position.y, coppelia_brain_item.pose.position.z);
        return (coppelia_camera_item_position - coppelia_brain_item_position).norm();
    }

    nlohmann::json ValidateEstimateShapeServer::_getOrangeSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        nlohmann::json result;

        nlohmann::json coppelia_brain_primitive_shape = nlohmann::json::parse(coppelia_brain_item.item_elements[0].parts_description[0]);

        float estimated_radius = static_cast<float>(coppelia_brain_primitive_shape["dims"]["radius"]);
        float camera_radius = coppelia_camera_item.size.x / 2;

        float size_error = (estimated_radius - camera_radius);
        float percent_of_error = abs(size_error / camera_radius * 100);

        if (abs(size_error) > max_size_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong size estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        result = {
            {"absolute_error", size_error},
            {"percent_of_error", percent_of_error},
            {"units", "meters"}};

        return result;
    }

    nlohmann::json ValidateEstimateShapeServer::_getCucumberSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        nlohmann::json coppelia_brain_primitive_shape = nlohmann::json::parse(coppelia_brain_item.item_elements[0].parts_description[0]);

        float estimated_radius = static_cast<float>(coppelia_brain_primitive_shape["dims"]["radius"]);
        float camera_radius = coppelia_camera_item.size.x / 2;

        float estimated_height = static_cast<float>(coppelia_brain_primitive_shape["dims"]["height"]);
        float camera_height = coppelia_camera_item.size.z;

        float radius_error = (estimated_radius - camera_radius);
        float percent_of_radius_error = abs(radius_error / camera_radius * 100);

        float height_error = (estimated_height - camera_height);
        float percent_of_height_error = abs(height_error / camera_height * 100);

        if (abs(radius_error) > max_size_error ||
            abs(height_error) > max_size_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong size estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"radius", radius_error},
              {"height", height_error}}},
            {"percent_of_error",
             {{"radius", percent_of_radius_error},
              {"height", percent_of_height_error}}},
            {"units", "meters"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getCarrotSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        nlohmann::json coppelia_brain_primitive_shape = nlohmann::json::parse(coppelia_brain_item.item_elements[0].parts_description[0]);

        float estimated_radius = static_cast<float>(coppelia_brain_primitive_shape["dims"]["radius"]);
        float camera_radius = coppelia_camera_item.size.x / 2;

        float estimated_height = static_cast<float>(coppelia_brain_primitive_shape["dims"]["height"]);
        float camera_height = coppelia_camera_item.size.z;

        float radius_error = (estimated_radius - camera_radius);
        float percent_of_radius_error = abs(radius_error / camera_radius * 100);

        float height_error = (estimated_height - camera_height);
        float percent_of_height_error = abs(height_error / camera_height * 100);

        if (abs(radius_error) > max_size_error ||
            abs(height_error) > max_size_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong size estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"radius", radius_error},
              {"height", height_error}}},
            {"percent_of_error",
             {{"radius", percent_of_radius_error},
              {"height", percent_of_height_error}}},
            {"units", "meters"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getBowlSizeError(custom_interfaces::msg::CoppeliaItemInfo & /*coppelia_camera_item*/, custom_interfaces::msg::Item & /*coppelia_brain_item*/)
    {
        return {
            "fixed size"};
    }

    nlohmann::json ValidateEstimateShapeServer::_getPlateSizeError(custom_interfaces::msg::CoppeliaItemInfo & /*coppelia_camera_item*/, custom_interfaces::msg::Item & /*coppelia_brain_item*/)
    {
        return {
            "fixed size"};
    }

    nlohmann::json ValidateEstimateShapeServer::_getMilkSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        nlohmann::json coppelia_brain_primitive_shape = nlohmann::json::parse(coppelia_brain_item.item_elements[0].parts_description[0]);

        float estimated_x = static_cast<float>(coppelia_brain_primitive_shape["dims"]["x"]);
        float camera_x = coppelia_camera_item.size.x;

        float estimated_y = static_cast<float>(coppelia_brain_primitive_shape["dims"]["y"]);
        float camera_y = coppelia_camera_item.size.y;

        float estimated_z = static_cast<float>(coppelia_brain_primitive_shape["dims"]["z"]);
        float camera_z = coppelia_camera_item.size.z;

        float x_error = estimated_x - camera_x;
        float y_error = estimated_y - camera_y;
        float z_error = estimated_z - camera_z;

        float percent_of_x_error = abs(x_error / camera_x * 100);
        float percent_of_y_error = abs(y_error / camera_y * 100);
        float percent_of_z_error = abs(z_error / camera_z * 100);

        if (abs(x_error) > max_size_error ||
            abs(y_error) > max_size_error ||
            abs(z_error) > max_size_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong size estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"x", x_error},
              {"y", y_error},
              {"z", z_error}}},
            {"percent_of_error",
             {{"x", percent_of_x_error},
              {"y", percent_of_y_error},
              {"z", percent_of_z_error}}},
            {"units", "meters"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getLiptonSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        nlohmann::json coppelia_brain_primitive_shape = nlohmann::json::parse(coppelia_brain_item.item_elements[0].parts_description[0]);

        float estimated_x = static_cast<float>(coppelia_brain_primitive_shape["dims"]["x"]);
        float camera_x = coppelia_camera_item.size.x;

        float estimated_y = static_cast<float>(coppelia_brain_primitive_shape["dims"]["y"]);
        float camera_y = coppelia_camera_item.size.y;

        float estimated_z = static_cast<float>(coppelia_brain_primitive_shape["dims"]["z"]);
        float camera_z = coppelia_camera_item.size.z;

        float x_error = estimated_x - camera_x;
        float y_error = estimated_y - camera_y;
        float z_error = estimated_z - camera_z;

        float percent_of_x_error = abs(x_error / camera_x * 100);
        float percent_of_y_error = abs(y_error / camera_y * 100);
        float percent_of_z_error = abs(z_error / camera_z * 100);

        if (abs(x_error) > max_size_error ||
            abs(y_error) > max_size_error ||
            abs(z_error) > max_size_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong size estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"x", x_error},
              {"y", y_error},
              {"z", z_error}}},
            {"percent_of_error",
             {{"x", percent_of_x_error},
              {"y", percent_of_y_error},
              {"z", percent_of_z_error}}},
            {"units", "meters"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getCucumberOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        geometry_msgs::msg::Pose brain_item_pose = coppelia_brain_item.pose;

        Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
        Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(brain_item_pose.orientation);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

        float absolute_angle = utils::getAngle(camera_item_axes[2], brain_item_axes[2]);
        float percent_of_error = utils::angleError(absolute_angle, true) * 100;

        if ((percent_of_error) > max_orientation_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong orientation estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"z", absolute_angle}}},
            {"percent_of_error",
             {{"z", percent_of_error}

             }},
            {"units", "radians"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getCarrotOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        geometry_msgs::msg::Pose brain_item_pose = coppelia_brain_item.pose;

        Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
        Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(brain_item_pose.orientation);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

        float absolute_angle = utils::getAngle(camera_item_axes[2], brain_item_axes[2]);
        float percent_of_error = abs(utils::angleError(absolute_angle, true) * 100);

        if ((percent_of_error) > max_orientation_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong orientation estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"z", absolute_angle}}},
            {"percent_of_error",
             {{"z", percent_of_error}

             }},
            {"units", "radians"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getBowlOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        geometry_msgs::msg::Pose brain_item_pose = coppelia_brain_item.pose;

        Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
        Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(brain_item_pose.orientation);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

        float absolute_angle = utils::getAngle(camera_item_axes[2], brain_item_axes[2]);
        float percent_of_error = abs(utils::angleError(absolute_angle, false) * 100);

        if ((percent_of_error) > max_orientation_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong orientation estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"z", absolute_angle}}},
            {"percent_of_error",
             {{"z", percent_of_error}

             }},
            {"units", "radians"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getPlateOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        geometry_msgs::msg::Pose brain_item_pose = coppelia_brain_item.pose;

        Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
        Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(brain_item_pose.orientation);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

        float absolute_angle = utils::getAngle(camera_item_axes[2], brain_item_axes[2]);
        float percent_of_error = abs(utils::angleError(absolute_angle, false) * 100);

        if ((percent_of_error) > max_orientation_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong orientation estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"z", absolute_angle}}},
            {"percent_of_error",
             {{"z", percent_of_error}

             }},
            {"units", "radians"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getMilkOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        geometry_msgs::msg::Pose brain_item_pose = coppelia_brain_item.pose;

        Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
        Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(brain_item_pose.orientation);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

        float x_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float x_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);
        float y_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float y_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);
        float z_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float z_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);

        if ((x_percent_of_error) > max_orientation_error || (y_percent_of_error) > max_orientation_error || (z_percent_of_error) > max_orientation_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong orientation estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"x", x_absolute_angle},
              {"y", y_absolute_angle},
              {"z", z_absolute_angle}}},
            {"percent_of_error",
             {{"x", x_percent_of_error},
              {"y", y_percent_of_error},
              {"z", z_percent_of_error}}},
            {"units", "radians"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getLiptonOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        geometry_msgs::msg::Pose brain_item_pose = coppelia_brain_item.pose;

        Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
        Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(brain_item_pose.orientation);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

        float x_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float x_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);
        float y_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float y_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);
        float z_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float z_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);

        if ((x_percent_of_error) > max_orientation_error ||
            (y_percent_of_error) > max_orientation_error ||
            (z_percent_of_error) > max_orientation_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong orientation estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"x", x_absolute_angle},
              {"y", y_absolute_angle},
              {"z", z_absolute_angle}}},
            {"percent_of_error",
             {{"x", x_percent_of_error},
              {"y", y_percent_of_error},
              {"z", z_percent_of_error}}},
            {"units", "radians"}};
    }

    // std::map<int, std::string> ValidateEstimateShapeServer::_readLabels()
    // {
    //     using namespace std::chrono_literals;

    //     std::map<int, std::string> result;

    //     auto cameras_parameters_client = std::make_shared<rclcpp::SyncParametersClient>(this, "parameters_server");
    //     while (!cameras_parameters_client->wait_for_service(1s))
    //     {
    //         if (!rclcpp::ok())
    //         {
    //             RCLCPP_ERROR(this->get_logger(), "Interrupted while waiting for the service. Exiting.");
    //             rclcpp::shutdown();
    //         }
    //         RCLCPP_INFO(this->get_logger(), "service not available, waiting again...");
    //     }

    //     std::vector<std::string> labels_strings = cameras_parameters_client->get_parameter<std::vector<std::string>>("labels");

    //     for (size_t i = 0; i < labels_strings.size(); i++)
    //     {
    //         nlohmann::json label_data = nlohmann::json::parse(labels_strings[i]);

    //         result.insert(std::make_pair(label_data["label_id"], label_data["label"]));
    //     }
    //     return result;
    // }

    ItemLabel_e ValidateEstimateShapeServer::_getItemLabel(std::string coppelia_name)
    {
        // std::cout << "+------------------pre proccessging--------------------+ " << std::endl;
        // std::cout << "Raw name: " << coppelia_name << " " << std::endl;
        auto isalphachar = [](auto const &c) -> bool
        { return !(c >= 'a' && c <= 'z'); };

        std::transform(coppelia_name.begin(), coppelia_name.end(), coppelia_name.begin(),
                       [](unsigned char c)
                       { return std::tolower(c); });

        coppelia_name.erase(std::remove_if(coppelia_name.begin(), coppelia_name.end(),
                                           isalphachar),
                            coppelia_name.end());
        // std::cout << "Proccessed name: " << coppelia_name << " " << std::endl;
        // std::cout << "Value: " <<  static_cast<int>(_string_to_label_assignment[coppelia_name]) << " " << std::endl;
        return _string_to_label_assignment[coppelia_name];
        //throw(std::runtime_error(std::string("label not found")));
    }

    void ValidateEstimateShapeServer::_getAreasParameter()
    {
        try
        {
            while (rclcpp::ok())
            {
                nlohmann::json areas_json = helpers::commons::getParameter("table_points");
                if (!areas_json.empty())
                {
                    for (auto it = areas_json.begin(); it != areas_json.end(); ++it)
                    {
                        std::pair<std::string, nlohmann::json> table_point;

                        table_point.first = it.key();
                        table_point.second["x"] = (*it)["x"].get<float>();
                        table_point.second["y"] = (*it)["y"].get<float>();
                        table_point.second["z"] = (*it)["z"].get<float>();
                        _real_scene_points.insert(table_point);
                    }
                    break;
                }
                else
                {
                    RCLCPP_WARN_THROTTLE(get_logger(), *get_clock(), 1000, "Not able to read table points from parameters server");
                }
            }
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), e.what() + '\n');
            rclcpp::shutdown();
        }
    }

    custom_interfaces::msg::CoppeliaItemInfo ValidateEstimateShapeServer::_getNearestCameraItem(custom_interfaces::msg::Item &estimated_item, float &out_distance)
    {
        float min = std::numeric_limits<float>::max();
        custom_interfaces::msg::CoppeliaItemInfo chosen_camera_item;
        float current_distance;
        ItemLabel_e estimated_item_label = _getItemLabel(estimated_item.label);

        ItemLabel_e camera_item_label;
        for (auto camera_item : _coppelia_camera_items)
        {
            camera_item_label = _getItemLabel(camera_item.label);
            if (estimated_item_label != camera_item_label)
                continue;
            current_distance = _getDistanceError(camera_item, estimated_item);
            if (current_distance < min)
            {
                min = current_distance;
                chosen_camera_item = camera_item;
            }
        }

        out_distance = min;
        return chosen_camera_item;
    }

    void ValidateEstimateShapeServer::topic_callback(const custom_interfaces::msg::Items::SharedPtr msg)
    {
        RCLCPP_INFO(this->get_logger(), "getting items estimations from msg");
        _estimated_items = msg->items;
    }

    rclcpp_action::GoalResponse ValidateEstimateShapeServer::_handle_goal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const ValidateEstimateShape::Goal> goal)
    {
        (void)uuid;
        (void)goal;
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse ValidateEstimateShapeServer::_handle_cancel(const std::shared_ptr<GoalHandleValidateEstimateShape> goal_handle)
    {
        (void)goal_handle;
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void ValidateEstimateShapeServer::_handle_accepted(const std::shared_ptr<GoalHandleValidateEstimateShape> goal_handle)
    {
        (void)goal_handle;
        std::thread{std::bind(&ValidateEstimateShapeServer::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    void ValidateEstimateShapeServer::_execute(const std::shared_ptr<GoalHandleValidateEstimateShape> goal_handle)
    {
        error_code = 0;
        max_position_error = goal_handle->get_goal().get()->position / 1000;
        max_orientation_error = goal_handle->get_goal().get()->orientation;
        max_size_error = goal_handle->get_goal().get()->size / 1000;
        std::string real_scene_description_path = goal_handle->get_goal().get()->real_scene_decscription_path;

        std::stringstream ss;
        ss << "\nExecuting validation with:  " << std::endl;
        ss << "- position: " << max_position_error * 1000 << " [mm] " << std::endl;
        ss << "- orientation: " << max_orientation_error << " [%] " << std::endl;
        ss << "- size: " << max_size_error * 1000 << " [mm] " << std::endl;
        ss << "- scene_description :" << real_scene_description_path;
        RCLCPP_INFO(this->get_logger(), ss.str());

        nlohmann::json real_scene_description;

        auto result = std::make_shared<ValidateEstimateShape::Result>();

        if (real_scene_description_path != "")
        {
            std::ifstream ifs(real_scene_description_path);
            if (!ifs.good())
            {
                RCLCPP_ERROR(this->get_logger(), real_scene_description_path + " doesn't exist!\n");
                goal_handle->abort(result);
                return;
            }
            else
            {
                //validate with data from json
                real_scene_description = nlohmann::json::parse(ifs);
                //TODO: validate json format
                std::cout << "real_scene_description_path" << real_scene_description_path << std::endl;
                RealSceneValidator rsv(_estimated_items, real_scene_description, _real_scene_points, real_scene_description_path);
                rsv.setMaxErrorThresholds(max_position_error, max_orientation_error, max_size_error);
                rsv.runValidation();
                if (rsv.getErrorCode() != 0)
                {
                    RCLCPP_ERROR(this->get_logger(), "Estimation erros occurs\n");
                    goal_handle->abort(result);
                    return;
                }
            }
        }
        else
        {

            _coppelia_camera_items = _harvestCoppeliaData("coppelia_camera");

            if (_estimated_items.size() == 0)
                RCLCPP_ERROR(this->get_logger(), "there is no estimated items in vector");

            nlohmann::json raport;

            custom_interfaces::msg::CoppeliaItemInfo coppelia_camera_item;
            float distance = 0.0;
            nlohmann::json size_error;
            nlohmann::json orientation_error;

            std_msgs::msg::String validation_debug_data;
            custom_interfaces::msg::ValidateData serialized_raport;

            for (auto &item : _estimated_items)
            {
                if (item.label == "orange")
                {
                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getOrangeSizeError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"size", size_error}};
                }
                else if (item.label == "cucumber")
                {
                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getCucumberSizeError(coppelia_camera_item, item);
                    orientation_error = _getCucumberOrientationError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"size", size_error},
                        {"orientation_error", orientation_error}};
                }
                else if (item.label == "carrot")
                {
                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getCarrotSizeError(coppelia_camera_item, item);
                    orientation_error = _getCarrotOrientationError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"size", size_error},
                        {"orientation_error", orientation_error}};
                }
                else if (item.label == "lipton")
                {
                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getLiptonSizeError(coppelia_camera_item, item);
                    orientation_error = _getLiptonOrientationError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"orientation_error", orientation_error},
                        {"size", size_error}};
                }
                else if (item.label == "milk")
                {
                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getMilkSizeError(coppelia_camera_item, item);
                    orientation_error = _getMilkOrientationError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"orientation_error", orientation_error},
                        {"size", size_error}};
                }
                else if (item.label == "bowl")
                {
                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getBowlSizeError(coppelia_camera_item, item);
                    orientation_error = _getBowlOrientationError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"size", size_error},
                        {"orientation", orientation_error}};
                }
                else if (item.label == "plate")
                {
                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getBowlSizeError(coppelia_camera_item, item);
                    orientation_error = _getBowlOrientationError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"size", size_error},
                        {"orientation", orientation_error}};
                }
                else if (item.label == "spatula")
                {
                    /////////////////////////////////////////////////////////////////////////////////////
                    // TODO: Take into account offset between Coppelias camera item center and estimation center
                    constexpr float spatula_center_to_handle_distance = 0.091637; // value read from Coppelia
                    Eigen::Affine3f coppelia_brain_item_pose;
                    helpers::converters::geometryToEigenAffine(item.pose, coppelia_brain_item_pose);
                    Eigen::Vector3f coppelia_brain_item_position(coppelia_brain_item_pose.translation());
                    Eigen::Quaternionf coppelia_brain_item_orientation(coppelia_brain_item_pose.rotation());
                    coppelia_brain_item_orientation.normalize();
                    Eigen::Vector3f z_shift = coppelia_brain_item_orientation.toRotationMatrix().col(2) * spatula_center_to_handle_distance;
                    coppelia_brain_item_position = coppelia_brain_item_position + z_shift;
                    coppelia_brain_item_pose = Eigen::Translation3f(coppelia_brain_item_position) * coppelia_brain_item_orientation;
                    helpers::converters::eigenAffineToGeometry(coppelia_brain_item_pose, item.pose);
                    /////////////////////////////////////////////////////////////////////////////////////

                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getSpatulaSizeError(coppelia_camera_item, item);
                    orientation_error = _getSpatulaOrientationError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"size", size_error},
                        {"orientation", orientation_error}};
                }
                else if (item.label == "knife")
                {
                    /////////////////////////////////////////////////////////////////////////////////////
                    // TODO: Take into account offset between Coppelias camera item center and estimation center
                    constexpr float knife_center_to_handle_distance = 0.10382; // value read from Coppelia
                    Eigen::Affine3f coppelia_brain_item_pose;
                    helpers::converters::geometryToEigenAffine(item.pose, coppelia_brain_item_pose);
                    Eigen::Vector3f coppelia_brain_item_position(coppelia_brain_item_pose.translation());
                    Eigen::Quaternionf coppelia_brain_item_orientation(coppelia_brain_item_pose.rotation());
                    coppelia_brain_item_orientation.normalize();
                    Eigen::Vector3f z_shift = coppelia_brain_item_orientation.toRotationMatrix().col(2) * knife_center_to_handle_distance;
                    coppelia_brain_item_position = coppelia_brain_item_position + z_shift;
                    coppelia_brain_item_pose = Eigen::Translation3f(coppelia_brain_item_position) * coppelia_brain_item_orientation;
                    helpers::converters::eigenAffineToGeometry(coppelia_brain_item_pose, item.pose);
                    /////////////////////////////////////////////////////////////////////////////////////

                    coppelia_camera_item = _getNearestCameraItem(item, distance);
                    size_error = _getKnifeSizeError(coppelia_camera_item, item);
                    orientation_error = _getKnifeOrientationError(coppelia_camera_item, item);

                    raport[coppelia_camera_item.label] = {
                        {"id", item.item_elements[0].id},
                        {"distance", distance},
                        {"size", size_error},
                        {"orientation", orientation_error}};
                }
                else
                {
                    RCLCPP_WARN(this->get_logger(), "Unrecognized label: " + item.label);
                    goal_handle->succeed(result);
                    return;
                }

                if (distance > max_position_error)
                {
                    RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong position estiated by EstimatedID(%d)", coppelia_camera_item.label.c_str(), item.id);
                    error_code = EXIT_FAILURE;
                }

                validation_debug_data.data = raport[coppelia_camera_item.label].dump();

                serialized_raport.data.push_back(validation_debug_data);
            }

            //std::cout << "RAPORT: " << std::setw(4) << raport << std::endl;
            _validate_data_publisher->publish(serialized_raport);
            if (error_code == EXIT_FAILURE)
            {
                RCLCPP_DEBUG(this->get_logger(), "EXIT_FAILURE");
                goal_handle->abort(result);
                return;
            }
            goal_handle->succeed(result);
            return;
        }
    }

    nlohmann::json ValidateEstimateShapeServer::_getSpatulaSizeError(custom_interfaces::msg::CoppeliaItemInfo & /*coppelia_camera_item*/, custom_interfaces::msg::Item & /*coppelia_brain_item*/)
    {
        return {
            "fixed size"};
    }

    nlohmann::json ValidateEstimateShapeServer::_getKnifeSizeError(custom_interfaces::msg::CoppeliaItemInfo & /*coppelia_camera_item*/, custom_interfaces::msg::Item & /*coppelia_brain_item*/)
    {
        return {
            "fixed size"};
    }

    nlohmann::json ValidateEstimateShapeServer::_getSpatulaOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        geometry_msgs::msg::Pose brain_item_pose = coppelia_brain_item.pose;

        Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
        Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(brain_item_pose.orientation);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

        float x_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float x_percent_of_error = abs(utils::angleError(x_absolute_angle, false) * 100);
        float y_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float y_percent_of_error = abs(utils::angleError(x_absolute_angle, false) * 100);
        float z_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float z_percent_of_error = abs(utils::angleError(x_absolute_angle, false) * 100);

        if ((x_percent_of_error) > max_orientation_error || (y_percent_of_error) > max_orientation_error || (z_percent_of_error) > max_orientation_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong orientation estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"x", x_absolute_angle},
              {"y", y_absolute_angle},
              {"z", z_absolute_angle}}},
            {"percent_of_error",
             {{"x", x_percent_of_error},
              {"y", y_percent_of_error},
              {"z", z_percent_of_error}}},
            {"units", "radians"}};
    }

    nlohmann::json ValidateEstimateShapeServer::_getKnifeOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::Item &coppelia_brain_item)
    {
        geometry_msgs::msg::Pose brain_item_pose = coppelia_brain_item.pose;

        Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
        Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(brain_item_pose.orientation);

        std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
        std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

        float x_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float x_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);
        float y_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float y_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);
        float z_absolute_angle = utils::getAngle(camera_item_axes[0], brain_item_axes[0]);
        float z_percent_of_error = abs(utils::angleError(x_absolute_angle, true) * 100);

        if ((x_percent_of_error) > max_orientation_error || (y_percent_of_error) > max_orientation_error || (z_percent_of_error) > max_orientation_error)
        {
            RCLCPP_ERROR(this->get_logger(), "Item with CoppeliaName(%s) has wrong orientation estimated by EsitamtedID(%d)", coppelia_camera_item.label.c_str(), coppelia_brain_item.id);
            error_code = EXIT_FAILURE;
        }

        return {
            {"absolute_error",
             {{"x", x_absolute_angle},
              {"y", y_absolute_angle},
              {"z", z_absolute_angle}}},
            {"percent_of_error",
             {{"x", x_percent_of_error},
              {"y", y_percent_of_error},
              {"z", z_percent_of_error}}},
            {"units", "radians"}};
    }

}
#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(validate_estimate_shape::ValidateEstimateShapeServer)
