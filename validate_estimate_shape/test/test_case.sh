#!/bin/bash

ros2 run virtualscene clear
ros2 run virtualscene create label orange amount 3
ros2 run virtualscene create label cucumber amount 1
ros2 run virtualscene create label carrot amount 1
ros2 run virtualscene create label lipton amount 1
ros2 run virtualscene create label milk amount 1
ros2 run virtualscene create label plate orientation 0,0,0
ros2 run virtualscene create label bowl orientation 0,0,0
sleep 10
ros2 run cli scene_publisher
sleep 2
ros2 run cli detect
sleep 2
ros2 run cli filter_detections
sleep 2
ros2 run cli compose_items
sleep 2
ros2 run cli estimate_shape
sleep 2
ros2 run cli validate_estimate_shape position 10 orientation 0 size 10