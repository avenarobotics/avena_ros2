import rclpy
from rclpy.node import Node

from custom_interfaces.msg import Statistics
import sys
import os
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import functools
import numpy as np
import random as rd
import matplotlib
import random
matplotlib.use("Qt5Agg")
from matplotlib.figure import Figure
from matplotlib.animation import TimedAnimation
from matplotlib.lines import Line2D
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import time
import threading
import psutil
from ros2cli.node.strategy import NodeStrategy
from ros2node.api import get_node_names


class CustomMainWindow(QMainWindow):
    def __init__(self, nodes_to_watch_list, watchfox_mode):
        super(CustomMainWindow, self).__init__()
        # Define the geometry of the main window
        self.node_names_list = nodes_to_watch_list
        self.setGeometry(300, 300, 800, 400)
        self.setWindowTitle("Avena node {} load".format(watchfox_mode))
        self.watchfox_mode = watchfox_mode
        # Create FRAME_A
        self.FRAME_A = QFrame(self)
        self.FRAME_A.setStyleSheet("QWidget { background-color: %s }" % QColor(210, 210, 235, 255).name())
        self.LAYOUT_A = QGridLayout()
        self.FRAME_A.setLayout(self.LAYOUT_A)
        self.setCentralWidget(self.FRAME_A)
        self.FRAME_B = QFrame(self)
        self.LAYOUT_LABLES = QGridLayout()
        self.FRAME_B.setLayout(self.LAYOUT_LABLES)
        self.selected_colors = [(random.uniform(0.1, 0.8), random.uniform(0.1, 0.8), random.uniform(0.1, 0.8)) for i in range(len(self.node_names_list))]

        r = 0
        for node_name in self.node_names_list:
            setattr(self, 'label_' + node_name, QLabel(node_name))
            lbl = getattr(self, 'label_' + node_name)
            color_str = "{}, {}, {}".format(int(self.selected_colors[r][0]*255), int(self.selected_colors[r][1]*255), int(self.selected_colors[r][2]*255))
            lbl.setStyleSheet("background-color: rgb({});".format(color_str))
            self.LAYOUT_LABLES.addWidget(lbl, *(r , 0))
            r+=1
        self.LAYOUT_A.addWidget(self.FRAME_B, *(0, 0))

        # Place the matplotlib figure
        self.myFig = CustomFigCanvas(self.node_names_list, self.selected_colors, self.watchfox_mode)
        self.LAYOUT_A.addWidget(self.myFig, *(0, 1))
        # Add the callbackfunc to ..
        def spin(node):
            rclpy.spin(watchfox)
            watchfox.destroy_node()       
            rclpy.shutdown()
 
        rclpy.init(args=None)
        watchfox = Watchfox(self, self.node_names_list, self.watchfox_mode)
        if self.watchfox_mode not in ['cpu', 'memory', 'time']:
            watchfox.get_logger().error('Wrong watchfox mode selected. Must be cpu, time or memory, you provided: ' + self.watchfox_mode)
            sys.exit(1)
        myDataLoop = threading.Thread(name='spin', target=spin, daemon=True,
                                      args=(watchfox,))
        myDataLoop.start()

        self.show()
        return

    def addData_callbackFunc(self, value, node_name):
        self.myFig.addData(value, node_name)
        return


''' End Class '''


class CustomFigCanvas(FigureCanvas, TimedAnimation):
    def __init__(self, node_name_list, selected_colors_list, mode):
        # self.addedData = []
        self.nodes_names_list = node_name_list
        self.selected_colors_list = selected_colors_list
        self.y_lim = 2000 if mode == 'memory' else 300
        

        for name in self.nodes_names_list:
            setattr(self, name, list())

        print(matplotlib.__version__)
        # The data
        self.xlim = 200
        for name in self.nodes_names_list:
            setattr(self, 'n_' + name, np.linspace(0, self.xlim - 1, self.xlim))

        for name in self.nodes_names_list:
            setattr(self, 'y_' + name, (getattr(self, 'n_'+name) * 0.0) + 50)

        # The window
        self.fig = Figure(figsize=(5, 5), dpi=100)
        self.ax1 = self.fig.add_subplot(111)
        self.ax1.set_xlabel('time')
        self.ax1.set_ylabel('raw data')

        i = 0
        for name in self.nodes_names_list:
            setattr(self, 'line_' + name, Line2D([], [], color=self.selected_colors_list[i]))
            setattr(self, 'line_' + name + '_tail', Line2D([], [], color=self.selected_colors_list[i]))
            setattr(self, 'line_' + name + '_head', Line2D([], [], color=self.selected_colors_list[i], marker='o', markeredgecolor='r'))
            self.ax1.add_line(getattr(self, 'line_' + name))
            self.ax1.add_line(getattr(self, 'line_' + name + '_tail'))
            self.ax1.add_line(getattr(self, 'line_' + name + '_head'))
            i+=1

        self.ax1.set_xlim(0, self.xlim - 1)
        self.ax1.set_ylim(0, self.y_lim)
        FigureCanvas.__init__(self, self.fig)
        TimedAnimation.__init__(self, self.fig, interval=10, blit=True)
        return

    def new_frame_seq(self):
        return iter(range(self.__getattr__('n_' + self.nodes_names_list[0]).size))

    def _init_draw(self):
        lines = []
        line_attr_list = self.__dict__.keys()
        for k in line_attr_list:
            if 'line' in k:
                lines.append(getattr(self, k))

        for l in lines:
            l.set_data([], [])
        return

    def addData(self, value, node_name):
        self.__getattr__(node_name).append(value)
        return

    def zoomIn(self, value):
        bottom = self.ax1.get_ylim()[0]
        top = self.ax1.get_ylim()[1]
        bottom += value
        top -= value
        self.ax1.set_ylim(bottom, top)
        self.draw()
        return

    def _step(self, *args):
        # Extends the _step() method for the TimedAnimation class.
        try:
            TimedAnimation._step(self, *args)
        except Exception as e:
            self.abc += 1
            print(str(self.abc))
            TimedAnimation._stop(self)
            pass
        return

    def __getattr__(self, name):
        try:
            return self.__dict__[name]
        except KeyError:
            msg = "'{0}' object has no attribute '{1}'"
            raise AttributeError(msg.format(type(self).__name__, name))

    def __setattr__(self, name, value):
        try:
            self.__dict__[name] = value
        except KeyError:
            msg = "'{0}' object has no attribute '{1}'"
            raise AttributeError(msg.format(type(self).__name__, name))

    def _draw_frame(self, framedata):
        margin = 2
        data_lists_attr = [self.__getattr__(name) for name in self.nodes_names_list]
        while (all(len(data_list) > 0 for data_list in data_lists_attr)):
            for node_name in self.nodes_names_list:
                y = self.__getattr__('y_'+node_name)
                self.__setattr__('y_'+node_name, np.roll(y, -1))
                self.__getattr__('y_' + node_name)[-1] = self.__getattr__(node_name)[0]
                del ( self.__getattr__(node_name)[0])

        for node_name in self.nodes_names_list:
            n = self.__getattr__('n_' + node_name)
            y = self.__getattr__('y_' + node_name)
            self.__getattr__('line_' + node_name).set_data(n[0: n.size - margin], y[0: n.size - margin])
            self.__getattr__('line_' + node_name + '_tail').set_data(np.append(n[-10:-1 - margin], n[-1 - margin]),
                                                                           np.append(y[-10:-1 - margin], y[-1 - margin]))
            self.__getattr__('line_' + node_name + '_head').set_data(n[-1 - margin], y[-1 - margin])


        self._drawn_artists = [self.__getattr__(k) for k in  self.__dict__.keys() if 'line' in k]
        for l in self._drawn_artists:
            l.set_animated(True)
        return

''' End Class '''


class Communicate(QObject):
    # add new signal for each new node - unfortunately...
    arm_controller_signal = pyqtSignal(float, str)
    arm_controller_signal = pyqtSignal(float, str)
    arm_controller_container_signal = pyqtSignal(float, str)
    change_detect_signal = pyqtSignal(float, str)
    change_detect_debug_signal = pyqtSignal(float, str)
    clear_database_signal = pyqtSignal(float, str)
    compose_items_signal = pyqtSignal(float, str)
    compose_items_container_signal = pyqtSignal(float, str)
    compose_items_debug_signal = pyqtSignal(float, str)
    control_coppelia_camera_signal = pyqtSignal(float, str)
    data_streams_container_signal = pyqtSignal(float, str)
    detect_signal = pyqtSignal(float, str)
    detect_container_signal = pyqtSignal(float, str)
    detect_debug_signal = pyqtSignal(float, str)
    estimate_shape_signal = pyqtSignal(float, str)
    estimate_shape_container_signal = pyqtSignal(float, str)
    estimate_shape_debug_signal = pyqtSignal(float, str)
    filter_detections_signal = pyqtSignal(float, str)
    filter_detections_container_signal = pyqtSignal(float, str)
    get_cameras_data_signal = pyqtSignal(float, str)
    get_occupancy_grid_signal = pyqtSignal(float, str)
    get_occupancy_grid_container_signal = pyqtSignal(float, str)
    get_occupancy_grid_debug_signal = pyqtSignal(float, str)
    grasp_debug_signal = pyqtSignal(float, str)
    gripper_controller_signal = pyqtSignal(float, str)
    gripper_controller_container_signal = pyqtSignal(float, str)
    gripper_state_signal = pyqtSignal(float, str)
    item_select_signal = pyqtSignal(float, str)
    item_select_container_signal = pyqtSignal(float, str)
    item_select_debug_signal = pyqtSignal(float, str)
    manager_prepare_pick_and_place_data_signal = pyqtSignal(float, str)
    manager_prepare_pick_and_place_data_container_signal = pyqtSignal(float, str)
    merge_items_signal = pyqtSignal(float, str)
    merge_items_container_signal = pyqtSignal(float, str)
    octomap_filter_signal = pyqtSignal(float, str)
    octomap_filter_container_signal = pyqtSignal(float, str)
    octomap_filter_debug_signal = pyqtSignal(float, str)
    parameters_server_signal = pyqtSignal(float, str)
    parameters_server_container_signal = pyqtSignal(float, str)
    path_buffer_signal = pyqtSignal(float, str)
    path_buffer_container_signal = pyqtSignal(float, str)
    ptcld_transformer_signal = pyqtSignal(float, str)
    robot_self_filter_signal = pyqtSignal(float, str)
    robot_state_publisher_signal = pyqtSignal(float, str)
    ros2mysql_signal = pyqtSignal(float, str)
    rosout2mysql_signal = pyqtSignal(float, str)
    security_controller_signal = pyqtSignal(float, str)
    security_controller_container_signal = pyqtSignal(float, str)
    security_controller_debug_signal = pyqtSignal(float, str)
    select_new_masks_signal = pyqtSignal(float, str)
    select_new_masks_container_signal = pyqtSignal(float, str)
    sim_coppelia_camera_1_signal = pyqtSignal(float, str)
    sim_coppelia_camera_2_signal = pyqtSignal(float, str)
    sim_ros2_interface_signal = pyqtSignal(float, str)
    validate_estimate_shape_signal = pyqtSignal(float, str)
    validate_estimate_shape_container_signal = pyqtSignal(float, str)
    validate_estimate_shape_debug_signal = pyqtSignal(float, str)


''' End Class '''

class Watchfox(Node):

    def __init__(self, gui_object, node_names_list, mode):
        super().__init__('watchfox')
        self.mode = mode
        self.nodes_names_list = node_names_list
        self.subscription = self.create_subscription(
            Statistics,
            '/system_monitor/statistics',
            self.listener_callback,
            10)
        self.gui_object = gui_object
        self.subscription  # prevent unused variable warning
        self.mySrc = Communicate()

        for node_name in self.nodes_names_list:
            sig = getattr(self.mySrc, node_name + '_signal')
            sig.connect(self.gui_object.addData_callbackFunc)

    def listener_callback(self, msg):
        if any(node_name in msg.module_name for node_name in self.nodes_names_list):
            assert msg.module_name
            signal = getattr(self.mySrc, str(msg.module_name) + '_signal')
            if self.mode == 'cpu':
                signal.emit(msg.cpu_util, str(msg.module_name))  # <- Here you emit a signal!
            elif self.mode == 'memory':
                signal.emit(msg.memory_load, str(msg.module_name))  # <- Here you emit a signal!
            elif self.mode == 'time':
                signal.emit(msg.action_time/1000000, str(msg.module_name))  # <- Here you emit a signal!
            else:
                self.get_logger().error('Wrong watchfox mode selected. Must be cpu, time or memory, you provided: ' + self.mode)
                sys.exit(1)


def main(args=sys.argv):
    running_node_name_list = list()
    with NodeStrategy(None) as node:
        node_names = get_node_names(node=node, include_hidden_nodes=False)
        for nd in node_names:
            running_node_name_list.append(nd.name)
    watchfox_mode = args[1]
    nodes_to_watch_list = args[2:]
    if len(nodes_to_watch_list) == 0:
        print("Please provide a name of at least one ROS2 running node to watch - aborting")
        sys.exit(1)
    for sel_node in nodes_to_watch_list:
        if sel_node not in running_node_name_list:
            print('No ROS2 module named: ', sel_node, ' found - aborting...')
            sys.exit(1)

    
    app = QApplication(sys.argv)
    QApplication.setStyle(QStyleFactory.create('Plastique'))
    myGUI = CustomMainWindow(nodes_to_watch_list, watchfox_mode)
    sys.exit(app.exec_())



if __name__ == '__main__':
    main()