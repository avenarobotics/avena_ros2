#include "get_real_cameras_data/get_real_cameras_data.hpp"

#include <chrono>
#include <iostream>
#include <memory>
#include <utility>

#include "rclcpp/rclcpp.hpp"
#include "custom_interfaces/msg/cameras_data.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "sensor_msgs/msg/camera_info.hpp"
#include "sensor_msgs/point_cloud2_iterator.hpp"

using namespace std::chrono_literals;

namespace get_real_cameras_data
{

// Create a Talker "component" that subclasses the generic rclcpp::Node base class.
// Components get built into shared libraries and as such do not write their own main functions.
// The process using the component's shared library will instantiate the class as a ROS node.
GetRealCamerasData::GetRealCamerasData(const rclcpp::NodeOptions & options)
: Node("get_real_cameras_data", options), count_(0), qos_(rclcpp::QoS(rclcpp::KeepLast(1)).best_effort())
{
    // Create a publisher of "std_mgs/String" messages on the "chatter" topic.
    // pub_ = create_publisher<custom_interfaces::msg::CamerasData>("cameras_data", 10);
    rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
    pub_ = this->create_publisher<custom_interfaces::msg::CamerasData>("cameras_data", latching_qos);

    // Use a timer to schedule periodic message publishing.
    // timer_ = create_wall_timer(1s, std::bind(&GetRealCamerasData::on_timer, this));

    _watchdog = std::make_shared<helpers::Watchdog>(this, "system_monitor");

    if (this->prepareCameras() == NO_ERRORS) {
        this->startCameras();
    }
}

uint8_t GetRealCamerasData::prepareCameras() 
{
    RCLCPP_INFO(this->get_logger(), "GetRealCamerasData::prepareCameras()");
    uint32_t device_count = k4a_device_get_installed_count();

    // - COUNT ----------------------------------------
    if (device_count == 0)
    {
        RCLCPP_ERROR(this->get_logger(), "No K4A devices found");
        return ERROR_NO_K4A_DEVICES_FOUND;
    } else {
        RCLCPP_INFO(this->get_logger(), "Found %d connected devices.", device_count);
    }
    
    this->camera_1_.setId(0);
    this->camera_2_.setId(1);


    k4a_device_configuration_t config_master = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
    config_master.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
    config_master.color_resolution = K4A_COLOR_RESOLUTION_1536P;
    config_master.depth_mode = K4A_DEPTH_MODE_WFOV_UNBINNED;
    config_master.camera_fps = K4A_FRAMES_PER_SECOND_15;
    config_master.synchronized_images_only = true;
    // config_master.depth_delay_off_color_usec
    config_master.wired_sync_mode = K4A_WIRED_SYNC_MODE_MASTER;
    // config_master.subordinate_delay_off_master_usec
    // config_master.disable_streaming_indicator

    k4a_device_configuration_t config_slave = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
    config_slave.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
    config_slave.color_resolution = K4A_COLOR_RESOLUTION_1536P;
    config_slave.depth_mode = K4A_DEPTH_MODE_WFOV_UNBINNED;
    config_slave.camera_fps = K4A_FRAMES_PER_SECOND_15;
    config_slave.synchronized_images_only = true;
    // config_slave.depth_delay_off_color_usec
    config_slave.wired_sync_mode = K4A_WIRED_SYNC_MODE_SUBORDINATE;
    config_slave.subordinate_delay_off_master_usec = 5;
    // config_slave.disable_streaming_indicator

    this->camera_1_.setConfiguration(config_master);
    this->camera_2_.setConfiguration(config_slave);

    switch (this->camera_1_.open()) {
        case NO_ERRORS: 
            RCLCPP_INFO(this->get_logger(), "Device opened propertly.");
            break;
        case ERROR_FAILED_TO_OPEN_DEVICE: 
            RCLCPP_ERROR(this->get_logger(), "Failed to open device.");
            break;
        case ERROR_FAILED_TO_CALIBRATE_DEVICE: 
            RCLCPP_ERROR(this->get_logger(), "Failed to calibrate device.");
            break;
        case ERROR_FAILED_TO_CALCULATE_TRANSFORM: 
            RCLCPP_ERROR(this->get_logger(), "Failed to calculate transform for device.");
            break;
        default:
            RCLCPP_ERROR(this->get_logger(), "Failed to start device. Unknown error.");
            return ERROR_UNKNOWN_ERROR;
    }
    switch (this->camera_2_.open()) {
        case NO_ERRORS: 
            RCLCPP_INFO(this->get_logger(), "Device opened propertly.");
            break;
        case ERROR_FAILED_TO_OPEN_DEVICE: 
            RCLCPP_ERROR(this->get_logger(), "Failed to open device.");
            break;
        case ERROR_FAILED_TO_CALIBRATE_DEVICE: 
            RCLCPP_ERROR(this->get_logger(), "Failed to calibrate device.");
            break;
        case ERROR_FAILED_TO_CALCULATE_TRANSFORM: 
            RCLCPP_ERROR(this->get_logger(), "Failed to calculate transform for device.");
            break;
        default:
            RCLCPP_ERROR(this->get_logger(), "Failed to start device. Unknown error.");
            return ERROR_UNKNOWN_ERROR;
    }



    // // - OPEN ----------------------------------------
    // if (K4A_RESULT_SUCCEEDED != k4a_device_open(0, &this->camera_1))
    // {
    //     RCLCPP_ERROR(this->get_logger(), "Failed to open device camera_1");
    //     if (this->camera_1 != NULL)
    //     {
    //         k4a_device_close(this->camera_1);
    //     }
    //     return ERROR_FAILED_TO_OPEN_DEVICE;
    // } else {
    //     RCLCPP_INFO(this->get_logger(), "Device camera_1 opened propertly.");
    // }

    // if (K4A_RESULT_SUCCEEDED != k4a_device_open(1, &this->camera_2))
    // {
    //     RCLCPP_ERROR(this->get_logger(), "Failed to open device camera_2");
    //     if (this->camera_2 != NULL)
    //     {
    //         k4a_device_close(this->camera_2);
    //     }
    //     return ERROR_FAILED_TO_OPEN_DEVICE;
    // } else {
    //     RCLCPP_INFO(this->get_logger(), "Device camera_2 opened propertly.");
    // }



    // - START ----------------------------------------
    // if (K4A_RESULT_SUCCEEDED != k4a_device_start_cameras(this->camera_1, &config_master))
    // {
    //     RCLCPP_ERROR(this->get_logger(), "Failed to start device camera_1");
    //     if (this->camera_1 != NULL)
    //     {
    //         k4a_device_close(camera_1);
    //     }
    //     return ERROR_FAILED_TO_START_DEVICE;
    // }

    // if (K4A_RESULT_SUCCEEDED != k4a_device_start_cameras(this->camera_2, &config_slave))
    // {
    //     RCLCPP_ERROR(this->get_logger(), "Failed to start device camera_2");
    //     if (this->camera_2 != NULL)
    //     {
    //         k4a_device_close(camera_2);
    //     }
    //     return ERROR_FAILED_TO_START_DEVICE;
    // }

    switch (this->camera_1_.start() ) {
        case NO_ERRORS: 
            RCLCPP_INFO(this->get_logger(), "Device opened propertly.");
            break;
        case ERROR_FAILED_TO_START_DEVICE: 
            RCLCPP_ERROR(this->get_logger(), "Failed to start device.");
            break;            
        default:
            RCLCPP_ERROR(this->get_logger(), "Failed to start device. Unknown error.");
            return ERROR_UNKNOWN_ERROR;

    }
    switch (this->camera_2_.start() ) {
        case NO_ERRORS: 
            RCLCPP_INFO(this->get_logger(), "Device opened propertly.");
            break;
        case ERROR_FAILED_TO_START_DEVICE: 
            RCLCPP_ERROR(this->get_logger(), "Failed to start device.");
            break;            
        default:
            RCLCPP_ERROR(this->get_logger(), "Failed to start device. Unknown error.");
            return ERROR_UNKNOWN_ERROR;

    }

    // if (k4a_device_get_calibration(this->camera_1, config_master.depth_mode, config_master.color_resolution, &this->camera_1_calibration) != K4A_RESULT_SUCCEEDED)
    // {
    //     std::cout << "Failed to get calibration data.\n";
    //     RCLCPP_ERROR(this->get_logger(), "Failed to get calibration data from camera_1");
    //     return ERROR_FAILED_TO_CALIBRATE_DEVICE;
    // }

    // this->camera_1_transformation = k4a_transformation_create(&this->camera_1_calibration);
    // if (this->camera_1_transformation == NULL)
    // {
    //     RCLCPP_ERROR(this->get_logger(), "Failed to create transform data from camera_1");
    //     return ERROR_FAILED_TO_CALCULATE_TRANSFORM;
    // }

    // if (k4a_device_get_calibration(this->camera_2, config_slave.depth_mode, config_slave.color_resolution, &this->camera_2_calibration) != K4A_RESULT_SUCCEEDED)
    // {
    //     RCLCPP_ERROR(this->get_logger(), "Failed to get calibration data from camera_2");
    //     // break;
    //     return ERROR_FAILED_TO_CALIBRATE_DEVICE;
    // }

    // this->camera_2_transformation = k4a_transformation_create(&this->camera_2_calibration);
    // if (this->camera_2_transformation == NULL)
    // {
    //     RCLCPP_ERROR(this->get_logger(), "Failed to create transform data from camera_2");
    //     return ERROR_FAILED_TO_CALCULATE_TRANSFORM;
    // }

    RCLCPP_INFO(this->get_logger(), "Devices started propertly.");

    return NO_ERRORS;

}

// int GetRealCamerasData::getDepthWidth() { return k4a_calibration_.depth_camera_calibration.resolution_width; }

// int GetRealCamerasData::getDepthHeight() { return k4a_calibration_.depth_camera_calibration.resolution_height; }

// int GetRealCamerasData::getColorWidth() { return k4a_calibration_.color_camera_calibration.resolution_width; }

// int GetRealCamerasData::getColorHeight() { return k4a_calibration_.color_camera_calibration.resolution_height; }

// void GetRealCamerasData::getRgbCameraInfo(sensor_msgs::msg::CameraInfo& camera_info)
// {
// //   camera_info.header.frame_id = tf_prefix_ + rgb_camera_frame_;
// //   camera_info.width = getColorWidth();
// //   camera_info.height = getColorHeight();
//   camera_info.distortion_model = sensor_msgs::distortion_models::RATIONAL_POLYNOMIAL;

//   k4a_calibration_intrinsic_parameters_t* parameters = &k4a_calibration_.color_camera_calibration.intrinsics.parameters;

//   // The distortion parameters, size depending on the distortion model.
//   // For "rational_polynomial", the 8 parameters are: (k1, k2, p1, p2, k3, k4, k5, k6).
//   camera_info.D = {parameters->param.k1, parameters->param.k2, parameters->param.p1, parameters->param.p2,
//                    parameters->param.k3, parameters->param.k4, parameters->param.k5, parameters->param.k6};

//   // clang-format off
//   // Intrinsic camera matrix for the raw (distorted) images.
//   //     [fx  0 cx]
//   // K = [ 0 fy cy]
//   //     [ 0  0  1]
//   // Projects 3D points in the camera coordinate frame to 2D pixel
//   // coordinates using the focal lengths (fx, fy) and principal point
//   // (cx, cy).
//   camera_info.K = {parameters->param.fx,  0.0f,                   parameters->param.cx,
//                    0.0f,                  parameters->param.fy,   parameters->param.cy,
//                    0.0f,                  0.0,                    1.0f};

//   // Projection/camera matrix
//   //     [fx'  0  cx' Tx]
//   // P = [ 0  fy' cy' Ty]
//   //     [ 0   0   1   0]
//   // By convention, this matrix specifies the intrinsic (camera) matrix
//   //  of the processed (rectified) image. That is, the left 3x3 portion
//   //  is the normal camera intrinsic matrix for the rectified image.
//   // It projects 3D points in the camera coordinate frame to 2D pixel
//   //  coordinates using the focal lengths (fx', fy') and principal point
//   //  (cx', cy') - these may differ from the values in K.
//   // For monocular cameras, Tx = Ty = 0. Normally, monocular cameras will
//   //  also have R = the identity and P[1:3,1:3] = K.
//   camera_info.P = {parameters->param.fx,  0.0f,                   parameters->param.cx,   0.0f,
//                    0.0f,                  parameters->param.fy,   parameters->param.cy,   0.0f,
//                    0.0f,                  0.0,                    1.0f,                   0.0f};

//   // Rectification matrix (stereo cameras only)
//   // A rotation matrix aligning the camera coordinate system to the ideal
//   // stereo image plane so that epipolar lines in both stereo images are
//   // parallel.
//   camera_info.R = {1.0f, 0.0f, 0.0f,
//                    0.0f, 1.0f, 0.0f,
//                    0.0f, 0.0f, 1.0f};
//   // clang-format on
// }

// void GetRealCamerasData::getDepthCameraInfo(sensor_msgs::msg::CameraInfo& camera_info)
// {
// //   camera_info.header.frame_id = tf_prefix_ + depth_camera_frame_;
// //   camera_info.width = getDepthWidth();
// //   camera_info.height = getDepthHeight();
//   camera_info.distortion_model = sensor_msgs::distortion_models::RATIONAL_POLYNOMIAL;

//   k4a_calibration_intrinsic_parameters_t* parameters = &k4a_calibration_.depth_camera_calibration.intrinsics.parameters;

//   // The distortion parameters, size depending on the distortion model.
//   // For "rational_polynomial", the 8 parameters are: (k1, k2, p1, p2, k3, k4, k5, k6).
//   camera_info.D = {parameters->param.k1, parameters->param.k2, parameters->param.p1, parameters->param.p2,
//                    parameters->param.k3, parameters->param.k4, parameters->param.k5, parameters->param.k6};

//   // clang-format off
//   // Intrinsic camera matrix for the raw (distorted) images.
//   //     [fx  0 cx]
//   // K = [ 0 fy cy]
//   //     [ 0  0  1]
//   // Projects 3D points in the camera coordinate frame to 2D pixel
//   // coordinates using the focal lengths (fx, fy) and principal point
//   // (cx, cy).
//   camera_info.K = {parameters->param.fx,  0.0f,                   parameters->param.cx,
//                    0.0f,                  parameters->param.fy,   parameters->param.cy,
//                    0.0f,                  0.0,                    1.0f};

//   // Projection/camera matrix
//   //     [fx'  0  cx' Tx]
//   // P = [ 0  fy' cy' Ty]
//   //     [ 0   0   1   0]
//   // By convention, this matrix specifies the intrinsic (camera) matrix
//   //  of the processed (rectified) image. That is, the left 3x3 portion
//   //  is the normal camera intrinsic matrix for the rectified image.
//   // It projects 3D points in the camera coordinate frame to 2D pixel
//   //  coordinates using the focal lengths (fx', fy') and principal point
//   //  (cx', cy') - these may differ from the values in K.
//   // For monocular cameras, Tx = Ty = 0. Normally, monocular cameras will
//   //  also have R = the identity and P[1:3,1:3] = K.
//   camera_info.P = {parameters->param.fx,  0.0f,                   parameters->param.cx,   0.0f,
//                    0.0f,                  parameters->param.fy,   parameters->param.cy,   0.0f,
//                    0.0f,                  0.0,                    1.0f,                   0.0f};

//   // Rectification matrix (stereo cameras only)
//   // A rotation matrix aligning the camera coordinate system to the ideal
//   // stereo image plane so that epipolar lines in both stereo images are
//   // parallel.
//   camera_info.R = {1.0f, 0.0f, 0.0f,
//                    0.0f, 1.0f, 0.0f,
//                    0.0f, 0.0f, 1.0f};
//   // clang-format on
// }

uint8_t GetRealCamerasData::captureImagesFromCamera(k4a_image_t & image_rgb, k4a_image_t & image_depth, const k4a_transformation_t & transformation, 
                                                 sensor_msgs::msg::Image & output_rgb, sensor_msgs::msg::Image & output_depth, sensor_msgs::msg::PointCloud2 & output_point_cloud) {
    // if (camera_1_image_rgb && camera_1_image_depth)
    // {
    size_t rgb_width = k4a_image_get_width_pixels(image_rgb);;
    size_t rgb_height = k4a_image_get_height_pixels(image_rgb);

    size_t depth_width = k4a_image_get_width_pixels(image_depth);;
    size_t depth_height = k4a_image_get_height_pixels(image_depth);

    cv::Mat rgba(rgb_height, rgb_width, CV_8UC4, k4a_image_get_buffer(image_rgb));
    cv::Mat rgb;
    cv::cvtColor(rgba, rgb, cv::COLOR_BGRA2BGR);

    k4a_image_t image_depth_transformed = NULL;
    k4a_image_create(K4A_IMAGE_FORMAT_DEPTH16,
                    rgb_width,
                    rgb_height,
                    rgb_width * sizeof(uint16_t),
                    &image_depth_transformed);

        k4a_transformation_depth_image_to_color_camera(transformation, image_depth, image_depth_transformed);

        cv::Mat depth_16bit(rgb_height, rgb_width, CV_16UC1, k4a_image_get_buffer(image_depth_transformed));
        cv::Mat depth_32bit(rgb_height, rgb_width, CV_32FC1);

        for (int r = 0; r < depth_16bit.rows; ++r)
            for (int c = 0; c < depth_16bit.cols; ++c)
                depth_32bit.at<float>(r, c) = static_cast<float>(depth_16bit.at<uint16_t>(r, c)) / 1000.0f;

        k4a_image_t image_pcl = NULL;
        k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM,
                        depth_width,
                        depth_height,
                        depth_width * 6,
                        &image_pcl);

        k4a_transformation_depth_image_to_point_cloud(transformation,
                image_depth,
                K4A_CALIBRATION_TYPE_DEPTH,
                image_pcl
        );

        const int16_t* point_cloud_buffer = reinterpret_cast<const int16_t*>(k4a_image_get_buffer(image_pcl));
        const size_t point_count = k4a_image_get_height_pixels(image_pcl) * k4a_image_get_width_pixels(image_pcl);
        // sensor_msgs::msg::PointCloud2 point_cloud;

        output_point_cloud.height = k4a_image_get_height_pixels(image_pcl);
        output_point_cloud.width = k4a_image_get_width_pixels(image_pcl);
        output_point_cloud.is_dense = false;
        output_point_cloud.is_bigendian = false;

        sensor_msgs::PointCloud2Modifier pcd_modifier(output_point_cloud);
        pcd_modifier.setPointCloud2FieldsByString(1, "xyz");

        sensor_msgs::PointCloud2Iterator<float> iter_x(output_point_cloud, "x");
        sensor_msgs::PointCloud2Iterator<float> iter_y(output_point_cloud, "y");
        sensor_msgs::PointCloud2Iterator<float> iter_z(output_point_cloud, "z");

        for (size_t i = 0; i < point_count; i++, ++iter_x, ++iter_y, ++iter_z)
        {
            float z = static_cast<float>(point_cloud_buffer[3 * i + 2]);

            if (z <= 0.0f)
            {
            *iter_x = *iter_y = *iter_z = std::numeric_limits<float>::quiet_NaN();
            }
            else
            {
            constexpr float kMillimeterToMeter = 1.0 / 1000.0f;
            *iter_x = kMillimeterToMeter * static_cast<float>(point_cloud_buffer[3 * i + 0]);
            *iter_y = kMillimeterToMeter * static_cast<float>(point_cloud_buffer[3 * i + 1]);
            *iter_z = kMillimeterToMeter * z;
            }
        }

        helpers::converters::cvMatToRos(rgb, output_rgb);
        helpers::converters::cvMatToRos(depth_32bit, output_depth);

        k4a_image_release(image_pcl);
        k4a_image_release(image_depth_transformed);
        k4a_image_release(image_depth);
        k4a_image_release(image_rgb);

    return NO_ERRORS;
}

uint8_t GetRealCamerasData::startCameras()
{
    RCLCPP_INFO(this->get_logger(), "GetRealCamerasData::startCameras()");

    k4a_capture_t capture_camera_1 = NULL;
    k4a_capture_t capture_camera_2 = NULL;
    const int32_t TIMEOUT_IN_MS = K4A_WAIT_INFINITE;

    while (true) {
        bool done_camera_1 = false;
        bool done_camera_2 = false;

        // RCLCPP_INFO(this->get_logger(), "while 1.");

        auto msg = std::make_unique<custom_interfaces::msg::CamerasData>();

        while (!done_camera_1 | !done_camera_2)
        {
            // RCLCPP_INFO(this->get_logger(), "while 2.");
            done_camera_1 = false;
            done_camera_2 = false;

            k4a_image_t camera_1_image_rgb;
            k4a_image_t camera_1_image_depth;
            // k4a_image_t camera_1_image_pcl;
            k4a_image_t camera_2_image_rgb;
            k4a_image_t camera_2_image_depth;
            // k4a_image_t camera_2_image_pcl;

            // Capture camera_1
            switch (k4a_device_get_capture(this->camera_1_.getDevice(), &capture_camera_1, TIMEOUT_IN_MS))
            {
                case K4A_WAIT_RESULT_SUCCEEDED:
                    done_camera_1 = true;
                    break;
                case K4A_WAIT_RESULT_TIMEOUT:
                    std::cout << "CAMERA_1: Timed out waiting for a capture. Reading one more time..." << std::endl;
                    continue;
                    break;
                case K4A_WAIT_RESULT_FAILED:
                    std::cout << "CAMERA_1: Failed to read a capture\n";
                    if (camera_1 != NULL)
                    {
                        k4a_device_close(camera_1);
                    }
                    return ERROR_K4A_WAIT_RESULT_FAILED;
            }

            // Capture camera_2
            switch (k4a_device_get_capture(this->camera_2_.getDevice(), &capture_camera_2, TIMEOUT_IN_MS))
            {
                case K4A_WAIT_RESULT_SUCCEEDED:
                    done_camera_2 = true;
                    break;
                case K4A_WAIT_RESULT_TIMEOUT:
                    std::cout << "CAMERA_2: Timed out waiting for a capture. Reading one more time..." << std::endl;
                    continue;
                    break;
                case K4A_WAIT_RESULT_FAILED:
                    std::cout << "CAMERA_2: Failed to read a capture\n";
                    if (camera_2 != NULL)
                    {
                        k4a_device_close(camera_2);
                    }
                    return ERROR_K4A_WAIT_RESULT_FAILED;
            }
            // RCLCPP_INFO(this->get_logger(), "Cameras captured data propertly.");

            // Probe for a color image
            camera_1_image_rgb = k4a_capture_get_color_image(capture_camera_1);
            // probe for a depth image
            camera_1_image_depth = k4a_capture_get_depth_image(capture_camera_1);

            // Probe for a color image
            camera_2_image_rgb = k4a_capture_get_color_image(capture_camera_2);
            // probe for a depth image
            camera_2_image_depth = k4a_capture_get_depth_image(capture_camera_2);

            if (camera_1_image_rgb && camera_1_image_depth)
            {
                if (this->captureImagesFromCamera(camera_1_image_rgb, camera_1_image_depth, this->camera_1_.getTransformation(), msg->cam1_rgb, msg->cam1_depth, msg->cam1_ptcld) == NO_ERRORS) done_camera_1 = true;
            }

            if (camera_2_image_rgb && camera_2_image_depth)
            {
                if (this->captureImagesFromCamera(camera_2_image_rgb, camera_2_image_depth, this->camera_2_.getTransformation(), msg->cam2_rgb, msg->cam2_depth, msg->cam2_ptcld) == NO_ERRORS) done_camera_2 = true;
            }

            // release capture
            k4a_capture_release(capture_camera_1);
            k4a_capture_release(capture_camera_2);

        }
        // Publish
        pub_->publish(std::move(msg));
        // std::cout << "ping!" << std::endl;

    }

    return NO_ERRORS;
}


}  // namespace get_real_cameras_data

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(get_real_cameras_data::GetRealCamerasData)
