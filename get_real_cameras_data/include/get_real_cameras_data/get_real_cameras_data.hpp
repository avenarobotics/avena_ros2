#ifndef GET_REAL_CAMERAS_DATA__COMPONENT_HPP_
#define GET_REAL_CAMERAS_DATA__COMPONENT_HPP_

#include "get_real_cameras_data/visibility_control.h"

#include "rclcpp/rclcpp.hpp"
// #include <mysql_connector_structs.h>
#include <opencv2/opencv.hpp>
#include <k4a/k4a.h>
#include <helpers.hpp>
#include "helpers/watchdog.hpp"

#include "custom_interfaces/msg/cameras_data.hpp"
#include "sensor_msgs/msg/camera_info.hpp"

namespace get_real_cameras_data
{

#define NO_ERRORS 0x00
#define ERROR_NO_K4A_DEVICES_FOUND 0x01
#define ERROR_FAILED_TO_OPEN_DEVICE 0x02
#define ERROR_FAILED_TO_START_DEVICE 0x03
#define ERROR_FAILED_TO_CALIBRATE_DEVICE 0x04
#define ERROR_FAILED_TO_CALCULATE_TRANSFORM 0x05
#define ERROR_K4A_WAIT_RESULT_FAILED 0x06

#define ERROR_UNKNOWN_ERROR 0xFF

class Camera {
  private:
    uint8_t _id;
    k4a_device_t _camera;
    k4a_device_configuration_t _configuration;
    k4a_calibration_t _calibration;
    k4a_transformation_t _transformation;

  public:
    void setId(uint8_t id) { this->_id = id; }

    uint8_t open() {
      if (K4A_RESULT_SUCCEEDED != k4a_device_open( this->_id, &this->_camera)) 
      {
        // RCLCPP_ERROR(this->get_logger(), "Failed to open device camera_1");
        if (this->_camera != NULL)
        {
            close();
        }
        return ERROR_FAILED_TO_OPEN_DEVICE;
      } else {
          // RCLCPP_INFO(this->get_logger(), "Device camera_1 opened propertly.");
      }

      if (k4a_device_get_calibration(this->_camera, this->_configuration.depth_mode, this->_configuration.color_resolution, &this->_calibration) != K4A_RESULT_SUCCEEDED)
      {
          // std::cout << "Failed to get calibration data.\n";
          // RCLCPP_ERROR(this->get_logger(), "Failed to get calibration data from camera_1");
          return ERROR_FAILED_TO_CALIBRATE_DEVICE;
      }

      this->_transformation = k4a_transformation_create(&this->_calibration);
      if (this->_transformation == NULL)
      {
          // RCLCPP_ERROR(this->get_logger(), "Failed to create transform data from camera_1");
          return ERROR_FAILED_TO_CALCULATE_TRANSFORM;
      }

      return NO_ERRORS;
    }

    uint8_t start() {

      if (K4A_RESULT_SUCCEEDED != k4a_device_start_cameras(this->_camera, &this->_configuration))
      {
          // RCLCPP_ERROR(this->get_logger(), "Failed to start device camera_1");
          if (this->_camera != NULL)
          {
              close();
          }
          return ERROR_FAILED_TO_START_DEVICE;
      }
      return NO_ERRORS;
    }

    void close() { k4a_device_close(this->_camera); }


    void setConfiguration(k4a_device_configuration_t & configuration) {
      this->_configuration = configuration;
    }

    k4a_device_t getDevice() { return this->_camera; }
    k4a_calibration_t getCalibration() { return this->_calibration; }
    k4a_transformation_t getTransformation() { return this->_transformation; }
};


class GetRealCamerasData : public rclcpp::Node
{
public:
  COMPOSITION_PUBLIC
  explicit GetRealCamerasData(const rclcpp::NodeOptions & options);

protected:
  void on_timer();

private:
  helpers::Watchdog::SharedPtr _watchdog;

  size_t count_;

  Camera camera_1_;
  Camera camera_2_;

  k4a_device_t camera_1;
  k4a_device_t camera_2;
  k4a_calibration_t camera_1_calibration;
  k4a_calibration_t camera_2_calibration;
  k4a_transformation_t camera_1_transformation;
  k4a_transformation_t camera_2_transformation;

  rclcpp::Publisher<custom_interfaces::msg::CamerasData>::SharedPtr pub_;
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::QoS qos_;
  uint8_t captureImagesFromCamera(k4a_image_t & image_rgb, k4a_image_t & image_depth, const k4a_transformation_t & transformation, sensor_msgs::msg::Image & output_rgb, sensor_msgs::msg::Image & output_depth, sensor_msgs::msg::PointCloud2 & output_point_cloud);
  // void getRgbCameraInfo(sensor_msgs::msg::CameraInfo& camera_info);
  // void getDepthCameraInfo(sensor_msgs::msg::CameraInfo& camera_info);
  void getSerialNumber(k4a_device_t device);
  uint8_t startCameras();
  uint8_t prepareCameras();

};

}  // namespace composition

#endif  // GET_REAL_CAMERAS_DATA__COMPONENT_HPP_
