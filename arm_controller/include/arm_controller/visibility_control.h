#ifndef ARM_CONTROLLER__VISIBILITY_CONTROL_H_
#define ARM_CONTROLLER__VISIBILITY_CONTROL_H_

#ifdef __cplusplus
extern "C"
{
#endif

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define ARM_CONTROLLER_EXPORT __attribute__ ((dllexport))
    #define ARM_CONTROLLER_IMPORT __attribute__ ((dllimport))
  #else
    #define ARM_CONTROLLER_EXPORT __declspec(dllexport)
    #define ARM_CONTROLLER_IMPORT __declspec(dllimport)
  #endif
  #ifdef ARM_CONTROLLER_BUILDING_DLL
    #define ARM_CONTROLLER_PUBLIC ARM_CONTROLLER_EXPORT
  #else
    #define ARM_CONTROLLER_PUBLIC ARM_CONTROLLER_IMPORT
  #endif
  #define ARM_CONTROLLER_PUBLIC_TYPE ARM_CONTROLLER_PUBLIC
  #define ARM_CONTROLLER_LOCAL
#else
  #define ARM_CONTROLLER_EXPORT __attribute__ ((visibility("default")))
  #define ARM_CONTROLLER_IMPORT
  #if __GNUC__ >= 4
    #define ARM_CONTROLLER_PUBLIC __attribute__ ((visibility("default")))
    #define ARM_CONTROLLER_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define ARM_CONTROLLER_PUBLIC
    #define ARM_CONTROLLER_LOCAL
  #endif
  #define ARM_CONTROLLER_PUBLIC_TYPE
#endif

#ifdef __cplusplus
}
#endif

#endif  // ARM_CONTROLLER__VISIBILITY_CONTROL_H_
