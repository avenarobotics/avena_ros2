// ___CPP___
#include <thread>
#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include <rclcpp/rclcpp.hpp>
#include <rclcpp/time.hpp>
#include <rclcpp_action/rclcpp_action.hpp>

#include <sensor_msgs/msg/joint_state.hpp>
#include <trajectory_msgs/msg/joint_trajectory.hpp>
#include <trajectory_msgs/msg/joint_trajectory_point.hpp>

#include <custom_interfaces/action/simple_action.hpp>
#include "rclcpp_components/register_node_macro.hpp"
#include "visibility_control.h"
#include "helpers_commons/watchdog.hpp"

namespace arm_controller
{
  class ArmController : public rclcpp::Node, public helpers::WatchdogInterface
  {

  public:
    ARM_CONTROLLER_PUBLIC
    explicit ArmController(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    using ExecuteMove = custom_interfaces::action::SimpleAction;
    using GoalHandleExecuteMove = rclcpp_action::ServerGoalHandle<ExecuteMove>;

  void initNode() override;
  void shutDownNode() override;

  private:
    helpers::Watchdog::SharedPtr _watchdog;
    // action server
    rclcpp_action::Server<ExecuteMove>::SharedPtr _action_server;
    ARM_CONTROLLER_LOCAL
    rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const ExecuteMove::Goal> goal);
    ARM_CONTROLLER_LOCAL
    rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandleExecuteMove> goal_handle);
    ARM_CONTROLLER_LOCAL
    void _handleAccepted(const std::shared_ptr<GoalHandleExecuteMove> goal_handle);
    ARM_CONTROLLER_LOCAL
    void _execute(const std::shared_ptr<GoalHandleExecuteMove> goal_handle);

    // publisher and subscribers
    rclcpp::Subscription<trajectory_msgs::msg::JointTrajectory>::SharedPtr _sub_path;
    rclcpp::Publisher<trajectory_msgs::msg::JointTrajectoryPoint>::SharedPtr _pub_trajectory_point;
    trajectory_msgs::msg::JointTrajectory::SharedPtr _path;

    ARM_CONTROLLER_LOCAL
    bool _executeMove();
  };
}
