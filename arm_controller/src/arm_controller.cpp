#include "arm_controller/arm_controller.h"
namespace arm_controller
{
    ArmController::ArmController(const rclcpp::NodeOptions &options) : Node("arm_action_control", options)
    {
        RCLCPP_INFO(get_logger(), "Initialization of ros2 arm controller.");
        rclcpp::QoS qos_latching = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();

        // get path (trajectory with no velocities and acceleration)
        auto getPathCallback = [this](const trajectory_msgs::msg::JointTrajectory::SharedPtr path_msg) -> void {
            _path = path_msg;
        };

        _sub_path = this->create_subscription<trajectory_msgs::msg::JointTrajectory>("generated_path", qos_latching, getPathCallback);
        _pub_trajectory_point = this->create_publisher<trajectory_msgs::msg::JointTrajectoryPoint>("trajectory_point", qos_latching);
        _action_server = rclcpp_action::create_server<custom_interfaces::action::SimpleAction>(this, "execute_move",
                                                                                               std::bind(&ArmController::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
                                                                                               std::bind(&ArmController::_handleCancel, this, std::placeholders::_1),
                                                                                               std::bind(&ArmController::_handleAccepted, this, std::placeholders::_1));

        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");

        RCLCPP_INFO(get_logger(), "Arm action controller loaded. Waiting for commands..");

        status=custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void ArmController::initNode(){
        status=custom_interfaces::msg::Heartbeat::STARTING;
        status=custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void ArmController::shutDownNode(){
        status=custom_interfaces::msg::Heartbeat::STOPPING;
        status=custom_interfaces::msg::Heartbeat::STOPPED;
    }

    rclcpp_action::GoalResponse ArmController::_handleGoal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const ExecuteMove::Goal> goal)
    {
        (void)uuid;
        (void)goal;
        // RCLCPP_INFO(this->get_logger(), "Goal accepted");

        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse ArmController::_handleCancel(const std::shared_ptr<GoalHandleExecuteMove> goal_handle)
    {
        (void)goal_handle;
        RCLCPP_INFO(this->get_logger(), "Goal canceled");

        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void ArmController::_handleAccepted(const std::shared_ptr<GoalHandleExecuteMove> goal_handle)
    {
        RCLCPP_INFO(this->get_logger(), "Goal accepted");
        _execute(goal_handle);

        //std::thread{std::bind(&ArmController::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    void ArmController::_execute(const std::shared_ptr<GoalHandleExecuteMove> goal_handle)
    {
        auto feedback = std::make_shared<ExecuteMove::Feedback>();
        auto result = std::make_shared<ExecuteMove::Result>();
        
        if(status!=custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
            goal_handle->abort(result);
            return;
        }

        RCLCPP_INFO(this->get_logger(), "Executing goal");
        // checking if subcribed msg is empty
        if (!_path)
        {
            RCLCPP_WARN(get_logger(), "Module has not received data yet.");
            _pub_trajectory_point->publish(trajectory_msgs::msg::JointTrajectoryPoint());
            goal_handle->abort(result);
            return;
        }
        if (_path->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_ERROR(this->get_logger(), "Invalid input message. generated_path message is empty, Goal failed.");
            _pub_trajectory_point->publish(trajectory_msgs::msg::JointTrajectoryPoint());
            goal_handle->abort(result);
            return;
        }

        if (!_executeMove())
        {
            RCLCPP_ERROR(this->get_logger(), "Error occured while executing move. Goal failed.");
            goal_handle->abort(result);
            return;
        }

        if (rclcpp::ok())
        {
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
        }

        RCLCPP_INFO(this->get_logger(), "finished");
    }

    bool ArmController::_executeMove()
    {
        using namespace std::chrono_literals;

        trajectory_msgs::msg::JointTrajectoryPoint::SharedPtr trajectory_point_msg(new trajectory_msgs::msg::JointTrajectoryPoint);
        auto move_speed = 15ms;
        int counter = 0;
        int traj_size = _path->points.size();

        // future feature here
        // do trajectory from path
        // return false if failure

        for (auto point : _path->points)
        {
            // slowing down at the begginig
            if (counter < traj_size * 0.2)
                move_speed = 50ms;
            else if (counter < traj_size * 0.4)
                move_speed = 30ms;
            else if (counter < traj_size * 0.6)
                move_speed = 15ms;

            // slowing down at the end of path
            if (counter > traj_size * 0.6)
                move_speed = 30ms;
            if (counter > traj_size * 0.8)
                move_speed = 50ms;

            trajectory_point_msg->positions = point.positions;
            trajectory_point_msg->time_from_start = rclcpp::Duration(move_speed * counter);
            _pub_trajectory_point->publish(*trajectory_point_msg);

            std::this_thread::sleep_for(move_speed);
            counter++;
        }
        _pub_trajectory_point->publish(*trajectory_point_msg);
        std::this_thread::sleep_for(20ms);
        _pub_trajectory_point->publish(*trajectory_point_msg);

        // after move publish empty message
        //  trajectory_msgs::msg::JointTrajectoryPoint::SharedPtr empty_trajectory_point_msg(new trajectory_msgs::msg::JointTrajectoryPoint);
        //  _pub_trajectory_point->publish(*empty_trajectory_point_msg);

        return true;
    }
}

RCLCPP_COMPONENTS_REGISTER_NODE(arm_controller::ArmController)
