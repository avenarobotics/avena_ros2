// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef GRASP_DEBUG__COMPONENT_HPP_
#define GRASP_DEBUG__COMPONENT_HPP_

#include "grasp_debug/visibility_control.h"
#include "rclcpp/rclcpp.hpp"
#include "custom_interfaces/msg/grasp.hpp"
#include <mysql_connector.h>
#include <assert.h>
#include <sstream>
#include <memory>

namespace ros2mysql
{

  class Grasp : public rclcpp::Node
  {
  public:
    COMPOSITION_PUBLIC
    explicit Grasp(const rclcpp::NodeOptions &options);

  private:
    /**;
     * Configures component.
     *
     * Declares parameters and configures video capture.
     */
    COMPOSITION_LOCAL
    void _configure();

    /**;
     * Declares the parameter using rcl_interfaces.
     */
    COMPOSITION_LOCAL
    void _initializeParameters();

    std::unique_ptr<MysqlConnector> _db;
    std::string _host;
    std::string _port;
    std::string _db_name;
    std::string _username;
    std::string _password;
    bool _debug;
    rclcpp::Subscription<custom_interfaces::msg::Grasp>::SharedPtr _sub;
  };

} // namespace composition

#endif // GRASP_DEBUG__COMPONENT_HPP_
