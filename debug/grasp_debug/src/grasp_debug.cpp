#include "grasp_debug/grasp_debug.hpp"

#include <iostream>
#include <memory>
#include "rclcpp/rclcpp.hpp"
#include <rclcpp/parameter.hpp>

namespace ros2mysql
{

  Grasp::Grasp(const rclcpp::NodeOptions &options)
      : Node("grasp_debug", options)
  {
    // Declare parameters.
    _initializeParameters();
    _configure();

    _db = std::make_unique<MysqlConnector>(_host, _port, _db_name, _username, _password, _debug);
    auto callback = [this](const typename custom_interfaces::msg::Grasp::SharedPtr msg) -> void {
      _db->clearTable("left_grasp");
      left_grasp_t left_grasp;
      left_grasp.item_id = msg->selected_item_id;

      // Eigen::Vector3f position(msg->grasp_pose.position.x,msg->grasp_pose.position.y,msg->grasp_pose.position.z);
      // Eigen::Quaternionf orientation(msg->grasp_pose.orientation.w,msg->grasp_pose.orientation.x,msg->grasp_pose.orientation.y,msg->grasp_pose.orientation.z);

      // left_grasp.grasp_position = helpers::commons::assignJsonFromPosition(position);
      // left_grasp.grasp_orientation = helpers::commons::assignJsonFromQuaternion(orientation);
      // left_grasp.grasp_width = msg->grasp_width;

      _db->setLeftGrasp(&left_grasp);
    };

    rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
    _sub = create_subscription<custom_interfaces::msg::Grasp>("grasp", latching_qos, callback);
  }

  void Grasp::_configure()
  {
    this->get_parameter<std::string>("host", _host);
    this->get_parameter<std::string>("port", _port);
    this->get_parameter<std::string>("db_name", _db_name);
    this->get_parameter<std::string>("username", _username);
    this->get_parameter<std::string>("password", _password);
    this->get_parameter<bool>("debug", _debug);
  }

  void Grasp::_initializeParameters()
  {
    rcl_interfaces::msg::ParameterDescriptor host_descriptor;
    host_descriptor.name = "host";
    host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("host", "", host_descriptor);

    rcl_interfaces::msg::ParameterDescriptor port_descriptor;
    port_descriptor.name = "port";
    port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("port", "", port_descriptor);

    rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
    db_name_descriptor.name = "db_name";
    db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("db_name", "", db_name_descriptor);

    rcl_interfaces::msg::ParameterDescriptor username_descriptor;
    username_descriptor.name = "username";
    username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("username", "", username_descriptor);

    rcl_interfaces::msg::ParameterDescriptor password_descriptor;
    password_descriptor.name = "password";
    password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("password", "", password_descriptor);

    rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
    debug_descriptor.name = "debug";
    debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
    this->declare_parameter("debug", false, debug_descriptor);
  }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::Grasp)
