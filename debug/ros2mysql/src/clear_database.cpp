// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "ros2mysql/clear_database.hpp"

#include <cinttypes>
#include <iostream>
#include <memory>

#include "custom_interfaces/srv/clear_database.hpp"
#include "rclcpp/rclcpp.hpp"
#include <rclcpp/parameter.hpp>
#include "helpers_commons/watchdog.hpp"

namespace ros2mysql
{

  ClearDatabase::ClearDatabase(const rclcpp::NodeOptions &options)
      : Node("clear_database", options)
  {
    // Declare parameters.
    this->initialize_parameters();

    this->configure();
    try
    {
      this->db_ = new MysqlConnector(host_, port_, db_name_, username_, password_, debug_);
    }
    catch (const network_exception &e)
    {
      RCLCPP_ERROR(this->get_logger(), e.what());
    }

    auto clear_database =
        [this](
            const std::shared_ptr<custom_interfaces::srv::ClearDatabase::Request> request,
            std::shared_ptr<custom_interfaces::srv::ClearDatabase::Response> response) -> void
    {
      if (status != custom_interfaces::msg::Heartbeat::RUNNING)
      {
        RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
        response->success = false;
        return;
      }

      RCLCPP_INFO(this->get_logger(), "Incoming request: [/custom_interfaces/srv/ClearDatabase]");
      std::flush(std::cout);
      RCLCPP_INFO(this->get_logger(), "Begin cleaning database...");

      try
      {
        this->db_->clearTable("camera_parameter");
        this->db_->clearTable("log");
        this->db_->clearTable("scene");
        this->db_->clearTable("area");
        this->db_->clearTable("item_cam1");
        this->db_->clearTable("item_cam2");
        this->db_->clearTable("item");
        this->db_->clearTable("item_element");
        this->db_->clearTable("left_arm_state");
        this->db_->clearTable("right_arm_state");
        this->db_->clearTable("left_arm_goal");
        this->db_->clearTable("right_arm_goal");
        this->db_->clearTable("left_arm_position");
        this->db_->clearTable("right_arm_position");
        this->db_->clearTable("left_arm_path");
        this->db_->clearTable("right_arm_path");
        this->db_->clearTable("left_arm_trajectory");
        this->db_->clearTable("right_arm_trajectory");
        this->db_->clearTable("left_gripper_state");
        this->db_->clearTable("right_gripper_state");
        this->db_->clearTable("left_gripper_goal");
        this->db_->clearTable("right_gripper_goal");
        this->db_->clearTable("left_grasp");
        this->db_->clearTable("right_grasp");
        this->db_->clearTable("left_grasp_quality");
        this->db_->clearTable("right_grasp_quality");
      }
      catch (const query_exception &e)
      {
        RCLCPP_ERROR(this->get_logger(), e.what());
      }

      RCLCPP_INFO(this->get_logger(), "Finish cleaning database...");
      response->success = true;
    };

    srv_ = create_service<custom_interfaces::srv::ClearDatabase>("clear_database", clear_database);

    _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
    status = custom_interfaces::msg::Heartbeat::STOPPED;
  }

  void ClearDatabase::initNode()
  {
    status = custom_interfaces::msg::Heartbeat::STARTING;

    status = custom_interfaces::msg::Heartbeat::RUNNING;
  }

  void ClearDatabase::shutDownNode()
  {
    status = custom_interfaces::msg::Heartbeat::STOPPING;

    status = custom_interfaces::msg::Heartbeat::STOPPED;
  }

  void ClearDatabase::configure()
  {
    this->get_parameter<std::string>("host", host_);
    this->get_parameter<std::string>("port", port_);
    this->get_parameter<std::string>("db_name", db_name_);
    this->get_parameter<std::string>("username", username_);
    this->get_parameter<std::string>("password", password_);
    this->get_parameter<bool>("debug", debug_);
  }

  void ClearDatabase::initialize_parameters()
  {
    rcl_interfaces::msg::ParameterDescriptor host_descriptor;
    host_descriptor.name = "host";
    host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    host_descriptor.read_only = true;
    this->declare_parameter("host", "", host_descriptor);

    rcl_interfaces::msg::ParameterDescriptor port_descriptor;
    port_descriptor.name = "port";
    port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    port_descriptor.read_only = true;
    this->declare_parameter("port", "", port_descriptor);

    rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
    db_name_descriptor.name = "db_name";
    db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    db_name_descriptor.read_only = true;
    this->declare_parameter("db_name", "", db_name_descriptor);

    rcl_interfaces::msg::ParameterDescriptor username_descriptor;
    username_descriptor.name = "username";
    username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    username_descriptor.read_only = true;
    this->declare_parameter("username", "", username_descriptor);

    rcl_interfaces::msg::ParameterDescriptor password_descriptor;
    password_descriptor.name = "password";
    password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    password_descriptor.read_only = true;
    this->declare_parameter("password", "", password_descriptor);

    rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
    debug_descriptor.name = "debug";
    debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
    debug_descriptor.read_only = true;
    this->declare_parameter("debug", false, debug_descriptor);
  }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::ClearDatabase)
