#include "ros2mysql/rosout2mysql.hpp"

#include <cinttypes>
#include <iostream>
#include <iomanip>
#include <memory>

#include "rcl_interfaces/msg/log.hpp"
#include "custom_interfaces/msg/occupancy_grid.hpp"
#include "rclcpp/rclcpp.hpp"
#include <rclcpp/parameter.hpp>

namespace ros2mysql
{

    Rosout2Mysql::Rosout2Mysql(const rclcpp::NodeOptions &options)
        : Node("rosout2mysql", options)
    {
        // Declare parameters.
        this->initialize_parameters();

        this->configure();
        try
        {
            this->db_ = new MysqlConnector(host_, port_, db_name_, username_, password_, debug_);
        }
        catch (const network_exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
        }

        auto callback = [this](const rcl_interfaces::msg::Log::SharedPtr msg) -> void
        {
            // Validate input message
            // if (msg->header.stamp == builtin_interfaces::msg::Time())
            //     return;

            if(status!=custom_interfaces::msg::Heartbeat::RUNNING)
            {
                RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
                return;
            }

            try
            {
                log_t log_data;
                log_data.log_level = msg->level;
                log_data.log_timestamp = this->_stampToString(msg, "%Y-%m-%d %H:%M:%S");
                log_data.log_name = msg->name;
                log_data.log_msg = msg->msg;
                log_data.log_file = msg->file;
                log_data.log_function = msg->function;
                log_data.log_line = msg->line;

                db_->log(&log_data);
            }
            catch (const network_exception &e)
            {
                // std::cout << e.what() << std::
                // RCLCPP_ERROR(this->get_logger(), e.what());
            }
            catch (const query_exception &e)
            {
                // RCLCPP_ERROR(this->get_logger(), e.what());
            }
        };

        sub_ = create_subscription<rcl_interfaces::msg::Log>("rosout", 1, callback);

        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void Rosout2Mysql::initNode()
    {
        status = custom_interfaces::msg::Heartbeat::STARTING;

        status = custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void Rosout2Mysql::shutDownNode()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;

        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    std::string Rosout2Mysql::_stampToString(const rcl_interfaces::msg::Log::SharedPtr msg, const std::string format = "%Y-%m-%d %H:%M:%S")
    {
        const int output_size = 100;
        char output[output_size];
        std::time_t raw_time = static_cast<time_t>(msg->stamp.sec);
        struct tm *timeinfo = localtime(&raw_time);
        std::strftime(output, output_size, format.c_str(), timeinfo);
        std::stringstream ss;
        ss << std::setw(9) << std::setfill('0') << msg->stamp.nanosec;
        const size_t fractional_second_digits = 4;
        return std::string(output) + "." + ss.str().substr(0, fractional_second_digits);
    }

    void Rosout2Mysql::configure()
    {
        this->get_parameter<std::string>("host", host_);
        this->get_parameter<std::string>("port", port_);
        this->get_parameter<std::string>("db_name", db_name_);
        this->get_parameter<std::string>("username", username_);
        this->get_parameter<std::string>("password", password_);
        this->get_parameter<bool>("debug", debug_);
    }

    void Rosout2Mysql::initialize_parameters()
    {
        rcl_interfaces::msg::ParameterDescriptor host_descriptor;
        host_descriptor.name = "host";
        host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        host_descriptor.read_only = true;
        this->declare_parameter("host", "", host_descriptor);

        rcl_interfaces::msg::ParameterDescriptor port_descriptor;
        port_descriptor.name = "port";
        port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        port_descriptor.read_only = true;
        this->declare_parameter("port", "", port_descriptor);

        rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
        db_name_descriptor.name = "db_name";
        db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        db_name_descriptor.read_only = true;
        this->declare_parameter("db_name", "", db_name_descriptor);

        rcl_interfaces::msg::ParameterDescriptor username_descriptor;
        username_descriptor.name = "username";
        username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        username_descriptor.read_only = true;
        this->declare_parameter("username", "", username_descriptor);

        rcl_interfaces::msg::ParameterDescriptor password_descriptor;
        password_descriptor.name = "password";
        password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        password_descriptor.read_only = true;
        this->declare_parameter("password", "", password_descriptor);

        rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
        debug_descriptor.name = "debug";
        debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
        debug_descriptor.read_only = true;
        this->declare_parameter("debug", false, debug_descriptor);
    }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::Rosout2Mysql)
