// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ROS2MYSQL__ROSOUT2MYSQL_COMPONENT_HPP_
#define ROS2MYSQL__ROSOUT2MYSQL_COMPONENT_HPP_

#include "ros2mysql/visibility_control.h"
#include "rcl_interfaces/msg/log.hpp"
#include "builtin_interfaces/msg/time.hpp"
#include "rclcpp/rclcpp.hpp"
#include <mysql_connector.h>
#include "helpers_commons/watchdog.hpp"

namespace ros2mysql
{

  class Rosout2Mysql : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    COMPOSITION_PUBLIC
    explicit Rosout2Mysql(const rclcpp::NodeOptions &options);

    void initNode() override;
    void shutDownNode() override;

  private:
    COMPOSITION_LOCAL
    void
    configure();

    COMPOSITION_LOCAL
    void
    initialize_parameters();

    COMPOSITION_LOCAL
    std::string _stampToString(const rcl_interfaces::msg::Log::SharedPtr msg, const std::string format);

    helpers::Watchdog::SharedPtr _watchdog;

    MysqlConnector *db_;
    std::string host_;
    std::string port_;
    std::string db_name_;
    std::string username_;
    std::string password_;
    bool debug_;
    rclcpp::Subscription<rcl_interfaces::msg::Log>::SharedPtr sub_;
  };

} // namespace ros2mysql

#endif // ROS2MYSQL__ROSOUT2MYSQL_COMPONENT_HPP_
