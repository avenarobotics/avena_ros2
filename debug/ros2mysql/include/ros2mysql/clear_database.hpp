// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef COMPOSITION__SERVER_COMPONENT_HPP_
#define COMPOSITION__SERVER_COMPONENT_HPP_

#include "ros2mysql/visibility_control.h"
#include "custom_interfaces/srv/clear_database.hpp"
#include "rclcpp/rclcpp.hpp"
#include <mysql_connector.h>
#include "helpers_commons/watchdog.hpp"

namespace ros2mysql
{

  class ClearDatabase : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    COMPOSITION_PUBLIC
    explicit ClearDatabase(const rclcpp::NodeOptions &options);

    void initNode() override;
    void shutDownNode() override;

  private:
    /**;
   * Configures component.
   *
   * Declares parameters and configures video capture.
   */
    COMPOSITION_PUBLIC
    void
    configure();

    /**;
   * Declares the parameter using rcl_interfaces.
   */
    COMPOSITION_PUBLIC
    void
    initialize_parameters();

    helpers::Watchdog::SharedPtr _watchdog;

    MysqlConnector *db_;
    std::string host_;
    std::string port_;
    std::string db_name_;
    std::string username_;
    std::string password_;
    bool debug_;
    rclcpp::Service<custom_interfaces::srv::ClearDatabase>::SharedPtr srv_;
  };

} // namespace ros2mysql

#endif // COMPOSITION__SERVER_COMPONENT_HPP_
