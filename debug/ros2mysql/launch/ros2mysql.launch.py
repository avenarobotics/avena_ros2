# Copyright 2019 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch a talker and a listener in a component container."""
import os
import launch
import yaml
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    """Generate launch description with multiple components."""
    config_dir = os.path.join(get_package_share_directory('ros2mysql'), 'config')   
    param_config = os.path.join(config_dir, "db_params.yaml")

    with open(param_config, 'r') as f:
        params = yaml.safe_load(f)["db_parameters"]

    container = ComposableNodeContainer(
            name='ros2mysql',
            namespace='',
            package='rclcpp_components',
            executable='component_container',
            composable_node_descriptions=[
                ComposableNode(package='item_select', plugin='ros2mysql::ItemSelect', name='item_select_debug', parameters=[params]),
                ComposableNode(package='place_debug', plugin='ros2mysql::Place', name='place_debug', parameters=[params]),
                ComposableNode(package='estimate_shape_debug', plugin='ros2mysql::EstimateShape', name='estimate_shape_debug', parameters=[params]),
                ComposableNode(package='compose_items', plugin='ros2mysql::ComposeItems', name='compose_items_debug', parameters=[params]),
                ComposableNode(package='detect_debug', plugin='ros2mysql::Detect', name='detect_debug', parameters=[params]),
                ComposableNode(package='scene_publisher', plugin='ros2mysql::ScenePublisher', name='scene_publisher_debug', parameters=[params]),
                ComposableNode(package='change_detect', plugin='ros2mysql::ChangeDetect', name='change_detect_debug', parameters=[params]),
                ComposableNode(package='get_occupancy_grid', plugin='ros2mysql::GetOccupancyGrid', name='get_occupancy_grid_debug', parameters=[params]),
                ComposableNode(package='octomap_filter', plugin='ros2mysql::OctomapFilter', name='octomap_filter_debug', parameters=[params]),
                ComposableNode(package='grasp_debug', plugin='ros2mysql::Grasp', name='grasp_debug', parameters=[params]),
                ComposableNode(package='validate_estimate_shape', plugin='ros2mysql::ValidateEstimateShape', name='validate_estimate_shape_debug', parameters=[params]),

                ComposableNode(package='ros2mysql', plugin='ros2mysql::ClearDatabase', name='clear_database', parameters=[params]),
                ComposableNode(package='ros2mysql', plugin='ros2mysql::Rosout2Mysql', name='rosout2mysql', parameters=[params]),
            ],
            output='screen',
    )

    return launch.LaunchDescription([container])
