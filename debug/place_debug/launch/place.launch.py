import launch
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode


def generate_launch_description():
    container = ComposableNodeContainer(
        name='place_container',
        namespace='',
        package='rclcpp_components',
        executable='component_container',
        composable_node_descriptions=[
                ComposableNode(
                    package='place',
                    plugin='place::PlaceServer',
                    name='place',
                    parameters=[
                         {"search_shift": 0.005},
                          ############Temp##########
                         {"item_in_hand_id": 1},
                         {"target_area": "operating_area"}
                    ])
        ],

        output='screen',
    )

    return launch.LaunchDescription([container])
