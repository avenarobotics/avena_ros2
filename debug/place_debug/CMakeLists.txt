cmake_minimum_required(VERSION 3.5)
project(place_debug)

# Default to C++17
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

set(CMAKE_BUILD_TYPE RELEASE)

# __ROS__
find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_components REQUIRED)
find_package(rclcpp_action REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(std_msgs REQUIRED)

# __External__
find_package(OpenCV REQUIRED)
find_package(PCL 1.10 REQUIRED)
find_package(nlohmann_json REQUIRED)
find_package(Eigen3 REQUIRED)

# __Avena__
find_package(helpers_commons REQUIRED)
find_package(helpers_vision REQUIRED)
find_package(custom_interfaces REQUIRED)
find_package(db_connector REQUIRED)

# __include__
include_directories(
  include
)



# - place_debug -------------------------------------
add_library(place_debug SHARED src/place_debug.cpp)
target_compile_definitions(place_debug PRIVATE "COMPOSITION_BUILDING_DLL")
ament_target_dependencies(place_debug
  "rclcpp"
  "rclcpp_components"
  "custom_interfaces"
  "db_connector"
  "helpers_commons"
  "helpers_vision"
  )
rclcpp_components_register_nodes(place_debug "ros2mysql::Place")


install(TARGETS
place_debug
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin)

install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}
)

ament_package()