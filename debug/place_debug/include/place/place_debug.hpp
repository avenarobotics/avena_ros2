#ifndef PLACE_DEBUG__COMPONENT_HPP_
#define PLACE_DEBUG__COMPONENT_HPP_

#include "place/visibility_control.h"
#include "rclcpp/rclcpp.hpp"
#include <mysql_connector.h>
#include <assert.h>
#include <sstream>

#include "custom_interfaces/msg/place.hpp"
#include "custom_interfaces/msg/place_data.hpp"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"

#include <memory>

namespace ros2mysql
{
  using json = nlohmann::json;

  class Place : public rclcpp::Node
  {
  public:
    COMPOSITION_PUBLIC
    explicit Place(const rclcpp::NodeOptions &options);

  private:
    /**;
   * Configures component.
   *
   * Declares parameters and configures video capture.
   */
    COMPOSITION_PUBLIC
    void
    configure();

    /**;
   * Declares the parameter using rcl_interfaces.
   */
    COMPOSITION_PUBLIC
    void
    initialize_parameters();

    std::unique_ptr<MysqlConnector> db_;
    std::string host_;
    std::string port_;
    std::string db_name_;
    std::string username_;
    std::string password_;
    bool debug_;
    rclcpp::Subscription<custom_interfaces::msg::Place>::SharedPtr sub_;
    std::map<int, std::string> _label_int_to_str;
  };

} // namespace ros2mysql

#endif // Place_DEBUG__COMPONENT_HPP_
