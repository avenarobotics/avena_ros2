#include "place/place_debug.hpp"

#include <iostream>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include <rclcpp/parameter.hpp>

namespace ros2mysql
{

    Place::Place(const rclcpp::NodeOptions &options)
        : Node("place_debug", options)
    {
        // Declare parameters.
        this->initialize_parameters();

        this->configure();

        using namespace std::chrono_literals;
        try
        {
            this->db_ = std::make_unique<MysqlConnector>(this->host_, this->port_, this->db_name_, this->username_, this->password_, this->debug_);
        }
        catch (const network_exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
        }

        auto callback =
            [this](const typename custom_interfaces::msg::Place::SharedPtr msg) -> void {
            RCLCPP_INFO(this->get_logger(), "Place debug: received data");

            left_arm_position_t left_arm_position_data;
            for (auto &place_pose : msg->place_poses)
            {
                for (auto &place_position : place_pose.poses)
                {
                    // RCLCPP_INFO(this->get_logger(), "place pose");

                    Eigen::Vector3f place_position_vec(place_position.position.x, place_position.position.y, place_position.position.z);
                    Eigen::Quaternionf place_position_orientation(place_position.orientation.w, place_position.orientation.x, place_position.orientation.y, place_position.orientation.z);
                    try
                    {
                        left_arm_position_data.position.push_back(helpers::vision::assignJsonFromPosition(place_position_vec));
                        left_arm_position_data.orientation.push_back(helpers::vision::assignJsonFromQuaternion(place_position_orientation));
                    }
                    catch (const json::exception &e)
                    {
                        RCLCPP_FATAL_STREAM(get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
                        return;
                        // rclcpp::shutdown();
                    }
                    catch (const std::exception &e)
                    {
                        RCLCPP_FATAL_STREAM(get_logger(), "OTHER Exception with message: " << e.what());
                        return;
                    }
                }
            }

            try
            {
                db_->clearTable("left_arm_position");
                db_->setLeftArmPosition(&left_arm_position_data); // setRightArmPosition
            }
            catch (const network_exception &e)
            {
                RCLCPP_ERROR(this->get_logger(), e.what());
            }
            catch (const query_exception &e)
            {
                RCLCPP_ERROR(this->get_logger(), e.what());
            }
            catch (const json::exception &e)
            {
                RCLCPP_ERROR(this->get_logger(), e.what());
            }
            catch (const sql::SQLException &e)
            {
                RCLCPP_ERROR(this->get_logger(), e.what());
            }
        };

        rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        sub_ = create_subscription<custom_interfaces::msg::Place>("place", latching_qos, callback);
    }

    void
    Place::configure()
    {
        this->get_parameter<std::string>("host", host_);
        this->get_parameter<std::string>("port", port_);
        this->get_parameter<std::string>("db_name", db_name_);
        this->get_parameter<std::string>("username", username_);
        this->get_parameter<std::string>("password", password_);
        this->get_parameter<bool>("debug", debug_);
    }

    void Place::initialize_parameters()
    {
        rcl_interfaces::msg::ParameterDescriptor host_descriptor;
        host_descriptor.name = "host";
        host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("host", "", host_descriptor);

        rcl_interfaces::msg::ParameterDescriptor port_descriptor;
        port_descriptor.name = "port";
        port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("port", "", port_descriptor);

        rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
        db_name_descriptor.name = "db_name";
        db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("db_name", "", db_name_descriptor);

        rcl_interfaces::msg::ParameterDescriptor username_descriptor;
        username_descriptor.name = "username";
        username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("username", "", username_descriptor);

        rcl_interfaces::msg::ParameterDescriptor password_descriptor;
        password_descriptor.name = "password";
        password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("password", "", password_descriptor);

        rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
        debug_descriptor.name = "debug";
        debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
        this->declare_parameter("debug", false, debug_descriptor);
    }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::Place)
