
#ifndef MYSQL_CONNECTOR_H
#define MYSQL_CONNECTOR_H

#include <cppconn/driver.h>
#include <cppconn/connection.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/exception.h>
#include <cppconn/statement.h>
#include <iostream>
#include <vector>

#include "mysql_connector_structs.h"

using namespace std;

enum log_level
{
    error = 0, //zamiast średników stosuje się przecinki
    warning = 1,
    info = 2,
    debug = 3
};

class MysqlConnector
{
    string hostname;
    string port;
    string schema_name;
    string username;
    string password;

    sql::Driver *driver;
    sql::Connection *connection;

    sql::Statement *stmt;
    sql::ResultSet *res;

    int connection_fd;

    bool is_connected = false;
    uint32_t queries_ = 0;
    uint32_t queries_limits_ = 5000;

    bool debug = false;

    void check_connection();
    void connect();
    bool disconnect();
    nlohmann::json convertStringToJSON(std::string content);
    string buildQuery(string table_name, string pk_column, std::map <std::string, std::string> fields_values, uint32_t row_id);
    void addColumnToQuery(std::map <std::string, std::string> *column_map, string column_name, bool timestamp = false);
    uint32_t getLastId();

    public:
    MysqlConnector(string hostname, string port, string schema_name, string username, string password, bool debug = false);
    ~MysqlConnector();

    bool isConnected();

    vector<uint32_t> getRowsId(string table_name);
    void getLabel(label_t *label_data, uint32_t row_id);
    // void getLabelElement(label_element_t *label_element_data, uint32_t row_id);
    void getCameraParameter(camera_parameter_t *label_element_data, uint32_t row_id);
    void getArea(area_t *area_data, uint32_t row_id);
    void getChangeDetect(change_detect_t *change_detect_data, uint32_t row_id);
    void getScene(scene_t *scene_data, uint32_t row_id);
    void getItemCam1(item_cam1_t *item_cam1_data, uint32_t row_id);
    void getItemCam2(item_cam2_t *item_cam2_data, uint32_t row_id);
    void getItem(item_t *item_data, uint32_t row_id);
    void getItemElement(item_element_t *item_element_data, uint32_t row_id);
    void getLeftArmState(left_arm_state_t *left_arm_state_data, uint32_t row_id);
    void getRightArmState(right_arm_state_t *right_arm_state_data, uint32_t row_id);
    void getLeftArmGoal(left_arm_goal_t *left_arm_goal_data, uint32_t row_id);
    void getRightArmGoal(right_arm_goal_t *right_arm_goal_data, uint32_t row_id);
    void getLeftArmPosition(left_arm_position_t *left_arm_position_data, uint32_t row_id);
    void getRightArmPosition(right_arm_position_t *right_arm_position_data, uint32_t row_id);
    void getLeftArmPath(left_arm_path_t *left_arm_path_data, uint32_t row_id);
    void getRightArmPath(right_arm_path_t *right_arm_path_data, uint32_t row_id);
    void getLeftArmTrajectory(left_arm_trajectory_t *left_arm_trajectory_data, uint32_t row_id);
    void getRightArmTrajectory(right_arm_trajectory_t *right_arm_trajectory_data, uint32_t row_id);
    void getLeftGripperState(left_gripper_state_t *left_gripper_state_data, uint32_t row_id);
    void getRightGripperState(right_gripper_state_t *right_gripper_state_data, uint32_t row_id);
    void getLeftGripperGoal(left_gripper_goal_t *left_gripper_goal_data, uint32_t row_id);
    void getRightGripperGoal(right_gripper_goal_t *right_gripper_goal_data, uint32_t row_id);
    void getLeftGrasp(left_grasp_t *left_grasp_data, uint32_t row_id);
    void getRightGrasp(right_grasp_t *right_grasp_data, uint32_t row_id);
    void getLeftGraspQuality(left_grasp_quality_t *left_grasp_quality_data, uint32_t row_id);
    void getRightGraspQuality(right_grasp_quality_t *right_grasp_quality_data, uint32_t row_id);

    void setLabel(label_t *label_data);
    // void setLabelElement(label_element_t *label_element_data);
    void setCameraParameter(camera_parameter_t *camera_parameter_data);
    void setArea(area_t *area_data);
    void setChangeDetect(change_detect_t *change_detect_data);
    void setScene(scene_t *scene_data);
    void setItemCam1(item_cam1_t *item_cam1_data);
    void setItemCam2(item_cam2_t *item_cam2_data);
    void setItem(item_t *item_data);
    void setItemElement(item_element_t *item_element_data);
    void setLeftArmState(left_arm_state_t *left_arm_state_data);
    void setRightArmState(right_arm_state_t *right_arm_state_data);
    void setLeftArmGoal(left_arm_goal_t *left_arm_goal_data);
    void setRightArmGoal(right_arm_goal_t *right_arm_goal_data);
    void setLeftArmPosition(left_arm_position_t *left_arm_position_data);
    void setRightArmPosition(right_arm_position_t *right_arm_position_data);
    void setLeftArmPath(left_arm_path_t *left_arm_path_data);
    void setRightArmPath(right_arm_path_t *right_arm_path_data);
    void setLeftArmTrajectory(left_arm_trajectory_t *left_arm_trajectory_data);
    void setRightArmTrajectory(right_arm_trajectory_t *right_arm_trajectory_data);
    void setLeftGripperState(left_gripper_state_t *left_gripper_state_data);
    void setRightGripperState(right_gripper_state_t *right_gripper_state_data);
    void setLeftGrasp(left_grasp_t *left_grasp_data);
    void setRightGrasp(right_grasp_t *right_grasp_data);

    void setLeftGripperGoal(left_gripper_goal_t *left_gripper_goal_data);
    void setRightGripperGoal(right_gripper_goal_t *right_gripper_goal_data);
    void setLeftGraspQuality(left_grasp_quality_t *left_grasp_quality_data);
    void setRightGraspQuality(right_grasp_quality_t *right_grasp_quality_data);

    void clearTable(string table_name);
    void log(log_t *log_data);
    // void log(log_level level, string process, string message);

    private:
    uint32_t writeFK(string table_name, string column_name, int fk_key, uint32_t row_id=0);
    uint32_t writeInt(string table_name, string column_name, int int_data, uint32_t row_id=0);
    uint32_t writeDouble(string table_name, string column_name, double double_data, uint32_t row_id=0);
    uint32_t writeString(string table_name, string column_name, string string_data, uint32_t row_id=0);
    uint32_t writeJSON(string table_name, string column_name, nlohmann::json json_data, uint32_t row_id=0);
    uint32_t writeBLOB(string table_name, string column_name, iostream *blob_data, uint32_t row_id=0);

};

#endif // MYSQL_CONNECTOR_H