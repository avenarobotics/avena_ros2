#ifndef MYSQL_CONNECTOR_STRUCTS_H
#define MYSQL_CONNECTOR_STRUCTS_H

#include <chrono>
#include <memory>
#include <exception>

#include <nlohmann/json.hpp>

using namespace std;

#define NULL_DOUBLE 90000000.0f
#define NULL_UINT -1
#define NULL_BOOL -1

struct network_exception : public std::exception
{
	const char * what () const throw ()
    {
    	return "Mysql is not accesible";
    }
};

struct query_exception : public std::exception
{
	const char * what () const throw ()
    {
    	return "Bad query";
    }
};

typedef struct label {
    uint32_t label_id = 0;
    string label {};
    string fit_method {};
    nlohmann::json fit_method_parameters {};
    bool item = NULL_BOOL;
    bool element = NULL_BOOL;
    nlohmann::json components {};
} label_t;

typedef struct camera_parameter {
    uint32_t camera_parameter_id = 0;
    string camera_name {};
    string camera_name_timestamp;
    nlohmann::json position {};
    string position_timestamp;
    nlohmann::json orientation {};
    string orientation_timestamp;
    nlohmann::json parameters {};
    string parameters_timestamp;
} camera_parameter_t;

typedef struct area {
    uint32_t area_id = 0;
    string area_name {};
    string area_name_timestamp;
    nlohmann::json position {};
    string position_timestamp;
    nlohmann::json orientation {};
    string orientation_timestamp;
    nlohmann::json shape {};
    string shape_timestamp;
} area_t;

typedef struct change_detect {
    uint32_t change_detect_id = 0;
    std::shared_ptr <iostream> camera_1_rgb {};
    string camera_1_rgb_timestamp;
    std::shared_ptr <iostream> camera_1_depth {};
    string camera_1_depth_timestamp;
    std::shared_ptr <iostream> camera_2_rgb {};
    string camera_2_rgb_timestamp;
    std::shared_ptr <iostream> camera_2_depth {};
    string camera_2_depth_timestamp;
    std::shared_ptr <iostream> pcl_camera_1 {};
    string pcl_camera_1_timestamp;
    std::shared_ptr <iostream> pcl_camera_2 {};
    string pcl_camera_2_timestamp;
    nlohmann::json robot_transforms {};
    string robot_transforms_timestamp;
    std::shared_ptr <iostream> pcl_camera_1_transformed {};
    string pcl_camera_1_transformed_timestamp;
    std::shared_ptr <iostream> pcl_camera_2_transformed {};
    string pcl_camera_2_transformed_timestamp;
    std::shared_ptr <iostream> merged_pcl {};
    string merged_pcl_timestamp;
    std::shared_ptr <iostream> merged_pcl_filtered {};
    string merged_pcl_filtered_timestamp;
    double scene_change_percentage = NULL_DOUBLE;
    string scene_change_percentage_timestamp;
    uint32_t tools_area = NULL_UINT;
    string tools_area_timestamp;
    uint32_t items_area = NULL_UINT;
    string items_area_timestamp;
    uint32_t security_area = NULL_UINT;
    string security_area_timestamp;
    uint32_t operating_area = NULL_UINT;
    string operating_area_timestamp;
    std::shared_ptr <iostream> pcl_diff {};
    string pcl_diff_timestamp;
} change_detect_t;

typedef struct scene {
    uint32_t scene_id = 0;
    std::shared_ptr <iostream> camera_1_rgb {};
    string camera_1_rgb_timestamp;
    std::shared_ptr <iostream> camera_1_depth {};
    string camera_1_depth_timestamp;
    std::shared_ptr <iostream> camera_2_rgb {};
    string camera_2_rgb_timestamp;
    std::shared_ptr <iostream> camera_2_depth {};
    string camera_2_depth_timestamp;
    std::shared_ptr <iostream> pcl_camera_1 {};
    string pcl_camera_1_timestamp;
    std::shared_ptr <iostream> pcl_camera_2 {};
    string pcl_camera_2_timestamp;
    std::shared_ptr <iostream> annotated_camera_1 {};
    string annotated_camera_1_timestamp;
    std::shared_ptr <iostream> annotated_camera_2 {};
    string annotated_camera_2_timestamp;
    std::shared_ptr <iostream> merged_pcl {};
    string merged_pcl_timestamp;
    std::shared_ptr <iostream> occupancy_grid {};
    string occupancy_grid_timestamp;
    std::shared_ptr <iostream> pcl_scene_diff {};
    string pcl_scene_diff_timestamp;
    nlohmann::json scene_diff_info {};
    string scene_diff_info_timestamp;
    std::shared_ptr <iostream> filtered_octomap {};
    string filtered_octomap_timestamp;
    std::shared_ptr <iostream> pcl_self_filtered {};
    string pcl_self_filtered_timestamp;
} scene_t;

typedef struct item_cam1 {
    uint32_t item_cam1_id = 0;
    uint32_t scene_id = NULL_UINT;
    std::shared_ptr <iostream> mask {};
    string mask_timestamp;
    double accuracy = NULL_DOUBLE;
    string accuracy_timestamp;
    uint32_t label_id = NULL_UINT;
    string label_id_timestamp;
    std::shared_ptr <iostream> depth_data {};
    string depth_data_timestamp;
    std::shared_ptr <iostream> pcl_data {};
    string pcl_data_timestamp;
} item_cam1_t;

typedef struct item_cam2 {
    uint32_t item_cam2_id = 0;
    uint32_t scene_id = NULL_UINT;
    std::shared_ptr <iostream> mask {};
    string mask_timestamp;
    double accuracy = NULL_DOUBLE;
    string accuracy_timestamp;
    uint32_t label_id = NULL_UINT;
    string label_id_timestamp;
    std::shared_ptr <iostream> depth_data {};
    string depth_data_timestamp;
    std::shared_ptr <iostream> pcl_data {};
    string pcl_data_timestamp;
} item_cam2_t;

typedef struct item {
    uint32_t item_id = 0;
    uint32_t item_cam1_id = NULL_UINT;
    uint32_t item_cam2_id = NULL_UINT;
    uint32_t id = 0;
    string item_id_hash {};
    string item_id_hash_timestamp;
    string label {};
    string label_timestamp;
} item_t;

typedef struct item_element {
    uint32_t item_element_id = 0;
    uint32_t item_id = NULL_UINT;
    uint32_t id = 0;
    string element_label;
    string element_label_timestamp;
    std::shared_ptr <iostream> pcl_merged {};
    string pcl_merged_timestamp;
    std::shared_ptr <iostream> element_mask_1 {};
    string element_mask_1_timestamp;
    std::shared_ptr <iostream> element_mask_2 {};
    string element_mask_2_timestamp;
    std::shared_ptr <iostream> element_depth_1 {};
    string element_depth_1_timestamp;
    std::shared_ptr <iostream> element_depth_2 {};
    string element_depth_2_timestamp;
    std::shared_ptr <iostream> element_pcl_1 {};
    string element_pcl_1_timestamp;
    std::shared_ptr <iostream> element_pcl_2 {};
    string element_pcl_2_timestamp;
    nlohmann::json gt_position {};
    string gt_position_timestamp;
    nlohmann::json gt_orientation {};
    string gt_orientation_timestamp;
    nlohmann::json primitive_shape {};
    string primitive_shape_timestamp;
    nlohmann::json position {};
    string position_timestamp;
    nlohmann::json orientation {};
    string orientation_timestamp;
    nlohmann::json estimation_error {};
    string estimation_error_timestamp;
} item_element_t;

typedef struct left_arm_state {
    uint32_t left_arm_state_id = 0;
    double joint1_position = NULL_DOUBLE;
    string joint1_position_timestamp;
    double joint2_position = NULL_DOUBLE;
    string joint2_position_timestamp;
    double joint3_position = NULL_DOUBLE;
    string joint3_position_timestamp;
    double joint4_position = NULL_DOUBLE;
    string joint4_position_timestamp;
    double joint5_position = NULL_DOUBLE;
    string joint5_position_timestamp;
    double joint6_position = NULL_DOUBLE;
    string joint6_position_timestamp;
    double joint7_position = NULL_DOUBLE;
    string joint7_position_timestamp;
    double joint1_temperature = NULL_DOUBLE;
    string joint1_temperature_timestamp;
    double joint2_temperature = NULL_DOUBLE;
    string joint2_temperature_timestamp;
    double joint3_temperature = NULL_DOUBLE;
    string joint3_temperature_timestamp;
    double joint4_temperature = NULL_DOUBLE;
    string joint4_temperature_timestamp;
    double joint5_temperature = NULL_DOUBLE;
    string joint5_temperature_timestamp;
    double joint6_temperature = NULL_DOUBLE;
    string joint6_temperature_timestamp;
    double joint7_temperature = NULL_DOUBLE;
    string joint7_temperature_timestamp;
    double joint1_current = NULL_DOUBLE;
    string joint1_current_timestamp;
    double joint2_current = NULL_DOUBLE;
    string joint2_current_timestamp;
    double joint3_current = NULL_DOUBLE;
    string joint3_current_timestamp;
    double joint4_current = NULL_DOUBLE;
    string joint4_current_timestamp;
    double joint5_current = NULL_DOUBLE;
    string joint5_current_timestamp;
    double joint6_current = NULL_DOUBLE;
    string joint6_current_timestamp;
    double joint7_current = NULL_DOUBLE;
    string joint7_current_timestamp;
    double joint1_torque = NULL_DOUBLE;
    string joint1_torque_timestamp;
    double joint2_torque = NULL_DOUBLE;
    string joint2_torque_timestamp;
    double joint3_torque = NULL_DOUBLE;
    string joint3_torque_timestamp;
    double joint4_torque = NULL_DOUBLE;
    string joint4_torque_timestamp;
    double joint5_torque = NULL_DOUBLE;
    string joint5_torque_timestamp;
    double joint6_torque = NULL_DOUBLE;
    string joint6_torque_timestamp;
    double joint7_torque = NULL_DOUBLE;
    string joint7_torque_timestamp;
    string joint1_state {};
    string joint1_state_timestamp;
    string joint2_state {};
    string joint2_state_timestamp;
    string joint3_state {};
    string joint3_state_timestamp;
    string joint4_state {};
    string joint4_state_timestamp;
    string joint5_state {};
    string joint5_state_timestamp;
    string joint6_state {};
    string joint6_state_timestamp;
    string joint7_state {};
    string joint7_state_timestamp;
} left_arm_state_t;

typedef struct right_arm_state {
    uint32_t right_arm_state_id = 0;
    double joint1_position = NULL_DOUBLE;
    string joint1_position_timestamp;
    double joint2_position = NULL_DOUBLE;
    string joint2_position_timestamp;
    double joint3_position = NULL_DOUBLE;
    string joint3_position_timestamp;
    double joint4_position = NULL_DOUBLE;
    string joint4_position_timestamp;
    double joint5_position = NULL_DOUBLE;
    string joint5_position_timestamp;
    double joint6_position = NULL_DOUBLE;
    string joint6_position_timestamp;
    double joint7_position = NULL_DOUBLE;
    string joint7_position_timestamp;
    double joint1_temperature = NULL_DOUBLE;
    string joint1_temperature_timestamp;
    double joint2_temperature = NULL_DOUBLE;
    string joint2_temperature_timestamp;
    double joint3_temperature = NULL_DOUBLE;
    string joint3_temperature_timestamp;
    double joint4_temperature = NULL_DOUBLE;
    string joint4_temperature_timestamp;
    double joint5_temperature = NULL_DOUBLE;
    string joint5_temperature_timestamp;
    double joint6_temperature = NULL_DOUBLE;
    string joint6_temperature_timestamp;
    double joint7_temperature = NULL_DOUBLE;
    string joint7_temperature_timestamp;
    double joint1_current = NULL_DOUBLE;
    string joint1_current_timestamp;
    double joint2_current = NULL_DOUBLE;
    string joint2_current_timestamp;
    double joint3_current = NULL_DOUBLE;
    string joint3_current_timestamp;
    double joint4_current = NULL_DOUBLE;
    string joint4_current_timestamp;
    double joint5_current = NULL_DOUBLE;
    string joint5_current_timestamp;
    double joint6_current = NULL_DOUBLE;
    string joint6_current_timestamp;
    double joint7_current = NULL_DOUBLE;
    string joint7_current_timestamp;
    double joint1_torque = NULL_DOUBLE;
    string joint1_torque_timestamp;
    double joint2_torque = NULL_DOUBLE;
    string joint2_torque_timestamp;
    double joint3_torque = NULL_DOUBLE;
    string joint3_torque_timestamp;
    double joint4_torque = NULL_DOUBLE;
    string joint4_torque_timestamp;
    double joint5_torque = NULL_DOUBLE;
    string joint5_torque_timestamp;
    double joint6_torque = NULL_DOUBLE;
    string joint6_torque_timestamp;
    double joint7_torque = NULL_DOUBLE;
    string joint7_torque_timestamp;
    string joint1_state {};
    string joint1_state_timestamp;
    string joint2_state {};
    string joint2_state_timestamp;
    string joint3_state {};
    string joint3_state_timestamp;
    string joint4_state {};
    string joint4_state_timestamp;
    string joint5_state {};
    string joint5_state_timestamp;
    string joint6_state {};
    string joint6_state_timestamp;
    string joint7_state {};
    string joint7_state_timestamp;
} right_arm_state_t;

typedef struct left_arm_goal {
    uint32_t left_arm_goal_id = 0;
    double joint1_torque = NULL_DOUBLE;
    string joint1_torque_timestamp;
    double joint2_torque = NULL_DOUBLE;
    string joint2_torque_timestamp;
    double joint3_torque = NULL_DOUBLE;
    string joint3_torque_timestamp;
    double joint4_torque = NULL_DOUBLE;
    string joint4_torque_timestamp;
    double joint5_torque = NULL_DOUBLE;
    string joint5_torque_timestamp;
    double joint6_torque = NULL_DOUBLE;
    string joint6_torque_timestamp;
    double joint7_torque = NULL_DOUBLE;
    string joint7_torque_timestamp;
} left_arm_goal_t;

typedef struct right_arm_goal {
    uint32_t right_arm_goal_id = 0;
    double joint1_torque = NULL_DOUBLE;
    string joint1_torque_timestamp;
    double joint2_torque = NULL_DOUBLE;
    string joint2_torque_timestamp;
    double joint3_torque = NULL_DOUBLE;
    string joint3_torque_timestamp;
    double joint4_torque = NULL_DOUBLE;
    string joint4_torque_timestamp;
    double joint5_torque = NULL_DOUBLE;
    string joint5_torque_timestamp;
    double joint6_torque = NULL_DOUBLE;
    string joint6_torque_timestamp;
    double joint7_torque = NULL_DOUBLE;
    string joint7_torque_timestamp;
} right_arm_goal_t;

typedef struct left_arm_position {
    uint32_t left_arm_position_id = 0;
    nlohmann::json position {};
    string position_timestamp;
    nlohmann::json orientation {};
    string orientation_timestamp;
} left_arm_position_t;

typedef struct right_arm_position {
    uint32_t right_arm_position_id = 0;
    nlohmann::json position {};
    string position_timestamp;
    nlohmann::json orientation {};
    string orientation_timestamp;
} right_arm_position_t;

typedef struct left_arm_path {
    uint32_t left_arm_path_id = 0;
    uint32_t left_arm_position_id = NULL_UINT;
    uint32_t line_number = NULL_UINT;
    string line_number_timestamp;
    double joint1_position = NULL_DOUBLE;
    string joint1_position_timestamp;
    double joint2_position = NULL_DOUBLE;
    string joint2_position_timestamp;
    double joint3_position = NULL_DOUBLE;
    string joint3_position_timestamp;
    double joint4_position = NULL_DOUBLE;
    string joint4_position_timestamp;
    double joint5_position = NULL_DOUBLE;
    string joint5_position_timestamp;
    double joint6_position = NULL_DOUBLE;
    string joint6_position_timestamp;
    double joint7_position = NULL_DOUBLE;
    string joint7_position_timestamp;
} left_arm_path_t;

typedef struct right_arm_path {
    uint32_t right_arm_path_id = 0;
    uint32_t right_arm_position_id = NULL_UINT;
    uint32_t line_number = NULL_UINT;
    string line_number_timestamp;
    double joint1_position = NULL_DOUBLE;
    string joint1_position_timestamp;
    double joint2_position = NULL_DOUBLE;
    string joint2_position_timestamp;
    double joint3_position = NULL_DOUBLE;
    string joint3_position_timestamp;
    double joint4_position = NULL_DOUBLE;
    string joint4_position_timestamp;
    double joint5_position = NULL_DOUBLE;
    string joint5_position_timestamp;
    double joint6_position = NULL_DOUBLE;
    string joint6_position_timestamp;
    double joint7_position = NULL_DOUBLE;
    string joint7_position_timestamp;
} right_arm_path_t;

typedef struct left_arm_trajectory {
    uint32_t left_arm_trajectory_id = 0;
    uint32_t left_arm_position_id = NULL_UINT;
    uint32_t line_number = NULL_UINT;
    string line_number_timestamp;
    double delta_t = NULL_DOUBLE;
    string delta_t_timestamp;
    double joint1_position = NULL_DOUBLE;
    string joint1_position_timestamp;
    double joint2_position = NULL_DOUBLE;
    string joint2_position_timestamp;
    double joint3_position = NULL_DOUBLE;
    string joint3_position_timestamp;
    double joint4_position = NULL_DOUBLE;
    string joint4_position_timestamp;
    double joint5_position = NULL_DOUBLE;
    string joint5_position_timestamp;
    double joint6_position = NULL_DOUBLE;
    string joint6_position_timestamp;
    double joint7_position = NULL_DOUBLE;
    string joint7_position_timestamp;
    double joint1_velocity = NULL_DOUBLE;
    string joint1_velocity_timestamp;
    double joint2_velocity = NULL_DOUBLE;
    string joint2_velocity_timestamp;
    double joint3_velocity = NULL_DOUBLE;
    string joint3_velocity_timestamp;
    double joint4_velocity = NULL_DOUBLE;
    string joint4_velocity_timestamp;
    double joint5_velocity = NULL_DOUBLE;
    string joint5_velocity_timestamp;
    double joint6_velocity = NULL_DOUBLE;
    string joint6_velocity_timestamp;
    double joint7_velocity = NULL_DOUBLE;
    string joint7_velocity_timestamp;
    double joint1_acceleration = NULL_DOUBLE;
    string joint1_acceleration_timestamp;
    double joint2_acceleration = NULL_DOUBLE;
    string joint2_acceleration_timestamp;
    double joint3_acceleration = NULL_DOUBLE;
    string joint3_acceleration_timestamp;
    double joint4_acceleration = NULL_DOUBLE;
    string joint4_acceleration_timestamp;
    double joint5_acceleration = NULL_DOUBLE;
    string joint5_acceleration_timestamp;
    double joint6_acceleration = NULL_DOUBLE;
    string joint6_acceleration_timestamp;
    double joint7_acceleration = NULL_DOUBLE;
    string joint7_acceleration_timestamp;
    double joint1_torque = NULL_DOUBLE;
    string joint1_torque_timestamp;
    double joint2_torque = NULL_DOUBLE;
    string joint2_torque_timestamp;
    double joint3_torque = NULL_DOUBLE;
    string joint3_torque_timestamp;
    double joint4_torque = NULL_DOUBLE;
    string joint4_torque_timestamp;
    double joint5_torque = NULL_DOUBLE;
    string joint5_torque_timestamp;
    double joint6_torque = NULL_DOUBLE;
    string joint6_torque_timestamp;
    double joint7_torque = NULL_DOUBLE;
    string joint7_torque_timestamp;
} left_arm_trajectory_t;

typedef struct right_arm_trajectory {
    uint32_t right_arm_trajectory_id = 0;
    uint32_t right_arm_position_id = NULL_UINT;
    uint32_t line_number = NULL_UINT;
    string line_number_timestamp;
    double delta_t = NULL_DOUBLE;
    string delta_t_timestamp;
    double joint1_position = NULL_DOUBLE;
    string joint1_position_timestamp;
    double joint2_position = NULL_DOUBLE;
    string joint2_position_timestamp;
    double joint3_position = NULL_DOUBLE;
    string joint3_position_timestamp;
    double joint4_position = NULL_DOUBLE;
    string joint4_position_timestamp;
    double joint5_position = NULL_DOUBLE;
    string joint5_position_timestamp;
    double joint6_position = NULL_DOUBLE;
    string joint6_position_timestamp;
    double joint7_position = NULL_DOUBLE;
    string joint7_position_timestamp;
    double joint1_velocity = NULL_DOUBLE;
    string joint1_velocity_timestamp;
    double joint2_velocity = NULL_DOUBLE;
    string joint2_velocity_timestamp;
    double joint3_velocity = NULL_DOUBLE;
    string joint3_velocity_timestamp;
    double joint4_velocity = NULL_DOUBLE;
    string joint4_velocity_timestamp;
    double joint5_velocity = NULL_DOUBLE;
    string joint5_velocity_timestamp;
    double joint6_velocity = NULL_DOUBLE;
    string joint6_velocity_timestamp;
    double joint7_velocity = NULL_DOUBLE;
    string joint7_velocity_timestamp;
    double joint1_acceleration = NULL_DOUBLE;
    string joint1_acceleration_timestamp;
    double joint2_acceleration = NULL_DOUBLE;
    string joint2_acceleration_timestamp;
    double joint3_acceleration = NULL_DOUBLE;
    string joint3_acceleration_timestamp;
    double joint4_acceleration = NULL_DOUBLE;
    string joint4_acceleration_timestamp;
    double joint5_acceleration = NULL_DOUBLE;
    string joint5_acceleration_timestamp;
    double joint6_acceleration = NULL_DOUBLE;
    string joint6_acceleration_timestamp;
    double joint7_acceleration = NULL_DOUBLE;
    string joint7_acceleration_timestamp;
    double joint1_torque = NULL_DOUBLE;
    string joint1_torque_timestamp;
    double joint2_torque = NULL_DOUBLE;
    string joint2_torque_timestamp;
    double joint3_torque = NULL_DOUBLE;
    string joint3_torque_timestamp;
    double joint4_torque = NULL_DOUBLE;
    string joint4_torque_timestamp;
    double joint5_torque = NULL_DOUBLE;
    string joint5_torque_timestamp;
    double joint6_torque = NULL_DOUBLE;
    string joint6_torque_timestamp;
    double joint7_torque = NULL_DOUBLE;
    string joint7_torque_timestamp;
} right_arm_trajectory_t;

typedef struct left_gripper_state {
    uint32_t left_gripper_state_id = 0;
    nlohmann::json matrix_from_fsr_upper_left;
    string matrix_from_fsr_upper_left_timestamp;
    nlohmann::json matrix_from_fsr_lower_left;
    string matrix_from_fsr_lower_left_timestamp;
    nlohmann::json matrix_from_fsr_upper_right;
    string matrix_from_fsr_upper_right_timestamp;
    nlohmann::json matrix_from_fsr_lower_right;
    string matrix_from_fsr_lower_right_timestamp;
    double servo_1_position;
    string servo_1_position_timestamp;
    double servo_2_position;
    string servo_2_position_timestamp;
    double current_force;
    string current_force_timestamp;
    string gripper_type;
    string gripper_type_timestamp;
    uint32_t item_element_id = NULL_UINT;
    string item_element_id_timestamp;
} left_gripper_state_t;

typedef struct right_gripper_state {
    uint32_t right_gripper_state_id = 0;
    nlohmann::json matrix_from_fsr_upper_left;
    string matrix_from_fsr_upper_left_timestamp;
    nlohmann::json matrix_from_fsr_lower_left;
    string matrix_from_fsr_lower_left_timestamp;
    nlohmann::json matrix_from_fsr_upper_right;
    string matrix_from_fsr_upper_right_timestamp;
    nlohmann::json matrix_from_fsr_lower_right;
    string matrix_from_fsr_lower_right_timestamp;
    double servo_1_position;
    string servo_1_position_timestamp;
    double servo_2_position;
    string servo_2_position_timestamp;
    double current_force;
    string current_force_timestamp;
    string gripper_type;
    string gripper_type_timestamp;
    uint32_t item_element_id = NULL_UINT;
    string item_element_id_timestamp;
} right_gripper_state_t;

typedef struct left_gripper_goal {
    uint32_t left_gripper_goal_id = 0;
    double target_force;
    string target_force_timestamp;
    double servo_1_goal_position;
    string servo_1_goal_position_timestamp;
    double servo_2_goal_position;
    string servo_2_goal_position_timestamp;
} left_gripper_goal_t;

typedef struct right_gripper_goal {
    uint32_t right_gripper_goal_id = 0;
    double target_force;
    string target_force_timestamp;
    double servo_1_goal_position;
    string servo_1_goal_position_timestamp;
    double servo_2_goal_position;
    string servo_2_goal_position_timestamp;
} right_gripper_goal_t;

typedef struct left_grasp {
    uint32_t left_grasp_id = 0;
    uint32_t item_id = NULL_UINT;
    string item_id_timestamp;
    nlohmann::json item_ids {};
    string item_ids_timestamp;
    nlohmann::json grasp_position {};
    string grasp_position_timestamp;
    nlohmann::json grasp_orientation {};
    string grasp_orientation_timestamp;
    double grasp_width = NULL_DOUBLE;
    string grasp_width_timestamp;
    string purpose_policy {};
    string purpose_policy_timestamp;
    double desired_force = NULL_DOUBLE;
    string desired_force_timestamp;
} left_grasp_t;

typedef struct right_grasp {
    uint32_t right_grasp_id = 0;
    uint32_t item_id = NULL_UINT;
    string item_id_timestamp;
    nlohmann::json item_ids {};
    string item_ids_timestamp;
    nlohmann::json grasp_position {};
    string grasp_position_timestamp;
    nlohmann::json grasp_orientation {};
    string grasp_orientation_timestamp;
    double grasp_width = NULL_DOUBLE;
    string grasp_width_timestamp;
    string purpose_policy {};
    string purpose_policy_timestamp;
    double desired_force = NULL_DOUBLE;
    string desired_force_timestamp;
} right_grasp_t;

typedef struct left_grasp_quality {
    uint32_t left_grasp_quality_id = 0;
    double result;
    string result_timestamp;
    nlohmann::json improvment_vector;
    string improvment_vector_timestamp;
} left_grasp_quality_t;

typedef struct right_grasp_quality {
    uint32_t right_grasp_quality_id = 0;
    double result;
    string result_timestamp;
    nlohmann::json improvment_vector;
    string improvment_vector_timestamp;
} right_grasp_quality_t;

typedef struct log {
    uint32_t log_id = 0;
    uint32_t log_level = 0;
    string log_timestamp {};
    string log_name {};
    string log_msg {};
    string log_file {};
    string log_function {};
    uint32_t log_line = 0;
} log_t;

#endif // MYSQL_CONNECTOR_STRUCTS_H
