#include <iomanip>

#include "mysql_connector.h"

// insert(), update(), delete() ---> executeUpdate()
// select()                     ---> executeQuery()

MysqlConnector::MysqlConnector(string hostname, string port, string schema_name, string username, string password, bool debug):
    hostname(hostname), port(port), schema_name(schema_name), username(username), password(password), debug(debug)
{
    is_connected = false;
    this->connect();
}

MysqlConnector::~MysqlConnector() {
    disconnect();
}

bool MysqlConnector::isConnected() 
{
    return is_connected;
}

nlohmann::json MysqlConnector::convertStringToJSON(std::string content)  {
    if (content.length() > 0) {
        return nlohmann::json::parse(content);
    }
    return nullptr;
}

void MysqlConnector::check_connection() {
    this->queries_++;

    // cout << "Curent count of queries " << (unsigned int) this->queries_ << endl;

    if (this->queries_ >= this->queries_limits_) {
        this->disconnect();
        cout << "Reached limit of queries, reconnecting" << endl;
    }

    this->connect();
}

void MysqlConnector::connect() 
{
    if (!isConnected()) {
        try {
            auto begin = std::chrono::high_resolution_clock::now();
            driver = get_driver_instance();
            connection = driver->connect(hostname + ":" + port, username, password);

            connection->setSchema(schema_name);

            is_connected = true; 
            auto end = std::chrono::high_resolution_clock::now();

            if (debug) std::cout << "connect () [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

        }
        catch (sql::SQLException &e)
        {
            /*
            The JDBC API throws three different exceptions:
            - sql::MethodNotImplementedException (derived from sql::SQLException)
            - sql::InvalidArgumentException (derived from sql::SQLException)
            - sql::SQLException (derived from std::runtime_error)
            */
            cout << "# ERR: SQLException in " << __FILE__ << "(" << "function connect" << ") on line " << __LINE__ << endl;
            /* Use what() (derived from std::runtime_error) to fetch the error message */
            cout << "# ERR: " << e.what() << " (MySQL error code: " << e.getErrorCode() << ", SQLState: " << e.getSQLState() << " )" << endl;

            is_connected = false; 

            throw network_exception();
        }
    }
}

bool MysqlConnector::disconnect()
{
    try
    {
        this->connection->close();
        delete this->connection;
        
        this->driver->threadEnd();

        this->queries_ = 0;
        this->is_connected = false;

        return true;
    }
    catch(sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__ << "(" << "function disconnect" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what() << " (MySQL error code: " << e.getErrorCode() << ", SQLState: " << e.getSQLState() << " )" << endl;
        throw network_exception();

    }
    catch (std::exception& e)
    {
        std::cout << "Exception caught : " << e.what() << std::endl;
    }
    return true;

}

/**
 * Function returns list of ids, every row in the selected table
 *
 * @param table_name name of selected table
 * @return vector contains list od private keys every row inselected table
 */
vector<uint32_t> MysqlConnector::getRowsId(string table_name) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    vector <uint32_t> ids = {};

    std::string queryBase = "SELECT " + table_name + "_id FROM " + table_name + " ORDER BY " + table_name + "_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);

        while (res->next()) {
            ids.push_back(res->getInt(table_name + "_id"));
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

    return ids;
}

/**
 * Function returns data of selected row, if exists, from table label
 *
 * @param label_data pointer to struct of type label_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getLabel(label_t * label_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM label WHERE label_id = " + to_string(row_id) + " ORDER BY label_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table label");
        } else {
            while(res->next()){
                label_data->label_id = res->getInt("label_id");
                label_data->label = res->getString("label");
                label_data->fit_method = res->getString("fit_method");
                label_data->fit_method_parameters = convertStringToJSON(res->getString("fit_method_parameters").c_str());
                label_data->item = res->getBoolean("item");
                label_data->element = res->getBoolean("element");
                label_data->components = convertStringToJSON(res->getString("components").c_str());
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table camera_parameter
 *
 * @param camera_parameter_data pointer to struct of type camera_parameter_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getCameraParameter(camera_parameter_t * camera_parameter_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM camera_parameter WHERE camera_parameter_id = " + to_string(row_id) + " ORDER BY camera_parameter_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table camera_parameter");
        } else {
            while(res->next()){
                camera_parameter_data->camera_parameter_id = res->getInt("camera_parameter_id");
                camera_parameter_data->camera_name = res->getString("camera_name");
                camera_parameter_data->camera_name_timestamp = res->getString("camera_name_timestamp");
                camera_parameter_data->position = convertStringToJSON(res->getString("position").c_str());
                camera_parameter_data->position_timestamp = res->getString("position_timestamp");
                camera_parameter_data->orientation = convertStringToJSON(res->getString("orientation").c_str());
                camera_parameter_data->orientation_timestamp = res->getString("orientation_timestamp");
                camera_parameter_data->parameters = convertStringToJSON(res->getString("parameters").c_str());
                camera_parameter_data->parameters_timestamp = res->getString("parameters_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table area
 *
 * @param area_data pointer to struct of type camera_parameter_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getArea(area_t * area_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM area WHERE area_id = " + to_string(row_id) + " ORDER BY area_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table area");
        } else {
            while(res->next()){
                area_data->area_id = res->getInt("area_id");
                area_data->area_name = res->getString("area_name");
                area_data->area_name_timestamp = res->getString("area_name_timestamp");
                area_data->position = convertStringToJSON(res->getString("position").c_str());
                area_data->position_timestamp = res->getString("position_timestamp");
                area_data->orientation = convertStringToJSON(res->getString("orientation").c_str());
                area_data->orientation_timestamp = res->getString("orientation_timestamp");
                area_data->shape = convertStringToJSON(res->getString("shape").c_str());
                area_data->shape_timestamp = res->getString("shape_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table scene
 *
 * @param change_detect_data pointer to struct of type change_detect_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getChangeDetect(change_detect_t *change_detect_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM change_detect WHERE change_detect_id = " + to_string(row_id) + " ORDER BY change_detect_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table change_detect");
        } else {
            while(res->next()){
                change_detect_data->change_detect_id = res->getInt("change_detect_id");

                if (res->getBlob("camera_1_rgb")->peek() != -1) {
                    change_detect_data->camera_1_rgb = std::make_shared <iostream> (res->getBlob("camera_1_rgb")->rdbuf());
                    change_detect_data->camera_1_rgb_timestamp = res->getString("camera_1_rgb_timestamp");
                }
                if (res->getBlob("camera_1_depth")->peek() != -1) {
                    change_detect_data->camera_1_depth = std::make_shared <iostream> (res->getBlob("camera_1_depth")->rdbuf());
                    change_detect_data->camera_1_depth_timestamp = res->getString("camera_1_depth_timestamp");
                }
                if (res->getBlob("camera_2_rgb")->peek() != -1) {
                    change_detect_data->camera_2_rgb = std::make_shared <iostream> (res->getBlob("camera_2_rgb")->rdbuf());
                    change_detect_data->camera_2_rgb_timestamp = res->getString("camera_2_rgb_timestamp");
                }
                if (res->getBlob("camera_2_depth")->peek() != -1) {
                    change_detect_data->camera_2_depth = std::make_shared <iostream> (res->getBlob("camera_2_depth")->rdbuf());
                    change_detect_data->camera_2_depth_timestamp = res->getString("camera_2_depth_timestamp");
                }
                if (res->getBlob("pcl_camera_1")->peek() != -1) {
                    change_detect_data->pcl_camera_1 = std::make_shared <iostream> (res->getBlob("pcl_camera_1")->rdbuf());
                    change_detect_data->pcl_camera_1_timestamp = res->getString("pcl_camera_1_timestamp");
                }
                if (res->getBlob("pcl_camera_2")->peek() != -1) {
                    change_detect_data->pcl_camera_2 = std::make_shared <iostream> (res->getBlob("pcl_camera_2")->rdbuf());
                    change_detect_data->pcl_camera_2_timestamp = res->getString("pcl_camera_2_timestamp");
                }
                if (res->getString("robot_transforms").length() > 0) {
                    change_detect_data->robot_transforms = convertStringToJSON(res->getString("robot_transforms").c_str());
                    change_detect_data->robot_transforms_timestamp = res->getString("robot_transforms_timestamp");
                }
                if (res->getBlob("pcl_camera_1_transformed")->peek() != -1) {
                    change_detect_data->pcl_camera_1_transformed = std::make_shared <iostream> (res->getBlob("pcl_camera_1_transformed")->rdbuf());
                    change_detect_data->pcl_camera_1_transformed_timestamp = res->getString("pcl_camera_1_transformed_timestamp");
                }
                if (res->getBlob("pcl_camera_2_transformed")->peek() != -1) {
                    change_detect_data->pcl_camera_2_transformed = std::make_shared <iostream> (res->getBlob("pcl_camera_2_transformed")->rdbuf());
                    change_detect_data->pcl_camera_2_transformed_timestamp = res->getString("pcl_camera_2_transformed_timestamp");
                }
                if (res->getBlob("merged_pcl")->peek() != -1) {
                    change_detect_data->merged_pcl = std::make_shared <iostream> (res->getBlob("merged_pcl")->rdbuf());
                    change_detect_data->merged_pcl_timestamp = res->getString("merged_pcl_timestamp");
                }
                if (res->getBlob("merged_pcl_filtered")->peek() != -1) {
                    change_detect_data->merged_pcl_filtered = std::make_shared <iostream> (res->getBlob("merged_pcl_filtered")->rdbuf());
                    change_detect_data->merged_pcl_filtered_timestamp = res->getString("merged_pcl_filtered_timestamp");
                }
                change_detect_data->scene_change_percentage = res->getDouble("scene_change_percentage");
                change_detect_data->scene_change_percentage_timestamp = res->getString("scene_change_percentage_timestamp");
                change_detect_data->tools_area = res->getInt("tools_area");
                change_detect_data->tools_area_timestamp = res->getString("tools_area_timestamp");
                change_detect_data->items_area = res->getInt("items_area");
                change_detect_data->items_area_timestamp = res->getString("items_area_timestamp");
                change_detect_data->security_area = res->getInt("security_area");
                change_detect_data->security_area_timestamp = res->getString("security_area_timestamp");
                change_detect_data->operating_area = res->getInt("operating_area");
                change_detect_data->operating_area_timestamp = res->getString("operating_area_timestamp");
                if (res->getBlob("pcl_diff")->peek() != -1) {
                    change_detect_data->pcl_diff = std::make_shared <iostream> (res->getBlob("pcl_diff")->rdbuf());
                    change_detect_data->pcl_diff_timestamp = res->getString("pcl_diff_timestamp");
                }

            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table scene
 *
 * @param scene_data pointer to struct of type scene_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getScene(scene_t * scene_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM scene WHERE scene_id = " + to_string(row_id) + " ORDER BY scene_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table scene");
        } else {
            while(res->next()){
                scene_data->scene_id = res->getInt("scene_id");
                if (res->getBlob("camera_1_rgb")->peek() != -1) {
                    scene_data->camera_1_rgb = std::make_shared <iostream> (res->getBlob("camera_1_rgb")->rdbuf());
                    scene_data->camera_1_rgb_timestamp = res->getString("camera_1_rgb_timestamp");
                }
                if (res->getBlob("camera_1_depth")->peek() != -1) {
                    scene_data->camera_1_depth = std::make_shared <iostream> (res->getBlob("camera_1_depth")->rdbuf());
                    scene_data->camera_1_depth_timestamp = res->getString("camera_1_depth_timestamp");
                }
                if (res->getBlob("camera_2_rgb")->peek() != -1) {
                    scene_data->camera_2_rgb = std::make_shared <iostream> (res->getBlob("camera_2_rgb")->rdbuf());
                    scene_data->camera_2_rgb_timestamp = res->getString("camera_2_rgb_timestamp");
                }
                if (res->getBlob("camera_2_depth")->peek() != -1) {
                    scene_data->camera_2_depth = std::make_shared <iostream> (res->getBlob("camera_2_depth")->rdbuf());
                    scene_data->camera_2_depth_timestamp = res->getString("camera_2_depth_timestamp");
                }
                if (res->getBlob("annotated_camera_1")->peek() != -1) {
                    scene_data->annotated_camera_1 = std::make_shared <iostream> (res->getBlob("annotated_camera_1")->rdbuf());
                    scene_data->annotated_camera_1_timestamp = res->getString("annotated_camera_1_timestamp");
                }
                if (res->getBlob("annotated_camera_2")->peek() != -1) {
                    scene_data->annotated_camera_2 = std::make_shared <iostream> (res->getBlob("annotated_camera_2")->rdbuf());
                    scene_data->annotated_camera_2_timestamp = res->getString("annotated_camera_2_timestamp");
                }
                if (res->getBlob("pcl_camera_1")->peek() != -1) {
                    scene_data->pcl_camera_1 = std::make_shared <iostream> (res->getBlob("pcl_camera_1")->rdbuf());
                    scene_data->pcl_camera_1_timestamp = res->getString("pcl_camera_1_timestamp");
                }
                if (res->getBlob("pcl_camera_2")->peek() != -1) {
                    scene_data->pcl_camera_2 = std::make_shared <iostream> (res->getBlob("pcl_camera_2")->rdbuf());
                    scene_data->pcl_camera_2_timestamp = res->getString("pcl_camera_2_timestamp");
                }
                if (res->getBlob("merged_pcl")->peek() != -1) {
                    scene_data->merged_pcl = std::make_shared <iostream> (res->getBlob("merged_pcl")->rdbuf());
                    scene_data->merged_pcl_timestamp = res->getString("merged_pcl_timestamp");
                }
                if (res->getBlob("occupancy_grid")->peek() != -1) {
                    scene_data->occupancy_grid = std::make_shared <iostream> (res->getBlob("occupancy_grid")->rdbuf());
                    scene_data->occupancy_grid_timestamp = res->getString("occupancy_grid_timestamp");
                }
                if (res->getBlob("pcl_scene_diff")->peek() != -1) {
                    scene_data->pcl_scene_diff = std::make_shared <iostream> (res->getBlob("pcl_scene_diff")->rdbuf());
                    scene_data->pcl_scene_diff_timestamp = res->getString("pcl_scene_diff_timestamp");
                }
                if (res->getString("scene_diff_info").length() > 0) {
                    scene_data->scene_diff_info = convertStringToJSON(res->getString("scene_diff_info").c_str());
                    scene_data->scene_diff_info_timestamp = res->getString("scene_diff_info_timestamp");
                }
                if (res->getBlob("filtered_octomap")->peek() != -1) {
                    scene_data->filtered_octomap = std::make_shared <iostream> (res->getBlob("filtered_octomap")->rdbuf());
                    scene_data->filtered_octomap_timestamp = res->getString("filtered_octomap_timestamp");
                }
                if (res->getBlob("pcl_self_filtered")->peek() != -1) {
                    scene_data->pcl_self_filtered = std::make_shared <iostream> (res->getBlob("pcl_self_filtered")->rdbuf());
                    scene_data->pcl_self_filtered_timestamp = res->getString("pcl_self_filtered_timestamp");
                }
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table item_cam1
 *
 * @param item_cam1_data pointer to struct of type item_cam1_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getItemCam1(item_cam1_t * item_cam1_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM item_cam1 WHERE item_cam1_id = " + to_string(row_id) + " ORDER BY item_cam1_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table item_cam1");
        } else {
            while(res->next()){
                item_cam1_data->item_cam1_id = res->getInt("item_cam1_id");
                item_cam1_data->mask = std::make_shared <iostream> (res->getBlob("mask")->rdbuf());
                item_cam1_data->mask_timestamp = res->getString("mask_timestamp");
                item_cam1_data->accuracy = res->getDouble("accuracy");
                item_cam1_data->accuracy_timestamp = res->getString("accuracy_timestamp");
                item_cam1_data->label_id = res->getInt("label_id");
                item_cam1_data->label_id_timestamp = res->getString("label_id_timestamp");
                item_cam1_data->depth_data = std::make_shared <iostream> (res->getBlob("depth_data")->rdbuf());
                item_cam1_data->depth_data_timestamp = res->getString("depth_data_timestamp");
                item_cam1_data->pcl_data = std::make_shared <iostream> (res->getBlob("pcl_data")->rdbuf());
                item_cam1_data->pcl_data_timestamp = res->getString("pcl_data_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table item_cam2
 *
 * @param item_cam2_data pointer to struct of type item_cam2_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getItemCam2(item_cam2_t * item_cam2_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM item_cam2 WHERE item_cam2_id = " + to_string(row_id) + " ORDER BY item_cam2_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table item_cam2");
        } else {
            while(res->next()){
                item_cam2_data->item_cam2_id = res->getInt("item_cam2_id");
                item_cam2_data->mask = std::make_shared <iostream> (res->getBlob("mask")->rdbuf());
                item_cam2_data->mask_timestamp = res->getString("mask_timestamp");
                item_cam2_data->accuracy = res->getDouble("accuracy");
                item_cam2_data->accuracy_timestamp = res->getString("accuracy_timestamp");
                item_cam2_data->label_id = res->getInt("label_id");
                item_cam2_data->label_id_timestamp = res->getString("label_id_timestamp");
                item_cam2_data->depth_data = std::make_shared <iostream> (res->getBlob("depth_data")->rdbuf());
                item_cam2_data->depth_data_timestamp = res->getString("depth_data_timestamp");
                item_cam2_data->pcl_data = std::make_shared <iostream> (res->getBlob("pcl_data")->rdbuf());
                item_cam2_data->pcl_data_timestamp = res->getString("pcl_data_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table item
 *
 * @param item_data pointer to struct of type item_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getItem(item_t * item_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM item WHERE item_id = " + to_string(row_id) + " ORDER BY item_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table item");
        } else {
            while(res->next()){
                item_data->item_id = res->getInt("item_id");
                item_data->id = res->getInt("id");
                item_data->item_cam1_id = res->getInt("item_cam1_id");
                item_data->item_cam2_id = res->getInt("item_cam2_id");
                item_data->item_id_hash = res->getString("item_id_hash");
                item_data->item_id_hash_timestamp = res->getString("item_id_hash_timestamp");
                item_data->label = res->getString("label");
                item_data->label_timestamp = res->getString("label_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table item_element
 *
 * @param item_element_data pointer to struct of type item_element_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getItemElement(item_element_t * item_element_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM item_element WHERE item_element_id = " + to_string(row_id) + " ORDER BY item_element_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table item_element");
        } else {
            while(res->next()){
                item_element_data->item_element_id = res->getInt("item_element_id");
                if (res->getInt("item_id") > 0) item_element_data->item_id = res->getInt("item_id");
                if (res->getInt("id") > 0) item_element_data->id = res->getInt("id");
                if (res->getString("element_label").length() > 0) {
                    item_element_data->element_label = res->getString("element_label");
                    item_element_data->element_label_timestamp = res->getString("element_label_timestamp");
                }
                if (res->getBlob("pcl_merged")->peek() != -1) {
                    item_element_data->pcl_merged = std::make_shared <iostream> (res->getBlob("pcl_merged")->rdbuf());
                    item_element_data->pcl_merged_timestamp = res->getString("pcl_merged_timestamp");
                }
                if (res->getBlob("element_mask_1")->peek() != -1) {
                    item_element_data->element_mask_1 = std::make_shared <iostream> (res->getBlob("element_mask_1")->rdbuf());
                    item_element_data->element_mask_1_timestamp = res->getString("element_mask_1_timestamp");
                }
                if (res->getBlob("element_mask_2")->peek() != -1) {
                    item_element_data->element_mask_2 = std::make_shared <iostream> (res->getBlob("element_mask_2")->rdbuf());
                    item_element_data->element_mask_2_timestamp = res->getString("element_mask_2_timestamp");
                }
                if (res->getBlob("element_depth_1")->peek() != -1) {
                    item_element_data->element_depth_1 = std::make_shared <iostream> (res->getBlob("element_depth_1")->rdbuf());
                    item_element_data->element_depth_1_timestamp = res->getString("element_depth_1_timestamp");
                }
                if (res->getBlob("element_depth_2")->peek() != -1) {
                    item_element_data->element_depth_2 = std::make_shared <iostream> (res->getBlob("element_depth_2")->rdbuf());
                    item_element_data->element_depth_2_timestamp = res->getString("element_depth_2_timestamp");
                }
                if (res->getBlob("element_pcl_1")->peek() != -1) {
                    item_element_data->element_pcl_1 = std::make_shared <iostream> (res->getBlob("element_pcl_1")->rdbuf());
                    item_element_data->element_pcl_1_timestamp = res->getString("element_pcl_1_timestamp");
                }
                if (res->getBlob("element_pcl_2")->peek() != -1) {
                    item_element_data->element_pcl_2 = std::make_shared <iostream> (res->getBlob("element_pcl_2")->rdbuf());
                    item_element_data->element_pcl_2_timestamp = res->getString("element_pcl_2_timestamp");
                }
                if (res->getString("primitive_shape").length() > 0) {
                    item_element_data->primitive_shape = convertStringToJSON(res->getString("primitive_shape").c_str());
                    item_element_data->primitive_shape_timestamp = res->getString("primitive_shape_timestamp");
                }
                if (res->getString("gt_position").length() > 0) {
                    item_element_data->gt_position = convertStringToJSON(res->getString("gt_position").c_str());
                    item_element_data->gt_position_timestamp = res->getString("gt_position_timestamp");
                }
                if (res->getString("gt_orientation").length() > 0) {
                    item_element_data->gt_orientation = convertStringToJSON(res->getString("gt_orientation").c_str());
                    item_element_data->gt_orientation_timestamp = res->getString("gt_orientation_timestamp");
                }
                if (res->getString("position").length() > 0) {
                    item_element_data->position = convertStringToJSON(res->getString("position").c_str());
                    item_element_data->position_timestamp = res->getString("position_timestamp");
                }
                if (res->getString("orientation").length() > 0) {
                    item_element_data->orientation = convertStringToJSON(res->getString("orientation").c_str());
                    item_element_data->orientation_timestamp = res->getString("orientation_timestamp");
                }
                if (res->getString("estimation_error").length() > 0) {
                    item_element_data->estimation_error = convertStringToJSON(res->getString("estimation_error").c_str());
                    item_element_data->estimation_error_timestamp = res->getString("estimation_error_timestamp");
                }
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table left_arm_state
 *
 * @param left_arm_state_data pointer to struct of type left_arm_state_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getLeftArmState(left_arm_state_t *left_arm_state_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM left_arm_state WHERE left_arm_state_id = " + to_string(row_id) + " ORDER BY left_arm_state_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_arm_state");
        } else {
            while(res->next()){
                left_arm_state_data->left_arm_state_id = res->getInt("left_arm_state_id");
                left_arm_state_data->joint1_position = res->getDouble("joint1_position");
                left_arm_state_data->joint1_position_timestamp = res->getString("joint1_position_timestamp");
                left_arm_state_data->joint2_position = res->getDouble("joint2_position");
                left_arm_state_data->joint2_position_timestamp = res->getString("joint2_position_timestamp");
                left_arm_state_data->joint3_position = res->getDouble("joint3_position");
                left_arm_state_data->joint3_position_timestamp = res->getString("joint3_position_timestamp");
                left_arm_state_data->joint4_position = res->getDouble("joint4_position");
                left_arm_state_data->joint4_position_timestamp = res->getString("joint4_position_timestamp");
                left_arm_state_data->joint5_position = res->getDouble("joint5_position");
                left_arm_state_data->joint5_position_timestamp = res->getString("joint5_position_timestamp");
                left_arm_state_data->joint6_position = res->getDouble("joint6_position");
                left_arm_state_data->joint6_position_timestamp = res->getString("joint6_position_timestamp");
                left_arm_state_data->joint7_position = res->getDouble("joint7_position");
                left_arm_state_data->joint7_position_timestamp = res->getString("joint7_position_timestamp");
                left_arm_state_data->joint1_temperature = res->getDouble("joint1_temperature");
                left_arm_state_data->joint1_temperature_timestamp = res->getString("joint1_temperature_timestamp");
                left_arm_state_data->joint2_temperature = res->getDouble("joint2_temperature");
                left_arm_state_data->joint2_temperature_timestamp = res->getString("joint2_temperature_timestamp");
                left_arm_state_data->joint3_temperature = res->getDouble("joint3_temperature");
                left_arm_state_data->joint3_temperature_timestamp = res->getString("joint3_temperature_timestamp");
                left_arm_state_data->joint4_temperature = res->getDouble("joint4_temperature");
                left_arm_state_data->joint4_temperature_timestamp = res->getString("joint4_temperature_timestamp");
                left_arm_state_data->joint5_temperature = res->getDouble("joint5_temperature");
                left_arm_state_data->joint5_temperature_timestamp = res->getString("joint5_temperature_timestamp");
                left_arm_state_data->joint6_temperature = res->getDouble("joint6_temperature");
                left_arm_state_data->joint6_temperature_timestamp = res->getString("joint6_temperature_timestamp");
                left_arm_state_data->joint7_temperature = res->getDouble("joint7_temperature");
                left_arm_state_data->joint7_temperature_timestamp = res->getString("joint7_temperature_timestamp");
                left_arm_state_data->joint1_current = res->getDouble("joint1_current");
                left_arm_state_data->joint1_current_timestamp = res->getString("joint1_current_timestamp");
                left_arm_state_data->joint2_current = res->getDouble("joint2_current");
                left_arm_state_data->joint2_current_timestamp = res->getString("joint2_current_timestamp");
                left_arm_state_data->joint3_current = res->getDouble("joint3_current");
                left_arm_state_data->joint3_current_timestamp = res->getString("joint3_current_timestamp");
                left_arm_state_data->joint4_current = res->getDouble("joint4_current");
                left_arm_state_data->joint4_current_timestamp = res->getString("joint4_current_timestamp");
                left_arm_state_data->joint5_current = res->getDouble("joint5_current");
                left_arm_state_data->joint5_current_timestamp = res->getString("joint5_current_timestamp");
                left_arm_state_data->joint6_current = res->getDouble("joint6_current");
                left_arm_state_data->joint6_current_timestamp = res->getString("joint6_current_timestamp");
                left_arm_state_data->joint7_current = res->getDouble("joint7_current");
                left_arm_state_data->joint7_current_timestamp = res->getString("joint7_current_timestamp");
                left_arm_state_data->joint1_torque = res->getDouble("joint1_torque");
                left_arm_state_data->joint1_torque_timestamp = res->getString("joint1_torque_timestamp");
                left_arm_state_data->joint2_torque = res->getDouble("joint2_torque");
                left_arm_state_data->joint2_torque_timestamp = res->getString("joint2_torque_timestamp");
                left_arm_state_data->joint3_torque = res->getDouble("joint3_torque");
                left_arm_state_data->joint3_torque_timestamp = res->getString("joint3_torque_timestamp");
                left_arm_state_data->joint4_torque = res->getDouble("joint4_torque");
                left_arm_state_data->joint4_torque_timestamp = res->getString("joint4_torque_timestamp");
                left_arm_state_data->joint5_torque = res->getDouble("joint5_torque");
                left_arm_state_data->joint5_torque_timestamp = res->getString("joint5_torque_timestamp");
                left_arm_state_data->joint6_torque = res->getDouble("joint6_torque");
                left_arm_state_data->joint6_torque_timestamp = res->getString("joint6_torque_timestamp");
                left_arm_state_data->joint7_torque = res->getDouble("joint7_torque");
                left_arm_state_data->joint7_torque_timestamp = res->getString("joint7_torque_timestamp");
                left_arm_state_data->joint1_state = res->getString("joint1_state");
                left_arm_state_data->joint1_state_timestamp = res->getString("joint1_state_timestamp");
                left_arm_state_data->joint2_state = res->getString("joint2_state");
                left_arm_state_data->joint2_state_timestamp = res->getString("joint2_state_timestamp");
                left_arm_state_data->joint3_state = res->getString("joint3_state");
                left_arm_state_data->joint3_state_timestamp = res->getString("joint3_state_timestamp");
                left_arm_state_data->joint4_state = res->getString("joint4_state");
                left_arm_state_data->joint4_state_timestamp = res->getString("joint4_state_timestamp");
                left_arm_state_data->joint5_state = res->getString("joint5_state");
                left_arm_state_data->joint5_state_timestamp = res->getString("joint5_state_timestamp");
                left_arm_state_data->joint6_state = res->getString("joint6_state");
                left_arm_state_data->joint6_state_timestamp = res->getString("joint6_state_timestamp");
                left_arm_state_data->joint7_state = res->getString("joint7_state");
                left_arm_state_data->joint7_state_timestamp = res->getString("joint7_state_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table right_arm_state
 *
 * @param right_arm_state_data pointer to struct of type right_arm_state_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getRightArmState(right_arm_state_t *right_arm_state_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM right_arm_state WHERE right_arm_state_id = " + to_string(row_id) + " ORDER BY right_arm_state_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table right_arm_state");
        } else {
            while(res->next()){
                right_arm_state_data->right_arm_state_id = res->getInt("right_arm_state_id");
                right_arm_state_data->joint1_position = res->getDouble("joint1_position");
                right_arm_state_data->joint1_position_timestamp = res->getString("joint1_position_timestamp");
                right_arm_state_data->joint2_position = res->getDouble("joint2_position");
                right_arm_state_data->joint2_position_timestamp = res->getString("joint2_position_timestamp");
                right_arm_state_data->joint3_position = res->getDouble("joint3_position");
                right_arm_state_data->joint3_position_timestamp = res->getString("joint3_position_timestamp");
                right_arm_state_data->joint4_position = res->getDouble("joint4_position");
                right_arm_state_data->joint4_position_timestamp = res->getString("joint4_position_timestamp");
                right_arm_state_data->joint5_position = res->getDouble("joint5_position");
                right_arm_state_data->joint5_position_timestamp = res->getString("joint5_position_timestamp");
                right_arm_state_data->joint6_position = res->getDouble("joint6_position");
                right_arm_state_data->joint6_position_timestamp = res->getString("joint6_position_timestamp");
                right_arm_state_data->joint7_position = res->getDouble("joint7_position");
                right_arm_state_data->joint7_position_timestamp = res->getString("joint7_position_timestamp");
                right_arm_state_data->joint1_temperature = res->getDouble("joint1_temperature");
                right_arm_state_data->joint1_temperature_timestamp = res->getString("joint1_temperature_timestamp");
                right_arm_state_data->joint2_temperature = res->getDouble("joint2_temperature");
                right_arm_state_data->joint2_temperature_timestamp = res->getString("joint2_temperature_timestamp");
                right_arm_state_data->joint3_temperature = res->getDouble("joint3_temperature");
                right_arm_state_data->joint3_temperature_timestamp = res->getString("joint3_temperature_timestamp");
                right_arm_state_data->joint4_temperature = res->getDouble("joint4_temperature");
                right_arm_state_data->joint4_temperature_timestamp = res->getString("joint4_temperature_timestamp");
                right_arm_state_data->joint5_temperature = res->getDouble("joint5_temperature");
                right_arm_state_data->joint5_temperature_timestamp = res->getString("joint5_temperature_timestamp");
                right_arm_state_data->joint6_temperature = res->getDouble("joint6_temperature");
                right_arm_state_data->joint6_temperature_timestamp = res->getString("joint6_temperature_timestamp");
                right_arm_state_data->joint7_temperature = res->getDouble("joint7_temperature");
                right_arm_state_data->joint7_temperature_timestamp = res->getString("joint7_temperature_timestamp");
                right_arm_state_data->joint1_current = res->getDouble("joint1_current");
                right_arm_state_data->joint1_current_timestamp = res->getString("joint1_current_timestamp");
                right_arm_state_data->joint2_current = res->getDouble("joint2_current");
                right_arm_state_data->joint2_current_timestamp = res->getString("joint2_current_timestamp");
                right_arm_state_data->joint3_current = res->getDouble("joint3_current");
                right_arm_state_data->joint3_current_timestamp = res->getString("joint3_current_timestamp");
                right_arm_state_data->joint4_current = res->getDouble("joint4_current");
                right_arm_state_data->joint4_current_timestamp = res->getString("joint4_current_timestamp");
                right_arm_state_data->joint5_current = res->getDouble("joint5_current");
                right_arm_state_data->joint5_current_timestamp = res->getString("joint5_current_timestamp");
                right_arm_state_data->joint6_current = res->getDouble("joint6_current");
                right_arm_state_data->joint6_current_timestamp = res->getString("joint6_current_timestamp");
                right_arm_state_data->joint7_current = res->getDouble("joint7_current");
                right_arm_state_data->joint7_current_timestamp = res->getString("joint7_current_timestamp");
                right_arm_state_data->joint1_torque = res->getDouble("joint1_torque");
                right_arm_state_data->joint1_torque_timestamp = res->getString("joint1_torque_timestamp");
                right_arm_state_data->joint2_torque = res->getDouble("joint2_torque");
                right_arm_state_data->joint2_torque_timestamp = res->getString("joint2_torque_timestamp");
                right_arm_state_data->joint3_torque = res->getDouble("joint3_torque");
                right_arm_state_data->joint3_torque_timestamp = res->getString("joint3_torque_timestamp");
                right_arm_state_data->joint4_torque = res->getDouble("joint4_torque");
                right_arm_state_data->joint4_torque_timestamp = res->getString("joint4_torque_timestamp");
                right_arm_state_data->joint5_torque = res->getDouble("joint5_torque");
                right_arm_state_data->joint5_torque_timestamp = res->getString("joint5_torque_timestamp");
                right_arm_state_data->joint6_torque = res->getDouble("joint6_torque");
                right_arm_state_data->joint6_torque_timestamp = res->getString("joint6_torque_timestamp");
                right_arm_state_data->joint7_torque = res->getDouble("joint7_torque");
                right_arm_state_data->joint7_torque_timestamp = res->getString("joint7_torque_timestamp");
                right_arm_state_data->joint1_state = res->getString("joint1_state");
                right_arm_state_data->joint1_state_timestamp = res->getString("joint1_state_timestamp");
                right_arm_state_data->joint2_state = res->getString("joint2_state");
                right_arm_state_data->joint2_state_timestamp = res->getString("joint2_state_timestamp");
                right_arm_state_data->joint3_state = res->getString("joint3_state");
                right_arm_state_data->joint3_state_timestamp = res->getString("joint3_state_timestamp");
                right_arm_state_data->joint4_state = res->getString("joint4_state");
                right_arm_state_data->joint4_state_timestamp = res->getString("joint4_state_timestamp");
                right_arm_state_data->joint5_state = res->getString("joint5_state");
                right_arm_state_data->joint5_state_timestamp = res->getString("joint5_state_timestamp");
                right_arm_state_data->joint6_state = res->getString("joint6_state");
                right_arm_state_data->joint6_state_timestamp = res->getString("joint6_state_timestamp");
                right_arm_state_data->joint7_state = res->getString("joint7_state");
                right_arm_state_data->joint7_state_timestamp = res->getString("joint7_state_timestamp");            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table left_arm_goal
 *
 * @param left_arm_goal_data pointer to struct of type left_arm_goal_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getLeftArmGoal(left_arm_goal_t * left_arm_goal_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM left_arm_goal WHERE left_arm_goal_id = " + to_string(row_id) + " ORDER BY left_arm_goal_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_arm_goal");
        } else {
            while(res->next()){
                left_arm_goal_data->left_arm_goal_id = res->getInt("left_arm_goal_id");
                left_arm_goal_data->joint1_torque = res->getDouble("joint1_torque");
                left_arm_goal_data->joint1_torque_timestamp = res->getString("joint1_torque_timestamp");
                left_arm_goal_data->joint2_torque = res->getDouble("joint2_torque");
                left_arm_goal_data->joint2_torque_timestamp = res->getString("joint2_torque_timestamp");
                left_arm_goal_data->joint3_torque = res->getDouble("joint3_torque");
                left_arm_goal_data->joint3_torque_timestamp = res->getString("joint3_torque_timestamp");
                left_arm_goal_data->joint4_torque = res->getDouble("joint4_torque");
                left_arm_goal_data->joint4_torque_timestamp = res->getString("joint4_torque_timestamp");
                left_arm_goal_data->joint5_torque = res->getDouble("joint5_torque");
                left_arm_goal_data->joint5_torque_timestamp = res->getString("joint5_torque_timestamp");
                left_arm_goal_data->joint6_torque = res->getDouble("joint6_torque");
                left_arm_goal_data->joint6_torque_timestamp = res->getString("joint6_torque_timestamp");
                left_arm_goal_data->joint7_torque = res->getDouble("joint7_torque");
                left_arm_goal_data->joint7_torque_timestamp = res->getString("joint7_torque_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table right_arm_goal
 *
 * @param right_arm_goal_data pointer to struct of type right_arm_goal_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getRightArmGoal(right_arm_goal_t * right_arm_goal_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM right_arm_goal WHERE right_arm_goal_id = " + to_string(row_id) + " ORDER BY right_arm_goal_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table right_arm_goal");
        } else {
            while(res->next()){
                right_arm_goal_data->right_arm_goal_id = res->getInt("right_arm_goal_id");
                right_arm_goal_data->joint1_torque = res->getDouble("joint1_torque");
                right_arm_goal_data->joint1_torque_timestamp = res->getString("joint1_torque_timestamp");
                right_arm_goal_data->joint2_torque = res->getDouble("joint2_torque");
                right_arm_goal_data->joint2_torque_timestamp = res->getString("joint2_torque_timestamp");
                right_arm_goal_data->joint3_torque = res->getDouble("joint3_torque");
                right_arm_goal_data->joint3_torque_timestamp = res->getString("joint3_torque_timestamp");
                right_arm_goal_data->joint4_torque = res->getDouble("joint4_torque");
                right_arm_goal_data->joint4_torque_timestamp = res->getString("joint4_torque_timestamp");
                right_arm_goal_data->joint5_torque = res->getDouble("joint5_torque");
                right_arm_goal_data->joint5_torque_timestamp = res->getString("joint5_torque_timestamp");
                right_arm_goal_data->joint6_torque = res->getDouble("joint6_torque");
                right_arm_goal_data->joint6_torque_timestamp = res->getString("joint6_torque_timestamp");
                right_arm_goal_data->joint7_torque = res->getDouble("joint7_torque");
                right_arm_goal_data->joint7_torque_timestamp = res->getString("joint7_torque_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table left_arm_position
 *
 * @param left_arm_position_data pointer to struct of type left_arm_position_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getLeftArmPosition(left_arm_position_t * left_arm_position_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM left_arm_position WHERE left_arm_position_id = " + to_string(row_id) + " ORDER BY left_arm_position_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_arm_position");
        } else {
            while(res->next()){
                left_arm_position_data->left_arm_position_id = res->getInt("left_arm_position_id");
                left_arm_position_data->position = convertStringToJSON(res->getString("position").c_str());
                left_arm_position_data->position_timestamp = res->getString("position_timestamp");
                left_arm_position_data->orientation = convertStringToJSON(res->getString("orientation").c_str());
                left_arm_position_data->orientation_timestamp = res->getString("orientation_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table right_arm_position
 *
 * @param right_arm_position_data pointer to struct of type right_arm_position_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getRightArmPosition(right_arm_position_t * right_arm_position_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM right_arm_position WHERE right_arm_position_id = " + to_string(row_id) + " ORDER BY right_arm_position_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table right_arm_position");
        } else {
            while(res->next()){
                right_arm_position_data->right_arm_position_id = res->getInt("right_arm_position_id");
                right_arm_position_data->position = convertStringToJSON(res->getString("position").c_str());
                right_arm_position_data->position_timestamp = res->getString("position_timestamp");
                right_arm_position_data->orientation = convertStringToJSON(res->getString("orientation").c_str());
                right_arm_position_data->orientation_timestamp = res->getString("orientation_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table left_arm_path
 *
 * @param left_arm_path_data pointer to struct of type left_arm_path_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getLeftArmPath(left_arm_path_t * left_arm_path_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM left_arm_path WHERE left_arm_path_id = " + to_string(row_id) + " ORDER BY left_arm_path_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_arm_path");
        } else {
            while(res->next()){
                left_arm_path_data->left_arm_path_id = res->getInt("left_arm_path_id");
                left_arm_path_data->left_arm_position_id = res->getInt("left_arm_position_id");
                left_arm_path_data->line_number = res->getInt("line_number");
                left_arm_path_data->line_number_timestamp = res->getString("line_number_timestamp");
                left_arm_path_data->joint1_position = res->getDouble("joint1_position");
                left_arm_path_data->joint1_position_timestamp = res->getString("joint1_position_timestamp");
                left_arm_path_data->joint2_position = res->getDouble("joint2_position");
                left_arm_path_data->joint2_position_timestamp = res->getString("joint2_position_timestamp");
                left_arm_path_data->joint3_position = res->getDouble("joint3_position");
                left_arm_path_data->joint3_position_timestamp = res->getString("joint3_position_timestamp");
                left_arm_path_data->joint4_position = res->getDouble("joint4_position");
                left_arm_path_data->joint4_position_timestamp = res->getString("joint4_position_timestamp");
                left_arm_path_data->joint5_position = res->getDouble("joint5_position");
                left_arm_path_data->joint5_position_timestamp = res->getString("joint5_position_timestamp");
                left_arm_path_data->joint6_position = res->getDouble("joint6_position");
                left_arm_path_data->joint6_position_timestamp = res->getString("joint6_position_timestamp");
                left_arm_path_data->joint7_position = res->getDouble("joint7_position");
                left_arm_path_data->joint7_position_timestamp = res->getString("joint7_position_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table right_arm_path
 *
 * @param right_arm_path_data pointer to struct of type right_arm_path_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getRightArmPath(right_arm_path_t * right_arm_path_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM right_arm_path WHERE right_arm_path_id = " + to_string(row_id) + " ORDER BY right_arm_path_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table right_arm_path");
        } else {
            while(res->next()){
                right_arm_path_data->right_arm_path_id = res->getInt("right_arm_path_id");
                right_arm_path_data->right_arm_position_id = res->getInt("right_arm_position_id");
                right_arm_path_data->line_number = res->getInt("line_number");
                right_arm_path_data->line_number_timestamp = res->getString("line_number_timestamp");
                right_arm_path_data->joint1_position = res->getDouble("joint1_position");
                right_arm_path_data->joint1_position_timestamp = res->getString("joint1_position_timestamp");
                right_arm_path_data->joint2_position = res->getDouble("joint2_position");
                right_arm_path_data->joint2_position_timestamp = res->getString("joint2_position_timestamp");
                right_arm_path_data->joint3_position = res->getDouble("joint3_position");
                right_arm_path_data->joint3_position_timestamp = res->getString("joint3_position_timestamp");
                right_arm_path_data->joint4_position = res->getDouble("joint4_position");
                right_arm_path_data->joint4_position_timestamp = res->getString("joint4_position_timestamp");
                right_arm_path_data->joint5_position = res->getDouble("joint5_position");
                right_arm_path_data->joint5_position_timestamp = res->getString("joint5_position_timestamp");
                right_arm_path_data->joint6_position = res->getDouble("joint6_position");
                right_arm_path_data->joint6_position_timestamp = res->getString("joint6_position_timestamp");
                right_arm_path_data->joint7_position = res->getDouble("joint7_position");
                right_arm_path_data->joint7_position_timestamp = res->getString("joint7_position_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table left_arm_trajectory
 *
 * @param left_arm_trajectory_data pointer to struct of type left_arm_trajectory_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getLeftArmTrajectory(left_arm_trajectory_t * left_arm_trajectory_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM left_arm_trajectory WHERE left_arm_trajectory_id = " + to_string(row_id) + " ORDER BY left_arm_trajectory_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_arm_trajectory");
        } else {
            while(res->next()){
                left_arm_trajectory_data->left_arm_trajectory_id = res->getInt("left_arm_trajectory_id");
                left_arm_trajectory_data->line_number = res->getInt("line_number");
                left_arm_trajectory_data->line_number_timestamp = res->getString("line_number_timestamp");
                left_arm_trajectory_data->delta_t = res->getDouble("delta_t");
                left_arm_trajectory_data->delta_t_timestamp = res->getString("delta_t_timestamp");
                left_arm_trajectory_data->joint1_position = res->getDouble("joint1_position");
                left_arm_trajectory_data->joint1_position_timestamp = res->getString("joint1_position_timestamp");
                left_arm_trajectory_data->joint2_position = res->getDouble("joint2_position");
                left_arm_trajectory_data->joint2_position_timestamp = res->getString("joint2_position_timestamp");
                left_arm_trajectory_data->joint3_position = res->getDouble("joint3_position");
                left_arm_trajectory_data->joint3_position_timestamp = res->getString("joint3_position_timestamp");
                left_arm_trajectory_data->joint4_position = res->getDouble("joint4_position");
                left_arm_trajectory_data->joint4_position_timestamp = res->getString("joint4_position_timestamp");
                left_arm_trajectory_data->joint5_position = res->getDouble("joint5_position");
                left_arm_trajectory_data->joint5_position_timestamp = res->getString("joint5_position_timestamp");
                left_arm_trajectory_data->joint6_position = res->getDouble("joint6_position");
                left_arm_trajectory_data->joint6_position_timestamp = res->getString("joint6_position_timestamp");
                left_arm_trajectory_data->joint7_position = res->getDouble("joint7_position");
                left_arm_trajectory_data->joint7_position_timestamp = res->getString("joint7_position_timestamp");
                left_arm_trajectory_data->joint1_velocity = res->getDouble("joint1_velocity");
                left_arm_trajectory_data->joint1_velocity_timestamp = res->getString("joint1_velocity_timestamp");
                left_arm_trajectory_data->joint2_velocity = res->getDouble("joint2_velocity");
                left_arm_trajectory_data->joint2_velocity_timestamp = res->getString("joint2_velocity_timestamp");
                left_arm_trajectory_data->joint3_velocity = res->getDouble("joint3_velocity");
                left_arm_trajectory_data->joint3_velocity_timestamp = res->getString("joint3_velocity_timestamp");
                left_arm_trajectory_data->joint4_velocity = res->getDouble("joint4_velocity");
                left_arm_trajectory_data->joint4_velocity_timestamp = res->getString("joint4_velocity_timestamp");
                left_arm_trajectory_data->joint5_velocity = res->getDouble("joint5_velocity");
                left_arm_trajectory_data->joint5_velocity_timestamp = res->getString("joint5_velocity_timestamp");
                left_arm_trajectory_data->joint6_velocity = res->getDouble("joint6_velocity");
                left_arm_trajectory_data->joint6_velocity_timestamp = res->getString("joint6_velocity_timestamp");
                left_arm_trajectory_data->joint7_velocity = res->getDouble("joint7_velocity");
                left_arm_trajectory_data->joint7_velocity_timestamp = res->getString("joint7_velocity_timestamp");
                left_arm_trajectory_data->joint1_acceleration = res->getDouble("joint1_acceleration");
                left_arm_trajectory_data->joint1_acceleration_timestamp = res->getString("joint1_acceleration_timestamp");
                left_arm_trajectory_data->joint2_acceleration = res->getDouble("joint2_acceleration");
                left_arm_trajectory_data->joint2_acceleration_timestamp = res->getString("joint2_acceleration_timestamp");
                left_arm_trajectory_data->joint3_acceleration = res->getDouble("joint3_acceleration");
                left_arm_trajectory_data->joint3_acceleration_timestamp = res->getString("joint3_acceleration_timestamp");
                left_arm_trajectory_data->joint4_acceleration = res->getDouble("joint4_acceleration");
                left_arm_trajectory_data->joint4_acceleration_timestamp = res->getString("joint4_acceleration_timestamp");
                left_arm_trajectory_data->joint5_acceleration = res->getDouble("joint5_acceleration");
                left_arm_trajectory_data->joint5_acceleration_timestamp = res->getString("joint5_acceleration_timestamp");
                left_arm_trajectory_data->joint6_acceleration = res->getDouble("joint6_acceleration");
                left_arm_trajectory_data->joint6_acceleration_timestamp = res->getString("joint6_acceleration_timestamp");
                left_arm_trajectory_data->joint7_acceleration = res->getDouble("joint7_acceleration");
                left_arm_trajectory_data->joint7_acceleration_timestamp = res->getString("joint7_acceleration_timestamp");
                left_arm_trajectory_data->joint1_torque = res->getDouble("joint1_torque");
                left_arm_trajectory_data->joint1_torque_timestamp = res->getString("joint1_torque_timestamp");
                left_arm_trajectory_data->joint2_torque = res->getDouble("joint2_torque");
                left_arm_trajectory_data->joint2_torque_timestamp = res->getString("joint2_torque_timestamp");
                left_arm_trajectory_data->joint3_torque = res->getDouble("joint3_torque");
                left_arm_trajectory_data->joint3_torque_timestamp = res->getString("joint3_torque_timestamp");
                left_arm_trajectory_data->joint4_torque = res->getDouble("joint4_torque");
                left_arm_trajectory_data->joint4_torque_timestamp = res->getString("joint4_torque_timestamp");
                left_arm_trajectory_data->joint5_torque = res->getDouble("joint5_torque");
                left_arm_trajectory_data->joint5_torque_timestamp = res->getString("joint5_torque_timestamp");
                left_arm_trajectory_data->joint6_torque = res->getDouble("joint6_torque");
                left_arm_trajectory_data->joint6_torque_timestamp = res->getString("joint6_torque_timestamp");
                left_arm_trajectory_data->joint7_torque = res->getDouble("joint7_torque");
                left_arm_trajectory_data->joint7_torque_timestamp = res->getString("joint7_torque_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

/**
 * Function returns data of selected row, if exists, from table right_arm_trajectory
 *
 * @param right_arm_trajectory_data pointer to struct of type right_arm_trajectory_t, which will be filled data from database
 * @param row_id private key of selected row
 */
void MysqlConnector::getRightArmTrajectory(right_arm_trajectory_t * right_arm_trajectory_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM right_arm_trajectory WHERE right_arm_trajectory_id = " + to_string(row_id) + " ORDER BY right_arm_trajectory_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table right_arm_trajectory");
        } else {
            while(res->next()){
                right_arm_trajectory_data->right_arm_trajectory_id = res->getInt("right_arm_trajectory_id");
                right_arm_trajectory_data->line_number = res->getInt("line_number");
                right_arm_trajectory_data->line_number_timestamp = res->getString("line_number_timestamp");
                right_arm_trajectory_data->delta_t = res->getDouble("delta_t");
                right_arm_trajectory_data->delta_t_timestamp = res->getString("delta_t_timestamp");
                right_arm_trajectory_data->joint1_position = res->getDouble("joint1_position");
                right_arm_trajectory_data->joint1_position_timestamp = res->getString("joint1_position_timestamp");
                right_arm_trajectory_data->joint2_position = res->getDouble("joint2_position");
                right_arm_trajectory_data->joint2_position_timestamp = res->getString("joint2_position_timestamp");
                right_arm_trajectory_data->joint3_position = res->getDouble("joint3_position");
                right_arm_trajectory_data->joint3_position_timestamp = res->getString("joint3_position_timestamp");
                right_arm_trajectory_data->joint4_position = res->getDouble("joint4_position");
                right_arm_trajectory_data->joint4_position_timestamp = res->getString("joint4_position_timestamp");
                right_arm_trajectory_data->joint5_position = res->getDouble("joint5_position");
                right_arm_trajectory_data->joint5_position_timestamp = res->getString("joint5_position_timestamp");
                right_arm_trajectory_data->joint6_position = res->getDouble("joint6_position");
                right_arm_trajectory_data->joint6_position_timestamp = res->getString("joint6_position_timestamp");
                right_arm_trajectory_data->joint7_position = res->getDouble("joint7_position");
                right_arm_trajectory_data->joint7_position_timestamp = res->getString("joint7_position_timestamp");
                right_arm_trajectory_data->joint1_velocity = res->getDouble("joint1_velocity");
                right_arm_trajectory_data->joint1_velocity_timestamp = res->getString("joint1_velocity_timestamp");
                right_arm_trajectory_data->joint2_velocity = res->getDouble("joint2_velocity");
                right_arm_trajectory_data->joint2_velocity_timestamp = res->getString("joint2_velocity_timestamp");
                right_arm_trajectory_data->joint3_velocity = res->getDouble("joint3_velocity");
                right_arm_trajectory_data->joint3_velocity_timestamp = res->getString("joint3_velocity_timestamp");
                right_arm_trajectory_data->joint4_velocity = res->getDouble("joint4_velocity");
                right_arm_trajectory_data->joint4_velocity_timestamp = res->getString("joint4_velocity_timestamp");
                right_arm_trajectory_data->joint5_velocity = res->getDouble("joint5_velocity");
                right_arm_trajectory_data->joint5_velocity_timestamp = res->getString("joint5_velocity_timestamp");
                right_arm_trajectory_data->joint6_velocity = res->getDouble("joint6_velocity");
                right_arm_trajectory_data->joint6_velocity_timestamp = res->getString("joint6_velocity_timestamp");
                right_arm_trajectory_data->joint7_velocity = res->getDouble("joint7_velocity");
                right_arm_trajectory_data->joint7_velocity_timestamp = res->getString("joint7_velocity_timestamp");
                right_arm_trajectory_data->joint1_acceleration = res->getDouble("joint1_acceleration");
                right_arm_trajectory_data->joint1_acceleration_timestamp = res->getString("joint1_acceleration_timestamp");
                right_arm_trajectory_data->joint2_acceleration = res->getDouble("joint2_acceleration");
                right_arm_trajectory_data->joint2_acceleration_timestamp = res->getString("joint2_acceleration_timestamp");
                right_arm_trajectory_data->joint3_acceleration = res->getDouble("joint3_acceleration");
                right_arm_trajectory_data->joint3_acceleration_timestamp = res->getString("joint3_acceleration_timestamp");
                right_arm_trajectory_data->joint4_acceleration = res->getDouble("joint4_acceleration");
                right_arm_trajectory_data->joint4_acceleration_timestamp = res->getString("joint4_acceleration_timestamp");
                right_arm_trajectory_data->joint5_acceleration = res->getDouble("joint5_acceleration");
                right_arm_trajectory_data->joint5_acceleration_timestamp = res->getString("joint5_acceleration_timestamp");
                right_arm_trajectory_data->joint6_acceleration = res->getDouble("joint6_acceleration");
                right_arm_trajectory_data->joint6_acceleration_timestamp = res->getString("joint6_acceleration_timestamp");
                right_arm_trajectory_data->joint7_acceleration = res->getDouble("joint7_acceleration");
                right_arm_trajectory_data->joint7_acceleration_timestamp = res->getString("joint7_acceleration_timestamp");
                right_arm_trajectory_data->joint1_torque = res->getDouble("joint1_torque");
                right_arm_trajectory_data->joint1_torque_timestamp = res->getString("joint1_torque_timestamp");
                right_arm_trajectory_data->joint2_torque = res->getDouble("joint2_torque");
                right_arm_trajectory_data->joint2_torque_timestamp = res->getString("joint2_torque_timestamp");
                right_arm_trajectory_data->joint3_torque = res->getDouble("joint3_torque");
                right_arm_trajectory_data->joint3_torque_timestamp = res->getString("joint3_torque_timestamp");
                right_arm_trajectory_data->joint4_torque = res->getDouble("joint4_torque");
                right_arm_trajectory_data->joint4_torque_timestamp = res->getString("joint4_torque_timestamp");
                right_arm_trajectory_data->joint5_torque = res->getDouble("joint5_torque");
                right_arm_trajectory_data->joint5_torque_timestamp = res->getString("joint5_torque_timestamp");
                right_arm_trajectory_data->joint6_torque = res->getDouble("joint6_torque");
                right_arm_trajectory_data->joint6_torque_timestamp = res->getString("joint6_torque_timestamp");
                right_arm_trajectory_data->joint7_torque = res->getDouble("joint7_torque");
                right_arm_trajectory_data->joint7_torque_timestamp = res->getString("joint7_torque_timestamp");            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}


void MysqlConnector::getLeftGripperState(left_gripper_state_t * left_gripper_state_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM left_gripper_state WHERE left_gripper_state_id = " + to_string(row_id) + " ORDER BY left_gripper_state_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_gripper_state");
        } else {
            while(res->next()){
                left_gripper_state_data->left_gripper_state_id = res->getInt("left_gripper_state_id");
                left_gripper_state_data->matrix_from_fsr_upper_left = convertStringToJSON(res->getString("matrix_from_fsr_upper_left").c_str());
                left_gripper_state_data->matrix_from_fsr_upper_left_timestamp = res->getString("matrix_from_fsr_upper_left_timestamp");
                left_gripper_state_data->matrix_from_fsr_lower_left = convertStringToJSON(res->getString("matrix_from_fsr_lower_left").c_str());
                left_gripper_state_data->matrix_from_fsr_lower_left_timestamp = res->getString("matrix_from_fsr_lower_left_timestamp");
                left_gripper_state_data->matrix_from_fsr_upper_right = convertStringToJSON(res->getString("matrix_from_fsr_upper_right").c_str());
                left_gripper_state_data->matrix_from_fsr_upper_right_timestamp = res->getString("matrix_from_fsr_upper_right_timestamp");
                left_gripper_state_data->matrix_from_fsr_lower_right = convertStringToJSON(res->getString("matrix_from_fsr_lower_right").c_str());
                left_gripper_state_data->matrix_from_fsr_lower_right_timestamp = res->getString("matrix_from_fsr_lower_right_timestamp");
                left_gripper_state_data->servo_1_position = res->getDouble("servo_1_position");
                left_gripper_state_data->servo_1_position_timestamp = res->getString("servo_1_position_timestamp");
                left_gripper_state_data->servo_2_position = res->getDouble("servo_2_position");
                left_gripper_state_data->servo_2_position_timestamp = res->getString("servo_2_position_timestamp");
                left_gripper_state_data->current_force = res->getDouble("current_force");
                left_gripper_state_data->current_force_timestamp = res->getString("current_force_timestamp");
                left_gripper_state_data->gripper_type = res->getString("gripper_type");
                left_gripper_state_data->gripper_type_timestamp = res->getString("gripper_type_timestamp");
                left_gripper_state_data->item_element_id = res->getInt64("item_element_id");
                left_gripper_state_data->item_element_id_timestamp = res->getString("item_element_id_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::getRightGripperState(right_gripper_state_t * right_gripper_state_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM right_gripper_state WHERE right_gripper_state_id = " + to_string(row_id) + " ORDER BY right_gripper_state_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table right_gripper_state");
        } else {
            while(res->next()){
                right_gripper_state_data->right_gripper_state_id = res->getInt("right_gripper_state_id");
                right_gripper_state_data->matrix_from_fsr_upper_left = convertStringToJSON(res->getString("matrix_from_fsr_upper_left").c_str());
                right_gripper_state_data->matrix_from_fsr_upper_left_timestamp = res->getString("matrix_from_fsr_upper_left_timestamp");
                right_gripper_state_data->matrix_from_fsr_lower_left = convertStringToJSON(res->getString("matrix_from_fsr_lower_left").c_str());
                right_gripper_state_data->matrix_from_fsr_lower_left_timestamp = res->getString("matrix_from_fsr_lower_left_timestamp");
                right_gripper_state_data->matrix_from_fsr_upper_right = convertStringToJSON(res->getString("matrix_from_fsr_upper_right").c_str());
                right_gripper_state_data->matrix_from_fsr_upper_right_timestamp = res->getString("matrix_from_fsr_upper_right_timestamp");
                right_gripper_state_data->matrix_from_fsr_lower_right = convertStringToJSON(res->getString("matrix_from_fsr_lower_right").c_str());
                right_gripper_state_data->matrix_from_fsr_lower_right_timestamp = res->getString("matrix_from_fsr_lower_right_timestamp");
                right_gripper_state_data->servo_1_position = res->getDouble("servo_1_position");
                right_gripper_state_data->servo_1_position_timestamp = res->getString("servo_1_position_timestamp");
                right_gripper_state_data->servo_2_position = res->getDouble("servo_2_position");
                right_gripper_state_data->servo_2_position_timestamp = res->getString("servo_2_position_timestamp");
                right_gripper_state_data->current_force = res->getDouble("current_force");
                right_gripper_state_data->current_force_timestamp = res->getString("current_force_timestamp");
                right_gripper_state_data->gripper_type = res->getString("gripper_type");
                right_gripper_state_data->gripper_type_timestamp = res->getString("gripper_type_timestamp");
                right_gripper_state_data->item_element_id = res->getInt64("item_element_id");
                right_gripper_state_data->item_element_id_timestamp = res->getString("item_element_id_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::getLeftGripperGoal(left_gripper_goal_t * left_gripper_goal_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM left_gripper_goal WHERE left_gripper_goal_id = " + to_string(row_id) + " ORDER BY left_gripper_goal_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_gripper_goal");
        } else {
            while(res->next()){
                left_gripper_goal_data->left_gripper_goal_id = res->getInt("left_gripper_goal_id");
                left_gripper_goal_data->servo_1_goal_position = res->getDouble("servo_1_goal_position");
                left_gripper_goal_data->servo_1_goal_position_timestamp = res->getString("servo_1_goal_position_timestamp");
                left_gripper_goal_data->servo_2_goal_position = res->getDouble("servo_2_goal_position");
                left_gripper_goal_data->servo_2_goal_position_timestamp = res->getString("servo_2_goal_position_timestamp");
                left_gripper_goal_data->target_force = res->getDouble("target_force");
                left_gripper_goal_data->target_force_timestamp = res->getString("target_force_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::getRightGripperGoal(right_gripper_goal_t * right_gripper_goal_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM right_gripper_goal WHERE right_gripper_goal_id = " + to_string(row_id) + " ORDER BY right_gripper_goal_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table right_gripper_goal");
        } else {
            while(res->next()){
                right_gripper_goal_data->right_gripper_goal_id = res->getInt("right_gripper_goal_id");
                right_gripper_goal_data->servo_1_goal_position = res->getDouble("servo_1_goal_position");
                right_gripper_goal_data->servo_1_goal_position_timestamp = res->getString("servo_1_goal_position_timestamp");
                right_gripper_goal_data->servo_2_goal_position = res->getDouble("servo_2_goal_position");
                right_gripper_goal_data->servo_2_goal_position_timestamp = res->getString("servo_2_goal_position_timestamp");
                right_gripper_goal_data->target_force = res->getDouble("target_force");
                right_gripper_goal_data->target_force_timestamp = res->getString("target_force_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::getLeftGrasp(left_grasp_t * left_grasp_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM left_grasp WHERE left_grasp_id = " + to_string(row_id) + " ORDER BY left_grasp_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_grasp");
        } else {
            while(res->next()){
                left_grasp_data->left_grasp_id = res->getInt("left_grasp_id");
                left_grasp_data->item_id = res->getInt("item_id");
                left_grasp_data->item_id_timestamp = res->getString("item_id_timestamp");
                left_grasp_data->item_ids = convertStringToJSON(res->getString("item_ids").c_str());
                left_grasp_data->item_ids_timestamp = res->getString("item_ids_timestamp");
                left_grasp_data->grasp_position = convertStringToJSON(res->getString("grasp_position").c_str());
                left_grasp_data->grasp_position_timestamp = res->getString("grasp_position_timestamp");
                left_grasp_data->grasp_orientation = convertStringToJSON(res->getString("grasp_orientation").c_str());
                left_grasp_data->grasp_orientation_timestamp = res->getString("grasp_orientation_timestamp");
                left_grasp_data->grasp_width = res->getDouble("grasp_width");
                left_grasp_data->grasp_width_timestamp = res->getString("grasp_width_timestamp");
                left_grasp_data->purpose_policy = res->getString("purpose_policy");
                left_grasp_data->purpose_policy_timestamp = res->getString("purpose_policy_timestamp");
                left_grasp_data->desired_force = res->getDouble("desired_force");
                left_grasp_data->desired_force_timestamp = res->getString("desired_force_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::getRightGrasp(right_grasp_t * right_grasp_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase = "SELECT * FROM right_grasp WHERE right_grasp_id = " + to_string(row_id) + " ORDER BY right_grasp_id";

    try {
        sql::Statement *stmt = connection->createStatement();
        sql::ResultSet  *res = stmt->executeQuery(queryBase);
        if (res->rowsCount() != 1) {
            throw std::invalid_argument("No row with ID = " + to_string(row_id) + " in table left_grasp");
        } else {
            while(res->next()){
                right_grasp_data->right_grasp_id = res->getInt("right_grasp_id");
                right_grasp_data->item_id = res->getInt("item_id");
                right_grasp_data->item_id_timestamp = res->getString("item_id_timestamp");
                right_grasp_data->item_ids = convertStringToJSON(res->getString("item_ids").c_str());
                right_grasp_data->item_ids_timestamp = res->getString("item_ids_timestamp");
                right_grasp_data->grasp_position = convertStringToJSON(res->getString("grasp_position").c_str());
                right_grasp_data->grasp_position_timestamp = res->getString("grasp_position_timestamp");
                right_grasp_data->grasp_orientation = convertStringToJSON(res->getString("grasp_orientation").c_str());
                right_grasp_data->grasp_orientation_timestamp = res->getString("grasp_orientation_timestamp");
                right_grasp_data->grasp_width = res->getDouble("grasp_width");
                right_grasp_data->grasp_width_timestamp = res->getString("grasp_width_timestamp");
                right_grasp_data->purpose_policy = res->getString("purpose_policy");
                right_grasp_data->purpose_policy_timestamp = res->getString("purpose_policy_timestamp");
                right_grasp_data->desired_force = res->getDouble("desired_force");
                right_grasp_data->desired_force_timestamp = res->getString("desired_force_timestamp");
            }
        }

        delete stmt;
        delete res;
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }
    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

string MysqlConnector::buildQuery(string table_name, string pk_column, std::map <std::string, std::string> fields_values, uint32_t row_id) {
    std::string queryBase;
    // creating INSER/UPDATE query
    if (row_id > 0) {
        bool pierwsze = true;
        // UPDATE
        queryBase = "UPDATE " + table_name + " SET ";

        for(auto it:fields_values) {
            if (pierwsze) {
                pierwsze = false;
                queryBase += it.first + " = " +  it.second;
            } else {
                queryBase += ", " + it.first + " = " +  it.second;
            }
        }

        queryBase += " WHERE " + pk_column + " = " + std::to_string(row_id);
    } else {
        bool pierwsze = true;
        // INSERT
        std::string queryBase2 = ") VALUES (";
        std::string queryBase3 = ")";
        queryBase = "INSERT INTO " + table_name + " (";

        for(auto it:fields_values) {
            if (pierwsze) {
                pierwsze = false;
                queryBase += it.first;
                queryBase2 += it.second;
            } else {
                queryBase += ", " + it.first;
                queryBase2 += ", " + it.second;
            }
        }

        queryBase += queryBase2;
        queryBase += queryBase3;

    }
    // std::cout << queryBase.c_str() << std::endl;

    return queryBase;
}

void MysqlConnector::addColumnToQuery(std::map <std::string, std::string> *column_map, string column_name, bool timestamp) {
    std::map <std::string, std::string> _map = *column_map;

    column_map->insert(pair<string, string> (column_name, "?"));
    if (timestamp) {
        column_map->insert(pair<string, string> (column_name + "_timestamp", "NOW(6)"));
    }
}

uint32_t MysqlConnector::getLastId() {
    stmt = connection->createStatement();
    sql::ResultSet *res = stmt->executeQuery("SELECT @@identity AS id");
    res->next();
    return res->getInt64("id");
}

void MysqlConnector::setLabel(label_t * label_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (!label_data->label.empty()) addColumnToQuery(&fields_values, "label", false);
    if (!label_data->fit_method.empty()) addColumnToQuery(&fields_values, "fit_method", false);
    if (label_data->fit_method_parameters != NULL) addColumnToQuery(&fields_values, "fit_method_parameters", false);
    if (label_data->item != NULL_BOOL) addColumnToQuery(&fields_values, "item", false);
    if (label_data->element != NULL_BOOL) addColumnToQuery(&fields_values, "element", false);
    if (label_data->components != NULL) addColumnToQuery(&fields_values, "components", false);

    std::string queryBase = buildQuery("label", "label_id", fields_values, label_data->label_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "label") {
            preparedStatement->setString(i, label_data->label); i++;
        } else if (it.first == "fit_method") {
            preparedStatement->setString(i, label_data->fit_method); i++;
        } else if (it.first == "fit_method_parameters") {
            preparedStatement->setString(i, label_data->fit_method_parameters.dump()); i++;
        } else if (it.first == "item") {
            preparedStatement->setBoolean(i, label_data->item); i++;
        } else if (it.first == "element") {
            preparedStatement->setBoolean(i, label_data->element); i++;
        } else if (it.first == "components") {
            preparedStatement->setString(i, label_data->components.dump()); i++;
        }
    }

    preparedStatement->execute();

    if (label_data->label_id == 0) {
        label_data->label_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::setCameraParameter(camera_parameter_t *camera_parameter_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (!camera_parameter_data->camera_name.empty()) addColumnToQuery(&fields_values, "camera_name", true);
    if (camera_parameter_data->position != NULL) addColumnToQuery(&fields_values, "position", true);
    if (camera_parameter_data->orientation != NULL) addColumnToQuery(&fields_values, "orientation", true);
    if (camera_parameter_data->parameters != NULL) addColumnToQuery(&fields_values, "parameters", true);

    std::string queryBase = buildQuery("camera_parameter", "camera_parameter_id", fields_values, camera_parameter_data->camera_parameter_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "camera_name") {
            preparedStatement->setString(i, camera_parameter_data->camera_name); i++;
        } else if (it.first == "position") {
            preparedStatement->setString(i, camera_parameter_data->position.dump()); i++;
        } else if (it.first == "orientation") {
            preparedStatement->setString(i, camera_parameter_data->orientation.dump()); i++;
        } else if (it.first == "parameters") {
            preparedStatement->setString(i, camera_parameter_data->parameters.dump()); i++;
        }
    }

    preparedStatement->execute();

    if (camera_parameter_data->camera_parameter_id == 0) {
        camera_parameter_data->camera_parameter_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::setArea(area_t *area_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (!area_data->area_name.empty()) addColumnToQuery(&fields_values, "area_name", true);
    if (area_data->position != NULL) addColumnToQuery(&fields_values, "position", true);
    if (area_data->orientation != NULL) addColumnToQuery(&fields_values, "orientation", true);
    if (area_data->shape != NULL) addColumnToQuery(&fields_values, "shape", true);

    std::string queryBase = buildQuery("area", "area_id", fields_values, area_data->area_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "area_name") {
            preparedStatement->setString(i, area_data->area_name); i++;
        } else if (it.first == "position") {
            preparedStatement->setString(i, area_data->position.dump()); i++;
        } else if (it.first == "orientation") {
            preparedStatement->setString(i, area_data->orientation.dump()); i++;
        } else if (it.first == "shape") {
            preparedStatement->setString(i, area_data->shape.dump()); i++;
        }
    }

    preparedStatement->execute();

    if (area_data->area_id == 0) {
        area_data->area_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::setChangeDetect(change_detect_t * change_detect_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (change_detect_data->camera_1_rgb != NULL) addColumnToQuery(&fields_values, "camera_1_rgb", true);
    if (change_detect_data->camera_1_depth != NULL) addColumnToQuery(&fields_values, "camera_1_depth", true);
    if (change_detect_data->camera_2_rgb != NULL) addColumnToQuery(&fields_values, "camera_2_rgb", true);
    if (change_detect_data->camera_2_depth != NULL) addColumnToQuery(&fields_values, "camera_2_depth", true);
    if (change_detect_data->pcl_camera_1 != NULL) addColumnToQuery(&fields_values, "pcl_camera_1", true);
    if (change_detect_data->pcl_camera_2 != NULL) addColumnToQuery(&fields_values, "pcl_camera_2", true);
    if (change_detect_data->robot_transforms != NULL) addColumnToQuery(&fields_values, "robot_transforms", true);
    if (change_detect_data->pcl_camera_1_transformed != NULL) addColumnToQuery(&fields_values, "pcl_camera_1_transformed", true);
    if (change_detect_data->pcl_camera_2_transformed != NULL) addColumnToQuery(&fields_values, "pcl_camera_2_transformed", true);
    if (change_detect_data->merged_pcl != NULL) addColumnToQuery(&fields_values, "merged_pcl", true);
    if (change_detect_data->merged_pcl_filtered != NULL) addColumnToQuery(&fields_values, "merged_pcl_filtered", true);
    if (change_detect_data->scene_change_percentage != NULL_DOUBLE) addColumnToQuery(&fields_values, "scene_change_percentage", true);
    if (change_detect_data->tools_area != NULL_UINT) addColumnToQuery(&fields_values, "tools_area", true);
    if (change_detect_data->items_area != NULL_UINT) addColumnToQuery(&fields_values, "items_area", true);
    if (change_detect_data->security_area != NULL_UINT) addColumnToQuery(&fields_values, "security_area", true);
    if (change_detect_data->operating_area != NULL_UINT) addColumnToQuery(&fields_values, "operating_area", true);
    if (change_detect_data->pcl_diff != NULL) addColumnToQuery(&fields_values, "pcl_diff", true);

    std::string queryBase = buildQuery("change_detect", "change_detect_id", fields_values, change_detect_data->change_detect_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "camera_1_rgb") {
            preparedStatement->setBlob(i, change_detect_data->camera_1_rgb.get()); i++;
        } else if (it.first == "camera_1_depth") {
            preparedStatement->setBlob(i, change_detect_data->camera_1_depth.get()); i++;
        } else if (it.first == "camera_2_rgb") {
            preparedStatement->setBlob(i, change_detect_data->camera_2_rgb.get()); i++;
        } else if (it.first == "camera_2_depth") {
            preparedStatement->setBlob(i, change_detect_data->camera_2_depth.get()); i++;
        } else if (it.first == "pcl_camera_1") {
            preparedStatement->setBlob(i, change_detect_data->pcl_camera_1.get()); i++;
        } else if (it.first == "pcl_camera_2") {
            preparedStatement->setBlob(i, change_detect_data->pcl_camera_2.get()); i++;
        } else if (it.first == "robot_transforms") {
            preparedStatement->setString(i, change_detect_data->robot_transforms.dump()); i++;
        } else if (it.first == "pcl_camera_1_transformed") {
            preparedStatement->setBlob(i, change_detect_data->pcl_camera_1_transformed.get()); i++;
        } else if (it.first == "pcl_camera_2_transformed") {
            preparedStatement->setBlob(i, change_detect_data->pcl_camera_2_transformed.get()); i++;
        } else if (it.first == "merged_pcl") {
            preparedStatement->setBlob(i, change_detect_data->merged_pcl.get()); i++;
        } else if (it.first == "merged_pcl_filtered") {
            preparedStatement->setBlob(i, change_detect_data->merged_pcl_filtered.get()); i++;
        } else if (it.first == "scene_change_percentage") {
            preparedStatement->setDouble(i, change_detect_data->scene_change_percentage); i++;
        } else if (it.first == "tools_area") {
            preparedStatement->setUInt(i, change_detect_data->tools_area); i++;
        } else if (it.first == "items_area") {
            preparedStatement->setUInt(i, change_detect_data->items_area); i++;
        } else if (it.first == "security_area") {
            preparedStatement->setUInt(i, change_detect_data->security_area); i++;
        } else if (it.first == "operating_area") {
            preparedStatement->setUInt(i, change_detect_data->operating_area); i++;
        } else if (it.first == "pcl_diff") {
            preparedStatement->setBlob(i, change_detect_data->pcl_diff.get()); i++;
        }
    }

    preparedStatement->execute();

    delete preparedStatement;

    if (change_detect_data->change_detect_id == 0) {
        change_detect_data->change_detect_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::setScene(scene_t * scene_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (scene_data->camera_1_rgb != NULL) addColumnToQuery(&fields_values, "camera_1_rgb", true);
    if (scene_data->camera_1_depth != NULL) addColumnToQuery(&fields_values, "camera_1_depth", true);
    if (scene_data->camera_2_rgb != NULL) addColumnToQuery(&fields_values, "camera_2_rgb", true);
    if (scene_data->camera_2_depth != NULL) addColumnToQuery(&fields_values, "camera_2_depth", true);
    if (scene_data->annotated_camera_1 != NULL) addColumnToQuery(&fields_values, "annotated_camera_1", true);
    if (scene_data->annotated_camera_2 != NULL) addColumnToQuery(&fields_values, "annotated_camera_2", true);
    if (scene_data->pcl_camera_1 != NULL) addColumnToQuery(&fields_values, "pcl_camera_1", true);
    if (scene_data->pcl_camera_2 != NULL) addColumnToQuery(&fields_values, "pcl_camera_2", true);
    if (scene_data->merged_pcl != NULL) addColumnToQuery(&fields_values, "merged_pcl", true);
    if (scene_data->occupancy_grid != NULL) addColumnToQuery(&fields_values, "occupancy_grid", true);
    if (scene_data->pcl_scene_diff != NULL) addColumnToQuery(&fields_values, "pcl_scene_diff", true);
    if (scene_data->scene_diff_info != NULL) addColumnToQuery(&fields_values, "scene_diff_info", true);
    if (scene_data->filtered_octomap != NULL) addColumnToQuery(&fields_values, "filtered_octomap", true);
    if (scene_data->pcl_self_filtered != NULL) addColumnToQuery(&fields_values, "pcl_self_filtered", true);

    std::string queryBase = buildQuery("scene", "scene_id", fields_values, scene_data->scene_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "camera_1_rgb") {
            preparedStatement->setBlob(i, scene_data->camera_1_rgb.get()); i++;
        } else if (it.first == "camera_1_depth") {
            preparedStatement->setBlob(i, scene_data->camera_1_depth.get()); i++;
        } else if (it.first == "camera_2_rgb") {
            preparedStatement->setBlob(i, scene_data->camera_2_rgb.get()); i++;
        } else if (it.first == "camera_2_depth") {
            preparedStatement->setBlob(i, scene_data->camera_2_depth.get()); i++;
        } else if (it.first == "annotated_camera_1") {
            preparedStatement->setBlob(i, scene_data->annotated_camera_1.get()); i++;
        } else if (it.first == "annotated_camera_2") {
            preparedStatement->setBlob(i, scene_data->annotated_camera_2.get()); i++;
        } else if (it.first == "pcl_camera_1") {
            preparedStatement->setBlob(i, scene_data->pcl_camera_1.get()); i++;
        } else if (it.first == "pcl_camera_2") {
            preparedStatement->setBlob(i, scene_data->pcl_camera_2.get()); i++;
        } else if (it.first == "merged_pcl") {
            preparedStatement->setBlob(i, scene_data->merged_pcl.get()); i++;
        } else if (it.first == "occupancy_grid") {
            preparedStatement->setBlob(i, scene_data->occupancy_grid.get()); i++;
        } else if (it.first == "pcl_scene_diff") {
            preparedStatement->setBlob(i, scene_data->pcl_scene_diff.get()); i++;
        } else if (it.first == "scene_diff_info") {
            preparedStatement->setString(i, scene_data->scene_diff_info.dump()); i++;
        } else if (it.first == "filtered_octomap") {
            preparedStatement->setBlob(i, scene_data->filtered_octomap.get()); i++;
        } else if (it.first == "pcl_self_filtered") {
            preparedStatement->setBlob(i, scene_data->pcl_self_filtered.get()); i++;
        }
    }

    preparedStatement->execute();

    if (scene_data->scene_id == 0) {
        scene_data->scene_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::setItemCam1(item_cam1_t *item_cam1_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (item_cam1_data->scene_id != NULL_UINT) addColumnToQuery(&fields_values, "scene_id");
    if (item_cam1_data->mask != NULL) addColumnToQuery(&fields_values, "mask", true);
    if (item_cam1_data->accuracy != NULL_DOUBLE) addColumnToQuery(&fields_values, "accuracy", true);
    if (item_cam1_data->label_id != NULL_UINT) addColumnToQuery(&fields_values, "label_id", true);
    if (item_cam1_data->depth_data != NULL) addColumnToQuery(&fields_values, "depth_data", true);
    if (item_cam1_data->pcl_data != NULL) addColumnToQuery(&fields_values, "pcl_data", true);

    std::string queryBase = buildQuery("item_cam1", "item_cam1_id", fields_values, item_cam1_data->item_cam1_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "scene_id") {
            preparedStatement->setUInt(i, item_cam1_data->scene_id); i++;
        } else if (it.first == "mask") {
            preparedStatement->setBlob(i, item_cam1_data->mask.get()); i++;
            // preparedStatement->setBlob(i, item_cam1_data->mask); i++;
        } else if (it.first == "accuracy") {
            preparedStatement->setDouble(i, item_cam1_data->accuracy); i++;
        } else if (it.first == "label_id") {
            preparedStatement->setInt64(i, item_cam1_data->label_id); i++;
        } else if (it.first == "depth_data") {
            preparedStatement->setBlob(i, item_cam1_data->depth_data.get()); i++;
        } else if (it.first == "pcl_data") {
            preparedStatement->setBlob(i, item_cam1_data->pcl_data.get()); i++;
        } 
    }

    preparedStatement->execute();

    if (item_cam1_data->item_cam1_id == 0) {
        item_cam1_data->item_cam1_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setItemCam2(item_cam2_t *item_cam2_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (item_cam2_data->scene_id != NULL_UINT) addColumnToQuery(&fields_values, "scene_id");
    if (item_cam2_data->mask != NULL) addColumnToQuery(&fields_values, "mask", true);
    if (item_cam2_data->accuracy != NULL_DOUBLE) addColumnToQuery(&fields_values, "accuracy", true);
    if (item_cam2_data->label_id != NULL_UINT) addColumnToQuery(&fields_values, "label_id", true);
    if (item_cam2_data->depth_data != NULL) addColumnToQuery(&fields_values, "depth_data", true);
    if (item_cam2_data->pcl_data != NULL) addColumnToQuery(&fields_values, "pcl_data", true);

    std::string queryBase = buildQuery("item_cam2", "item_cam2_id", fields_values, item_cam2_data->item_cam2_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "scene_id") {
            preparedStatement->setUInt(i, item_cam2_data->scene_id); i++;
        } else if (it.first == "mask") {
            preparedStatement->setBlob(i, item_cam2_data->mask.get()); i++;
            // preparedStatement->setBlob(i, item_cam2_data->mask); i++;
        } else if (it.first == "accuracy") {
            preparedStatement->setDouble(i, item_cam2_data->accuracy); i++;
        } else if (it.first == "label_id") {
            preparedStatement->setUInt(i, item_cam2_data->label_id); i++;
        } else if (it.first == "depth_data") {
            preparedStatement->setBlob(i, item_cam2_data->depth_data.get()); i++;
        } else if (it.first == "pcl_data") {
            preparedStatement->setBlob(i, item_cam2_data->pcl_data.get()); i++;
        } 
    }

    preparedStatement->execute();

    if (item_cam2_data->item_cam2_id == 0) {
        item_cam2_data->item_cam2_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setItem(item_t *item_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (item_data->id != NULL_UINT) addColumnToQuery(&fields_values, "id");
    if (item_data->item_cam1_id != NULL_UINT) addColumnToQuery(&fields_values, "item_cam1_id");
    if (item_data->item_cam2_id != NULL_UINT) addColumnToQuery(&fields_values, "item_cam2_id");
    if (!item_data->item_id_hash.empty()) addColumnToQuery(&fields_values, "item_id_hash", true);
    if (!item_data->label.empty()) addColumnToQuery(&fields_values, "label", true);

    std::string queryBase = buildQuery("item", "item_id", fields_values, item_data->item_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "id") {
            preparedStatement->setUInt(i, item_data->id); i++;
        } else if (it.first == "id") {
            preparedStatement->setUInt(i, item_data->item_cam1_id); i++;
        } else if (it.first == "item_cam2_id") {
            preparedStatement->setUInt(i, item_data->item_cam2_id); i++;
        } else if (it.first == "item_id_hash") {
            preparedStatement->setString(i, item_data->item_id_hash); i++;
        } else if (it.first == "label") {
            preparedStatement->setString(i, item_data->label); i++;
        } 
    }

    preparedStatement->execute();

    if (item_data->item_id == 0) {
        item_data->item_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setItemElement(item_element_t *item_element_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (item_element_data->id != NULL_UINT) addColumnToQuery(&fields_values, "id");
    if (item_element_data->item_id != NULL_UINT) addColumnToQuery(&fields_values, "item_id");
    if (!item_element_data->element_label.empty()) addColumnToQuery(&fields_values, "element_label", true);
    if (item_element_data->pcl_merged != NULL) addColumnToQuery(&fields_values, "pcl_merged", true);
    if (item_element_data->element_mask_1 != NULL) addColumnToQuery(&fields_values, "element_mask_1", true);
    if (item_element_data->element_mask_2 != NULL) addColumnToQuery(&fields_values, "element_mask_2", true);
    if (item_element_data->element_depth_1 != NULL) addColumnToQuery(&fields_values, "element_depth_1", true);
    if (item_element_data->element_depth_2 != NULL) addColumnToQuery(&fields_values, "element_depth_2", true);
    if (item_element_data->element_pcl_1 != NULL) addColumnToQuery(&fields_values, "element_pcl_1", true);
    if (item_element_data->element_pcl_2 != NULL) addColumnToQuery(&fields_values, "element_pcl_2", true);
    if (item_element_data->primitive_shape != NULL) addColumnToQuery(&fields_values, "primitive_shape", true);
    if (item_element_data->gt_position != NULL) addColumnToQuery(&fields_values, "gt_position", true);
    if (item_element_data->gt_orientation != NULL) addColumnToQuery(&fields_values, "gt_orientation", true);
    if (item_element_data->position != NULL) addColumnToQuery(&fields_values, "position", true);
    if (item_element_data->orientation != NULL) addColumnToQuery(&fields_values, "orientation", true);
    if (item_element_data->estimation_error != NULL) addColumnToQuery(&fields_values, "estimation_error", true);

    std::string queryBase = buildQuery("item_element", "item_element_id", fields_values, item_element_data->item_element_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "id") {
            preparedStatement->setUInt(i, item_element_data->id); i++;
        } else if (it.first == "item_id") {
            preparedStatement->setUInt(i, item_element_data->item_id); i++;
        } else if (it.first == "element_label") {
            preparedStatement->setString(i, item_element_data->element_label); i++;
        } else if (it.first == "pcl_merged") {
            preparedStatement->setBlob(i, item_element_data->pcl_merged.get()); i++;
        } else if (it.first == "element_mask_1") {
            preparedStatement->setBlob(i, item_element_data->element_mask_1.get()); i++;
        } else if (it.first == "element_mask_2") {
            preparedStatement->setBlob(i, item_element_data->element_mask_2.get()); i++;
        } else if (it.first == "element_depth_1") {
            preparedStatement->setBlob(i, item_element_data->element_depth_1.get()); i++;
        } else if (it.first == "element_depth_2") {
            preparedStatement->setBlob(i, item_element_data->element_depth_2.get()); i++;
        } else if (it.first == "element_pcl_1") {
            preparedStatement->setBlob(i, item_element_data->element_pcl_1.get()); i++;
        } else if (it.first == "element_pcl_2") {
            preparedStatement->setBlob(i, item_element_data->element_pcl_2.get()); i++;
        } else if (it.first == "primitive_shape") {
            preparedStatement->setString(i, item_element_data->primitive_shape.dump()); i++;
        } else if (it.first == "gt_position") {
            preparedStatement->setString(i, item_element_data->gt_position.dump()); i++;
        } else if (it.first == "gt_orientation") {
            preparedStatement->setString(i, item_element_data->gt_orientation.dump()); i++;
        } else if (it.first == "position") {
            preparedStatement->setString(i, item_element_data->position.dump()); i++;
        } else if (it.first == "orientation") {
            preparedStatement->setString(i, item_element_data->orientation.dump()); i++;
        } else if (it.first == "estimation_error") {
            preparedStatement->setString(i, item_element_data->estimation_error.dump()); i++;
        } 
    }

    preparedStatement->execute();

    if (item_element_data->item_element_id == 0) {
        item_element_data->item_element_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}


void MysqlConnector::setLeftArmState(left_arm_state_t *left_arm_state_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (left_arm_state_data->joint1_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_position", true);
    if (left_arm_state_data->joint2_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_position", true);
    if (left_arm_state_data->joint3_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_position", true);
    if (left_arm_state_data->joint4_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_position", true);
    if (left_arm_state_data->joint5_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_position", true);
    if (left_arm_state_data->joint6_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_position", true);
    if (left_arm_state_data->joint7_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_position", true);
    if (left_arm_state_data->joint1_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_temperature", true);
    if (left_arm_state_data->joint2_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_temperature", true);
    if (left_arm_state_data->joint3_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_temperature", true);
    if (left_arm_state_data->joint4_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_temperature", true);
    if (left_arm_state_data->joint5_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_temperature", true);
    if (left_arm_state_data->joint6_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_temperature", true);
    if (left_arm_state_data->joint7_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_temperature", true);
    if (left_arm_state_data->joint1_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_current", true);
    if (left_arm_state_data->joint2_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_current", true);
    if (left_arm_state_data->joint3_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_current", true);
    if (left_arm_state_data->joint4_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_current", true);
    if (left_arm_state_data->joint5_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_current", true);
    if (left_arm_state_data->joint6_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_current", true);
    if (left_arm_state_data->joint7_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_current", true);
    if (left_arm_state_data->joint1_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_torque", true);
    if (left_arm_state_data->joint2_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_torque", true);
    if (left_arm_state_data->joint3_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_torque", true);
    if (left_arm_state_data->joint4_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_torque", true);
    if (left_arm_state_data->joint5_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_torque", true);
    if (left_arm_state_data->joint6_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_torque", true);
    if (left_arm_state_data->joint7_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_torque", true);
    if (!left_arm_state_data->joint1_state.empty()) addColumnToQuery(&fields_values, "joint1_state", true);
    if (!left_arm_state_data->joint2_state.empty()) addColumnToQuery(&fields_values, "joint2_state", true);
    if (!left_arm_state_data->joint3_state.empty()) addColumnToQuery(&fields_values, "joint3_state", true);
    if (!left_arm_state_data->joint4_state.empty()) addColumnToQuery(&fields_values, "joint4_state", true);
    if (!left_arm_state_data->joint5_state.empty()) addColumnToQuery(&fields_values, "joint5_state", true);
    if (!left_arm_state_data->joint6_state.empty()) addColumnToQuery(&fields_values, "joint6_state", true);
    if (!left_arm_state_data->joint7_state.empty()) addColumnToQuery(&fields_values, "joint7_state", true);

    std::string queryBase = buildQuery("left_arm_state", "left_arm_state_id", fields_values, left_arm_state_data->left_arm_state_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "joint1_position") {
            preparedStatement->setDouble(i, left_arm_state_data->joint1_position); i++;
        } else if (it.first == "joint2_position") {
            preparedStatement->setDouble(i, left_arm_state_data->joint2_position); i++;
        } else if (it.first == "joint3_position") {
            preparedStatement->setDouble(i, left_arm_state_data->joint3_position); i++;
        } else if (it.first == "joint4_position") {
            preparedStatement->setDouble(i, left_arm_state_data->joint4_position); i++;
        } else if (it.first == "joint5_position") {
            preparedStatement->setDouble(i, left_arm_state_data->joint5_position); i++;
        } else if (it.first == "joint6_position") {
            preparedStatement->setDouble(i, left_arm_state_data->joint6_position); i++;
        } else if (it.first == "joint7_position") {
            preparedStatement->setDouble(i, left_arm_state_data->joint7_position); i++;
        } else if (it.first == "joint1_temperature") {
            preparedStatement->setDouble(i, left_arm_state_data->joint1_temperature); i++;
        } else if (it.first == "joint2_temperature") {
            preparedStatement->setDouble(i, left_arm_state_data->joint2_temperature); i++;
        } else if (it.first == "joint3_temperature") {
            preparedStatement->setDouble(i, left_arm_state_data->joint3_temperature); i++;
        } else if (it.first == "joint4_temperature") {
            preparedStatement->setDouble(i, left_arm_state_data->joint4_temperature); i++;
        } else if (it.first == "joint5_temperature") {
            preparedStatement->setDouble(i, left_arm_state_data->joint5_temperature); i++;
        } else if (it.first == "joint6_temperature") {
            preparedStatement->setDouble(i, left_arm_state_data->joint6_temperature); i++;
        } else if (it.first == "joint7_temperature") {
            preparedStatement->setDouble(i, left_arm_state_data->joint7_temperature); i++;
        } else if (it.first == "joint1_current") {
            preparedStatement->setDouble(i, left_arm_state_data->joint1_current); i++;
        } else if (it.first == "joint2_current") {
            preparedStatement->setDouble(i, left_arm_state_data->joint2_current); i++;
        } else if (it.first == "joint3_current") {
            preparedStatement->setDouble(i, left_arm_state_data->joint3_current); i++;
        } else if (it.first == "joint4_current") {
            preparedStatement->setDouble(i, left_arm_state_data->joint4_current); i++;
        } else if (it.first == "joint5_current") {
            preparedStatement->setDouble(i, left_arm_state_data->joint5_current); i++;
        } else if (it.first == "joint6_current") {
            preparedStatement->setDouble(i, left_arm_state_data->joint6_current); i++;
        } else if (it.first == "joint7_current") {
            preparedStatement->setDouble(i, left_arm_state_data->joint7_current); i++;
        } else if (it.first == "joint1_torque") {
            preparedStatement->setDouble(i, left_arm_state_data->joint1_torque); i++;
        } else if (it.first == "joint2_torque") {
            preparedStatement->setDouble(i, left_arm_state_data->joint2_torque); i++;
        } else if (it.first == "joint3_torque") {
            preparedStatement->setDouble(i, left_arm_state_data->joint3_torque); i++;
        } else if (it.first == "joint4_torque") {
            preparedStatement->setDouble(i, left_arm_state_data->joint4_torque); i++;
        } else if (it.first == "joint5_torque") {
            preparedStatement->setDouble(i, left_arm_state_data->joint5_torque); i++;
        } else if (it.first == "joint6_torque") {
            preparedStatement->setDouble(i, left_arm_state_data->joint6_torque); i++;
        } else if (it.first == "joint7_torque") {
            preparedStatement->setDouble(i, left_arm_state_data->joint7_torque); i++;
        } else if (it.first == "joint1_state") {
            preparedStatement->setString(i, left_arm_state_data->joint1_state); i++;
        } else if (it.first == "joint2_state") {
            preparedStatement->setString(i, left_arm_state_data->joint2_state); i++;
        } else if (it.first == "joint3_state") {
            preparedStatement->setString(i, left_arm_state_data->joint3_state); i++;
        } else if (it.first == "joint4_state") {
            preparedStatement->setString(i, left_arm_state_data->joint4_state); i++;
        } else if (it.first == "joint5_state") {
            preparedStatement->setString(i, left_arm_state_data->joint5_state); i++;
        } else if (it.first == "joint6_state") {
            preparedStatement->setString(i, left_arm_state_data->joint6_state); i++;
        } else if (it.first == "joint7_state") {
            preparedStatement->setString(i, left_arm_state_data->joint7_state); i++;
        }        
    }

    preparedStatement->execute();

    if (left_arm_state_data->left_arm_state_id == 0) {
        left_arm_state_data->left_arm_state_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}


void MysqlConnector::setRightArmState(right_arm_state_t *right_arm_state_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (right_arm_state_data->joint1_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_position", true);
    if (right_arm_state_data->joint2_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_position", true);
    if (right_arm_state_data->joint3_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_position", true);
    if (right_arm_state_data->joint4_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_position", true);
    if (right_arm_state_data->joint5_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_position", true);
    if (right_arm_state_data->joint6_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_position", true);
    if (right_arm_state_data->joint7_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_position", true);
    if (right_arm_state_data->joint1_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_temperature", true);
    if (right_arm_state_data->joint2_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_temperature", true);
    if (right_arm_state_data->joint3_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_temperature", true);
    if (right_arm_state_data->joint4_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_temperature", true);
    if (right_arm_state_data->joint5_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_temperature", true);
    if (right_arm_state_data->joint6_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_temperature", true);
    if (right_arm_state_data->joint7_temperature != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_temperature", true);
    if (right_arm_state_data->joint1_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_current", true);
    if (right_arm_state_data->joint2_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_current", true);
    if (right_arm_state_data->joint3_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_current", true);
    if (right_arm_state_data->joint4_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_current", true);
    if (right_arm_state_data->joint5_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_current", true);
    if (right_arm_state_data->joint6_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_current", true);
    if (right_arm_state_data->joint7_current != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_current", true);
    if (right_arm_state_data->joint1_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_torque", true);
    if (right_arm_state_data->joint2_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_torque", true);
    if (right_arm_state_data->joint3_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_torque", true);
    if (right_arm_state_data->joint4_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_torque", true);
    if (right_arm_state_data->joint5_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_torque", true);
    if (right_arm_state_data->joint6_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_torque", true);
    if (right_arm_state_data->joint7_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_torque", true);
    if (!right_arm_state_data->joint1_state.empty()) addColumnToQuery(&fields_values, "joint1_state", true);
    if (!right_arm_state_data->joint2_state.empty()) addColumnToQuery(&fields_values, "joint2_state", true);
    if (!right_arm_state_data->joint3_state.empty()) addColumnToQuery(&fields_values, "joint3_state", true);
    if (!right_arm_state_data->joint4_state.empty()) addColumnToQuery(&fields_values, "joint4_state", true);
    if (!right_arm_state_data->joint5_state.empty()) addColumnToQuery(&fields_values, "joint5_state", true);
    if (!right_arm_state_data->joint6_state.empty()) addColumnToQuery(&fields_values, "joint6_state", true);
    if (!right_arm_state_data->joint7_state.empty()) addColumnToQuery(&fields_values, "joint7_state", true);

    std::string queryBase = buildQuery("right_arm_state", "right_arm_state_id", fields_values, right_arm_state_data->right_arm_state_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "joint1_position") {
            preparedStatement->setDouble(i, right_arm_state_data->joint1_position); i++;
        } else if (it.first == "joint2_position") {
            preparedStatement->setDouble(i, right_arm_state_data->joint2_position); i++;
        } else if (it.first == "joint3_position") {
            preparedStatement->setDouble(i, right_arm_state_data->joint3_position); i++;
        } else if (it.first == "joint4_position") {
            preparedStatement->setDouble(i, right_arm_state_data->joint4_position); i++;
        } else if (it.first == "joint5_position") {
            preparedStatement->setDouble(i, right_arm_state_data->joint5_position); i++;
        } else if (it.first == "joint6_position") {
            preparedStatement->setDouble(i, right_arm_state_data->joint6_position); i++;
        } else if (it.first == "joint7_position") {
            preparedStatement->setDouble(i, right_arm_state_data->joint7_position); i++;
        } else if (it.first == "joint1_temperature") {
            preparedStatement->setDouble(i, right_arm_state_data->joint1_temperature); i++;
        } else if (it.first == "joint2_temperature") {
            preparedStatement->setDouble(i, right_arm_state_data->joint2_temperature); i++;
        } else if (it.first == "joint3_temperature") {
            preparedStatement->setDouble(i, right_arm_state_data->joint3_temperature); i++;
        } else if (it.first == "joint4_temperature") {
            preparedStatement->setDouble(i, right_arm_state_data->joint4_temperature); i++;
        } else if (it.first == "joint5_temperature") {
            preparedStatement->setDouble(i, right_arm_state_data->joint5_temperature); i++;
        } else if (it.first == "joint6_temperature") {
            preparedStatement->setDouble(i, right_arm_state_data->joint6_temperature); i++;
        } else if (it.first == "joint7_temperature") {
            preparedStatement->setDouble(i, right_arm_state_data->joint7_temperature); i++;
        } else if (it.first == "joint1_current") {
            preparedStatement->setDouble(i, right_arm_state_data->joint1_current); i++;
        } else if (it.first == "joint2_current") {
            preparedStatement->setDouble(i, right_arm_state_data->joint2_current); i++;
        } else if (it.first == "joint3_current") {
            preparedStatement->setDouble(i, right_arm_state_data->joint3_current); i++;
        } else if (it.first == "joint4_current") {
            preparedStatement->setDouble(i, right_arm_state_data->joint4_current); i++;
        } else if (it.first == "joint5_current") {
            preparedStatement->setDouble(i, right_arm_state_data->joint5_current); i++;
        } else if (it.first == "joint6_current") {
            preparedStatement->setDouble(i, right_arm_state_data->joint6_current); i++;
        } else if (it.first == "joint7_current") {
            preparedStatement->setDouble(i, right_arm_state_data->joint7_current); i++;
        } else if (it.first == "joint1_torque") {
            preparedStatement->setDouble(i, right_arm_state_data->joint1_torque); i++;
        } else if (it.first == "joint2_torque") {
            preparedStatement->setDouble(i, right_arm_state_data->joint2_torque); i++;
        } else if (it.first == "joint3_torque") {
            preparedStatement->setDouble(i, right_arm_state_data->joint3_torque); i++;
        } else if (it.first == "joint4_torque") {
            preparedStatement->setDouble(i, right_arm_state_data->joint4_torque); i++;
        } else if (it.first == "joint5_torque") {
            preparedStatement->setDouble(i, right_arm_state_data->joint5_torque); i++;
        } else if (it.first == "joint6_torque") {
            preparedStatement->setDouble(i, right_arm_state_data->joint6_torque); i++;
        } else if (it.first == "joint7_torque") {
            preparedStatement->setDouble(i, right_arm_state_data->joint7_torque); i++;
        } else if (it.first == "joint1_state") {
            preparedStatement->setString(i, right_arm_state_data->joint1_state); i++;
        } else if (it.first == "joint2_state") {
            preparedStatement->setString(i, right_arm_state_data->joint2_state); i++;
        } else if (it.first == "joint3_state") {
            preparedStatement->setString(i, right_arm_state_data->joint3_state); i++;
        } else if (it.first == "joint4_state") {
            preparedStatement->setString(i, right_arm_state_data->joint4_state); i++;
        } else if (it.first == "joint5_state") {
            preparedStatement->setString(i, right_arm_state_data->joint5_state); i++;
        } else if (it.first == "joint6_state") {
            preparedStatement->setString(i, right_arm_state_data->joint6_state); i++;
        } else if (it.first == "joint7_state") {
            preparedStatement->setString(i, right_arm_state_data->joint7_state); i++;
        }        
    }

    preparedStatement->execute();

    if (right_arm_state_data->right_arm_state_id == 0) {
        right_arm_state_data->right_arm_state_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setLeftArmGoal(left_arm_goal_t *left_arm_goal_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (left_arm_goal_data->joint1_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_torque", true);
    if (left_arm_goal_data->joint2_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_torque", true);
    if (left_arm_goal_data->joint3_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_torque", true);
    if (left_arm_goal_data->joint4_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_torque", true);
    if (left_arm_goal_data->joint5_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_torque", true);
    if (left_arm_goal_data->joint6_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_torque", true);
    if (left_arm_goal_data->joint7_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_torque", true);

    std::string queryBase = buildQuery("left_arm_goal", "left_arm_goal_id", fields_values, left_arm_goal_data->left_arm_goal_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "joint1_torque") {
            preparedStatement->setDouble(i, left_arm_goal_data->joint1_torque); i++;
        } else if (it.first == "joint2_torque") {
            preparedStatement->setDouble(i, left_arm_goal_data->joint2_torque); i++;
        } else if (it.first == "joint3_torque") {
            preparedStatement->setDouble(i, left_arm_goal_data->joint3_torque); i++;
        } else if (it.first == "joint4_torque") {
            preparedStatement->setDouble(i, left_arm_goal_data->joint4_torque); i++;
        } else if (it.first == "joint5_torque") {
            preparedStatement->setDouble(i, left_arm_goal_data->joint5_torque); i++;
        } else if (it.first == "joint6_torque") {
            preparedStatement->setDouble(i, left_arm_goal_data->joint6_torque); i++;
        } else if (it.first == "joint7_torque") {
            preparedStatement->setDouble(i, left_arm_goal_data->joint7_torque); i++;
        }        
    }

    preparedStatement->execute();

    if (left_arm_goal_data->left_arm_goal_id == 0) {
        left_arm_goal_data->left_arm_goal_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setRightArmGoal(right_arm_goal_t *right_arm_goal_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (right_arm_goal_data->joint1_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_torque", true);
    if (right_arm_goal_data->joint2_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_torque", true);
    if (right_arm_goal_data->joint3_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_torque", true);
    if (right_arm_goal_data->joint4_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_torque", true);
    if (right_arm_goal_data->joint5_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_torque", true);
    if (right_arm_goal_data->joint6_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_torque", true);
    if (right_arm_goal_data->joint7_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_torque", true);

    std::string queryBase = buildQuery("right_arm_goal", "right_arm_goal_id", fields_values, right_arm_goal_data->right_arm_goal_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "joint1_torque") {
            preparedStatement->setDouble(i, right_arm_goal_data->joint1_torque); i++;
        } else if (it.first == "joint2_torque") {
            preparedStatement->setDouble(i, right_arm_goal_data->joint2_torque); i++;
        } else if (it.first == "joint3_torque") {
            preparedStatement->setDouble(i, right_arm_goal_data->joint3_torque); i++;
        } else if (it.first == "joint4_torque") {
            preparedStatement->setDouble(i, right_arm_goal_data->joint4_torque); i++;
        } else if (it.first == "joint5_torque") {
            preparedStatement->setDouble(i, right_arm_goal_data->joint5_torque); i++;
        } else if (it.first == "joint6_torque") {
            preparedStatement->setDouble(i, right_arm_goal_data->joint6_torque); i++;
        } else if (it.first == "joint7_torque") {
            preparedStatement->setDouble(i, right_arm_goal_data->joint7_torque); i++;
        }        
    }

    preparedStatement->execute();

    if (right_arm_goal_data->right_arm_goal_id == 0) {
        right_arm_goal_data->right_arm_goal_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << (double) std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000000.0 << "ms ]" << std::endl;
}

void MysqlConnector::setLeftArmPosition(left_arm_position_t *left_arm_position_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (left_arm_position_data->position != NULL_DOUBLE) addColumnToQuery(&fields_values, "position", true);
    if (left_arm_position_data->orientation != NULL_DOUBLE) addColumnToQuery(&fields_values, "orientation", true);

    std::string queryBase = buildQuery("left_arm_position", "left_arm_position_id", fields_values, left_arm_position_data->left_arm_position_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "position") {
            preparedStatement->setString(i, left_arm_position_data->position.dump()); i++;
        } else if (it.first == "orientation") {
            preparedStatement->setString(i, left_arm_position_data->orientation.dump()); i++;
        }        
    }

    preparedStatement->execute();

    if (left_arm_position_data->left_arm_position_id == 0) {
        left_arm_position_data->left_arm_position_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;

}

void MysqlConnector::setRightArmPosition(right_arm_position_t *right_arm_position_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (right_arm_position_data->position != NULL_DOUBLE) addColumnToQuery(&fields_values, "position", true);
    if (right_arm_position_data->orientation != NULL_DOUBLE) addColumnToQuery(&fields_values, "orientation", true);

    std::string queryBase = buildQuery("right_arm_position", "right_arm_position_id", fields_values, right_arm_position_data->right_arm_position_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "position") {
            preparedStatement->setString(i, right_arm_position_data->position.dump()); i++;
        } else if (it.first == "orientation") {
            preparedStatement->setString(i, right_arm_position_data->orientation.dump()); i++;
        }        
    }

    preparedStatement->execute();

    if (right_arm_position_data->right_arm_position_id == 0) {
        right_arm_position_data->right_arm_position_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;

}

void MysqlConnector::setLeftArmPath(left_arm_path_t *left_arm_path_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (left_arm_path_data->left_arm_position_id != NULL_UINT) addColumnToQuery(&fields_values, "left_arm_position_id");
    if (left_arm_path_data->line_number != NULL_UINT) addColumnToQuery(&fields_values, "line_number", true);
    if (left_arm_path_data->joint1_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_position", true);
    if (left_arm_path_data->joint2_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_position", true);
    if (left_arm_path_data->joint3_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_position", true);
    if (left_arm_path_data->joint4_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_position", true);
    if (left_arm_path_data->joint5_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_position", true);
    if (left_arm_path_data->joint6_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_position", true);
    if (left_arm_path_data->joint7_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_position", true);

    std::string queryBase = buildQuery("left_arm_path", "left_arm_path_id", fields_values, left_arm_path_data->left_arm_path_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "left_arm_position_id") {
            preparedStatement->setUInt(i, left_arm_path_data->left_arm_position_id); i++;
        } else if (it.first == "line_number") {
            preparedStatement->setUInt(i, left_arm_path_data->line_number); i++;
        } else if (it.first == "joint1_position") {
            preparedStatement->setDouble(i, left_arm_path_data->joint1_position); i++;
        } else if (it.first == "joint2_position") {
            preparedStatement->setDouble(i, left_arm_path_data->joint2_position); i++;
        } else if (it.first == "joint3_position") {
            preparedStatement->setDouble(i, left_arm_path_data->joint3_position); i++;
        } else if (it.first == "joint4_position") {
            preparedStatement->setDouble(i, left_arm_path_data->joint4_position); i++;
        } else if (it.first == "joint5_position") {
            preparedStatement->setDouble(i, left_arm_path_data->joint5_position); i++;
        } else if (it.first == "joint6_position") {
            preparedStatement->setDouble(i, left_arm_path_data->joint6_position); i++;
        } else if (it.first == "joint7_position") {
            preparedStatement->setDouble(i, left_arm_path_data->joint7_position); i++;
        }        
    }

    preparedStatement->execute();

    if (left_arm_path_data->left_arm_path_id == 0) {
        left_arm_path_data->left_arm_path_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setRightArmPath(right_arm_path_t *right_arm_path_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (right_arm_path_data->right_arm_position_id != NULL_UINT) addColumnToQuery(&fields_values, "left_arm_position_id");
    if (right_arm_path_data->line_number != NULL_UINT) addColumnToQuery(&fields_values, "line_number", true);
    if (right_arm_path_data->joint1_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_position", true);
    if (right_arm_path_data->joint2_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_position", true);
    if (right_arm_path_data->joint3_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_position", true);
    if (right_arm_path_data->joint4_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_position", true);
    if (right_arm_path_data->joint5_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_position", true);
    if (right_arm_path_data->joint6_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_position", true);
    if (right_arm_path_data->joint7_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_position", true);

    std::string queryBase = buildQuery("right_arm_path", "right_arm_path_id", fields_values, right_arm_path_data->right_arm_path_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "right_arm_position_id") {
            preparedStatement->setUInt(i, right_arm_path_data->right_arm_position_id); i++;
        } else if (it.first == "line_number") {
            preparedStatement->setUInt(i, right_arm_path_data->line_number); i++;
        } else if (it.first == "joint1_position") {
            preparedStatement->setDouble(i, right_arm_path_data->joint1_position); i++;
        } else if (it.first == "joint2_position") {
            preparedStatement->setDouble(i, right_arm_path_data->joint2_position); i++;
        } else if (it.first == "joint3_position") {
            preparedStatement->setDouble(i, right_arm_path_data->joint3_position); i++;
        } else if (it.first == "joint4_position") {
            preparedStatement->setDouble(i, right_arm_path_data->joint4_position); i++;
        } else if (it.first == "joint5_position") {
            preparedStatement->setDouble(i, right_arm_path_data->joint5_position); i++;
        } else if (it.first == "joint6_position") {
            preparedStatement->setDouble(i, right_arm_path_data->joint6_position); i++;
        } else if (it.first == "joint7_position") {
            preparedStatement->setDouble(i, right_arm_path_data->joint7_position); i++;
        }        
    }

    preparedStatement->execute();

    if (right_arm_path_data->right_arm_path_id == 0) {
        right_arm_path_data->right_arm_path_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setLeftArmTrajectory(left_arm_trajectory_t *left_arm_trajectory_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (left_arm_trajectory_data->left_arm_position_id != NULL_UINT) addColumnToQuery(&fields_values, "left_arm_position_id");
    if (left_arm_trajectory_data->line_number != NULL_UINT) addColumnToQuery(&fields_values, "line_number", true);
    if (left_arm_trajectory_data->delta_t != NULL_DOUBLE) addColumnToQuery(&fields_values, "delta_t", true);
    if (left_arm_trajectory_data->joint1_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_position", true);
    if (left_arm_trajectory_data->joint2_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_position", true);
    if (left_arm_trajectory_data->joint3_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_position", true);
    if (left_arm_trajectory_data->joint4_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_position", true);
    if (left_arm_trajectory_data->joint5_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_position", true);
    if (left_arm_trajectory_data->joint6_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_position", true);
    if (left_arm_trajectory_data->joint7_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_position", true);
    if (left_arm_trajectory_data->joint1_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_velocity", true);
    if (left_arm_trajectory_data->joint2_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_velocity", true);
    if (left_arm_trajectory_data->joint3_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_velocity", true);
    if (left_arm_trajectory_data->joint4_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_velocity", true);
    if (left_arm_trajectory_data->joint5_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_velocity", true);
    if (left_arm_trajectory_data->joint6_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_velocity", true);
    if (left_arm_trajectory_data->joint7_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_velocity", true);
    if (left_arm_trajectory_data->joint1_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_acceleration", true);
    if (left_arm_trajectory_data->joint2_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_acceleration", true);
    if (left_arm_trajectory_data->joint3_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_acceleration", true);
    if (left_arm_trajectory_data->joint4_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_acceleration", true);
    if (left_arm_trajectory_data->joint5_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_acceleration", true);
    if (left_arm_trajectory_data->joint6_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_acceleration", true);
    if (left_arm_trajectory_data->joint7_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_acceleration", true);
    if (left_arm_trajectory_data->joint1_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_torque", true);
    if (left_arm_trajectory_data->joint2_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_torque", true);
    if (left_arm_trajectory_data->joint3_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_torque", true);
    if (left_arm_trajectory_data->joint4_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_torque", true);
    if (left_arm_trajectory_data->joint5_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_torque", true);
    if (left_arm_trajectory_data->joint6_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_torque", true);
    if (left_arm_trajectory_data->joint7_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_torque", true);

    std::string queryBase = buildQuery("left_arm_trajectory", "left_arm_trajectory_id", fields_values, left_arm_trajectory_data->left_arm_trajectory_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "left_arm_position_id") {
            preparedStatement->setUInt(i, left_arm_trajectory_data->left_arm_position_id); i++;
        } else if (it.first == "line_number") {
            preparedStatement->setInt(i, left_arm_trajectory_data->line_number); i++;
        } else if (it.first == "delta_t") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->delta_t); i++;
        } else if (it.first == "joint1_position") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint1_position); i++;
        } else if (it.first == "joint2_position") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint2_position); i++;
        } else if (it.first == "joint3_position") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint3_position); i++;
        } else if (it.first == "joint4_position") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint4_position); i++;
        } else if (it.first == "joint5_position") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint5_position); i++;
        } else if (it.first == "joint6_position") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint6_position); i++;
        } else if (it.first == "joint7_position") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint7_position); i++;
        } else if (it.first == "joint1_velocity") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint1_velocity); i++;
        } else if (it.first == "joint2_velocity") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint2_velocity); i++;
        } else if (it.first == "joint3_velocity") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint3_velocity); i++;
        } else if (it.first == "joint4_velocity") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint4_velocity); i++;
        } else if (it.first == "joint5_velocity") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint5_velocity); i++;
        } else if (it.first == "joint6_velocity") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint6_velocity); i++;
        } else if (it.first == "joint7_velocity") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint7_velocity); i++;
        } else if (it.first == "joint1_acceleration") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint1_acceleration); i++;
        } else if (it.first == "joint2_acceleration") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint2_acceleration); i++;
        } else if (it.first == "joint3_acceleration") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint3_acceleration); i++;
        } else if (it.first == "joint4_acceleration") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint4_acceleration); i++;
        } else if (it.first == "joint5_acceleration") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint5_acceleration); i++;
        } else if (it.first == "joint6_acceleration") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint6_acceleration); i++;
        } else if (it.first == "joint7_acceleration") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint7_acceleration); i++;
        } else if (it.first == "joint1_torque") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint1_torque); i++;
        } else if (it.first == "joint2_torque") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint2_torque); i++;
        } else if (it.first == "joint3_torque") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint3_torque); i++;
        } else if (it.first == "joint4_torque") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint4_torque); i++;
        } else if (it.first == "joint5_torque") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint5_torque); i++;
        } else if (it.first == "joint6_torque") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint6_torque); i++;
        } else if (it.first == "joint7_torque") {
            preparedStatement->setDouble(i, left_arm_trajectory_data->joint7_torque); i++;
        }        
    }

    preparedStatement->execute();

    if (left_arm_trajectory_data->left_arm_trajectory_id == 0) {
        left_arm_trajectory_data->left_arm_trajectory_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setRightArmTrajectory(right_arm_trajectory_t *right_arm_trajectory_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (right_arm_trajectory_data->right_arm_position_id != NULL_UINT) addColumnToQuery(&fields_values, "right_arm_position_id");
    if (right_arm_trajectory_data->line_number != NULL_UINT) addColumnToQuery(&fields_values, "line_number", true);
    if (right_arm_trajectory_data->delta_t != NULL_DOUBLE) addColumnToQuery(&fields_values, "delta_t", true);
    if (right_arm_trajectory_data->joint1_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_position", true);
    if (right_arm_trajectory_data->joint2_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_position", true);
    if (right_arm_trajectory_data->joint3_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_position", true);
    if (right_arm_trajectory_data->joint4_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_position", true);
    if (right_arm_trajectory_data->joint5_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_position", true);
    if (right_arm_trajectory_data->joint6_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_position", true);
    if (right_arm_trajectory_data->joint7_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_position", true);
    if (right_arm_trajectory_data->joint1_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_velocity", true);
    if (right_arm_trajectory_data->joint2_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_velocity", true);
    if (right_arm_trajectory_data->joint3_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_velocity", true);
    if (right_arm_trajectory_data->joint4_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_velocity", true);
    if (right_arm_trajectory_data->joint5_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_velocity", true);
    if (right_arm_trajectory_data->joint6_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_velocity", true);
    if (right_arm_trajectory_data->joint7_velocity != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_velocity", true);
    if (right_arm_trajectory_data->joint1_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_acceleration", true);
    if (right_arm_trajectory_data->joint2_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_acceleration", true);
    if (right_arm_trajectory_data->joint3_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_acceleration", true);
    if (right_arm_trajectory_data->joint4_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_acceleration", true);
    if (right_arm_trajectory_data->joint5_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_acceleration", true);
    if (right_arm_trajectory_data->joint6_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_acceleration", true);
    if (right_arm_trajectory_data->joint7_acceleration != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_acceleration", true);
    if (right_arm_trajectory_data->joint1_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint1_torque", true);
    if (right_arm_trajectory_data->joint2_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint2_torque", true);
    if (right_arm_trajectory_data->joint3_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint3_torque", true);
    if (right_arm_trajectory_data->joint4_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint4_torque", true);
    if (right_arm_trajectory_data->joint5_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint5_torque", true);
    if (right_arm_trajectory_data->joint6_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint6_torque", true);
    if (right_arm_trajectory_data->joint7_torque != NULL_DOUBLE) addColumnToQuery(&fields_values, "joint7_torque", true);

    std::string queryBase = buildQuery("right_arm_trajectory", "right_arm_trajectory_id", fields_values, right_arm_trajectory_data->right_arm_trajectory_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "left_arm_position_id") {
            preparedStatement->setUInt(i, right_arm_trajectory_data->right_arm_position_id); i++;
        } else if (it.first == "line_number") {
            preparedStatement->setInt(i, right_arm_trajectory_data->line_number); i++;
        } else if (it.first == "delta_t") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->delta_t); i++;
        } else if (it.first == "joint1_position") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint1_position); i++;
        } else if (it.first == "joint2_position") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint2_position); i++;
        } else if (it.first == "joint3_position") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint3_position); i++;
        } else if (it.first == "joint4_position") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint4_position); i++;
        } else if (it.first == "joint5_position") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint5_position); i++;
        } else if (it.first == "joint6_position") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint6_position); i++;
        } else if (it.first == "joint7_position") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint7_position); i++;
        } else if (it.first == "joint1_velocity") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint1_velocity); i++;
        } else if (it.first == "joint2_velocity") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint2_velocity); i++;
        } else if (it.first == "joint3_velocity") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint3_velocity); i++;
        } else if (it.first == "joint4_velocity") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint4_velocity); i++;
        } else if (it.first == "joint5_velocity") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint5_velocity); i++;
        } else if (it.first == "joint6_velocity") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint6_velocity); i++;
        } else if (it.first == "joint7_velocity") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint7_velocity); i++;
        } else if (it.first == "joint1_acceleration") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint1_acceleration); i++;
        } else if (it.first == "joint2_acceleration") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint2_acceleration); i++;
        } else if (it.first == "joint3_acceleration") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint3_acceleration); i++;
        } else if (it.first == "joint4_acceleration") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint4_acceleration); i++;
        } else if (it.first == "joint5_acceleration") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint5_acceleration); i++;
        } else if (it.first == "joint6_acceleration") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint6_acceleration); i++;
        } else if (it.first == "joint7_acceleration") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint7_acceleration); i++;
        } else if (it.first == "joint1_torque") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint1_torque); i++;
        } else if (it.first == "joint2_torque") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint2_torque); i++;
        } else if (it.first == "joint3_torque") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint3_torque); i++;
        } else if (it.first == "joint4_torque") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint4_torque); i++;
        } else if (it.first == "joint5_torque") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint5_torque); i++;
        } else if (it.first == "joint6_torque") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint6_torque); i++;
        } else if (it.first == "joint7_torque") {
            preparedStatement->setDouble(i, right_arm_trajectory_data->joint7_torque); i++;
        }        
    }

    preparedStatement->execute();

    if (right_arm_trajectory_data->right_arm_trajectory_id == 0) {
        right_arm_trajectory_data->right_arm_trajectory_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setLeftGripperState(left_gripper_state_t *left_gripper_state_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (left_gripper_state_data->matrix_from_fsr_upper_left != NULL) addColumnToQuery(&fields_values, "matrix_from_fsr_upper_left", true);
    if (left_gripper_state_data->matrix_from_fsr_lower_left != NULL) addColumnToQuery(&fields_values, "matrix_from_fsr_lower_left", true);
    if (left_gripper_state_data->matrix_from_fsr_upper_right != NULL) addColumnToQuery(&fields_values, "matrix_from_fsr_upper_right", true);
    if (left_gripper_state_data->matrix_from_fsr_lower_right != NULL) addColumnToQuery(&fields_values, "matrix_from_fsr_lower_right", true);
    if (left_gripper_state_data->servo_1_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "servo_1_position", true);
    if (left_gripper_state_data->servo_2_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "servo_2_position", true);
    if (left_gripper_state_data->current_force != NULL_DOUBLE) addColumnToQuery(&fields_values, "current_force", true);
    if (!left_gripper_state_data->gripper_type.empty()) addColumnToQuery(&fields_values, "gripper_type", true);
    if (left_gripper_state_data->item_element_id != NULL_UINT) addColumnToQuery(&fields_values, "item_element_id");

    std::string queryBase = buildQuery("left_gripper_state", "left_gripper_state_id", fields_values, left_gripper_state_data->left_gripper_state_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "matrix_from_fsr_upper_left") {
            preparedStatement->setString(i, left_gripper_state_data->matrix_from_fsr_upper_left.dump()); i++;
        } else if (it.first == "matrix_from_fsr_lower_left") {
            preparedStatement->setString(i, left_gripper_state_data->matrix_from_fsr_lower_left.dump()); i++;
        } else if (it.first == "matrix_from_fsr_upper_right") {
            preparedStatement->setString(i, left_gripper_state_data->matrix_from_fsr_upper_right.dump()); i++;
        } else if (it.first == "matrix_from_fsr_lower_right") {
            preparedStatement->setString(i, left_gripper_state_data->matrix_from_fsr_lower_right.dump()); i++;
        } else if (it.first == "servo_1_position") {
            preparedStatement->setDouble(i, left_gripper_state_data->servo_1_position); i++;
        } else if (it.first == "servo_2_position") {
            preparedStatement->setDouble(i, left_gripper_state_data->servo_1_position); i++;
        } else if (it.first == "current_force") {
            preparedStatement->setDouble(i, left_gripper_state_data->current_force); i++;
        } else if (it.first == "gripper_type") {
            preparedStatement->setString(i, left_gripper_state_data->gripper_type); i++;
        } else if (it.first == "item_element_id") {
            preparedStatement->setInt64(i, left_gripper_state_data->item_element_id); i++;
        }
    }

    preparedStatement->execute();

    if (left_gripper_state_data->left_gripper_state_id == 0) {
        left_gripper_state_data->left_gripper_state_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setRightGripperState(right_gripper_state_t *right_gripper_state_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (right_gripper_state_data->matrix_from_fsr_upper_left != NULL) addColumnToQuery(&fields_values, "matrix_from_fsr_upper_left", true);
    if (right_gripper_state_data->matrix_from_fsr_lower_left != NULL) addColumnToQuery(&fields_values, "matrix_from_fsr_lower_left", true);
    if (right_gripper_state_data->matrix_from_fsr_upper_right != NULL) addColumnToQuery(&fields_values, "matrix_from_fsr_upper_right", true);
    if (right_gripper_state_data->matrix_from_fsr_lower_right != NULL) addColumnToQuery(&fields_values, "matrix_from_fsr_lower_right", true);
    if (right_gripper_state_data->servo_1_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "servo_1_position", true);
    if (right_gripper_state_data->servo_2_position != NULL_DOUBLE) addColumnToQuery(&fields_values, "servo_2_position", true);
    if (right_gripper_state_data->current_force != NULL_DOUBLE) addColumnToQuery(&fields_values, "current_force", true);
    if (!right_gripper_state_data->gripper_type.empty()) addColumnToQuery(&fields_values, "gripper_type", true);
    if (right_gripper_state_data->item_element_id != NULL_UINT) addColumnToQuery(&fields_values, "item_element_id");

    std::string queryBase = buildQuery("right_gripper_state", "right_gripper_state_id", fields_values, right_gripper_state_data->right_gripper_state_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "matrix_from_fsr_upper_left") {
            preparedStatement->setString(i, right_gripper_state_data->matrix_from_fsr_upper_left.dump()); i++;
        } else if (it.first == "matrix_from_fsr_lower_left") {
            preparedStatement->setString(i, right_gripper_state_data->matrix_from_fsr_lower_left.dump()); i++;
        } else if (it.first == "matrix_from_fsr_upper_right") {
            preparedStatement->setString(i, right_gripper_state_data->matrix_from_fsr_upper_right.dump()); i++;
        } else if (it.first == "matrix_from_fsr_lower_right") {
            preparedStatement->setString(i, right_gripper_state_data->matrix_from_fsr_lower_right.dump()); i++;
        } else if (it.first == "servo_1_position") {
            preparedStatement->setDouble(i, right_gripper_state_data->servo_1_position); i++;
        } else if (it.first == "servo_2_position") {
            preparedStatement->setDouble(i, right_gripper_state_data->servo_1_position); i++;
        } else if (it.first == "current_force") {
            preparedStatement->setDouble(i, right_gripper_state_data->current_force); i++;
        } else if (it.first == "gripper_type") {
            preparedStatement->setString(i, right_gripper_state_data->gripper_type); i++;
        } else if (it.first == "item_element_id") {
            preparedStatement->setInt64(i, right_gripper_state_data->item_element_id); i++;
        }
    }

    preparedStatement->execute();

    if (right_gripper_state_data->right_gripper_state_id == 0) {
        right_gripper_state_data->right_gripper_state_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us ]" << std::endl;
}

void MysqlConnector::setLeftGrasp(left_grasp_t *left_grasp_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (left_grasp_data->item_id != NULL_UINT) addColumnToQuery(&fields_values, "item_id", true);
    if (left_grasp_data->item_ids != NULL) addColumnToQuery(&fields_values, "item_ids", true);
    if (left_grasp_data->grasp_position != NULL) addColumnToQuery(&fields_values, "grasp_position", true);
    if (left_grasp_data->grasp_orientation != NULL) addColumnToQuery(&fields_values, "grasp_orientation", true);
    if (left_grasp_data->grasp_width != NULL_DOUBLE) addColumnToQuery(&fields_values, "grasp_width", true);
    if (!left_grasp_data->purpose_policy.empty()) addColumnToQuery(&fields_values, "purpose_policy", true);
    if (left_grasp_data->desired_force != NULL_DOUBLE) addColumnToQuery(&fields_values, "desired_force", true);

    std::string queryBase = buildQuery("left_grasp", "left_grasp_id", fields_values, left_grasp_data->left_grasp_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "item_id") {
            preparedStatement->setInt64(i, left_grasp_data->item_id); i++;
        } else if (it.first == "item_ids") {
            preparedStatement->setString(i, left_grasp_data->item_ids.dump()); i++;
        } else if (it.first == "grasp_position") {
            preparedStatement->setString(i, left_grasp_data->grasp_position.dump()); i++;
        } else if (it.first == "grasp_orientation") {
            preparedStatement->setString(i, left_grasp_data->grasp_orientation.dump()); i++;
        } else if (it.first == "grasp_width") {
            preparedStatement->setDouble(i, left_grasp_data->grasp_width); i++;
        } else if (it.first == "purpose_policy") {
            preparedStatement->setString(i, left_grasp_data->purpose_policy); i++;
        } else if (it.first == "desired_force") {
            preparedStatement->setDouble(i, left_grasp_data->desired_force); i++;
        }
    }

    preparedStatement->execute();

    if (left_grasp_data->left_grasp_id == 0) {
        left_grasp_data->left_grasp_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}

void MysqlConnector::setRightGrasp(right_grasp_t *right_grasp_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (right_grasp_data->item_id != NULL_UINT) addColumnToQuery(&fields_values, "item_id", true);
    if (right_grasp_data->item_ids != NULL) addColumnToQuery(&fields_values, "item_ids", true);
    if (right_grasp_data->grasp_position != NULL) addColumnToQuery(&fields_values, "grasp_position", true);
    if (right_grasp_data->grasp_orientation != NULL) addColumnToQuery(&fields_values, "grasp_orientation", true);
    if (right_grasp_data->grasp_width != NULL_DOUBLE) addColumnToQuery(&fields_values, "grasp_width", true);
    if (!right_grasp_data->purpose_policy.empty()) addColumnToQuery(&fields_values, "purpose_policy", true);
    if (right_grasp_data->desired_force != NULL_DOUBLE) addColumnToQuery(&fields_values, "desired_force", true);

    std::string queryBase = buildQuery("right_grasp", "right_grasp_id", fields_values, right_grasp_data->right_grasp_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "item_id") {
            preparedStatement->setInt64(i, right_grasp_data->item_id); i++;
        } else if (it.first == "item_ids") {
            preparedStatement->setString(i, right_grasp_data->item_ids.dump()); i++;
        } else if (it.first == "grasp_position") {
            preparedStatement->setString(i, right_grasp_data->grasp_position.dump()); i++;
        } else if (it.first == "grasp_orientation") {
            preparedStatement->setString(i, right_grasp_data->grasp_orientation.dump()); i++;
        } else if (it.first == "grasp_width") {
            preparedStatement->setDouble(i, right_grasp_data->grasp_width); i++;
        } else if (it.first == "purpose_policy") {
            preparedStatement->setString(i, right_grasp_data->purpose_policy); i++;
        } else if (it.first == "desired_force") {
            preparedStatement->setDouble(i, right_grasp_data->desired_force); i++;
        }
    }

    preparedStatement->execute();

    if (right_grasp_data->right_grasp_id == 0) {
        right_grasp_data->right_grasp_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}






// --------------------------------------------------------------------------------------------------------------------
/**
 * Function writes column data of type FK (no timestamp)
 *
 * @param table_name name of the table in the database
 * @param column_name name of the column in the table
 * @param row_id private key of selected row
 * 
 * @return id of row where data were saved
 */
uint32_t MysqlConnector::writeFK(string table_name, string column_name, int fk_key, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase;

    try {
        sql::PreparedStatement *preparedStatement;

        if (row_id == 0) {
            // insert
            queryBase = "INSERT INTO " + table_name + "(" + column_name + ") VALUES (?)";
        } else {
            // update
            queryBase = "UPDATE " + table_name + " SET " + column_name + " = ? WHERE " + table_name + "_id = " + to_string(row_id);
        }

        preparedStatement = connection->prepareStatement(queryBase);
        preparedStatement->setInt(1, fk_key);
        preparedStatement->executeUpdate();

        delete preparedStatement;

        if (row_id == 0) {
            stmt = connection->createStatement();
            sql::ResultSet *res = stmt->executeQuery("SELECT @@identity AS id");
            res->next();
            row_id = res->getInt64("id");

            delete res;

        } else {
        }

    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << "WriteFK(" << table_name << "," << column_name << ") [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

    return row_id;
}

/**
 * Function writes column data of type integer (with timestamp)
 *
 * @param table_name name of the table in the database
 * @param column_name name of the column in the table
 * @param row_id private key of selected row, if not exists new row is created
 * 
 * @return id of row where data were saved
 */
uint32_t MysqlConnector::writeInt(string table_name, string column_name, int int_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase;

    try {
        sql::PreparedStatement *preparedStatement;
        if (row_id == 0) {
            // insert
            queryBase = "INSERT INTO " + table_name + "(" + column_name + ", " + column_name + "_timestamp) VALUES (?, NOW(6))";
        } else {
            // update
            queryBase = "UPDATE " + table_name + " SET " + column_name + " = ?, " + column_name + "_timestamp = NOW(6) WHERE " + table_name + "_id = " + to_string(row_id);
        }
        preparedStatement = connection->prepareStatement(queryBase);
        preparedStatement->setInt(1, int_data);
        preparedStatement->executeUpdate();

        delete preparedStatement;

        if (row_id == 0) {
            stmt = connection->createStatement();
            sql::ResultSet *res = stmt->executeQuery("SELECT @@identity AS id");
            res->next();
            row_id = res->getInt64("id");

            delete res;

        } else {
        }
    }
    catch (sql::SQLException &e)
    {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << "EXAMPLE_FUNCTION" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << "WriteInt(" << table_name << "," << column_name << ") [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

    return row_id;
}

/**
 * Function writes column data of type double (with timestamp)
 *
 * @param table_name name of the table in the database
 * @param column_name name of the column in the table
 * @param row_id private key of selected row, if not exists new row is created
 * 
 * @return id of row where data were saved
 */
uint32_t MysqlConnector::writeDouble(string table_name, string column_name, double double_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase;

    sql::PreparedStatement *preparedStatement;

    if (row_id == 0) {
        // insert
        queryBase = "INSERT INTO " + table_name + "(" + column_name + ", " + column_name + "_timestamp) VALUES (?, NOW(6))";
    } else {
        // update
        queryBase = "UPDATE " + table_name + " SET " + column_name + " = ?, " + column_name + "_timestamp = NOW(6) WHERE " + table_name + "_id = " + to_string(row_id);
    }

    preparedStatement = connection->prepareStatement(queryBase);
    preparedStatement->setDouble(1, double_data);
    preparedStatement->executeUpdate();

    delete preparedStatement;

    if (row_id == 0) {
        stmt = connection->createStatement();
        sql::ResultSet *res = stmt->executeQuery("SELECT @@identity AS id");
        res->next();
        row_id = res->getInt64("id");            

        delete res;
    } else {
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << "WriteDouble(" << table_name << "," << column_name << ") [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

    return row_id;
}


/**
 * Function writes column data of type string (with timestamp)
 *
 * @param table_name name of the table in the database
 * @param column_name name of the column in the table
 * @param row_id private key of selected row, if not exists new row is created
 * 
 * @return id of row where data were saved
 */
uint32_t MysqlConnector::writeString(string table_name, string column_name, string string_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase;

    sql::PreparedStatement *preparedStatement;

    if (row_id == 0) {
        // insert
        queryBase = "INSERT INTO " + table_name + "(" + column_name + ", " + column_name + "_timestamp) VALUES (?, NOW(6))";
    } else {
        // update
        queryBase = "UPDATE " + table_name + " SET " + column_name + " = ?, " + column_name + "_timestamp = NOW(6) WHERE " + table_name + "_id = " + to_string(row_id);
    }

    preparedStatement = connection->prepareStatement(queryBase);
    preparedStatement->setString(1, string_data);
    preparedStatement->executeUpdate();

    delete preparedStatement;

    if (row_id == 0) {
        stmt = connection->createStatement();
        sql::ResultSet *res = stmt->executeQuery("SELECT @@identity AS id");
        res->next();
        row_id = res->getInt64("id");            

        delete res;
    } else {
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << "WriteString(" << table_name << "," << column_name << ") [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

    return row_id;

}

/**
 * Function writes column data of type JSON (with timestamp)
 *
 * @param table_name name of the table in the database
 * @param column_name name of the column in the table
 * @param row_id private key of selected row, if not exists new row is created
 * 
 * @return id of row where data were saved
 */
uint32_t MysqlConnector::writeJSON(string table_name, string column_name, nlohmann::json json_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase;

    sql::PreparedStatement *preparedStatement;

    if (row_id == 0) {
        // insert
        queryBase = "INSERT INTO " + table_name + "(" + column_name + ", " + column_name + "_timestamp) VALUES (?, NOW(6))";
    } else {
        // update
        queryBase = "UPDATE " + table_name + " SET " + column_name + " = ?, " + column_name + "_timestamp = NOW(6) WHERE " + table_name + "_id = " + to_string(row_id);
    }

    preparedStatement = connection->prepareStatement(queryBase);
    preparedStatement->setString(1, json_data.dump());
    preparedStatement->executeUpdate();

    if (row_id == 0) {
        stmt = connection->createStatement();
        sql::ResultSet *res = stmt->executeQuery("SELECT @@identity AS id");
        res->next();
        uint32_t id = res->getInt64("id");            
        return id;
    } else {
        return row_id;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << "WriteJSON(" << table_name << "," << column_name << ") [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

    return row_id;
}

/**
 * Function writes column data of type blob (with timestamp)
 *
 * @param table_name name of the table in the database
 * @param column_name name of the column in the table
 * @param row_id private key of selected row, if not exists new row is created
 * 
 * @return id of row where data were saved
 */
uint32_t MysqlConnector::writeBLOB(string table_name, string column_name, iostream * blob_data, uint32_t row_id) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    std::string queryBase;

    sql::PreparedStatement *preparedStatement;

    if (row_id == 0) {
        // insert
        queryBase = "INSERT INTO " + table_name + "(" + column_name + ", " + column_name + "_timestamp) VALUES (?, NOW(6))";
    } else {
        // update
        queryBase = "UPDATE " + table_name + " SET " + column_name + " = ?, " + column_name + "_timestamp = NOW(6) WHERE " + table_name + "_id = " + to_string(row_id);
    }

    if (debug) std::cout << queryBase << std::endl;

    preparedStatement = connection->prepareStatement(queryBase);
    preparedStatement->setBlob(1, blob_data);
    preparedStatement->executeUpdate();

    if (row_id == 0) {
        stmt = connection->createStatement();
        sql::ResultSet *res = stmt->executeQuery("SELECT @@identity AS id");
        res->next();
        uint32_t id = res->getInt64("id");            
        return id;
    } else {
        return row_id;
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << "WriteBlob(" << table_name << "," << column_name << ") [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

    return row_id;
}

/**
 * Function deletes all rows from selected table
 *
 * @param table_name name of the table in the database
 * 
 */
void MysqlConnector::clearTable(string table_name) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();

    try {
        sql::Statement *stmt = connection->createStatement();

        stmt->execute("DELETE FROM " + table_name);
    }
    catch (sql::SQLException &e)
    {
        /*
        The JDBC API throws three different exceptions:
        - sql::MethodNotImplementedException (derived from sql::SQLException)
        - sql::InvalidArgumentException (derived from sql::SQLException)
        - sql::SQLException (derived from std::runtime_error)
        */
        cout << "# ERR: SQLException in " << __FILE__ << "(" << "function connect" << ") on line " << __LINE__ << endl;
        /* Use what() (derived from std::runtime_error) to fetch the error message */
        cout << "# ERR: " << e.what() << " (MySQL error code: " << e.getErrorCode() << ", SQLState: " << e.getSQLState() << " )" << endl;
        throw query_exception();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << "clearTable(" << table_name << ") [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;

}

void MysqlConnector::log(log_t *log_data) {
    check_connection();

    auto begin = std::chrono::high_resolution_clock::now();
    std::map <std::string, std::string> fields_values = {};

    if (log_data->log_level != NULL_UINT) addColumnToQuery(&fields_values, "log_level");
    if (!log_data->log_timestamp.empty()) addColumnToQuery(&fields_values, "log_timestamp");
    if (!log_data->log_name.empty()) addColumnToQuery(&fields_values, "log_name");
    if (!log_data->log_msg.empty()) addColumnToQuery(&fields_values, "log_msg");
    if (!log_data->log_file.empty()) addColumnToQuery(&fields_values, "log_file");
    if (!log_data->log_function.empty()) addColumnToQuery(&fields_values, "log_function");
    if (log_data->log_line != NULL_UINT) addColumnToQuery(&fields_values, "log_line");

    std::string queryBase = buildQuery("log", "log_id", fields_values, log_data->log_id);

    sql::PreparedStatement *preparedStatement;    

    preparedStatement = connection->prepareStatement(queryBase);

    int i = 1;
    for(auto it:fields_values) {
        if (it.first == "log_level") {
            preparedStatement->setUInt(i, log_data->log_level); i++;
        } else if (it.first == "log_timestamp") {
            preparedStatement->setString(i, log_data->log_timestamp); i++;
        } else if (it.first == "log_name") {
            preparedStatement->setString(i, log_data->log_name); i++;
        } else if (it.first == "log_msg") {
            preparedStatement->setString(i, log_data->log_msg); i++;
        } else if (it.first == "log_file") {
            preparedStatement->setString(i, log_data->log_file); i++;
        } else if (it.first == "log_function") {
            preparedStatement->setString(i, log_data->log_function); i++;
        } else if (it.first == "log_line") {
            preparedStatement->setUInt(i, log_data->log_line); i++;
        }
    }

    preparedStatement->execute();

    if (log_data->log_id == 0) {
        log_data->log_id = getLastId();
    }

    auto end = std::chrono::high_resolution_clock::now();

    if (debug) std::cout << queryBase.c_str() << " [" << std::chrono::duration_cast<std::chrono::nanoseconds>(end   - begin).count() / 1000 << "us]" << std::endl;
}
