#ifndef ESTIMATE_SHAPE_DEBUG__COMPONENT_HPP_
#define ESTIMATE_SHAPE_DEBUG__COMPONENT_HPP_

// ___CPP___
#include <cassert>
#include <sstream>
#include <memory>

// ___ROS2___
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/parameter.hpp>

// ___Package___
#include "estimate_shape_debug/visibility_control.h"
#include "mysql_connector.h"
#include "custom_interfaces/msg/items.hpp"
#include "helpers_vision/helpers_vision.hpp"

namespace ros2mysql
{
  using json = nlohmann::json;
  class EstimateShape : public rclcpp::Node
  {
  public:
    ESTIMATE_SHAPE_DEBUG_PUBLIC
    explicit EstimateShape(const rclcpp::NodeOptions &options);

  private:
    /**;
     * Configures component.
     *
     * Declares parameters and configures video capture.
     */
    ESTIMATE_SHAPE_DEBUG_LOCAL
    void _configure();

    /**;
     * Declares the parameter using rcl_interfaces.
     */
    ESTIMATE_SHAPE_DEBUG_LOCAL
    void _initializeParameters();

    // ESTIMATE_SHAPE_DEBUG_LOCAL
    // void _readLabelsFromServer();

    std::unique_ptr<MysqlConnector> _db;
    std::string _host;
    std::string _port;
    std::string _db_name;
    std::string _username;
    std::string _password;
    bool _debug;
    rclcpp::Subscription<custom_interfaces::msg::Items>::SharedPtr _sub;
    // std::map<int, std::string> _label_int_to_str;
  };

} // namespace ros2mysql

#endif // EstimateShape_DEBUG__COMPONENT_HPP_
