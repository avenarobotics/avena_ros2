#include "estimate_shape_debug/estimate_shape_debug.hpp"

namespace ros2mysql
{
    EstimateShape::EstimateShape(const rclcpp::NodeOptions &options)
        : Node("estimate_shape_debug", options)
    {
        // Declare parameters.
        _initializeParameters();
        _configure();
        // _readLabelsFromServer();

        _db = std::make_unique<MysqlConnector>(_host, _port, _db_name, _username, _password, _debug);
        auto callback = [this](const typename custom_interfaces::msg::Items::SharedPtr msg) -> void {
            RCLCPP_INFO(this->get_logger(), "Estimate shape debug: received data");
            // clear database
            _db->clearTable("item_element");
            // _db->clearTable("item");

            // Validate input message
            if (msg->header.stamp == builtin_interfaces::msg::Time())
                return;

            std::vector<item_element_t> item_elements_db;
            RCLCPP_INFO_STREAM(get_logger(), "Number of items to saved: " << msg->items.size());
            for (auto &item : msg->items)
            {
                // item
                RCLCPP_INFO_STREAM(get_logger(), "Number of item elements to saved: " << item.item_elements.size());
                for (auto &item_element : item.item_elements)
                {
                    item_element_t item_element_db;
                    json item_pose;

                    json position;
                    position["x"] = item.pose.position.x;
                    position["y"] = item.pose.position.y;
                    position["z"] = item.pose.position.z;
                    item_pose["position"] = position;

                    json orientation;
                    orientation["x"] = item.pose.orientation.x;
                    orientation["y"] = item.pose.orientation.y;
                    orientation["z"] = item.pose.orientation.z;
                    orientation["w"] = item.pose.orientation.w;
                    item_pose["orientation"] = orientation;

                    item_element_db.primitive_shape["item_pose"] = item_pose;
                    item_element_db.position["item_pose"]["position"] = item_pose["position"];
                    item_element_db.orientation["item_pose"]["orientation"] = item_pose["orientation"];

                    try
                    {
                        json parts_description_db_primitive_shape;
                        json parts_description_db_position;
                        json parts_description_db_orientation;
                        for (auto &part_description_str : item_element.parts_description)
                        {
                            if (!part_description_str.empty())
                            {
                                json part_description = json::parse(part_description_str);

                                // Primitive shape column
                                json part_primitive_shape;
                                part_primitive_shape["dims"] = part_description["dims"];
                                part_primitive_shape["fit_method"] = part_description["fit_method"];
                                part_primitive_shape["spawn_method"] = part_description["spawn_method"];
                                part_primitive_shape["part_name"] = part_description["part_name"];
                                
                                // Position column
                                json part_position;
                                part_position["position"] = part_description["pose"]["position"];
                                part_position["part_name"] = part_description["part_name"];

                                // Orientation column
                                json part_orientation;
                                part_orientation["orientation"] = part_description["pose"]["orientation"];
                                part_orientation["part_name"] = part_description["part_name"];

                                // Assigning data
                                parts_description_db_primitive_shape.push_back(part_primitive_shape);
                                parts_description_db_position.push_back(part_position);
                                parts_description_db_orientation.push_back(part_orientation);
                            }
                        }
                        item_element_db.primitive_shape["parts_description"] = parts_description_db_primitive_shape;
                        item_element_db.position["parts_description"] = parts_description_db_position;
                        item_element_db.orientation["parts_description"] = parts_description_db_orientation;

                    }
                    catch (const json::exception &e)
                    {
                        RCLCPP_FATAL_STREAM(get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
                        // rclcpp::shutdown();
                    }

                    std::shared_ptr<std::stringstream> ss_mask1(new std::stringstream);
                    if (item_element.cam1_mask.size() != 0)
                    {
                        *ss_mask1 << item_element.cam1_mask;
                        item_element_db.element_mask_1 = ss_mask1;
                    }
                    std::shared_ptr<std::stringstream> ss_mask2(new std::stringstream);
                    if (item_element.cam2_mask.size() != 0)
                    {
                        *ss_mask2 << item_element.cam2_mask;
                        item_element_db.element_mask_2 = ss_mask2;
                    }
                    cv::Mat cam1_depth;
                    cv::Mat cam2_depth;

                    if (item_element.cam1_depth.data.size() != 0)
                    {
                        helpers::converters::rosImageToCV(item_element.cam1_depth, cam1_depth);
                        helpers::converters::imageToStream(cam1_depth, item_element_db.element_depth_1);
                    }
                    if (item_element.cam2_depth.data.size() != 0)
                    {
                        helpers::converters::rosImageToCV(item_element.cam2_depth, cam2_depth);
                        helpers::converters::imageToStream(cam2_depth, item_element_db.element_depth_2);
                    }
                    pcl::PointCloud<pcl::PointXYZ>::Ptr ptcld(new pcl::PointCloud<pcl::PointXYZ>);

                    if (item_element.cam1_ptcld.data.size() != 0)
                    {
                        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.cam1_ptcld, ptcld);
                        helpers::converters::pointcloudToStream<pcl::PointXYZ>(ptcld, item_element_db.element_pcl_1);
                        ptcld->clear();
                    }
                    if (item_element.cam2_ptcld.data.size() != 0)
                    {
                        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.cam2_ptcld, ptcld);
                        helpers::converters::pointcloudToStream<pcl::PointXYZ>(ptcld, item_element_db.element_pcl_2);
                        ptcld->clear();
                    }
                    if (item_element.merged_ptcld.data.size() != 0)
                    {
                        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.merged_ptcld, ptcld);
                        helpers::converters::pointcloudToStream<pcl::PointXYZ>(ptcld, item_element_db.pcl_merged);
                    }
                    item_element_db.element_label = item_element.label;
                    item_element_db.id = item_element.id;
                    item_element_db.item_id = item.id;

                    item_elements_db.push_back(item_element_db);
                }
            }

            for (auto &item_element_db : item_elements_db)
            {
                // std::cout << " ---------------- DATA FROM ITEM_DB STRUCTURE   ------------------------  " << std::endl;
                // std::cout << " item_element_db.element_label :  " << item_element_db.element_label << std::endl;
                // std::cout << " item_element_db.item_element_id:  " << item_element_db.id << std::endl;
                // std::cout << " item_element_db.item_id:  " << item_element_db.item_id << std::endl;
                // std::cout << " item_element_db.orientation " << item_element_db.orientation << std::endl;
                // std::cout << " item_element_db.position " << item_element_db.position << std::endl;
                // std::cout << " -----------------------------------------------------------------------  " << std::endl;
                _db->setItemElement(&item_element_db);
            }

            RCLCPP_INFO(this->get_logger(), "Estimate shape debug: saved");
        };

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));//.transient_local().reliable();
        _sub = create_subscription<custom_interfaces::msg::Items>("merged_items", qos_settings, callback);
    }

    void EstimateShape::_configure()
    {
        this->get_parameter<std::string>("host", _host);
        this->get_parameter<std::string>("port", _port);
        this->get_parameter<std::string>("db_name", _db_name);
        this->get_parameter<std::string>("username", _username);
        this->get_parameter<std::string>("password", _password);
        this->get_parameter<bool>("debug", _debug);
    }

    void EstimateShape::_initializeParameters()
    {
        rcl_interfaces::msg::ParameterDescriptor host_descriptor;
        host_descriptor.name = "host";
        host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("host", "", host_descriptor);

        rcl_interfaces::msg::ParameterDescriptor port_descriptor;
        port_descriptor.name = "port";
        port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("port", "", port_descriptor);

        rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
        db_name_descriptor.name = "db_name";
        db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("db_name", "", db_name_descriptor);

        rcl_interfaces::msg::ParameterDescriptor username_descriptor;
        username_descriptor.name = "username";
        username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("username", "", username_descriptor);

        rcl_interfaces::msg::ParameterDescriptor password_descriptor;
        password_descriptor.name = "password";
        password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("password", "", password_descriptor);

        rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
        debug_descriptor.name = "debug";
        debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
        this->declare_parameter("debug", false, debug_descriptor);
    }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::EstimateShape)
