#include "detect_debug/detect_debug.hpp"

namespace ros2mysql
{

    Detect::Detect(const rclcpp::NodeOptions &options)
        : Node("detect_debug", options)
    {
        // Declare parameters.
        _initializeParameters();
        _configure();

        _db = std::make_unique<MysqlConnector>(_host, _port, _db_name, _username, _password, _debug);

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local();
        auto detections_callback = [this](const typename custom_interfaces::msg::Detections::SharedPtr msg) -> void {
            RCLCPP_INFO(get_logger(), "Received message from \"detections\" topic");
            _detections = msg;
            if (_detections && _rgb_images)
                _saveDetectionsToDb();
        };
        _sub = create_subscription<custom_interfaces::msg::Detections>("detections", qos_settings, detections_callback);

        auto rgb_images_callback = [this](const custom_interfaces::msg::RgbImages::SharedPtr rgb_images) {
            RCLCPP_INFO(get_logger(), "Received message from \"rgb_images\" topic");
            _rgb_images = rgb_images;
            if (_detections && _rgb_images)
                _saveDetectionsToDb();
        };
        _rgb_images_sub = create_subscription<custom_interfaces::msg::RgbImages>("rgb_images", qos_settings, rgb_images_callback);
    }

    void Detect::_saveDetectionsToDb()
    {
        RCLCPP_INFO(get_logger(), "Saving detections to database");
        // Validate input data
        if (!_rgb_images || _rgb_images->header.stamp == builtin_interfaces::msg::Time() || !_detections || _detections->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(get_logger(), "Invalid input data. Clearing \"item_cam1\" and \"item_cam2\" tables");
            _db->clearTable("item_cam1");
            _db->clearTable("item_cam2");
            _rgb_images.reset();
            _detections.reset();
            return;
        }

        scene_t scene_data;

        std::vector<uint32_t> scene_ids{_db->getRowsId("scene")};
        uint32_t scene_id{scene_ids[0]};

        // unpack msg
        std::vector<std::string> cam1_labels{_detections->cam1_labels};
        std::vector<std::string> cam2_labels{_detections->cam2_labels};

        std::vector<std::string> cam1_masks{_detections->cam1_masks};
        std::vector<std::string> cam2_masks{_detections->cam2_masks};

        // Images annotation
        cv::Mat cam1_rgb;
        cv::Mat cam2_rgb;
        helpers::converters::rosImageToCV(_rgb_images->cam1_rgb, cam1_rgb);
        helpers::converters::rosImageToCV(_rgb_images->cam2_rgb, cam2_rgb);
        cv::Mat cam1_annotated = AnnotationsVisualizer::annotateDetections(cam1_rgb, cam1_labels, cam1_masks);
        cv::Mat cam2_annotated = AnnotationsVisualizer::annotateDetections(cam2_rgb, cam2_labels, cam2_masks);
        helpers::converters::imageToStream(cam1_annotated, scene_data.annotated_camera_1);
        helpers::converters::imageToStream(cam2_annotated, scene_data.annotated_camera_2);

        assert(cam1_labels.size() == cam1_masks.size());
        assert(cam2_labels.size() == cam2_masks.size());

        // clear database
        _db->clearTable("item_cam1");
        _db->clearTable("item_cam2");

        for (size_t i = 0; i < cam1_labels.size(); i++)
        {
            item_cam1 item_1;
            std::stringstream curr_mask_cam1{cam1_masks[i]};
            auto mask_cam1_ptr = std::make_shared<std::iostream>(curr_mask_cam1.rdbuf());
            item_1.item_cam1_id = 0;
            item_1.scene_id = scene_id;
            item_1.mask = mask_cam1_ptr;
            item_1.label_id = std::numeric_limits<decltype(item_1.label_id)>::max();
            _db->setItemCam1(&item_1);
        }

        for (size_t j = 0; j < cam2_labels.size(); j++)
        {
            item_cam2 item_2;
            std::stringstream curr_mask_cam2{cam2_masks[j]};
            auto mask_cam2_ptr = std::make_shared<std::iostream>(curr_mask_cam2.rdbuf());
            item_2.item_cam2_id = 0;
            item_2.scene_id = scene_id;
            item_2.mask = mask_cam2_ptr;
            item_2.label_id = std::numeric_limits<decltype(item_2.label_id)>::max();
            _db->setItemCam2(&item_2);
        }

        scene_data.scene_id = scene_id;
        _db->setScene(&scene_data);

        _rgb_images.reset();
        _detections.reset();
        // std::flush(std::cout);
    }

    void Detect::_configure()
    {
        this->get_parameter<std::string>("host", _host);
        this->get_parameter<std::string>("port", _port);
        this->get_parameter<std::string>("db_name", _db_name);
        this->get_parameter<std::string>("username", _username);
        this->get_parameter<std::string>("password", _password);
        this->get_parameter<bool>("debug", _debug);
    }

    void Detect::_initializeParameters()
    {
        rcl_interfaces::msg::ParameterDescriptor host_descriptor;
        host_descriptor.name = "host";
        host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("host", "", host_descriptor);

        rcl_interfaces::msg::ParameterDescriptor port_descriptor;
        port_descriptor.name = "port";
        port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("port", "", port_descriptor);

        rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
        db_name_descriptor.name = "db_name";
        db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("db_name", "", db_name_descriptor);

        rcl_interfaces::msg::ParameterDescriptor username_descriptor;
        username_descriptor.name = "username";
        username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("username", "", username_descriptor);

        rcl_interfaces::msg::ParameterDescriptor password_descriptor;
        password_descriptor.name = "password";
        password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("password", "", password_descriptor);

        rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
        debug_descriptor.name = "debug";
        debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
        this->declare_parameter("debug", false, debug_descriptor);
    }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::Detect)
