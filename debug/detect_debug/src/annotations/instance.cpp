#include "detect_debug/annotations/instance.hpp"

namespace ros2mysql
{

    Instance::Instance(const std::string &label, const std::string &mask_rle, const cv::Scalar &color)
        : _label(label),
          _mask_rle(mask_rle),
          _color(color)
    {
        helpers::converters::stringToBinaryMask(mask_rle, _mask);
        _bounding_box = cv::boundingRect(_mask);
        cv::findContours(_mask, _contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
        for (const auto &contour : _contours)
            _area += cv::contourArea(contour);
    }

    double Instance::area() const
    {
        return _area;
    }

    void Instance::drawMaskAndBoundingBox(cv::Mat &image, const float &alpha) const
    {
        const auto draw_all_contours = -1;
        const auto box_thickness = 2;

        cv::Mat temp_rgb;
        image.copyTo(temp_rgb);
        cv::drawContours(temp_rgb, _contours, draw_all_contours, _color, cv::FILLED);
        cv::drawContours(temp_rgb, _contours, draw_all_contours, cv::Scalar::all(0), 2);
        cv::rectangle(temp_rgb, _bounding_box, _color, box_thickness);
        cv::addWeighted(image, alpha, temp_rgb, 1 - alpha, 0, image);
    }

    void Instance::drawLabel(cv::Mat &image) const
    {
        const auto font_scale = 0.5;
        const auto font_face = cv::FONT_HERSHEY_SIMPLEX;
        const auto thickness = 1;
        int baseline = 0;
        cv::Size text_size = cv::getTextSize(_label, font_face, font_scale, thickness, &baseline);
        cv::rectangle(image, cv::Point(_bounding_box.x, _bounding_box.y), cv::Point(_bounding_box.x + text_size.width, _bounding_box.y + text_size.height + baseline), cv::Scalar::all(0), cv::FILLED);
        cv::putText(image, _label, cv::Point(_bounding_box.x, _bounding_box.y + text_size.height), font_face, font_scale, cv::Scalar::all(255), thickness);
    }

    bool Instance::operator>(const Instance &other) const
    {
        return _area > other._area;
    }

} // namespace ros2mysql
