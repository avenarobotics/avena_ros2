#include "detect_debug/annotations/annotations_visualizer.hpp"

namespace ros2mysql
{
    cv::Mat AnnotationsVisualizer::annotateDetections(const cv::Mat &rgb, const std::vector<std::string> &labels, const std::vector<std::string> &masks, const float &alpha)
    {
        // Initialize same color for each label
        cv::RNG rng;
        std::map<std::string, cv::Scalar> label_color;
        for (const auto &label : labels)
            label_color[label] = cv::Scalar(rng.uniform(0, 256), rng.uniform(0, 256), rng.uniform(0, 256));

        std::vector<Instance> instances;
        for (size_t idx = 0; idx < masks.size(); ++idx)
            instances.push_back(Instance(labels[idx], masks[idx], label_color[labels[idx]]));

        // sort instances by mask area so that bigger masks do not cover smaller ones
        std::sort(instances.begin(), instances.end(), std::greater<Instance>());

        cv::Mat out_annotated;
        rgb.copyTo(out_annotated);
        for (auto &instance : instances)
        {
            instance.drawMaskAndBoundingBox(out_annotated, alpha);
            instance.drawLabel(out_annotated);
        }
        return out_annotated;
    }

} // namespace ros2mysql
