#ifndef DETECT_DEBUG__INSTANCE_HPP
#define DETECT_DEBUG__INSTANCE_HPP

#include <helpers_vision/helpers_vision.hpp>

namespace ros2mysql
{

  class Instance
  {
  public:
    explicit Instance(const std::string &label, const std::string &mask_rle, const cv::Scalar &color = cv::Scalar(255, 0, 0));
    double area() const;
    void drawMaskAndBoundingBox(cv::Mat &image, const float &alpha = 0.5) const;
    void drawLabel(cv::Mat &image) const;
    bool operator>(const Instance &other) const;

  private:
    std::string _label;
    std::string _mask_rle;
    cv::Mat _mask;
    cv::Scalar _color;
    cv::Rect _bounding_box;
    double _area = 0;
    std::vector<std::vector<cv::Point>> _contours;
  };

} // namespace ros2mysql

#endif // DETECT_DEBUG__STRUCTURES_HPP
