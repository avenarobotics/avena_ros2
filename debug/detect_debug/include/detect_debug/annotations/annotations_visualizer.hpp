#ifndef DETECT_DEBUG__ANNOTATIONS_VISUALIZER_HPP
#define DETECT_DEBUG__ANNOTATIONS_VISUALIZER_HPP

#include <rclcpp/logging.hpp>
#include "detect_debug/annotations/instance.hpp"

namespace ros2mysql
{
  class AnnotationsVisualizer
  {
  public:
    static cv::Mat annotateDetections(const cv::Mat &rgb, const std::vector<std::string> &labels, const std::vector<std::string> &masks, const float &alpha = 0.5);
  };

} // namespace ros2mysql

#endif // DETECT_DEBUG__ANNOTATIONS_VISUALIZER_HPP
