#ifndef DETECT_DEBUG__COMPONENT_HPP_
#define DETECT_DEBUG__COMPONENT_HPP_

// ___CPP___
#include <iostream>
#include <memory>
// #include <assert.h>
// #include <sstream>

// ___ROS___
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/parameter.hpp>

// ___Avena packages___
#include "mysql_connector.h"
#include "custom_interfaces/msg/detections.hpp"
#include "custom_interfaces/msg/rgb_images.hpp"
#include <helpers_vision/helpers_vision.hpp>
#include <helpers_commons/helpers_commons.hpp>
#include "detect_debug/visibility_control.h"
// #include "detect_debug/annotations/instance.hpp"
#include "detect_debug/annotations/annotations_visualizer.hpp"

namespace ros2mysql
{

  class Detect : public rclcpp::Node
  {
  public:
    COMPOSITION_PUBLIC
    explicit Detect(const rclcpp::NodeOptions &options);

  private:
    /**;
     * Configures component.
     *
     * Declares parameters and configures video capture.
     */
    COMPOSITION_PUBLIC
    void _configure();

    /**;
     * Declares the parameter using rcl_interfaces.
     */
    COMPOSITION_PUBLIC
    void _initializeParameters();

    COMPOSITION_PUBLIC
    void _saveDetectionsToDb();

    std::unique_ptr<MysqlConnector> _db;
    std::string _host;
    std::string _port;
    std::string _db_name;
    std::string _username;
    std::string _password;
    bool _debug;
    custom_interfaces::msg::RgbImages::SharedPtr _rgb_images;
    custom_interfaces::msg::Detections::SharedPtr _detections;
    rclcpp::Subscription<custom_interfaces::msg::Detections>::SharedPtr _sub;
    rclcpp::Subscription<custom_interfaces::msg::RgbImages>::SharedPtr _rgb_images_sub;
  };

} // namespace composition

#endif // DETECT_DEBUG__COMPONENT_HPP_
