#!/bin/bash

set -e

./single_item.sh banana
# ./single_item.sh bowl 
# ./single_item.sh broccoli 
# ./single_item.sh carrot 
# ./single_item.sh cucumber 
# ./single_item.sh cutting_board 
# ./single_item.sh knife 
# ./single_item.sh lipton 
# ./single_item.sh milk 
# ./single_item.sh onion 
# ./single_item.sh orange 
# ./single_item.sh plate

exit 0
