#ifndef SIM_SPAWN_COLLISION_COMMONS_H_INCLUDED
#define SIM_SPAWN_COLLISION_COMMONS_H_INCLUDED

// ___ROS___
#include <rclcpp/rclcpp.hpp>

// ___Coppelia___
#include "simPlusPlus/Plugin.h"

// ___Other CPP___
#include <nlohmann/json.hpp>

// ___PCL___
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace plugin_spawn_collision_items
{
  static const rclcpp::Logger LOGGER = rclcpp::get_logger("spawn_collision_items");
  using json = nlohmann::json;
  using Handle = simInt;
  constexpr int InvalidHandle = -128;

  enum ReturnCode_e
  {
    SUCCESS,
    FAILURE,
  };

  enum ShapeType_e : int
  {
    BOX = 0,
    SPHERE = 1,
    CYLINDER = 2,
  };

  struct ItemElement
  {
    int32_t id = 0;
    int32_t item_id = -1;
    std::string label;
    pcl::PointCloud<pcl::PointXYZ>::Ptr merged_ptcld;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cam1_ptcld;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cam2_ptcld;
    std::vector<json> parts_description;
  };

  struct Item
  {
    int32_t id = 0;
    std::string item_id_hash{};
    std::string label{};
    Eigen::Affine3f pose;
    std::vector<ItemElement> item_elements;
  };

  struct PartAdditionalData
  {
    Handle item_handle;
    std::string item_label;
  };

  class CoppeliaUtils
  {
  public:
    CoppeliaUtils() = delete;
    ~CoppeliaUtils() = delete;
    CoppeliaUtils(const CoppeliaUtils &other) = delete;
    static ReturnCode_e setObjectParent(const Handle &object_handle, const Handle &parent_handle, const bool &keep_in_place);
    static ReturnCode_e setObjectVisible(const Handle &object_handle);
    static ReturnCode_e setObjectRenderable(const Handle &object_handle);
    static ReturnCode_e setObjectCollidable(const Handle &object_handle);
    static ReturnCode_e setObjectName(const Handle &object_handle, const std::string &name);
    static ReturnCode_e setObjectPosition(const Handle &object_handle, const Handle &reference_handle, const Eigen::Vector3f &position);
    static ReturnCode_e setObjectQuaternion(const Handle &object_handle, const Handle &reference_handle, const Eigen::Quaternionf &rotation);
    static ReturnCode_e setObjectPose(const Handle &object_handle, const Handle &reference_handle, const Eigen::Affine3f &pose);
    static Handle copyObject(const Handle &object_handle);
  };
}

#endif
