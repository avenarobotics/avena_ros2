#ifndef SIM_SPAWN_COLLISION_ITEMSS_H_INCLUDED
#define SIM_SPAWN_COLLISION_ITEMSS_H_INCLUDED

// ___Coppelia___
#include "config.h"
#include "simPlusPlus/Plugin.h"
#include "stubs.h"

// ___CPP___
#include <cmath>
#include <iostream>
#include <chrono>
#include <utility>

// ___ROS2___
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include "custom_interfaces/action/simple_action.hpp"
#include "custom_interfaces/msg/filtered_scene_octomap.hpp"
#include "custom_interfaces/msg/items.hpp"

// ___Avena___
#include "commons.hpp"
#include "spawn_methods/factory_spawn_methods.hpp"
#include "helpers_commons/subscriptions_manager.hpp"
#include "helpers_commons/watchdog.hpp"

typedef std::chrono::system_clock::time_point TimeVar;

#define duration(a) std::chrono::duration_cast<std::chrono::milliseconds>(a).count()
#define timeNow() std::chrono::high_resolution_clock::now()
#define PLUGIN_NAME "SpawnCollisionItems"
#define PLUGIN_VERSION 1

namespace plugin_spawn_collision_items
{
    struct TopicInfo
    {
        std::string name;
        std::string type;
    };

    struct Topics
    {
        inline const static TopicInfo items{"merged_items", "custom_interfaces/Items"};
        inline const static TopicInfo octomap{"octomap_filter", "custom_interfaces/FilteredSceneOctomap"};
    };

    class SpawnCollisionItems : public sim::Plugin, public rclcpp::Node
    {

    public:
        using SpawnItems = custom_interfaces::action::SimpleAction;
        using GoalHandleSpawnItems = rclcpp_action::ServerGoalHandle<SpawnItems>;

        using ItemsMsg = custom_interfaces::msg::Items;
        using ItemMsg = custom_interfaces::msg::Item;
        using ItemElementMsg = custom_interfaces::msg::ItemElement;
        using FilteredSceneOctomapMsg = custom_interfaces::msg::FilteredSceneOctomap;

        SpawnCollisionItems();
        virtual ~SpawnCollisionItems();

        void onStart() override;
        void onInstancePass(const sim::InstancePassFlags &flags) override;
        void onSimulationAboutToStart() override;

    private:
        int _spawnCollisionItems(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, const ItemsMsg::SharedPtr &items_msg);
        int _removeCollisionItems();
        int _decodeRosMsg(const ItemsMsg::SharedPtr &items_msg, std::vector<Item> &items);
        std::vector<ItemElement> _getItemElements(Item &item);
        rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const SpawnItems::Goal> goal);
        rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandleSpawnItems> goal_handle);
        void _handleAccepted(const std::shared_ptr<GoalHandleSpawnItems> goal_handle);
        int _spawnCollisionOctomap(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, int world_handle, float octomap_grid_size);
        int _validateInputs(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, ItemsMsg::SharedPtr &items_msg);
        int _readParametersFromServer();
        int _parseLabelsFromParametersServer(const json &labels);
        int _parseOctomapVoxelSizeFromParametersServer(const json &octomap_parameters);
        Handle _getHandle(const std::string &name);
        void _setCollisionParentPoseToWorld();
        void _checkLastMessagesTimestamps(const builtin_interfaces::msg::Time &merged_items_stamp, const builtin_interfaces::msg::Time &filtered_octomap_stamp);


        Handle _world_handle;
        Handle _collisions_parent_handle;

        std::map<std::string, std::string> _default_fit_methods;
        rclcpp_action::Server<SpawnItems>::SharedPtr _action_server;
        float _voxel_size;
        bool _debug;

        bool _parameters_read;

        helpers::SubscriptionsManager::SharedPtr _sub_manager;
        helpers::Watchdog::SharedPtr _watchdog;

        builtin_interfaces::msg::Time _last_merged_items_msg_timestamp;
        builtin_interfaces::msg::Time _last_filtered_octomap_msg_timestamp;
    };
}

#endif
