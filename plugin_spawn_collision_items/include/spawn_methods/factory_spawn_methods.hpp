#ifndef FACTORY_SPAWN_METHODS_HPP
#define FACTORY_SPAWN_METHODS_HPP

#include <memory>
#include "spawn_methods/base_spawn_method.hpp"
#include "spawn_methods/spawn_primitive.hpp"
#include "spawn_methods/spawn_tool.hpp"

namespace plugin_spawn_collision_items
{
    class FactorySpawnMethod
    {
    public:
        FactorySpawnMethod() = delete;
        virtual ~FactorySpawnMethod() = delete;
        static std::shared_ptr<BaseSpawnMethod> createSpawnMethod(const std::string &spawn_method)
        {
            if (spawn_method == "cylinder" ||
                spawn_method == "box" ||
                spawn_method == "sphere")
                return std::make_shared<SpawnPrimitive>();
            else if (spawn_method == "tool")
                return std::make_shared<SpawnTool>();
            else
                return nullptr;
        }
    };
}
#endif