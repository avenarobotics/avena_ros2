#ifndef SPAWN_PRIMITIVE_HPP
#define SPAWN_PRIMITIVE_HPP

#include "spawn_methods/base_spawn_method.hpp"

namespace plugin_spawn_collision_items
{
  class SpawnPrimitive : public BaseSpawnMethod
  {
  public:
    SpawnPrimitive();
    virtual ~SpawnPrimitive();
    virtual Handle spawn(const json &part_description, const PartAdditionalData &additional_data) override;

  private:
    std::vector<float> _assignObjectSizes(const json &part_description);
  };
}
#endif
