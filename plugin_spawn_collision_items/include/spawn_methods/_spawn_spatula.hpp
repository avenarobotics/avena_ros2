#ifndef SPAWN_SPATULA_HPP
#define SPAWN_SPATULA_HPP

#include "spawn_methods/base_spawn_method.hpp"
#include "spawn_methods/spawn_primitive.hpp"
#include "helpers.hpp"

namespace plugin_spawn_collision_items
{
    class SpawnSpatula : public BaseSpawnMethod
    {

    public:
        SpawnSpatula();
        virtual ~SpawnSpatula();
        virtual int spawn(ItemElement &item_element) override;

    private:
        struct SpatulaData
        {
            Eigen::Vector3f spatula_position;
            Eigen::Quaternionf spatula_orientation;
        };
        SpatulaData _readData(ItemElement &item_element);
    };
}
#endif
