#ifndef BASE_SPAWN_METHOD_HPP
#define BASE_SPAWN_METHOD_HPP

// ___Coppelia___
#include "simPlusPlus/Plugin.h"

// ___CPP___
#include <map>
#include <memory>

// ___Other
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <eigen3/Eigen/Eigen>
#include <nlohmann/json.hpp>

// ___ROS___
#include <rclcpp/rclcpp.hpp>

// ___Package___
#include "commons.hpp"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"

namespace plugin_spawn_collision_items
{
  class BaseSpawnMethod
  {
  public:
    BaseSpawnMethod()
    {
      _parent_handle = simGetObjectHandle("collisions_parent");
      _world_handle = simGetObjectHandle("DummyWorld");

      if (_readSpawnModelsHandles() != ReturnCode_e::SUCCESS)
      {
        RCLCPP_ERROR(LOGGER, "Error occured while loading spawn models handles from Coppelia");
        // Probably need to throw exception and handle it inside plugin
        // throw std::runtime_error("Error occured while loading tools handles from Coppelia");
      }
    };
    virtual ~BaseSpawnMethod(){};
    virtual Handle spawn(const json &part_description, const PartAdditionalData &additional_data) = 0;

  protected:
    int _world_handle;
    int _parent_handle;
    std::map<std::string, int> _spawn_models_handles;
    // std::vector<label_t> _labels_data;

    const std::map<std::string, ShapeType_e> _shape_type{
        {"box", ShapeType_e::BOX},
        {"sphere", ShapeType_e::SPHERE},
        {"cylinder", ShapeType_e::CYLINDER},
    };

    const simInt _shape_option_static = 16;

  private:
    ReturnCode_e _readSpawnModelsHandles()
    {
      simInt tools_dummy_handle = simGetObjectHandle("spawn_models");
      if (tools_dummy_handle == -1)
        return ReturnCode_e::FAILURE;
      simInt nr_tools;
      simInt *tools_handles = simGetObjectsInTree(tools_dummy_handle, sim_handle_all, 1, &nr_tools);
      if (tools_handles == nullptr)
        return ReturnCode_e::FAILURE;
      for (simInt tool_idx = 0; tool_idx < nr_tools; ++tool_idx)
      {
        simChar *tool_name = simGetObjectName(tools_handles[tool_idx]);
        if (tool_name == nullptr)
          return ReturnCode_e::FAILURE;
        _spawn_models_handles[std::string(tool_name)] = tools_handles[tool_idx];
        simReleaseBuffer(tool_name);
      }
      simReleaseBuffer(reinterpret_cast<const simChar *>(tools_handles));
      return ReturnCode_e::SUCCESS;
    }
  };
}
#endif