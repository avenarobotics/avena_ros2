#ifndef SPAWN_TOOL_HPP
#define SPAWN_TOOL_HPP

#include "spawn_methods/base_spawn_method.hpp"

namespace plugin_spawn_collision_items
{
  class SpawnTool : public BaseSpawnMethod
  {
  public:
    SpawnTool();
    virtual ~SpawnTool();
    virtual Handle spawn(const json &part_description, const PartAdditionalData &additional_data) override;
  };
}
#endif
