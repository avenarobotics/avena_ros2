#ifndef SPAWN_DISH_HPP
#define SPAWN_DISH_HPP

#include "spawn_methods/base_spawn_method.hpp"
#include "spawn_methods/spawn_primitive.hpp"
#include "helpers.hpp"

namespace plugin_spawn_collision_items
{
    class SpawnDish : public BaseSpawnMethod
    {

    public:
        SpawnDish();
        virtual ~SpawnDish();
        virtual int spawn(ItemElement &item_element) override;

    private:
        struct DishData
        {
            Eigen::Vector3f dish_position;
            Eigen::Quaternionf dish_orientation;
        };
        DishData _readData(ItemElement &item_element);
    };
}
#endif
