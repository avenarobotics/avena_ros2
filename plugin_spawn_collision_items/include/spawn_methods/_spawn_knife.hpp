#ifndef SPAWN_KNIFE_HPP
#define SPAWN_KNIFE_HPP

#include "spawn_methods/base_spawn_method.hpp"
#include "spawn_methods/spawn_primitive.hpp"
#include "helpers.hpp"

namespace plugin_spawn_collision_items
{
    class SpawnKnife : public BaseSpawnMethod
    {

    public:
        SpawnKnife();
        virtual ~SpawnKnife();
        virtual int spawn(ItemElement &item_element) override;

    private:
        struct KnifeData
        {
            Eigen::Vector3f knife_position;
            Eigen::Quaternionf knife_orientation;
        };
        KnifeData _readData(ItemElement &item_element);
    };
}
#endif
