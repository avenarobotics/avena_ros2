#include "commons.hpp"

namespace plugin_spawn_collision_items
{
    ReturnCode_e CoppeliaUtils::setObjectParent(const Handle &object_handle, const Handle &parent_handle, const bool &keep_in_place)
    {
        simInt return_code = simSetObjectParent(object_handle, parent_handle, keep_in_place);
        return return_code != -1 ? ReturnCode_e::SUCCESS : ReturnCode_e::FAILURE;
    }

    ReturnCode_e CoppeliaUtils::setObjectVisible(const simInt &object_handle)
    {
        const simInt visibility_layer = 1;
        simInt return_code = simSetObjectInt32Parameter(object_handle, sim_objintparam_visibility_layer, visibility_layer);
        return return_code == 1 ? ReturnCode_e::SUCCESS : ReturnCode_e::FAILURE;
    }

    ReturnCode_e CoppeliaUtils::setObjectCollidable(const simInt &object_handle)
    {
        simInt return_code = simSetObjectSpecialProperty(object_handle, sim_objectspecialproperty_collidable);
        return return_code != -1 ? ReturnCode_e::SUCCESS : ReturnCode_e::FAILURE;
    }

    ReturnCode_e CoppeliaUtils::setObjectName(const simInt &object_handle, const std::string &name)
    {
        simInt return_code = simSetObjectName(object_handle, name.c_str());
        return return_code != -1 ? ReturnCode_e::SUCCESS : ReturnCode_e::FAILURE;
    }

    ReturnCode_e CoppeliaUtils::setObjectPosition(const Handle &object_handle, const Handle &reference_handle, const Eigen::Vector3f &position)
    {
        std::vector<float> object_position{position.x(), position.y(), position.z()};
        simInt return_code = simSetObjectPosition(object_handle, reference_handle, object_position.data());
        return return_code != -1 ? ReturnCode_e::SUCCESS : ReturnCode_e::FAILURE;
    }

    ReturnCode_e CoppeliaUtils::setObjectQuaternion(const Handle &object_handle, const Handle &reference_handle, const Eigen::Quaternionf &rotation)
    {
        std::vector<float> quat{rotation.x(), rotation.y(), rotation.z(), rotation.w()};
        simInt return_code = simSetObjectQuaternion(object_handle, reference_handle, quat.data());
        return return_code != -1 ? ReturnCode_e::SUCCESS : ReturnCode_e::FAILURE;
    }

    ReturnCode_e CoppeliaUtils::setObjectPose(const Handle &object_handle, const Handle &reference_handle, const Eigen::Affine3f &pose)
    {
        const Eigen::Vector3f position(pose.translation());
        simInt return_code = CoppeliaUtils::setObjectPosition(object_handle, reference_handle, position);
        if (return_code == ReturnCode_e::FAILURE)
            return ReturnCode_e::FAILURE;

        const Eigen::Quaternionf orientation(pose.rotation());
        return_code = CoppeliaUtils::setObjectQuaternion(object_handle, reference_handle, orientation);
        if (return_code == ReturnCode_e::FAILURE)
            return ReturnCode_e::FAILURE;
        return ReturnCode_e::SUCCESS;
    }

    Handle CoppeliaUtils::copyObject(const Handle &object_handle)
    {
        Handle obj_handle = object_handle;
        simInt nr_copied_handles = simCopyPasteObjects(&obj_handle, 1, 0);
        if (nr_copied_handles == -1)
            return -1;
        return obj_handle;
    }
}
