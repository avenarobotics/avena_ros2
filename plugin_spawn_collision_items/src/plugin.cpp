#include "plugin.h"

namespace plugin_spawn_collision_items
{
    SpawnCollisionItems::SpawnCollisionItems()
        : Node("spawn_collision_items"),
          _voxel_size(0.01),
          _debug(false),
          _parameters_read(false)
    {
    }

    SpawnCollisionItems::~SpawnCollisionItems(){};

    void SpawnCollisionItems::onStart()
    {
        if (!registerScriptStuff())
            throw std::runtime_error("script stuff initialization failed");

        setExtVersion("Plugin for spawning collision objects");
        setBuildDate(BUILD_DATE);

        helpers::commons::setLoggerLevel(get_logger(), "debug");

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        _sub_manager = std::make_shared<helpers::SubscriptionsManager>(get_node_topics_interface());
        _sub_manager->createSubscription(Topics::octomap.name, Topics::octomap.type, qos_settings);
        _sub_manager->createSubscription(Topics::items.name, Topics::items.type, qos_settings);
        _action_server = rclcpp_action::create_server<SpawnItems>(
            get_node_base_interface(),
            get_node_clock_interface(),
            get_node_logging_interface(),
            get_node_waitables_interface(),
            "spawn_collision_items",
            std::bind(&SpawnCollisionItems::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&SpawnCollisionItems::_handleCancel, this, std::placeholders::_1),
            std::bind(&SpawnCollisionItems::_handleAccepted, this, std::placeholders::_1));

        _readParametersFromServer();
        // _watchdog = std::make_shared<helpers::Watchdog>(this, "system_monitor");
    }

    void SpawnCollisionItems::onInstancePass(const sim::InstancePassFlags &flags)
    {
        rclcpp::spin_some(get_node_base_interface());
    }

    void SpawnCollisionItems::onSimulationAboutToStart()
    {
        _world_handle = simGetObjectHandle("DummyWorld");
        _collisions_parent_handle = _getHandle("collisions_parent");
        _setCollisionParentPoseToWorld();
    }

    rclcpp_action::GoalResponse SpawnCollisionItems::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const SpawnItems::Goal> /*goal*/)
    {
        RCLCPP_INFO(get_logger(), "Received goal request");
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse SpawnCollisionItems::_handleCancel(const std::shared_ptr<GoalHandleSpawnItems> /*goal_handle*/)
    {
        RCLCPP_INFO(get_logger(), "Goal canceled");
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void SpawnCollisionItems::_handleAccepted(const std::shared_ptr<GoalHandleSpawnItems> goal_handle)
    {
        RCLCPP_INFO(get_logger(), "Goal accepted");
        auto execute = [this](const std::shared_ptr<GoalHandleSpawnItems> goal_handle) -> void
        {
            helpers::Timer timer("Spawn collision items", get_logger());
            auto result = std::make_shared<SpawnItems::Result>();
            if (_readParametersFromServer())
            {
                RCLCPP_ERROR(get_logger(), "Not able to read parameteres from the server");
                goal_handle->abort(result);
                return;
            }
            _collisions_parent_handle = _getHandle("collisions_parent");

            RCLCPP_INFO(get_logger(), "Executing goal to spawn collision objects");
            auto filtered_scene_octomap_msg = _sub_manager->getData<custom_interfaces::msg::FilteredSceneOctomap>(Topics::octomap.name);
            auto items_msg = _sub_manager->getData<custom_interfaces::msg::Items>(Topics::items.name);
            if (_validateInputs(filtered_scene_octomap_msg, items_msg))
            {
                RCLCPP_ERROR(get_logger(), "Invalid input data. Goal aborted...");
                goal_handle->abort(result);
                return;
            }
            _checkLastMessagesTimestamps(items_msg->header.stamp, filtered_scene_octomap_msg->header.stamp);
            _removeCollisionItems();

            if (_spawnCollisionOctomap(filtered_scene_octomap_msg, _world_handle, _voxel_size))
            {
                RCLCPP_ERROR(get_logger(), "Error occured while spawning collision octomap. Goal failed.");
                goal_handle->abort(result);
                return;
            }

            if (_spawnCollisionItems(filtered_scene_octomap_msg, items_msg))
            {
                RCLCPP_ERROR(get_logger(), "Error occured while spawning collision items. Goal failed.");
                goal_handle->abort(result);
                return;
            }

            if (rclcpp::ok())
            {
                goal_handle->succeed(result);
                RCLCPP_INFO(get_logger(), "Goal succeeded");
            }
        };
        execute(goal_handle);
    }

    int SpawnCollisionItems::_validateInputs(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, ItemsMsg::SharedPtr &items_msg)
    {
        if (!_parameters_read)
        {
            RCLCPP_WARN(get_logger(), "Labels are not loaded yet");
            return 1;
        }

        // It is only necessary to spawn octomap in some cases. Initialize item message with empty data
        // if (filtered_scene_octomap_msg && filtered_scene_octomap_msg->header.stamp != builtin_interfaces::msg::Time())
        // {
        //     items_msg = std::make_shared<ItemsMsg>();
        //     return 0;
        // }

        if (!filtered_scene_octomap_msg ||
            !items_msg ||
            filtered_scene_octomap_msg->header.stamp == builtin_interfaces::msg::Time() ||
            items_msg->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(get_logger(), "Invalid header in message.");
            return 1;
        }
        return 0;
    }

    int SpawnCollisionItems::_spawnCollisionOctomap(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, int world_handle, float octomap_grid_size)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_scene_octomap(new pcl::PointCloud<pcl::PointXYZ>);
        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(filtered_scene_octomap_msg->filtered_scene_octomap, filtered_scene_octomap);

        if (filtered_scene_octomap->size() == 0)
        {
            RCLCPP_DEBUG(get_logger(), "Empty collision octomap.");
            return 0;
        }

        std::vector<float> points(filtered_scene_octomap->points.size() * 3);
        for (size_t i = 0; i < filtered_scene_octomap->points.size(); ++i)
        {
            points[3 * i + 0] = filtered_scene_octomap->points[i].x;
            points[3 * i + 1] = filtered_scene_octomap->points[i].y;
            points[3 * i + 2] = filtered_scene_octomap->points[i].z;
        }

        simInt octree_handle = simCreateOctree(octomap_grid_size, 0, 0, nullptr);
        CoppeliaUtils::setObjectParent(octree_handle, world_handle, false);
        simInsertVoxelsIntoOctree(octree_handle, 1, points.data(), points.size() / 3, nullptr, nullptr, nullptr);
        simSetModelProperty(octree_handle, sim_modelproperty_not_renderable);
        CoppeliaUtils::setObjectName(octree_handle, "collision_octomap");

        if (octree_handle == -1)
        {
            RCLCPP_ERROR(get_logger(), "Problem occured while spawning octomap.");
            return 1;
        }
        else
        {
            RCLCPP_INFO(get_logger(), "Octomap spawned correctly.");
            simSetObjectParent(octree_handle, _collisions_parent_handle, true);
        }

        return 0;
    }

    int SpawnCollisionItems::_decodeRosMsg(const ItemsMsg::SharedPtr &items_msg, std::vector<Item> &items)
    {
        items.resize(items_msg->items.size());
        RCLCPP_DEBUG_STREAM(get_logger(), "Decoding item message with: " << items_msg->items.size() << " items");
        for (size_t i = 0; i < items_msg->items.size(); i++)
        {
            Item &item = items[i];
            ItemMsg &item_msg = items_msg->items[i];

            item.id = item_msg.id;
            item.label = item_msg.label;
            helpers::converters::geometryToEigenAffine(item_msg.pose, item.pose);
            item.item_elements.resize(item_msg.item_elements.size());
            for (size_t el_idx = 0; el_idx < item_msg.item_elements.size(); el_idx++)
            {
                ItemElement &item_element = item.item_elements[el_idx];
                ItemElementMsg &item_element_msg = item_msg.item_elements[el_idx];

                item_element.id = item_element_msg.id;
                item_element.item_id = item_msg.id;
                item_element.label = item_element_msg.label;

                // PointClouds
                helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element_msg.merged_ptcld, item_element.merged_ptcld);
                helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element_msg.cam1_ptcld, item_element.cam1_ptcld);
                helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element_msg.cam2_ptcld, item_element.cam2_ptcld);

                // Descriptions with information about all parts of element
                for (auto &part_description : item_element_msg.parts_description)
                {
                    if (!part_description.empty())
                        item_element.parts_description.push_back(json::parse(part_description));
                }
            }
        }
        RCLCPP_DEBUG(get_logger(), "Topic message converted to structures succesfully");
        return 0;
    }

    int SpawnCollisionItems::_spawnCollisionItems(const FilteredSceneOctomapMsg::SharedPtr &filtered_scene_octomap_msg, const ItemsMsg::SharedPtr &items_msg)
    {
        std::vector<Item> items;
        if (_decodeRosMsg(items_msg, items))
        {
            RCLCPP_ERROR(get_logger(), "Problem occured while decoding ROS topic message.");
            return 1;
        }

        for (auto &item : items)
        {
            helpers::Timer timer("Item ID: " + std::to_string(item.id) + ", label: " + item.label, get_logger());
            Handle item_handle = simCreateDummy(0.001, nullptr);

            CoppeliaUtils::setObjectPose(item_handle, _world_handle, item.pose);
            CoppeliaUtils::setObjectParent(item_handle, _collisions_parent_handle, true);
            CoppeliaUtils::setObjectName(item_handle, item.label + "_" + std::to_string(item.id));
            CoppeliaUtils::setObjectCollidable(item_handle);

            for (auto &item_element : item.item_elements)
            {
                RCLCPP_DEBUG(get_logger(), "Element ID: " + std::to_string(item_element.id) + ", label: " + item_element.label);
                for (size_t idx = 0; idx < item_element.parts_description.size(); ++idx)
                {
                    json &part_description = item_element.parts_description[idx];
                    RCLCPP_DEBUG_STREAM(get_logger(), "Part with spawn_method: " << part_description["spawn_method"]);
                    auto spawn_method = FactorySpawnMethod::createSpawnMethod(part_description["spawn_method"]);
                    if (spawn_method == nullptr)
                    {
                        RCLCPP_WARN_STREAM(get_logger(), "Invalid spawn method. Dropping spawning item element with ID: " << item_element.id << ", label: " << item_element.label);
                        continue;
                    }

                    PartAdditionalData additional_data{item_handle, item.label};
                    Handle part_handle = spawn_method->spawn(part_description, additional_data);
                    CoppeliaUtils::setObjectParent(part_handle, item_handle, true);
                    CoppeliaUtils::setObjectVisible(part_handle);
                    CoppeliaUtils::setObjectCollidable(part_handle);
                    CoppeliaUtils::setObjectName(part_handle, item.label + "_" + std::to_string(item.id) + "_el_" + std::to_string(item_element.id) + "_part_" + std::to_string(idx));
                }
            }
        }
        return 0;
    }

    int SpawnCollisionItems::_removeCollisionItems()
    {
        int child_handle = simGetObjectChild(_collisions_parent_handle, 0);
        while (child_handle != -1)
        {
            simRemoveObject(child_handle);
            child_handle = simGetObjectChild(_collisions_parent_handle, 0);
        }
        return 0;
    }

    int SpawnCollisionItems::_readParametersFromServer()
    {
        RCLCPP_INFO_ONCE(get_logger(), "Reading parameters from the server");

        if (_parameters_read)
            return 0;

        std::map<std::string, json> parameters = helpers::commons::getParameters({"labels", "octomap_filter"});
        if (parameters.empty())
            return 1;

        json labels = parameters["labels"];
        json octomap_filter = parameters["octomap_filter"];

        if (_parseLabelsFromParametersServer(labels) || _parseOctomapVoxelSizeFromParametersServer(octomap_filter))
        {
            _default_fit_methods.clear();
            return 1;
        }

        _parameters_read = true;
        RCLCPP_INFO(get_logger(), "Parameters read successfully...");
        return 0;
    }

    int SpawnCollisionItems::_parseLabelsFromParametersServer(const json &labels)
    {
        RCLCPP_INFO(get_logger(), "Parsing \"labels\" parameter...");
        // RCLCPP_INFO_STREAM(get_logger(), "Number of labels: " << label_data.items());
        try
        {
            for (auto label : labels)
            {
                if (label.contains("label") && label.contains("fit_method"))
                {
                    _default_fit_methods[label["label"]] = label["fit_method"];
                }
            }
        }
        catch (const json::exception &e)
        {
            RCLCPP_ERROR_STREAM(get_logger(), "Error while parsing labels information. Dropping reading rest of parameters from the server");
            return 1;
        }
        RCLCPP_INFO(get_logger(), "Label data loaded successfully");
        return 0;
    }

    int SpawnCollisionItems::_parseOctomapVoxelSizeFromParametersServer(const json &octomap_params)
    {
        RCLCPP_INFO(get_logger(), "Parsing \"octomap_voxel_size\" parameter...");
        try
        {
            _voxel_size = octomap_params["octomap_voxel_size"];
        }
        catch (const json::exception &e)
        {
            RCLCPP_ERROR_STREAM(get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
            return 1;
        }
        RCLCPP_INFO(get_logger(), "Octomap voxel size loaded successfully");
        return 0;
    }

    Handle SpawnCollisionItems::_getHandle(const std::string &name)
    {
        Handle handle = simGetObjectHandle(name.c_str());
        if (handle == -1)
        {
            handle = simCreateDummy(0.001, nullptr);
            simSetObjectName(handle, name.c_str());
        }
        return handle;
    }

    void SpawnCollisionItems::_setCollisionParentPoseToWorld()
    {
        Eigen::Translation3f translation = Eigen::Translation3f::Identity();
        Eigen::Quaternionf quaternion = Eigen::Quaternionf::Identity();
        Eigen::Affine3f pose = translation * quaternion;
        CoppeliaUtils::setObjectPose(_collisions_parent_handle, _world_handle, pose);
    }

    void SpawnCollisionItems::_checkLastMessagesTimestamps(const builtin_interfaces::msg::Time &merged_items_stamp, const builtin_interfaces::msg::Time &filtered_octomap_stamp)
    {
        if (_last_merged_items_msg_timestamp == merged_items_stamp)
            RCLCPP_WARN(get_logger(), "New message with merged items has not arrived yet. Processing old message.");
        _last_merged_items_msg_timestamp = merged_items_stamp;

        if (_last_filtered_octomap_msg_timestamp == filtered_octomap_stamp)
            RCLCPP_WARN(get_logger(), "New message with filtered scene octomap has not arrived yet. Processing old message.");
        _last_filtered_octomap_msg_timestamp = filtered_octomap_stamp;
    }

}
SIM_PLUGIN(PLUGIN_NAME, PLUGIN_VERSION, plugin_spawn_collision_items::SpawnCollisionItems)
#include "stubsPlusPlus.cpp"
