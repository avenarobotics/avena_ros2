#include "spawn_methods/spawn_knife.hpp"
namespace plugin_spawn_collision_items
{
    SpawnKnife::SpawnKnife()
        : BaseSpawnMethod()
    {
    }

    SpawnKnife::~SpawnKnife()
    {
    }

    int SpawnKnife::spawn(ItemElement &item_element)
    {
        KnifeData knife_data = _readData(item_element);
        std::string knife_name = "knife";//item_element.label;

        simInt knife_handle = _spawn_models_handles[knife_name];
        knife_handle = CoppeliaUtils::copyObject(knife_handle);
        if (knife_handle == -1)
            return -1;

        CoppeliaUtils::setObjectPosition(knife_handle, _world_handle, knife_data.knife_position);
        CoppeliaUtils::setObjectQuaternion(knife_handle, _world_handle, knife_data.knife_orientation);

        return knife_handle;
    }

    SpawnKnife::KnifeData SpawnKnife::_readData(ItemElement &item_element)
    {
        KnifeData knife_data;
        knife_data.knife_position = Eigen::Vector3f(item_element.position["x"], item_element.position["y"], item_element.position["z"]);
        knife_data.knife_orientation = Eigen::Quaternionf(item_element.orientation["w"], item_element.orientation["x"], item_element.orientation["y"], item_element.orientation["z"]);
        return knife_data;
    }
}
