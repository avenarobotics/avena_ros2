#include "spawn_methods/spawn_primitive.hpp"

namespace plugin_spawn_collision_items
{
    SpawnPrimitive::SpawnPrimitive()
        : BaseSpawnMethod()
    {
    }

    SpawnPrimitive::~SpawnPrimitive()
    {
    }

    Handle SpawnPrimitive::spawn(const json &part_description, const PartAdditionalData &additional_data)
    {
        if (part_description.empty())
            return -1;

        auto obj_size = _assignObjectSizes(part_description);
        if (obj_size.empty())
            return -1;

        Handle shape_handle = -1;
        // It is faster to create box pure shape every time than copy model inside the scene
        if (part_description["spawn_method"] == "box")
            shape_handle = simCreatePureShape(_shape_type.at(part_description["spawn_method"]), _shape_option_static, obj_size.data(), 1, nullptr);
        else
        {
            simInt shape_handle_to_copy = _spawn_models_handles[part_description["spawn_method"]];
            shape_handle = CoppeliaUtils::copyObject(shape_handle_to_copy);
            if (shape_handle == -1)
                return -1;
            simScaleObject(shape_handle, obj_size[0], obj_size[1], obj_size[2], 0);
        }

        json part_position = part_description["pose"]["position"];
        json part_orientation = part_description["pose"]["orientation"];

        Eigen::Translation3f translation(part_position["x"], part_position["y"], part_position["z"]);
        Eigen::Quaternionf quaternion(part_orientation["w"], part_orientation["x"], part_orientation["y"], part_orientation["z"]);
        Eigen::Affine3f pose = translation * quaternion;

        CoppeliaUtils::setObjectPose(shape_handle, additional_data.item_handle, pose);

        return shape_handle;
    }

    std::vector<float> SpawnPrimitive::_assignObjectSizes(const json &part_description)
    {
        ShapeType_e shape = _shape_type.at(part_description["spawn_method"]);
        const json json_dims = part_description["dims"];
        std::vector<float> dims;
        switch (shape)
        {
        case 0:
            dims = {json_dims["x"].get<float>(), json_dims["y"].get<float>(), json_dims["z"].get<float>()};
            break;
        case 1:
            dims = {json_dims["radius"].get<float>() * 2.0f, json_dims["radius"].get<float>() * 2.0f, json_dims["radius"].get<float>() * 2.0f};
            break;
        case 2:
            dims = {json_dims["radius"].get<float>() * 2.0f, json_dims["radius"].get<float>() * 2.0f, json_dims["height"].get<float>()};
            break;
        default:
            RCLCPP_WARN(LOGGER, "Unsupported shape type");
            break;
        }
        return dims;
    }
}
