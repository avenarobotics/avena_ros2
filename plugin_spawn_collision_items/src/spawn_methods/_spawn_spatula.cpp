#include "spawn_methods/spawn_spatula.hpp"
namespace plugin_spawn_collision_items
{
    SpawnSpatula::SpawnSpatula()
        : BaseSpawnMethod()
    {
    }

    SpawnSpatula::~SpawnSpatula()
    {
    }

    int SpawnSpatula::spawn(ItemElement &item_element)
    {
        SpatulaData spatula_data = _readData(item_element);
        std::string spatula_name = "spatula";//item_element.label;

        simInt spatula_handle = _spawn_models_handles[spatula_name];
        spatula_handle = CoppeliaUtils::copyObject(spatula_handle);
        if (spatula_handle == -1)
            return -1;

        CoppeliaUtils::setObjectPosition(spatula_handle, _world_handle, spatula_data.spatula_position);
        CoppeliaUtils::setObjectQuaternion(spatula_handle, _world_handle, spatula_data.spatula_orientation);

        return spatula_handle;
    }

    SpawnSpatula::SpatulaData SpawnSpatula::_readData(ItemElement &item_element)
    {
        SpatulaData spatula_data;
        spatula_data.spatula_position = Eigen::Vector3f(item_element.position["x"], item_element.position["y"], item_element.position["z"]);
        spatula_data.spatula_orientation = Eigen::Quaternionf(item_element.orientation["w"], item_element.orientation["x"], item_element.orientation["y"], item_element.orientation["z"]);
        return spatula_data;
    }
}
