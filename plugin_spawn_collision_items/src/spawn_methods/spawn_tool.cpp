#include "spawn_methods/spawn_tool.hpp"

namespace plugin_spawn_collision_items
{
    SpawnTool::SpawnTool()
        : BaseSpawnMethod()
    {
    }

    SpawnTool::~SpawnTool()
    {
    }

    Handle SpawnTool::spawn(const json &part_description, const PartAdditionalData &additional_data)
    {
        std::string tool_name = additional_data.item_label;
        simInt tool_handle = _spawn_models_handles[tool_name];
        tool_handle = CoppeliaUtils::copyObject(tool_handle);
        if (tool_handle == -1)
            return -1;

        json part_position = part_description["pose"]["position"];
        json part_orientation = part_description["pose"]["orientation"];

        Eigen::Translation3f translation(part_position["x"], part_position["y"], part_position["z"]);
        Eigen::Quaternionf quaternion(part_orientation["w"], part_orientation["x"], part_orientation["y"], part_orientation["z"]);
        Eigen::Affine3f pose = translation * quaternion;

        CoppeliaUtils::setObjectPose(tool_handle, additional_data.item_handle, pose);

        return tool_handle;
    }
}
