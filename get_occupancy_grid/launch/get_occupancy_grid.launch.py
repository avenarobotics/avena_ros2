import launch
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode


def generate_launch_description():
    """Generate launch description with get occupancy grid component."""
    container = ComposableNodeContainer(
            name='get_occupancy_grid_container',
            namespace='',
            package='rclcpp_components',
            executable='component_container',
            composable_node_descriptions=[
                ComposableNode(
                    package='get_occupancy_grid',
                    plugin='get_occupancy_grid::GetOccupancyGrid',
                    name='get_occupancy_grid'),
            ],
            output='screen',
    )

    return launch.LaunchDescription([container])