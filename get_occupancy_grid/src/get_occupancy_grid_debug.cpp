#include "get_occupancy_grid/get_occupancy_grid_debug.hpp"

#include <iostream>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include <rclcpp/parameter.hpp>

namespace ros2mysql
{

    GetOccupancyGrid::GetOccupancyGrid(const rclcpp::NodeOptions &options)
        : Node("get_occupancy_grid_debug", options)
    {
        // Declare parameters.
        this->initialize_parameters();

        this->configure();

        try
        {
            this->db_ = std::make_unique<MysqlConnector>(this->host_, this->port_, this->db_name_, this->username_, this->password_, this->debug_);
        }
        catch (const network_exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
        }

        auto callback = [this](const typename custom_interfaces::msg::OccupancyGrid::SharedPtr msg) -> void {
            RCLCPP_INFO(get_logger(), "Occupancy grid data callback called.");

            // Validate input message
            if (msg->header.stamp == builtin_interfaces::msg::Time())
                return;

            try
            {
                std::vector<uint32_t> scene_ids = db_->getRowsId("scene");
                scene_t scene_data;
                if (scene_ids.size() > 0)
                    scene_data.scene_id = scene_ids.back();

                cv::Mat occupancy_grid;
                helpers::converters::rosImageToCV(msg->occupancy_grid_ros,occupancy_grid);
                helpers::converters::imageToStream(occupancy_grid,scene_data.occupancy_grid);
                // binary version
                // helpers::converters::stringToBinaryMask(msg->occupancy_grid, occupancy_grid);
                // helpers::converters::binaryMaskToStream(occupancy_grid, scene_data.occupancy_grid);

                db_->setScene(&scene_data);
            }
            catch (const network_exception &e)
            {
                RCLCPP_ERROR(this->get_logger(), e.what());
            }
            catch (const query_exception &e)
            {
                RCLCPP_ERROR(this->get_logger(), e.what());
            }
        };

        rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        sub_ = create_subscription<custom_interfaces::msg::OccupancyGrid>("occupancy_grid", latching_qos, callback);
    }

    void GetOccupancyGrid::configure()
    {
        this->get_parameter<std::string>("host", host_);
        this->get_parameter<std::string>("port", port_);
        this->get_parameter<std::string>("db_name", db_name_);
        this->get_parameter<std::string>("username", username_);
        this->get_parameter<std::string>("password", password_);
        this->get_parameter<bool>("debug", debug_);
    }

    void GetOccupancyGrid::initialize_parameters()
    {
        rcl_interfaces::msg::ParameterDescriptor host_descriptor;
        host_descriptor.name = "host";
        host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("host", "", host_descriptor);

        rcl_interfaces::msg::ParameterDescriptor port_descriptor;
        port_descriptor.name = "port";
        port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("port", "", port_descriptor);

        rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
        db_name_descriptor.name = "db_name";
        db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("db_name", "", db_name_descriptor);

        rcl_interfaces::msg::ParameterDescriptor username_descriptor;
        username_descriptor.name = "username";
        username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("username", "", username_descriptor);

        rcl_interfaces::msg::ParameterDescriptor password_descriptor;
        password_descriptor.name = "password";
        password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("password", "", password_descriptor);

        rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
        debug_descriptor.name = "debug";
        debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
        this->declare_parameter("debug", false, debug_descriptor);
    }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::GetOccupancyGrid)
