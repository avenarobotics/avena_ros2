#include "get_occupancy_grid/get_occupancy_grid.hpp"

namespace get_occupancy_grid
{

    GetOccupancyGrid::GetOccupancyGrid(const rclcpp::NodeOptions &options)
        : Node("get_occupancy_grid", options),
          _merged_point_cloud(new pcl::PointCloud<pcl::PointXYZ>)
    {
        // setvbuf(stdout, NULL, _IONBF, BUFSIZ); // this allows to use std::cout without buffering
        RCLCPP_INFO(get_logger(), "Initializing get occupancy grid.");

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        _initializeSubscribers(qos_settings);
        qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        _publisher_occupancy_grid = create_publisher<GetOccupancyGridMsg>("occupancy_grid", qos_settings);

        _action_server = rclcpp_action::create_server<GetOccupancyGridAction>(
            this,
            "get_occupancy_grid",
            std::bind(&GetOccupancyGrid::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&GetOccupancyGrid::_handleCancel, this, std::placeholders::_1),
            std::bind(&GetOccupancyGrid::_handleAccepted, this, std::placeholders::_1));

        _readAreasParameters();
        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        status = custom_interfaces::msg::Heartbeat::STOPPED;
        RCLCPP_INFO(get_logger(), "...done");
    }

    void GetOccupancyGrid::initNode()
    {
        status = custom_interfaces::msg::Heartbeat::STARTING;

        status = custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void GetOccupancyGrid::shutDownNode()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;

        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void GetOccupancyGrid::_initializeSubscribers(const rclcpp::QoS &qos_settings)
    {
        _merged_ptcld_filtered_sub = create_subscription<custom_interfaces::msg::MergedPtcldFiltered>(
            "merged_ptcld_filtered", qos_settings,
            [this](custom_interfaces::msg::MergedPtcldFiltered::SharedPtr merged_ptcld_filtered_msg)
            {
                RCLCPP_DEBUG(get_logger(), "Merged point cloud filtered message received");
                _merged_ptcld_filtered_msg = merged_ptcld_filtered_msg;
            });
        _merged_items_sub = create_subscription<custom_interfaces::msg::Items>(
            "merged_items", qos_settings,
            [this](custom_interfaces::msg::Items::SharedPtr items_msg)
            {
                RCLCPP_DEBUG(get_logger(), "Merged items message received");
                _items_msg = items_msg;
            });
    }

    void GetOccupancyGrid::_checkLastProcessedMessagesTimestamps()
    {
        if (_last_merged_items_msg_timestamp == _items_msg->header.stamp)
            RCLCPP_WARN(get_logger(), "New message with merged items has not arrived yet. Processing old message.");
        _last_merged_items_msg_timestamp = _items_msg->header.stamp;

        if (_last_merged_ptcld_filtered_msg_timestamp == _merged_ptcld_filtered_msg->header.stamp)
            RCLCPP_WARN(get_logger(), "New message with merged point cloud filtered has not arrived yet. Processing old message.");
        _last_merged_ptcld_filtered_msg_timestamp = _merged_ptcld_filtered_msg->header.stamp;
    }

    rclcpp_action::GoalResponse GetOccupancyGrid::_handleGoal(const rclcpp_action::GoalUUID &, std::shared_ptr<const GetOccupancyGridAction::Goal>)
    {
        RCLCPP_INFO(get_logger(), "Received goal request");
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse GetOccupancyGrid::_handleCancel(const std::shared_ptr<GoalHandleGetOccupancyGrid>)
    {
        RCLCPP_INFO(get_logger(), "Received request to cancel goal");
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void GetOccupancyGrid::_handleAccepted(const std::shared_ptr<GoalHandleGetOccupancyGrid> goal_handle)
    {
        std::thread{std::bind(&GetOccupancyGrid::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    void GetOccupancyGrid::_execute(const std::shared_ptr<GoalHandleGetOccupancyGrid> goal_handle)
    {
        auto result = std::make_shared<GetOccupancyGridAction::Result>();

        if (status != custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
            goal_handle->abort(result);
            return;
        }

        RCLCPP_INFO(get_logger(), "Executing goal");

        if (_validateInput())
        {
            RCLCPP_ERROR(get_logger(), "Invalid input data. Goal failed");
            _publisher_occupancy_grid->publish(custom_interfaces::msg::OccupancyGrid());
            goal_handle->abort(result);
            return;
        }
        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(_merged_ptcld_filtered_msg->merged_ptcld_filtered, _merged_point_cloud);
        _checkLastProcessedMessagesTimestamps();

        auto abort_goal = [this, goal_handle, result](std::string error_message) -> void
        {
            RCLCPP_ERROR(get_logger(), error_message);
            if (rclcpp::ok())
            {
                _publisher_occupancy_grid->publish(custom_interfaces::msg::OccupancyGrid());
                goal_handle->abort(result);
                RCLCPP_INFO(this->get_logger(), "Goal exited with failure.");
            }
            // else
            //     throw std::runtime_error("Problem with ROS");
        };

        if (_merged_point_cloud->size() == 0)
        {
            abort_goal("Point cloud of the scene is empty");
            return;
        }

        cv::Mat occupancy_grid;
        if (_calculateOccupancyGrid(_items_msg, _merged_point_cloud, _grid_size, occupancy_grid))
        {
            abort_goal("Error occured while creating occupancy grid.");
            return;
        }
        custom_interfaces::msg::OccupancyGrid::SharedPtr occupancy_grid_msg = _prepareOutputMsg(occupancy_grid);
        if (!occupancy_grid_msg)
        {
            abort_goal("Error occured while preparing output message.");
            return;
        }

        _publisher_occupancy_grid->publish(*occupancy_grid_msg);

        if (rclcpp::ok())
        {
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded.");
        }
        // else
        //     throw std::runtime_error("Problem with ROS");
    }

    custom_interfaces::msg::OccupancyGrid::SharedPtr GetOccupancyGrid::_prepareOutputMsg(cv::Mat &occupancy_grid)
    {
        custom_interfaces::msg::OccupancyGrid::SharedPtr occupancy_grid_msg(new custom_interfaces::msg::OccupancyGrid);
        occupancy_grid_msg->header.frame_id = "world";
        occupancy_grid_msg->header.stamp = now();
        int return_code = helpers::converters::cvMatToRos(occupancy_grid, occupancy_grid_msg->occupancy_grid_ros);
        RCLCPP_INFO(this->get_logger(), "_prepareOutputMsg succeeded.");

        // int return_code = helpers::converters::binaryMaskToString(occupancy_grid, occupancy_grid_msg->occupancy_grid);

        // occupancy_grid_msg->cam1_rgb = _input_msg_data->cam1_rgb;
        // occupancy_grid_msg->cam2_rgb = _input_msg_data->cam2_rgb;
        // occupancy_grid_msg->cam1_depth = _input_msg_data->cam1_depth;
        // occupancy_grid_msg->cam2_depth = _input_msg_data->cam2_depth;
        // occupancy_grid_msg->merged_ptcld_filtered = _input_msg_data->merged_ptcld_filtered;
        // occupancy_grid_msg->cam1_masks = _input_msg_data->cam1_masks;
        // occupancy_grid_msg->cam1_labels = _input_msg_data->cam1_labels;
        // occupancy_grid_msg->cam2_masks = _input_msg_data->cam2_masks;
        // occupancy_grid_msg->cam2_labels = _input_msg_data->cam2_labels;
        // occupancy_grid_msg->items = _input_msg_data->items;

        return return_code == 0 ? occupancy_grid_msg : nullptr;
    }

    int GetOccupancyGrid::_calculateOccupancyGrid(custom_interfaces::msg::Items::SharedPtr items_msg, pcl::PointCloud<pcl::PointXYZ>::Ptr ptcld, float leaf_size, cv::Mat &out_occ_grid)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr scene_cloud(new pcl::PointCloud<pcl::PointXYZ>);
        *scene_cloud = *ptcld;

        //filter the points below the table and on the edges
        // float safety_area_offset = 0.1;
        float safety_area_offset_clean_edge = 0.08;
        helpers::vision::passThroughFilter(scene_cloud, "x", _workspace_area.x_min + safety_area_offset_clean_edge, _workspace_area.x_max - safety_area_offset_clean_edge, false);
        helpers::vision::passThroughFilter(scene_cloud, "y", _workspace_area.y_min + safety_area_offset_clean_edge, _workspace_area.y_max - safety_area_offset_clean_edge, false);
        helpers::vision::passThroughFilter(scene_cloud, "z", _workspace_area.z_min - 0.01, _workspace_area.z_max, false);
        // helpers::vision::passThroughFilter(scene_cloud, "x", _workspace_area.x_min + safety_area_offset, _workspace_area.x_max - safety_area_offset, false);
        // helpers::vision::passThroughFilter(scene_cloud, "y", _workspace_area.y_min + safety_area_offset, _workspace_area.y_max - safety_area_offset, false);
        // helpers::vision::passThroughFilter(scene_cloud, "z", _workspace_area.z_min - 0.01, _workspace_area.z_max, false);

        // Items
        // cv::Mat items_occ_grid;
        const float x_length = _workspace_area.x_max - _workspace_area.x_min;
        const float y_length = _workspace_area.y_max - _workspace_area.y_min;
        cv::Mat container_occ_grid = cv::Mat::zeros(std::ceil(x_length / leaf_size), std::ceil(y_length / leaf_size), CV_8UC1);
        _fillContainerData(items_msg, _grid_size, container_occ_grid);
        // cv::namedWindow("occ_grid_container", cv::WINDOW_GUI_NORMAL);
        // cv::imshow("occ_grid_container", container_occ_grid);
        // cv::waitKey(0);
        // _itemsOccupancyGrid(scene_cloud, leaf_size, container_occ_grid);
        cv::Mat table_occ_grid = cv::Mat::zeros(std::ceil(x_length / leaf_size), std::ceil(y_length / leaf_size), CV_8UC1);
        _tableOccupancyGrid(scene_cloud, leaf_size, table_occ_grid);

        // int top, bottom, left, right;
        // top = (int)(10);
        // bottom = top;
        // left = (int)(10);
        // right = left;
        // cv::Scalar value( 255, 255, 255 );
        // int borderType = cv::BORDER_CONSTANT;
        // cv::copyMakeBorder( table_occ_grid, table_occ_grid, top, bottom, left, right, borderType, value );

        cv::Rect border(cv::Point(0, 0), table_occ_grid.size());
        cv::Scalar color(255, 255, 255);
        int thickness = 10;

        cv::rectangle(table_occ_grid, border, color, thickness);
        // cv::namedWindow("occ_grid_table", cv::WINDOW_GUI_NORMAL);
        // cv::imshow("occ_grid_table", table_occ_grid);
        // cv::waitKey(0);

        // Combine occupancy grid from items and holes in table occupancy grids
        // std::cout << table_occ_grid.rows << container_occ_grid.rows << std::endl;
        // std::cout << table_occ_grid.cols << container_occ_grid.cols << std::endl;
        cv::bitwise_or(table_occ_grid, container_occ_grid, out_occ_grid);
        // cv::bitwise_or(out_occ_grid,container_occ_grid,)
        // cv::namedWindow("occ_grid_final", cv::WINDOW_GUI_NORMAL);
        // cv::imshow("occ_grid_final", out_occ_grid);
        // cv::waitKey(0);
        // cv::destroyAllWindows();

        return 0;
    }

    int GetOccupancyGrid::_itemsOccupancyGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr scene_cloud, float leaf_size, cv::Mat &out_items_occ_grid)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr items_cloud(new pcl::PointCloud<pcl::PointXYZ>);
        *items_cloud = *scene_cloud;
        helpers::vision::passThroughFilter(items_cloud, "z", _workspace_area.z_min + 0.0025, _workspace_area.z_max, false);
        items_cloud->getMatrixXfMap().row(2) = Eigen::VectorXf::Zero(items_cloud->size());
        _getInitialOccupancyGrid(items_cloud, leaf_size, out_items_occ_grid);

        cv::Mat kernel = cv::getStructuringElement(cv::MorphShapes::MORPH_RECT, cv::Size(3, 3));
        cv::dilate(out_items_occ_grid, out_items_occ_grid, kernel);
        cv::erode(out_items_occ_grid, out_items_occ_grid, kernel);
        return 0;
    }

    int GetOccupancyGrid::_tableOccupancyGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr scene_cloud, float leaf_size, cv::Mat &out_table_occ_grid)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr table_cloud(new pcl::PointCloud<pcl::PointXYZ>);
        *table_cloud = *scene_cloud;
        table_cloud->getMatrixXfMap().row(2) = Eigen::VectorXf::Zero(table_cloud->size());

        _getInitialOccupancyGrid(table_cloud, leaf_size, out_table_occ_grid);

        // cv::Mat kernel = cv::getStructuringElement(cv::MorphShapes::MORPH_RECT, cv::Size(3, 3));
        // cv::dilate(out_table_occ_grid, out_table_occ_grid, kernel);
        // cv::erode(out_table_occ_grid, out_table_occ_grid, kernel);
        cv::bitwise_not(out_table_occ_grid, out_table_occ_grid);
        float shadow_ratio = static_cast<float>(cv::countNonZero(out_table_occ_grid)) / static_cast<float>(out_table_occ_grid.total());
        if (cv::countNonZero(out_table_occ_grid) > 0.5 * out_table_occ_grid.total())
        {
            RCLCPP_WARN_STREAM(this->get_logger(), "More than " << shadow_ratio * 100 << "% of the table is not visible from cameras ");
        }
        return 0;
    }

    int GetOccupancyGrid::_getInitialOccupancyGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud, float leaf_size, cv::Mat &out_occ_grid)
    {
        std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ>> cloud_voxel_centers;
        _getOccupiedVoxelCenters(input_cloud, leaf_size, cloud_voxel_centers);

        // const float x_length = _workspace_area.x_max - _workspace_area.x_min;
        // const float y_length = _workspace_area.y_max - _workspace_area.y_min;
        // out_occ_grid = cv::Mat::zeros(std::ceil(x_length / leaf_size), std::ceil(y_length / leaf_size), CV_8UC1);
        for (pcl::PointXYZ &voxel : cloud_voxel_centers)
        {
            int x = std::floor((voxel.x + std::abs(_workspace_area.x_min)) / leaf_size);
            int y = std::floor((voxel.y + std::abs(_workspace_area.y_min)) / leaf_size);
            if (out_occ_grid.at<uint8_t>(x, y) > 0)
                continue;
            else
                out_occ_grid.at<uint8_t>(x, y) = 255;
        }
        return 0;
    }
    int GetOccupancyGrid::_fillContainerData(custom_interfaces::msg::Items::SharedPtr items_msg, float leaf_size, cv::Mat &out_occ_grid)
    {
        cv::Mat occupancy_grid_container(out_occ_grid.rows, out_occ_grid.cols, CV_8U, 0.0);
        // filling occ grid with containers
        for (auto &item : items_msg->items)
        { //                 bowl          cutting_board           plate
            if (item.label == "bowl" || item.label == "cutting_board" || item.label == "plate")
            {
                pcl::PointCloud<pcl::PointXYZ>::Ptr containers_cloud(new pcl::PointCloud<pcl::PointXYZ>);
                for (auto &item_element : item.item_elements)
                {
                    pcl::PointCloud<pcl::PointXYZ>::Ptr item_element_ptcld(new pcl::PointCloud<pcl::PointXYZ>);

                    helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.merged_ptcld, item_element_ptcld);
                    *containers_cloud += *item_element_ptcld;
                }
                _cropPointCloudToTable(containers_cloud);
                //create octree and set occupied voxels on occupancy grid mat
                std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ>> cloud_voxel_centers;
                _getOccupiedVoxelCenters(containers_cloud, leaf_size, cloud_voxel_centers);
                if (item.id > 255)
                {
                    RCLCPP_WARN(get_logger(), "Item ID is out of range (bigger than 255)");
                    break;
                }
                for (pcl::PointXYZ &voxel : cloud_voxel_centers)
                {
                    int x = std::floor((voxel.x + std::abs(_workspace_area.x_min)) / leaf_size);
                    int y = std::floor((voxel.y + std::abs(_workspace_area.y_min)) / leaf_size);
                    out_occ_grid.at<uint8_t>(x, y) = item.id;
                }
            }
        }
        // fills holes in ptcld of containers
        cv::Mat kernel_1 = cv::getStructuringElement(cv::MorphShapes::MORPH_RECT, cv::Size(3, 3));
        cv::dilate(out_occ_grid, out_occ_grid, kernel_1);
        cv::erode(out_occ_grid, out_occ_grid, kernel_1);
        // get the edges of containers
        cv::Mat occupancy_grid_edge(out_occ_grid.rows, out_occ_grid.cols, CV_8U, 0.0);
        cv::dilate(out_occ_grid, occupancy_grid_container, kernel_1);
        // extract the edge
        cv::bitwise_xor(out_occ_grid, occupancy_grid_container, occupancy_grid_edge);

        // cv::namedWindow("occupancy_grid_container", cv::WINDOW_GUI_NORMAL);
        // cv::imshow("occupancy_grid_container", out_occ_grid);
        // cv::waitKey(0);
        cv::threshold(occupancy_grid_edge, occupancy_grid_edge, 0.5, 255, cv::THRESH_BINARY);

        // cv::namedWindow("occupancy_grid_edge", cv::WINDOW_GUI_NORMAL);
        // cv::imshow("occupancy_grid_edge", occupancy_grid_edge);
        // cv::waitKey(0);

        cv::Mat kernel = cv::Mat::ones(3, 3, CV_8U);
        cv::bitwise_or(out_occ_grid, occupancy_grid_edge, out_occ_grid);

        // cv::namedWindow("occupancy_grid_container", cv::WINDOW_GUI_NORMAL);
        // cv::imshow("occupancy_grid_container", out_occ_grid);
        // cv::waitKey(0);
        // container data is filled

        // filling items ptcld
        for (auto &item : items_msg->items)
        { //                 bowl          cutting_board           plate
            if (item.label == "bowl" || item.label == "cutting_board" || item.label == "plate")
                continue;
            else
            {
                pcl::PointCloud<pcl::PointXYZ>::Ptr containers_cloud(new pcl::PointCloud<pcl::PointXYZ>);
                for (auto &item_element : item.item_elements)
                {
                    pcl::PointCloud<pcl::PointXYZ>::Ptr item_element_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
                    helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.merged_ptcld, item_element_ptcld);
                    *containers_cloud += *item_element_ptcld;
                }
                _cropPointCloudToTable(containers_cloud);
                //create octree and set occupied voxels on occupancy grid mat
                std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ>> cloud_voxel_centers;
                _getOccupiedVoxelCenters(containers_cloud, leaf_size, cloud_voxel_centers);
                for (pcl::PointXYZ &voxel : cloud_voxel_centers)
                {
                    int x = std::floor((voxel.x + std::abs(_workspace_area.x_min)) / leaf_size);
                    int y = std::floor((voxel.y + std::abs(_workspace_area.y_min)) / leaf_size);
                    out_occ_grid.at<uint8_t>(x, y) = 255;
                }
            }
        }

        // cv::namedWindow("occ_grid_new_aproach", cv::WINDOW_GUI_NORMAL);
        // cv::imshow("occ_grid_new_aproach", out_occ_grid);
        // cv::waitKey(0);
        // cv::destroyAllWindows();
        RCLCPP_INFO(get_logger(), "Container and item Data filled");
        return 0;
    }

    void GetOccupancyGrid::_readAreasParameters()
    {
        try
        {
            json area = helpers::commons::getParameter("areas");
            _workspace_area.x_min = area["table_area"]["min"]["x"].get<float>();
            _workspace_area.y_min = area["table_area"]["min"]["y"].get<float>();
            _workspace_area.z_min = area["table_area"]["min"]["z"].get<float>();

            _workspace_area.x_max = area["table_area"]["max"]["x"].get<float>();
            _workspace_area.y_max = area["table_area"]["max"]["y"].get<float>();
            _workspace_area.z_max = area["table_area"]["max"]["z"].get<float>();

            json grid_size = helpers::commons::getParameter("get_occupancy_grid");
            _grid_size = grid_size["occ_grid_leaf_size"];
        }
        catch (const json::exception &e)
        {
            RCLCPP_FATAL_STREAM(get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
            throw std::runtime_error("Get occupancy grid reading grid size from JSON error.");
        }
    }

    int GetOccupancyGrid::_validateInput()
    {
        if (!_merged_ptcld_filtered_msg)
        {
            RCLCPP_WARN(get_logger(), "Module has not received data yet.");
            return 1;
        }
        if (_merged_ptcld_filtered_msg->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(get_logger(), "Invalid input message header.");
            return 1;
        }
        if (!_items_msg)
        {
            RCLCPP_WARN(get_logger(), "Module has not received data yet.");
            return 1;
        }
        if (_items_msg->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(get_logger(), "Invalid input message header.");
            return 1;
        }
        return 0;
    }

    int GetOccupancyGrid::_cropPointCloudToTable(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
    {
        if (helpers::vision::passThroughFilter(cloud, "x", _workspace_area.x_min + _grid_size, _workspace_area.x_max - _grid_size))
            return 1;
        if (helpers::vision::passThroughFilter(cloud, "y", _workspace_area.y_min + _grid_size, _workspace_area.y_max - _grid_size))
            return 1;
        return 0;
    }

    int GetOccupancyGrid::_getOccupiedVoxelCenters(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, const float &leaf_size, std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ>> &out_cloud_voxel_centers)
    {
        pcl::octree::OctreePointCloudVoxelCentroid<pcl::PointXYZ> octree(leaf_size);
        octree.setInputCloud(cloud);
        octree.addPointsFromInputCloud();
        octree.getOccupiedVoxelCenters(out_cloud_voxel_centers);
        return 0;
    }

} // namespace get_occupancy_grid

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(get_occupancy_grid::GetOccupancyGrid)
