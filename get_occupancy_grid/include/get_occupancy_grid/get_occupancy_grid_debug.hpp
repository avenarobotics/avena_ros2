// Copyright 2016 Open Source Robotics Foundation, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef GET_OCCUPANCY_GRID_DEBUG__COMPONENT_HPP_
#define GET_OCCUPANCY_GRID_DEBUG__COMPONENT_HPP_

#include "get_occupancy_grid/visibility_control.h"
#include "rclcpp/rclcpp.hpp"
#include <mysql_connector.h>
#include <assert.h>
#include <sstream>

#include "custom_interfaces/msg/occupancy_grid.hpp"
// #include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"

#include <memory>

namespace ros2mysql
{

  class GetOccupancyGrid : public rclcpp::Node
  {
  public:
    GET_OCCUPANCY_GRID_PUBLIC
    explicit GetOccupancyGrid(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

  private:
    /**;
     * Configures component.
     *
     * Declares parameters and configures video capture.
     */
    GET_OCCUPANCY_GRID_LOCAL
    void configure();

    /**;
     * Declares the parameter using rcl_interfaces.
     */
    GET_OCCUPANCY_GRID_LOCAL
    void initialize_parameters();

    std::unique_ptr<MysqlConnector> db_;
    std::string host_;
    std::string port_;
    std::string db_name_;
    std::string username_;
    std::string password_;
    bool debug_;
    rclcpp::Subscription<custom_interfaces::msg::OccupancyGrid>::SharedPtr sub_;
    // helpers::SubscriptionsManager::SharedPtr _sub_manager;

  };

} // namespace ros2mysql

#endif // DETECT_DEBUG__COMPONENT_HPP_
