#ifndef GET_OCCUPANCY_GRID__VISIBILITY_CONTROL_H_
#define GET_OCCUPANCY_GRID__VISIBILITY_CONTROL_H_

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define GET_OCCUPANCY_GRID_EXPORT __attribute__ ((dllexport))
    #define GET_OCCUPANCY_GRID_IMPORT __attribute__ ((dllimport))
  #else
    #define GET_OCCUPANCY_GRID_EXPORT __declspec(dllexport)
    #define GET_OCCUPANCY_GRID_IMPORT __declspec(dllimport)
  #endif
  #ifdef GET_OCCUPANCY_GRID_BUILDING_LIBRARY
    #define GET_OCCUPANCY_GRID_PUBLIC GET_OCCUPANCY_GRID_EXPORT
  #else
    #define GET_OCCUPANCY_GRID_PUBLIC GET_OCCUPANCY_GRID_IMPORT
  #endif
  #define GET_OCCUPANCY_GRID_PUBLIC_TYPE GET_OCCUPANCY_GRID_PUBLIC
  #define GET_OCCUPANCY_GRID_LOCAL
#else
  #define GET_OCCUPANCY_GRID_EXPORT __attribute__ ((visibility("default")))
  #define GET_OCCUPANCY_GRID_IMPORT
  #if __GNUC__ >= 4
    #define GET_OCCUPANCY_GRID_PUBLIC __attribute__ ((visibility("default")))
    #define GET_OCCUPANCY_GRID_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define GET_OCCUPANCY_GRID_PUBLIC
    #define GET_OCCUPANCY_GRID_LOCAL
  #endif
  #define GET_OCCUPANCY_GRID_PUBLIC_TYPE
#endif

#endif  // GET_OCCUPANCY_GRID__VISIBILITY_CONTROL_H_
