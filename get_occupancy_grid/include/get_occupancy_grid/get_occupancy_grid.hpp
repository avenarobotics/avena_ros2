#ifndef GET_OCCUPANCY_GRID__OCCUPANCY_GRID_HPP_
#define GET_OCCUPANCY_GRID__OCCUPANCY_GRID_HPP_

// ___ROS___
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

// ___PCL___
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/octree/octree_pointcloud_voxelcentroid.h>

// ___Avena___
#include "custom_interfaces/msg/occupancy_grid.hpp"
// #include "custom_interfaces/msg/scene.hpp"
// #include "custom_interfaces/msg/estimate_shape.hpp"
#include "custom_interfaces/action/simple_action.hpp"
#include "get_occupancy_grid/visibility_control.h"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"

#include "custom_interfaces/msg/merged_ptcld_filtered.hpp"
#include "custom_interfaces/msg/items.hpp"

namespace get_occupancy_grid
{
  using json = nlohmann::json;

  struct WorkspaceArea
  {
    float x_min;
    float y_min;
    float z_min;

    float x_max;
    float y_max;
    float z_max;
  };

  class GetOccupancyGrid : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    // Action
    using GetOccupancyGridAction = custom_interfaces::action::SimpleAction;
    using GoalHandleGetOccupancyGrid = rclcpp_action::ServerGoalHandle<GetOccupancyGridAction>;
    // Message
    using GetOccupancyGridMsg = custom_interfaces::msg::OccupancyGrid;
    // using InputMsg = custom_interfaces::msg::EstimateShape;

    GET_OCCUPANCY_GRID_PUBLIC
    explicit GetOccupancyGrid(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    void initNode() override;
    void shutDownNode() override;

  private:
    helpers::Watchdog::SharedPtr _watchdog;

    GET_OCCUPANCY_GRID_LOCAL
    rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const GetOccupancyGridAction::Goal> goal);

    GET_OCCUPANCY_GRID_LOCAL
    rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandleGetOccupancyGrid> goal_handle);

    GET_OCCUPANCY_GRID_LOCAL
    void _handleAccepted(const std::shared_ptr<GoalHandleGetOccupancyGrid> goal_handle);

    GET_OCCUPANCY_GRID_LOCAL
    void _execute(const std::shared_ptr<GoalHandleGetOccupancyGrid> goal_handle);

    GET_OCCUPANCY_GRID_LOCAL
    int _calculateOccupancyGrid(custom_interfaces::msg::Items::SharedPtr items_msg, pcl::PointCloud<pcl::PointXYZ>::Ptr ptcld, float leaf_size, cv::Mat &out_occ_grid);

    GET_OCCUPANCY_GRID_LOCAL
    int _itemsOccupancyGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr scene_cloud, float leaf_size, cv::Mat &out_items_occ_grid);

    GET_OCCUPANCY_GRID_LOCAL
    int _tableOccupancyGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr scene_cloud, float leaf_size, cv::Mat &out_table_occ_grid);

    GET_OCCUPANCY_GRID_LOCAL
    int _getInitialOccupancyGrid(pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloud, float leaf_size, cv::Mat &out_occ_grid);

    GET_OCCUPANCY_GRID_LOCAL
    custom_interfaces::msg::OccupancyGrid::SharedPtr _prepareOutputMsg(cv::Mat &occupancy_grid);

    GET_OCCUPANCY_GRID_LOCAL
    void _readAreasParameters();

    GET_OCCUPANCY_GRID_LOCAL
    int _fillContainerData(custom_interfaces::msg::Items::SharedPtr items_msg, float leaf_size, cv::Mat &out_occ_grid);

    GET_OCCUPANCY_GRID_LOCAL
    int _validateInput();

    GET_OCCUPANCY_GRID_LOCAL
    int _cropPointCloudToTable(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);

    GET_OCCUPANCY_GRID_LOCAL
    int _getOccupiedVoxelCenters(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud, const float &leaf_size, std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ>> &out_cloud_voxel_centers);

    GET_OCCUPANCY_GRID_LOCAL
    void _initializeSubscribers(const rclcpp::QoS &qos_settings);

    GET_OCCUPANCY_GRID_LOCAL
    void _checkLastProcessedMessagesTimestamps();

    rclcpp_action::Server<GetOccupancyGridAction>::SharedPtr _action_server;
    rclcpp::Publisher<GetOccupancyGridMsg>::SharedPtr _publisher_occupancy_grid;
    pcl::PointCloud<pcl::PointXYZ>::Ptr _merged_point_cloud;

    rclcpp::Subscription<custom_interfaces::msg::MergedPtcldFiltered>::SharedPtr _merged_ptcld_filtered_sub;
    rclcpp::Subscription<custom_interfaces::msg::Items>::SharedPtr _merged_items_sub;
    custom_interfaces::msg::MergedPtcldFiltered::SharedPtr _merged_ptcld_filtered_msg;
    custom_interfaces::msg::Items::SharedPtr _items_msg;

    builtin_interfaces::msg::Time _last_merged_items_msg_timestamp;
    builtin_interfaces::msg::Time _last_merged_ptcld_filtered_msg_timestamp;

    // InputMsg::SharedPtr _input_msg_data;
    float _grid_size;
    WorkspaceArea _workspace_area;
    bool _show = true;
  };

} // namespace get_occupancy_grid

#endif // GET_OCCUPANCY_GRID__OCCUPANCY_GRID_HPP_
