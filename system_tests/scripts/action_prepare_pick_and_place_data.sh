#!/bin/bash

source bash_utils.sh

SELECT_SORT_POLICY=${1:-nearest}
LABEL=${2:-orange}
SELECT_AREA_POLICY=${3:-inside}
SELECT_AREA=${4:-table_area}
PLACE_AREA_POLICY=${5:-inside}
PLACE_AREA=${6:-operating_area}
# PLACE_POSITION_ORDER=${7:-top_left} # top_left, left_bottom, ....

run ros2 run cli get_occupancy_grid
run ros2 run cli manager_prepare_pick_and_place_data \
			select_sort_policy $SELECT_SORT_POLICY \
			label $LABEL \
			select_area_policy $SELECT_AREA_POLICY \
			select_area $SELECT_AREA \
			place_area_policy $PLACE_AREA_POLICY \
			place_area $PLACE_AREA \
			search_shift 0.1 \
			nr_poses 2 \
			constrain_value 40 \
			ik_trials_number 150 \
			max_final_states 4 \
			ompl_compare_trials 1 \
			min_path_points 200 \
			max_time 15 \
			max_simplification_time 15
