#!/bin/bash

source bash_utils.sh

LABEL=${1:-orange}
AREA=${2:-table_area}
ORIENTATION=${3:-random}
AMOUNT=${4:-1}

run ros2 run virtualscene create label $LABEL area $AREA orientation $ORIENTATION amount $AMOUNT

exit 0
