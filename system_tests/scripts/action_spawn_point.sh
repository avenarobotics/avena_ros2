#!/bin/bash

source bash_utils.sh

LABEL=${1:-orange}
POINT=${2:-p_0_0}
ORIENTATION=${3:-random}
AMOUNT=${4:-1}

run ros2 run virtualscene create label $LABEL point $POINT orientation $ORIENTATION amount $AMOUNT

exit 0
