#!/bin/bash

set -e 

SELECT_SORT_POLICY=${1:-nearest}
LABEL=${2:-orange}
SELECT_AREA_POLICY=${3:-inside}
SELECT_AREA=${4:-whole}
PLACE_AREA_POLICY=${5:-inside}
PLACE_AREA=${6:-operating_area}

action_generate_objects.sh
action_home_position.sh
action_generate_objects.sh
action_prepare_pick_and_place_data.sh $SELECT_SORT_POLICY $LABEL $SELECT_AREA_POLICY $SELECT_AREA $PLACE_AREA_POLICY $PLACE_AREA
action_pick_and_place.sh

sleep 1

exit 0
