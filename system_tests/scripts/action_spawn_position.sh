#!/bin/bash

source bash_utils.sh

LABEL=${1:-orange}
POSITION=${2:-random}
ORIENTATION=${3:-random}
AMOUNT=${4:-1}

run ros2 run virtualscene create label $LABEL position $POSITION orientation $ORIENTATION amount $AMOUNT

exit 0
