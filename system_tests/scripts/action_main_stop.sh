#!/bin/bash

source ~/.bashrc

containers=`ros2 component list --containers-only`

for container in  $containers ; do
    components=`ros2 component list $container`
    for id in $components ; do
        [[ $id == ?(-)+([0-9]) ]] && ros2 component unload $container $id
        # echo $id
    done
done

exit 0

