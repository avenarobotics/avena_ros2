#!/bin/bash

source bash_utils.sh

POSITION=${1:-5} # mm
ORIENTATION=${2:-5} # %
SIZE=${3:-10} # %

run ros2 run cli validate_estimate_shape position $POSITION orientation $ORIENTATION size $SIZE

exit 0
