#!/bin/bash

set -e 

action_main_stop.sh
action_coppelia_brain_stop.sh
action_coppelia_camera_stop.sh

exit 0
