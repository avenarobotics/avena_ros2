#!/bin/bash

set -e 

action_main_start.sh & 
action_coppelia_brain_start.sh & 
action_coppelia_camera_start.sh &

exit 0
