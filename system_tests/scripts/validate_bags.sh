# !/bin/bash

set -e
PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/scripts
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh
source bash_utils.sh
RED='\033[0;31m'
BLUE='\033[0;34m'



path_to_bags=${1:-}

POSITION=${2:-10}    # mm
ORIENTATION=${3:-10} # %
SIZE=${4:-10}        # %
# get list of all files within selected directory
path=$path_to_bags\*.tgz

echo -e "\e[32m Extracting bag files from directory. \e[0m"
for f in $path; do
    tar -xf $f -C $path_to_bags
done
echo -e "\e[32m All bags extracted. \e[0m"

cd $path_to_bags
echo -e "\e[32m Launching bags to perform validations \e[0m"
for d in $(ls -d */); do
    # echo $d
    abs_path=$(
        cd $d
        pwd
    )
    ros2 bag play oranges_0.db3 -l &
    bag_play_pid=$! && sleep 1s && kill -KILL $bag_play_pid && action_generate_objects.sh

    ros2 bag play $d*.db3 -l &
    bag_play_pid=$! && sleep 1s && kill -KILL $bag_play_pid && action_generate_objects.sh
    # old 
    # ros2 run cli validate_estimate_shape position $POSITION orientation $ORIENTATION size $SIZE scene_description $abs_path/*.json &
    # echo Result code of validate "$?"

    ros2 run cli validate_estimate_shape position $POSITION orientation $ORIENTATION size $SIZE scene_description $abs_path/*.json & result_code=$?
    echo "--------------------------------------------------------------------------------------------------------------- : "
    if [ $result_code = 0 ];
    then 
        echo -e "${BLUE} Scene have no errors  : " $abs_path
    else
        echo -e "${RED} Scene contains errors : " $abs_path
    fi
done
# Get all resulted cvs files from selected directory and combine them in one file
find $(pwd) -name '*.csv' -exec awk 'FNR==1 && NR!=1{next;}{print}' {} \; > merged.csv

echo -e "${BLUE}<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Finished validation>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "

exit 0 
