#!/bin/bash

cd /home/avena/ros2_ws

source /opt/ros/foxy/setup.bash
source /home/avena/ros2_ws/install/setup.bash

ros2 launch avena_bringup main_with_debug.launch.py
