#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/scripts
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh

label=plate

labels=( "orange" )

script_clear_scene.sh
action_home_position.sh
# spawn and set plate 1 id 
action_spawn_position.sh $label 0.5,0.5,0.7 0,0,0 1
sleep 1s
action_generate_objects.sh
# spawn and set plate 2 id 

action_spawn_position.sh $label 0.3,-0.5,0.7 0,0,0 1
sleep 1s
action_generate_objects.sh
# spawn orange in plate 1 
# action_spawn_position.sh orange random random 10
action_spawn_position.sh orange 0.5,0.5,0.7 0,0,0 1
action_spawn_position.sh orange random random 10
sleep 2s
action_generate_objects.sh
# spawn oranges and at least one in container and 10 oranges spawned 

# demo_pick_and_place_cutting_board.sh 

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
