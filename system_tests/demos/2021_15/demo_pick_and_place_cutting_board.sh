#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/demos/2021_15
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh
REPEATS=1

runx $REPEATS script_pick_and_place_cutting_board_test.sh
echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
