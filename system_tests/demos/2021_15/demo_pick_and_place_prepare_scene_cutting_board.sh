#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/scripts
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh

script_clear_scene.sh
action_home_position.sh
action_spawn_position.sh cuttingboard 0.6,-0.5,0.8 90,-90,0 1
sleep 1s
action_generate_objects.sh
action_spawn_position.sh orange random random 3
sleep 2s
action_generate_objects.sh

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
