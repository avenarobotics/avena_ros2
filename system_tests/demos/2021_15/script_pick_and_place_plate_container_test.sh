#!/bin/bash
PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/scripts
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh
set -e 
# pick from table and place on cutting board
SELECT_SORT_POLICY=${1:-nearest}
LABEL=${2:-orange}
SELECT_AREA_POLICY=${3:-inside}
SELECT_AREA=${4:-plate_1}
PLACE_AREA_POLICY=${5:-inside}
PLACE_AREA=${6:-plate_2}

SELECT_AREA_2=${3:-plate_2}
PLACE_AREA_2=${4:-plate_1}

action_generate_objects.sh
action_home_position.sh
action_generate_objects.sh
run ros2 run cli get_occupancy_grid
run ros2 run cli manager_prepare_pick_and_place_data \
			select_sort_policy $SELECT_SORT_POLICY \
			label $LABEL \
			select_area_policy $SELECT_AREA_POLICY \
			select_area $SELECT_AREA \
			place_area_policy $PLACE_AREA_POLICY \
			place_area $PLACE_AREA \
			search_shift 0.03 \
			nr_poses 2 \
			constrain_value 100 \
			ik_trials_number 150 \
			max_final_states 4 \
			ompl_compare_trials 2 \
			min_path_points 200 \
			max_time 60 \
			max_simplification_time 20
action_pick_and_place.sh
sleep 1
# pick from cutting board and place on table
action_generate_objects.sh
action_home_position.sh
action_generate_objects.sh
run ros2 run cli get_occupancy_grid
run ros2 run cli manager_prepare_pick_and_place_data \
			select_sort_policy $SELECT_SORT_POLICY \
			label $LABEL \
			select_area_policy $SELECT_AREA_POLICY \
			select_area $SELECT_AREA_2 \
			place_area_policy $PLACE_AREA_POLICY \
			place_area $PLACE_AREA_2 \
			search_shift 0.03 \
			nr_poses 2 \
			constrain_value 100 \
			ik_trials_number 150 \
			max_final_states 4 \
			ompl_compare_trials 2 \
			min_path_points 200 \
			max_time 60 \
			max_simplification_time 20
action_pick_and_place.sh
sleep 1
 
exit 0
