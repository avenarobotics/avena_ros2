#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/demos/2021_15
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh

# FILTER_POLICY=${1:-container}
# LABEL=${2:-orange}
# SOURCE_AREA=${3:-whole}
# DESTINATION_AREA=${4:-cutting_board_1}
REPEATS=1

runx $REPEATS script_pick_and_place_plate_container_test.sh
echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
