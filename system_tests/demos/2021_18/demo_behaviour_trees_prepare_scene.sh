#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/scripts
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh

script_clear_scene.sh

action_spawn_position.sh bowl 0.3,-0.7,0.6 0,0,0 1
action_spawn_position.sh orange 0.3,-0.3,0.6 0,0,0 1
action_spawn_position.sh carrot 0.3,0.3,0.6 0,1.57,0.75 1
action_spawn_position.sh milk 0.3,0.6,0.6 0,0,0 1
action_spawn_position.sh lipton 0.6,-0.6,0.6 0,0,1.57 1
action_spawn_position.sh plate 0.7,0,0.6 0,0,0 1
action_spawn_position.sh orange 0.7,0.6,0.6 0,-1.57,0.75 1

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
