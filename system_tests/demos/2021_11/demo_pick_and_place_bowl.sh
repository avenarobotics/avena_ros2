#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/scripts
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh



SELECT_SORT_POLICY=${1:-nearest}
LABEL=${2:-bowl}
SELECT_AREA_POLICY=${3:-inside}
SELECT_AREA=${4:-whole}
PLACE_AREA_POLICY=${5:-inside}
PLACE_AREA=${6:-operating_area}
REPEATS=10

runx $REPEATS script_pick_and_place.sh $SELECT_SORT_POLICY $LABEL $SELECT_AREA_POLICY $SELECT_AREA $PLACE_AREA_POLICY $PLACE_AREA

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
