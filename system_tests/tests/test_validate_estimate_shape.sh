#!/bin/bash

cd "$(dirname "$0")"

source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh

LABELS=( "bowl" "carrot" "lipton" "knife" "milk" "orange" "plate" "spatula" )
RASTER=${1:-0.4}
POSITION_ERROR=${2:-10}
ORIENTATION_ERROR=${3:-10}
SIZE_ERROR=${4:-10}

for i in "${LABELS[@]}"
do

    ./generic_test_validate_estimate_shape.sh $i $RASTER $POSITION_ERROR $ORIENTATION_ERROR $SIZE_ERROR
done

#                                         label      m      mm    %    %
# ./generic_test_validate_estimate_shape.sh bowl       0.3    10    5    5
# ./generic_test_validate_estimate_shape.sh carrot     0.3    10    5    5
# ./generic_test_validate_estimate_shape.sh lipton     0.4    10    5    5
# ./generic_test_validate_estimate_shape.sh knife      0.4    10    5    5
# ./generic_test_validate_estimate_shape.sh milk       0.3    10    5    5
# ./generic_test_validate_estimate_shape.sh orange     0.3    10    5    5
# ./generic_test_validate_estimate_shape.sh plate      0.4    10    5    5
# ./generic_test_validate_estimate_shape.sh spatula    0.4    10    5    5

exit 0
