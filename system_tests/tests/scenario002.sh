#!/bin/bash

set -e 

PATH=$PATH:~/ros2_ws/src/avena_ros2/system_tests/scripts
source ~/ros2_ws/src/avena_ros2/system_tests/scripts/bash_utils.sh

SELECT_SORT_POLICY=nearest
LABEL=orange
SELECT_AREA_POLICY=inside
SELECT_AREA=item_area
PLACE_AREA_POLICY=inside
PLACE_AREA=operating_area
REPEATS=4
AMOUNT=6


action_coppelia_brain_clear.sh
action_coppelia_camera_clear.sh

action_home_position.sh

action_spawn_point.sh $LABEL p_1_2 random 1
action_spawn_point.sh $LABEL p_2_2 random 1
action_spawn_point.sh $LABEL p_3_2 random 1
action_spawn_point.sh $LABEL p_4_2 random 1


sleep 10

runx $REPEATS script_pick_and_place.sh $SELECT_SORT_POLICY $LABEL $SELECT_AREA_POLICY $SELECT_AREA $PLACE_AREA_POLICY $PLACE_AREA

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
