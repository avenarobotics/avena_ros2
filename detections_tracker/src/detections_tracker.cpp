#include <detections_tracker.hpp>

namespace detections_tracker
{
    DetectionsTracker::DetectionsTracker(const rclcpp::NodeOptions &options)
        : Node("detections_tracker", options)
    {
        helpers::commons::setLoggerLevelFromParameter(this);

        auto qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        _detections_sub = create_subscription<Detections>("fast_detections", qos_settings,
                                                          std::bind(&DetectionsTracker::_detectionsCallback, this, std::placeholders::_1));
        _detections_tracking_pub = create_publisher<DetectionsTracking>("detections_tracking", qos_settings);
        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        status=custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void DetectionsTracker::initNode()
    {
        status=custom_interfaces::msg::Heartbeat::STARTING;

        status=custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void DetectionsTracker::shutDownNode()
    {
        status=custom_interfaces::msg::Heartbeat::STOPPING;

        status=custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void DetectionsTracker::_assignPreviousMasks(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections)
    {
        prev_detections = curr_detections;

        for (auto &single_class : prev_detections)
        {
            for (auto mask = single_class.masks.begin(); mask != single_class.masks.end();)
            {
                if (mask->status == DetectionStatus::MISSING)
                    mask = single_class.masks.erase(mask);
                else
                    mask++;
            }
        }
    }

    void DetectionsTracker::_detectionsCallback(const Detections::SharedPtr msg)
    {

        time_check;

        if(status!=custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
            return;
        }

        _curr_timestamp = msg->header.stamp;

        DetectionsTracking::UniquePtr tracked_detections(new DetectionsTracking);
        std::thread cam1_thread(std::bind(&DetectionsTracker::_cameraThread, this, std::cref(msg->cam1_masks), std::cref(msg->cam1_labels), std::ref(_current_index_cam1),
                                          std::ref(_curr_detections_cam1), std::ref(_prev_detections_cam1), std::ref(tracked_detections->cam1_detections)));
        std::thread cam2_thread(std::bind(&DetectionsTracker::_cameraThread, this, std::cref(msg->cam2_masks), std::cref(msg->cam2_labels), std::ref(_current_index_cam2),
                                          std::ref(_curr_detections_cam2), std::ref(_prev_detections_cam2), std::ref(tracked_detections->cam2_detections)));

        cam1_thread.join();
        cam2_thread.join();

        tracked_detections->header = msg->header;

        _assignPreviousMasks(_curr_detections_cam1, _prev_detections_cam1);
        _assignPreviousMasks(_curr_detections_cam2, _prev_detections_cam2);

        _prev_timestamp = _curr_timestamp;

        
        // ////////////////////////////////////////////////////////////
        // // TODO: Remove this after testing
        // auto draw_masks = [this](const std::vector<DetectionData> &detection_data, cv::Mat &out_mask, size_t frame_nr)
        // {
        //     if (detection_data.size() == 0)
        //     {
        //         RCLCPP_WARN(get_logger(), "Empty detection data");
        //         return;
        //     }
        //     auto convert_status_to_str = [this](const int32_t status) -> std::string
        //     {
        //         std::string status_msg = "NONE";
        //         switch (status)
        //         {
        //         case DetectionStatus::UNCHANGED:
        //             status_msg = "UNCHANGED";
        //             break;
        //         case DetectionStatus::NEW:
        //             status_msg = "NEW";
        //             break;
        //         case DetectionStatus::MISSING:
        //             status_msg = "MISSING";
        //             break;
        //         case DetectionStatus::MOVING:
        //             status_msg = "MOVING";
        //             break;
        //         case DetectionStatus::COVERED:
        //             status_msg = "COVERED";
        //             break;
        //         case DetectionStatus::REAPPEAR:
        //             status_msg = "REAPPEAR";
        //             break;
        //         case DetectionStatus::LOST:
        //             status_msg = "LOST";
        //             break;
        //         default:
        //             status_msg = "DEFAULT";
        //             break;
        //         }
        //         return status_msg;
        //     };
        //     // Get size of mask
        //     cv::Mat temp_mask;
        //     helpers::converters::stringToBinaryMask(detection_data[0].mask, temp_mask);
        //     out_mask = cv::Mat::zeros(temp_mask.size(), CV_8UC1);
        //     const auto font_scale = 0.5;
        //     const auto font_face = cv::FONT_HERSHEY_SIMPLEX;
        //     const auto thickness = 1;
        //     cv::putText(out_mask, "frame_" + std::to_string(frame_nr), cv::Point(10, 20), font_face, font_scale, cv::Scalar::all(128), thickness);
        //     for (size_t i = 0; i < detection_data.size(); ++i)
        //     {
        //         if (!(detection_data[i].status.status == DetectionStatus::NEW || detection_data[i].status.status == DetectionStatus::UNCHANGED || detection_data[i].status.status == DetectionStatus::MOVING))
        //         {
        //             RCLCPP_ERROR_STREAM(get_logger(), convert_status_to_str(detection_data[i].status.status));
        //         }
        //         cv::Mat temp_mask;
        //         helpers::converters::stringToBinaryMask(detection_data[i].mask, temp_mask);
        //         cv::Moments m = cv::moments(temp_mask, true);
        //         cv::Point centroid(m.m10 / m.m00, m.m01 / m.m00);

        //         cv::bitwise_or(out_mask, temp_mask, out_mask);
        //         cv::putText(out_mask, detection_data[i].label + "_" + std::to_string(detection_data[i].detection_id), centroid, font_face, font_scale, cv::Scalar::all(128), thickness);
        //         int baseline = 0;
        //         cv::Size text_size = cv::getTextSize(convert_status_to_str(detection_data[i].status.status), font_face, font_scale, thickness, &baseline);
        //         cv::putText(out_mask, convert_status_to_str(detection_data[i].status.status), cv::Point(centroid.x, centroid.y + text_size.height + 2), font_face, font_scale, cv::Scalar::all(128), thickness);

        //         std::stringstream ss;
        //         ss << "x: " << std::fixed << std::setprecision(2) << detection_data[i].velocity.vel_x << ", y: " << std::fixed << std::setprecision(2) << detection_data[i].velocity.vel_y;
        //         std::string velocity = ss.str();
        //         cv::Size vel_text_size = cv::getTextSize(velocity, font_face, font_scale, thickness, &baseline);
        //         cv::putText(out_mask, velocity, cv::Point(centroid.x, centroid.y + vel_text_size.height + text_size.height + 2), font_face, font_scale, cv::Scalar::all(128), thickness);
        //     }
        // };

        // static size_t frame_nr = 0;
        // cv::Mat cam1_masks;
        // draw_masks(tracked_detections->cam1_detections, cam1_masks, frame_nr++);
        // // cv::Mat cam2_masks;
        // // draw_masks(tracked_detections->cam2_detections, cam2_masks);
        // // cv::Mat concatenated_masks;
        // // cv::hconcat(cam1_masks, cam2_masks, concatenated_masks);
        // // cv::imshow("masks_tracking", concatenated_masks);

        // if (!cam1_masks.empty())
        //     cv::imshow("masks_tracking", cam1_masks);

        // // // cv::imshow("cam1_tracker", cam1_masks);
        // // // cv::imshow("cam2_tracker", cam2_masks);
        // cv::waitKey(1);
        // ////////////////////////////////////////////////////////////

        _detections_tracking_pub->publish(std::move(tracked_detections));
    }

    void DetectionsTracker::_cameraThread(const std::vector<std::string> &masks, const std::vector<std::string> &labels, int &current_index,
                                          std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections,
                                          std::vector<DetectionData> &out_tracked_detections)
    {   

        


        auto track_masks = [this](const std::vector<std::string> &masks, const std::vector<std::string> &labels,
                                  std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections)
        {
            _assignInputData(masks, labels, curr_detections);
            _handleMultipleDetections(curr_detections);
            if (_countMasksLeft(prev_detections) == 0)
                return;
            _findUnchangedMasks(curr_detections, prev_detections);
            if (_countMasksLeft(prev_detections) == 0)
                return;
            _checkForMovement(curr_detections, prev_detections);
            if (_countMasksLeft(prev_detections) == 0)
                return;
            _checkForCover(curr_detections, prev_detections);
            if (_countMasksLeft(prev_detections) == 0)
                return;
            _matchFastMovingMasks(curr_detections, prev_detections);
            if (_countMasksLeft(prev_detections) == 0)
                return;
        };

        track_masks(masks, labels, curr_detections, prev_detections);
        _assignRemainingMasks(curr_detections, prev_detections);
        _fillMessage(curr_detections, prev_detections, out_tracked_detections, current_index);
    }

    int DetectionsTracker::_countMasksLeft(std::vector<LabeledMasks> &detections)
    {
        int total_amount = 0;
        for (auto single_class : detections)
            total_amount += single_class.masks.size();
        return total_amount;
    }

    void DetectionsTracker::_matchFastMovingMasks(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections)
    {
        // I know it is not readable but it is fast
        auto offset_image_with_padding = [this](const cv::Mat &original_image, const cv::Point2f &displacement) -> cv::Mat
        {
            std::shared_ptr<cv::Mat> padded = std::make_shared<cv::Mat>();
            *padded = cv::Mat(original_image.rows + 2 * std::ceil(std::abs(displacement.y)), original_image.cols + 2 * std::ceil(std::abs(displacement.x)), CV_8UC1, cv::Scalar::all(0));
            original_image.copyTo((*padded)(cv::Rect(std::ceil(std::abs(displacement.x)), std::ceil(std::abs(displacement.y)), original_image.cols, original_image.rows)));
            cv::Mat predicted = cv::Mat(*padded, cv::Rect(std::ceil(std::abs(displacement.x)) + std::floor(displacement.x), std::ceil(std::abs(displacement.y)) + std::floor(displacement.y), original_image.cols, original_image.rows));
            return predicted;
        };
        auto dt = _rosTimeToSeconds(_curr_timestamp) - _rosTimeToSeconds(_prev_timestamp);

        for (auto &prev_single_class : prev_detections)
        {
            if (prev_single_class.masks.size() == 0)
                continue;

            auto curr_equivalent_it = std::find_if(curr_detections.begin(), curr_detections.end(), [&prev_single_class](const LabeledMasks &curr_class)
                                                   { return (prev_single_class.label == curr_class.label); });

            if (curr_equivalent_it == curr_detections.end())
                continue;

            for (auto prev_mask = prev_single_class.masks.begin(); prev_mask != prev_single_class.masks.end();)
            {

                cv::Point2f displacement = prev_mask->velocity * dt;
                cv::Mat predicted_curr_mask = offset_image_with_padding(prev_mask->mask, displacement);

                bool matched = false;
                for (auto &curr_mask : curr_equivalent_it->masks)
                {   

                    if (curr_mask.status != DetectionStatus::NEW)
                        continue;
                    if (_computeIOU(curr_mask.mask, predicted_curr_mask) < IOU_THRESHOLD_FAST_MOVING)
                        continue;

                    curr_mask.status = DetectionStatus::MOVING;
                    _computeVelocity(curr_mask.mask, prev_mask->mask, curr_mask.velocity);
                    curr_mask.idx = prev_mask->idx;
                    prev_mask = prev_single_class.masks.erase(prev_mask);
                    matched = true;
                }

                if (!matched)
                    prev_mask++;
            }
        }
    }

    void DetectionsTracker::_computeVelocity(const cv::Mat &curr_mask, const cv::Mat &prev_mask, cv::Point2f &velocity)
    {

        if (cv::countNonZero(prev_mask) == 0 || cv::countNonZero(curr_mask) == 0)
        {
            velocity.x = 0.0;
            velocity.y = 0.0;
            return;
        }

        cv::Moments m = cv::moments(prev_mask, true);
        auto origin = cv::Point2f(m.m10 / m.m00, m.m01 / m.m00);
        m = cv::moments(curr_mask, true);
        auto end = cv::Point2f(m.m10 / m.m00, m.m01 / m.m00);

        auto dt = _rosTimeToSeconds(_curr_timestamp) - _rosTimeToSeconds(_prev_timestamp);
        auto diff = end - origin;
        velocity = diff / dt;
    }

    void DetectionsTracker::_checkForMovement(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections)
    {
        for (auto &single_class : curr_detections)
        {
            auto prev_equivalent_it = std::find_if(prev_detections.begin(), prev_detections.end(), [&single_class](const LabeledMasks &prev_class)
                                                   { return (single_class.label == prev_class.label); });

            for (auto &curr_mask : single_class.masks)
            {
                if (curr_mask.status != DetectionStatus::NEW)
                    continue;
                auto matched_mask_it = std::find_if(prev_equivalent_it->masks.begin(), prev_equivalent_it->masks.end(),
                                                    [this, &curr_mask](const Mask &prev_mask)
                                                    {
                                                        cv::Mat masks_intersection;
                                                        cv::bitwise_and(curr_mask.mask, prev_mask.mask, masks_intersection);
                                                        const auto intersection_count = cv::countNonZero(masks_intersection);
                                                        // If there is no intersection we cannot be sure if this mask is the same from current and previous
                                                        if (intersection_count == 0)
                                                            return false;
                                                        cv::Mat masks_union;
                                                        cv::bitwise_or(curr_mask.mask, prev_mask.mask, masks_union);
                                                        const auto union_count = cv::countNonZero(masks_union);
                                                        const auto prev_mask_count = cv::countNonZero(prev_mask.mask);
                                                        return union_count > prev_mask_count;
                                                    });
                if (matched_mask_it != prev_equivalent_it->masks.end())
                {
                    curr_mask.status = DetectionStatus::MOVING;
                    _computeVelocity(curr_mask.mask, matched_mask_it->mask, curr_mask.velocity);
                    curr_mask.idx = matched_mask_it->idx;
                    prev_equivalent_it->masks.erase(matched_mask_it);
                }
            }
        }
    }

    void DetectionsTracker::_checkForCover(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections)
    {
        for (auto &single_class : curr_detections)
        {
            auto prev_equivalent_it = std::find_if(prev_detections.begin(), prev_detections.end(), [&single_class](const LabeledMasks &prev_class)
                                                   { return (single_class.label == prev_class.label); });
            for (auto &curr_mask : single_class.masks)
            {
                if (curr_mask.status != DetectionStatus::NEW)
                    continue;
                auto matched_mask_it = std::find_if(prev_equivalent_it->masks.begin(), prev_equivalent_it->masks.end(),
                                                    [this, &curr_mask](const Mask &prev_mask)
                                                    {
                                                        cv::Mat masks_intersection;
                                                        cv::bitwise_and(curr_mask.mask, prev_mask.mask, masks_intersection);
                                                        const auto intersection_count = cv::countNonZero(masks_intersection);
                                                        // If there is no intersection we cannot be sure if this mask is the same from current and previous
                                                        if (intersection_count == 0)
                                                            return false;

                                                        cv::Mat masks_union;
                                                        cv::bitwise_or(curr_mask.mask, prev_mask.mask, masks_union);
                                                        const auto union_count = cv::countNonZero(masks_union);
                                                        const auto prev_mask_count = cv::countNonZero(prev_mask.mask);
                                                        const auto curr_mask_count = cv::countNonZero(curr_mask.mask);
                                                        return prev_mask_count <= union_count || curr_mask_count <= union_count;
                                                    });
                if (matched_mask_it != prev_equivalent_it->masks.end())
                {
                    curr_mask.status = DetectionStatus::COVERED;
                    curr_mask.idx = matched_mask_it->idx;
                    prev_equivalent_it->masks.erase(matched_mask_it);
                }
            }
        }
    }

    void DetectionsTracker::_findUnchangedMasks(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections)
    {
        for (auto &curr_single_class : curr_detections)
        {
            auto prev_equivalent_it = std::find_if(prev_detections.begin(), prev_detections.end(), [curr_single_class](const LabeledMasks &prev_class)
                                                   { return (curr_single_class.label == prev_class.label); });

            for (auto &mask : curr_single_class.masks)
            {
                auto matched_mask_it = std::find_if(prev_equivalent_it->masks.begin(), prev_equivalent_it->masks.end(),
                                                    [this, &mask](const Mask &prev_mask)
                                                    {
                                                        auto iou = _computeIOU(mask.mask, prev_mask.mask);
                                                        return iou > IOU_THRESHOLD_UNCHANGED && mask.status == DetectionStatus::NEW;
                                                    });

                if (matched_mask_it != prev_equivalent_it->masks.end())
                {
                    mask.status = DetectionStatus::UNCHANGED;
                    mask.idx = matched_mask_it->idx;
                    prev_equivalent_it->masks.erase(matched_mask_it);
                }
            }
        }
    }

    void DetectionsTracker::_prepareDetectionVectors(const std::vector<std::string> &cam_labels, std::vector<LabeledMasks> &out_curr_detections)
    {
        out_curr_detections.clear();
        std::set<std::string> unique_labels(cam_labels.begin(), cam_labels.end());
        out_curr_detections.resize(unique_labels.size());
    }

    int DetectionsTracker::_assignInputData(const std::vector<std::string> &masks, const std::vector<std::string> &labels, std::vector<LabeledMasks> &out_curr_detections)
    {
        if (masks.size() != labels.size())
        {
            RCLCPP_ERROR(this->get_logger(), "Detectron data incorrect - number of mask does not match numer of labels.");
            return 1;
        }

        _prepareDetectionVectors(labels, out_curr_detections);

        for (size_t i = 0; i < masks.size(); i++)
        {
            std::string current_label = labels[i];
            std::vector<LabeledMasks>::iterator labeled_mask_it = std::find_if(out_curr_detections.begin(), out_curr_detections.end(), [current_label](const LabeledMasks &labeled_mask)
                                                                               { return (labeled_mask.label == current_label || labeled_mask.label.empty()); });
            if (labeled_mask_it == out_curr_detections.end())
            {
                RCLCPP_ERROR(this->get_logger(), "Data was not assign properly.");
                return 1;
            }

            labeled_mask_it->label = current_label;
            labeled_mask_it->masks.push_back(Mask(masks[i]));
        }
        return 0;
    }

    float DetectionsTracker::_computeIOU(const cv::Mat &mask1, const cv::Mat &mask2)
    {
        if (mask1.empty() || mask2.empty())
            return -1.0;

        cv::Mat iou_int;
        cv::Mat iou_uni;

        cv::bitwise_or(mask1, mask2, iou_uni);
        cv::bitwise_and(mask1, mask2, iou_int);

        return static_cast<float>(cv::countNonZero(iou_int)) / cv::countNonZero(iou_uni);
    }

    int DetectionsTracker::_handleMultipleDetections(std::vector<LabeledMasks> &curr_detections)
    {
        for (auto &detections : curr_detections)
        {
            for (auto it = detections.masks.begin(); it != detections.masks.end() - 1;)
            {
                auto overlayed_mask_it = std::find_if(it + 1, detections.masks.end(),
                                                      [this, it](Mask &mask)
                                                      { return _computeIOU(it->mask, mask.mask); });

                if (overlayed_mask_it == detections.masks.end())
                {
                    it++;
                    continue;
                }
                it = detections.masks.erase(it);
            }
        }
        return 0;
    }

    void DetectionsTracker::_assignRemainingMasks(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections)
    {
        auto assign_status = [this](std::vector<LabeledMasks> &detections, const int32_t status)
        {
            for (auto &detection : detections)
                for (auto &mask : detection.masks)
                    mask.status = status;
        };

        assign_status(prev_detections, DetectionStatus::MISSING);
    }

    void DetectionsTracker::_fillMessage(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections,
                                         std::vector<DetectionData> &out_tracked_detections, int &current_index)
    {
        auto fill_message_masks = [this]( std::vector<LabeledMasks> &detections, std::vector<DetectionData> &out_tracked_detections, int &current_index)
        {
            for (auto &detection : detections)
            {
                for (auto &mask : detection.masks)
                {
                    DetectionData detection_data;
                    if(mask.status == DetectionStatus::NEW)
                        mask.idx = ++current_index;
                    detection_data.detection_id = mask.idx;
                    detection_data.mask = mask.mask_str;
                    detection_data.label = detection.label;
                    MaskVelocity velocity;
                    velocity.vel_x = mask.velocity.x;
                    velocity.vel_y = mask.velocity.y;
                    detection_data.velocity = velocity;
                    DetectionStatus status;
                    status.status = mask.status;
                    detection_data.status = status;
                    out_tracked_detections.push_back(detection_data);
                }
            }
        };
        fill_message_masks(curr_detections, out_tracked_detections, current_index);
        fill_message_masks(prev_detections, out_tracked_detections, current_index);
    }

    double DetectionsTracker::_rosTimeToSeconds(const builtin_interfaces::msg::Time &ros_time)
    {
        return ros_time.sec + ros_time.nanosec / 1e9;
    }

} // namespace detections_tracker

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(detections_tracker::DetectionsTracker)
