#ifndef DETECTIONS_TRACKER__DETECTIONS_TRACKER_HPP_
#define DETECTIONS_TRACKER__DETECTIONS_TRACKER_HPP_

// ___CPP___
#include <iomanip>

// ___ROS___
#include <rclcpp/rclcpp.hpp>

// ___Avena___
#include <helpers_vision/helpers_vision.hpp>
#include <helpers_commons/helpers_commons.hpp>
#include <custom_interfaces/msg/detections.hpp>
#include <custom_interfaces/msg/detections_tracking.hpp>
#include <custom_interfaces/msg/detection_status.hpp>
#include <custom_interfaces/msg/mask_velocity.hpp>

#define time_check helpers::Timer timer(__func__, this->get_logger())
#define debug std::cout << __func__ << ": " << ++(debug_map[std::string(__func__)].value) << std::endl

namespace detections_tracker
{
  constexpr float IOU_THRESHOLD_UNCHANGED = 0.9;
  constexpr float IOU_THRESHOLD_FAST_MOVING = 0.5;

  using DetectionStatus = custom_interfaces::msg::DetectionStatus;
  using Detections = custom_interfaces::msg::Detections;
  using DetectionsTracking = custom_interfaces::msg::DetectionsTracking;
  using DetectionData = custom_interfaces::msg::DetectionData;
  using MaskVelocity = custom_interfaces::msg::MaskVelocity;

  struct Mask
  {
    explicit Mask(const std::string &mask_str)
        : mask_str(mask_str)
    {
      helpers::converters::stringToBinaryMask(mask_str, mask);
    //   velocity.x = 0.0;
    //   velocity.y = 0.0;
    }

    std::string mask_str = "";
    cv::Mat mask;

    int32_t status = DetectionStatus::NEW;
    cv::Point2f velocity = cv::Point2f(0.0,0.0);
    size_t idx = -1;
  };

  struct LabeledMasks
  {
    std::string label = "";
    std::vector<Mask> masks;
  };

  struct debug_t
  {
    int value = 0;
  };

  class DetectionsTracker : public rclcpp::Node, public helpers::WatchdogInterface
  {

  public:
    explicit DetectionsTracker(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    void initNode() override;
    void shutDownNode() override;

  private:
    void _detectionsCallback(const Detections::SharedPtr msg);
    int _assignInputData(const std::vector<std::string> &masks, const std::vector<std::string> &labels, std::vector<LabeledMasks> &out_curr_detections);
    void _prepareDetectionVectors(const std::vector<std::string> &cam_labels, std::vector<LabeledMasks> &out_curr_detections);
    void _cameraThread(const std::vector<std::string> &masks, const std::vector<std::string> &labels, int &current_index,
                       std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections,
                       std::vector<DetectionData> &out_tracked_detections);
    int _handleMultipleDetections(std::vector<LabeledMasks> &curr_detections);
    void _findUnchangedMasks(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections);
    void _checkForCover(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections);
    void _checkForMovement(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections);
    void _matchFastMovingMasks(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections);
    void _computeVelocity(const cv::Mat &curr_mask, const cv::Mat &prev_mask, cv::Point2f &velocity);
    int _countMasksLeft(std::vector<LabeledMasks> &detections);
    float _computeIOU(const cv::Mat &mask1, const cv::Mat &mask2);
    void _assignRemainingMasks(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections);
    void _fillMessage(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections,
                      std::vector<DetectionData> &out_tracked_detections, int &current_index);
    double _rosTimeToSeconds(const builtin_interfaces::msg::Time &ros_time);
void _assignPreviousMasks(std::vector<LabeledMasks> &curr_detections, std::vector<LabeledMasks> &prev_detections);

    std::vector<LabeledMasks> _prev_detections_cam1;
    std::vector<LabeledMasks> _prev_detections_cam2;

    std::vector<LabeledMasks> _curr_detections_cam1;
    std::vector<LabeledMasks> _curr_detections_cam2;

    builtin_interfaces::msg::Time _prev_timestamp;
    builtin_interfaces::msg::Time _curr_timestamp;

    rclcpp::Subscription<Detections>::SharedPtr _detections_sub;
    rclcpp::Publisher<DetectionsTracking>::SharedPtr _detections_tracking_pub;

    int _current_index_cam1 = 0;
    int _current_index_cam2 = 0;

    helpers::Watchdog::SharedPtr _watchdog;

    std::map<std::string, debug_t> debug_map;
  };
} // namespace detections_tracker

#endif // DETECTIONS_TRACKER__DETECTIONS_TRACKER_HPP_
