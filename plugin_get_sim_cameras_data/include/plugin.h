#ifndef SIM_GET_SIM_CAMERAS_DATA_PLUGIN_HPP
#define SIM_GET_SIM_CAMERAS_DATA_PLUGIN_HPP

// ___COPPELIA PLUGINS___
// #include "config.h"
#include "simPlusPlus/Plugin.h"
#include "stubs.h"

// ___CPP___
#include <memory>
#include <regex>

// ___Avena libraries___
#include <helpers_commons/helpers_commons.hpp>
#include <helpers_vision/helpers_vision.hpp>

//___ROS___
#include <rclcpp/rclcpp.hpp>
#include <tf2_ros/static_transform_broadcaster.h>
#include <sensor_msgs/msg/image.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include "custom_interfaces/msg/cameras_data.hpp"

// ___Plugin___
#include "camera_name.h"
// #include "transforms_manager.hpp"

#define PLUGIN_NAME "GetSimCamerasData" CAMERA_NAME
#define PLUGIN_VERSION 1

namespace get_sim_cameras_data
{

  using namespace helpers;
  using namespace std::literals;

  // struct VisionSensorNames
  // {
  //   inline const static std::string RGB1 = "RGB1";
  //   inline const static std::string RGB2 = "RGB2";
  //   inline const static std::string DEPTH1 = "Depth1";
  //   inline const static std::string DEPTH2 = "Depth2";
  // };

  // struct VisionSensorInfo
  // {
  //   VisionSensorInfo() = default;
  //   explicit VisionSensorInfo(std::string name, std::string frame, std::string topic)
  //       : name(name), frame(frame), topic(topic) {}
  //   std::string name;
  //   std::string frame;
  //   std::string topic;
  // };

  struct CameraIntrinsics
  {
    int width;
    int height;
    float cx;
    float cy;
    float fx;
    float fy;
  };

  class GetSimCamerasData : public sim::Plugin, rclcpp::Node
  {
  public:
    GetSimCamerasData();
    virtual ~GetSimCamerasData();
    void onStart();
    void publishSimulationData(publishSimulationData_in *in, publishSimulationData_out *out);
    void onInstancePass(const sim::InstancePassFlags &flags) override;
    void onSimulationAboutToStart() override;

  private:
    helpers::Watchdog::SharedPtr _watchdog;

    void _publishCamerasInfo();
    int _renderVisionSensorData(const std::string &sensor_name);
    void _publishData(const std::string &sensor_name, const std::string &sensor_type);
    void _publishImageToTopic(const cv::Mat &image, const std::string &sensor_type);
    void _publishPointCloudToTopic(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud);
    geometry_msgs::msg::TransformStamped _getVisionSensorTransform();
    pcl::PointCloud<pcl::PointXYZ>::Ptr _createPointCloud(cv::Mat &depth_image, const CameraIntrinsics &camera_params);
    static std::string _getFormattedCameraPrefix(std::string camera_prefix_raw);
    static size_t _getCameraNumber(std::string camera_prefix);

    rclcpp::Publisher<sensor_msgs::msg::CameraInfo>::SharedPtr _rgb_camera_info_pub;
    rclcpp::Publisher<sensor_msgs::msg::CameraInfo>::SharedPtr _depth_camera_info_pub;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr _rgb_pub;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr _depth_pub;
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr _point_cloud_pub;
    std::unique_ptr<tf2_ros::StaticTransformBroadcaster> _static_broadcaster;

    std::thread _cameras_info_thread;
    std::atomic<bool> _running;
    sensor_msgs::msg::CameraInfo::SharedPtr _depth_camera_info;
    sensor_msgs::msg::CameraInfo::SharedPtr _rgb_camera_info;
    CameraIntrinsics _rgb_intrinsics;
    CameraIntrinsics _depth_intrinsics;

    static std::string _camera_name;
    std::string _camera_frame;
    std::string _rgb_sensor_name;
    std::string _depth_sensor_name;
    size_t _camera_number;

    const std::string _rgb_topic = "/rgb/image_raw";
    const std::string _depth_topic = "/depth_to_rgb/image_raw";
    const std::string _point_cloud_topic = "/points2";

    const std::string _rgb_camera_info_topic = "/rgb/camera_info";
    const std::string _depth_camera_info_topic = "/depth_to_rgb/camera_info";

  };
}

#endif // SIM_GET_SIM_CAMERAS_DATA_PLUGIN_HPP
