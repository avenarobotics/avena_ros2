#ifndef TRANSFORMS_MANAGER_GET_SIM_CAMERAS_DATA_HPP
#define TRANSFORMS_MANAGER_GET_SIM_CAMERAS_DATA_HPP

// ___CPP___
#include <optional>

// ___ROS2___
#include <rclcpp/rclcpp.hpp>
#include <tf2_ros/transform_listener.h>

namespace get_sim_cameras_data
{
  static const rclcpp::Logger LOGGER = rclcpp::get_logger("get_sim_cameras_data");

  class TransformsManager
  {
  public:
    using UniquePtr = std::unique_ptr<TransformsManager>;
    using SharedPtr = std::shared_ptr<TransformsManager>;

    explicit TransformsManager(rclcpp::Clock::SharedPtr clock)
    {
      _tf_buffer = std::make_unique<tf2_ros::Buffer>(clock);
      _tf_listener = std::make_unique<tf2_ros::TransformListener>(*_tf_buffer);
    }
    TransformsManager(const TransformsManager &other) = delete;
    ~TransformsManager() {}

    size_t nrFrames() const
    {
      return _tf_buffer->getAllFrameNames().size();
    }

    std::optional<geometry_msgs::msg::TransformStamped> lookupTransform(const std::string &target_frame, const std::string &source_frame, const tf2::TimePoint &time)
    {
      geometry_msgs::msg::TransformStamped transform;
      try
      {
        transform = _tf_buffer->lookupTransform(target_frame, source_frame, time);
      }
      catch (const tf2::TransformException &e)
      {
        RCLCPP_WARN_STREAM(LOGGER, "Parent frame: \"" << target_frame << "\"; child frame: \"" << source_frame << "\"; error: " << e.what());
        return std::nullopt;
      }
      return transform;
    }

    std::optional<geometry_msgs::msg::TransformStamped> lookupLatestTransform(const std::string &target_frame, const std::string &source_frame)
    {
      return lookupTransform(target_frame, source_frame, tf2::TimePointZero);
    }

  private:
    std::unique_ptr<tf2_ros::Buffer> _tf_buffer;
    std::unique_ptr<tf2_ros::TransformListener> _tf_listener;
  };

}

#endif
