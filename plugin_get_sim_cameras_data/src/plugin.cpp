#include "plugin.h"

namespace get_sim_cameras_data
{
    std::string GetSimCamerasData::_camera_name = GetSimCamerasData::_getFormattedCameraPrefix(CAMERA_NAME);

    GetSimCamerasData::GetSimCamerasData()
        : Node("sim_coppelia_" + _camera_name),
          _running(true)
    {
        _camera_frame = _camera_name + "/rgb_camera_link";
        _camera_number = GetSimCamerasData::_getCameraNumber(_camera_name);
        _rgb_sensor_name = "RGB" + std::to_string(_camera_number);
        _depth_sensor_name = "Depth" + std::to_string(_camera_number);
        // _watchdog = std::make_shared<helpers::Watchdog>(this, "system_monitor");
    }

    void GetSimCamerasData::onStart()
    {
        if (!registerScriptStuff())
            throw std::runtime_error("script stuff initialization failed");

        setExtVersion("Plugin publishing simulation data (RGB, Depth, Point clouds, TF) data to ROS2 topic.");
        setBuildDate(__DATE__);
        helpers::commons::setLoggerLevel(get_logger(), "info");

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        _rgb_pub = create_publisher<sensor_msgs::msg::Image>(_camera_name + _rgb_topic, qos_settings);
        _depth_pub = create_publisher<sensor_msgs::msg::Image>(_camera_name + _depth_topic, qos_settings);
        _point_cloud_pub = create_publisher<sensor_msgs::msg::PointCloud2>(_camera_name + _point_cloud_topic, qos_settings);

        _rgb_camera_info_pub = create_publisher<sensor_msgs::msg::CameraInfo>(_camera_name + _rgb_camera_info_topic, qos_settings);
        _depth_camera_info_pub = create_publisher<sensor_msgs::msg::CameraInfo>(_camera_name + _depth_camera_info_topic, qos_settings);

        _static_broadcaster = std::make_unique<tf2_ros::StaticTransformBroadcaster>(get_node_topics_interface());

        _cameras_info_thread = std::thread([this]() {
            rclcpp::Rate rate(10);
            while (rclcpp::ok() && _running)
            {
                _publishCamerasInfo();
                rate.sleep();
            }
        });
    }

    GetSimCamerasData::~GetSimCamerasData()
    {
        _running = false;
        if (_cameras_info_thread.joinable())
            _cameras_info_thread.join();
    }

    void GetSimCamerasData::onInstancePass(const sim::InstancePassFlags &flags)
    {
        rclcpp::spin_some(get_node_base_interface());
    }

    void GetSimCamerasData::_publishCamerasInfo()
    {
        if (_rgb_camera_info && _rgb_camera_info_pub->get_subscription_count() > 0)
        {
            _rgb_camera_info->header.stamp = now();
            _rgb_camera_info_pub->publish(*_rgb_camera_info);
        }

        if (_depth_camera_info && _depth_camera_info_pub->get_subscription_count() > 0)
        {
            _depth_camera_info->header.stamp = now();
            _depth_camera_info_pub->publish(*_depth_camera_info);
        }
    }

    void GetSimCamerasData::onSimulationAboutToStart()
    {
        // Get static transform for cameras
        auto sensor_vision_transform = _getVisionSensorTransform();
        _static_broadcaster->sendTransform({sensor_vision_transform});

        // Get camera intrinsics
        auto getCameraIntrinsics = [this](const std::string &sensor_name) -> sensor_msgs::msg::CameraInfo::SharedPtr {
            sensor_msgs::msg::CameraInfo::SharedPtr camera_info(new sensor_msgs::msg::CameraInfo);
            camera_info->header.frame_id = _camera_frame;
            simInt handle = simGetObjectHandle(sensor_name.c_str());
            simInt resolution[2];
            simGetVisionSensorResolution(handle, resolution);
            camera_info->width = resolution[0];
            camera_info->height = resolution[1];
            float cx = camera_info->width / 2;
            float cy = camera_info->height / 2;

            simFloat fov_rad;
            simGetObjectFloatParameter(handle, sim_visionfloatparam_perspective_angle, &fov_rad);
            float fx = cx / std::tan(fov_rad / 2.0);
            float fy = fx;

            camera_info->k = {fx, 0, cx,
                              0, fy, cy,
                              0, 0, 1};
            return camera_info;
        };

        _rgb_camera_info = getCameraIntrinsics(_rgb_sensor_name);
        _depth_camera_info = getCameraIntrinsics(_depth_sensor_name);

        // Assign parameters to structures for readability
        _rgb_intrinsics.width = _rgb_camera_info->width;
        _rgb_intrinsics.height = _rgb_camera_info->height;
        _rgb_intrinsics.cx = _rgb_camera_info->k[2];
        _rgb_intrinsics.cy = _rgb_camera_info->k[5];
        _rgb_intrinsics.fx = _rgb_camera_info->k[0];
        _rgb_intrinsics.fy = _rgb_camera_info->k[4];

        _depth_intrinsics.width = _depth_camera_info->width;
        _depth_intrinsics.height = _depth_camera_info->height;
        _depth_intrinsics.cx = _depth_camera_info->k[2];
        _depth_intrinsics.cy = _depth_camera_info->k[5];
        _depth_intrinsics.fx = _depth_camera_info->k[0];
        _depth_intrinsics.fy = _depth_camera_info->k[4];
    }

    geometry_msgs::msg::TransformStamped GetSimCamerasData::_getVisionSensorTransform()
    {
        geometry_msgs::msg::TransformStamped transform;
        transform.header.frame_id = "world";
        transform.header.stamp = now();
        transform.child_frame_id = _camera_frame;

        std::string camera_name = _rgb_sensor_name;
        simInt handle = simGetObjectHandle(camera_name.c_str());
        simInt world_handle = simGetObjectHandle("DummyWorld");

        simFloat position[3];
        simGetObjectPosition(handle, world_handle, position);
        transform.transform.translation.x = position[0];
        transform.transform.translation.y = position[1];
        transform.transform.translation.z = position[2];

        simFloat quaternion[4];
        simGetObjectQuaternion(handle, world_handle, quaternion);
        transform.transform.rotation.x = quaternion[0];
        transform.transform.rotation.y = quaternion[1];
        transform.transform.rotation.z = quaternion[2];
        transform.transform.rotation.w = quaternion[3];

        return transform;
    }

    int GetSimCamerasData::_renderVisionSensorData(const std::string &sensor_name)
    {
        // Timer timer("Render " + sensor_name, get_logger());
        size_t camera_handle = simGetObjectHandle(sensor_name.c_str());
        simHandleVisionSensor(camera_handle, nullptr, nullptr);
        return camera_handle;
    }

    void GetSimCamerasData::publishSimulationData(publishSimulationData_in *in, publishSimulationData_out *out)
    {
        const float dt = 0.2; // seconds
        auto previous_time = std::chrono::steady_clock::now();
        while (simGetSimulationState() != sim_simulation_advancing_abouttostop)
        {
            auto current_time = std::chrono::steady_clock::now();
            if (std::chrono::duration<float>(current_time - previous_time).count() >= dt)
            {
                previous_time = current_time;
                _publishData(_rgb_sensor_name, "rgb");
                _publishData(_depth_sensor_name, "depth");
            }
            simSwitchThread();
        }
    }

    void GetSimCamerasData::_publishData(const std::string &sensor_name, const std::string &sensor_type)
    {
        Timer timer("Publish image " + sensor_name, get_logger());

        // if ((sensor_type == "rgb" && _rgb_pub->get_subscription_count() == 0) ||
        //     (sensor_type == "depth" && _depth_pub->get_subscription_count() == 0 && _point_cloud_pub->get_subscription_count() == 0))
        //     return;

        auto camera_handle = _renderVisionSensorData(sensor_name);
        // simSwitchThread();

        simInt resolution[2];
        simGetVisionSensorResolution(camera_handle, &resolution[0]);
        cv::Mat image;
        void *data_cam = nullptr;
        if (sensor_type == "rgb")
        {
            data_cam = simGetVisionSensorCharImage(camera_handle, &resolution[0], &resolution[1]);
            if (!data_cam)
            {
                std::string error_msg = "Cannot get data from sensor " + sensor_name;
                RCLCPP_ERROR_STREAM(get_logger(), error_msg);
                throw std::runtime_error(error_msg);
            }
            // simSetThreadIsFree(true);
            image = cv::Mat(resolution[1], resolution[0], CV_8UC3, data_cam);
            cv::cvtColor(image, image, cv::COLOR_RGB2BGR);
            cv::flip(image, image, 1);
        }
        else if (sensor_type == "depth")
        {
            data_cam = simGetVisionSensorDepthBuffer(camera_handle + sim_handleflag_depthbuffermeters);
            if (!data_cam)
            {
                std::string error_msg = "Cannot get data from sensor " + sensor_name;
                RCLCPP_ERROR_STREAM(get_logger(), error_msg);
                throw std::runtime_error(error_msg);
            }
            // simSetThreadIsFree(true);
            image = cv::Mat(resolution[1], resolution[0], CV_32FC1, data_cam);
            cv::flip(image, image, 1);

            // if (_point_cloud_pub->get_subscription_count() > 0)
            // {
            // Timer timer("Point cloud reconstruction", get_logger());
            auto point_cloud = _createPointCloud(image, _depth_intrinsics);
            _publishPointCloudToTopic(point_cloud);
            // }
        }
        else
        {
            std::string error_msg = "Invalid \"sensor_type\". Allowed types: \"rgb\", \"depth\"";
            RCLCPP_ERROR_STREAM(get_logger(), error_msg);
            throw std::runtime_error(error_msg);
        }
        _publishImageToTopic(image, sensor_type);
        // simSetThreadIsFree(false);
        simReleaseBuffer(reinterpret_cast<simChar *>(data_cam));
    }

    void GetSimCamerasData::_publishImageToTopic(const cv::Mat &image, const std::string &sensor_type)
    {
        sensor_msgs::msg::Image ros_image;
        helpers::converters::cvMatToRos(image, ros_image);
        ros_image.header.frame_id = _camera_frame;
        ros_image.header.stamp = now();
        if (sensor_type == "rgb")
            _rgb_pub->publish(ros_image);
        else
            _depth_pub->publish(ros_image);
    }

    void GetSimCamerasData::_publishPointCloudToTopic(const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud)
    {
        sensor_msgs::msg::PointCloud2 ros_point_cloud;
        helpers::converters::pclToRosPtcld<pcl::PointXYZ>(cloud, ros_point_cloud);
        ros_point_cloud.header.frame_id = _camera_frame;
        ros_point_cloud.header.stamp = now();
        _point_cloud_pub->publish(ros_point_cloud);
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr GetSimCamerasData::_createPointCloud(cv::Mat &depth_image, const CameraIntrinsics &camera_params)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out(new pcl::PointCloud<pcl::PointXYZ>);
        cloud_out->clear();
        cloud_out->reserve(depth_image.total());

        float cx = camera_params.cx;
        float cy = camera_params.cy;
        float fx = camera_params.fx;
        float fy = camera_params.fy;

        pcl::PointXYZ point;
        for (int i = 0; i < depth_image.cols; i++)
        {
            for (int j = 0; j < depth_image.rows; j++)
            {
                // Get Z value of 3D point
                float z = depth_image.at<float>(j, i);
                if (z >= 0.1 && z < 5.0) //if not NAN
                {
                    // Compute XY of 3D point from 2D depth masks using camera intriniscs
                    point.z = z;
                    point.x = point.z * (i - cx) / fx;
                    point.y = point.z * (j - cy) / fy;
                    cloud_out->points.push_back(point);
                }
            }
        }
        cloud_out->points.shrink_to_fit();
        cloud_out->width = cloud_out->points.size();
        cloud_out->height = 1;
        return cloud_out;
    }

    std::string GetSimCamerasData::_getFormattedCameraPrefix(std::string camera_prefix_raw)
    {
        std::string camera_prefix_formatter = camera_prefix_raw;

        std::regex re("[0-9]+");
        std::smatch match;
        if (std::regex_search(camera_prefix_raw, match, re))
        {
            std::string camera_number = match[0];
            auto pos = camera_prefix_raw.find(camera_number);
            if (pos == std::string::npos)
                RCLCPP_ERROR_STREAM(rclcpp::get_logger(camera_prefix_raw), "Invalid camera number. Using user provided value of \"" << camera_prefix_raw << "\"");
            else
            {
                std::string camera_name = camera_prefix_raw.substr(0, pos);
                std::transform(camera_name.begin(), camera_name.end(), camera_name.begin(), [](unsigned char c) -> unsigned char { return std::tolower(c); });
                camera_prefix_formatter = camera_name + "_" + camera_number;
            }
        }
        else
        {
            std::string error_msg = "Camera name provided in CMake has bad format.";
            RCLCPP_ERROR_STREAM(rclcpp::get_logger(camera_prefix_raw), error_msg);
            throw std::runtime_error(error_msg);
        }
        return camera_prefix_formatter;
    }

    size_t GetSimCamerasData::_getCameraNumber(std::string camera_prefix)
    {
        size_t camera_number = 1;

        std::regex re("[0-9]+");
        std::smatch match;
        if (std::regex_search(camera_prefix, match, re))
        {
            std::string camera_number_str = match[0];
            camera_number = std::stoi(camera_number_str);
        }
        else
        {
            std::string error_msg = "Camera name provided in CMake has bad format.";
            RCLCPP_ERROR_STREAM(rclcpp::get_logger(camera_prefix), error_msg);
            throw std::runtime_error(error_msg);
        }
        return camera_number;
    }

} // namespace get_sim_cameras_data

SIM_PLUGIN(PLUGIN_NAME, PLUGIN_VERSION, get_sim_cameras_data::GetSimCamerasData)
#include "stubsPlusPlus.cpp"
