#include "logic_bt/nodes/tracker/security_rgb/action/reset_timer.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            ResetTimer::ResetTimer(const std::string &name, const BT::NodeConfiguration &config)
                : BT::AsyncActionNode(name, config)
            {
            }
            BT::PortsList ResetTimer::providedPorts()
            {
                return {};
            }

            BT::NodeStatus ResetTimer::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");
                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ ResetTimer: STARTED ]. ");
                auto reset_timer_msg = String();
                reset_timer_msg.data = "reset";
                _reset_timer_pub->publish(reset_timer_msg);
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "ResetTimer is done");
                std::this_thread::sleep_for(std::chrono::microseconds(1000));

                return BT::NodeStatus::SUCCESS;
            }
            bool ResetTimer::init(ROSNode::SharedPtr node_ptr)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from ResetTimer");
                    _reset_timer_pub = node_ptr->create_publisher<String>("timer_command", 1);
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes