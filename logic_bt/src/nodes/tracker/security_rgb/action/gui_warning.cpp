#include "logic_bt/nodes/tracker/security_rgb/action/gui_warning.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            GuiWarning::GuiWarning(const std::string &name, const BT::NodeConfiguration &config)
                : BT::AsyncActionNode(name, config)
            {
            }
            BT::PortsList GuiWarning::providedPorts()
            {
                return {};
            }

            BT::NodeStatus GuiWarning::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");
                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ GuiWarning: STARTED ]. ");
                auto security_pause_msg = Bool();
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "Gui warning Trigger ON, GUI shows warning for 5 seconds ");
                security_pause_msg.data = true;
                _gui_warning_pub->publish(security_pause_msg);
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "Gui warning Trigger Off to wait for another Trigger ON after 5 seconds");
                security_pause_msg.data = false;
                _gui_warning_pub->publish(security_pause_msg);
                std::this_thread::sleep_for(std::chrono::microseconds(1000));
                return BT::NodeStatus::SUCCESS;
            }
            bool GuiWarning::init(ROSNode::SharedPtr node_ptr)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from GuiWarning");
                    _gui_warning_pub = node_ptr->create_publisher<Bool>("/gui_warning", 1);
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes