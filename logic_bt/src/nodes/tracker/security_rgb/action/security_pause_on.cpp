#include "logic_bt/nodes/tracker/security_rgb/action/security_pause_on.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            SecurityPauseOn::SecurityPauseOn(const std::string &name, const BT::NodeConfiguration &config)
                : BT::AsyncActionNode(name, config)
            {
            }
            BT::PortsList SecurityPauseOn::providedPorts()
            {
                return {};
            }

            BT::NodeStatus SecurityPauseOn::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");
                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ SecurityPauseON: STARTED ]. ");
                auto security_pause_msg = Bool();
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "Pause ON ");
                security_pause_msg.data = true;
                _security_pause_pub->publish(security_pause_msg);
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "SecurityPauseON is done");
                std::this_thread::sleep_for(std::chrono::microseconds(1000));

                return BT::NodeStatus::SUCCESS;
            }
            bool SecurityPauseOn::init(ROSNode::SharedPtr node_ptr)
            {
                if (node_ptr)
                {

                    RCLCPP_INFO(node_ptr->get_logger(), " init from SecurityPauseOn");
                    _security_pause_pub = node_ptr->create_publisher<Bool>("/security_pause", 1);
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes