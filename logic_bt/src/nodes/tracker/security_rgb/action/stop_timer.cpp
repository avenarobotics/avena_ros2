#include "logic_bt/nodes/tracker/security_rgb/action/stop_timer.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            StopTimer::StopTimer(const std::string &name, const BT::NodeConfiguration &config)
                : BT::AsyncActionNode(name, config)
            {
            }
            BT::PortsList StopTimer::providedPorts()
            {
                return {};
            }

            BT::NodeStatus StopTimer::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");
                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ StopTimer: STARTED ]. ");
                auto stop_timer_msg = String();
                stop_timer_msg.data = "stop";
                _stop_timer_pub->publish(stop_timer_msg);
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "StopTimer is done");
                std::this_thread::sleep_for(std::chrono::microseconds(1000));
                return BT::NodeStatus::SUCCESS;
            }
            bool StopTimer::init(ROSNode::SharedPtr node_ptr)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from StopTimer");
                    _stop_timer_pub = node_ptr->create_publisher<String>("timer_command", 1);
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes