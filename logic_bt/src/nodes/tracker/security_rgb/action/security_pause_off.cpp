#include "logic_bt/nodes/tracker/security_rgb/action/security_pause_off.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            SecurityPauseOff::SecurityPauseOff(const std::string &name, const BT::NodeConfiguration &config)
                : BT::AsyncActionNode(name, config)
            {
            }
            BT::PortsList SecurityPauseOff::providedPorts()
            {
                return {};
            }

            BT::NodeStatus SecurityPauseOff::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");
                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ SecurityPauseOff: STARTED ]. ");
                auto security_pause_msg = Bool();
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "Pause OFF ");
                security_pause_msg.data = false;
                _security_pause_pub->publish(security_pause_msg);
                std::this_thread::sleep_for(std::chrono::microseconds(1000));

                return BT::NodeStatus::SUCCESS;
            }
            bool SecurityPauseOff::init(ROSNode::SharedPtr node_ptr)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from SecurityPauseOff");
                    _security_pause_pub = node_ptr->create_publisher<Bool>("/security_pause", 1);
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes