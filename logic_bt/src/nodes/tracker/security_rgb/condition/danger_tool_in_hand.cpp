#include "logic_bt/nodes/tracker/security_rgb/condition/danger_tool_in_hand.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            DangerToolInHand::DangerToolInHand(const std::string &name, const BT::NodeConfiguration &config)
                : BT::ConditionNode(name, config)
            {
            }

            BT::NodeStatus DangerToolInHand::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");

                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ DangerToolInHand: STARTED ]");
                if (_danger_tool_in_hand == std::nullopt)
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "danger tool in hand is Not Accessible ");
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "So, We consider anger tool in hand is OFF ");
                    return BT::NodeStatus::FAILURE;
                }
                else
                {
                    if (_danger_tool_in_hand.value() == true)
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "danger tool in hand is ON ");
                        return BT::NodeStatus::SUCCESS;
                    }
                    else
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "danger tool in hand is OFF ");
                        return BT::NodeStatus::FAILURE;
                    }
                }
            }
            bool DangerToolInHand::init(ROSNode::SharedPtr node_ptr)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from DangerToolInHand");
                    _danger_tool_in_hand_sub = node_ptr->create_subscription<Bool>(
                        "/danger_tool_in_hand", 1, std::bind(&DangerToolInHand::_danger_tool_in_hand_callback, this, std::placeholders::_1));
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }
            void DangerToolInHand::_danger_tool_in_hand_callback(const Bool::SharedPtr msg)
            {
                if (msg)
                {
                    // RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "danger tool in hand is : " << (msg->data
                    //                                                                                  ? "ON"
                    //                                                                                  : "OFF"));
                    _danger_tool_in_hand = msg->data;
                }
            }
            BT::PortsList DangerToolInHand::providedPorts()
            {
                return {};
            }

        } // namespace security_rgbi
    }     // namespace tracker
} // namespace logic_bt_nodes