#include "logic_bt/nodes/tracker/security_rgb/condition/read_timer_between.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            ReadTimerBetween::ReadTimerBetween(const std::string &name, const BT::NodeConfiguration &config)
                : BT::ConditionNode(name, config)
            {
            }

            BT::NodeStatus ReadTimerBetween::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");

                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ ReadTimerBetween: STARTED ]");
                if (_timer_reading == std::nullopt || _time_out == std::nullopt)
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "timer reading is Not Accessible ");
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "So, We consider there is no timeout ");
                    return BT::NodeStatus::FAILURE;
                }
                else
                {
                    if (_timer_reading.value() < _time_out.value() && _timer_reading.value() > 0.0)
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "Timeout Between");
                        return BT::NodeStatus::SUCCESS;
                    }
                    else
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "No Timeout Between");
                        return BT::NodeStatus::FAILURE;
                    }
                }
            }
            bool ReadTimerBetween::init(ROSNode::SharedPtr node_ptr, const double time_out)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from ReadTimerBetween");
                    _timer_reading_sub = node_ptr->create_subscription<Float64>(
                        "timer_reading", 1, std::bind(&ReadTimerBetween::_timer_reading_callback, this, std::placeholders::_1));
                    if (time_out > 0 && time_out < 10e4)
                    {
                        _time_out = time_out;
                    }
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }
            void ReadTimerBetween::_timer_reading_callback(const Float64::SharedPtr msg)
            {
                if (msg)
                {
                    // RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "danger tool in hand is : " << (msg->data
                    //                                                                                  ? "ON"
                    //                                                                                  : "OFF"));
                    _timer_reading = msg->data;
                }
            }
            BT::PortsList ReadTimerBetween::providedPorts()
            {
                return {};
            }

        } // namespace security_rgbi
    }     // namespace tracker
} // namespace logic_bt_nodes