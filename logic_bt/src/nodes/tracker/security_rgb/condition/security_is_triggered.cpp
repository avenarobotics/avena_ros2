#include "logic_bt/nodes/tracker/security_rgb/condition/security_is_triggered.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            SecurityIsTriggered::SecurityIsTriggered(const std::string &name, const BT::NodeConfiguration &config)
                : BT::ConditionNode(name, config)
            {
            }

            BT::NodeStatus SecurityIsTriggered::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");

                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ SecurityIsTriggered: STARTED ]");
                if (_security_trigger == std::nullopt)
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "security trigger is Not Accessible ");
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "So, We consider security trigger OFF ");
                    return BT::NodeStatus::FAILURE;
                }
                else
                {
                    if (_security_trigger.value() == true)
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "security trigger is ON ");
                        return BT::NodeStatus::SUCCESS;
                    }
                    else
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "security trigger is OFF ");
                        return BT::NodeStatus::FAILURE;
                    }
                }
            }
            bool SecurityIsTriggered::init(ROSNode::SharedPtr node_ptr)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from SecurityIsTriggered");
                    _security_trigger_sub = node_ptr->create_subscription<Bool>(
                        "/security_trigger", 1, std::bind(&SecurityIsTriggered::_security_trigger_callback, this, std::placeholders::_1));
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }
            void SecurityIsTriggered::_security_trigger_callback(const Bool::SharedPtr msg)
            {
                if (msg)
                {
                    // RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "security trigger is : " << (msg->data
                    //                                                                                  ? "ON"
                    //                                                                                  : "OFF"));
                    _security_trigger = msg->data;
                }
            }
            BT::PortsList SecurityIsTriggered::providedPorts()
            {
                return {};
            }

        } // namespace security_rgbi
    }     // namespace tracker
} // namespace logic_bt_nodes