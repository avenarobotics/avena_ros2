#include "logic_bt/nodes/tracker/security_rgb/condition/read_timer.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            ReadTimer::ReadTimer(const std::string &name, const BT::NodeConfiguration &config)
                : BT::ConditionNode(name, config)
            {
            }

            BT::NodeStatus ReadTimer::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");

                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ ReadTimer: STARTED ]");
                if (_timer_reading == std::nullopt || _time_out == std::nullopt)
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "timer reading is Not Accessible ");
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "So, We consider there is no timeout ");
                    return BT::NodeStatus::FAILURE;
                }
                else
                {
                    if (_timer_reading.value() >= _time_out.value())
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "Timeout");
                        return BT::NodeStatus::SUCCESS;
                    }
                    else
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "No Timeout");
                        return BT::NodeStatus::FAILURE;
                    }
                }
            }
            bool ReadTimer::init(ROSNode::SharedPtr node_ptr, const double time_out)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from ReadTimer");
                    _timer_reading_sub = node_ptr->create_subscription<Float64>(
                        "timer_reading", 1, std::bind(&ReadTimer::_timer_reading_callback, this, std::placeholders::_1));
                    if (time_out > 0 && time_out < 10e4)
                    {
                        _time_out = time_out;
                    }
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }
            void ReadTimer::_timer_reading_callback(const Float64::SharedPtr msg)
            {
                if (msg)
                {
                    // RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "danger tool in hand is : " << (msg->data
                    //                                                                                  ? "ON"
                    //                                                                                  : "OFF"));
                    _timer_reading = msg->data;
                }
            }
            BT::PortsList ReadTimer::providedPorts()
            {
                return {};
            }

        } // namespace security_rgbi
    }     // namespace tracker
} // namespace logic_bt_nodes