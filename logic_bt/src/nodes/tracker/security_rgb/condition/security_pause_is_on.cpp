#include "logic_bt/nodes/tracker/security_rgb/condition/security_pause_is_on.hpp"
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            SecurityPauseIsOn::SecurityPauseIsOn(const std::string &name, const BT::NodeConfiguration &config)
                : BT::ConditionNode(name, config)
            {
            }

            BT::NodeStatus SecurityPauseIsOn::tick()
            {
                // RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");

                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ SecurityPauseIsOn: STARTED ]");
                if (_security_pause == std::nullopt)
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "security pause is Not Accessible ");
                    RCLCPP_INFO(rclcpp::get_logger("debug"), "So, We consider security pause OFF ");
                    return BT::NodeStatus::FAILURE;
                }
                else
                {
                    if (_security_pause.value() == true)
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "security pause is ON ");
                        return BT::NodeStatus::SUCCESS;
                    }
                    else
                    {
                        RCLCPP_INFO(rclcpp::get_logger("debug"), "security pause is OFF ");
                        return BT::NodeStatus::FAILURE;
                    }
                }
            }
            bool SecurityPauseIsOn::init(ROSNode::SharedPtr node_ptr)
            {
                if (node_ptr)
                {
                    RCLCPP_INFO(node_ptr->get_logger(), " init from SecurityPauseIsOn");
                    _security_pause_sub = node_ptr->create_subscription<Bool>(
                        "/security_pause", 1, std::bind(&SecurityPauseIsOn::_security_pause_callback, this, std::placeholders::_1));
                    return true;
                }
                else
                {
                    RCLCPP_INFO(rclcpp::get_logger("debug"), " node pointer not passed properly");
                    return false;
                }
            }
            void SecurityPauseIsOn::_security_pause_callback(const Bool::SharedPtr msg)
            {
                if (msg)
                {
                    // RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "security trigger is : " << (msg->data
                    //                                                                                  ? "ON"
                    //                                                                                  : "OFF"));
                    _security_pause = msg->data;
                }
            }
            BT::PortsList SecurityPauseIsOn::providedPorts()
            {
                return {};
            }

        } // namespace security_rgbi
    }     // namespace tracker
} // namespace logic_bt_nodes