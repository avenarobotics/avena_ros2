#include "logic_bt/nodes/PopUpMessage.hpp"
namespace logic_bt_nodes
{
    //-------------------------
    PopUpMessage::PopUpMessage(const std::string &name, const BT::NodeConfiguration &config)
        : BT::AsyncActionNode(name, config)
    {
    }
    BT::PortsList PopUpMessage::providedPorts()
    {
        return {BT::InputPort<std::string>("what_to_pick"), BT::InputPort<std::string>("where_to_put")};
    }

    BT::NodeStatus PopUpMessage::tick()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");
        static int frame_id = 0;
        if (!_halt_requested)
        {
            std::string what_to_pick, where_to_put;
            if (getInput<std::string>("what_to_pick", what_to_pick) && getInput<std::string>("where_to_put", where_to_put))
            {
                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ PopUpMessage: STARTED ]. What to pick: " << what_to_pick << " , where_to_put: " << where_to_put);
                auto user_question = GuiBtMessage();
                user_question.header.stamp.sec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                user_question.header.frame_id = std::to_string(frame_id);
                user_question.msg = what_to_pick + " , " + where_to_put;
                RCLCPP_INFO(rclcpp::get_logger("debug"), "Publishing: '%s'", user_question.msg.c_str());
                _user_question_publisher->publish(user_question);

                // wait for user answer for only first time
                while (!_user_answer && rclcpp::ok())
                    ;
                // RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ PopUpMessage ] No answer at all yet");
                // wait for specific user answer
                while (_user_answer->header.frame_id != user_question.header.frame_id && rclcpp::ok())
                    RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ PopUpMessage ] Did not receive the answer for question yet");
                frame_id++;
                if (_user_answer->msg == "true")
                {
                    return BT::NodeStatus::SUCCESS;
                }
                else
                {
                    return BT::NodeStatus::FAILURE;
                }
            }
            else
            {
                RCLCPP_ERROR(rclcpp::get_logger("debug"), "inputs are not passed");
                return BT::NodeStatus::FAILURE;
            }
        }
        else // halted
        {
            RCLCPP_INFO(rclcpp::get_logger("debug"), "action is halted");
            // client_ptr_->async_cancel_all_goals();
            _cleanup(); // clear all after halt
            return BT::NodeStatus::FAILURE;
        }
    }
    void PopUpMessage::_cleanup()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tree node clean up");
        // _halt_requested = false; // something has to enable running again
        // _action_result.reset();
        // _action_result = std::nullopt;
    }
    void PopUpMessage::halt()
    {
        // rclcpp::shutdown();
        // RCLCPP_INFO(rclcpp::get_logger("debug"), "cancel button is pressed");
        _halt_requested.store(true);
    }
    bool PopUpMessage::init(rclcpp::Node::SharedPtr node_ptr)
    {
        if (node_ptr)
        {
            RCLCPP_INFO(node_ptr->get_logger(), " init from PopUpMessage");
            _user_question_publisher = node_ptr->create_publisher<GuiBtMessage>("/bt_question", 1);
            _user_answer_subscription = node_ptr->create_subscription<GuiBtMessage>(
                "/user_answer", 10, std::bind(&PopUpMessage::_user_answer_callback, this, std::placeholders::_1));
            return true;
        }
        else
        {
            RCLCPP_INFO(node_ptr->get_logger(), " node pointer not passed properly");
            return false;
        }
    }
    void PopUpMessage::_user_answer_callback(const GuiBtMessage::SharedPtr msg)
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "I heard: '%s'", msg->msg.c_str());
        if (msg)
            _user_answer = msg;
    }

} // namespace logic