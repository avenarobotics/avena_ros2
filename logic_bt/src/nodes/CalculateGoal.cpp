#include "logic_bt/nodes/CalculateGoal.hpp"
namespace logic_bt_nodes
{
    //-------------------------
    CalculateGoal::CalculateGoal(const std::string &name, const BT::NodeConfiguration &config)
        : BT::AsyncActionNode(name, config)
    {
    }
    BT::PortsList CalculateGoal::providedPorts()
    {
        return {BT::OutputPort<CustomDataTypes::Pose2D>("target")};
    }

    BT::NodeStatus CalculateGoal::tick()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");

        CustomDataTypes::Pose2D target = {1.1, 2.3, .25};
        setOutput<CustomDataTypes::Pose2D>("target", target);

        RCLCPP_INFO(rclcpp::get_logger("debug"), "[ CalculateGoal: STARTED ]. target: x=%.f y=%.1f theta=%.2f\n",
                    target.x, target.y, target.theta);

        // _halt_requested.store(false);
        // _action_result.store(std::nullopt);
        // RCLCPP_INFO(rclcpp::get_logger("debug"), "sending goal");
        // client_ptr_->async_send_goal(goal, _send_goal_options);
        auto goal_msg = SimpleAction::Goal();
        client_ptr_->async_send_goal(goal_msg, _send_goal_options);
        if (!_halt_requested)
        {
            while (!_action_result) // keep running if not halted or not received a result
            {

                // RCLCPP_INFO(rclcpp::get_logger("debug"), "action is runnnig");
                // for visulazation
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            }
            // received result
            RCLCPP_INFO(rclcpp::get_logger("debug"), "action is finished");
            // _cleanup(); // clear all after finishing goal
            if (_action_result.value() == static_cast<int8_t>(rclcpp_action::ResultCode::SUCCEEDED))
            {
                _cleanup(); // clear all after finishing goal
                return BT::NodeStatus::SUCCESS;
            }
            else
            {
                _cleanup(); // clear all after finishing goal
                return BT::NodeStatus::FAILURE;
            }
            // return (_action_result.value() == static_cast<int8_t>(rclcpp_action::ResultCode::SUCCEEDED)) ? BT::NodeStatus::SUCCESS : BT::NodeStatus::FAILURE;
        }
        else // halted
        {
            RCLCPP_INFO(rclcpp::get_logger("debug"), "action is halted");
            client_ptr_->async_cancel_all_goals();
            _cleanup(); // clear all after halt
            return BT::NodeStatus::FAILURE;
        }
    }
    void CalculateGoal::_cleanup()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tree node clean up");
        // _halt_requested = false; // something has to enable running again
        _action_result.reset();
        // _action_result = std::nullopt;
    }
    void CalculateGoal::halt()
    {
        // rclcpp::shutdown();
        // RCLCPP_INFO(rclcpp::get_logger("debug"), "cancel button is pressed");
        _halt_requested.store(true);
    }
    bool CalculateGoal::init(rclcpp::Node::SharedPtr node_ptr)
    {
        if (node_ptr)
        {
            RCLCPP_INFO(node_ptr->get_logger(), " init from CalculateGoal");
            client_ptr_ = rclcpp_action::create_client<SimpleAction>(
                node_ptr,
                "test");
            if (!client_ptr_->wait_for_action_server(std::chrono::seconds(1)))
            {
                RCLCPP_ERROR(node_ptr->get_logger(), "Action server not available after waiting");
                // halt the tree node
                halt();
                return false;
            }
            _send_goal_options = rclcpp_action::Client<SimpleAction>::SendGoalOptions();
            _send_goal_options.goal_response_callback =
                std::bind(&CalculateGoal::_goal_response_callback, this, std::placeholders::_1, node_ptr);
            _send_goal_options.feedback_callback =
                std::bind(&CalculateGoal::_feedback_callback, this, std::placeholders::_1, std::placeholders::_2, node_ptr);
            _send_goal_options.result_callback =
                std::bind(&CalculateGoal::_result_callback, this, std::placeholders::_1, node_ptr);
            return true;
        }
        else
        {
            RCLCPP_INFO(node_ptr->get_logger(), " node pointer not passed properly");
            return false;
        }
    }
    void CalculateGoal::_goal_response_callback(std::shared_future<GoalHandleSimpleAction::SharedPtr> future, rclcpp::Node::SharedPtr node_ptr)
    {
        auto goal_handle = future.get();
        if (!goal_handle)
        {
            RCLCPP_ERROR(node_ptr->get_logger(), "Goal was rejected by server");
        }
        else
        {
            RCLCPP_INFO(node_ptr->get_logger(), "Goal accepted by server, Asynchronous waiting for result");
        }
    }
    void CalculateGoal::_feedback_callback(GoalHandleSimpleAction::SharedPtr,
                                           const std::shared_ptr<const SimpleAction::Feedback> feedback, rclcpp::Node::SharedPtr node_ptr)
    {
        (void)feedback;
        RCLCPP_INFO(node_ptr->get_logger(), "feedback is received");
    }
    void CalculateGoal::_result_callback(const GoalHandleSimpleAction::WrappedResult &result, rclcpp::Node::SharedPtr node_ptr)
    {
        switch (result.code)
        {
        case rclcpp_action::ResultCode::SUCCEEDED:
            RCLCPP_INFO(node_ptr->get_logger(), "Goal was succeeded");
            _action_result = static_cast<int8_t>(rclcpp_action::ResultCode::SUCCEEDED);
            return;
        case rclcpp_action::ResultCode::ABORTED:
            RCLCPP_ERROR(node_ptr->get_logger(), "Goal was aborted");
            _action_result = static_cast<int8_t>(rclcpp_action::ResultCode::ABORTED);
            return;
        case rclcpp_action::ResultCode::CANCELED:
            RCLCPP_ERROR(node_ptr->get_logger(), "Goal was canceled");
            _action_result = static_cast<int8_t>(rclcpp_action::ResultCode::CANCELED);
            return;
        default:
            RCLCPP_ERROR(node_ptr->get_logger(), "Unknown result code");
            _action_result = static_cast<int8_t>(rclcpp_action::ResultCode::UNKNOWN);
            return;
        }
    }

} // namespace logic