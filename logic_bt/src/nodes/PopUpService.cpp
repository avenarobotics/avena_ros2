#include "logic_bt/nodes/PopUpService.hpp"
namespace logic_bt_nodes
{
    //-------------------------
    PopUpService::PopUpService(const std::string &name, const BT::NodeConfiguration &config)
        : BT::AsyncActionNode(name, config)
    {
    }
    BT::PortsList PopUpService::providedPorts()
    {
        return {BT::InputPort<std::string>("what_to_pick"), BT::InputPort<std::string>("where_to_put")};
    }

    BT::NodeStatus PopUpService::tick()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");

        if (!_halt_requested)
        {
            std::string what_to_pick, where_to_put;
            if (getInput<std::string>("what_to_pick", what_to_pick) && getInput<std::string>("where_to_put", where_to_put))
            {
                auto request = std::make_shared<custom_interfaces::srv::GUIPopUp::Request>();
                request->what_to_pick = what_to_pick;
                request->where_to_put = where_to_put;
                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ PopUpService: STARTED ]. What to pick: " << what_to_pick << " , where_to_put: " << where_to_put);
                // _halt_requested.store(false);
                auto result_future = _client_ptr->async_send_request(request);
                result_future.wait();

                // if (future_status != std::future_status::ready)
                //     throw(std::runtime_error(std::string("error while loading coppelia items")));

                auto result = result_future.get()->result;
                RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "result of service" << result);
                return result ? BT::NodeStatus::SUCCESS : BT::NodeStatus::FAILURE;
                // if (_node_ptr)
                // {
                //     if (rclcpp::spin_until_future_complete(_node_ptr, result_future) !=
                //         rclcpp::FutureReturnCode::SUCCESS)
                //     {
                //         RCLCPP_ERROR(rclcpp::get_logger("debug"), "service call failed ");
                //         return BT::NodeStatus::FAILURE;
                //     }
                //     else
                //     {
                //         auto result = result_future.get()->result;
                //         RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "result of service" << result);
                //         return result ? BT::NodeStatus::SUCCESS : BT::NodeStatus::FAILURE;
                //     }
                // }
                // else
                // {
                //     RCLCPP_ERROR(rclcpp::get_logger("debug"), "node is not passed properly");
                //     return BT::NodeStatus::FAILURE;
                // }
            }
            else
            {
                RCLCPP_ERROR(rclcpp::get_logger("debug"), "inputs are not passed");
                return BT::NodeStatus::FAILURE;
            }
        }
        else // halted
        {
            RCLCPP_INFO(rclcpp::get_logger("debug"), "action is halted");
            // client_ptr_->async_cancel_all_goals();
            _cleanup(); // clear all after halt
            return BT::NodeStatus::FAILURE;
        }
    }
    void PopUpService::_cleanup()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tree node clean up");
        // _halt_requested = false; // something has to enable running again
        // _action_result.reset();
        // _action_result = std::nullopt;
    }
    void PopUpService::halt()
    {
        // rclcpp::shutdown();
        // RCLCPP_INFO(rclcpp::get_logger("debug"), "cancel button is pressed");
        _halt_requested.store(true);
    }
    bool PopUpService::init(rclcpp::Node::SharedPtr node_ptr)
    {
        if (node_ptr)
        {
            RCLCPP_INFO(node_ptr->get_logger(), " init from PopUpService");
            _client_ptr = node_ptr->create_client<custom_interfaces::srv::GUIPopUp>("gui_popup");
            // pass node ptr (only for services)
            // _node_ptr = node_ptr;
            if (!_client_ptr->wait_for_service(std::chrono::seconds(1)))
            {
                if (!rclcpp::ok())
                {
                    RCLCPP_ERROR(node_ptr->get_logger(), "client interrupted while waiting for service to appear.");
                    return false;
                }
                RCLCPP_ERROR(node_ptr->get_logger(), "service server not available after waiting");
                return false;
            }
            return true;
        }
        else
        {
            RCLCPP_INFO(node_ptr->get_logger(), " node pointer not passed properly");
            return false;
        }
    }

} // namespace logic