#include "logic_bt/nodes/GUIParamsWriter.hpp"
namespace logic_bt_nodes
{
    //-------------------------
    GUIParamsWriter::GUIParamsWriter(const std::string &name, const BT::NodeConfiguration &config)
        : BT::ConditionNode(name, config)
    {
    }
    BT::PortsList GUIParamsWriter::providedPorts()
    {
        return {BT::OutputPort<std::string>("what_to_pick"), BT::OutputPort<std::string>("where_to_put")};
    }

    BT::NodeStatus GUIParamsWriter::tick()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");
        RCLCPP_INFO(rclcpp::get_logger("debug"), "[ GUIParamsWriter: STARTED ]");
        RCLCPP_INFO(rclcpp::get_logger("debug"), "condition satsified");
        return BT::NodeStatus::SUCCESS;
    }
    void GUIParamsWriter::_cleanup()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tree node clean up");
        // _halt_requested = false; // something has to enable running again
        // _action_result = std::nullopt;
    }
    bool GUIParamsWriter::init(rclcpp::Node::SharedPtr node_ptr)
    {
        if (node_ptr)
        {
            RCLCPP_INFO(node_ptr->get_logger(), " init from GUIParamsWriter");
            return true;
        }
        else
        {
            RCLCPP_INFO(node_ptr->get_logger(), " node pointer not passed properly");
            return false;
        }
    }

} // namespace logic