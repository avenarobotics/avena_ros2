#include "logic_bt/nodes/GoalCondition.hpp"
namespace logic_bt_nodes
{
    //-------------------------
    GoalCondition::GoalCondition(const std::string &name, const BT::NodeConfiguration &config)
        : BT::ConditionNode(name, config)
    {
    }
    BT::PortsList GoalCondition::providedPorts()
    {
        return {BT::InputPort<std::string>("what_to_pick"), BT::InputPort<std::string>("where_to_put")};
    }

    BT::NodeStatus GoalCondition::tick()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tick");

        // CustomDataTypes::Pose2D goal;
        std::string what_to_pick, where_to_put;
        if (getInput<std::string>("what_to_pick", what_to_pick) && getInput<std::string>("where_to_put", where_to_put))
        {
            RCLCPP_INFO_STREAM(rclcpp::get_logger("debug"), "[ GoalCondition: STARTED ]. What to pick: " << what_to_pick << " , where_to_put: " << where_to_put);
            if (what_to_pick == "plate")
            {
                RCLCPP_INFO(rclcpp::get_logger("debug"), "condition satsified ");
                return BT::NodeStatus::SUCCESS;
            }
            else
            {
                RCLCPP_INFO(rclcpp::get_logger("debug"), "condition unsatsified ");
                return BT::NodeStatus::FAILURE;
            }
        }
        else // not filled yet is failure (because we do not have default value)
        {
            RCLCPP_INFO(rclcpp::get_logger("debug"), "missing input [what_to_pick,where_to_put]");
            // throw BT::RuntimeError("missing required input [goal]");
            return BT::NodeStatus::FAILURE;
        }
    }
    void GoalCondition::_cleanup()
    {
        RCLCPP_INFO(rclcpp::get_logger("debug"), "tree node clean up");
        // _halt_requested = false; // something has to enable running again
        // _action_result = std::nullopt;
    }
    bool GoalCondition::init(rclcpp::Node::SharedPtr node_ptr)
    {
        if (node_ptr)
        {
            RCLCPP_INFO(node_ptr->get_logger(), " init from GoalCondition");
            return true;
        }
        else
        {
            RCLCPP_INFO(node_ptr->get_logger(), " node pointer not passed properly");
            return false;
        }
    }

} // namespace logic