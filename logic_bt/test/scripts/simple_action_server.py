import rclpy
from rclpy.action import ActionServer
from rclpy.node import Node
 
from custom_interfaces.action import SimpleAction
 
 
class SimpleActionServer(Node):

    def __init__(self):
        super().__init__('simple_action_server')
        self._action_server = ActionServer(
            self,
            SimpleAction,
            'test',
            self.execute_callback)

    def execute_callback(self, goal_handle):
        self.get_logger().info('Executing goal...')
        goal_handle.succeed()
        result = SimpleAction.Result()
        return result


def main(args=None):
   rclpy.init(args=args)

   simple_action_server = SimpleActionServer()

   rclpy.spin(simple_action_server)


if __name__ == '__main__':
   main()