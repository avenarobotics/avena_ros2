#ifndef _POPUPSERVICE_HPP_
#define _POPUPSERVICE_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
#include "custom_interfaces/srv/gui_pop_up.hpp"

namespace logic_bt_nodes
{

    class PopUpService : public BT::AsyncActionNode
    {
    public:
        PopUpService(const std::string &name, const BT::NodeConfiguration &config);
        BT::NodeStatus tick() override;
        static BT::PortsList providedPorts();
        // This overloaded method is used to stop the execution of this node.
        void halt() override;
        // void init(rclcpp::Node::SharedPtr node);
        bool init(rclcpp::Node::SharedPtr node_ptr);

    private:
        std::atomic_bool _halt_requested{false};
        void _cleanup();
        rclcpp::Client<custom_interfaces::srv::GUIPopUp>::SharedPtr _client_ptr;
        // rclcpp::Node::SharedPtr _node_ptr; // only for service
    };

} // namespace logic_bt_nodes
#endif