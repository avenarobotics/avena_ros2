#ifndef _SIMPLECONDITION_HPP_
#define _SIMPLECONDITION_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
// #include "logic_bt/converters/Pose2D.hpp"
#include "rclcpp/rclcpp.hpp"
// #include "rclcpp_action/rclcpp_action.hpp"
// #include "custom_interfaces/action/simple_action.hpp"
#include <optional>
#include <atomic>

namespace logic_bt_nodes
{

    // inline void SleepMS(int ms)
    // {
    //     std::this_thread::sleep_for(std::chrono::milliseconds(ms));
    // }
    // using TimePoint = std::chrono::time_point<std::chrono::high_resolution_clock>;
    // using Milliseconds = std::chrono::milliseconds;

    class GoalCondition :  public BT::ConditionNode
    {
    public:
        // using SimpleAction = custom_interfaces::action::SimpleAction;
        // using GoalHandleSimpleAction = rclcpp_action::ClientGoalHandle<SimpleAction>;
        GoalCondition(const std::string &name, const BT::NodeConfiguration &config);
        BT::NodeStatus tick() override;
        static BT::PortsList providedPorts();
        // void init(rclcpp::Node::SharedPtr node);
        bool init(rclcpp::Node::SharedPtr node_ptr);

    private:
        // std::optional<std::atomic_int8_t> _action_result{std::nullopt};
        // bool _sent_goal{false};
        // rclcpp_action::Client<SimpleAction>::SharedPtr client_ptr_;
        // rclcpp_action::Client<SimpleAction>::SendGoalOptions _send_goal_options;
        // void _goal_response_callback(std::shared_future<GoalHandleSimpleAction::SharedPtr> future, rclcpp::Node::SharedPtr node_ptr);
        // void _feedback_callback(GoalHandleSimpleAction::SharedPtr,
                                // const std::shared_ptr<const SimpleAction::Feedback> feedback, rclcpp::Node::SharedPtr node_ptr);
        // void _result_callback(const GoalHandleSimpleAction::WrappedResult &result, rclcpp::Node::SharedPtr node_ptr);
        void _cleanup();
    };

} // namespace logic_bt_nodes
#endif