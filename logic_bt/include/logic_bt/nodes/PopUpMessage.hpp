#ifndef _POPUPMESSAGE_HPP_
#define _POPUPMESSAGE_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
// #include "custom_interfaces/srv/gui_pop_up.hpp"
#include <custom_interfaces/msg/gui_bt_message.hpp>

namespace logic_bt_nodes
{

    class PopUpMessage : public BT::AsyncActionNode
    {
    public:
        using GuiBtMessage = custom_interfaces::msg::GuiBtMessage;
        PopUpMessage(const std::string &name, const BT::NodeConfiguration &config);
        BT::NodeStatus tick() override;
        static BT::PortsList providedPorts();
        // This overloaded method is used to stop the execution of this node.
        void halt() override;
        // void init(rclcpp::Node::SharedPtr node);
        bool init(rclcpp::Node::SharedPtr node_ptr);

    private:
        std::atomic_bool _halt_requested{false};
        void _cleanup();
        // rclcpp::Client<custom_interfaces::srv::GUIPopUp>::SharedPtr _client_ptr;
        rclcpp::Publisher<GuiBtMessage>::SharedPtr _user_question_publisher;
        rclcpp::Subscription<GuiBtMessage>::SharedPtr _user_answer_subscription;
        void _user_answer_callback(const GuiBtMessage::SharedPtr msg);
        GuiBtMessage::SharedPtr _user_answer; //
    };

} // namespace logic_bt_nodes
#endif