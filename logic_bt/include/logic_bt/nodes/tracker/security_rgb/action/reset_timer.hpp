#ifndef _RESETTIMER_HPP_
#define _RESETTIMER_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            // Maybe better SyncActionNode , but ros pub takes time
            class ResetTimer : public BT::AsyncActionNode
            {
            public:
                using String = std_msgs::msg::String;
                using ROSNode = rclcpp::Node;
                ResetTimer(const std::string &name, const BT::NodeConfiguration &config);
                BT::NodeStatus tick() override;
                static BT::PortsList providedPorts();
                bool init(ROSNode::SharedPtr node_ptr);

            private:
                rclcpp::Publisher<String>::SharedPtr _reset_timer_pub;
            };

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes
#endif