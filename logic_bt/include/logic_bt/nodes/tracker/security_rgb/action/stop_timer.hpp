#ifndef _STOPTIMER_HPP_
#define _STOPTIMER_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            // Maybe better SyncActionNode , but ros pub takes time
            class StopTimer : public BT::AsyncActionNode
            {
            public:
                using String = std_msgs::msg::String;
                using ROSNode = rclcpp::Node;
                StopTimer(const std::string &name, const BT::NodeConfiguration &config);
                BT::NodeStatus tick() override;
                static BT::PortsList providedPorts();
                bool init(ROSNode::SharedPtr node_ptr);

            private:
                rclcpp::Publisher<String>::SharedPtr _stop_timer_pub;
            };

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes
#endif