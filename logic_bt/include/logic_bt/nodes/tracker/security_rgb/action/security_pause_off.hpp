#ifndef _SECURITYPAUSE_OFF_HPP_
#define _SECURITYPAUSE_OFF_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/bool.hpp"

namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            // Maybe better SyncActionNode , but ros pub takes time 
            class SecurityPauseOff : public BT::AsyncActionNode 
            {
            public:
                using Bool = std_msgs::msg::Bool;
                using ROSNode = rclcpp::Node;
                SecurityPauseOff(const std::string &name, const BT::NodeConfiguration &config);
                BT::NodeStatus tick() override;
                static BT::PortsList providedPorts();
                bool init(ROSNode::SharedPtr node_ptr);

            private:
                rclcpp::Publisher<Bool>::SharedPtr _security_pause_pub;
            };

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes
#endif