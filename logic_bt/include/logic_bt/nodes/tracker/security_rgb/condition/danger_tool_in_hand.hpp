#ifndef _DANGERTOOLINHAND_HPP_
#define _DANGERTOOLINHAND_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/bool.hpp"
#include <optional>
#include <atomic>
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            class DangerToolInHand : public BT::ConditionNode
            {
            public:
                using Bool = std_msgs::msg::Bool;
                using ROSNode = rclcpp::Node;
                DangerToolInHand(const std::string &name, const BT::NodeConfiguration &config);
                BT::NodeStatus tick() override;
                static BT::PortsList providedPorts();
                bool init(rclcpp::Node::SharedPtr node_ptr);

            private:
                rclcpp::Subscription<Bool>::SharedPtr _danger_tool_in_hand_sub;
                void _danger_tool_in_hand_callback(const Bool::SharedPtr msg);
                std::optional<std::atomic_bool> _danger_tool_in_hand{std::nullopt};
            };

        } // namespace security_rgbi
    }     // namespace tracker
} // namespace logic_bt_nodes
#endif