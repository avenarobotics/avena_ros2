#ifndef _SECURITY_IS_TRIGGERED_HPP_
#define _SECURITY_IS_TRIGGERED_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/bool.hpp"
#include <optional>
#include <atomic>
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            class SecurityIsTriggered : public BT::ConditionNode
            {
            public:
                using Bool = std_msgs::msg::Bool;
                using ROSNode = rclcpp::Node;
                SecurityIsTriggered(const std::string &name, const BT::NodeConfiguration &config);
                BT::NodeStatus tick() override;
                bool init(ROSNode::SharedPtr node_ptr);
                static BT::PortsList providedPorts();

            private:
                rclcpp::Subscription<Bool>::SharedPtr _security_trigger_sub;
                void _security_trigger_callback(const Bool::SharedPtr msg);
                std::optional<std::atomic_bool> _security_trigger{std::nullopt};
            };

        } // namespace security_rgbi
    }     // namespace tracker
} // namespace logic_bt_nodes
#endif