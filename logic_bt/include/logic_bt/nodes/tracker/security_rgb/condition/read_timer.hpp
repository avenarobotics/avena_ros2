#ifndef _READTIMER_HPP_
#define _READTIMER_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/float64.hpp"
#include <optional>
#include <atomic>
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            class ReadTimer : public BT::ConditionNode
            {
            public:
                using Float64 = std_msgs::msg::Float64;
                using ROSNode = rclcpp::Node;
                ReadTimer(const std::string &name, const BT::NodeConfiguration &config);
                BT::NodeStatus tick() override;
                static BT::PortsList providedPorts();
                bool init(rclcpp::Node::SharedPtr node_ptr, const double time_out);

            private:
                rclcpp::Subscription<Float64>::SharedPtr _timer_reading_sub;
                void _timer_reading_callback(const Float64::SharedPtr msg);
                std::optional<std::atomic<double>> _timer_reading{std::nullopt};
                std::optional<double>  _time_out;
            };

        } // namespace security_rgbi
    }     // namespace tracker
} // namespace logic_bt_nodes
#endif