#ifndef _SECURITY_PAUSE_IS_ON_HPP_
#define _SECURITY_PAUSE_IS_ON_HPP_

#include "behaviortree_cpp_v3/bt_factory.h"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/bool.hpp"
#include <optional>
#include <atomic>
namespace logic_bt_nodes
{
    namespace tracker
    {
        namespace security_rgb
        {
            class SecurityPauseIsOn : public BT::ConditionNode
            {
            public:
                using Bool = std_msgs::msg::Bool;
                using ROSNode = rclcpp::Node;
                SecurityPauseIsOn(const std::string &name, const BT::NodeConfiguration &config);
                BT::NodeStatus tick() override;
                bool init(ROSNode::SharedPtr node_ptr);
                static BT::PortsList providedPorts();

            private:
                rclcpp::Subscription<Bool>::SharedPtr _security_pause_sub;
                void _security_pause_callback(const Bool::SharedPtr msg);
                std::optional<std::atomic_bool> _security_pause{std::nullopt};
            };

        } // namespace security_rgb
    }     // namespace tracker
} // namespace logic_bt_nodes
#endif