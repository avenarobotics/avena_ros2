#ifndef POSE2D_CONVERTER_H
#define POSE2D_CONVERTER_H

#include "behaviortree_cpp_v3/behavior_tree.h"
// Custom type
namespace CustomDataTypes
{
    struct Pose2D
    {
        double x, y, theta;
    };
} // namespace CustomDataTypes

namespace BT
{
    // This template specialization is needed only if you want
    // to AUTOMATICALLY convert a NodeParameter into a Pose2D
    // In other words, implement it if you want to be able to do:
    //
    //   TreeNode::getInput<Pose2D>(key, ...)
    //
    template <>
    inline CustomDataTypes::Pose2D convertFromString(StringView key)
    {
        // three real numbers separated by semicolons
        auto parts = BT::splitString(key, ';');
        if (parts.size() != 3)
        {
            throw BT::RuntimeError("invalid input)");
        }
        else
        {
            CustomDataTypes::Pose2D output;
            output.x = convertFromString<double>(parts[0]);
            output.y = convertFromString<double>(parts[1]);
            output.theta = convertFromString<double>(parts[2]);
            return output;
        }
    }
} // end namespace BT
#endif // POSE2D_CONVERTER_H