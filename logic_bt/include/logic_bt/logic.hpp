#ifndef _LOGIC_HPP
#define LOGIC_HPP
#include "nodes/MoveBaseAction.hpp"
#include "nodes/CalculateGoal.hpp"
#include "nodes/GoalCondition.hpp"
#include "nodes/GUIParamsWriter.hpp"
#include "nodes/tracker/security_rgb/condition/security_is_triggered.hpp"
#include "nodes/tracker/security_rgb/condition/danger_tool_in_hand.hpp"
#include "nodes/tracker/security_rgb/condition/read_timer.hpp"
#include "nodes/tracker/security_rgb/condition/read_timer_between.hpp"
#include "nodes/tracker/security_rgb/condition/security_pause_is_on.hpp"
#include "nodes/tracker/security_rgb/action/security_pause_on.hpp"
#include "nodes/tracker/security_rgb/action/security_pause_off.hpp"
#include "nodes/tracker/security_rgb/action/gui_warning.hpp"
#include "nodes/tracker/security_rgb/action/reset_timer.hpp"
#include "nodes/tracker/security_rgb/action/start_timer.hpp"
#include "nodes/tracker/security_rgb/action/stop_timer.hpp"
// #include "nodes/PopUpService.hpp"
#include "nodes/PopUpMessage.hpp"

#include "behaviortree_cpp_v3/loggers/bt_zmq_publisher.h"
#include "behaviortree_cpp_v3/loggers/bt_minitrace_logger.h"
#include "helpers_commons/helpers_commons.hpp"
#include "custom_interfaces/action/bt_pick_and_place_action.hpp"
#include <algorithm>
#include "logic_bt/converters/Pose2D.hpp"
#include <ament_index_cpp/get_package_share_directory.hpp>

#include "std_msgs/msg/float64.hpp"
#include "std_msgs/msg/string.hpp"
#include "timer.hpp"

// #include "custom_interfaces/msg/heartbeat.hpp"
// #include "helpers_commons/helpers_commons.hpp"
using Milliseconds = std::chrono::milliseconds;
namespace logic_bt
{
    // class Logic : public rclcpp::Node, public helpers::WatchdogInterface
    class Logic : public rclcpp::Node, public helpers::WatchdogInterface
    {
    private:
        using PickAndPlaceAction = custom_interfaces::action::BTPickAndPlaceAction;
        using GoalHandlePickAndPlaceAction = rclcpp_action::ServerGoalHandle<PickAndPlaceAction>;
        BT::Tree _logic_tree;
        helpers::Watchdog::SharedPtr _watchdog;
        std::shared_ptr<BT::PublisherZMQ> _publisher_zmq_ptr;
        rclcpp_action::Server<PickAndPlaceAction>::SharedPtr logic_server_;
        rclcpp_action::GoalResponse _handle_goal(
            const rclcpp_action::GoalUUID &uuid,
            std::shared_ptr<const PickAndPlaceAction::Goal> goal);
        rclcpp_action::CancelResponse _handle_cancel(
            const std::shared_ptr<GoalHandlePickAndPlaceAction> goal_handle);

        void _handle_accepted(const std::shared_ptr<GoalHandlePickAndPlaceAction> goal_handle);

        void _execute(const std::shared_ptr<GoalHandlePickAndPlaceAction> goal_handle);
        const std::array<std::string, 4> _commands{"start", "pause", "resume", "stop"};

        const std::unordered_map<BT::NodeStatus, std::string> _bt_states = {{BT::NodeStatus::RUNNING, "running"},
                                                                            {BT::NodeStatus::IDLE, "idle"},
                                                                            {BT::NodeStatus::SUCCESS, "success"},
                                                                            {BT::NodeStatus::FAILURE, "failure"}};
        rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr _timer_pub;
        rclcpp::Subscription<std_msgs::msg::String>::SharedPtr _timer_sub;
        void _timer_callback(const std_msgs::msg::String::SharedPtr msg);
        Timer _timer;

    public:
        explicit Logic(const rclcpp::NodeOptions &options);
        ~Logic();
        virtual void initNode() override;
        virtual void shutDownNode() override;
        // void initNode();
        // void shutDownNode();
        // virtual void run() override;
        void registerNodes(BT::BehaviorTreeFactory &factory);
    };
} // end of logic_bt
#endif
