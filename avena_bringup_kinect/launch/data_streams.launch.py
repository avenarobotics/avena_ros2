"""Launch a talker and a listener in a component container."""
import os
import sys
import launch
import yaml
from launch_ros.descriptions import ComposableNode
from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription, conditions
import launch.actions 
import launch_ros.actions 
from launch.event_handlers import OnProcessExit
# from launch.actions import LogInfo
# from launch.actions import (DeclareLaunchArgument, GroupAction, IncludeLaunchDescription)
# from launch_ros.actions import ComposableNodeContainer
from launch.substitutions import LaunchConfiguration, Command

from launch.launch_description_sources import PythonLaunchDescriptionSource

import launch.actions
import launch_ros.actions

extra_arguments = {'use_intra_process_comms': True}


cam_params = [
    {'cam1_depth_enabled': True},
    {'cam1_depth_mode': "WFOV_2X2BINNED"},
    {'cam1_color_enabled': True},
    {'cam1_color_format': "bgra"},
    {'cam1_color_resolution': "720P"},
    {'cam1_fps': 5},
    {'cam1_point_cloud': True},
    {'cam1_rgb_point_cloud': False},
    {'cam1_point_cloud_in_depth_frame': True},
    {'cam1_sensor_sn': ""},
    {'cam1_recording_file': ""},
    {'cam1_recording_loop_enabled': False},
    {'cam1_body_tracking_enabled': False},
    {'cam1_body_tracking_smoothing_factor': 1.0},
    {'cam1_rescale_ir_to_mono8': False},
    {'cam1_ir_mono8_scaling_factor': 1.0},
    {'cam1_imu_rate_target': 0},
    {'cam1_wired_sync_mode': 1},
    {'cam1_subordinate_delay_off_master_usec': 0},

    {'cam2_depth_enabled': True},
    {'cam2_depth_mode': "WFOV_2X2BINNED"},
    {'cam2_color_enabled': True},
    {'cam2_color_format': "bgra"},
    {'cam2_color_resolution': "720P"},
    {'cam2_fps': 5},
    {'cam2_point_cloud': True},
    {'cam2_rgb_point_cloud': False},
    {'cam2_point_cloud_in_depth_frame': True},
    {'cam2_sensor_sn': ""},
    {'cam2_recording_file': ""},
    {'cam2_recording_loop_enabled': False},
    {'cam2_body_tracking_enabled': False},
    {'cam2_body_tracking_smoothing_factor': 1.0},
    {'cam2_rescale_ir_to_mono8': False},
    {'cam2_ir_mono8_scaling_factor': 1.0},
    {'cam2_imu_rate_target': 0},
    {'cam2_wired_sync_mode': 2},
    {'cam2_subordinate_delay_off_master_usec': 2000},

    {'depth_change_factor': 0.01},
    {'smoothing_size': 4.0},
    {'shadow_treshold': 0.5},
    {'rect_size': 5}
]


def generate_launch_description():
    """Generate launch description with multiple components."""

    avena = launch.actions.IncludeLaunchDescription(
        PythonLaunchDescriptionSource(os.path.join(get_package_share_directory(
            'avena_bringup_kinect'), 'launch', 'avena.launch.py'))
    )

    tf = launch.actions.IncludeLaunchDescription(
            PythonLaunchDescriptionSource(os.path.join(get_package_share_directory(
                'camera_extrinsics_calibration'), 'launch', 'publish_extrinsics.launch.py'))
        )

    log_levels_params = {
        'ptcld_transformer': {'log_level': 'info'},
        'robot_self_filter': {'log_level': 'info'},
        'change_detect':     {'log_level': 'debug'},
        'scene_publisher':   {'log_level': 'info'},
    }

    container = launch_ros.actions.ComposableNodeContainer(
        name='data_streams_container',
        namespace='',
        package='rclcpp_components',
        executable='component_container',
        composable_node_descriptions=[
            ComposableNode(
                package='ptcld_transformer',
                plugin='ptcld_transformer::PtcldTransformer',
                name='ptcld_transformer',
                extra_arguments=[extra_arguments],
                parameters=[log_levels_params['ptcld_transformer']],
            ), 
            ComposableNode(
                package='robot_self_filter',
                plugin='robot_self_filter::RobotSelfFilter',
                name='robot_self_filter',
                extra_arguments=[extra_arguments],
                parameters=[log_levels_params['robot_self_filter']],
            ),
            ComposableNode(
                package='change_detect',
                plugin='change_detect::ChangeDetect',
                name='change_detect',
                extra_arguments=[extra_arguments],
                parameters=[log_levels_params['change_detect']],
            ),            
            ComposableNode(
                package='scene_publisher',
                plugin='scene_publisher::ScenePublisher',
                name='scene_publisher',
                extra_arguments=[extra_arguments],
                parameters=[log_levels_params['scene_publisher']],
            ),
        
        ],
        output='screen',
    )    
    driver_container = launch_ros.actions.ComposableNodeContainer(
        name='driver_container',
        namespace='',
        package='rclcpp_components',
        executable='component_container',
        composable_node_descriptions=[
            ComposableNode(
                package='azure_kinect_ros_driver',
                plugin='K4AFastPublisher',
                name='azure_kinect_ros_driver',
                extra_arguments=[extra_arguments],
                parameters=cam_params,
            )
        ],
        output='both',
    )

    emit_shutdown_action = launch.actions.Shutdown(reason='launch is shutting down')
    # emit_shutdown_action = sys.exit()
    # emit_shutdown_action = (os.kill(os.getpid(), 9))


    ld = launch.LaunchDescription([container,driver_container])
    # ld.add_action(launch.actions.RegisterEventHandler(event_handler=OnProcessExit(
    #     on_exit=[launch.actions.LogInfo(msg=["A Node stopped. Stopping everything..."]),
    #              emit_shutdown_action]
    # )))

    return ld

