#include <cstdlib>
#include <fstream>
#include <iostream>
#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <sensor_msgs/msg/joint_state.hpp>
#include "rclcpp/rclcpp.hpp"

using namespace std::chrono_literals;

class MinimalPublisher : public rclcpp::Node
{

public:
    MinimalPublisher()
        : Node("franka_joint_state_publisher")
    {
        RCLCPP_INFO(get_logger(), "Initializing franka joint state publisher");

        _pub_arm_joint_state = this->create_publisher<sensor_msgs::msg::JointState>("joint_states", 10);
        _arm_joints_timer = this->create_wall_timer(50ms, std::bind(&MinimalPublisher::_broadcastArmJointState, this));

    }

private:
    void _broadcastArmJointState()
    {
        sensor_msgs::msg::JointState joint_state_msg;
        if (!file_readed)
        {
             std::system("sshpass -p ros scp ros@10.3.15.221:/home/ros/joint_states.txt . ");

            std::ifstream input("joint_states.txt");
            for (std::string line; getline(input, line);)
                readed_poses.push_back(std::stof(line));

            file_readed = true;
        }

        
        std::vector<std::string> joint_names;
        joint_state_msg.position.resize(readed_poses.size(), 0);
        for (int i = 0; i < readed_poses.size(); i++){
            joint_names.push_back("joint_" + std::to_string(i+1));
            joint_state_msg.position[i] = readed_poses[i];
        }

        joint_state_msg.name = joint_names;
        joint_state_msg.header.stamp = now();
        _pub_arm_joint_state->publish(joint_state_msg);
    }
    bool file_readed = false;
    std::vector<float> readed_poses;
    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_arm_joint_state;
    rclcpp::TimerBase::SharedPtr _arm_joints_timer;
};

int main(int argc, char *argv[])
{

   
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<MinimalPublisher>());
    rclcpp::shutdown();

    return 0;
}
