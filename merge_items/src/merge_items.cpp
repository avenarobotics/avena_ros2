#include "merge_items/merge_items.hpp"

namespace merge_items
{

    MergeItems::MergeItems(const rclcpp::NodeOptions &options)
        : Node("merge_items", options)
    {
        helpers::commons::setLoggerLevelFromParameter(this);

        _merge_items_action_server = rclcpp_action::create_server<MergeItemsAction>(
            this,
            "merge_items",
            std::bind(&MergeItems::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&MergeItems::_handleCancel, this, std::placeholders::_1),
            std::bind(&MergeItems::_handleAccepted, this, std::placeholders::_1));

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local().reliable();
        _pub_merge_items = create_publisher<ItemsMsg>("merged_items", qos_settings);
        _initializeSubscribers(qos_settings);

        _merge_items_manager = std::make_shared<MergeItemsManager>(get_logger());

        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void MergeItems::initNode()
    {
        status = custom_interfaces::msg::Heartbeat::STARTING;

        status = custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void MergeItems::shutDownNode()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;

        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    MergeItems::~MergeItems() {}

    void MergeItems::_initializeSubscribers(const rclcpp::QoS &qos_settings)
    {
        _sub_estimate_shape_items = create_subscription<custom_interfaces::msg::Items>("estimate_shape", qos_settings,
                                                                                       [this](custom_interfaces::msg::Items::SharedPtr items)
                                                                                       {
                                                                                           RCLCPP_DEBUG(get_logger(), "Estimated shapes message received");
                                                                                           _estimate_shape_items_msg = items;
                                                                                       });
        _sub_missing_items_ids = create_subscription<custom_interfaces::msg::MissingItemsIds>("missing_items_ids", qos_settings,
                                                                                              [this](custom_interfaces::msg::MissingItemsIds::SharedPtr missing_items_ids)
                                                                                              {
                                                                                                  RCLCPP_DEBUG(get_logger(), "Estimated shapes message received");
                                                                                                  _missing_items_ids_msg = missing_items_ids;
                                                                                              });
    }

    rclcpp_action::GoalResponse MergeItems::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const MergeItemsAction::Goal> /*goal*/)
    {
        // RCLCPP_INFO(get_logger(), "Received goal request.");
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse MergeItems::_handleCancel(const std::shared_ptr<GoalHandleMergeItems> /*goal_handle*/)
    {
        RCLCPP_INFO(get_logger(), "Received request to cancel goal");
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void MergeItems::_handleAccepted(const std::shared_ptr<GoalHandleMergeItems> goal_handle)
    {
        auto execute = [this](const std::shared_ptr<GoalHandleMergeItems> goal_handle) -> void
        {
            helpers::Timer timer("Merge items action", get_logger());
            auto result = std::make_shared<MergeItemsAction::Result>();
            if (status != custom_interfaces::msg::Heartbeat::RUNNING)
            {
                RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
                goal_handle->abort(result);
                return;
            }
            RCLCPP_INFO(get_logger(), "Executing merge items action.");
            const auto goal = goal_handle->get_goal();

            if (_validateInput(_estimate_shape_items_msg, _missing_items_ids_msg) != ResultCode_e::SUCCESS)
            {
                RCLCPP_ERROR(get_logger(), "Invalid input data. Goal aborted.");
                _pub_merge_items->publish(ItemsMsg());
                goal_handle->abort(result);
                return;
            }
            _checkLastMessagesTimestamps(_estimate_shape_items_msg->header.stamp, _missing_items_ids_msg->header.stamp);

            ItemsMsg::UniquePtr merged_items(new ItemsMsg);
            if (_merge_items_manager->process(_estimate_shape_items_msg, _missing_items_ids_msg, merged_items) != ResultCode_e::SUCCESS)
            {
                RCLCPP_ERROR(get_logger(), "Error occured while merging . Goal aborted.");
                merged_items.reset();
                _pub_merge_items->publish(std::move(merged_items));
                goal_handle->abort(result);
                return;
            }

            if (rclcpp::ok())
            {
                RCLCPP_INFO(get_logger(), "Goal succeeded");
                // Store current items for next processing.
                merged_items->header.stamp = now();
                _merge_items_manager->savePreviousItems(merged_items);
                _pub_merge_items->publish(std::move(merged_items));
                goal_handle->succeed(result);
            }
        };
        std::thread{execute, goal_handle}.detach();
    }

    int MergeItems::_validateInput(const ItemsMsg::SharedPtr items_msg, const MissingItemsIdsMsg::SharedPtr missing_items_ids_msg)
    {
        if (!items_msg)
        {
            RCLCPP_WARN(get_logger(), "Module has not received estimated items data.");
            return ResultCode_e::FAILURE;
        }
        if (!missing_items_ids_msg)
        {
            RCLCPP_WARN(get_logger(), "Module has not received missing items ids data.");
            return ResultCode_e::FAILURE;
        }
        if (items_msg->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(get_logger(), "Invalid header in message.");
            return ResultCode_e::FAILURE;
        }
        if (missing_items_ids_msg->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(get_logger(), "Invalid header in message.");
            return ResultCode_e::FAILURE;
        }
        return ResultCode_e::SUCCESS;
    }

    void MergeItems::_checkLastMessagesTimestamps(const builtin_interfaces::msg::Time &new_items_stamp, const builtin_interfaces::msg::Time &missing_items_ids_stamp)
    {
        if (_last_new_items_processed_msg_timestamp == new_items_stamp)
            RCLCPP_WARN(get_logger(), "New message with new estimated items has not arrived yet. Processing old message.");
        _last_new_items_processed_msg_timestamp = new_items_stamp;

        if (_last_missing_items_ids_processed_msg_timestamp == missing_items_ids_stamp)
            RCLCPP_WARN(get_logger(), "New message with missing items IDs has not arrived yet. Processing old message.");
        _last_missing_items_ids_processed_msg_timestamp = missing_items_ids_stamp;
    }

} // namespace merge_items

#include <rclcpp_components/register_node_macro.hpp>
RCLCPP_COMPONENTS_REGISTER_NODE(merge_items::MergeItems)
