#include "merge_items/merge_items_manager.hpp"

namespace merge_items
{
    MergeItemsManager::MergeItemsManager(rclcpp::Logger logger)
        : _logger(logger) {}

    ResultCode_e MergeItemsManager::process(ItemsMsg::SharedPtr &new_items_msg, MissingItemsIdsMsg::SharedPtr &missing_items_ids_msg, ItemsMsg::UniquePtr &out_merged_items)
    {
        helpers::Timer timer("Merge items manager processing", _logger);

        // Remove duplicates
        RCLCPP_DEBUG_STREAM(_logger, "Number of new items before removing duplicates: " << new_items_msg->items.size());
        RCLCPP_INFO(_logger, "Removing duplicate estimations");
        if (_removeDuplicates(new_items_msg->items, _distance_threshold_duplicates) != ResultCode_e::SUCCESS)
            return ResultCode_e::FAILURE;
        RCLCPP_DEBUG_STREAM(_logger, "Number of new items after removing duplicates: " << new_items_msg->items.size());

        if (!_previous_items || _previous_items->items.size() == 0)
        {
            RCLCPP_INFO(_logger, "Module is called for the first time. Publishing all items from input.");
            _previous_items = std::make_shared<ItemsMsg>();
            out_merged_items->items = new_items_msg->items;
            return ResultCode_e::SUCCESS;
        }

        // Assignments for convenience
        auto &missing_items_ids = missing_items_ids_msg->items_ids;
        auto &out_items = out_merged_items->items;
        auto &new_items = new_items_msg->items;
        auto &prev_items = _previous_items->items;

        // auto displayItemData = [this](const ItemMsg &item) {
        //     std::stringstream ss;
        //     ss << "\nBegin Item:" << std::endl;
        //     ss << "  ID: " << item.id << std::endl;
        //     ss << "  Label: " << item.label << std::endl;
        //     ss << "  Number of item elements: " << item.item_elements.size() << std::endl;
        //     if (item.item_elements.size() > 0)
        //     {
        //         ss << "  Item elements:" << std::endl;
        //         for (auto &ie : item.item_elements)
        //         {
        //             ss << "    ID: " << ie.id << std::endl;
        //             ss << "    Label: " << ie.label << std::endl;
        //         }
        //     }
        //     ss << "End Item" << std::endl;
        //     RCLCPP_WARN(_logger, ss.str());
        // };

        RCLCPP_DEBUG_STREAM(_logger, "Remaining new items before matching same items: " << new_items.size());
        RCLCPP_DEBUG_STREAM(_logger, "Remaining previous items before matching same items: " << prev_items.size());

        RCLCPP_INFO(_logger, "Matching items which were not moved");
        if (_mergeStaticItems(new_items, prev_items, _match_distance, out_items) != ResultCode_e::SUCCESS)
            return ResultCode_e::FAILURE;

        RCLCPP_DEBUG_STREAM(_logger, "Remaining new items after matching same items: " << new_items.size());
        RCLCPP_DEBUG_STREAM(_logger, "Remaining previous items after matching same items: " << prev_items.size());

        RCLCPP_INFO(_logger, "Handling items which might be moved (they might be set as missing)");
        if (_mergeMissingItems(new_items, prev_items, missing_items_ids, _moved_item_match_distance, out_items) != ResultCode_e::SUCCESS)
            return ResultCode_e::FAILURE;

        RCLCPP_DEBUG_STREAM(_logger, "Remaining new items after matching missing items: " << new_items.size());
        RCLCPP_DEBUG_STREAM(_logger, "Remaining previous items after matching missing items: " << prev_items.size());

        RCLCPP_INFO(_logger, "Adding new items");
        if (_addNewItems(new_items, out_items) != ResultCode_e::SUCCESS)
            return ResultCode_e::FAILURE;

        RCLCPP_DEBUG_STREAM(_logger, "Number of items after merging: " << out_items.size());
        // for (auto it = out_items.begin(); it != out_items.end(); ++it)
        // {
        //     displayItemData(*it);
        //     // std::cout << "ID: " << it->id << ", label: " << it->label << std::endl;
        // }

        return ResultCode_e::SUCCESS;
    }

    ResultCode_e MergeItemsManager::_mergeStaticItems(std::vector<ItemMsg> &new_items, std::vector<ItemMsg> &prev_items, const float &match_distance, std::vector<ItemMsg> &out_items)
    {
        helpers::Timer timer("Merge items which were not moved", _logger);

        for (auto prev_item_it = prev_items.begin(); prev_item_it != prev_items.end();)
        {
            bool prev_item_matched_with_new_item = false;
            for (auto new_item_it = new_items.begin(); new_item_it != new_items.end();)
            {
                if (new_item_it->label != prev_item_it->label)
                {
                    ++new_item_it;
                    continue;
                }

                auto distance_between_items = _getDistanceBetweenItemsPositions(*new_item_it, *prev_item_it);
                if (distance_between_items < match_distance)
                {
                    prev_item_matched_with_new_item = true;
                    _addItem(prev_item_it->id, prev_item_it->label, *new_item_it, out_items);
                    new_item_it = new_items.erase(new_item_it);
                    prev_item_it = prev_items.erase(prev_item_it);
                    break;
                }
                else
                    ++new_item_it;
            }

            if (!prev_item_matched_with_new_item)
                ++prev_item_it;
        }
        return ResultCode_e::SUCCESS;
    }

    ResultCode_e MergeItemsManager::_mergeMissingItems(std::vector<ItemMsg> &new_items, std::vector<ItemMsg> &prev_items,
                                                       std::vector<int32_t> &missing_items_ids, const float &moved_item_match_distance,
                                                       std::vector<ItemMsg> &out_items)
    {
        helpers::Timer timer("Merge missing items", _logger);

        for (auto prev_item_it = prev_items.begin(); prev_item_it != prev_items.end(); ++prev_item_it)
        {
            auto missing_item_id_it = std::find(missing_items_ids.begin(), missing_items_ids.end(), prev_item_it->id);
            if (missing_item_id_it != missing_items_ids.end())
            {
                // missing_items_ids.erase(missing_item_id_it);

                for (auto new_item_it = new_items.begin(); new_item_it != new_items.end();)
                {
                    if (new_item_it->label != prev_item_it->label)
                    {
                        ++new_item_it;
                        continue;
                    }

                    auto distance_between_items = _getDistanceBetweenItemsPositions(*new_item_it, *prev_item_it);
                    // RCLCPP_WARN_STREAM(_logger, "\tdistance: " << distance_between_items);
                    if (distance_between_items < moved_item_match_distance)
                    {
                        _addItem(prev_item_it->id, prev_item_it->label, *new_item_it, out_items);
                        new_item_it = new_items.erase(new_item_it);
                        break;
                    }
                    else
                    {
                        ++new_item_it;
                    }
                }
            }
            else
            {
                RCLCPP_DEBUG_STREAM(_logger, "Add previous item with ID: " << prev_item_it->id << ", label: " << prev_item_it->label);
                _addItem(prev_item_it->id, prev_item_it->label, *prev_item_it, out_items);
            }
        }

        return ResultCode_e::SUCCESS;
    }

    ResultCode_e MergeItemsManager::_addNewItems(std::vector<ItemMsg> &new_items, std::vector<ItemMsg> &out_items)
    {
        // auto displayItemData = [this](const ItemMsg &item) {
        //     std::stringstream ss;
        //     ss << "\nBegin Item:" << std::endl;
        //     ss << "  ID: " << item.id << std::endl;
        //     ss << "  Label: " << item.label << std::endl;
        //     ss << "  Number of item elements: " << item.item_elements.size() << std::endl;
        //     if (item.item_elements.size() > 0)
        //     {
        //         ss << "  Item elements:" << std::endl;
        //         for (auto &ie : item.item_elements)
        //         {
        //             ss << "    ID: " << ie.id << std::endl;
        //             ss << "    Label: " << ie.label << std::endl;
        //         }
        //     }
        //     ss << "End Item" << std::endl;
        //     RCLCPP_WARN(_logger, ss.str());
        // };

        helpers::Timer timer("Add new items", _logger);
        if (new_items.size() > 0)
        {
            auto item_with_max_id = std::max_element(out_items.begin(), out_items.end(), [](ItemMsg &l, ItemMsg &r) { return l.id < r.id; });
            int32_t current_item_id;
            if (item_with_max_id == out_items.end())
            {
                RCLCPP_DEBUG(_logger, "No previous items were matched.");
                current_item_id = _last_previous_item_id + 1;
            }
            else
                current_item_id = item_with_max_id->id + 1;

            // Add remaining new items as output items
            for (auto &remaining_new_item_it : new_items)
            {
                if (current_item_id > _last_allowed_item_id)
                    current_item_id = 1;
                remaining_new_item_it.id = current_item_id++;
                out_items.push_back(remaining_new_item_it);
            }
        }
        return ResultCode_e::SUCCESS;
    }

    void MergeItemsManager::_addItem(const ItemId &item_id, const Label &label, const ItemMsg &item, std::vector<ItemMsg> &out_items)
    {
        ItemMsg item_to_add;
        item_to_add.id = item_id;
        item_to_add.label = label;
        item_to_add.pose = item.pose;
        item_to_add.item_elements = item.item_elements;
        out_items.push_back(item_to_add);
    }

    float MergeItemsManager::_getDistanceBetweenItemsPositions(const ItemMsg &new_item, const ItemMsg &prev_item)
    {
        auto prev_item_pos = _getItemCenterPosition(prev_item);
        auto new_item_pos = _getItemCenterPosition(new_item);
        return (prev_item_pos - new_item_pos).norm();
    }

    Eigen::Vector3f MergeItemsManager::_getItemCenterPosition(const ItemMsg &item)
    {
        Eigen::Affine3f item_aff;
        helpers::converters::geometryToEigenAffine(item.pose, item_aff);
        Eigen::Vector3f item_center_position = item_aff.translation();

        // Eigen::Vector3f item_center_position = Eigen::Vector3f::Zero();
        // if (_supported_item_labels.find(item.label) != _supported_item_labels.end())
        // {
        //     ItemElementMsg element_to_measure;
        //     if (item.label == "broccoli")
        //     {
        //         if (item.item_elements.size() == 1)
        //             element_to_measure = item.item_elements[0];
        //         element_to_measure = item.item_elements[0].label == "broccoli_handle" ? item.item_elements[0] : item.item_elements[1];
        //     }
        //     else if (item.label == "spatula" || item.label == "knife")
        //     {
        //         if (item.item_elements.size() == 1)
        //             element_to_measure = item.item_elements[0];
        //         element_to_measure = item.item_elements[0].label == "handle" ? item.item_elements[0] : item.item_elements[1];
        //     }
        //     else
        //         element_to_measure = item.item_elements[0];
        //     json item_center_pos_json = json::parse(element_to_measure.position);
        //     // std::cout << "item_center_pos_json : " << item_center_pos_json << std::endl;
        //     if (item_center_pos_json.contains("x") && item_center_pos_json.contains("y") && item_center_pos_json.contains("z"))
        //         item_center_position = helpers::commons::assignPositionFromJson(item_center_pos_json);
        //     else
        //     {
        //         RCLCPP_WARN_STREAM(_logger, "Item with ID: " << item.id << " with label \"" << item.label << "\" has invalid position.");
        //         item_center_position = {-2, -2, -2};
        //     }
        // }

        // else
        //     RCLCPP_WARN_STREAM(_logger, "Item with ID: " << item.id << " with label \"" << item.label << "\" is not supported.");
        return item_center_position;
    }

    ResultCode_e MergeItemsManager::_removeDuplicates(std::vector<ItemMsg> &items, const float &distance_threshold)
    {
        helpers::Timer timer("Remove duplicates", _logger);
        auto compareItems = [this](const ItemMsg &l, const ItemMsg &r) -> bool {
            auto l_item = _getItemCenterPosition(l);
            auto r_item = _getItemCenterPosition(r);
            return l_item.norm() < r_item.norm();
        };

        auto equalItems = [this, &distance_threshold](const ItemMsg &l, const ItemMsg &r) -> bool {
            if (l.label != r.label)
                return false;
            auto l_item = _getItemCenterPosition(l);
            auto r_item = _getItemCenterPosition(r);
            return (l_item - r_item).norm() < distance_threshold;
        };

        std::sort(items.begin(), items.end(), compareItems);
        auto unique_end = std::unique(items.begin(), items.end(), equalItems);
        items.erase(unique_end, items.end());
        return ResultCode_e::SUCCESS;
    }

    void MergeItemsManager::savePreviousItems(const ItemsMsg::UniquePtr &previous_items)
    {
        _previous_items->items = previous_items->items;
        // Get valid item ID for next call
        auto item_with_max_id = std::max_element(_previous_items->items.begin(), _previous_items->items.end(), [](ItemMsg &l, ItemMsg &r) { return l.id < r.id; });
        if (item_with_max_id == _previous_items->items.end())
            _last_previous_item_id = 1;
        else
            _last_previous_item_id = item_with_max_id->id;
    }

} // namespace merge_items
