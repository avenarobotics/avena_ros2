cmake_minimum_required(VERSION 3.5)
project(merge_items)

# Default to C99
if(NOT CMAKE_C_STANDARD)
  set(CMAKE_C_STANDARD 99)
endif()

# Default to C++17
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# Find dependencies
find_package(ament_cmake REQUIRED)
find_package(ament_cmake_ros REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_action REQUIRED)
find_package(rclcpp_components REQUIRED)
find_package(helpers_vision REQUIRED)
find_package(helpers_commons REQUIRED)
find_package(custom_interfaces REQUIRED)

# Library as a component
add_library(merge_items SHARED
  src/merge_items_manager.cpp
  src/merge_items.cpp
)
target_include_directories(merge_items 
PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)
target_compile_definitions(merge_items PRIVATE "MERGE_ITEMS_BUILDING_LIBRARY")
ament_target_dependencies(merge_items
  rclcpp
  rclcpp_action
  rclcpp_components
  helpers_vision
  helpers_commons
  custom_interfaces
)
rclcpp_components_register_nodes(merge_items "merge_items::MergeItems")

# Installing library, headers and launch file
install(
  DIRECTORY include/
  DESTINATION include
)
install(
  TARGETS merge_items
  EXPORT export_${PROJECT_NAME}
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin
)
install(
  DIRECTORY launch
  DESTINATION share/${PROJECT_NAME}
)

# Ament exports to be able to use library
ament_export_include_directories(
  include
)
ament_export_libraries(
  merge_items
)
ament_export_targets(
  export_${PROJECT_NAME}
)
ament_export_dependencies(
  helpers_vision
  helpers_commons
)

# This command makes it possible to find all installed libraries by ROS
ament_package()
