#!/bin/bash

echo "Running pipeline"

set -e 

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

####################################################
run ros2 run cli scene_publisher
run ros2 run cli detect
run ros2 run cli filter_detections
run ros2 run cli select_new_masks
run ros2 run cli compose_items
run ros2 run cli estimate_shape
run ros2 run cli merge_items
run ros2 run cli octomap_filter
run ros2 run cli spawn_collision_items
####################################################

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
