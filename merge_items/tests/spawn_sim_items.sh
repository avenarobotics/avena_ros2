#!/bin/bash

echo "Running pipeline"

set -e 

PATH=$PATH:~/ros2_ws/src/system_tests/scripts
source ~/ros2_ws/src/system_tests/scripts/bash_utils.sh

####################################################
main_label=bowl
rest_items_label=cucumber

script_clear_scene.sh
action_spawn_position.sh $main_label random 0,0,0 3
action_spawn_position.sh $rest_items_label random random 3
# sleep 5
####################################################

echo -e "\e[32mALL TESTS PASSED. \e[0m"
exit 0
