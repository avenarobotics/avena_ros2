import launch
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode


def generate_launch_description():
    """Generate launch description with merge_items component."""
    container = ComposableNodeContainer(
        name='merge_items_container',
        namespace='',
        package='rclcpp_components',
        executable='component_container',
        composable_node_descriptions=[
            ComposableNode(
                package='merge_items',
                plugin='merge_items::MergeItems',
                name='merge_items',
                parameters=[{'log_level': 'debug'}],
            ),
        ],
        output='screen',
    )
    return launch.LaunchDescription([container])
