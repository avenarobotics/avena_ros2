#ifndef MERGE_ITEMS__COMMONS_HPP_
#define MERGE_ITEMS__COMMONS_HPP_

// ___Avena packages___
#include "custom_interfaces/msg/items.hpp"
#include "custom_interfaces/msg/missing_items_ids.hpp"
#include "merge_items/visibility_control.h"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"

namespace merge_items
{
  using json = nlohmann::json;
  using ItemsMsg = custom_interfaces::msg::Items;
  using ItemMsg = custom_interfaces::msg::Item;
  using ItemElementMsg = custom_interfaces::msg::ItemElement;
  using MissingItemsIdsMsg = custom_interfaces::msg::MissingItemsIds;
  using Label = std::string;
  using ItemId = int32_t;

  enum ResultCode_e
  {
    SUCCESS = 0,
    FAILURE = 1
  };

} // namespace merge_items

#endif // MERGE_ITEMS__COMMONS_HPP_
