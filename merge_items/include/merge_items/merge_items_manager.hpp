#ifndef MERGE_ITEMS__MERGE_ITEMS_MANAGER_HPP_
#define MERGE_ITEMS__MERGE_ITEMS_MANAGER_HPP_

// ___ROS___
#include <rclcpp/rclcpp.hpp>

// ___Package___
#include "merge_items/commons.hpp"

namespace merge_items
{
  class MergeItemsManager
  {
  public:
    MERGE_ITEMS_PUBLIC
    explicit MergeItemsManager(rclcpp::Logger logger);

    MERGE_ITEMS_PUBLIC
    ~MergeItemsManager() = default;

    MERGE_ITEMS_PUBLIC
    MergeItemsManager(const MergeItemsManager &other) = delete;

    MERGE_ITEMS_PUBLIC
    ResultCode_e process(ItemsMsg::SharedPtr &new_items_msg, MissingItemsIdsMsg::SharedPtr &missing_items_ids_msg, ItemsMsg::UniquePtr &out_merged_items);

    MERGE_ITEMS_PUBLIC
    void savePreviousItems(const ItemsMsg::UniquePtr &previous_items);

    using SharedPtr = std::shared_ptr<MergeItemsManager>;
    using UniquePtr = std::unique_ptr<MergeItemsManager>;

  private:
    MERGE_ITEMS_LOCAL
    float _getDistanceBetweenItemsPositions(const ItemMsg &new_item, const ItemMsg &prev_item);

    MERGE_ITEMS_LOCAL
    Eigen::Vector3f _getItemCenterPosition(const ItemMsg &item);

    MERGE_ITEMS_LOCAL
    ResultCode_e _removeDuplicates(std::vector<ItemMsg> &items, const float &distance_threshold);

    MERGE_ITEMS_LOCAL
    ResultCode_e _mergeStaticItems(std::vector<ItemMsg> &new_items, std::vector<ItemMsg> &prev_items, const float &match_distance, std::vector<ItemMsg> &out_items);

    MERGE_ITEMS_LOCAL
    ResultCode_e _mergeMissingItems(std::vector<ItemMsg> &new_items, std::vector<ItemMsg> &prev_items,
                                    std::vector<int32_t> &missing_items_ids, const float &moved_item_match_distance,
                                    std::vector<ItemMsg> &out_items);

    MERGE_ITEMS_LOCAL
    ResultCode_e _addNewItems(std::vector<ItemMsg> &new_items, std::vector<ItemMsg> &out_items);

    MERGE_ITEMS_LOCAL
    void _addItem(const ItemId &item_id, const Label &label, const ItemMsg &item, std::vector<ItemMsg> &out_items);

    ItemsMsg::SharedPtr _previous_items;
    rclcpp::Logger _logger;
    int32_t _last_previous_item_id;

    // ___Constants___
    const float _distance_threshold_duplicates = 0.04;
    const float _match_distance = 0.05;
    const float _moved_item_match_distance = 0.5;
    const int32_t _last_allowed_item_id = 200;
  };

} // namespace merge_items

#endif // MERGE_ITEMS__MERGE_ITEMS_HPP_
