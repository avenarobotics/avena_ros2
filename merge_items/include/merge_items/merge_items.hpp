#ifndef MERGE_ITEMS__MERGE_ITEMS_HPP_
#define MERGE_ITEMS__MERGE_ITEMS_HPP_

// ___ROS___
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>

// ___Avena packages___
#include "custom_interfaces/action/simple_action.hpp"

// ___Package___
#include "merge_items/merge_items_manager.hpp"

namespace merge_items
{
  class MergeItems : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    using MergeItemsAction = custom_interfaces::action::SimpleAction;
    using GoalHandleMergeItems = rclcpp_action::ServerGoalHandle<MergeItemsAction>;

    MERGE_ITEMS_PUBLIC
    explicit MergeItems(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    void initNode() override;
    void shutDownNode() override;

    MERGE_ITEMS_PUBLIC
    virtual ~MergeItems();

  private:
    helpers::Watchdog::SharedPtr _watchdog;

    // ___Member functions___
    MERGE_ITEMS_LOCAL
    rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const MergeItemsAction::Goal> goal);

    MERGE_ITEMS_LOCAL
    rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandleMergeItems> goal_handle);

    MERGE_ITEMS_LOCAL
    void _handleAccepted(const std::shared_ptr<GoalHandleMergeItems> goal_handle);

    MERGE_ITEMS_LOCAL
    int _validateInput(const ItemsMsg::SharedPtr items_msg, const MissingItemsIdsMsg::SharedPtr missing_items_ids_msg);

    MERGE_ITEMS_LOCAL
    void _checkLastMessagesTimestamps(const builtin_interfaces::msg::Time &new_items_stamp, const builtin_interfaces::msg::Time &missing_items_ids_stamp);

    MERGE_ITEMS_LOCAL
    void _initializeSubscribers(const rclcpp::QoS &qos_settings);

    // ___Members___
    rclcpp_action::Server<MergeItemsAction>::SharedPtr _merge_items_action_server;
    rclcpp::Publisher<ItemsMsg>::SharedPtr _pub_merge_items;
    rclcpp::Subscription<custom_interfaces::msg::Items>::SharedPtr _sub_estimate_shape_items;
    rclcpp::Subscription<custom_interfaces::msg::MissingItemsIds>::SharedPtr _sub_missing_items_ids;
    custom_interfaces::msg::Items::SharedPtr _estimate_shape_items_msg;
    custom_interfaces::msg::MissingItemsIds::SharedPtr _missing_items_ids_msg;

    MergeItemsManager::SharedPtr _merge_items_manager;
    builtin_interfaces::msg::Time _last_new_items_processed_msg_timestamp;
    builtin_interfaces::msg::Time _last_missing_items_ids_processed_msg_timestamp;
  };

} // namespace merge_items

#endif // MERGE_ITEMS__MERGE_ITEMS_HPP_
