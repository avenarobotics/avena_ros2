#ifndef MERGE_ITEMS__VISIBILITY_CONTROL_H_
#define MERGE_ITEMS__VISIBILITY_CONTROL_H_

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define MERGE_ITEMS_EXPORT __attribute__ ((dllexport))
    #define MERGE_ITEMS_IMPORT __attribute__ ((dllimport))
  #else
    #define MERGE_ITEMS_EXPORT __declspec(dllexport)
    #define MERGE_ITEMS_IMPORT __declspec(dllimport)
  #endif
  #ifdef MERGE_ITEMS_BUILDING_LIBRARY
    #define MERGE_ITEMS_PUBLIC MERGE_ITEMS_EXPORT
  #else
    #define MERGE_ITEMS_PUBLIC MERGE_ITEMS_IMPORT
  #endif
  #define MERGE_ITEMS_PUBLIC_TYPE MERGE_ITEMS_PUBLIC
  #define MERGE_ITEMS_LOCAL
#else
  #define MERGE_ITEMS_EXPORT __attribute__ ((visibility("default")))
  #define MERGE_ITEMS_IMPORT
  #if __GNUC__ >= 4
    #define MERGE_ITEMS_PUBLIC __attribute__ ((visibility("default")))
    #define MERGE_ITEMS_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define MERGE_ITEMS_PUBLIC
    #define MERGE_ITEMS_LOCAL
  #endif
  #define MERGE_ITEMS_PUBLIC_TYPE
#endif

#endif  // MERGE_ITEMS__VISIBILITY_CONTROL_H_
