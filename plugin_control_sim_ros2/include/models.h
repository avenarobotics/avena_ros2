#pragma once

#include <iostream>
#include <filesystem>
#include <fstream>
#include <random>
#include <filesystem>
#include <algorithm>
#include <map>
#include <rclcpp/rclcpp.hpp>

namespace fs = std::filesystem;

static const std::string MODELS_ROOT = getenv("HOME") + std::string("/ros2_ws/src/avena_ros2/scenes/models/dataset_models/");

std::vector<std::string> str_split(std::string s, std::string delimiter);

struct Models
{
    std::map<std::string, std::string> dict;

    Models()
    {
        if(!fs::is_directory(MODELS_ROOT))
        {
            RCLCPP_ERROR(rclcpp::get_logger("coppelia_control"), "Directory with models does not exists");
            RCLCPP_ERROR(rclcpp::get_logger("coppelia_control"), MODELS_ROOT);
            throw(std::runtime_error(std::string("Directory with models does not exists"))); 
        }
        std::string cli_name;
        std::string model_name;
        std::vector<std::string> splited_path;
        std::vector<std::string> splited_name;
        for (const auto &file_name : fs::directory_iterator(MODELS_ROOT))
        {
            splited_path = str_split(file_name.path(), "/");
            model_name = splited_path[splited_path.size() - 1];
            splited_name = str_split(model_name, "proper");
            cli_name = splited_name[0];
            cli_name.erase(cli_name.size() - 1);
            size_t floor_pos = cli_name.find('_');
            if (floor_pos != std::string::npos)
                cli_name.erase(floor_pos, 1);
            dict.insert({cli_name, model_name});
        }
    }
    void display()
    {
        printf("%-30s%-30s\n", "CLI", "MODEL_PATH");
        for (auto item : dict)
        {
            printf("%-30s%-30s\n", item.first.c_str(), item.second.c_str());
        }
    }

    std::string getRandomObjectName()
    {
        auto it = dict.begin();
        std::advance(it, rand() % dict.size());
        std::string key = it->first;
        return key;
    }
};

