#pragma once

#include "config.h"
#include "simPlusPlus/Plugin.h"
#include "stubs.h"

#include <rclcpp/rclcpp.hpp>

#include "custom_interfaces/srv/clear_coppelia.hpp"
#include "custom_interfaces/srv/spawn_item.hpp"
#include "custom_interfaces/srv/list3_d_models.hpp"
#include "custom_interfaces/srv/list_spawned_items.hpp"
#include "custom_interfaces/srv/get_items_info.hpp"
#include "custom_interfaces/srv/set_simulation_status.hpp"
#include "custom_interfaces/srv/get_coppelia_handle.hpp"

#include "models.h"
#include "utilities.h"
#include <unordered_map>

#include <regex>
#include "nlohmann/json.hpp"
#include "helpers_commons/helpers_commons.hpp"

#define PLUGIN_NAME "ControlSimROS2"
#define PLUGIN_VERSION 1

namespace control_sim_ros_2
{
    using namespace std::chrono_literals;

    class ControlSim : public sim::Plugin, rclcpp::Node
    {
    private:
        std::string plugin_namespace;
        
        simInt _dummy_for_spawned_objects_handle;
        simInt _dummy_reference_point;

        nlohmann::json _areas_info;

        rclcpp::Service<custom_interfaces::srv::ClearCoppelia>::SharedPtr _srv_clear_coppelia;
        rclcpp::Service<custom_interfaces::srv::SpawnItem>::SharedPtr _srv_spawn_model;
        rclcpp::Service<custom_interfaces::srv::List3DModels>::SharedPtr _srv_list_3_d_models;
        rclcpp::Service<custom_interfaces::srv::ListSpawnedItems>::SharedPtr _srv_list_spawned_items;
        rclcpp::Service<custom_interfaces::srv::GetItemsInfo>::SharedPtr _srv_get_items_info;
        rclcpp::Service<custom_interfaces::srv::SetSimulationStatus>::SharedPtr _srv_set_simulation_status;
        rclcpp::Service<custom_interfaces::srv::GetCoppeliaHandle>::SharedPtr _srv_get_coppelia_handle;
        
        bool _parameters_read;


        bool _spawnObject(const std::string& type, geometry_msgs::msg::Pose& pose, Models& models);
        void _createDummyForSpawnedObjects();

        geometry_msgs::msg::Pose _getItemPose(simInt handle);
        geometry_msgs::msg::Vector3 _getItemSize(simInt handle);
        void _getSpawnedItemsHandles(std::vector<simInt>& handles);

        int _getAreasInfo();
    public:
        ControlSim();
        virtual ~ControlSim();
        void onStart();
        void onInstancePass(const sim::InstancePassFlags &flags, bool first) override;
        void onSimulationAboutToStart() override;

        
        void startSetSimulationStatus();
        void startClearCoppeliaService();
        void startSpawnModelService();
        void startList3DModels();
        void startListSpawnedItems();
        void startGetItemsInfo();
        void startGetCoppeliaHandle();
    };
}; // namespace control_sim_ros_2