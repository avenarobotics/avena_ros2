#include "models.h"

std::vector<std::string> str_split(std::string s, std::string delimiter)
{
    std::vector<std::string> result;
    size_t last = 0;
    size_t next = 0;
    while ((next = s.find(delimiter, last)) != std::string::npos)
    {
        result.push_back(s.substr(last, next - last));
        last = next + 1;
    }
    result.push_back(s.substr(last));
    return result;
}