#include "plugin.h"

namespace control_sim_ros_2
{
#ifdef USE_BRAIN
    ControlSim::ControlSim()
        : Node("control_coppelia_brain")
    {
        plugin_namespace = "control_coppelia_brain";
        RCLCPP_INFO(get_logger(), "Started all services on namespace: %s", plugin_namespace.c_str());
    }
#else
    ControlSim::ControlSim()
        : Node("control_coppelia_camera")
    {
        plugin_namespace = "control_coppelia_camera";
        RCLCPP_INFO(get_logger(), "Started all services on namespace: %s", plugin_namespace.c_str());
    }
#endif

    ControlSim::~ControlSim()
    {
    }

    void ControlSim::onStart()
    {
        if (!registerScriptStuff())
            throw std::runtime_error("script stuff initialization failed");
        setExtVersion("Plugin for publishing robot links state (transforms between them)");
        setBuildDate(BUILD_DATE);
        startClearCoppeliaService();
        startSpawnModelService();
        startList3DModels();
        startListSpawnedItems();
        startGetItemsInfo();
        startSetSimulationStatus();
        startGetCoppeliaHandle();

        _parameters_read = false;
        _getAreasInfo();
    }

    void ControlSim::onSimulationAboutToStart()
    {
        _createDummyForSpawnedObjects();
        _dummy_reference_point = simGetObjectHandle("DummyWorld");
        if (_dummy_reference_point == -1)
            RCLCPP_WARN(get_logger(), "Returning absolute position");
        else
        {
            RCLCPP_INFO(get_logger(), "Returning position relative to " + std::to_string(_dummy_reference_point));
        }
    }

    void ControlSim::_createDummyForSpawnedObjects()
    {
        simInt collision_parent_handle = simGetObjectHandle("collisions_parent");

        if (collision_parent_handle != -1)
        {
            _dummy_for_spawned_objects_handle = collision_parent_handle;
            RCLCPP_INFO(get_logger(), "Using dummy from scene with id " + std::to_string(collision_parent_handle));
            return;
        }

        collision_parent_handle = simCreateDummy(0.0001, nullptr);
        if (collision_parent_handle == -1)
        {
            RCLCPP_ERROR(get_logger(), "Creating dummy was unsucessfull");
        }
        _dummy_for_spawned_objects_handle = collision_parent_handle;

        simInt naming_operation_result = simSetObjectName(_dummy_for_spawned_objects_handle, "collisions_parent");

        RCLCPP_INFO(get_logger(), "Created dummy with id " + std::to_string(_dummy_for_spawned_objects_handle));
    }

    void ControlSim::onInstancePass(const sim::InstancePassFlags &flags, bool first)
    {
        //RCLCPP_INFO_STREAM_THROTTLE(get_logger(), *get_clock(), 1000 , "SPIN" << std::this_thread::get_id());
        rclcpp::spin_some(this->get_node_base_interface());
    }

    void ControlSim::startGetCoppeliaHandle()
    {
        auto get_coppelia_handle = [this](
                                       const std::shared_ptr<custom_interfaces::srv::GetCoppeliaHandle::Request> request,
                                       std::shared_ptr<custom_interfaces::srv::GetCoppeliaHandle::Response> response) -> void {
            RCLCPP_INFO(get_logger(), "Incoming request: [/custom_interfaces/srv/GetCoppeliaHandle]");

            std::vector<simInt> handles;
            _getSpawnedItemsHandles(handles);
            RCLCPP_INFO(get_logger(), "Size: %d", handles.size());
            for (auto handle : handles)
            {
                std::string full_name = simGetObjectName(handle);
                std::string number_in_name = std::regex_replace(full_name, std::regex(R"([\D])"), "");
                int item_id = std::stoi(number_in_name);
                RCLCPP_INFO(get_logger(), "Number in name: %s => %d", full_name.c_str(), item_id);

                if (item_id == request->item_id)
                {
                    response->coppelia_handle = handle;
                    response->coppelia_name.data = full_name;
                    break;
                }
            }
        };

        RCLCPP_INFO(get_logger(), "GetCoppeliaHandle service starting...");
        _srv_get_coppelia_handle = create_service<custom_interfaces::srv::GetCoppeliaHandle>(
            "/" + plugin_namespace + "/get_coppelia_handle",
            get_coppelia_handle);
        RCLCPP_INFO(get_logger(), "GetCoppeliaHandle service started");
    }

    void ControlSim::startClearCoppeliaService()
    {
        auto clear_coppelia = [this](
                                  const std::shared_ptr<custom_interfaces::srv::ClearCoppelia::Request> request,
                                  std::shared_ptr<custom_interfaces::srv::ClearCoppelia::Response> response) -> void {
            RCLCPP_INFO(get_logger(), "Incoming request: [/custom_interfaces/srv/ClearCoppelia]");

            int child_handle = simGetObjectChild(this->_dummy_for_spawned_objects_handle, 0);
            while (child_handle != -1)
            {
                simRemoveObject(child_handle);
                child_handle = simGetObjectChild(this->_dummy_for_spawned_objects_handle, 0);
            }

            response->success = true;
        };

        RCLCPP_INFO(get_logger(), "ClearCoppelia service starting...");
        _srv_clear_coppelia = create_service<custom_interfaces::srv::ClearCoppelia>(
            "/" + plugin_namespace + "/clear_coppelia",
            clear_coppelia);
        RCLCPP_INFO(get_logger(), "ClearCoppelia service started");
    }

    bool ControlSim::_spawnObject(const std::string &type, geometry_msgs::msg::Pose &pose, Models &models)
    {
        std::string model_path = std::string(MODELS_ROOT) + "/" + models.dict[type];
        RCLCPP_INFO(get_logger(), "Trying to spawn: " + model_path);
        simInt handle = simLoadModel(model_path.c_str());
        if (handle == -1)
        {
            RCLCPP_ERROR(get_logger(), "Spawning operation was unsucessfull [No such file]");
            return false;
        }

        float position[3];
        float oriantation[4];

        position[0] = pose.position.x;
        position[1] = pose.position.y;
        position[2] = pose.position.z;

        simInt operation_result = simSetObjectPosition(handle, -1, position);
        if (operation_result == -1)
        {
            RCLCPP_ERROR(get_logger(), "Setting position was unsucessfull");
            return false;
        }

        oriantation[0] = pose.orientation.x;
        oriantation[1] = pose.orientation.y;
        oriantation[2] = pose.orientation.z;
        oriantation[3] = pose.orientation.w;

        operation_result = simSetObjectQuaternion(handle, -1, oriantation);
        if (operation_result == -1)
        {
            RCLCPP_ERROR(get_logger(), "Setting orientation was unsucesfull");
            return false;
        }

        operation_result = simSetObjectParent(handle, _dummy_for_spawned_objects_handle, true);
        if (operation_result == -1)
        {
            RCLCPP_ERROR(get_logger(), "Setting parent was unsucesfull");
            return false;
        }
        RCLCPP_INFO(get_logger(), "Spawning was sucessfull");
        return true;
    }

    void ControlSim::startListSpawnedItems()
    {
        auto list_spawned_items = [this](const std::shared_ptr<custom_interfaces::srv::ListSpawnedItems::Request> request, std::shared_ptr<custom_interfaces::srv::ListSpawnedItems::Response> response) -> void {
            RCLCPP_INFO(get_logger(), "Incoming request: [/custom_interfaces/srv/ListSpawnedItems]");
            std::vector<simInt> handles;
            _getSpawnedItemsHandles(handles);

            std::for_each(
                handles.begin(),
                handles.end(),
                [this, &response](simInt handle) {
                    simChar *item_name = simGetObjectName(handle);
                    if (item_name == nullptr)
                    {
                        RCLCPP_ERROR(get_logger(), "Getting item name was unsucessfull");
                    }
                    else
                    {
                        std_msgs::msg::String ros_name;
                        ros_name.data = std::string(item_name);
                        response->items.push_back(ros_name);
                    }
                });
        };

        RCLCPP_INFO(get_logger(), "ListSpawnedItems service starting...");
        _srv_list_spawned_items = create_service<custom_interfaces::srv::ListSpawnedItems>(
            "/" + plugin_namespace + "/list_spawned_items",
            list_spawned_items);
        RCLCPP_INFO(get_logger(), "ListSpawnedItems service started");
    }

    void ControlSim::startSpawnModelService()
    {
        auto spawn_model = [this](const std::shared_ptr<custom_interfaces::srv::SpawnItem::Request> request, std::shared_ptr<custom_interfaces::srv::SpawnItem::Response> response) -> void {
            RCLCPP_INFO(get_logger(), "Incoming request: [/custom_interfaces/srv/SpawnModel]");

            if (_getAreasInfo())
            {
                RCLCPP_ERROR(get_logger(), "Areas parameters are null! Aborting...");
                response->success = false;
                return;
            }

            time_var start = timeNow();
            Models models;

            if (request->amount.data <= 0)
            {
                RCLCPP_ERROR(get_logger(), "Amount of object must be a positive number");
                response->success = false;
                return;
            }

            if (request->random_type == false && request->type.data.empty())
            {
                RCLCPP_ERROR(get_logger(), "Set random_type to true or set non empty type");
                response->success = false;
                return;
            }

            if (request->random_type == false && models.dict.find(request->type.data) == models.dict.end())
            {
                RCLCPP_ERROR(get_logger(), "Model not found");
                response->success = false;
                return;
            }

            if ((request->point_name.data != "" || request->area_name.data != "") && !request->random_position)
            {
                RCLCPP_ERROR(get_logger(), "You spefied area/point and position parameters in the same request");
                response->success = false;
                return;
            }

            int amount = request->amount.data;
            std::string type;
            geometry_msgs::msg::Pose pose;

            //RCLCPP_ERROR_STREAM(get_logger(), std::setw(4) << _areas_info);

            float start_z = 1.0;

            while (amount--)
            {
                type = (request->random_type) ? models.getRandomObjectName() : request->type.data;

                if (request->point_name.data != "")
                {
                    nlohmann::json position = _areas_info["table_points"][request->point_name.data];
                    //RCLCPP_DEBUG_STREAM(get_logger(), "Point to spawn: " << std::setw(4) << position);
                    pose.position.x = static_cast<double>(position["x"]);
                    pose.position.y = static_cast<double>(position["y"]);
                }
                else if (request->area_name.data != "")
                {
                    RCLCPP_DEBUG_STREAM(get_logger(), "Area to spawn: " << request->area_name.data);
                    nlohmann::json min_position = _areas_info[request->area_name.data]["min"];
                    nlohmann::json max_position = _areas_info[request->area_name.data]["max"];

                    pose.position = getRandomPosition(
                        static_cast<float>(min_position["x"]) + 0.1,
                        static_cast<float>(min_position["y"]) + 0.1,
                        static_cast<float>(max_position["x"]) - 0.1,
                        static_cast<float>(max_position["y"]) - 0.1,
                        start_z);
                }
                else if (!request->random_position)
                {
                    RCLCPP_DEBUG_STREAM(get_logger(), "Position from parameter");
                    pose.position = request->pose.position;
                }
                else
                {
                    RCLCPP_DEBUG_STREAM(get_logger(), "Random position on table");
                    nlohmann::json min_position = _areas_info["table_area"]["min"];
                    nlohmann::json max_position = _areas_info["table_area"]["max"];
                    pose.position = getRandomPosition(
                        static_cast<float>(min_position["x"]) + 0.1,
                        static_cast<float>(min_position["y"]) + 0.1,
                        static_cast<float>(max_position["x"]) - 0.1,
                        static_cast<float>(max_position["y"]) - 0.1,
                        start_z);
                }

                RCLCPP_DEBUG_STREAM(get_logger(), "Pose: \n\t" << pose.position.x << "\n\t" << pose.position.y << "\n");

                pose.position.z = start_z;
                pose.orientation = (request->random_orientation) ? getRandomQuaternion() : request->pose.orientation;
                _spawnObject(type, pose, models);
                start_z += 0.30;
            }

            response->success = true;
        };

        RCLCPP_INFO(get_logger(), "SpawnItem service starting...");
        _srv_spawn_model = create_service<custom_interfaces::srv::SpawnItem>(
            "/" + plugin_namespace + "/spawn_items",
            spawn_model);
        RCLCPP_INFO(get_logger(), "SpawnItem service started");
    }

    void ControlSim::startList3DModels()
    {
        auto list_3_d_models = [this](const std::shared_ptr<custom_interfaces::srv::List3DModels::Request> request, std::shared_ptr<custom_interfaces::srv::List3DModels::Response> response) -> void {
            Models models;
            for (auto [cli, model_file_name] : models.dict)
            {
                std_msgs::msg::String cli_string;
                cli_string.data = cli;
                response->models.push_back(cli_string);
            }
        };

        RCLCPP_INFO(get_logger(), "List3DModels service starting...");
        _srv_list_3_d_models = create_service<custom_interfaces::srv::List3DModels>(
            "/" + plugin_namespace + "/list_3_d_models",
            list_3_d_models);
        RCLCPP_INFO(get_logger(), "List3DModels service started");
    }

    void ControlSim::startSetSimulationStatus()
    {
        auto set_simulation_status = [this](const std::shared_ptr<custom_interfaces::srv::SetSimulationStatus::Request> request, std::shared_ptr<custom_interfaces::srv::SetSimulationStatus::Response> response) -> void {
            if (request->status.data == "stop")
            {
                simStopSimulation();
            }
            else if (request->status.data == "pause")
            {
                simPauseSimulation();
            }
            else if (request->status.data == "start")
            {
                simStartSimulation();
            }
            else
            {
                throw(std::runtime_error(std::string("unrecognized operation: " + request->status.data)));
            }
        };

        RCLCPP_INFO(get_logger(), "SetSimulationStatus service starting...");
        _srv_set_simulation_status = create_service<custom_interfaces::srv::SetSimulationStatus>(
            "/" + plugin_namespace + "/set_simulation_status",
            set_simulation_status);
        RCLCPP_INFO(get_logger(), "SetSimulationStatus service started");
    }

    void ControlSim::_getSpawnedItemsHandles(std::vector<simInt> &handles)
    {
        simInt object_count;
        simInt *returned_handles = simGetObjectsInTree(_dummy_for_spawned_objects_handle, sim_object_shape_type, 2, &object_count);

        handles = std::vector<simInt>(returned_handles, returned_handles + object_count);
    }

    void ControlSim::startGetItemsInfo()
    {
        auto get_items_info = [this](const std::shared_ptr<custom_interfaces::srv::GetItemsInfo::Request> request, std::shared_ptr<custom_interfaces::srv::GetItemsInfo::Response> response) -> void {
            std::vector<simInt> handles;
            _getSpawnedItemsHandles(handles);
            for (auto handle : handles)
            {

                simChar *item_name = simGetObjectName(handle);
                if (item_name == nullptr)
                    throw std::runtime_error(std::string("error while getting name of object"));

                custom_interfaces::msg::CoppeliaItemInfo item;
                RCLCPP_INFO(get_logger(), "Getting name of item with ID: " + std::to_string(handle));
                item.label = std::string(item_name);
                RCLCPP_INFO(get_logger(), "Label: " + item.label);
                RCLCPP_INFO(get_logger(), "Getting pose of item with ID: " + std::to_string(handle));
                item.pose = this->_getItemPose(handle);
                RCLCPP_INFO(get_logger(), "Getting size of item with ID: " + std::to_string(handle));
                item.size = this->_getItemSize(handle);

                response->coppelia_items.push_back(item);
            };
        };

        RCLCPP_INFO(get_logger(), "GetItemsInfo service starting...");
        _srv_get_items_info = create_service<custom_interfaces::srv::GetItemsInfo>(
            "/" + plugin_namespace + "/get_items_info",
            get_items_info);
        RCLCPP_INFO(get_logger(), "GetItemsInfo service started");
    }

    geometry_msgs::msg::Pose ControlSim::_getItemPose(simInt handle)
    {
        geometry_msgs::msg::Pose pose;

        RCLCPP_INFO(get_logger(), "Getting position relative to: " + std::to_string(_dummy_reference_point));
        simFloat position[3];
        simInt getting_position_result = simGetObjectPosition(handle, _dummy_reference_point, position);
        RCLCPP_INFO(get_logger(), "Getting position result" + std::to_string(getting_position_result));
        if (getting_position_result == -1)
            RCLCPP_ERROR(get_logger(), "error while getting position of object");
        pose.position.x = position[0];
        pose.position.y = position[1];
        pose.position.z = position[2];

        RCLCPP_INFO(get_logger(), "Getting rotation");
        simFloat quaternion[4];
        simInt getting_quaternion_result = simGetObjectQuaternion(handle, _dummy_reference_point, quaternion);
        if (getting_quaternion_result == -1)
            RCLCPP_ERROR(get_logger(), "error while getting quaternion of object");
        pose.orientation.x = quaternion[0];
        pose.orientation.y = quaternion[1];
        pose.orientation.z = quaternion[2];
        pose.orientation.w = quaternion[3];

        return pose;
    }

    geometry_msgs::msg::Vector3 ControlSim::_getItemSize(simInt handle)
    {
        geometry_msgs::msg::Vector3 size;

        int intData[5];
        float floatData[5];

        simInt shape_type = simGetShapeGeomInfo(handle, intData, floatData, nullptr);

        size.x = floatData[0];
        size.y = floatData[1];
        size.z = floatData[2];

        return size;
    }

    int ControlSim::_getAreasInfo()
    {
        RCLCPP_INFO_ONCE(get_logger(), "Reading parameters from the server");

        if (_parameters_read)
            return 0;

        auto parameters = helpers::commons::getParameters({"areas", "table_points"});
        if (parameters.empty())
            return 1;
        _areas_info = parameters["areas"];
        _areas_info["table_points"] = parameters["table_points"];

        RCLCPP_INFO(get_logger(), "Parameters read successfully...");
        _parameters_read = true;
        return 0;
    }

    SIM_PLUGIN("ControlSimROS2", 1, ControlSim)
#include "stubsPlusPlus.cpp"
} // namespace control_sim_ros_2