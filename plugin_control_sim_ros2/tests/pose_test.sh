#!/bin/bash

ros2 run virtualscene clear



ros2 run virtualscene create label bowl amount 1 point p_3_2 orientation 0,0,0
ros2 run virtualscene create label orange amount 10 point p_3_2

ros2 run virtualscene create label bowl3 amount 1 point p_2_5 orientation 0,0,0
ros2 run virtualscene create label orange amount 5 point p_2_5

ros2 run virtualscene create label bowl2 amount 1 point p_5_8 orientation 0,0,0
ros2 run virtualscene create label orange amount 2 point p_5_8

ros2 run virtualscene create label orange amount 10 area operating_area
