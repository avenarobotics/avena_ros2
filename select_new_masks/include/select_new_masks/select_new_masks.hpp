#ifndef SELECT_NEW_MASKS_COMPONENT_HPP
#define SELECT_NEW_MASKS_COMPONENT_HPP

// ___CPP___
#include <functional>
#include <memory>
#include <string>
#include <nlohmann/json.hpp>


// __ROS__
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include <rclcpp/parameter.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>

// ___AVENA___
#include "custom_interfaces/action/simple_action.hpp"
#include "custom_interfaces/msg/items.hpp"
#include "custom_interfaces/msg/detections.hpp"
#include "custom_interfaces/msg/heartbeat.hpp"
#include "custom_interfaces/msg/missing_items_ids.hpp"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"

#include "custom_interfaces/msg/heartbeat.hpp"

struct items_mask_t
{
    uint32_t item_id;
    cv::Mat mask_cam1;
    cv::Mat mask_cam2;
};

struct missing_t
{
    cv::Mat mask;
    std::string label;
};


struct label_group
{   
  label_group(std::string label) : label(label){}
    std::string label;
    std::vector<cv::Mat> mask_cam1;
    std::vector<cv::Mat> mask_cam2;
    std::vector<items_mask_t> items;
};


namespace select_new_masks
{
  using json = nlohmann::json;
  using ItemElement = custom_interfaces::msg::ItemElement;
  using Item = custom_interfaces::msg::Item;
  using namespace custom_interfaces::msg; // usage only in this namespace, so not a big problem
  using transform_map = std::map<std::string, Eigen::Affine3f, std::less<std::string>, Eigen::aligned_allocator<std::pair<const std::string, Eigen::Affine3f>>>;

  class SelectNewMasks : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    explicit SelectNewMasks(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    virtual void initNode() override;
    virtual void shutDownNode() override;

    using SelectMasks = custom_interfaces::action::SimpleAction;
    using GoalHandleSelectMasks = rclcpp_action::ServerGoalHandle<SelectMasks>;

  private:
    helpers::Watchdog::SharedPtr _watchdog;

    //ROS
    rclcpp_action::Server<SelectMasks>::SharedPtr _action_server;
    rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const SelectMasks::Goal> goal);
    rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandleSelectMasks> goal_handle);
    void _handleAccepted(const std::shared_ptr<GoalHandleSelectMasks> goal_handle);
    void _execute(const std::shared_ptr<GoalHandleSelectMasks> goal_handle);

    int _readLabels();
    int _unpackItems(std::vector<label_group> &label_groups);
    int _unpackDetections(std::vector<label_group> &label_groups);
    int _compareInputs(custom_interfaces::msg::Detections::UniquePtr &out_detections, std::vector<uint32_t> &out_missing_items);
    bool _detectOverlap(cv::Mat &detection_mask, cv::Mat &item_mask);

   
    rclcpp::Publisher<custom_interfaces::msg::Detections>::SharedPtr _new_masks_publisher;
    rclcpp::Publisher<custom_interfaces::msg::MissingItemsIds>::SharedPtr _missing_items_publisher;
    rclcpp::Subscription<custom_interfaces::msg::Items>::SharedPtr _merged_items_subscriber;
    rclcpp::Subscription<custom_interfaces::msg::Detections>::SharedPtr _filtered_detections_subscriber;
    custom_interfaces::msg::Detections::SharedPtr _detections_data;
    custom_interfaces::msg::Items::SharedPtr _items_data;


    std::map<std::string, bool> _items;
    std::map<std::string, bool> _elements;
    std::map<std::string, std::vector<std::string>> _components;
    float masks_overlap_procentage;

    builtin_interfaces::msg::Time _last_processed_detections_msg_timestamp;
    builtin_interfaces::msg::Time _last_processed_merged_items_msg_timestamp;

  };

} // namespace select_new_masks

#endif
