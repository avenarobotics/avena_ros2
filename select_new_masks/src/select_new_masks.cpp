#include "select_new_masks/select_new_masks.hpp"

namespace select_new_masks
{
    SelectNewMasks::SelectNewMasks(const rclcpp::NodeOptions &options)
        : Node("select_new_masks_node", options),
          masks_overlap_procentage(0.9)
    {
        status = custom_interfaces::msg::Heartbeat::STOPPED;
        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        helpers::commons::setLoggerLevelFromParameter(this);
        // initNode();
    }

    void SelectNewMasks::initNode()
    {
        status = custom_interfaces::msg::Heartbeat::STARTING;
        RCLCPP_INFO(this->get_logger(), "Starting module");
        _readLabels();
        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local();
        _merged_items_subscriber = create_subscription<custom_interfaces::msg::Items>("merged_items", qos_settings,
                                                                                      [this](custom_interfaces::msg::Items::SharedPtr items_msg)
                                                                                      {
                                                                                          RCLCPP_DEBUG(get_logger(), "Merged items message received");
                                                                                          _items_data = items_msg;
                                                                                      });
        _filtered_detections_subscriber = create_subscription<custom_interfaces::msg::Detections>("detections_filtered", qos_settings,
                                                                                                  [this](custom_interfaces::msg::Detections::SharedPtr detections_msg)
                                                                                                  {
                                                                                                      RCLCPP_DEBUG(get_logger(), "Filtered detections message received");
                                                                                                      _detections_data = detections_msg;
                                                                                                  });
        _new_masks_publisher = this->create_publisher<custom_interfaces::msg::Detections>("new_masks", qos_settings);
        _missing_items_publisher = this->create_publisher<custom_interfaces::msg::MissingItemsIds>("missing_items_ids", qos_settings);

        this->_action_server = rclcpp_action::create_server<SelectMasks>(
            this,
            "select_new_masks",
            std::bind(&SelectNewMasks::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&SelectNewMasks::_handleCancel, this, std::placeholders::_1),
            std::bind(&SelectNewMasks::_handleAccepted, this, std::placeholders::_1));
        
        RCLCPP_INFO(this->get_logger(), "Module started");
        status = custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void SelectNewMasks::shutDownNode()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;
        RCLCPP_INFO(this->get_logger(), "Gracefully stopping select_new_masks");
        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    int SelectNewMasks::_readLabels()
    {
        json labels_params = helpers::commons::getParameter("labels"); 
        for (auto label : labels_params)
        {
            json label_data = label;
            if (label_data.contains("label") && label_data.contains("item") && label_data.contains("element"))
            {
                bool item = label_data["item"].get<bool>();
                _items[label_data["label"]] = item;
                bool element = label_data["element"].get<bool>();
                _elements[label_data["label"]] = element;

                if (!element)
                {
                    std::vector<std::string> components = label_data["components"];
                    _components[label_data["label"]] = components;
                }
            }
        }
        return 0;
    }

    int SelectNewMasks::_unpackItems(std::vector<label_group> &label_groups)
    {
        auto extend_group = [](std::string label, std::vector<label_group> &label_groups)
        {
            label_group group(label);
            label_groups.push_back(group);
            return label_groups.end() - 1;
        };

        auto convert_Mask = [](ItemElement &element, items_mask_t &items_mask)
        {
            helpers::converters::stringToBinaryMask(element.cam1_mask, items_mask.mask_cam1);
            helpers::converters::stringToBinaryMask(element.cam2_mask, items_mask.mask_cam2);
        };

        for (auto item = _items_data->items.begin(); item != _items_data->items.end(); item++)
        {
            auto group_it = std::find_if(label_groups.begin(), label_groups.end(), [item](label_group group)
                                         { return (item->label == group.label); });
            if (group_it == label_groups.end())
                group_it = extend_group(item->label, label_groups);

            int group_index = std::distance(label_groups.begin(), group_it);

            if (!_elements[item->label])
            {
                items_mask_t wrapper;
                wrapper.item_id = item->id;
                for (auto &el : item->item_elements)
                {
                    items_mask_t element;
                    element.item_id = item->id;
                    convert_Mask(el, element);

                    auto el_group_it = std::find_if(label_groups.begin(), label_groups.end(), [el](label_group group)
                                                    { return (el.label == group.label); });
                    if (el_group_it == label_groups.end())
                        el_group_it = extend_group(el.label, label_groups);
                    el_group_it->items.push_back(element);

                    if (wrapper.mask_cam1.empty() && !element.mask_cam1.empty())
                        helpers::converters::stringToBinaryMask(item->item_elements[0].cam1_mask, wrapper.mask_cam1);
                    else if (!element.mask_cam1.empty())
                        cv::bitwise_or(wrapper.mask_cam1, element.mask_cam1, wrapper.mask_cam1);

                    if (wrapper.mask_cam2.empty() && !element.mask_cam2.empty())
                        helpers::converters::stringToBinaryMask(item->item_elements[0].cam2_mask, wrapper.mask_cam2);
                    else if (!element.mask_cam2.empty())
                        cv::bitwise_or(wrapper.mask_cam2, element.mask_cam2, wrapper.mask_cam2);
                }

                label_groups[group_index].items.push_back(wrapper);
            }
            else
            {
                items_mask_t unpacked;
                unpacked.item_id = item->id;
                convert_Mask(item->item_elements[0], unpacked);
                group_it->items.push_back(unpacked);
            }
        }

        return 0;
    }

    int SelectNewMasks::_unpackDetections(std::vector<label_group> &label_groups)
    {
        auto assign_masks = [](int cam_idx, std::vector<std::string> &masks, std::vector<std::string> &labels, std::vector<label_group> &label_groups) mutable
        {
            for (size_t i = 0; i < masks.size(); i++)
            {
                auto group_it = std::find_if(label_groups.begin(), label_groups.end(), [i, labels](label_group group)
                                             { return (labels[i] == group.label); });
                if (group_it == label_groups.end())
                {
                    label_group group(labels[i]);
                    label_groups.push_back(group);
                    group_it = label_groups.end() - 1;
                }

                cv::Mat mask;
                helpers::converters::stringToBinaryMask(masks[i], mask);
                if (cam_idx == 1)
                    group_it->mask_cam1.push_back(mask);
                else if (cam_idx == 2)
                    group_it->mask_cam2.push_back(mask);
            }
        };

        if (_detections_data->cam1_masks.size() != _detections_data->cam1_labels.size() ||
            _detections_data->cam2_masks.size() != _detections_data->cam2_labels.size())
        {
            RCLCPP_ERROR(this->get_logger(), "masks do not match labels for cam1");
            return 1;
        }

        assign_masks(1, _detections_data->cam1_masks, _detections_data->cam1_labels, label_groups);
        assign_masks(2, _detections_data->cam2_masks, _detections_data->cam2_labels, label_groups);

        return 0;
    }

    int SelectNewMasks::_compareInputs(custom_interfaces::msg::Detections::UniquePtr &out_detections, std::vector<uint32_t> &out_missing_items)
    {
        std::vector<label_group> label_groups;

        int error_code = _unpackItems(label_groups);

        error_code += _unpackDetections(label_groups);
        RCLCPP_INFO(this->get_logger(), "_unpackDetections");
        if (error_code > 0)
            return 1;

        std::set<uint32_t> missing_items;

        for (auto &group : label_groups)
        {
            for (auto &item : group.items)
            {

                bool cam1_matched = false;
                bool cam2_matched = false;
                std::vector<cv::Mat>::iterator mask_cam1;
                std::vector<cv::Mat>::iterator mask_cam2;
                for (mask_cam1 = group.mask_cam1.begin(); mask_cam1 != group.mask_cam1.end(); mask_cam1++)
                {
                    cam1_matched = _detectOverlap(*mask_cam1, item.mask_cam1);
                    if (cam1_matched)
                        break;
                }

                for (mask_cam2 = group.mask_cam2.begin(); mask_cam2 != group.mask_cam2.end(); mask_cam2++)
                {
                    cam2_matched = _detectOverlap(*mask_cam2, item.mask_cam2);
                    if (cam2_matched)
                        break;
                }

                if (cam1_matched && cam2_matched)
                {
                    group.mask_cam1.erase(mask_cam1);
                    group.mask_cam2.erase(mask_cam2);
                }
                else if (!cam1_matched && !cam2_matched)
                    missing_items.insert(item.item_id);
            }
        }

        for (auto &group : label_groups)
        {
            for (auto mask : group.mask_cam1)
            {
                std::string missing_mask_str;
                helpers::converters::binaryMaskToString(mask, missing_mask_str);
                out_detections->cam1_masks.push_back(missing_mask_str);
                out_detections->cam1_labels.push_back(group.label);
            }

            for (auto mask : group.mask_cam2)
            {
                std::string missing_mask_str;
                helpers::converters::binaryMaskToString(mask, missing_mask_str);
                out_detections->cam2_masks.push_back(missing_mask_str);
                out_detections->cam2_labels.push_back(group.label);
            }
        }

        for (auto item : missing_items)
        {
            out_missing_items.push_back(item);
        }

        return 0;
    }

    bool SelectNewMasks::_detectOverlap(cv::Mat &detection_mask, cv::Mat &item_mask)
    {
        if (detection_mask.empty() || item_mask.empty())
            return false;

        // cv::Mat merged = cv::Mat::zeros(cvSize(mask1.cols, mask1.rows) , CV_8UC1);
        cv::Mat merged, detection_mask_resized;

        cv::resize(detection_mask, detection_mask_resized, item_mask.size(), 0, 0, cv::INTER_NEAREST);
        cv::bitwise_and(detection_mask_resized, item_mask, merged);

        int size_merged = cv::countNonZero(merged);
        std::vector<int> sizes = {cv::countNonZero(detection_mask_resized), cv::countNonZero(item_mask)};
        int size_min = (sizes[0] < sizes[1]) ? sizes[0] : sizes[1];
        if (size_merged > (size_min * masks_overlap_procentage))
            return true;
        return false;
    }

    rclcpp_action::GoalResponse SelectNewMasks::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const SelectMasks::Goal> /*goal*/)
    {
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse SelectNewMasks::_handleCancel(const std::shared_ptr<GoalHandleSelectMasks> /*goal_handle*/)
    {
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void SelectNewMasks::_handleAccepted(const std::shared_ptr<GoalHandleSelectMasks> goal_handle)
    {
        std::thread{std::bind(&SelectNewMasks::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    void SelectNewMasks::_execute(const std::shared_ptr<GoalHandleSelectMasks> goal_handle)
    {
        helpers::Timer timer("Select new masks action", get_logger());

        RCLCPP_INFO(this->get_logger(), "Executing goal");
        auto result = std::make_shared<SelectMasks::Result>();

        auto abortReturn = [this, &goal_handle, &result]()
        {
            _new_masks_publisher->publish(custom_interfaces::msg::Detections());
            _missing_items_publisher->publish(custom_interfaces::msg::MissingItemsIds());
            goal_handle->abort(result);
        };

        if(status != custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_ERROR(this->get_logger(), "Node is not in running state");
            abortReturn();
            return;
        }

        // Validate input message
        if (!_detections_data || _detections_data->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_ERROR(this->get_logger(), "Invalid input message. Goal failed.");
            abortReturn();
            return;
        }

        if (_last_processed_detections_msg_timestamp == _detections_data->header.stamp)
            RCLCPP_WARN(get_logger(), "New message with detections has not arrived yet. Processing old message.");
        _last_processed_detections_msg_timestamp = _detections_data->header.stamp;

        if (!_items_data || _items_data->header.stamp == builtin_interfaces::msg::Time())
        {
            //this is 1st call - there is no data about old items - just rewrite.
            std_msgs::msg::Header header;
            header.stamp = now();
            custom_interfaces::msg::Detections::UniquePtr detections_data_unique(new custom_interfaces::msg::Detections);
            detections_data_unique->header = header;
            detections_data_unique->cam1_labels = _detections_data->cam1_labels;
            detections_data_unique->cam1_masks = _detections_data->cam1_masks;
            detections_data_unique->cam2_labels = _detections_data->cam2_labels;
            detections_data_unique->cam2_masks = _detections_data->cam2_masks;
            {
                helpers::Timer timer("SelectNewMasks::publish", get_logger());
                _new_masks_publisher->publish(std::move(detections_data_unique));
            }
            custom_interfaces::msg::MissingItemsIds::UniquePtr empty(new custom_interfaces::msg::MissingItemsIds);
            empty->header = header;
            _missing_items_publisher->publish(std::move(empty));
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded - no previous items recived - passing all masks");
            return;
        }

        if (_last_processed_merged_items_msg_timestamp == _items_data->header.stamp)
            RCLCPP_WARN(get_logger(), "New message with merged items has not arrived yet. Processing old message.");
        _last_processed_merged_items_msg_timestamp = _items_data->header.stamp;

        custom_interfaces::msg::Detections::UniquePtr new_masks(new custom_interfaces::msg::Detections);
        std::vector<uint32_t> missing_items;
        if (_compareInputs(new_masks, missing_items) == 1)
        {
            abortReturn();
            return;
        }

        if (rclcpp::ok())
        {
            std_msgs::msg::Header header;
            header.stamp = now();
            new_masks->header = header;
            custom_interfaces::msg::MissingItemsIds::UniquePtr missing_items_msg(new custom_interfaces::msg::MissingItemsIds);
            missing_items_msg->header = header;
            for (auto id : missing_items)
                missing_items_msg->items_ids.push_back(id);
            _new_masks_publisher->publish(std::move(new_masks));
            _missing_items_publisher->publish(std::move(missing_items_msg));
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
        }
    }

} // namespace select_new_masks

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(select_new_masks::SelectNewMasks)
