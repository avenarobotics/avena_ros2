#include "ptcld_transformer/ptcld_transformer.hpp"
namespace ptcld_transformer
{
    PtcldTransformer::PtcldTransformer(const rclcpp::NodeOptions &options)
        : Node("ptcld_transformer", options)
    {
        status = custom_interfaces::msg::Heartbeat::STOPPED;
        helpers::commons::setLoggerLevelFromParameter(this);
        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
    }

    void PtcldTransformer::initNode()
    {
        try
        {
            status = custom_interfaces::msg::Heartbeat::STARTING;
            RCLCPP_INFO(get_logger(), "Initialization of ptcld transformer.");
            rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
            _cameras_parameters = _getCameraTransform();
            _sub_cameras_data = create_subscription<custom_interfaces::msg::CamerasData>("cameras_data", qos_settings, std::bind(&PtcldTransformer::_camerasDataCallback, this, std::placeholders::_1));
            _pub_ptcld_transformer = create_publisher<custom_interfaces::msg::PtcldTransformer>("ptcld_transformer", qos_settings);
            _param_server_read = false;
            _param_server_thread = std::thread(std::bind(&PtcldTransformer::_getTableAreaFromParametersServer, this));
            // _table_area = _getTableAreaFromParametersServer();

            _timer = this->create_wall_timer(std::chrono::milliseconds(100), std::bind(&PtcldTransformer::joinThread, this));

            RCLCPP_INFO(get_logger(), "...done");
            status = custom_interfaces::msg::Heartbeat::RUNNING;
        }
        catch (std::exception &e)
        {
            RCLCPP_ERROR(get_logger(), e.what());
            status = custom_interfaces::msg::Heartbeat::STOPPED;
        }
    }

    void PtcldTransformer::shutDownNode()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;
        RCLCPP_INFO(this->get_logger(), "Gracefully stopping ptcld_transformer");
        _param_server_read = true;
        joinThread();
        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    PtcldTransformer::~PtcldTransformer()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;
        _param_server_read = true;
        joinThread();
        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void PtcldTransformer::joinThread()
    {
        if (_param_server_read && _param_server_thread.joinable())
        {
            _param_server_thread.join();
            _timer.reset();
        }
    }

    void PtcldTransformer::_camerasDataCallback(const custom_interfaces::msg::CamerasData::SharedPtr cameras_data_msg)
    {
        if (status == custom_interfaces::msg::Heartbeat::RUNNING)
        {
            helpers::Timer timer(__func__, get_logger());
            if (!_param_server_read)
                return;
            helpers::TicTok::GetInstance()->start();
            // helpers::Timer timer("PtcldTransformer::callback", get_logger());
            CamerasData::SharedPtr cameras_data = _readInputData(cameras_data_msg);
            TransformedPointClouds::SharedPtr transformed_point_clouds = _processCamerasData(cameras_data, _cameras_parameters);
            custom_interfaces::msg::PtcldTransformer::UniquePtr ptcld_transformer_data = _prepareOutputData(cameras_data_msg, transformed_point_clouds);
            {
                // helpers::Timer timer("PtcldTransformer::publish", get_logger());
                _pub_ptcld_transformer->publish(std::move(ptcld_transformer_data));
            }
            _watchdog->endAction();
        }
    }

    TransformedPointClouds::SharedPtr PtcldTransformer::_processCamerasData(CamerasData::SharedPtr cameras_data, CameraTransform::SharedPtr cameras_parameters)
    {
        // helpers::Timer timer(__func__, get_logger());
        TransformedPointClouds::SharedPtr transformed_point_clouds(new TransformedPointClouds);

        pcl::PointCloud<pcl::PointXYZ>::Ptr temp1(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr temp2(new pcl::PointCloud<pcl::PointXYZ>);

        std::thread t3(std::bind(&PtcldTransformer::_filterAndTransformPointCloud, this, cameras_data->cam1_ptcld, cameras_parameters->cam1_aff, temp1));
        std::thread t4(std::bind(&PtcldTransformer::_filterAndTransformPointCloud, this, cameras_data->cam2_ptcld, cameras_parameters->cam2_aff, temp2));
        t3.join();
        t4.join();

        // _filterAndTransformPointCloud(cameras_data->cam1_ptcld, cameras_parameters->cam1_aff, temp1);
        // _filterAndTransformPointCloud(cameras_data->cam2_ptcld, cameras_parameters->cam2_aff, temp2);

        // helpers::commons::voxelize(transformed_point_clouds->merged_ptcld, 0.005);
        // helpers::commons::statisticalOutlierRemovalFilter(transformed_point_clouds->merged_ptcld,100,1.8);

        *transformed_point_clouds->merged_ptcld += *temp1;
        *transformed_point_clouds->merged_ptcld += *temp2;
        return transformed_point_clouds;
    }

    void PtcldTransformer::_filterAndTransformPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr &in_cloud, const Eigen::Affine3f &camera_pose, pcl::PointCloud<pcl::PointXYZ>::Ptr &out_cloud)
    {
        // helpers::Timer timer(__func__, get_logger());
        // helpers::commons::passThroughFilter(in_cloud, "z", 0.1, 10);  // filtration in camera frame
        helpers::vision::voxelize(in_cloud, 0.005);

        pcl::transformPointCloud(*in_cloud, *out_cloud, camera_pose); // transform point cloud to world frame

        pcl::PointXYZ min;
        min.getVector3fMap() = Eigen::Vector3f(_table_area.x_min, _table_area.y_min, _table_area.z_min - 0.01);
        pcl::PointXYZ max;
        max.getVector3fMap() = Eigen::Vector3f(_table_area.x_max, _table_area.y_max, _table_area.z_max);
        pcl::CropBox<pcl::PointXYZ> crop_box_table_edge_ext;
        crop_box_table_edge_ext.setInputCloud(out_cloud);
        crop_box_table_edge_ext.setMin(min.getVector4fMap());
        crop_box_table_edge_ext.setMax(max.getVector4fMap());
        crop_box_table_edge_ext.filter(*out_cloud);
    }

    // ros2 run tf2_ros static_transform_publisher 0.6630728244781494, 1.101435661315918, 0.8472383618354797, 0.520514190196991, 0.7729775905609131, -0.3478240370750427, 0.10290282219648361 world camera_2/rgb_camera_link
    // ros2 run tf2_ros static_transform_publisher 0.6784907579421997, -1.0350241661071777, 0.9399753212928772, 0.7491486668586731, 0.5671202540397644, 0.091000996530056, -0.3299541175365448 world camera_1/rgb_camera_link

    // ros2 run tf2_ros static_transform_publisher 0.6630728244781494, 1.101435661315918, 0.8472383618354797, 0.10290282219648361, 0.520514190196991, 0.7729775905609131, -0.3478240370750427 world camera_2/rgb_camera_link
    // ros2 run tf2_ros static_transform_publisher 0.6784907579421997, -1.0350241661071777, 0.9399753212928772, -0.3299541175365448, 0.7491486668586731, 0.5671202540397644, 0.091000996530056 world camera_1/rgb_camera_link

    CameraTransform::SharedPtr PtcldTransformer::_getCameraTransform()
    {
        // helpers::Timer timer(__func__, get_logger());
        CameraTransform::SharedPtr cameras_parameters(new CameraTransform);
        auto get_camera_affine = [this](std::string camera_frame)
        {
            std::optional<Eigen::Affine3f> camera_affine_opt;
            camera_affine_opt = helpers::vision::getCameraTransformAffine("world", camera_frame);
            if (camera_affine_opt == std::nullopt)
            {
                RCLCPP_INFO(get_logger(), "Ptcld transformer - cannot obtain transform to \"" + camera_frame);
                throw(std::runtime_error(std::string("Ptcld transformer - cannot obtain transform to \"") + camera_frame));
            }
            else
            {
                Eigen::Affine3f cam_aff = *camera_affine_opt;
                return cam_aff;
            }
        };

        cameras_parameters->cam1_aff = get_camera_affine("camera_1");
        cameras_parameters->cam2_aff = get_camera_affine("camera_2");
        return cameras_parameters;
    }

    void PtcldTransformer::_getTableAreaFromParametersServer()
    {
        helpers::Timer timer(__func__, get_logger());

        // TableArea table_area;
        try
        {
            while (rclcpp::ok())
            {
                json areas_parameters = helpers::commons::getParameter("areas");
                // if (!rclcpp::ok())
                //     rclcpp::shutdown();
                if (!areas_parameters.empty())
                {
                    _table_area.x_min = areas_parameters["table_area"]["min"]["x"].get<float>();
                    _table_area.y_min = areas_parameters["table_area"]["min"]["y"].get<float>();
                    _table_area.z_min = areas_parameters["table_area"]["min"]["z"].get<float>();
                    _table_area.x_max = areas_parameters["table_area"]["max"]["x"].get<float>();
                    _table_area.y_max = areas_parameters["table_area"]["max"]["y"].get<float>();
                    _table_area.z_max = areas_parameters["table_area"]["max"]["z"].get<float>();
                    _param_server_read = true;
                    break;
                }

                RCLCPP_WARN_STREAM_THROTTLE(get_logger(), *get_clock(), 1000, "Ptcld transformer - cannot read parameter from server");
            }
        }
        catch (const json::exception &e)
        {
            std::string error_message = "aaaa Error occured while parsing parameter from the server. Message: " + std::string(e.what()) + ". Exiting...";
            RCLCPP_FATAL(get_logger(), error_message);
            rclcpp::shutdown();
            throw std::runtime_error(error_message);
        }
        // return table_area;
    }

    custom_interfaces::msg::PtcldTransformer::UniquePtr PtcldTransformer::_prepareOutputData(const custom_interfaces::msg::CamerasData::SharedPtr cameras_data_msg,
                                                                                             const TransformedPointClouds::SharedPtr transformed_point_clouds)
    {
        // helpers::Timer timer(__func__, get_logger());
        custom_interfaces::msg::PtcldTransformer::UniquePtr ptcld_transformer_data(new custom_interfaces::msg::PtcldTransformer);
        ptcld_transformer_data->cam1_rgb = cameras_data_msg->cam1_rgb;
        ptcld_transformer_data->cam2_rgb = cameras_data_msg->cam2_rgb;
        ptcld_transformer_data->cam1_depth = cameras_data_msg->cam1_depth;
        ptcld_transformer_data->cam2_depth = cameras_data_msg->cam2_depth;
        ptcld_transformer_data->cam1_ptcld = cameras_data_msg->cam1_ptcld;
        ptcld_transformer_data->cam2_ptcld = cameras_data_msg->cam2_ptcld;

        // helpers::converters::pclToRosPtcld<pcl::PointXYZ>(transformed_point_clouds->cam1_ptcld_trans, ptcld_transformer_data->cam1_ptcld_trans);
        // helpers::converters::pclToRosPtcld<pcl::PointXYZ>(transformed_point_clouds->cam2_ptcld_trans, ptcld_transformer_data->cam2_ptcld_trans);
        helpers::converters::pclToRosPtcld<pcl::PointXYZ>(transformed_point_clouds->merged_ptcld, ptcld_transformer_data->merged_ptcld);

        //  std::thread t1(std::bind(&PtcldTransformer::_convertCloudToRos, this,transformed_point_clouds->cam1_ptcld_trans, ptcld_transformer_data->cam1_ptcld_trans));
        //  std::thread t2(std::bind(&PtcldTransformer::_convertCloudToRos, this,transformed_point_clouds->cam2_ptcld_trans, ptcld_transformer_data->cam2_ptcld_trans));
        //  std::thread t3(std::bind(&PtcldTransformer::_convertCloudToRos, this,transformed_point_clouds->merged_ptcld, ptcld_transformer_data->merged_ptcld));

        // t1.join();
        // t2.join();
        // t3.join();

        auto set_ptcld_header = [this](std_msgs::msg::Header &header, const builtin_interfaces::msg::Time &stamp)
        {
            header.frame_id = "world";
            header.stamp = stamp;
        };
        // set_ptcld_header(ptcld_transformer_data->cam1_ptcld_trans.header, ptcld_transformer_data->cam1_ptcld.header.stamp);
        // set_ptcld_header(ptcld_transformer_data->cam2_ptcld_trans.header, ptcld_transformer_data->cam2_ptcld.header.stamp);

        // auto merged_time_msg = _getMergedPtcldTime(ptcld_transformer_data->cam1_ptcld.header.stamp, ptcld_transformer_data->cam2_ptcld.header.stamp);
        set_ptcld_header(ptcld_transformer_data->merged_ptcld.header, cameras_data_msg->cam2_depth.header.stamp);

        ptcld_transformer_data->header.stamp = cameras_data_msg->cam2_depth.header.stamp;
        ptcld_transformer_data->header.frame_id = "world";

        return ptcld_transformer_data;
    }

    CamerasData::SharedPtr PtcldTransformer::_readInputData(const custom_interfaces::msg::CamerasData::SharedPtr cameras_data_msg)
    {
        // helpers::Timer timer(__func__, get_logger());
        CamerasData::SharedPtr cameras_data(new CamerasData);

        std::thread t1(std::bind(&PtcldTransformer::_convertCloudToPCL, this, cameras_data_msg->cam1_ptcld, cameras_data->cam1_ptcld));
        std::thread t2(std::bind(&PtcldTransformer::_convertCloudToPCL, this, cameras_data_msg->cam2_ptcld, cameras_data->cam2_ptcld));

        t1.join();
        t2.join();

        // helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(cameras_data_msg->cam1_ptcld, cameras_data->cam1_ptcld);
        // helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(cameras_data_msg->cam2_ptcld, cameras_data->cam2_ptcld);

        return cameras_data;
    }

    void PtcldTransformer::_convertCloudToPCL(const sensor_msgs::msg::PointCloud2 &ros_cloud_msg, pcl::PointCloud<pcl::PointXYZ>::Ptr &out_pcl_cloud)
    {
        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(ros_cloud_msg, out_pcl_cloud);
    }

    void PtcldTransformer::_convertCloudToRos(const pcl::PointCloud<pcl::PointXYZ>::Ptr &pcl_cloud, sensor_msgs::msg::PointCloud2 &out_ros_cloud_msg)
    {
        // helpers::converters::pclToRosPtcld<pcl::PointXYZ>(transformed_point_clouds->cam1_ptcld_trans, ptcld_transformer_data->cam1_ptcld_trans);
        helpers::converters::pclToRosPtcld<pcl::PointXYZ>(pcl_cloud, out_ros_cloud_msg);
    }

    builtin_interfaces::msg::Time PtcldTransformer::_getMergedPtcldTime(const builtin_interfaces::msg::Time &cam1_stamp, const builtin_interfaces::msg::Time &cam2_stamp)
    {
        double cam1_time_sec = cam1_stamp.sec + cam1_stamp.nanosec / 1e9;
        double cam2_time_sec = cam2_stamp.sec + cam2_stamp.nanosec / 1e9;
        double merged_time_sec = 0;
        if (cam1_time_sec > cam2_time_sec)
            merged_time_sec = cam2_time_sec + (cam1_time_sec - cam2_time_sec) / 2.0;
        else
            merged_time_sec = cam1_time_sec + (cam2_time_sec - cam1_time_sec) / 2.0;
        builtin_interfaces::msg::Time merged_time_msg;
        merged_time_msg.sec = std::floor(merged_time_sec);
        merged_time_msg.nanosec = (merged_time_sec - merged_time_msg.sec) * 1e9;
        return merged_time_msg;
    }

} // namespace ptcld_transformer

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(ptcld_transformer::PtcldTransformer)
