#ifndef CLI_MANAGER_SERVER_HPP_
#define CLI_MANAGER_SERVER_HPP_
// __CPP__
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include "rclcpp_action/client_goal_handle.hpp"
// ___Avena___
#include "custom_interfaces/action/simple_action.hpp"
#include "custom_interfaces/action/manager_prepare_pick_and_place_data_action.hpp"
#include "custom_interfaces/action/item_select.hpp"
#include "custom_interfaces/action/grasp_action.hpp"
#include "custom_interfaces/action/place_action.hpp"
#include "custom_interfaces/action/generate_path_pick_action.hpp"
#include "custom_interfaces/action/generate_path_place_action.hpp"
#include "custom_interfaces/action/path_buffer.hpp"
#include "visibility_control.h"
#include "util.hpp"
#include "helpers_commons/helpers_commons.hpp"

namespace manager_prepare_pick_and_place_data
{
    using namespace std::chrono_literals;
    class ManagerPreparePickAndPlaceData : public rclcpp::Node, public helpers::WatchdogInterface
    {
    public:
        using ManagerPreparePickAndPlaceDataAction = custom_interfaces::action::ManagerPreparePickAndPlaceDataAction;
        using GoalHandleManagerPreparePickAndPlaceDataAction = rclcpp_action::ServerGoalHandle<ManagerPreparePickAndPlaceDataAction>;

        void initNode() override;
        void shutDownNode() override;

        CLI_PUBLIC
        explicit ManagerPreparePickAndPlaceData(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());
        // CLI_PUBLIC
        template <typename ActionT>
        int waitForServer(const typename rclcpp_action::Client<ActionT>::SharedPtr action_client)
        {
            size_t wait_time = 60;
            size_t seconds_waited = 0;
            while (seconds_waited < wait_time)
            {
                if (!action_client->wait_for_action_server(1s))
                {
                    RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available after waiting for " << ++seconds_waited << " seconds.");
                    if (!rclcpp::ok())
                    {
                        RCLCPP_ERROR(this->get_logger(), "Problem with ROS. Exiting...");
                        throw std::runtime_error("Problem with ROS. Exiting...");
                    }
                }
                else
                {
                    RCLCPP_INFO(this->get_logger(), "Action server avaiable.");
                    break;
                }
            }
            if (seconds_waited == wait_time)
            {
                RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available. Exiting...");
                return 1;
            }
            return 0;
        }

    private:
        helpers::Watchdog::SharedPtr _watchdog;
        CLI_LOCAL
        rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const ManagerPreparePickAndPlaceDataAction::Goal> /*goal*/);
        CLI_LOCAL
        rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle);
        CLI_LOCAL
        void _handleAccepted(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle);
        CLI_LOCAL
        void _execute(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle);
        CLI_LOCAL
        int _callGetOccupancyGrid();
        CLI_LOCAL
        int _callItemSelect(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle, std::vector<int32_t> &selected_item_ids);
        CLI_LOCAL
        int _callOctomapFilter();
        CLI_LOCAL
        int _callSpawnCollisionItems();
        CLI_LOCAL
        int _callGrasp(int32_t selected_item_id, std::vector<custom_interfaces::msg::GraspData> &grasp_poses);
        CLI_LOCAL
        int _callPlace(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle, int32_t selected_item_id, custom_interfaces::msg::GraspData grasp_pose, std::vector<custom_interfaces::msg::PlaceData> &place_poses);
        CLI_LOCAL
        int _callGeneratePathPick(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle, custom_interfaces::msg::GraspData grasp_pose, trajectory_msgs::msg::JointTrajectory &path_to_pick);
        CLI_LOCAL
        int _callGeneratePathPlace(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle, custom_interfaces::msg::PlaceData place_pose, custom_interfaces::msg::GraspData grasp_pose, int selected_item_id, trajectory_msgs::msg::JointTrajectory &pick_to_place);
        CLI_LOCAL
        int _callPathBufferSet(trajectory_msgs::msg::JointTrajectory path, std::string path_name);
        CLI_LOCAL
        int _callPathBufferGet(std::string path_name);
        CLI_LOCAL
        int _isCancelling(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle);

        rclcpp_action::Server<ManagerPreparePickAndPlaceDataAction>::SharedPtr _action_server;
        std::string _path_to_pick = "path_to_pick";
        std::string _pick_to_place = "pick_to_place";

        std::string _item_select_action_name = "item_select";
        std::string _octomap_filter_action_name = "octomap_filter";
        std::string _spawn_collision_items_action_name = "spawn_collision_items";
        std::string _grasp_action_name = "grasp";
        std::string _place_action_name = "place";
        std::string _generate_path_pick_action_name = "generate_path_pick";
        std::string _generate_path_place_action_name = "generate_path_place";
        std::string _get_occupancy_grid_action_name = "get_occupancy_grid";
        std::string _path_buffer_set_action_name = "path_buffer_set";
        std::string _path_buffer_get_action_name = "path_buffer_get";
    };
} // namespace cli_manager

#endif //CLI_MANAGER_SERVER_HPP_
