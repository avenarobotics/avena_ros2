#include "manager_prepare_pick_and_place_data_server.hpp"

namespace manager_prepare_pick_and_place_data
{
  ManagerPreparePickAndPlaceData::ManagerPreparePickAndPlaceData(const rclcpp::NodeOptions &options)
      : Node("cli_manager", options)

  {
    RCLCPP_INFO(get_logger(), "Initialization of cli manager");
    // rclcpp::QoS qos_latching = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
    _action_server = rclcpp_action::create_server<ManagerPreparePickAndPlaceDataAction>(
        this,
        "manager_prepare_pick_and_place_data",
        std::bind(&ManagerPreparePickAndPlaceData::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
        std::bind(&ManagerPreparePickAndPlaceData::_handleCancel, this, std::placeholders::_1),
        std::bind(&ManagerPreparePickAndPlaceData::_handleAccepted, this, std::placeholders::_1));

    _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
    RCLCPP_INFO(get_logger(), "...cli manager init done .");
    status = custom_interfaces::msg::Heartbeat::STOPPED;
  }

  void ManagerPreparePickAndPlaceData::initNode()
  {
    status = custom_interfaces::msg::Heartbeat::STARTING;

    status = custom_interfaces::msg::Heartbeat::RUNNING;
  }

  void ManagerPreparePickAndPlaceData::shutDownNode()
  {
    status = custom_interfaces::msg::Heartbeat::STOPPING;

    status = custom_interfaces::msg::Heartbeat::STOPPED;
  }

  rclcpp_action::GoalResponse ManagerPreparePickAndPlaceData::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const ManagerPreparePickAndPlaceDataAction::Goal> /*goal*/)
  {
    RCLCPP_INFO(get_logger(), "Received goal request with order ");
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
  }
  rclcpp_action::CancelResponse ManagerPreparePickAndPlaceData::_handleCancel(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle)
  {
    RCLCPP_INFO(get_logger(), "Received request to cancel goal");
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
  }
  void ManagerPreparePickAndPlaceData::_handleAccepted(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle)
  {
    using namespace std::placeholders;
    // this needs to return quickly to avoid blocking the executor, so spin up a new thread
    std::thread{std::bind(&ManagerPreparePickAndPlaceData::_execute, this, std::placeholders::_1), goal_handle}.detach();
  }
  int ManagerPreparePickAndPlaceData::_isCancelling(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle)
  {
    auto result = std::make_shared<ManagerPreparePickAndPlaceDataAction::Result>();
    if (goal_handle->is_canceling())
    {
      goal_handle->canceled(result);
      RCLCPP_ERROR(this->get_logger(), "Goal Canceled");
      return 1;
    }
    else
      return 0;
  }
  void ManagerPreparePickAndPlaceData::_execute(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle)
  {

    auto result = std::make_shared<ManagerPreparePickAndPlaceDataAction::Result>();
    if (status != custom_interfaces::msg::Heartbeat::RUNNING)
    {
      RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
      goal_handle->abort(result);
      return;
    }

    helpers::Timer czas("--------------------------------------------ManagerPreparePickAndPlaceData------------------------------------------------------", true);
    
    RCLCPP_INFO(get_logger(), "Executing goal");

    if (_isCancelling(goal_handle))
      return;
    std::vector<int32_t> selected_item_ids;
    if (_callItemSelect(goal_handle, selected_item_ids))
    {
      RCLCPP_ERROR(this->get_logger(), "Item select failed");
      return;
    }
    if (_isCancelling(goal_handle))
      return;
    auto selected_item_id = selected_item_ids.front();
    std::vector<custom_interfaces::msg::GraspData> grasp_poses;
    if (_callGrasp(selected_item_id, grasp_poses))
    {
      // RCLCPP_WARN(this->get_logger(), "Grasp failed for current item, taking next one from the list");
      RCLCPP_WARN(this->get_logger(), "Grasp failed for current item");
      // continue;
    }
    else
    {
      for (auto grasp_pose : grasp_poses)
      {
        if (_isCancelling(goal_handle))
          return;
        std::vector<custom_interfaces::msg::PlaceData> place_poses;
        if (_callPlace(goal_handle, selected_item_id, grasp_pose, place_poses))
        {
          RCLCPP_WARN(this->get_logger(), "Moving to next grasp pose");
          continue;
        }
        if (_isCancelling(goal_handle))
          return;
        trajectory_msgs::msg::JointTrajectory path_to_pick;
        if (_callGeneratePathPick(goal_handle, grasp_pose, path_to_pick))
        {
          RCLCPP_WARN(this->get_logger(), "Moving to next grasp pose");
          continue;
        }
        std::this_thread::sleep_for(0.5s);
        for (auto place_pose : place_poses)
        {
          if (_isCancelling(goal_handle))
            return;
          trajectory_msgs::msg::JointTrajectory pick_to_place;
          if (_callGeneratePathPlace(goal_handle, place_pose, grasp_pose, selected_item_id, pick_to_place))
          {
            RCLCPP_WARN(this->get_logger(), "Moving to next place pose");
            continue;
          }
          else
          {
            RCLCPP_INFO(get_logger(), "Succesfully generated valid Grasp pose, Path to pick and Path to place ");
            if (_callPathBufferSet(path_to_pick, _path_to_pick))
            {
              RCLCPP_WARN(this->get_logger(), "Cant save data in path buffer");
            }
            if (_callPathBufferSet(pick_to_place, _pick_to_place))
            {
              RCLCPP_WARN(this->get_logger(), "Cant save data in path buffer");
            }
            result->grasp_pose = grasp_pose;
            result->path_to_pick = path_to_pick;
            result->pick_to_place = pick_to_place;
            RCLCPP_INFO(get_logger(), "Data Succesfully saved");
            if (rclcpp::ok())
            {
              goal_handle->succeed(result);
              RCLCPP_INFO(get_logger(), "Goal succeeded.");
              // publish some data
              return;
            }
            else
            {
              RCLCPP_WARN(this->get_logger(), "Moving to next place pose");
              continue;
            }
          }
        }
      }
    }

    if (rclcpp::ok())
    {
      goal_handle->abort(result);
      RCLCPP_ERROR(get_logger(), "Goal failed.");
    }

    //   throw std::runtime_error("Problem with ROS");
  }
  int ManagerPreparePickAndPlaceData::_callItemSelect(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle, std::vector<int32_t> &selected_item_ids)
  {
    int exit_code = 0;
    auto goal = goal_handle->get_goal();
    // create item select action client
    auto item_select_action = rclcpp_action::create_client<custom_interfaces::action::ItemSelect>(this, _item_select_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::ItemSelect>(item_select_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _item_select_action_name << "\" action.");
    auto goal_msg = custom_interfaces::action::ItemSelect::Goal();
    //assign values
    std::vector<std::string> areas_vector;
    std::stringstream ss(goal->selected_area_label);
    if (goal->selected_area_label.find(","))
    {
      std::stringstream ss(goal->selected_area_label);
      while (ss.good())
      {
        std::string substr;
        getline(ss, substr, ',');
        areas_vector.push_back(substr);
      }
    }
    else
    {
      areas_vector.push_back(goal->selected_area_label);
    }
    goal_msg.selected_item_label = goal->selected_item_label;
    goal_msg.selected_area_label = areas_vector;
    goal_msg.selected_area_operator = goal->selected_area_operator;
    goal_msg.selected_policy = goal->selected_policy;
    goal_msg.distance_parameter_min = goal->distance_parameter_min;
    goal_msg.distance_parameter_max = goal->distance_parameter_max;
    std::cout << "Goal Request" << std::endl;
    std::cout << "selected_item_label: " << goal_msg.selected_item_label << std::endl;
    for (auto area : goal_msg.selected_area_label)
      std::cout << "selected_area_label: " << area << std::endl;
    std::cout << "selected_area_operator: " << goal_msg.selected_area_operator << std::endl;
    std::cout << "selected_policy: " << goal_msg.selected_policy << std::endl;
    std::cout << "distance_parameter_min: " << goal_msg.distance_parameter_min << std::endl;
    std::cout << "distance_parameter_max: " << goal_msg.distance_parameter_max << std::endl;
    //send goal
    auto goal_future = item_select_action->async_send_goal(goal_msg);
    //wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for item_select results...");
      return 1;
    }
    //read results
    auto result_future = item_select_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Item select result SUCCEEDED");
      selected_item_ids = result_future.get().result->selected_items_ids;
      // for (auto selected_item_id : selected_item_ids)
      //   std::cout << "selected_item_id: " << selected_item_id << std::endl;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Item select result ABORTED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Item select result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Item select result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
  int ManagerPreparePickAndPlaceData::_callGrasp(int32_t selected_item_id, std::vector<custom_interfaces::msg::GraspData> &grasp_poses)
  {
    int exit_code = 0;
    auto grasp_action = rclcpp_action::create_client<custom_interfaces::action::GraspAction>(this, _grasp_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::GraspAction>(grasp_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _grasp_action_name << "\" action.");
    auto goal_msg = custom_interfaces::action::GraspAction::Goal();
    // assign values
    goal_msg.selected_item_id = selected_item_id;
    // send goal
    auto goal_future = grasp_action->async_send_goal(goal_msg);
    // wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for grasp results...");
      return 1;
    }
    auto result_future = grasp_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Grasp result SUCCEEDED");
      grasp_poses = result_future.get().result->grasp_poses;
      std::cout << "grasp_poses.size()" << grasp_poses.size() << std::endl;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Grasp result ABORTED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Grasp result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Grasp result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
  int ManagerPreparePickAndPlaceData::_callOctomapFilter()
  {
    int exit_code = 0;
    auto octomap_filter_action = rclcpp_action::create_client<custom_interfaces::action::SimpleAction>(this, _octomap_filter_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::SimpleAction>(octomap_filter_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _octomap_filter_action_name << "\" action.");
    auto goal_msg = custom_interfaces::action::SimpleAction::Goal();
    // send empty goal
    auto goal_future = octomap_filter_action->async_send_goal(goal_msg);
    // wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for Octomap filter results...");
      return 1;
    }
    auto result_future = octomap_filter_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Octomap filter result SUCCEEDED");
      return 0;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Octomap filter result ABORTED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Octomap filter result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Octomap filter result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
  int ManagerPreparePickAndPlaceData::_callSpawnCollisionItems()
  {
    int exit_code = 0;
    auto spawn_collision_items_action = rclcpp_action::create_client<custom_interfaces::action::SimpleAction>(this, _spawn_collision_items_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::SimpleAction>(spawn_collision_items_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _spawn_collision_items_action_name << "\" action.");
    auto goal_msg = custom_interfaces::action::SimpleAction::Goal();
    // send empty goal
    auto goal_future = spawn_collision_items_action->async_send_goal(goal_msg);
    // wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for Spawn Collision Items results...");
      return 1;
    }
    auto result_future = spawn_collision_items_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Spawn Collision Items result SUCCEEDED");
      return 0;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Spawn Collision Items result ABORTED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Spawn Collision Items result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Spawn Collision Items result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
  int ManagerPreparePickAndPlaceData::_callPlace(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle, int32_t selected_item_id, custom_interfaces::msg::GraspData grasp_pose, std::vector<custom_interfaces::msg::PlaceData> &place_poses)
  {
    int exit_code = 0;
    auto goal = goal_handle->get_goal();
    // create item select action client
    auto place_action = rclcpp_action::create_client<custom_interfaces::action::PlaceAction>(this, _place_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::PlaceAction>(place_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _place_action_name << "\" action.");
    auto goal_msg = custom_interfaces::action::PlaceAction::Goal();
    //assign values
    std::vector<std::string> areas_vector;
    std::stringstream ss(goal->selected_area_label_place);
    if (goal->selected_area_label_place.find(","))
    {
      std::stringstream ss(goal->selected_area_label_place);
      while (ss.good())
      {
        std::string substr;
        getline(ss, substr, ',');
        areas_vector.push_back(substr);
      }
    }
    else
    {
      areas_vector.push_back(goal->selected_area_label_place);
    }
    goal_msg.selected_item_id = selected_item_id;
    goal_msg.grasp_pose = grasp_pose;
    goal_msg.selected_area_label = areas_vector;
    goal_msg.selected_area_operator = goal->selected_area_operator_place;
    goal_msg.search_shift = goal->search_shift;
    goal_msg.place_positions_required = goal->place_positions_required;
    std::cout << "Goal Request" << std::endl;
    for (auto area : goal_msg.selected_area_label)
      std::cout << "selected_area_label: " << area << std::endl;
    std::cout << "selected_area_operator: " << goal_msg.selected_area_operator << std::endl;
    std::cout << "search shift : " << goal_msg.search_shift << std::endl;
    std::cout << "place positions required : " << goal_msg.place_positions_required << std::endl;
    //send goal
    auto goal_future = place_action->async_send_goal(goal_msg);
    //wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for Place results...");
      return 1;
    }
    //read results
    auto result_future = place_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Place result SUCCEEDED");
      place_poses = result_future.get().result->place_poses;
      std::cout << "place_poses.size()" << place_poses.size() << std::endl;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Place result ABORTED");
      return 2;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Place result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Place result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
  int ManagerPreparePickAndPlaceData::_callGeneratePathPick(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle, custom_interfaces::msg::GraspData grasp_pose, trajectory_msgs::msg::JointTrajectory &path_to_pick)
  {
    int exit_code = 0;
    // auto goal = goal_handle->get_goal();
    // create item select action client
    RCLCPP_INFO_STREAM(this->get_logger(), "_callGeneratePathPick called...");
    auto generate_path_pick_action = rclcpp_action::create_client<custom_interfaces::action::GeneratePathPickAction>(this, _generate_path_pick_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::GeneratePathPickAction>(generate_path_pick_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _generate_path_pick_action_name << "\" action.");
    auto goal_msg = custom_interfaces::action::GeneratePathPickAction::Goal();
    //assign values
    goal_msg.grasp_pose = grasp_pose;
    goal_msg.constrain_value = goal_handle.get()->get_goal()->constrain_value;
    goal_msg.ik_trials_number = goal_handle.get()->get_goal()->ik_trials_number;
    goal_msg.max_final_states = goal_handle.get()->get_goal()->max_final_states;
    goal_msg.ompl_compare_trials = goal_handle.get()->get_goal()->ompl_compare_trials;
    goal_msg.min_path_points = goal_handle.get()->get_goal()->min_path_points;
    goal_msg.max_time = goal_handle.get()->get_goal()->max_time;
    goal_msg.max_simplification_time = goal_handle.get()->get_goal()->max_simplification_time;

    // send goal
    auto goal_future = generate_path_pick_action->async_send_goal(goal_msg);
    //wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for Generate Path Pick results...");
      return 1;
    }
    //read results
    auto result_future = generate_path_pick_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Generate Path Pick result SUCCEEDED");
      path_to_pick = result_future.get().result->path_to_pick;
      return 0;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path Pick result ABORTED");
      return 2;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path Pick result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path Pick result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
  int ManagerPreparePickAndPlaceData::_callGeneratePathPlace(const std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction> goal_handle, custom_interfaces::msg::PlaceData place_pose, custom_interfaces::msg::GraspData grasp_pose, int selected_item_id, trajectory_msgs::msg::JointTrajectory &pick_to_place)
  {
    int exit_code = 0;
    // create GeneratePathPlace client
    RCLCPP_INFO_STREAM(this->get_logger(), "_callGeneratePathPlace called...");
    auto generate_path_place_action = rclcpp_action::create_client<custom_interfaces::action::GeneratePathPlaceAction>(this, _generate_path_place_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::GeneratePathPlaceAction>(generate_path_place_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _generate_path_place_action_name << "\" action.");
    auto goal_msg = custom_interfaces::action::GeneratePathPlaceAction::Goal();
    //assign values
    goal_msg.selected_item_id = selected_item_id;
    std::vector<custom_interfaces::msg::PlaceData> place_poses;
    place_poses.push_back(place_pose);
    goal_msg.place_poses = place_poses;
    goal_msg.grasp_pose = grasp_pose;
    goal_msg.constrain_value = goal_handle.get()->get_goal()->constrain_value;
    goal_msg.ik_trials_number = goal_handle.get()->get_goal()->ik_trials_number;
    goal_msg.max_final_states = goal_handle.get()->get_goal()->max_final_states;
    goal_msg.ompl_compare_trials = goal_handle.get()->get_goal()->ompl_compare_trials;
    goal_msg.min_path_points = goal_handle.get()->get_goal()->min_path_points;
    goal_msg.max_time = goal_handle.get()->get_goal()->max_time;
    goal_msg.max_simplification_time = goal_handle.get()->get_goal()->max_simplification_time;
    // send goal
    auto goal_future = generate_path_place_action->async_send_goal(goal_msg);
    //wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for Generate Path place results...");
      return 1;
    }
    //read results
    auto result_future = generate_path_place_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Generate Path place result SUCCEEDED");
      pick_to_place = result_future.get().result->pick_to_place;
      return 0;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path place result ABORTED");
      return 2;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path place result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path place result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
  int ManagerPreparePickAndPlaceData::_callGetOccupancyGrid()
  {
    int exit_code = 0;
    // create Getocuupancygrid client
    RCLCPP_INFO_STREAM(this->get_logger(), "_callGetOccupancyGrid called...");
    auto get_occupancy_grid_action = rclcpp_action::create_client<custom_interfaces::action::SimpleAction>(this, _get_occupancy_grid_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::SimpleAction>(get_occupancy_grid_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _get_occupancy_grid_action_name << "\" action.");
    auto goal_msg = custom_interfaces::action::SimpleAction::Goal();
    //assign values
    // send goal
    auto goal_future = get_occupancy_grid_action->async_send_goal(goal_msg);
    //wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for Get occupancy grid results...");
      return 1;
    }
    //read results
    auto result_future = get_occupancy_grid_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Get occupancy grid  result SUCCEEDED");
      return 0;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Get occupancy grid  result ABORTED");
      return 2;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Get occupancy grid  result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Get occupancy grid  result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
  int ManagerPreparePickAndPlaceData::_callPathBufferSet(trajectory_msgs::msg::JointTrajectory path, std::string path_name)

  {
    int exit_code = 0;
    // create _callPathBufferPick action client
    RCLCPP_INFO_STREAM(this->get_logger(), "_callPathBuffer called...");
    auto path_buffer_pick_action = rclcpp_action::create_client<custom_interfaces::action::PathBuffer>(this, _path_buffer_set_action_name);
    RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
    cli::Timer timer("Action execution");
    if (waitForServer<custom_interfaces::action::PathBuffer>(path_buffer_pick_action))
      return 1;
    RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _path_buffer_set_action_name << "\" action.");
    RCLCPP_INFO_STREAM(this->get_logger(), "To save path with name \"" << path_name << "\" action.");
    auto goal_msg = custom_interfaces::action::PathBuffer::Goal();
    //assign values
    goal_msg.path_name = path_name;
    goal_msg.generated_path = path;
    // send goal
    auto goal_future = path_buffer_pick_action->async_send_goal(goal_msg);
    //wait for results
    if (goal_future.wait_for(10s) == std::future_status::timeout)
    {
      RCLCPP_ERROR(this->get_logger(), "Timeout while waiting for Path buffer results...");
      return 1;
    }
    //read results
    auto result_future = path_buffer_pick_action->async_get_result(goal_future.get());
    if (result_future.get().code == rclcpp_action::ResultCode::SUCCEEDED)
    {
      RCLCPP_INFO_STREAM(this->get_logger(), "Generate Path buffer result SUCCEEDED");
      return 0;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::ABORTED)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path buffer result ABORTED");
      return 2;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::CANCELED)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path buffer result CANCELED");
      return 1;
    }
    else if (result_future.get().code == rclcpp_action::ResultCode::UNKNOWN)
    {
      RCLCPP_ERROR(this->get_logger(), "Generate Path buffer result UNKNOWN");
      return 1;
    }
    return exit_code;
  }
} // namespace manager_prepare_pick_and_place_data
#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(manager_prepare_pick_and_place_data::ManagerPreparePickAndPlaceData)
