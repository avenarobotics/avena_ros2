#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include <rclcpp/rclcpp.hpp>
#include <rclcpp/time.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include "rclcpp/rclcpp.hpp"
#include <custom_interfaces/action/simple_action.hpp>
#include "rclcpp_components/register_node_macro.hpp"
#include "visibility_control.h"
#include "helpers_commons/helpers_commons.hpp"

using namespace std::chrono_literals;
namespace gripper_controller
{
  class GripperController : public rclcpp::Node, public helpers::WatchdogInterface
  {

  public:
    GRIPPER_CONTROLLER_PUBLIC
    explicit GripperController(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    using GripperOpen = custom_interfaces::action::SimpleAction;
    using GoalHandleGripperOpen = rclcpp_action::ServerGoalHandle<GripperOpen>;

    using GripperClose = custom_interfaces::action::SimpleAction;
    using GoalHandleGripperClose = rclcpp_action::ServerGoalHandle<GripperClose>;

    void initNode() override;
    void shutDownNode() override;


    GRIPPER_CONTROLLER_LOCAL
    void openGripper();
    GRIPPER_CONTROLLER_LOCAL
    bool closeGripper();
    GRIPPER_CONTROLLER_LOCAL
    void stopGripper();

  private:
    helpers::Watchdog::SharedPtr _watchdog;

    rclcpp_action::Server<GripperOpen>::SharedPtr _action_server_open;
    GRIPPER_CONTROLLER_LOCAL
    rclcpp_action::GoalResponse _handleGoalOpen(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const GripperOpen::Goal> goal);
    GRIPPER_CONTROLLER_LOCAL
    rclcpp_action::CancelResponse _handleCancelOpen(const std::shared_ptr<GoalHandleGripperOpen> goal_handle);
    GRIPPER_CONTROLLER_LOCAL
    void _handleAcceptedOpen(const std::shared_ptr<GoalHandleGripperOpen> goal_handle);
    GRIPPER_CONTROLLER_LOCAL
    void _executeOpen(const std::shared_ptr<GoalHandleGripperOpen> goal_handle);

    rclcpp_action::Server<GripperClose>::SharedPtr _action_server_close;
    GRIPPER_CONTROLLER_LOCAL
    rclcpp_action::GoalResponse _handleGoalClose(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const GripperClose::Goal> goal);
    GRIPPER_CONTROLLER_LOCAL
    rclcpp_action::CancelResponse _handleCancelClose(const std::shared_ptr<GoalHandleGripperClose> goal_handle);
    GRIPPER_CONTROLLER_LOCAL
    void _handleAcceptedClose(const std::shared_ptr<GoalHandleGripperClose> goal_handle);
    GRIPPER_CONTROLLER_LOCAL
    void _executeClose(const std::shared_ptr<GoalHandleGripperClose> goal_handle);

    rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr _sub_gripper_status;
    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_gripper_state;
    sensor_msgs::msg::JointState::SharedPtr _gripper_current_status;

  };
}
