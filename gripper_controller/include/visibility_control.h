#ifndef GRIPPER_CONTROLLER__VISIBILITY_CONTROL_H_
#define GRIPPER_CONTROLLER__VISIBILITY_CONTROL_H_

#ifdef __cplusplus
extern "C"
{
#endif

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define GRIPPER_CONTROLLER_EXPORT __attribute__ ((dllexport))
    #define GRIPPER_CONTROLLER_IMPORT __attribute__ ((dllimport))
  #else
    #define GRIPPER_CONTROLLER_EXPORT __declspec(dllexport)
    #define GRIPPER_CONTROLLER_IMPORT __declspec(dllimport)
  #endif
  #ifdef GRIPPER_CONTROLLER_BUILDING_DLL
    #define GRIPPER_CONTROLLER_PUBLIC GRIPPER_CONTROLLER_EXPORT
  #else
    #define GRIPPER_CONTROLLER_PUBLIC GRIPPER_CONTROLLER_IMPORT
  #endif
  #define GRIPPER_CONTROLLER_PUBLIC_TYPE GRIPPER_CONTROLLER_PUBLIC
  #define GRIPPER_CONTROLLER_LOCAL
#else
  #define GRIPPER_CONTROLLER_EXPORT __attribute__ ((visibility("default")))
  #define GRIPPER_CONTROLLER_IMPORT
  #if __GNUC__ >= 4
    #define GRIPPER_CONTROLLER_PUBLIC __attribute__ ((visibility("default")))
    #define GRIPPER_CONTROLLER_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define GRIPPER_CONTROLLER_PUBLIC
    #define GRIPPER_CONTROLLER_LOCAL
  #endif
  #define GRIPPER_CONTROLLER_PUBLIC_TYPE
#endif

#ifdef __cplusplus
}
#endif

#endif  // GRIPPER_CONTROLLER__VISIBILITY_CONTROL_H_
