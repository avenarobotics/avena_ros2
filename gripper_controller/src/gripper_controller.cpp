#include "gripper_controller.h"
namespace gripper_controller
{
    GripperController::GripperController(const rclcpp::NodeOptions &options) : Node("gripper_action_control", options)
    {
        RCLCPP_INFO(get_logger(), "Initialization of ros2 gripper controller.");
        rclcpp::QoS qos_latching = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();

        auto getGripperStatusCallback = [this](const sensor_msgs::msg::JointState::SharedPtr gripper_current_msg) -> void {
            if (gripper_current_msg->position.size() == 3)
            {
                _gripper_current_status = gripper_current_msg;
                // RCLCPP_INFO(this->get_logger(), "Recieved gripper values");
            }
        };

        _sub_gripper_status = this->create_subscription<sensor_msgs::msg::JointState>("gripper_joint_states", qos_latching, getGripperStatusCallback);

        _pub_gripper_state = this->create_publisher<sensor_msgs::msg::JointState>("gripper_control_status", qos_latching);

        _action_server_close = rclcpp_action::create_server<custom_interfaces::action::SimpleAction>(this, "gripper_close",
                                                                                                     std::bind(&GripperController::_handleGoalClose, this, std::placeholders::_1, std::placeholders::_2),
                                                                                                     std::bind(&GripperController::_handleCancelClose, this, std::placeholders::_1),
                                                                                                     std::bind(&GripperController::_handleAcceptedClose, this, std::placeholders::_1));

        _action_server_open = rclcpp_action::create_server<custom_interfaces::action::SimpleAction>(this, "gripper_open",
                                                                                                    std::bind(&GripperController::_handleGoalOpen, this, std::placeholders::_1, std::placeholders::_2),
                                                                                                    std::bind(&GripperController::_handleCancelOpen, this, std::placeholders::_1),
                                                                                                    std::bind(&GripperController::_handleAcceptedOpen, this, std::placeholders::_1));

        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");

        RCLCPP_INFO(get_logger(), "Gripper action controller loaded. Waiting for commands..");
        status=custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void GripperController::initNode()
    {
        status=custom_interfaces::msg::Heartbeat::STARTING;

        status=custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void GripperController::shutDownNode()
    {
        status=custom_interfaces::msg::Heartbeat::STOPPING;

        status=custom_interfaces::msg::Heartbeat::STOPPED;
    }

    rclcpp_action::GoalResponse GripperController::_handleGoalOpen(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const GripperOpen::Goal> goal)
    {
        (void)uuid;
        (void)goal;
        RCLCPP_INFO(this->get_logger(), "Goal succeeded");

        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse GripperController::_handleCancelOpen(const std::shared_ptr<GoalHandleGripperOpen> goal_handle)
    {
        (void)goal_handle;
        RCLCPP_INFO(this->get_logger(), "Goal canceled");

        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void GripperController::_handleAcceptedOpen(const std::shared_ptr<GoalHandleGripperOpen> goal_handle)
    {
        RCLCPP_INFO(this->get_logger(), "Goal accepted");
        std::thread{std::bind(&GripperController::_executeOpen, this, std::placeholders::_1), goal_handle}.detach();
    }

    void GripperController::_executeOpen(const std::shared_ptr<GoalHandleGripperOpen> goal_handle)
    {
        auto feedback = std::make_shared<GripperOpen::Feedback>();
        auto result = std::make_shared<GripperOpen::Result>();
        
        if(status!=custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
            goal_handle->abort(result);
            return;
        }
        
        const auto goal = goal_handle->get_goal();
        rclcpp::Rate loop_rate(1);

        openGripper();
        if (rclcpp::ok())
        {
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
        }

        RCLCPP_INFO(this->get_logger(), "finished");
    }
    rclcpp_action::GoalResponse GripperController::_handleGoalClose(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const GripperClose::Goal> goal)
    {
        (void)uuid;
        (void)goal;
        RCLCPP_INFO(this->get_logger(), "Goal succeeded");
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse GripperController::_handleCancelClose(const std::shared_ptr<GoalHandleGripperClose> goal_handle)
    {
        (void)goal_handle;
        RCLCPP_INFO(this->get_logger(), "Goal canceled");
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void GripperController::_handleAcceptedClose(const std::shared_ptr<GoalHandleGripperClose> goal_handle)
    {
        RCLCPP_INFO(this->get_logger(), "Goal accepted");
        std::thread{std::bind(&GripperController::_executeClose, this, std::placeholders::_1), goal_handle}.detach();
    }

    void GripperController::_executeClose(const std::shared_ptr<GoalHandleGripperClose> goal_handle)
    {
        auto feedback = std::make_shared<GripperClose::Feedback>();
        auto result = std::make_shared<GripperClose::Result>();

        if(status!=custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
            goal_handle->abort(result);
            return;
        }

        rclcpp::Rate loop_rate(1);
        const auto goal = goal_handle->get_goal();

        if (!closeGripper())
        {
            RCLCPP_ERROR(this->get_logger(), "Error occured while closing gripper. Goal failed.");
            goal_handle->abort(result);
            return;
        }

        if (rclcpp::ok())
        {
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
        }
        RCLCPP_INFO(this->get_logger(), "Closing finished");
    }

    bool GripperController::closeGripper()
    {
        bool is_close_finished = false;
        sensor_msgs::msg::JointState::SharedPtr gripper_set_state_msg(new sensor_msgs::msg::JointState);
        gripper_set_state_msg->name = {"Panda_gripper", "Panda_gripper_joint1", "Panda_gripper_joint2"};
        gripper_set_state_msg->position.resize(3);
        gripper_set_state_msg->effort.resize(3);
        int retry_attempt = 0;
        bool gripper_succed_grab = true;
        float gripper_close_velocity = -0.09;

        while (!is_close_finished)
        {
            gripper_set_state_msg->velocity.resize(3);
            gripper_set_state_msg->header.stamp = this->now();


            for (size_t i = 1; i < _gripper_current_status->position.size(); i++)
            {
                if (_gripper_current_status->effort[i] <= 0)
                    gripper_set_state_msg->velocity[i] = gripper_close_velocity;
                else
                    gripper_set_state_msg->velocity[i] = 0;
            }
            if (_gripper_current_status->effort[1] == 1 && _gripper_current_status->effort[2] == 1)
                is_close_finished = true;

            else if (_gripper_current_status->effort[1] == 0 && _gripper_current_status->effort[2] == 1)
            {

                if (retry_attempt < 3 && _gripper_current_status->velocity[1] > -0.03)
                {
                    RCLCPP_INFO(this->get_logger(), "Finger 1 velocity : %f ", _gripper_current_status->velocity[1]);

                    openGripper();
                    retry_attempt++;
                    gripper_close_velocity -= 0.02;
                    RCLCPP_INFO(this->get_logger(), "Gripper can't close property. Retry attempt %d from 3.", retry_attempt);
                }
                else if (retry_attempt < 3 && _gripper_current_status->position[1] < 0.0001)
                {
                    RCLCPP_INFO(this->get_logger(), "Finger 1 velocity : %f ", _gripper_current_status->velocity[1]);

                    openGripper();
                    retry_attempt++;
                    gripper_close_velocity -= 0.02;
                    RCLCPP_INFO(this->get_logger(), "Gripper can't close property. Retry attempt %d from 3.", retry_attempt);
                }
                else if (retry_attempt >= 3)
                {
                    RCLCPP_INFO(this->get_logger(), "Gripper close error. Can't grab successfully item.");
                    is_close_finished = true;
                    gripper_succed_grab = false;
                }
            }
            else if (_gripper_current_status->effort[1] == 1 && _gripper_current_status->effort[2] == 0)
            {
                if (retry_attempt < 3 && _gripper_current_status->velocity[2] > -0.03)
                {
                    RCLCPP_INFO(this->get_logger(), "Finger 2 velocity : %f ", _gripper_current_status->velocity[2]);
                    openGripper();
                    retry_attempt++;
                    gripper_close_velocity -= 0.02;
                    RCLCPP_INFO(this->get_logger(), "Gripper cant close property. Retry attempt %d from 3.", retry_attempt);
                }
                else if (retry_attempt < 3 && _gripper_current_status->position[2] < 0.0001)
                {
                    RCLCPP_INFO(this->get_logger(), "Finger 2 velocity : %f ", _gripper_current_status->velocity[2]);
                    openGripper();
                    retry_attempt++;
                    gripper_close_velocity -= 0.02;
                    RCLCPP_INFO(this->get_logger(), "Gripper cant close property. Retry attempt %d from 3.", retry_attempt);
                }
                else if (retry_attempt >= 3)
                {
                    RCLCPP_INFO(this->get_logger(), "Gripper close error. Can't grab successfully item.");
                    is_close_finished = true;
                    gripper_succed_grab = false;
                }
            }

            if (_gripper_current_status->position[1] < 0.0001 && _gripper_current_status->position[2] < 0.0001)
            {
                RCLCPP_INFO(this->get_logger(), "Gripper closed without any item between fingers.");
                is_close_finished = true;
                gripper_succed_grab = false;
            }

            if (is_close_finished)
                for (size_t i = 1; i < gripper_set_state_msg->velocity.size(); i++)
                    gripper_set_state_msg->velocity[i] = 0;

            _pub_gripper_state->publish(*gripper_set_state_msg);
        }
        return gripper_succed_grab;
    }

    void GripperController::openGripper()
    {
        bool is_open_finished = false;
        sensor_msgs::msg::JointState::SharedPtr gripper_set_state_msg(new sensor_msgs::msg::JointState);
        gripper_set_state_msg->name = {"Panda_gripper", "Panda_gripper_joint1", "Panda_gripper_joint2"};
        gripper_set_state_msg->position.resize(3);
        gripper_set_state_msg->effort.resize(3);

        while (!is_open_finished)
        {

            gripper_set_state_msg->velocity.resize(3);
            gripper_set_state_msg->header.stamp = this->now();

            for (size_t i = 1; i < _gripper_current_status->position.size(); i++)
            {
                if (_gripper_current_status->position[i] <= 0.039)
                    gripper_set_state_msg->velocity[i] = 0.09;
                else
                    gripper_set_state_msg->velocity[i] = 0;
            }
            if (_gripper_current_status->position[1] >= 0.039 && _gripper_current_status->position[2] >= 0.039)
            {
                for (size_t i = 1; i < gripper_set_state_msg->velocity.size(); i++)
                    gripper_set_state_msg->velocity[i] = 0;
                is_open_finished = true;
            }
            _pub_gripper_state->publish(*gripper_set_state_msg);
        }
    }

}

RCLCPP_COMPONENTS_REGISTER_NODE(gripper_controller::GripperController)
