cmake_minimum_required(VERSION 3.5)
project(gripper_controller)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()
set(CMAKE_BUILD_TYPE RELEASE)

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(sensor_msgs REQUIRED)
find_package(rclcpp_action REQUIRED)
find_package(custom_interfaces REQUIRED)
find_package(rclcpp_components REQUIRED)
find_package(helpers_commons REQUIRED)

include_directories(include)



add_library(gripper_controller SHARED src/gripper_controller.cpp)
target_compile_definitions(gripper_controller PRIVATE "GRIPPER_CONTROLLER_BUILDING_LIBRARY")
ament_target_dependencies(gripper_controller
  rclcpp
  sensor_msgs
  rclcpp_action
  custom_interfaces
  rclcpp_components
  helpers_commons
)
# rclcpp_components_register_nodes(gripper_controller "")
rclcpp_components_register_node(gripper_controller
  PLUGIN "gripper_controller::GripperController"
  EXECUTABLE gripper_controller_node)


install(TARGETS
  gripper_controller
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin
)

install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}
)


# install(TARGETS
#   gripper_controller
#   DESTINATION lib/${PROJECT_NAME})


ament_package()