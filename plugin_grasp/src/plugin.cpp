#include "plugin.h"

GraspPlugin::GraspPlugin()
    : _parameters_read(false)
{
    // RCLCPP_INFO(_node->get_logger(), "ctor");
}

GraspPlugin::~GraspPlugin(){};

void GraspPlugin::onStart()
{
    _grasp_offset = 0.01;
    if (!registerScriptStuff())
        throw std::runtime_error("script stuff initialization failed");

    setExtVersion("Plugin for spawning collision objects");
    setBuildDate(BUILD_DATE);

    _node = rclcpp::Node::make_shared("grasp_items_node");

    // _watchdog = std::make_shared<helpers::Watchdog>(_node.get(), "system_monitor");

    rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
    _publisher = _node->create_publisher<custom_interfaces::msg::Grasp>("grasp", qos_settings);

    qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local().reliable();
    _subscriptions_manager = std::make_shared<helpers::SubscriptionsManager>(_node);
    // _subscriptions_manager->createSubscription(TopicNames::selected_items_ids, TopicTypes::selected_items_ids);
    // _subscriptions_manager->createSubscription(TopicNames::merged_ptcld_filtered, TopicTypes::merged_ptcld_filtered);
    // _subscriptions_manager->createSubscription(TopicNames::items, TopicTypes::items);
    // _subscriptions_manager->createSubscription(Topics::selected_items_ids.name, Topics::selected_items_ids.type);

    _subscriptions_manager->createSubscription(Topics::merged_ptcld_filtered.name, Topics::merged_ptcld_filtered.type, qos_settings);
    _subscriptions_manager->createSubscription(Topics::items.name, Topics::items.type, qos_settings);

    _action_server = rclcpp_action::create_server<GraspItems>(
        _node,
        "grasp",
        std::bind(&GraspPlugin::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
        std::bind(&GraspPlugin::_handleCancel, this, std::placeholders::_1),
        std::bind(&GraspPlugin::_handleAccepted, this, std::placeholders::_1));

    _readParamServerData();
}

void GraspPlugin::onInstancePass(const sim::InstancePassFlags &flags, bool first)
{
    rclcpp::spin_some(_node);
}

rclcpp_action::GoalResponse GraspPlugin::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const GraspItems::Goal> goal)
{
    RCLCPP_INFO(_node->get_logger(), "handle goal");
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
}

rclcpp_action::CancelResponse GraspPlugin::_handleCancel(const std::shared_ptr<GoalHandleGraspItems> /*goal_handle*/)
{
    RCLCPP_INFO(_node->get_logger(), "Goal canceled");
    return rclcpp_action::CancelResponse::ACCEPT;
}

void GraspPlugin::_handleAccepted(const std::shared_ptr<GoalHandleGraspItems> goal_handle)
{
    RCLCPP_INFO(_node->get_logger(), "Goal accepted");
    _execute(goal_handle);
}

custom_interfaces::msg::Item GraspPlugin::_chooseSelectedItem(const custom_interfaces::msg::Items::SharedPtr items_msg, int id)
{
    custom_interfaces::msg::Item item;
    auto item_it = std::find_if(items_msg->items.begin(), items_msg->items.end(), [id](const custom_interfaces::msg::Item &item) { return item.id == id; });
    if (item_it == items_msg->items.end())
    {
        item.id = -1;
    }
    else
    {
        item = *item_it;
    }

    return item;
}

int GraspPlugin::_readParamServerData()
{
    RCLCPP_INFO_ONCE(_node->get_logger(), "Reading parameters from the server");

    if (_parameters_read)
        return 0;

    auto parameters = helpers::commons::getParameters({"labels", "gripper_parameters_franka", "robot"});
    if (parameters.empty())
        return 1;
    json labels = parameters["labels"];
    json gripper_property = parameters["gripper_parameters_franka"];
    json robot_property = parameters["robot"];

    try
    {
        for (auto label : labels)
        {
            if (label.contains("label") && label.contains("fit_method"))
            {
                _default_fit_methods[label["label"]] = label["fit_method"];
            }
        }
        RCLCPP_INFO(_node->get_logger(), "Labels data loaded successfully");

        _gripper_params.finger_depth = gripper_property["finger_depth"];
        _gripper_params.finger_length = gripper_property["finger_length"];
        _gripper_params.finger_width = gripper_property["finger_width"];
        _gripper_params.hand_depth = gripper_property["hand_depth"];
        _gripper_params.hand_length = gripper_property["hand_length"];
        _gripper_params.hand_width = gripper_property["hand_width"];
        _gripper_params.max_grasp_width = gripper_property["max_grasp_width"];

        const std::string working_side = robot_property["working_side"];
        _robot_info = helpers::commons::getRobotInfo(working_side);
        if (working_side == "right")
            _collision_robot_info = helpers::commons::getRobotInfo("left");
        else
            _collision_robot_info = helpers::commons::getRobotInfo("right");

    }
    catch (const json::exception &e)
    {
        RCLCPP_ERROR(_node->get_logger(), "Error occured while parsing JSON string.");
        return 1;
    }

    RCLCPP_INFO(_node->get_logger(), "Gripper data loaded successfully");
    _parameters_read = true;
    return 0;
}

void GraspPlugin::_loadSceneData()
{
    _ik_handle = simGetIkGroupHandle((_robot_info.robot_prefix + "_ik").c_str());
    _target = simGetObjectHandle((_robot_info.robot_prefix + "_target").c_str());
    _world = simGetObjectHandle("DummyWorld");

    const std::string robot_collection_name = _robot_info.robot_prefix + "_collection";
    int robot_handle = simGetCollectionHandle(robot_collection_name.c_str());
    const std::string collision_robot_collection_name = _collision_robot_info.robot_prefix + "_collection";
    int collision_robot_handle = simGetCollectionHandle(collision_robot_collection_name.c_str());
    int scene_collection_handle = simGetCollectionHandle("scene_collection");

    _panda_collision.clear();
    _panda_collision.push_back(robot_handle);
    _panda_collision.push_back(scene_collection_handle);
    _panda_collision.push_back(robot_handle);
    _panda_collision.push_back(collision_robot_handle);
    
    _joints.clear();
    for (auto joint_name : _robot_info.joint_names)
    {
        int joint = simGetObjectHandle(joint_name.c_str());
        _joints.push_back(joint);
    }

    _gripper_joints.push_back(simGetObjectHandle((_robot_info.robot_prefix + "_gripper_joint_1").c_str()));
    _gripper_joints.push_back(simGetObjectHandle((_robot_info.robot_prefix + "_gripper_joint_2").c_str()));
    _joint_positions.resize(_joints.size());



}

void GraspPlugin::_openGripper()
{
    for (auto handle : _gripper_joints)
        simSetJointPosition(handle, 0.04);
}

void GraspPlugin::_closeGripper()
{
    for (auto handle : _gripper_joints)
        simSetJointPosition(handle, 0); // it doesnt work....
}

void GraspPlugin::onSceneLoaded()
{
    _loadSceneData();
    _prepareScene();
}

void GraspPlugin::_prepareScene()
{
    int collision_parent = simGetObjectHandle("collisions_parent");
    int child_idx = 0;
    int child_handle;
    do
    {
        child_handle = simGetObjectChild(collision_parent, child_idx);
        if (child_handle != -1)
            simSetObjectSpecialProperty(child_handle, sim_objectspecialproperty_collidable);
        child_idx++;
    } while (child_handle != -1);
}

std::vector<GraspPair> GraspPlugin::_validateGrasp(std::vector<Eigen::Affine3f> &grasp_poses)
{

    std::vector<GraspPair> grasp_pairs;

    // std::cout << grasp_poses.size() << std::endl;
    // helpers::visualization::visualize({},grasp_poses);

    for (auto grasp : grasp_poses)
    {
        if (_validatePose(grasp))
        {
            Eigen::Affine3f pregrasp = _computePregrasp(grasp);
            if (_validatePose(pregrasp))
            {
                GraspPair grasp_pair;
                grasp_pair.first = grasp;
                grasp_pair.second = pregrasp;
                grasp_pairs.push_back(grasp_pair);
                //TODO for now return just one pair.
                return grasp_pairs;
            }
        }
    }

    return grasp_pairs;
}

bool GraspPlugin::_validatePose(Eigen::Affine3f &pose)
{
    std::vector<size_t> grasps;

    Eigen::Vector3f vec = pose.translation();
    std::vector<float> position = {vec.x(), vec.y(), vec.z()};
    Eigen::Quaternionf quat(pose.rotation());
    std::vector<float> orientation = {quat.w(), quat.x(), quat.y(), quat.z()};

    simSetObjectPosition(_target, _world, position.data());
    simSetObjectQuaternion(_target + sim_handleflag_wxyzquaternion, _world, orientation.data());

    int result = simGetConfigForTipPose(_ik_handle, _joints.size(), _joints.data(), 0.2, 55,
                                        _joint_positions.data(), nullptr, _panda_collision.size() / 2, _panda_collision.data(),
                                        nullptr, nullptr, nullptr, nullptr);

    // int res = simCheckIkGroup(_ik_handle, _joints.size(), _joints.data(), _joint_positions.data(), nullptr);
    // _openGripper();
    // if (res == 1)
    // {
    //     for (int i = 0; i < 7; i++)
    //         simSetJointPosition(_joints[i], _joint_positions[i]);

    //     auto result = simGenerateIkPath(_ik_handle, _joints.size(), _joints.data(), 20, 1, _panda_collision.data(), nullptr, nullptr);

    if (result != -1)
    {
        // _closeGripper();
        return true;
    }
    // }

    return false;
}

Eigen::Affine3f GraspPlugin::_computePregrasp(Eigen::Affine3f &grasp_pose)
{
    float pregrasp_distance = 0.1;
    Eigen::Vector3f pregrasp_position = grasp_pose.translation() - (grasp_pose.rotation().col(0) * pregrasp_distance);
    Eigen::Affine3f pregrasp_pose = Eigen::Translation3f(pregrasp_position) * Eigen::Quaternionf(grasp_pose.rotation());
    return pregrasp_pose;
}

int GraspPlugin::_validateInputs(const custom_interfaces::msg::MergedPtcldFiltered::SharedPtr &merged_ptcld_filtered,
                                 const custom_interfaces::msg::Items::SharedPtr &items,
                                 const int &selected_item_id)
{
    if (!_parameters_read)
    {
        RCLCPP_WARN(_node->get_logger(), "Gripper parameters and labels data are not loaded yet");
        return 1;
    }

    if (!merged_ptcld_filtered || merged_ptcld_filtered->header.stamp == builtin_interfaces::msg::Time())
    {
        RCLCPP_WARN(_node->get_logger(), "Invalid header in merged ptcld filtered message.");
        return 1;
    }

    if (!items || items->header.stamp == builtin_interfaces::msg::Time())
    {
        RCLCPP_WARN(_node->get_logger(), "Invalid header in estimated items message.");
        return 1;
    }
    // std::cout << "selected_item_id validate" << selected_item_id << std::endl;
    if (selected_item_id <= 0) /*|| selected_items_id->header.stamp == builtin_interfaces::msg::Time())*/
    {
        RCLCPP_WARN(_node->get_logger(), "Selected item ID cannot be negative.");
        return 1;
    }

    return 0;
}

void GraspPlugin::_execute(const std::shared_ptr<GoalHandleGraspItems> goal_handle)
{
    auto result = std::make_shared<custom_interfaces::action::GraspAction_Result>();
    if (_readParamServerData())
    {
        RCLCPP_ERROR(_node->get_logger(), "Not able to read parameteres from the server");
        goal_handle->abort(result);
        return;
    }

    auto merged_ptcld_filtered_msg = _subscriptions_manager->getData<custom_interfaces::msg::MergedPtcldFiltered>(Topics::merged_ptcld_filtered.name);
    auto estimate_shape_items_msg = _subscriptions_manager->getData<custom_interfaces::msg::Items>(Topics::items.name);
    int selected_item_id = static_cast<int>(goal_handle.get()->get_goal()->selected_item_id);
    if (_validateInputs(merged_ptcld_filtered_msg, estimate_shape_items_msg, selected_item_id))
    {
        RCLCPP_ERROR(_node->get_logger(), "Invalid input data. Goal aborted...");
        _publisher->publish(custom_interfaces::msg::Grasp());
        goal_handle->abort(result);
        return;
    }
    custom_interfaces::msg::Item item = _chooseSelectedItem(estimate_shape_items_msg, selected_item_id);
    if (item.id == -1)
    {
        RCLCPP_INFO(_node->get_logger(), "Invalid index of selected item: " + std::to_string(selected_item_id));
        _publisher->publish(custom_interfaces::msg::Grasp());
        goal_handle->abort(result);
        return;
    }

    auto grasp_method = FactoryGraspMethod::createGraspMethod(_default_fit_methods[item.label]);
    if (grasp_method == nullptr)
    {
        RCLCPP_INFO(_node->get_logger(), "Invalid grasp method, for item with id: " + std::to_string(selected_item_id));
        _publisher->publish(custom_interfaces::msg::Grasp());
        goal_handle->abort(result);
        return;
    }

    std::pair<pcl::PointXYZ, pcl::PointXYZ> grip_area = grasp_method->loadGripperProperties(_gripper_params);

    pcl::PointCloud<pcl::PointXYZ>::Ptr scene_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(merged_ptcld_filtered_msg->merged_ptcld_filtered, scene_cloud);
    grasp_method->setScenePtcld(scene_cloud);
    std::vector<Eigen::Affine3f> grasp_poses;
    int grasp_generate_resault = grasp_method->genereateGrasp(item, grasp_poses);

    if (grasp_poses.size() == 0)
    {
        RCLCPP_WARN(_node->get_logger(), "Cant find any valid grasp, after initial PCL filtration, for item with id: " + std::to_string(selected_item_id));
        _publisher->publish(custom_interfaces::msg::Grasp());
        goal_handle->abort(result);
        return;
    }
    if (grasp_generate_resault == 1)
    {
        RCLCPP_WARN(_node->get_logger(), "Cant find any valid grasp, for item with id: " + std::to_string(selected_item_id));
        _publisher->publish(custom_interfaces::msg::Grasp());
        goal_handle->abort(result);
        return;
    }

    std::vector<GraspPair> choosen_grasps_pairs = _validateGrasp(grasp_poses);

    if (choosen_grasps_pairs.size() == 0)
    {
        RCLCPP_WARN(_node->get_logger(), "Cant find any valid grasp with accesable pregrasp position, after IK validation, for item with id: " + std::to_string(selected_item_id));
        RCLCPP_WARN(_node->get_logger(), "Object might be out of reach or something is coliding with a robot.");
        _publisher->publish(custom_interfaces::msg::Grasp());
        goal_handle->abort(result);
        return;
    }

    RCLCPP_INFO(_node->get_logger(), "main logic finished, rewriting data.");
    custom_interfaces::msg::Grasp grasp_msg;
    grasp_msg.selected_item_id = selected_item_id;
    pcl::PointCloud<pcl::PointXYZ>::Ptr item_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    for (size_t idx = 0; idx < item.item_elements.size(); idx++)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr element_cloud(new pcl::PointCloud<pcl::PointXYZ>);
        helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item.item_elements[idx].merged_ptcld, element_cloud);
        *item_cloud += *element_cloud;
    }
    for (auto grasp_pair : choosen_grasps_pairs)
    {
        custom_interfaces::msg::GraspData grasp;
        helpers::converters::eigenAffineToGeometry(grasp_pair.first, grasp.grasp_pose);
        helpers::converters::eigenAffineToGeometry(grasp_pair.second, grasp.pregrasp_pose);
        grasp.item_width = _computeItemWidth(item_cloud, grasp_pair.first, grip_area);
        grasp.grasp_width = grasp.item_width + _grasp_offset;
        grasp.grasp_width = (grasp.grasp_width < 0.08) ? grasp.grasp_width : 0.08;
        grasp.squeeze_width = grasp.item_width * 0.9;
        grasp_msg.grasp_poses.push_back(grasp);
    }

    // for (auto handle : _gripper_joints)
    //     simSetJointPosition(handle, 0);

    grasp_msg.header.stamp = _node->now();
    _publisher->publish(grasp_msg);
    result->grasp_poses = grasp_msg.grasp_poses;
    RCLCPP_INFO(_node->get_logger(), "GraspPlugin finished");
    if (rclcpp::ok())
    {
        goal_handle->succeed(result);
        RCLCPP_INFO(_node->get_logger(), "Goal succeeded");
        return;
    }
    if (!rclcpp::ok())
    {
        _publisher->publish(custom_interfaces::msg::Grasp());
        goal_handle->abort(result);
        RCLCPP_WARN(_node->get_logger(), "Cant find any valid grasp for any of selected items");
        return;
    }
}

float GraspPlugin::_computeItemWidth(pcl::PointCloud<pcl::PointXYZ>::Ptr item, Eigen::Affine3f &grasp_pose, std::pair<pcl::PointXYZ, pcl::PointXYZ> &grip_area)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud<pcl::PointXYZ>(*item, *transformed_cloud, grasp_pose.inverse().matrix());

    pcl::CropBox<pcl::PointXYZ> crop_box;
    crop_box.setInputCloud(transformed_cloud);
    crop_box.setMin(grip_area.first.getVector4fMap());
    crop_box.setMax(grip_area.second.getVector4fMap());
    crop_box.filter(*transformed_cloud);

    float min = transformed_cloud->getMatrixXfMap().row(1).minCoeff();
    float max = transformed_cloud->getMatrixXfMap().row(1).maxCoeff();
    float grip_width = max - min;
    return grip_width;
}

SIM_PLUGIN("GraspPlugin", 1, GraspPlugin)
#include "stubsPlusPlus.cpp"
