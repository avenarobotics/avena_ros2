#include "grasp_methods/grasp_onion.hpp"

Item GraspOnion::_readItemData(custom_interfaces::msg::Item &item)
{
    Item onion_handle_data;

    // for (auto el : item.item_elements)
    // {
    //     json primitive_shape = json::parse(el.primitive_shape);
    //     if (primitive_shape.contains("fit_method") && primitive_shape["fit_method"] == "sphere")
    //     {   
    //         json pos = json::parse(el.position);
    //         Eigen::Vector3f position = helpers::commons::assignPositionFromJson(pos);
    //         json orient = json::parse(el.orientation);
    //         Eigen::Quaternionf rotation = helpers::commons::assignQuaternionFromJson(orient);
    //         onion_handle_data.obj_pose = Eigen::Translation3f(position) * rotation;
    //         onion_handle_data.obj_dim.resize(1);
    //         onion_handle_data.obj_dim[0] = primitive_shape["dims"]["radius"];
    //     }
    // }

    return onion_handle_data;
}

int GraspOnion::genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses)
{   
    Item item_data = _readItemData(item);

    if (item_data.obj_dim.size() != 1)
        return 1;


    GraspSphere grasp_sphere;
    grasp_sphere.loadGripperProperties(_gripper_params);
    grasp_sphere.setScenePtcld(_cloud);
    grasp_sphere.genereateGrasp(item_data, grasp_poses);


    return 0;
}
