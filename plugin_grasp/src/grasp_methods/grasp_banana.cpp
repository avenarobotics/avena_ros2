#include "grasp_methods/grasp_banana.hpp"

bool GraspBanana::_readItemData(custom_interfaces::msg::Item &item, Item &banana_handle_data)
{
    // int32_t segments_count = 0;
    // json primitive_shape = json::parse(item.item_elements[0].primitive_shape);
    // std::string segment_prefix = "segment";
    // while (primitive_shape.contains(segment_prefix + std::to_string(segments_count)))
    //     segments_count++;
    // segments_count--;

    // if (segments_count == -1)
    // {
    //     RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no segments in banana estimation");
    //     return false;
    // }

    // uint32_t idx = std::ceil(segments_count / 2.0);

    // json position_json = json::parse(item.item_elements[0].position);
    // std::string segment_name = segment_prefix + std::to_string(idx);
    // json pos = position_json[segment_name];
    // if (!pos.contains("x") || !pos.contains("y") || !pos.contains("z"))
    // {
    //     RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about position");
    //     return false;
    // }
    // Eigen::Vector3f position = helpers::commons::assignPositionFromJson(pos);

    // json orient_json = json::parse(item.item_elements[0].orientation);
    // json orient = orient_json[segment_name];
    // if (!orient.contains("x") || !orient.contains("y") || !orient.contains("z") || !orient.contains("w"))
    // {
    //     RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about orientation");
    //     return false;
    // }

    // Eigen::Quaternionf rotation = helpers::commons::assignQuaternionFromJson(orient);

    // banana_handle_data.obj_pose = Eigen::Translation3f(position) * rotation;
    // banana_handle_data.obj_dim.resize(2);

    // banana_handle_data.obj_dim[0] = primitive_shape[segment_name]["dims"]["height"];
    // banana_handle_data.obj_dim[1] = primitive_shape[segment_name]["dims"]["radius"];

    // if (banana_handle_data.obj_dim[1] < 0.0001)
    // {
    //     RCLCPP_WARN(rclcpp::get_logger("grasp"), "radius of middle segment is zero.");
    //     return false;
    // }

    return true;
}

int GraspBanana::genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses)
{
    Item item_data;
    if (!_readItemData(item, item_data))
        return 1;

    if (item_data.obj_dim.size() != 2)
        return 1;
    GraspCylinder grasp_cylinder;
    grasp_cylinder.loadGripperProperties(_gripper_params);
    grasp_cylinder.setScenePtcld(_cloud);
    grasp_cylinder.genereateGrasp(item_data, grasp_poses);

    return 0;
}
