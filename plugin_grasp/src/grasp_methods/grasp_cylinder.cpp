#include "grasp_methods/grasp_cylinder.hpp"

// GraspCylinder::GraspCylinder()
// : BaseGraspMethod()
// {
//     // loadGripperProperties();

// };

bool GraspCylinder::_readItemData(custom_interfaces::msg::Item &item, Item &item_data)
{
    if (item.item_elements[0].parts_description.size() > 0)
    {
        json primitive_shape = json::parse(item.item_elements[0].parts_description[0]);

        if (!primitive_shape.contains("dims") || !primitive_shape["dims"].contains("height") || !primitive_shape["dims"].contains("radius"))
        {
            RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object dimensions");
            return false;
        }
        item_data.obj_dim.resize(2);
        item_data.obj_dim[0] = primitive_shape["dims"]["height"];
        item_data.obj_dim[1] = primitive_shape["dims"]["radius"];
        if (item_data.obj_dim[1] < 0.0001)
        {
            RCLCPP_WARN(rclcpp::get_logger("grasp"), "radius of a cylinder is zero.");
            return false;
        }
    }
    else
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object dimensions");
        return false;
    }

    if (item.pose == geometry_msgs::msg::Pose())
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - pose data was not probably filled corretly because item is in the world origin");
        return false;
    }

    if (helpers::converters::geometryToEigenAffine(item.pose, item_data.obj_pose))
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "can't generate grasps - there is no data about object pose");
        return false;
    }

    return true;
}

int GraspCylinder::genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses)
{
    Item item_data;
    if (!_readItemData(item, item_data))
        return 1;

    if (item_data.obj_dim[1] > _gripper_params.finger_length)
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "cylinder diameter to big to grasp.");
        return 1;
    }

    if (item_data.obj_dim[1] < 0.0001)
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "radius of a cylinder is zero.");
        return 1;
    }
    else if (item_data.obj_dim.size() == 0)
        return 1;

    grasp_poses = _findCylinderGraspPose(item_data);
    _filterGrasps(grasp_poses);
    _sortGrasps(grasp_poses, item_data);
    return 0;
}

int GraspCylinder::genereateGrasp(Item item_data, std::vector<Eigen::Affine3f> &grasp_poses)
{

    if (item_data.obj_dim[1] > _gripper_params.finger_length)
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "cylinder diameter to big to grasp.");
        return 1;
    }
    if (item_data.obj_dim[1] < 0.0001)
    {
        RCLCPP_WARN(rclcpp::get_logger("grasp"), "radius of a cylinder is zero.");
        return 1;
    }
    else if (item_data.obj_dim.size() == 0)
        return 1;

    grasp_poses = _findCylinderGraspPose(item_data);

    _filterGrasps(grasp_poses);
    _sortGrasps(grasp_poses, item_data);
    return 0;
}

Eigen::Quaternionf GraspCylinder::_adjustCylinderRotation(Item &item_data)
{

    Eigen::Vector3f z_axis = item_data.obj_pose.rotation().col(2);
    Eigen::Vector3f z_axis_projected = z_axis;
    z_axis_projected(2) = 0;
    z_axis_projected.normalized();
    Eigen::Vector3f x_axis = helpers::vision::assignRotationMatrixAroundZ(M_PI_2) * z_axis_projected;
    Eigen::Matrix3f final_rot;
    final_rot.col(0) = x_axis.normalized();
    final_rot.col(1) = z_axis.cross(x_axis);
    final_rot.col(2) = z_axis;
    Eigen::Quaternionf final_rot_quat(final_rot);
    if (final_rot.col(1)(2) > 0)
        final_rot_quat = final_rot_quat * helpers::vision::assignRotationMatrixAroundZ(M_PI);

    return final_rot_quat;
}

std::vector<Eigen::Affine3f> GraspCylinder::_findCylinderGraspPose(Item item_data)
{
    float z_step = 0.01;
    std::vector<Eigen::Affine3f> grasp_poses;

    Eigen::Quaternionf orient = _adjustCylinderRotation(item_data);

    Eigen::Vector3f pos = item_data.obj_pose.translation();
    orient *= Eigen::Quaternionf(helpers::vision::assignRotationMatrixAroundZ(M_PI_2));
    Eigen::Affine3f initial_grasp = Eigen::Translation3f(pos) * orient;
    float full_hand_length = (_gripper_params.hand_length + _gripper_params.finger_length);

    for (int i = 0; i < (item_data.obj_dim[0] / 2) / z_step; i++)
    {
        for (int k = -1; k <= 1; k += 2)
        {
            Eigen::Vector3f new_grasp_position = pos + initial_grasp.rotation().col(2) * z_step * i * k;
            new_grasp_position -= initial_grasp.rotation().col(0) * full_hand_length;
            Eigen::Affine3f new_grasp = Eigen::Translation3f(new_grasp_position) * orient;
            grasp_poses.push_back(new_grasp);
        }
    }

    int grasps_size = grasp_poses.size();
    for (int i = 0; i < grasps_size; i++)
    {
        Eigen::Affine3f new_grasp;
        Eigen::Quaternionf new_orient(grasp_poses[i].rotation());
        new_orient = new_orient * helpers::vision::assignRotationMatrixAroundX(M_PI);
        new_grasp = Eigen::Translation3f(grasp_poses[i].translation()) * new_orient;
        grasp_poses.push_back(new_grasp);
    }

    grasps_size = grasp_poses.size();
    for (int i = 0; i < grasps_size; i++)
    {
        Eigen::Vector3f initial_position = grasp_poses[i].translation();
        for (int j = 1; j <= 6; j++)
        {
            for (int k = -1; k <= 1; k += 2)
            {
                float grasp_offset = (grasp_poses[i].translation() - initial_position).norm();
                float angle = k * (M_PI / 36) * j;
                Eigen::Quaternionf rotation_y(helpers::vision::assignRotationMatrixAroundY(angle));
                grasp_offset = cos(abs(angle)) * grasp_offset;
                Eigen::Quaternionf new_grasp_rotation = Eigen::Quaternionf(grasp_poses[i].rotation()) * rotation_y;
                Eigen::Vector3f new_grasp_position = initial_position - (new_grasp_rotation.toRotationMatrix().col(0) * grasp_offset);
                Eigen::Affine3f new_grasp = Eigen::Translation3f(new_grasp_position) * new_grasp_rotation;
                grasp_poses.push_back(new_grasp);
            }
        }
    }
    _addTopGrasps(item_data, grasp_poses);
    // helpers::vision::visualize({},{grasp_poses});
    return grasp_poses;
}

int GraspCylinder::_addTopGrasps(Item &item_data, std::vector<Eigen::Affine3f> &grasp_poses)
{

    //angle to rotate around tips of cylinder - here 180/36 = 5degrees
    float angle_step = M_PI / 36;

    std::vector<Eigen::Affine3f> new_grasps_poses(2);
    Eigen::Quaternionf bottom_rotation(item_data.obj_pose.rotation());
    bottom_rotation = bottom_rotation * helpers::vision::assignRotationMatrixAroundY(M_PI_2);
    new_grasps_poses[0] = Eigen::Translation3f(item_data.obj_pose.translation()) * bottom_rotation;

    Eigen::Translation3f top_grasp_position(item_data.obj_pose.translation());
    Eigen::Quaternionf top_grasp_rotation;
    top_grasp_rotation = bottom_rotation * helpers::vision::assignRotationMatrixAroundZ(M_PI);
    new_grasps_poses[1] = top_grasp_position * top_grasp_rotation;
    float full_hand_length = (_gripper_params.hand_length + _gripper_params.finger_length);

    for (auto &grasp : new_grasps_poses)
    {
        // Eigen::Vector3f grasp_position = grasp.translation();
        Eigen::Quaternionf new_grasp_rotation(grasp.rotation());
        Eigen::Vector3f grasp_position = grasp.translation() - (new_grasp_rotation.toRotationMatrix().col(0) * (item_data.obj_dim[0] / 2 - _gripper_params.finger_length + full_hand_length));
        grasp = Eigen::Translation3f(grasp_position) * new_grasp_rotation;
    }

    for (auto &grasp : new_grasps_poses)
    {
        Eigen::Translation3f grasp_position(grasp.translation());
        for (int s = 0; s < (M_PI * 2 / angle_step); s++)
        {
            Eigen::Quaternionf grasp_rot(grasp.rotation());
            grasp_rot = grasp_rot * helpers::vision::assignRotationMatrixAroundX(angle_step * s);
            Eigen::Affine3f new_grasp = grasp_position * grasp_rot;
            grasp_poses.push_back(new_grasp);
        }
    }

    return 0;
}
