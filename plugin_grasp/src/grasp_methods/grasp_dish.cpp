#include "grasp_methods/grasp_dish.hpp"

bool GraspDish::_printWarn(std::string warn_msg)
{
    RCLCPP_WARN(rclcpp::get_logger("grasp"), warn_msg);
    return false;
}

bool GraspDish::_readItemData(custom_interfaces::msg::Item &item, Item &item_data)
{
    if (item.item_elements.size() != 1)
        return _printWarn("item has diferent ammount of elements than expected.");

    if (item.item_elements[0].parts_description.size() != 1)
        return _printWarn("item has diferent ammount of parts in element than expected.");

    json primitive_shape = json::parse(item.item_elements[0].parts_description[0]);

    auto check_float_value = [primitive_shape](std::string field1, std::string field2, std::string field3) {
        if (!primitive_shape.contains(field1) ||
            !primitive_shape[field1].contains(field2) ||
            !primitive_shape[field1][field2].contains(field3) ||
            !primitive_shape[field1][field2][field3].is_number_float())
            return true;
        return false;
    };

    if (check_float_value("ramp", "dims", "angle"))
        return _printWarn("can't generate grasps - there is no information about wall angle");

    if (check_float_value("smaller_circle", "dims", "radius"))
        return _printWarn("can't generate grasps - there is no information about bottom of a dish (smaller circle)");

    if (check_float_value("bigger_circle", "dims", "radius"))
        return _printWarn("can't generate grasps - there is no information about top of a dish (bigger circle)");

    if (check_float_value("ramp", "dims", "height"))
        return _printWarn("can't generate grasps - there is no information about dish height)");

    item_data.obj_dim.resize(3);

    float height = primitive_shape["ramp"]["dims"]["height"];
    float big_radius = primitive_shape["bigger_circle"]["dims"]["radius"];
    item_data.obj_dim[0] = primitive_shape["smaller_circle"]["dims"]["radius"];
    item_data.obj_dim[1] = primitive_shape["ramp"]["dims"]["angle"];
    item_data.obj_dim[2] = std::sqrt(static_cast<float>(std::pow((big_radius - item_data.obj_dim[0]), 2) + std::pow(height, 2)));

    Eigen::Affine3f item_pose;
    helpers::converters::geometryToEigenAffine(item.pose, item_pose);
    Eigen::Vector3f item_position(item_pose.translation());
    Eigen::Quaternionf item_rotation(item_pose.rotation());

    // To generate grasps, pose of bottom of dish is used
    item_rotation.normalize();
    Eigen::Vector3f offset = item_rotation.toRotationMatrix().col(2) * height / 2.0;
    Eigen::Vector3f offset_item_position = item_position - offset;
    item_data.obj_pose = Eigen::Translation3f(offset_item_position) * item_rotation;

    return true;
}

int GraspDish::genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses)
{
    Item item_data;
    if (!_readItemData(item, item_data))
        return 1;

    Eigen::Affine3f initial_grasp;
    _computeInitialGrasp(item_data, initial_grasp);

    _computeGrasps(item_data, initial_grasp, grasp_poses);
    _filterGrasps(grasp_poses);

    return 0;
}

bool GraspDish::_computeGrasps(Item &item_data, Eigen::Affine3f &initial_grasp, std::vector<Eigen::Affine3f> &out_grasps)
{
    //Grasp every 3degrees (PI/60)
    size_t grasps_ammount = (M_PI * 2) / (M_PI / 60);
    out_grasps.resize(grasps_ammount * 2);
    Eigen::Quaternionf rotation_x(helpers::vision::assignRotationMatrixAroundX(M_PI));

    for (size_t i = 0; i < grasps_ammount; i++)
    {
        Eigen::Quaternionf rotation(helpers::vision::assignRotationMatrixAroundZ((M_PI / 60) * i));
        Eigen::Affine3f grasp_obj_frame = rotation * initial_grasp;
        out_grasps[i] = item_data.obj_pose * grasp_obj_frame;
        out_grasps[i + grasps_ammount] = item_data.obj_pose * (grasp_obj_frame * rotation_x);
    }
    return true;
}

bool GraspDish::_computeInitialGrasp(Item &item_data, Eigen::Affine3f &initial_grasp)
{

    Eigen::Quaternionf grasp_rot = helpers::vision::computeQuaternionFromAngle(M_PI_2 - item_data.obj_dim[1], Eigen::Vector3f::UnitY());
    grasp_rot = grasp_rot * Eigen::Quaternionf(helpers::vision::assignRotationMatrixAroundX(M_PI_2));

    float z = std::sin(M_PI_2 - item_data.obj_dim[1]) * (item_data.obj_dim[2] + _gripper_params.hand_length + _gripper_params.finger_length - _grasp_offset);
    float x = std::cos(M_PI_2 - item_data.obj_dim[1]) * (item_data.obj_dim[2] + _gripper_params.hand_length + _gripper_params.finger_length - _grasp_offset);
    x += item_data.obj_dim[0];
    Eigen::Translation3f grasp_trans(-x, 0.0, z);
    initial_grasp = grasp_trans * grasp_rot;

    return true;
}