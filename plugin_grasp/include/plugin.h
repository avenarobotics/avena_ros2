#ifndef GRASP_PLUGIN_HPP
#define GRASP_PLUGIN_HPP

// ___Coppelia___
#include "config.h"
#include "simPlusPlus/Plugin.h"
#include "stubs.h"

// ___CPP___
#include <memory>
#include <cmath>
#include <iostream>
#include <chrono>
#include <fstream>

// ___ROS___
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>

// ___Packages___
#include "custom_interfaces/action/simple_action.hpp"
#include "custom_interfaces/msg/selected_items_ids.hpp"
#include "custom_interfaces/msg/merged_ptcld_filtered.hpp"
#include "custom_interfaces/msg/items.hpp"
#include "custom_interfaces/msg/grasp.hpp"
#include "custom_interfaces/action/grasp_action.hpp"

#include <helpers_commons/helpers_commons.hpp>
#include <helpers_vision/helpers_vision.hpp>

#include "grasp_methods/factory_grasp_methods.hpp"

#define PLUGIN_NAME "AvenaGrasp"
#define PLUGIN_VERSION 1

using GraspPair = std::pair<Eigen::Affine3f, Eigen::Affine3f>;

struct TopicInfo
{
    std::string name;
    std::string type;
};

struct Topics
{
    // inline const static TopicInfo selected_items_ids{"item_select", "custom_interfaces/SelectedItemsIds"};
    inline const static TopicInfo items{"merged_items", "custom_interfaces/Items"};
    inline const static TopicInfo merged_ptcld_filtered{"merged_ptcld_filtered", "custom_interfaces/MergedPtcldFiltered"};
};

class GraspPlugin : public sim::Plugin
{
public:
    using GraspItems = custom_interfaces::action::GraspAction;
    using GoalHandleGraspItems = rclcpp_action::ServerGoalHandle<GraspItems>;

    GraspPlugin();
    virtual ~GraspPlugin();

    void onStart() override;
    void onInstancePass(const sim::InstancePassFlags &flags, bool first) override;
    void onSceneLoaded();

private:
    helpers::Watchdog::SharedPtr _watchdog;

    custom_interfaces::msg::Item _chooseSelectedItem(const custom_interfaces::msg::Items::SharedPtr items_items, int id);
    int _readParamServerData();

    //TODO change it to std::optional
    std::vector<GraspPair> _validateGrasp(std::vector<Eigen::Affine3f> &grasp_poses);

    bool _validatePose(Eigen::Affine3f &pose);
    int _validateInputs(const custom_interfaces::msg::MergedPtcldFiltered::SharedPtr &merged_ptcld_filtered,
                        const custom_interfaces::msg::Items::SharedPtr &items,
                        const int &selected_item_id);

    void _prepareScene();
    void _loadSceneData();
    Eigen::Affine3f _computePregrasp(Eigen::Affine3f &grasp_pose);

    std::map<std::string, std::string> _default_fit_methods;

    //ROS
    rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const GraspItems::Goal> goal);
    rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandleGraspItems> goal_handle);
    void _handleAccepted(const std::shared_ptr<GoalHandleGraspItems> goal_handle);
    void _execute(const std::shared_ptr<GoalHandleGraspItems> goal_handle);
    float _computeItemWidth(pcl::PointCloud<pcl::PointXYZ>::Ptr item, Eigen::Affine3f &grasp_pose, std::pair<pcl::PointXYZ, pcl::PointXYZ> &grip_area);
    void _openGripper();
    void _closeGripper();


    rclcpp_action::Server<GraspItems>::SharedPtr _action_server;
    rclcpp::Publisher<custom_interfaces::msg::Grasp>::SharedPtr _publisher;
    rclcpp::Node::SharedPtr _node;
    helpers::SubscriptionsManager::SharedPtr _subscriptions_manager;

    helpers::commons::RobotInfo _robot_info;
    helpers::commons::RobotInfo _collision_robot_info;

    //scene data
    int _ik_handle;
    std::vector<int> _joints;
    std::vector<int> _gripper_joints;
    int _target;
    int _world;
    float _grasp_offset;
    std::vector<int> _panda_collision;
    std::vector<float> _joint_positions;

    GripperParams _gripper_params;

    bool _parameters_read;
};

#endif // SIM_PLUGIN_SKELETON_NG_H_INCLUDED
