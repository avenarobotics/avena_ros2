#ifndef GRASP_CYLINDER_HPP
#define GRASP_CYLINDER_HPP

#include "grasp_methods/base_grasp_method.hpp"

class GraspCylinder : public BaseGraspMethod
{

public:
    // GraspCylinder();
    // virtual ~GraspCylinder() {};
    virtual int genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses) override;
    int genereateGrasp(Item item_data, std::vector<Eigen::Affine3f> &grasp_poses);

private:
    std::vector<Eigen::Affine3f> _findCylinderGraspPose(Item item_data);
    bool _readItemData(custom_interfaces::msg::Item &item, Item &item_data);
    int _addTopGrasps(Item &item_data, std::vector<Eigen::Affine3f> &grasp_poses);
    Eigen::Quaternionf _adjustCylinderRotation(Item &item_data);
};

#endif
