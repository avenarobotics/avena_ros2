#ifndef FACTORY_GRASP_METHODS_HPP
#define FACTORY_GRASP_METHODS_HPP

#include <memory>
#include "grasp_methods/base_grasp_method.hpp"
#include "grasp_methods/grasp_box.hpp"
#include "grasp_methods/grasp_sphere.hpp"
#include "grasp_methods/grasp_cylinder.hpp"
#include "grasp_methods/grasp_broccoli.hpp"
#include "grasp_methods/grasp_onion.hpp"
#include "grasp_methods/grasp_banana.hpp"
#include "grasp_methods/grasp_dish.hpp"

class FactoryGraspMethod
{
public:
    FactoryGraspMethod() = delete;
    virtual ~FactoryGraspMethod() = delete;
    static std::shared_ptr<BaseGraspMethod> createGraspMethod(std::string fit_method)
    {
        if (fit_method == "box")
            return std::make_shared<GraspBox>();
        else if (fit_method == "sphere")
            return std::make_shared<GraspSphere>();
        else if (fit_method == "cylinder")
            return std::make_shared<GraspCylinder>();
        else if (fit_method == "broccoli")
            return std::make_shared<GraspBroccoli>();
        else if (fit_method == "octosphere")
            return std::make_shared<GraspOnion>();
        else if (fit_method == "cylindersegments")
            return std::make_shared<GraspBanana>();
        else if (fit_method == "bowl" || fit_method == "plate")
            return std::make_shared<GraspDish>();
        else
            return nullptr;
    }
};

#endif