#ifndef GRASP_BROCCOLI_HPP
#define GRASP_BROCCOLI_HPP

#include "grasp_methods/base_grasp_method.hpp"
#include "grasp_methods/grasp_cylinder.hpp"

class GraspBroccoli : public BaseGraspMethod
{

public:
    // GraspSphere();
    // virtual ~GraspSphere();
    virtual int genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses) override;

private:
    Item _readItemData(custom_interfaces::msg::Item &item);
};

#endif
