#ifndef BASE_GRASP_METHOD_HPP
#define BASE_GRASP_METHOD_HPP

#include "simPlusPlus/Plugin.h"

#include <map>
#include <memory>

//PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/crop_box.h>


#include <eigen3/Eigen/Eigen>


#include <nlohmann/json.hpp>
#include <helpers_commons/helpers_commons.hpp>
#include <helpers_vision/helpers_vision.hpp>

//ROS
#include <custom_interfaces/msg/item.hpp>

using json = nlohmann::json;

struct Item
{
    Eigen::Affine3f obj_pose;
    std::vector<float> obj_dim;
    
};

struct GripperParams
{
  float max_grasp_width;
  float finger_width;
  float finger_length;
  float finger_depth;
  float hand_width;
  float hand_length;
  float hand_depth;
};

class BaseGraspMethod
{
public:
    // BaseGraspMethod();
    int setScenePtcld(pcl::PointCloud<pcl::PointXYZ>::Ptr scene_cloud);
    std::pair<pcl::PointXYZ, pcl::PointXYZ> loadGripperProperties(GripperParams &gripper_params);

    // virtual ~BaseGraspMethod(){};
    virtual int genereateGrasp(custom_interfaces::msg::Item &item, std::vector<Eigen::Affine3f> &grasp_poses) = 0;

protected:

int _filterGrasps(std::vector<Eigen::Affine3f> &grasp_candidates);
int _sortGrasps(std::vector<Eigen::Affine3f> &grasp_candidates, Item &item_data);


    // float _finger_length;
    // float _hand_width;
    // float _hand_depth ;
    // float _hand_length;

    // float _hand_height;
    // float _gripper_width_offset;
    // float _finger_width;
    GripperParams _gripper_params;

    std::pair<pcl::PointXYZ, pcl::PointXYZ> _hand_boundry;
    std::pair<pcl::PointXYZ, pcl::PointXYZ> _grip_area;
    pcl::PointCloud<pcl::PointXYZ>::Ptr _cloud;


private:
bool _isGraspValid(Eigen::Affine3f grasp_pose);


};

#endif