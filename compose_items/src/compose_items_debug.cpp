#include "compose_items_debug.hpp"

#include <iostream>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include <rclcpp/parameter.hpp>

namespace ros2mysql
{

  ComposeItems::ComposeItems(const rclcpp::NodeOptions &options)
      : Node("compose_items_debug", options)
  {
    // Declare parameters.
    _initializeParameters();
    _configure();

    _db = std::make_unique<MysqlConnector>(_host, _port, _db_name, _username, _password, _debug);
    auto callback = [this](const typename custom_interfaces::msg::Items::SharedPtr msg) -> void {
      RCLCPP_INFO(this->get_logger(), "Compose items debug received data.");
      _db->clearTable("item");
      _db->clearTable("item_element");

      // Validate input message
      if (msg->header.stamp == builtin_interfaces::msg::Time())
        return;

      for (size_t i = 0; i < msg->items.size(); i++)
      {
        item_t item;
        item.label = msg->items[i].label;
        item.item_id_hash = item.label + std::to_string(msg->items[i].id);
        _db->setItem(&item);
        uint32_t idx = _getItemsIdx();

        for (auto &ele : msg->items[i].item_elements)
        {
          item_element_t element;
          element.item_id = idx;
          element.element_label = ele.label;

          pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
          helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(ele.merged_ptcld, cloud);
          helpers::converters::pointcloudToStream<pcl::PointXYZ>(cloud, element.pcl_merged);
          cloud->clear();

          helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(ele.cam1_ptcld, cloud);
          helpers::converters::pointcloudToStream<pcl::PointXYZ>(cloud, element.element_pcl_1);
          cloud->clear();

          helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(ele.cam2_ptcld, cloud);
          helpers::converters::pointcloudToStream<pcl::PointXYZ>(cloud, element.element_pcl_2);
          cloud->clear();

          std::shared_ptr<std::stringstream> ss_mask1(new std::stringstream);
          *ss_mask1 << ele.cam1_mask;
          element.element_mask_1 = ss_mask1;

          std::shared_ptr<std::stringstream> ss_mask2(new std::stringstream);
          *ss_mask2 << ele.cam2_mask;
          element.element_mask_2 = ss_mask2;

          cv::Mat cam1_depth;
          cv::Mat cam2_depth;
          if (ele.cam1_depth.encoding != "")
            helpers::converters::rosImageToCV(ele.cam1_depth, cam1_depth);

          if (ele.cam2_depth.encoding != "")
            helpers::converters::rosImageToCV(ele.cam2_depth, cam2_depth);
          helpers::converters::imageToStream(cam1_depth, element.element_depth_1);
          helpers::converters::imageToStream(cam2_depth, element.element_depth_2);
          _db->setItemElement(&element);
        }
      }
      RCLCPP_INFO(this->get_logger(), "Compose items debug saved data.");
    };

    rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));//.transient_local().reliable();
    _sub = create_subscription<custom_interfaces::msg::Items>("compose_items", qos_settings, callback);
  }

  uint32_t ComposeItems::_getItemsIdx()
  {
    std::vector<uint32_t> item_ids = _db->getRowsId("item");
    return item_ids[item_ids.size() - 1];
  }

  void ComposeItems::_configure()
  {
    this->get_parameter<std::string>("host", _host);
    this->get_parameter<std::string>("port", _port);
    this->get_parameter<std::string>("db_name", _db_name);
    this->get_parameter<std::string>("username", _username);
    this->get_parameter<std::string>("password", _password);
    this->get_parameter<bool>("debug", _debug);
  }

  void ComposeItems::_initializeParameters()
  {
    rcl_interfaces::msg::ParameterDescriptor host_descriptor;
    host_descriptor.name = "host";
    host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("host", "", host_descriptor);

    rcl_interfaces::msg::ParameterDescriptor port_descriptor;
    port_descriptor.name = "port";
    port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("port", "", port_descriptor);

    rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
    db_name_descriptor.name = "db_name";
    db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("db_name", "", db_name_descriptor);

    rcl_interfaces::msg::ParameterDescriptor username_descriptor;
    username_descriptor.name = "username";
    username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("username", "", username_descriptor);

    rcl_interfaces::msg::ParameterDescriptor password_descriptor;
    password_descriptor.name = "password";
    password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("password", "", password_descriptor);

    rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
    debug_descriptor.name = "debug";
    debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
    this->declare_parameter("debug", false, debug_descriptor);
  }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::ComposeItems)
