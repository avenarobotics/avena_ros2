#!/bin/bash

ros2 run virtualscene clear
ros2 run virtualscene clear coppelia_brain
ros2 run virtualscene create orange position random 10
# ros2 run virtualscene create carrot position random 5
# ros2 run virtualscene create milk position random 5
# ros2 run virtualscene create lipton position random 5
ros2 run virtualscene create random 15
sleep 10
ros2 run cli scene_publisher
sleep 2
ros2 run cli detect
sleep 2
ros2 run cli filter_detections
sleep 2
ros2 run cli compose_items
sleep 2
ros2 run cli estimate_shape
sleep 2
ros2 run cli get_occupancy_grid
sleep 2
ros2 run cli item_select orange operating_area include nearest
sleep 2
ros2 run cli octomap_filter
sleep 2
ros2 run cli spawn_collision_items