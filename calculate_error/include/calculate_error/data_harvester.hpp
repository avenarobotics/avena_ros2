#pragma once
#include "rclcpp/rclcpp.hpp"
#include "custom_interfaces/srv/get_items_info.hpp"
#include <memory>

using namespace std::chrono_literals;

std::vector<custom_interfaces::msg::CoppeliaItemInfo> harvestCoppeliaData(std::shared_ptr<rclcpp::Node> node, std::string coppelia_name);