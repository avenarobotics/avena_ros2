#include "calculate_error/data_harvester.hpp"

std::vector<custom_interfaces::msg::CoppeliaItemInfo> harvestCoppeliaData(std::shared_ptr<rclcpp::Node> node, std::string coppelia_name)
{
    std::string service_name = "/control_"+coppelia_name+"/get_items_info";

    rclcpp::Client<custom_interfaces::srv::GetItemsInfo>::SharedPtr client =
        node->create_client<custom_interfaces::srv::GetItemsInfo>(service_name);
    
    auto request = std::make_shared<custom_interfaces::srv::GetItemsInfo::Request>();
    while (!client->wait_for_service(1s))
    {
        if (!rclcpp::ok())
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
            throw std::runtime_error(std::string("Interrupted while waiting for the service. Exiting."));
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
    }

    auto result = client->async_send_request(request);

    if (rclcpp::spin_until_future_complete(node, result) == rclcpp::FutureReturnCode::SUCCESS)
    {
        return result.get().get()->coppelia_items;
    }
    return std::vector<custom_interfaces::msg::CoppeliaItemInfo>();
}