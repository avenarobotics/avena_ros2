#include "rclcpp/rclcpp.hpp"
#include "calculate_error/calc.hpp"

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("calculate_error");

    try{
        ErrorCalculator ec(node);
        ec.run();
    }
    catch(std::runtime_error &e)
    {
        RCLCPP_ERROR(node->get_logger(), e.what());
    }
}