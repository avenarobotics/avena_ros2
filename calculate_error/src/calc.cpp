#include "calculate_error/calc.hpp"

ErrorCalculator::ErrorCalculator(std::shared_ptr<rclcpp::Node> &node)
{
    _string_to_label_assignment = {{"orange", ItemLabel_e::ORANGE},
                                   {"lipton", ItemLabel_e::LIPTON},
                                   {"milk", ItemLabel_e::MILK},
                                   {"cucumber", ItemLabel_e::CUCUMBER},
                                   {"broccoli", ItemLabel_e::BROCCOLI},
                                   {"onion", ItemLabel_e::ONION},
                                   {"bowl", ItemLabel_e::BOWL},
                                   {"banana", ItemLabel_e::BANANA},
                                   {"plate", ItemLabel_e::PLATE},
                                   {"carrot", ItemLabel_e::CARROT},
                                   {"knife", ItemLabel_e::KNIFE},
                                   {"spatula", ItemLabel_e::SPATULA},
                                   {"cuttingboard", ItemLabel_e::CUTTING_BOARD}};

    _camera_items = harvestCoppeliaData(node, "coppelia_camera");
    _brain_items = harvestCoppeliaData(node, "coppelia_brain");
}

ItemLabel_e ErrorCalculator::_getItemLabel(std::string coppelia_name)
{
    // std::cout << "+------------------pre proccessging--------------------+\n";
    // std::cout << "Raw name: " << coppelia_name << "\n";
    auto isalphachar = [](auto const &c) -> bool { return !(c >= 'a' && c <= 'z'); };

    std::transform(coppelia_name.begin(), coppelia_name.end(), coppelia_name.begin(),
                   [](unsigned char c) { return std::tolower(c); });

    coppelia_name.erase(std::remove_if(coppelia_name.begin(), coppelia_name.end(),
                                       isalphachar),
                        coppelia_name.end());
    // std::cout << "Proccessed name: " << coppelia_name << "\n";
    // std::cout << "Value: " <<  static_cast<int>(_string_to_label_assignment[coppelia_name]) << "\n";
    return _string_to_label_assignment[coppelia_name];
    //throw(std::runtime_error(std::string("label not found")));
}

float ErrorCalculator::_getDistanceError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::CoppeliaItemInfo &coppelia_brain_item)
{
    return sqrt(
        pow((coppelia_camera_item.pose.position.x - coppelia_brain_item.pose.position.x), 2) +
        pow((coppelia_camera_item.pose.position.y - coppelia_brain_item.pose.position.y), 2) +
        pow((coppelia_camera_item.pose.position.z - coppelia_brain_item.pose.position.z), 2));
}

nlohmann::json ErrorCalculator::_getSizeError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::CoppeliaItemInfo &coppelia_brain_item)
{
    nlohmann::json result;
    std::cout << "Coppelia Camera item size:\n";
    std::cout << "  x: " << coppelia_camera_item.size.x << "\n";
    std::cout << "  y: " << coppelia_camera_item.size.y << "\n";
    std::cout << "  z: " << coppelia_camera_item.size.z << "\n";
    std::cout << "Coppelia Brain item size:\n";
    std::cout << "  x: " << coppelia_brain_item.size.x << "\n";
    std::cout << "  y: " << coppelia_brain_item.size.y << "\n";
    std::cout << "  z: " << coppelia_brain_item.size.z << "\n";
    double absolute_values[3] = {
        coppelia_camera_item.size.x - coppelia_brain_item.size.x,
        coppelia_camera_item.size.y - coppelia_brain_item.size.y,
        coppelia_camera_item.size.z - coppelia_brain_item.size.z};

    result["absolute"] = nlohmann::json{
        {"x", abs(absolute_values[0])},
        {"y", abs(absolute_values[1])},
        {"z", abs(absolute_values[2])}};

    result["percentage"] = nlohmann::json{
        {"x", abs(absolute_values[0] / coppelia_camera_item.size.x * 100)},
        {"y", abs(absolute_values[1] / coppelia_camera_item.size.y * 100)},
        {"z", abs(absolute_values[2] / coppelia_camera_item.size.z * 100)}};
    return result;
}

nlohmann::json ErrorCalculator::_getOrientationError(custom_interfaces::msg::CoppeliaItemInfo &coppelia_camera_item, custom_interfaces::msg::CoppeliaItemInfo &coppelia_brain_item, OrientationsToCheck orientations)
{
    Eigen::Matrix3f camera_item_matrix = utils::quaternionToMatrix(coppelia_camera_item.pose.orientation);
    Eigen::Matrix3f brain_item_matrix = utils::quaternionToMatrix(coppelia_brain_item.pose.orientation);

    std::vector<geometry_msgs::msg::Vector3> camera_item_axes = utils::extractVectorAxes(camera_item_matrix);
    std::vector<geometry_msgs::msg::Vector3> brain_item_axes = utils::extractVectorAxes(brain_item_matrix);

    switch (orientations)
    {
    case OrientationsToCheck::XYZ:
        return nlohmann::json{
            {"x", utils::getAngle(camera_item_axes[0], brain_item_axes[0])},
            {"y", utils::getAngle(camera_item_axes[1], brain_item_axes[1])},
            {"z", utils::getAngle(camera_item_axes[2], brain_item_axes[2])}};

    case OrientationsToCheck::XY:
        return nlohmann::json{
            {"x", utils::getAngle(camera_item_axes[0], brain_item_axes[0])},
            {"y", utils::getAngle(camera_item_axes[1], brain_item_axes[1])}};

    case OrientationsToCheck::XZ:
        return nlohmann::json{
            {"y", utils::getAngle(camera_item_axes[0], brain_item_axes[0])},
            {"z", utils::getAngle(camera_item_axes[2], brain_item_axes[2])}};

    case OrientationsToCheck::YZ:
        return nlohmann::json{
            {"y", utils::getAngle(camera_item_axes[1], brain_item_axes[1])},
            {"z", utils::getAngle(camera_item_axes[2], brain_item_axes[2])}};

    case OrientationsToCheck::X:
        return nlohmann::json{
            {"x", utils::getAngle(camera_item_axes[0], brain_item_axes[0])}};

    case OrientationsToCheck::Y:
        return nlohmann::json{
            {"y", utils::getAngle(camera_item_axes[1], brain_item_axes[1])}};

    case OrientationsToCheck::Z:
        return nlohmann::json{
            {"z", utils::getAngle(camera_item_axes[2], brain_item_axes[2])}};

    default:
        throw(std::runtime_error(std::string("Wrong orientation option")));
    }
}

custom_interfaces::msg::CoppeliaItemInfo ErrorCalculator::_getNearestBrainItem(custom_interfaces::msg::CoppeliaItemInfo &camera_item, float &out_distance)
{
    float min = std::numeric_limits<float>::max();
    custom_interfaces::msg::CoppeliaItemInfo chosen_brain_item;
    float current_distance;
    ItemLabel_e camera_item_label = _getItemLabel(camera_item.label);

    ItemLabel_e brain_item_label;
    for (auto brain_item : _brain_items)
    {
        brain_item_label = _getItemLabel(brain_item.label);
        if (brain_item_label != camera_item_label)
            continue;
        current_distance = _getDistanceError(camera_item, brain_item);
        if (current_distance < min)
        {
            min = current_distance;
            chosen_brain_item = brain_item;
        }
    }

    out_distance = min;
    return chosen_brain_item;
}

void ErrorCalculator::run()
{
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "coppelia_camera");
    std::for_each(
        _camera_items.begin(),
        _camera_items.end(),
        [this](custom_interfaces::msg::CoppeliaItemInfo item) {
            float distance;
            custom_interfaces::msg::CoppeliaItemInfo nearest_brain_item = this->_getNearestBrainItem(item, distance);
            routeCalculationMethod(item, nearest_brain_item);
        });
}

void ErrorCalculator::routeCalculationMethod(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    std::cout << "+------------------------------------------------------+\n\n";
    std::cout << "Coppelia Camera name: " << camera_item.label << "\n";
    std::cout << "Coppelia Brain name: " << brain_item.label << "\n";
    std::cout << "Type ID: " << static_cast<int>(_getItemLabel(camera_item.label)) << "\n";

    nlohmann::json result;
    switch (_getItemLabel(camera_item.label))
    {
    case ItemLabel_e::ORANGE:
        result = getOrangeError(camera_item, brain_item); //done
        break;

    case ItemLabel_e::LIPTON:
        result = getLiptonError(camera_item, brain_item); // done
        break;

    case ItemLabel_e::CUCUMBER:
        result = getCucumberError(camera_item, brain_item); //done
        break;

    case ItemLabel_e::MILK:
        result = getMilkError(camera_item, brain_item); // done
        break;

    case ItemLabel_e::CARROT:
        result = getCarrotError(camera_item, brain_item); //done
        break;

    case ItemLabel_e::BANANA:
        result = getBananaError(camera_item, brain_item);
        break;

    case ItemLabel_e::BROCCOLI:
        result = getBroccoliError(camera_item, brain_item);
        break;

    case ItemLabel_e::BOWL:
        result = getBowlError(camera_item, brain_item);
        break;

    case ItemLabel_e::PLATE:
        result = getPlateError(camera_item, brain_item);
        break;

    case ItemLabel_e::ONION:
        result = getOnionError(camera_item, brain_item);
        break;

    case ItemLabel_e::KNIFE:
        result = getKnifeError(camera_item, brain_item);
        break;

    case ItemLabel_e::SPATULA:
        result = getSpatulaError(camera_item, brain_item);
        break;

    case ItemLabel_e::CUTTING_BOARD:
        result = getCuttingBoardError(camera_item, brain_item);
        break;
    }

    std::cout << result.dump(4) << '\n';
    std::cout << "\n";
}

nlohmann::json ErrorCalculator::getOrangeError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    nlohmann::json size_error = _getSizeError(camera_item, brain_item);

    return {
        {"type", "orange"},
        {"size_error", size_error},
        {"distance_error", distance}};
}

nlohmann::json ErrorCalculator::getLiptonError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    nlohmann::json size_error = _getSizeError(camera_item, brain_item);
    nlohmann::json orientation_error = _getOrientationError(camera_item, brain_item, OrientationsToCheck::XYZ);

    return {
        {"type", "lipton"},
        {"size_error", size_error},
        {"distance_error", distance},
        {"orientation_error", orientation_error}};
}

nlohmann::json ErrorCalculator::getMilkError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    nlohmann::json size_error = _getSizeError(camera_item, brain_item);
    nlohmann::json orientation_error = _getOrientationError(camera_item, brain_item, OrientationsToCheck::XYZ);

    return {
        {"type", "milk"},
        {"size_error", size_error},
        {"distance_error", distance},
        {"orientation_error", orientation_error}};
}

nlohmann::json ErrorCalculator::getCucumberError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    nlohmann::json coppelia_size_error = _getSizeError(camera_item, brain_item);
    nlohmann::json orientation_error = _getOrientationError(camera_item, brain_item, OrientationsToCheck::Z);

    nlohmann::json size_error = {
        {"absolute",
         {"radius", coppelia_size_error["absolute"]["x"],
          "height", coppelia_size_error["absolute"]["z"]}},
        {"percentage",
         {"radius", coppelia_size_error["percentage"]["x"],
          "height", coppelia_size_error["percentage"]["z"]}}};

    return {
        {"type", "cucumber"},
        {"size_error", size_error},
        {"distance_error", distance},
        {"orientation_error", orientation_error}};
}

nlohmann::json ErrorCalculator::getBroccoliError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    return {
        {"type", "broccoli"}};
}

nlohmann::json ErrorCalculator::getBananaError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    nlohmann::json orientation_error = _getOrientationError(camera_item, brain_item, OrientationsToCheck::XYZ);
    return {
        {"type", "banana"},
        {"distance_error", distance},
        {"size_error", "unavailable"},
        {"orientation_error", orientation_error}};
}

nlohmann::json ErrorCalculator::getCarrotError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    nlohmann::json coppelia_size_error = _getSizeError(camera_item, brain_item);
    nlohmann::json orientation_error = _getOrientationError(camera_item, brain_item, OrientationsToCheck::Z);

    nlohmann::json size_error = {
        {"absolute",
         {"radius", coppelia_size_error["absolute"]["x"],
          "height", coppelia_size_error["absolute"]["z"]}},
        {"percentage",
         {"radius", coppelia_size_error["percentage"]["x"],
          "height", coppelia_size_error["percentage"]["z"]}}};

    return {
        {"type", "carrot"},
        {"size_error", size_error},
        {"distance_error", distance},
        {"orientation_error", orientation_error}};
}

nlohmann::json ErrorCalculator::getSpatulaError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    return {
        {"type", "spatula"}};
}

nlohmann::json ErrorCalculator::getKnifeError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    return {
        {"type", "knife"}};
}

nlohmann::json ErrorCalculator::getPlateError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    return {
        {"type", "plate"},
        {"distance", distance}};
}

nlohmann::json ErrorCalculator::getBowlError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    return {
        {"type", "bowl"},
        {"distance", distance}};
}

nlohmann::json ErrorCalculator::getOnionError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    return {
        {"type", "onion"}};
}

nlohmann::json ErrorCalculator::getCuttingBoardError(custom_interfaces::msg::CoppeliaItemInfo &camera_item, custom_interfaces::msg::CoppeliaItemInfo &brain_item)
{
    float distance = _getDistanceError(camera_item, brain_item);
    nlohmann::json orientation_error = _getOrientationError(camera_item, brain_item, OrientationsToCheck::XYZ);
    return {
        {"type", "cutting_board"},
        {"distance_error", distance},
        {"size_error", "unavailable"},
        {"orientation_error", "unavailable"}};
}