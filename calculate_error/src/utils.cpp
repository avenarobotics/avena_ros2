#include "calculate_error/utils.hpp"

namespace utils
{

    float getDistance(const geometry_msgs::msg::Point &p1, const geometry_msgs::msg::Point &p2)
    {
        float result = sqrt(
            (p2.x - p1.x) * (p2.x - p1.x) +
            (p2.y - p1.y) * (p2.y - p1.y) +
            (p2.z - p1.z) * (p2.z - p1.z));
        return result;
    }

    float getAngle(const geometry_msgs::msg::Vector3 &v1, const geometry_msgs::msg::Vector3 &v2)
    {
        float dot_product = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
        float len_v1_sq = v1.x * v1.x + v1.y * v1.y + v1.z * v1.z;
        float len_v2_sq = v2.x * v2.x + v2.y * v2.y + v2.z * v2.z;

        return acos(dot_product / sqrt(len_v1_sq * len_v2_sq));
    }

    Eigen::Matrix3f quaternionToMatrix(const geometry_msgs::msg::Quaternion &q1)
    {
        Eigen::Quaternionf eigen_quaternion(q1.w, q1.x, q1.y, q1.z);
        Eigen::Matrix3f rotation_matrix = eigen_quaternion.normalized().toRotationMatrix();
        return rotation_matrix;
    }

    std::vector<geometry_msgs::msg::Vector3> extractVectorAxes(const Eigen::Matrix3f &matrix)
    {
        geometry_msgs::msg::Vector3 x_axis;
        x_axis.x = matrix(0);
        x_axis.y = matrix(1);
        x_axis.z = matrix(2);

        geometry_msgs::msg::Vector3 y_axis;
        y_axis.x = matrix(3);
        y_axis.y = matrix(4);
        y_axis.z = matrix(5);

        geometry_msgs::msg::Vector3 z_axis;
        z_axis.x = matrix(6);
        z_axis.y = matrix(7);
        z_axis.z = matrix(8);

        return {x_axis, y_axis, z_axis};
    }

    float angleError(float angle, bool only_direction)
    {
        if (only_direction)
            return abs(sin(angle));
        else
            return abs(angle / M_PI);
    }
}
