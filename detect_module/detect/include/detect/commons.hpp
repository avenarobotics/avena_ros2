#ifndef DETECT__COMMONS_HPP_
#define DETECT__COMMONS_HPP_

#include <exception>
#include <nlohmann/json.hpp>

class DetectronServerException : public std::exception
{
public:
    DetectronServerException(nlohmann::json &error_json) : _error_json(error_json) {}
    const char *what() const throw()
    {
        std::string exception = "";
        for (auto &el : this->_error_json["error_body"].items())
        {
            exception = el.value();
        }
        return exception.c_str();
    };

private:
    nlohmann::json _error_json;
};

struct EmptySceneMsgException : public std::exception
{
    const char *what() const throw()
    {
        return "The message on the Scene topic is empty - pipeline will fail!";
    };
};

struct NoSceneMsgException : public std::exception
{
    const char *what() const throw()
    {
        return "There is NO message on the scene topic - pipeline will fail!";
    };
};

#endif // DETECT__COMMONS_HPP_