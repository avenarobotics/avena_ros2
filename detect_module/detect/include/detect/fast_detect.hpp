#ifndef DETECT__FAST_DETECT_HPP_
#define DETECT__FAST_DETECT_HPP_

// ___CPP___
#include <functional>
#include <memory>

// __ROS__
#include <rclcpp/rclcpp.hpp>

// ___AVENA___
#include <custom_interfaces/action/simple_action.hpp>
#include <custom_interfaces/msg/detections.hpp>
#include <custom_interfaces/msg/rgb_images.hpp>
#include <helpers_vision/helpers_vision.hpp>
#include <helpers_commons/helpers_commons.hpp>

// ___Package___
#include "detect/visibility_control.h"
#include "detect/websocket_session.hpp"
#include "detect/commons.hpp"

namespace detect
{
  using json = nlohmann::json;
  using SerializedDetection = std::vector<std::string>;

  class FastDetect : public rclcpp::Node
  {
  public:
    explicit FastDetect(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());
    ~FastDetect() = default;

  private:
    /**
     * @brief This method connect to parameters server and reads parameters specific to detection server 
     * 
     * @param param_name in this module it is "detect"
     */
    void _getParameters(const std::string &param_name);

    /**
     * @brief This method connects to detection server using websockets, waits for result and publish detections to topic
     * 
     * @param rgb_images RGB images received from topic for all cameras currently in use
     */
    void _runDetection(const custom_interfaces::msg::RgbImages::SharedPtr rgb_images);

    /**
     * @brief Send serialized images to detection server, waits for result and returns detections in serialized JSON format.
     * 
     * @param serialized_images RGB images serialized using PNG encoding
     * @param detection_threshold threshold of confidence for detection
     * @param out_detections detections from the server in the same amount as number of serialized_images in JSON format
     */
    void _sendToServer(const std::vector<std::string> &serialized_images, const float &detection_threshold, std::vector<SerializedDetection> &out_detections);

    /**
     * @brief Serialize RGB images using PNG encoding
     * 
     * @param images input vector of RGB images to be serialized
     * @param out_serialized_images output serialized string with encoded image data
     */
    void _serializeImages(const std::vector<cv::Mat> &images, std::vector<std::string> &out_serialized_images);

    helpers::Watchdog::SharedPtr _watchdog;
    std::string _ip;
    std::string _model;
    std::map<std::string, std::string> _ports;
    const float _detection_threshold;
    const std::string _detectron_error_body_key = "error_body";

    rclcpp::Publisher<custom_interfaces::msg::Detections>::SharedPtr _detections_pub;
    rclcpp::Subscription<custom_interfaces::msg::RgbImages>::SharedPtr _rgb_images_sub;
  };

} // namespace detect

#endif // DETECT__FAST_DETECT_HPP_
