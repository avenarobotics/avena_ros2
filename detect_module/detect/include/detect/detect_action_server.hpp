#ifndef DETECT__DETECT_ACTION_SERVER_HPP_
#define DETECT__DETECT_ACTION_SERVER_HPP_

// ___CPP___
#include <functional>
#include <memory>
#include <string>

// __ROS__
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>

// ___AVENA___
#include <custom_interfaces/action/simple_action.hpp>
#include <custom_interfaces/msg/detections.hpp>
#include <custom_interfaces/msg/rgb_images.hpp>
#include <helpers_vision/helpers_vision.hpp>
#include <helpers_commons/helpers_commons.hpp>
#include "detect/visibility_control.h"
#include "detect/websocket_session.hpp"
#include "detect/commons.hpp"

namespace detect
{
  using json = nlohmann::json;

  class DetectActionServer : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    using DetectAction = custom_interfaces::action::SimpleAction;
    using GoalHandleDetectAction = rclcpp_action::ServerGoalHandle<DetectAction>;

    explicit DetectActionServer(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    void initNode() override;
    void shutDownNode() override;

    ~DetectActionServer() = default;

  private:
    helpers::Watchdog::SharedPtr _watchdog;
    void _getParameters(std::string param_name);
    std::string ip;
    std::string model;
    std::string detectron_error_body_key{"error_body"};

    //ROS
    rclcpp_action::Server<DetectAction>::SharedPtr _action_server;
    rclcpp_action::GoalResponse _handle_goal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const DetectAction::Goal> goal);
    rclcpp_action::CancelResponse _handle_cancel(const std::shared_ptr<GoalHandleDetectAction> goal_handle);
    void _handle_accepted(const std::shared_ptr<GoalHandleDetectAction> goal_handle);
    void _execute(const std::shared_ptr<GoalHandleDetectAction> goal_handle);
    void _toLowerCase(std::string &input);

    rclcpp::Publisher<custom_interfaces::msg::Detections>::SharedPtr _publisher;
    rclcpp::Subscription<custom_interfaces::msg::RgbImages>::SharedPtr _rgb_images_sub;
    custom_interfaces::msg::RgbImages::SharedPtr _rgb_images;
    builtin_interfaces::msg::Time _last_processed_msg_timestamp;
  };

} // namespace detect

#endif // DETECT__DETECT_ACTION_SERVER_HPP_
