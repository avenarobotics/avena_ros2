#ifndef DETECT__WEBSOCKET_SESSION_HPP_
#define DETECT__WEBSOCKET_SESSION_HPP_

// ___CPP___
#include <iostream>

// ___Boost___
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/program_options.hpp>

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace po = boost::program_options;

// Sends a WebSocket message and prints the response
class WebsocketSession : public std::enable_shared_from_this<WebsocketSession>
{
    /***
     * Class responsible for creating a websocket connection to the detectron server.
     * params resolver_, ws_,
     */
    tcp::resolver resolver_;
    websocket::stream<beast::tcp_stream> ws_;
    beast::flat_buffer buffer_;
    std::string host_;
    const char *data_;
    int message_size_{};
    float det_thresh_;
    std::vector<std::string> &internalJson;

public:
    // Resolver and socket require an io_context
    WebsocketSession(std::vector<std::string> &externalJson, net::io_context &ioc, float det_thresh);

    // Start the asynchronous operation
    void run(char const *host, char const *port, char const *data, int imgSize);

    void on_resolve(beast::error_code ec, tcp::resolver::results_type results);
    void on_connect(beast::error_code ec, tcp::resolver::results_type::endpoint_type ep);
    void on_handshake(beast::error_code ec);
    void on_write(beast::error_code ec, std::size_t bytes_transferred);
    void on_read(beast::error_code ec, std::size_t bytes_transferred);
    void on_close(beast::error_code ec);
};

#endif // DETECT__WEBSOCKET_SESSION_HPP_
