#include "detect/detect_action_server.hpp"

namespace detect
{
    DetectActionServer::DetectActionServer(const rclcpp::NodeOptions &options)
        : Node("detect", options)
    {
        helpers::commons::setLoggerLevelFromParameter(this);
        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local();
        _rgb_images_sub = create_subscription<custom_interfaces::msg::RgbImages>("rgb_images", qos_settings,
                                                                                 [this](const custom_interfaces::msg::RgbImages::SharedPtr rgb_images)
                                                                                 {
                                                                                     RCLCPP_DEBUG(this->get_logger(), "RGB images message received");
                                                                                     _rgb_images = rgb_images;
                                                                                 });
        _publisher = create_publisher<custom_interfaces::msg::Detections>("detections", qos_settings);

        this->_action_server = rclcpp_action::create_server<DetectAction>(
            this,
            "detect",
            std::bind(&DetectActionServer::_handle_goal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&DetectActionServer::_handle_cancel, this, std::placeholders::_1),
            std::bind(&DetectActionServer::_handle_accepted, this, std::placeholders::_1));

        _getParameters("detect");
        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        status=status=custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void DetectActionServer::_getParameters(std::string param_name)
    {
        RCLCPP_INFO(this->get_logger(), "Getting ip of detectron server...");
        std::string param_string;
        try
        {
            json data = helpers::commons::getParameter(param_name);
            this->model = data["model"].get<std::string>();
            this->ip = data["ip"].get<std::string>();
        }
        catch (const json::exception &e)
        {
            RCLCPP_ERROR_STREAM(this->get_logger(), "Reading \"" << param_name << "\" JSON error: " << e.what());
        }
    }

    void DetectActionServer::initNode()
    {
        status = custom_interfaces::msg::Heartbeat::STARTING;

        status = custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void DetectActionServer::shutDownNode()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;

        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    rclcpp_action::GoalResponse DetectActionServer::_handle_goal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const DetectAction::Goal> /*goal*/)
    {
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse DetectActionServer::_handle_cancel(const std::shared_ptr<GoalHandleDetectAction> /*goal_handle*/)
    {
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void DetectActionServer::_handle_accepted(const std::shared_ptr<GoalHandleDetectAction> goal_handle)
    {
        std::thread{std::bind(&DetectActionServer::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    void DetectActionServer::_execute(const std::shared_ptr<GoalHandleDetectAction> goal_handle)
    {

        helpers::Timer timer("Detect action", get_logger());
        try
        {
            auto result = std::make_shared<DetectAction::Result>();
            if (status != custom_interfaces::msg::Heartbeat::RUNNING)
            {
                RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
                goal_handle->abort(result);
                return;
            }
            RCLCPP_INFO(this->get_logger(), "Executing goal");
            const auto goal = goal_handle->get_goal();
            // auto feedback = std::make_shared<DetectAction::Feedback>();
            std::vector<std::string> det_result_json;
            if (!_rgb_images || _rgb_images->header.stamp == builtin_interfaces::msg::Time())
            {
                _publisher->publish(custom_interfaces::msg::Detections());
                goal_handle->abort(result);
                RCLCPP_ERROR(this->get_logger(), "Goal aborted - empty header in the RGB images message");
                return;
            }

            if (_last_processed_msg_timestamp == _rgb_images->header.stamp)
                RCLCPP_WARN(get_logger(), "New message has not arrived yet. Processing old message.");
            _last_processed_msg_timestamp = _rgb_images->header.stamp;

            //get pictures from scene topic
            cv::Mat img_1_rgba;
            cv::Mat img_2_rgba;
            helpers::converters::rosImageToCV(_rgb_images->cam1_rgb, img_1_rgba);
            helpers::converters::rosImageToCV(_rgb_images->cam2_rgb, img_2_rgba);
            cv::Mat img_1;
            cv::Mat img_2;
            helpers::converters::rosImageToCV(_rgb_images->cam1_rgb, img_1);
            helpers::converters::rosImageToCV(_rgb_images->cam2_rgb, img_2);
            cv::cvtColor(img_1, img_1, cv::COLOR_BGRA2BGR);
            cv::cvtColor(img_2, img_2, cv::COLOR_BGRA2BGR);

            cv::cvtColor(img_1_rgba, img_1, cv::COLOR_RGBA2RGB);
            cv::cvtColor(img_2_rgba, img_2, cv::COLOR_RGBA2RGB);

            std::vector<int> img_size(2);

            std::shared_ptr<std::iostream> blb_cam1{};
            std::shared_ptr<std::iostream> blb_cam2{};

            helpers::converters::imageToStream(img_1, blb_cam1);
            helpers::converters::imageToStream(img_2, blb_cam2);

            std::string str_cam1;
            std::string str_cam2;
            std::string str;

            std::istreambuf_iterator<char> isb_cam1{std::istreambuf_iterator<char>(*blb_cam1)};
            std::istreambuf_iterator<char> isb_cam2{std::istreambuf_iterator<char>(*blb_cam2)};
            str_cam1 = std::string(isb_cam1, std::istreambuf_iterator<char>());
            str_cam2 = std::string(isb_cam2, std::istreambuf_iterator<char>());

            char *buffer_cam1{nullptr};
            char *buffer_cam2{nullptr};
            std::vector<char *> img{buffer_cam1, buffer_cam2};
            std::vector<std::shared_ptr<std::iostream>> cam_blbs{blb_cam1, blb_cam2};
            for (size_t i = 0; i < cam_blbs.size(); i++)
            {
                // get length of file:
                cam_blbs[i]->seekg(0, std::istream::end);
                int length = cam_blbs[i]->tellg();
                img_size[i] = length;
                cam_blbs[i]->seekg(0, std::istream::beg);
                img[i] = new char[length];
                // read data as a block:
                cam_blbs[i]->read(img[i], length);
                std::string opsRes = "Read " + std::to_string(cam_blbs[i]->gcount()) + " of " + std::to_string(length) + " bytes\n";
                RCLCPP_INFO(this->get_logger(), opsRes);
            }

            float detection_threshold{0.7};
            const char *serverIpAddress = this->ip.c_str(); /*this->detect_server_ip.c_str();*/
            const char *port1;
            const char *port2;

            if (this->model == "virtual")
            {
                RCLCPP_INFO(this->get_logger(), "Setting detection model to 'VIRTUAL'");
                port1 = "8765";
                port2 = "8766";
            }
            else if (this->model == "real")
            {
                RCLCPP_INFO(this->get_logger(), "Setting detection model to 'REAL'");
                port1 = "8767";
                port2 = "8768";
            }
            else
            {
                RCLCPP_INFO(this->get_logger(), "Unknown value of param 'model' from parameter server, defaulting to 'virtual'");
                port1 = "8765";
                port2 = "8766";
            }

            net::io_context ioc;

            RCLCPP_INFO(this->get_logger(), "Sending images to detectron server...");
            auto t_start = std::chrono::high_resolution_clock::now();

            std::vector<std::string> results_cam1;
            std::vector<std::string> results_cam2;

            // Launch the asynchronous operation
            for (size_t i = 0; i < img.size(); i++)
            {
                if (i == 0)
                {
                    std::make_shared<WebsocketSession>(results_cam1, ioc, detection_threshold)->run(serverIpAddress, port1, img[i], img_size[i]);
                }
                else
                {
                    std::make_shared<WebsocketSession>(results_cam2, ioc, detection_threshold)->run(serverIpAddress, port2, img[i], img_size[i]);
                }
            }
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();

            while (results_cam2.size() != 1 && results_cam1.size() != 1)
            {
                RCLCPP_INFO(this->get_logger(), "Waiting for websocket read....");
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }

            auto t_stop = std::chrono::high_resolution_clock::now();
            RCLCPP_INFO(this->get_logger(), "Inference and communication took: " + std::to_string((std::chrono::duration_cast<std::chrono::nanoseconds>(t_stop - t_start).count()) * 1e-9) + "seconds");

            custom_interfaces::msg::Detections::UniquePtr detect_msg(new custom_interfaces::msg::Detections);
            std::vector<std::string> results{results_cam1[0], results_cam2[0]};
            for (size_t j = 0; j < results.size(); j++)
            {
                nlohmann::json jsonObj;
                std::stringstream(results[j]) >> jsonObj;
                std::vector<std::string> masks{};
                std::vector<std::string> instances_labels{};
                std::vector<float> scores{};
                std::vector<std::string> class_ids{};

                if (jsonObj.find(this->detectron_error_body_key) != jsonObj.end())
                {
                    throw DetectronServerException(jsonObj);
                }

                for (auto &el : jsonObj["masks"].items())
                {
                    masks.push_back(el.value().dump());
                }
                for (auto &el : jsonObj["classes"].items())
                {
                    std::string instance_class = el.value();
                    this->_toLowerCase(instance_class);
                    instances_labels.push_back(instance_class);
                }

                if (j == 0)
                {
                    detect_msg->cam1_masks = masks;
                    detect_msg->cam1_labels = instances_labels;
                }
                else if (j == 1)
                {
                    detect_msg->cam2_masks = masks;
                    detect_msg->cam2_labels = instances_labels;
                }
            }

            RCLCPP_INFO(this->get_logger(), "Publishing to detect topic ...");
            detect_msg->header.stamp = this->now();
            _publisher->publish(std::move(detect_msg));

            RCLCPP_INFO(this->get_logger(), "Success!");

            if (rclcpp::ok())
            {
                goal_handle->succeed(result);
                RCLCPP_INFO(this->get_logger(), "Goal succeeded");
            }
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
            RCLCPP_ERROR(this->get_logger(), "Exception caught, publishing empty detect message...");
            _publisher->publish(custom_interfaces::msg::Detections());
        }
    }

} // namespace detect

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(detect::DetectActionServer)
