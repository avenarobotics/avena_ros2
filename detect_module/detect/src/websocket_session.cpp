#include "detect/websocket_session.hpp"

void fail(beast::error_code ec, char const *what)
{
    std::cerr << what << ": " << ec.message() << "\n";
}

WebsocketSession::WebsocketSession(std::vector<std::string> &externalJson, net::io_context &ioc, float det_thresh)
    : resolver_(net::make_strand(ioc)), ws_(net::make_strand(ioc)),
      det_thresh_(det_thresh), internalJson(externalJson)
{
    // set websocket payloiads type to binary
    this->ws_.binary(true);
}

// Start the asynchronous operation
void WebsocketSession::run(char const *host, char const *port, char const *data, int imgSize)
{
    // Save these for lat
    host_ = host;
    data_ = data;
    this->message_size_ = imgSize;
    // Look up the domain name
    resolver_.async_resolve(host, port, beast::bind_front_handler(&WebsocketSession::on_resolve, shared_from_this()));
}

void WebsocketSession::on_resolve(beast::error_code ec, tcp::resolver::results_type results)
{
    if (ec)
        return fail(ec, "resolve");

    // Set the timeout for the operation
    beast::get_lowest_layer(ws_).expires_after(std::chrono::seconds(30));

    // Make the connection on the IP address we get from a lookup
    beast::get_lowest_layer(ws_).async_connect(results,
                                               beast::bind_front_handler(&WebsocketSession::on_connect, shared_from_this()));
}

void WebsocketSession::on_connect(beast::error_code ec, tcp::resolver::results_type::endpoint_type ep)
{
    if (ec)
        return fail(ec, "connect");

    // Turn off the timeout on the tcp_stream, because
    // the websocket stream has its own timeout system.
    beast::get_lowest_layer(ws_).expires_never();

    // Set suggested timeout settings for the websocket
    ws_.set_option(
        websocket::stream_base::timeout::suggested(
            beast::role_type::client));

    // Set a decorator to change the User-Agent of the handshake
    ws_.set_option(websocket::stream_base::decorator(
        [](websocket::request_type &req)
        {
            req.set(http::field::user_agent,
                    std::string(BOOST_BEAST_VERSION_STRING) +
                        " websocket-client-async");
        }));

    // Update the host_ string. This will provide the value of the
    // Host HTTP header during the WebSocket handshake.
    // See https://tools.ietf.org/html/rfc7230#section-5.4
    host_ += ':' + std::to_string(ep.port());

    // Perform the websocket handshake
    ws_.async_handshake(host_, "/",
                        beast::bind_front_handler(
                            &WebsocketSession::on_handshake,
                            shared_from_this()));
}

void WebsocketSession::on_handshake(beast::error_code ec)
{
    if (ec)
        return fail(ec, "handshake");
    // Send the message
    ws_.async_write(net::buffer(data_, this->message_size_),
                    beast::bind_front_handler(&WebsocketSession::on_write, shared_from_this()));
}

void WebsocketSession::on_write(beast::error_code ec, std::size_t bytes_transferred)
{
    boost::ignore_unused(bytes_transferred);
    if (ec)
        return fail(ec, "write");

    // Read a message into our buffer
    ws_.async_read(buffer_, beast::bind_front_handler(&WebsocketSession::on_read, shared_from_this()));
}

void WebsocketSession::on_read(beast::error_code ec, std::size_t bytes_transferred)
{
    boost::ignore_unused(bytes_transferred);

    if (ec)
        return fail(ec, "read");
    std::string json{boost::beast::buffers_to_string(buffer_.data())};
    this->internalJson.push_back(json);

    // Close the WebSocket connection
    ws_.async_close(websocket::close_code::normal,
                    beast::bind_front_handler(&WebsocketSession::on_close, shared_from_this()));
}

void WebsocketSession::on_close(beast::error_code ec)
{
    if (ec)
        return fail(ec, "close");
}
