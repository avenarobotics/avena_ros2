#include "detect/fast_detect.hpp"

namespace detect
{
    FastDetect::FastDetect(const rclcpp::NodeOptions &options)
        : Node("fast_detect", options),
          _detection_threshold(0.7)
    {
        helpers::commons::setLoggerLevelFromParameter(this);

        _getParameters("detect");
        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local();
        _detections_pub = create_publisher<custom_interfaces::msg::Detections>("fast_detections", qos_settings);
        _rgb_images_sub = create_subscription<custom_interfaces::msg::RgbImages>("rgb_images_stream", qos_settings, std::bind(&FastDetect::_runDetection, this, std::placeholders::_1));
    }

    void FastDetect::_getParameters(const std::string &param_name)
    {
        RCLCPP_INFO(get_logger(), "Getting ip of detectron server...");
        std::string param_string;
        try
        {
            json data = helpers::commons::getParameter(param_name);
            _model = data["model"].get<std::string>();
            _ip = data["ip"].get<std::string>();

            if (_model == "virtual")
            {
                RCLCPP_INFO(get_logger(), "Setting detection model to 'VIRTUAL'");
                _ports["port1"] = "8769";
                _ports["port2"] = "8770";
                // _ports["port1"] = "8765";
                // _ports["port2"] = "8766";
            }
            else if (_model == "real")
            {
                RCLCPP_FATAL(get_logger(), "Currently there is only Coppelia model available");
                rclcpp::shutdown();
                throw std::runtime_error("Run for life because there is no fast model you FOOL!!!");
                // RCLCPP_INFO(get_logger(), "Setting detection model to 'REAL'");
                _ports["port1"] = "8771";
                _ports["port2"] = "8772";
            }
            else
            {
                RCLCPP_INFO(get_logger(), "Unknown value of param 'model' from parameter server, defaulting to 'virtual'");
                _ports["port1"] = "8769";
                _ports["port2"] = "8770";
            }

            std::stringstream ss;
            ss << "Model type: \"" << _model << "\" with server IP: " << _ip << " and ports: ";
            for (auto &[k, v] : _ports)
                ss << v << ", ";
            RCLCPP_INFO(get_logger(), ss.str());
        }
        catch (const json::exception &e)
        {
            RCLCPP_ERROR_STREAM(get_logger(), "Reading \"" << param_name << "\" JSON error: " << e.what());
        }
    }

    void FastDetect::_runDetection(const custom_interfaces::msg::RgbImages::SharedPtr rgb_images)
    {
        helpers::Timer timer("Fast detect callback", get_logger());
        try
        {
            if (!rgb_images || rgb_images->header.stamp == builtin_interfaces::msg::Time())
            {
                _detections_pub->publish(custom_interfaces::msg::Detections());
                RCLCPP_ERROR(get_logger(), "Further processing skipped: empty header in the RGB images message");
                return;
            }

            // get pictures from scene topic
            std::vector<cv::Mat> mat_rgb_images(2);
            helpers::converters::rosImageToCV(rgb_images->cam1_rgb, mat_rgb_images[0]);
            helpers::converters::rosImageToCV(rgb_images->cam2_rgb, mat_rgb_images[1]);

            // Serialize images to PNG streams
            std::vector<std::string> serialized_img;
            _serializeImages(mat_rgb_images, serialized_img);

            std::vector<SerializedDetection> serialized_detections;
            _sendToServer(serialized_img, _detection_threshold, serialized_detections);

            custom_interfaces::msg::Detections::UniquePtr detect_msg(new custom_interfaces::msg::Detections);
            for (size_t j = 0; j < serialized_detections.size(); j++)
            {
                auto &detection = serialized_detections[j][0];
                nlohmann::json jsonObj = nlohmann::json::parse(detection);
                std::vector<std::string> masks{};

                if (jsonObj.find(_detectron_error_body_key) != jsonObj.end())
                    throw DetectronServerException(jsonObj);

                for (auto &el : jsonObj["masks"].items())
                    masks.push_back(el.value().dump());

                if (j == 0)
                {
                    detect_msg->cam1_masks = masks;
                    detect_msg->cam1_labels = jsonObj["classes"].get<std::vector<std::string>>();
                }
                else if (j == 1)
                {
                    detect_msg->cam2_masks = masks;
                    detect_msg->cam2_labels = jsonObj["classes"].get<std::vector<std::string>>();
                }
            }

            // ////////////////////////////////////////////////////////////
            // // TODO: Remove this after testing
            // auto draw_masks_with_labels = [this](const std::vector<std::string> &masks_rle, const std::vector<std::string> &labels, const cv::Mat &rgb_image, cv::Mat &out_mask)
            // {
            //     out_mask = cv::Mat::zeros(rgb_image.size(), CV_8UC1);
            //     for (size_t i = 0; i < masks_rle.size(); i++)
            //     {
            //         cv::Mat temp_mask;
            //         helpers::converters::stringToBinaryMask(masks_rle[i], temp_mask);
            //         cv::Moments m = cv::moments(temp_mask, true);
            //         cv::Point centroid(m.m10 / m.m00, m.m01 / m.m00);

            //         cv::bitwise_or(out_mask, temp_mask, out_mask);
            //         const auto font_scale = 0.5;
            //         const auto font_face = cv::FONT_HERSHEY_SIMPLEX;
            //         const auto thickness = 2;
            //         cv::putText(out_mask, labels[i], centroid, font_face, font_scale, cv::Scalar::all(128), thickness);
            //     }
            // };
            // cv::Mat cam1_masks;
            // draw_masks_with_labels(detect_msg->cam1_masks, detect_msg->cam1_labels, mat_rgb_images[0], cam1_masks);
            // cv::Mat cam2_masks;
            // draw_masks_with_labels(detect_msg->cam2_masks, detect_msg->cam2_labels, mat_rgb_images[0], cam2_masks);
            // cv::resize(cam1_masks, cam1_masks, cam1_masks.size() / 2);
            // cv::resize(cam2_masks, cam2_masks, cam2_masks.size() / 2);

            // cv::Mat concatenated_masks;
            // cv::hconcat(cam1_masks, cam2_masks, concatenated_masks);
            // cv::imshow("masks_fast_detect", concatenated_masks);

            // // cv::imshow("cam1_fast_detect", cam1_masks);
            // // cv::imshow("cam2_fast_detect", cam2_masks);
            // cv::waitKey(1);
            // ////////////////////////////////////////////////////////////

            RCLCPP_DEBUG(get_logger(), "Publishing to detect topic ...");
            detect_msg->header = rgb_images->header;
            _detections_pub->publish(std::move(detect_msg));
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(get_logger(), e.what());
            RCLCPP_ERROR(get_logger(), "Exception caught, publishing empty detect message...");
            _detections_pub->publish(custom_interfaces::msg::Detections());
        }
    }

    void FastDetect::_serializeImages(const std::vector<cv::Mat> &images, std::vector<std::string> &out_serialized_images)
    {
        helpers::Timer timer("Serialize RGB images using PNG encoding", get_logger());
        out_serialized_images.resize(images.size());

        for (size_t idx = 0; idx < images.size(); ++idx)
        {
            std::vector<uint8_t> encoded_img;
            cv::imencode(".png", images[idx], encoded_img);
            out_serialized_images[idx] = std::string(reinterpret_cast<const char *>(encoded_img.data()), encoded_img.size());
            RCLCPP_DEBUG_STREAM(get_logger(), "Camera " << idx + 1 << " RGB: Read " << out_serialized_images[idx].size() << " bytes");
        }
    }

    void FastDetect::_sendToServer(const std::vector<std::string> &serialized_images, const float &detection_threshold, std::vector<SerializedDetection> &out_detections)
    {
        helpers::Timer timer("Inference and communication", get_logger());
        out_detections.resize(serialized_images.size());
        net::io_context ioc;

        // Launch the asynchronous operation
        for (size_t i = 0; i < serialized_images.size(); i++)
        {
            if (i == 0)
                std::make_shared<WebsocketSession>(out_detections[0], ioc, detection_threshold)->run(_ip.c_str(), _ports["port1"].c_str(), serialized_images[i].c_str(), serialized_images[i].size());
            else
                std::make_shared<WebsocketSession>(out_detections[1], ioc, detection_threshold)->run(_ip.c_str(), _ports["port2"].c_str(), serialized_images[i].c_str(), serialized_images[i].size());
        }

        // Run the I/O service. The call will return when
        // the socket is closed.
        ioc.run();

        while (out_detections[0].size() != 1 && out_detections[1].size() != 1)
        {
            RCLCPP_INFO(get_logger(), "Waiting for websocket read....");
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }
} // namespace detect

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(detect::FastDetect)
