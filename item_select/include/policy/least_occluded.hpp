#ifndef LEASTOCCLUDED_HPP
#define LEASTOCCLUDED_HPP
#include "policy/policy.hpp"
#include <pcl/common/transforms.h>
#include <stdarg.h>
class LeastOccluded : public policy::Policy
{
public:
    explicit LeastOccluded( const std::map<std::string, std::string> &policy_values = {}, const std::map<std::string, double> &policy_parameters = {}, const bool &visualize = false);
    virtual ~LeastOccluded(){};
    // virtual int filter(const std::vector<uint16_t> &target_items_ids_in_target_area, const std::vector<custom_interfaces::msg::Item> &items, std::vector<uint16_t> &out_filtered_target_items_ids_in_target_area) override;
    virtual bool filter(const std::vector<uint16_t> &target_items_ids_in_target_area, const policy::PolicyInputData &data_msg, std::vector<uint16_t> &out_filtered_target_items_ids_in_target_area) override;

private:
    virtual bool _sort(std::vector<std::pair<uint16_t, double>> &target_items_ids_with_occulsion_ratio, const std::vector<double> &desired_occulsion_ratios = {}) override;
    void _getItemOcculsionRatio(pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld, std::string &occupancy_grid, const double &leaf_size, const double &gripper_offset, double &out_occulsion_ratio);
    void _dilatateItem(const float &gripper_offset, cv::Mat &item_mat, cv::Mat &out_item_border);
    void _convertPtcldToMask(pcl::PointCloud<pcl::PointXYZ>::Ptr flattened_item_ptcloud, const float &leaf_size, cv::Mat &out_item_mat);
    void _getItemOcculsionRatioOctomap(pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld, pcl::PointCloud<pcl::PointXYZ>::Ptr merged_ptcld, const double &leaf_size, const double &gripper_offset, double &out_occulsion_ratio);
    void _ShowManyImages(std::string title, int nArgs, ...);
    std::string _logger_name;
};
#endif