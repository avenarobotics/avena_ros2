#ifndef BIGGERTHAN_HPP
#define BIGGERTHAN_HPP
#include "policy/policy.hpp"
class BiggerThan : public policy::Policy
{
public:
    explicit BiggerThan(const std::map<std::string, std::string> &policy_values = {}, const std::map<std::string, double> &policy_parameters = {}, const bool &visualize = false);
    virtual ~BiggerThan(){};
    // virtual int filter(const std::vector<uint16_t> &target_items_ids_in_target_area, const std::vector<custom_interfaces::msg::Item> &items, std::vector<uint16_t> &out_filtered_target_items_ids_in_target_area) override;
    virtual bool filter(const std::vector<uint16_t> &target_items_ids_in_target_area, const policy::PolicyInputData &data_msg, std::vector<uint16_t> &out_filtered_target_items_ids_in_target_area) override;

private:
    virtual bool _sort(std::vector<std::pair<uint16_t, double>> &target_items_ids_with_distance, const std::vector<double> &target_distances = {}) override;
    std::string _logger_name;
};
#endif