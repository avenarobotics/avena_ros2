#ifndef POLICY_FACTORY_HPP
#define POLICY_FACTORY_HPP
// ___cpp__
#include <memory>
#include <vector>
// __Avena__
#include "nearest.hpp"
#include "farthest.hpp"
#include "lower_than.hpp"
#include "bigger_than.hpp"
#include "in_range.hpp"
#include "least_occluded.hpp"
class PolicyFactory
{
public:
    PolicyFactory() = delete;
    virtual ~PolicyFactory() = delete;
    static std::shared_ptr<policy::Policy> PolicySelector(const std::string &policy_type = "",
                                                  const std::map<std::string, std::string> &policy_values = {},
                                                  const std::map<std::string, double> &policy_parameters = {},
                                                  const bool &visualize = false)
    {
        if (policy_type == "nearest")
        {
            return std::make_shared<Nearest>(policy_values, policy_parameters, visualize);
        }
        if (policy_type == "farthest")
        {
            return std::make_shared<Farthest>(policy_values, policy_parameters, visualize);
        }
        if (policy_type == "lower_than")
        {
            return std::make_shared<LowerThan>(policy_values, policy_parameters, visualize);
        }
        if (policy_type == "bigger_than")
        {
            return std::make_shared<BiggerThan>(policy_values, policy_parameters, visualize);
        }
        if (policy_type == "in_range")
        {
            return std::make_shared<InRange>(policy_values, policy_parameters, visualize);
        }
        if (policy_type == "least_occluded")
        {
            return std::make_shared<LeastOccluded>(policy_values, policy_parameters, visualize);
        }
        else
            return nullptr;
    }
};

#endif