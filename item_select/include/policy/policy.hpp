#ifndef POLICY_HPP
#define POLICY_HPP
// _____CPP___
#include <iostream>
#include <map>
// _____AVENA___
#include "custom_interfaces/msg/occupancy_grid.hpp"
#include "custom_interfaces/msg/item.hpp"
#include "custom_interfaces/msg/items.hpp"
#include "custom_interfaces/msg/merged_ptcld_filtered.hpp"
#include <helpers_vision/helpers_vision.hpp>

namespace policy
{
    struct PolicyInputData
    {
        custom_interfaces::msg::Items items_msg;
        custom_interfaces::msg::MergedPtcldFiltered merged_ptcld_filtered_msg;
    };
    class Policy
    {
    public:
        explicit Policy(const std::map<std::string, std::string> &policy_values = {}, const std::map<std::string, double> &policy_parameters = {}, const bool &visualize = false) : _policy_values(policy_values),
                                                                                                                                                                                    _policy_parameters(policy_parameters),
                                                                                                                                                                                    _visualize(visualize){};
        virtual ~Policy(){};
        // virtual int filter(const std::vector<uint16_t> &target_items_ids_in_target_area, const std::vector<custom_interfaces::msg::Item> &items, std::vector<uint16_t> &out_filtered_target_items_ids_in_target_area) = 0;
        virtual bool filter(const std::vector<uint16_t> &target_items_ids_in_target_area, const PolicyInputData &data_msg, std::vector<uint16_t> &out_filtered_target_items_ids_in_target_area) = 0;

    protected:
        virtual bool _sort(std::vector<std::pair<uint16_t, double>> &target_items_ids_with_policy_value, const std::vector<double> &desired_policy_values = {}) = 0;

        std::map<std::string, std::string> _policy_values;
        std::map<std::string, double> _policy_parameters;
        bool _visualize;

        void _computeItemDistanceToRefFrame(pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld, const Eigen::Vector3d &ref_frame_position, double &out_item_distance_to_ref_frame);
        void _computeItemsDistanceToRefFrame(const std::vector<uint16_t> &target_items_ids_in_target_area, const custom_interfaces::msg::Items &data_msg, const Eigen::Vector3d &ref_frame_position, std::vector<std::pair<uint16_t, double>> &out_target_items_ids_with_distance_to_ref_frame);
        bool _composeTargetItemPtcld(const std::vector<custom_interfaces::msg::Item> &items, const uint16_t &target_item_id, pcl::PointCloud<pcl::PointXYZ>::Ptr out_item_ptcld);
        bool _checkAndConvertParamArg(std::string &param_arg_str, double &param_arg_double);
    };
} // policy end of namespace
#endif