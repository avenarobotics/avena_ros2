#ifndef ITEM_SELECT_HPP_
#define ITEM_SELECT_HPP_

//__AVENA__
#include "policy/policy_factory.hpp"

// __CPP__
#include <functional>
#include <memory>
#include <thread>
#include <chrono>
// __ROS__
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include <rclcpp_components/register_node_macro.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include "custom_interfaces/msg/occupancy_grid.hpp"
#include "custom_interfaces/action/item_select.hpp"
// #include "custom_interfaces/msg/item_select.hpp"
#include "custom_interfaces/msg/selected_items_ids.hpp"
#include "visibility_control.h"
#include "std_msgs/msg/string.hpp"

// __PCL__
#include <pcl/common/transforms.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/crop_box.h>
#include "Eigen/Geometry"
// __OPENCV__
#include <opencv2/core/eigen.hpp>
#include <stdarg.h>

// __Avena__
#include <helpers_commons/helpers_commons.hpp>
#include <helpers_vision/helpers_vision.hpp>

using namespace std::chrono_literals;
struct table_point
{
  std::string point_id;
  float x;
  float y;
  float z;
};
namespace item_select
{
  using json = nlohmann::json;

  class ItemSelectServer : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    using ItemSelectAction = custom_interfaces::action::ItemSelect;
    using GoalHandleItemSelectAction = rclcpp_action::ServerGoalHandle<ItemSelectAction>;
    COMPOSITION_PUBLIC
    explicit ItemSelectServer(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    void initNode() override;
    void shutDownNode() override;

  private:
    helpers::Watchdog::SharedPtr _watchdog;
    rclcpp_action::Server<ItemSelectAction>::SharedPtr _item_select_server;
    rclcpp_action::GoalResponse handle_goal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const ItemSelectAction::Goal> goal);
    rclcpp_action::CancelResponse handle_cancel(const std::shared_ptr<GoalHandleItemSelectAction> goal_handle);
    void handle_accepted(const std::shared_ptr<GoalHandleItemSelectAction> goal_handle);
    void execute(const std::shared_ptr<GoalHandleItemSelectAction> goal_handle);
    // void _occupancyGridCallback(const custom_interfaces::msg::OccupancyGrid::SharedPtr occupancy_grid_msg);
    bool _selectItemsInAreaByLabelAndPolicy(const std::string &target_item_label, const std::vector<std::string> &target_area_label, const std::string &selection_policy, const std::string &policy_frame, std::vector<uint16_t> &out_selected_items_ids);
    bool _findItemInTargetArea(const std::vector<pcl::PointXYZ> &target_area_coords, const custom_interfaces::msg::Item &target_item_by_label);
    void _getParameters();
    void _cliSelectCallback(const std_msgs::msg::String::SharedPtr cli_select_msg);
    bool _computeTargetAreasCoords(const std::vector<std::string> &target_area_label, const std::string &selected_area_operator, std::map<std::string, std::vector<pcl::PointXYZ>> &areas, std::vector<std::vector<pcl::PointXYZ>> &out_target_areas_coords);
    bool _getTargetItemsinTargetAreas(const std::string &target_item_label, std::vector<std::vector<pcl::PointXYZ>> &target_areas_coords, const custom_interfaces::msg::Items &data_msg, std::vector<uint16_t> &out_target_items_ids);
    pcl::PointXYZ _jsonToPointXYZ(const json &data);
    // void _fillItemSelectMsgByOccGridMsg(custom_interfaces::msg::OccupancyGrid &occupancy_grid_data, custom_interfaces::msg::ItemSelect &out_item_select_msg);
    bool _checkTargetItemLabelArg(std::vector<std::string> &labels, std::string &target_item_label);
    bool _checkTargetAreasLabelsArg(std::map<std::string, std::vector<pcl::PointXYZ>> &areas, std::vector<std::string> &target_areas_labels);
    int _validateInput();
    int _getBrainId(std::string container_brain_name, int &container_id);
    int _checkArea(Eigen::Vector3f position, Eigen::Vector3f dimemsions, Eigen::Quaternionf orientation, Eigen::Vector3f item_position);
    int _checkContainers(std::string area_name, std::string target_item_label, policy::PolicyInputData &policy_input, std::vector<uint16_t> &target_items_ids_in_target_area);
    Eigen::Vector3f _getReferenceFramePosition(const std::string &reference_frame_name);

    // // ROS
    rclcpp::Publisher<custom_interfaces::msg::SelectedItemsIds>::SharedPtr _item_select_pub;

    // rclcpp::Subscription<custom_interfaces::msg::EstimateShape>::SharedPtr _occupancy_grid_sub;
    rclcpp::Subscription<std_msgs::msg::String>::SharedPtr _cli_select_sub;

    // custom_interfaces::msg::EstimateShape::SharedPtr _occupancy_grid_data;
    custom_interfaces::msg::Items::SharedPtr _items_msg;
    custom_interfaces::msg::MergedPtcldFiltered::SharedPtr _merged_ptcld_filtered_msg;
    std::vector<std::string> _cli_arguments;
    std::string _selected_item_label;
    std::vector<std::string> _selected_area_label;
    std::string _selected_area_operator;
    std::string _distance_parameter_min;
    std::string _distance_parameter_max;

    std::map<std::string, std::vector<pcl::PointXYZ>> _areas;

    // std::map<int, std::string> _label_int_to_str;
    std::vector<std::string> _labels;
    float _area_height;
    float _leaf_size;
    float _min_inclusion_ratio;
    float _occlusion_offset;
    float _max_occlusion_ratio;

    std::string _selected_policy;

    bool _visualize;
    float _finger_length;
    float _finger_depth;

    std::string _policy_frame;

    std::map<std::string, Eigen::Vector3f> _ref_frame_position;

    std::map<std::string, std::string> _policy_values;

    helpers::SubscriptionsManager::SharedPtr _sub_manager;
  };
} // namespace item_select

#endif //ITEM_SELECT_HPP_
