#ifndef IIEM_SELECT_DEBUG__COMPONENT_HPP_
#define IIEM_SELECT_DEBUG__COMPONENT_HPP_

#include "visibility_control.h"
#include "rclcpp/rclcpp.hpp"
#include <mysql_connector.h>
#include <assert.h>
#include <sstream>

#include <iostream>
#include <memory>

#include <rclcpp/parameter.hpp>
#include "custom_interfaces/msg/selected_items_ids.hpp"
#include <memory>

namespace ros2mysql
{

  class ItemSelect : public rclcpp::Node
  {
  public:
    COMPOSITION_PUBLIC
    explicit ItemSelect(const rclcpp::NodeOptions &options);

  private:
    /**;
   * Configures component.
   *
   * Declares parameters and configures video capture.
   */
    COMPOSITION_PUBLIC
    void
    configure();

    /**;
   * Declares the parameter using rcl_interfaces.
   */
    COMPOSITION_PUBLIC
    void
    initialize_parameters();

    std::unique_ptr<MysqlConnector> db_;
    std::string host_;
    std::string port_;
    std::string db_name_;
    std::string username_;
    std::string password_;
    bool debug_;
    rclcpp::Subscription<custom_interfaces::msg::SelectedItemsIds>::SharedPtr sub_;
    // helpers::SubscriptionsManager::SharedPtr _sub_manager;
    std::map<int, std::string> _label_int_to_str;
  };

} // namespace ros2mysql

#endif // ITEM_SELECT_DEBUG__COMPONENT_HPP_
