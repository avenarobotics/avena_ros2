#include "policy/policy.hpp"
namespace policy
{
    void Policy::_computeItemDistanceToRefFrame(pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld, const Eigen::Vector3d &ref_frame_position, double &out_item_distance_to_ref_frame)
    {
        Eigen::Vector4d item_centroid(Eigen::Vector4d::Zero());
        pcl::compute3DCentroid(*item_ptcld, item_centroid);
        Eigen::Vector3d item_relative_position(item_centroid[0] - ref_frame_position.x(),
                                               item_centroid[1] - ref_frame_position.y(),
                                               item_centroid[2] - ref_frame_position.z());
        out_item_distance_to_ref_frame = item_relative_position.norm();
    }
    bool Policy::_composeTargetItemPtcld(const std::vector<custom_interfaces::msg::Item> &items, const uint16_t &target_item_id, pcl::PointCloud<pcl::PointXYZ>::Ptr out_item_ptcld)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_element_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
        auto target_item_iter = std::find_if(items.begin(), items.end(),
                                             [this, target_item_id](custom_interfaces::msg::Item item) {
                                                 return item.id == target_item_id;
                                             });
        if (target_item_iter != items.end())
        {
            for (auto item_element : target_item_iter->item_elements)
            {
                if (item_element.merged_ptcld.data.size() != 0)
                {
                    helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.merged_ptcld, item_element_ptcld);
                    *out_item_ptcld += *item_element_ptcld;
                    item_element_ptcld->clear();
                }
            }
            helpers::vision::statisticalOutlierRemovalFilter(out_item_ptcld, 100, 1.96);
            return true;
        }
        return false;
    }
    void Policy::_computeItemsDistanceToRefFrame(const std::vector<uint16_t> &target_items_ids_in_target_area, const custom_interfaces::msg::Items &data_msg, const Eigen::Vector3d &ref_frame_position, std::vector<std::pair<uint16_t, double>> &out_target_items_ids_with_distance_to_ref_frame)
    {
        double item_distance_to_ref_frame;
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
        for (auto &target_item_id : target_items_ids_in_target_area)
        {
            item_distance_to_ref_frame = 0.0;
            if (_composeTargetItemPtcld(data_msg.items, target_item_id, item_ptcld))
            {
                _computeItemDistanceToRefFrame(item_ptcld, ref_frame_position, item_distance_to_ref_frame);
                out_target_items_ids_with_distance_to_ref_frame.push_back({target_item_id, item_distance_to_ref_frame});
                item_ptcld->clear();
            }
            else
                continue;
        }
    }
    bool Policy::_checkAndConvertParamArg(std::string &param_arg_str, double &param_arg_double)
    {
        if (param_arg_str == "NULL")
        {
            std::cout << "no distance parameter passed, Goal Failed \n";
            std::cout << "Goal Failed \n";
            return false;
        }
        else
        {
            try
            {
                param_arg_double = std::stod(param_arg_str);
            }
            catch (const std::exception &e)
            {
                std::cerr << e.what() << '\n';
                std::cout << "wrong distance parameter passed, Goal Failed \n";
                std::cout << "Goal Failed \n";
                return false;
            }
            return true;
        }
    }
}