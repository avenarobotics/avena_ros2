#include "policy/least_occluded.hpp"
LeastOccluded::LeastOccluded(const std::map<std::string, std::string> &policy_values,
                             const std::map<std::string, double> &policy_parameters,
                             const bool &visualize) : Policy(policy_values, policy_parameters, visualize)
{
    _logger_name ="Item_select: least_occluded";
    RCLCPP_INFO(rclcpp::get_logger(_logger_name), "Policy chosen: Least Occluded");
}
bool LeastOccluded::filter(const std::vector<uint16_t> &target_items_ids_in_target_area, const policy::PolicyInputData &data_msg, std::vector<uint16_t> &out_filtered_target_items_ids_in_target_area)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
    std::vector<std::pair<uint16_t, double>> target_items_ids_with_occlusion_ratio;
    double occulsion_ratio(0.0), max_occlusion_ratio(0.0), gripper_offset(0.0);
    try
    {
        gripper_offset = _policy_parameters.at("finger_depth") + _policy_parameters.at("occlusion_offset");
    }
    catch (const std::out_of_range &e)
    {
        std::cout << e.what() << std::endl;
        return false;
    }
    // std::string occ_grid = data_msg.occupancy_grid;
    pcl::PointCloud<pcl::PointXYZ>::Ptr merged_scene(new pcl::PointCloud<pcl::PointXYZ>);
    helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(data_msg.merged_ptcld_filtered_msg.merged_ptcld_filtered, merged_scene);
    for (auto &target_item_id : target_items_ids_in_target_area)
    {
        if (_composeTargetItemPtcld(data_msg.items_msg.items, target_item_id, item_ptcld))
        {
            try
            {
                _getItemOcculsionRatioOctomap(item_ptcld, merged_scene, _policy_parameters.at("occ_grid_leaf_size"), gripper_offset, occulsion_ratio);
            }
            catch (const std::out_of_range &e)
            {
                std::cout << e.what() << std::endl;
                return false;
            }
            target_items_ids_with_occlusion_ratio.push_back({target_item_id, occulsion_ratio});
            item_ptcld->clear();
            occulsion_ratio = 0.0;
        }
        else
            continue;
    }
    if (target_items_ids_with_occlusion_ratio.size() != 0)
    {
        if (target_items_ids_with_occlusion_ratio.size() > 1)
            _sort(target_items_ids_with_occlusion_ratio, {});

        // for (auto &target_item_with_with_occlusion_ratio : target_items_ids_with_occlusion_ratio)
        //     out_filtered_target_items_ids_in_target_area.push_back(target_item_with_with_occlusion_ratio.first);
        RCLCPP_INFO(rclcpp::get_logger(_logger_name), "Item list with occupancy ratio");
        for (auto &target_item_with_with_occlusion_ratio : target_items_ids_with_occlusion_ratio)
        {
            out_filtered_target_items_ids_in_target_area.push_back(target_item_with_with_occlusion_ratio.first);
            RCLCPP_INFO(rclcpp::get_logger(_logger_name), "Item ID :  %hu -----  Occ_Ratio : %f", target_item_with_with_occlusion_ratio.first, target_item_with_with_occlusion_ratio.second);
        }
        try
        {
            max_occlusion_ratio = _policy_parameters.at("max_occlusion_ratio");
            if (target_items_ids_with_occlusion_ratio.begin()->second > max_occlusion_ratio)
                RCLCPP_INFO(rclcpp::get_logger(_logger_name), "Occlusion Detected! item surroundings are higher than max_occlusion_ratio:  %f", target_items_ids_with_occlusion_ratio.begin()->second);
            // std::cout << "Occlusion Detected! item surroundings are higher than max_occlusion_ratio: " << target_items_ids_with_occlusion_ratio.begin()->second << "\n";
            else
                RCLCPP_INFO(rclcpp::get_logger(_logger_name), "No occulsion! item surroundings are lower than max_occlusion_ratio : %f", target_items_ids_with_occlusion_ratio.begin()->second);
            // std::cout << "No occulsion! item surroundings are lower than max_occlusion_ratio : %f" << target_items_ids_with_occlusion_ratio.begin()->second << "\n";
        }
        catch (const std::out_of_range &e)
        {
            std::cout << e.what() << std::endl;
            // return false;
        }
        return true;
    }
    else
        return false;
}

void LeastOccluded::_getItemOcculsionRatioOctomap(pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld, pcl::PointCloud<pcl::PointXYZ>::Ptr merged_ptcld, const double &leaf_size, const double &gripper_offset, double &out_occulsion_ratio)
{
    helpers::vision::passThroughFilter(merged_ptcld, "z", 0.005, 2.0f, false);
    // _getItemPtcld(_item_select_data.selected_item_id, buffer);
    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree_occupancy(leaf_size);
    octree_occupancy.setInputCloud(merged_ptcld);
    octree_occupancy.addPointsFromInputCloud();
    float radius_search = leaf_size * 3;
    // std::vector<int> cloudNWRSearch;
    // std::vector<float> cloudNWRRadius;

    // remove item from scene
    for (const auto &point : item_ptcld->points)
    {
        std::vector<int> cloudNWRSearch;
        std::vector<float> cloudNWRRadius;
        octree_occupancy.radiusSearch(point, radius_search, cloudNWRSearch, cloudNWRRadius);
        for (const auto &cloud_ix : cloudNWRSearch)
        {
            //  std::cout<<" deleting voxel cloudNWRSearch"<<cloud_ix<<std::endl;

            octree_occupancy.deleteVoxelAtPoint(cloud_ix);
        }
    }
    // std::vector<int> cloudNWRSearch_filter;
    // std::vector<float> cloudNWRRadius_filter;
    int occ_ratio = 0;
    for (const auto &point : item_ptcld->points)
    {
        std::vector<int> cloudNWRSearch;
        std::vector<float> cloudNWRRadius;
        octree_occupancy.radiusSearch(point, radius_search + static_cast<float>(gripper_offset), cloudNWRSearch, cloudNWRRadius);
        for (const auto &cloud_ix : cloudNWRSearch)
        {
            //  std::cout<<" deleting voxel cloudNWRSearch"<<cloud_ix<<std::endl;
            occ_ratio++;
            octree_occupancy.deleteVoxelAtPoint(cloud_ix);
        }
    }
    out_occulsion_ratio = (static_cast<double>(occ_ratio) / static_cast<double>(item_ptcld->points.size()));
    // std::cout<<"occlusion voxel number is equal to "<<occ_ratio<<std::endl;
    // std::cout<<"occlusion ratio is equal to "<<out_occulsion_ratio<<std::endl;
}

void LeastOccluded::_getItemOcculsionRatio(pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld, std::string &occupancy_grid, const double &leaf_size, const double &gripper_offset, double &out_occulsion_ratio)
{
    pcl::PointXYZ centroid = pcl::PointXYZ(0, 0, 0);
    Eigen::Quaternionf z_rot = Eigen::Quaternionf(1, 0, 0, 0);
    cv::Mat item_img(cv::Mat::zeros(1 / leaf_size, 2 / leaf_size, CV_8UC1)),
        item_border(cv::Mat::zeros(1 / leaf_size, 2 / leaf_size, CV_8UC1)),
        item_border_collisions(cv::Mat::zeros(1 / leaf_size, 2 / leaf_size, CV_8UC1)),
        filtered_item_border_collisions(cv::Mat::zeros(1 / leaf_size, 2 / leaf_size, CV_8UC1)),
        occ_grid(cv::Mat::zeros(1 / leaf_size, 2 / leaf_size, CV_8UC1));

    helpers::vision::passThroughFilter(item_ptcld, "z", -0.005, 0.005, true);
    helpers::vision::flattenPlaneCloud(z_rot, centroid, item_ptcld);
    _convertPtcldToMask(item_ptcld, leaf_size, item_img);
    _dilatateItem(gripper_offset, item_img, item_border);
    helpers::converters::stringToBinaryMask(occupancy_grid, occ_grid);
    // cv::Point2f src_center(occ_grid.cols / 2.0F, occ_grid.rows / 2.0F);
    // cv::Mat rot_mat = cv::getRotationMatrix2D(src_center, 180, 1.0);
    // cv::warpAffine(occ_grid, occ_grid, rot_mat, occ_grid.size());
    cv::bitwise_and(occ_grid, item_border, item_border_collisions);
    cv::Mat kernel = cv::getStructuringElement(cv::MorphShapes::MORPH_ELLIPSE, cv::Size(3, 3));
    cv::morphologyEx(item_border_collisions, filtered_item_border_collisions, cv::MORPH_OPEN, kernel, cv::Point(-1, -1), 1);
    std::vector<cv::Point> item_border_locations, item_collision_locations;
    cv::findNonZero(item_border, item_border_locations);
    cv::findNonZero(filtered_item_border_collisions, item_collision_locations);
    out_occulsion_ratio = static_cast<float>(item_collision_locations.size()) / static_cast<float>(item_border_locations.size());
    if (_visualize)
    {
        _ShowManyImages("Occulsion", 5, item_img, item_border, occ_grid, item_border_collisions, filtered_item_border_collisions);
    }
}
bool LeastOccluded::_sort(std::vector<std::pair<uint16_t, double>> &target_items_ids_with_occulsion_ratio, const std::vector<double> &desired_occulsion_ratio)
{
    (void)desired_occulsion_ratio;
    std::sort(target_items_ids_with_occulsion_ratio.begin(), target_items_ids_with_occulsion_ratio.end(), [this](std::pair<uint16_t, double> &item1_with_occlusion, std::pair<uint16_t, double> &item2_with_occlussion) {
        return item1_with_occlusion.second < item2_with_occlussion.second;
    });
    return true;
}

void LeastOccluded::_dilatateItem(const float &gripper_offset, cv::Mat &item_mat, cv::Mat &out_item_border)
{
    int offset = std::ceil(gripper_offset * 100); // meters to cm
    cv::Mat kernel = cv::getStructuringElement(cv::MorphShapes::MORPH_ELLIPSE, cv::Size(3, 3));
    cv::Mat dilatated_item_img = cv::Mat::zeros(100, 200, CV_8UC1);
    cv::dilate(item_mat, dilatated_item_img, kernel, cv::Point(-1, -1), offset);
    cv::subtract(dilatated_item_img, item_mat, out_item_border);
}
void LeastOccluded::_convertPtcldToMask(pcl::PointCloud<pcl::PointXYZ>::Ptr flattened_item_ptcloud, const float &leaf_size, cv::Mat &out_item_mat)
{
    // transform flattened pt from world to image frame and get index in image
    for (auto &point : flattened_item_ptcloud->points)
    {
        int x = floor(point.x / leaf_size);
        int y = floor((point.y + 1) / leaf_size);
        out_item_mat.at<uint8_t>(x, y) = 255;
    }
}
void LeastOccluded::_ShowManyImages(std::string title, int nArgs, ...)
{
    using namespace cv;
    using namespace std;
    int size;
    int i;
    int m, n;
    int x, y;

    // w - Maximum number of images in a row
    // h - Maximum number of images in a column
    int w, h;

    // scale - How much we have to resize the image
    float scale;
    int max;

    // If the number of arguments is lesser than 0 or greater than 12
    // return without displaying
    if (nArgs <= 0)
    {
        printf("Number of arguments too small....\n");
        return;
    }
    else if (nArgs > 14)
    {
        printf("Number of arguments too large, can only handle maximally 12 images at a time ...\n");
        return;
    }
    // Determine the size of the image,
    // and the number of rows/cols
    // from number of arguments
    else if (nArgs == 1)
    {
        w = h = 1;
        size = 300;
    }
    else if (nArgs == 2)
    {
        w = 2;
        h = 1;
        size = 300;
    }
    else if (nArgs == 3 || nArgs == 4)
    {
        w = 2;
        h = 2;
        size = 300;
    }
    else if (nArgs == 5 || nArgs == 6)
    {
        w = 3;
        h = 2;
        size = 200;
    }
    else if (nArgs == 7 || nArgs == 8)
    {
        w = 4;
        h = 2;
        size = 200;
    }
    else
    {
        w = 4;
        h = 3;
        size = 150;
    }

    // Create a new 3 channel image
    Mat DispImage = Mat::zeros(Size(100 + size * w, 60 + size * h), CV_8UC1);

    // Used to get the arguments passed
    va_list args;
    va_start(args, nArgs);

    // Loop for nArgs number of arguments
    for (i = 0, m = 20, n = 20; i < nArgs; i++, m += (20 + size))
    {
        // Get the Pointer to the IplImage
        Mat img = va_arg(args, Mat);

        // Check whether it is NULL or not
        // If it is NULL, release the image, and return
        if (img.empty())
        {
            printf("Invalid arguments");
            return;
        }

        // Find the width and height of the image
        x = img.cols;
        y = img.rows;

        // Find whether height or width is greater in order to resize the image
        max = (x > y) ? x : y;

        // Find the scaling factor to resize the image
        scale = (float)((float)max / size);

        // Used to Align the images
        if (i % w == 0 && m != 20)
        {
            m = 20;
            n += 20 + size;
        }

        // Set the image ROI to display the current image
        // Resize the input image and copy the it to the Single Big Image
        Rect ROI(m, n, (int)(x / scale), (int)(y / scale));
        Mat temp;
        resize(img, temp, Size(ROI.width, ROI.height));
        temp.copyTo(DispImage(ROI));
    }

    // Create a new window, and show the Single Big Image
    namedWindow(title, 1);
    imshow(title, DispImage);
    // waitKey();
    waitKey(0);
    destroyAllWindows();

    // End the number of arguments
    va_end(args);
}
