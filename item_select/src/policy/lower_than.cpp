#include "policy/lower_than.hpp"
LowerThan::LowerThan(const std::map<std::string, std::string> &policy_values,
                     const std::map<std::string, double> &policy_parameters,
                     const bool &visualize) : Policy(policy_values, policy_parameters, visualize)
{
    _logger_name = "Item_select: lower_than";
    RCLCPP_INFO(rclcpp::get_logger(_logger_name), "Policy chosen: Lower Than");
}
bool LowerThan::filter(const std::vector<uint16_t> &target_items_ids_in_target_area, const policy::PolicyInputData &data_msg, std::vector<uint16_t> &out_filtered_target_items_ids_in_target_area)
{
    std::vector<std::pair<uint16_t, double>> target_items_ids_with_distance_to_ref_frame;
    Eigen::Vector3d ref_frame_position;
    double max_distance(0.0);
    try
    {
        ref_frame_position << _policy_parameters.at("ref_frame_x"), _policy_parameters.at("ref_frame_y"), _policy_parameters.at("ref_frame_z");
    }
    catch (const std::out_of_range &e)
    {
        std::cout << e.what() << std::endl;
        return false;
    }
    _computeItemsDistanceToRefFrame(target_items_ids_in_target_area, data_msg.items_msg, ref_frame_position, target_items_ids_with_distance_to_ref_frame);
    if (target_items_ids_with_distance_to_ref_frame.size() == 0)
        return false;
    else
    {
        try
        {
            // TODO: change min to max at cli for readibility 
            if (_checkAndConvertParamArg(_policy_values.at("min_distance"), max_distance))
            {
                if (!_sort(target_items_ids_with_distance_to_ref_frame, {max_distance}))
                    return false;
            }
            else
                return false;
        }
        catch (const std::out_of_range &e)
        {
            std::cout << e.what() << std::endl;
            return false;
        }
        for (auto &target_item_with_distance : target_items_ids_with_distance_to_ref_frame)
            out_filtered_target_items_ids_in_target_area.push_back(target_item_with_distance.first);
        return true;
    }
}
bool LowerThan::_sort(std::vector<std::pair<uint16_t, double>> &target_items_ids_with_distance_to_ref_frame, const std::vector<double> &desired_distance)
{
    for (auto target_item_iter = target_items_ids_with_distance_to_ref_frame.begin(); target_item_iter != target_items_ids_with_distance_to_ref_frame.end();)
    {
        if (target_item_iter->second >= desired_distance[0])
        {
            target_item_iter = target_items_ids_with_distance_to_ref_frame.erase(target_item_iter);
        }
        else
            ++target_item_iter;
    }
    if (target_items_ids_with_distance_to_ref_frame.size() > 1)
        std::sort(target_items_ids_with_distance_to_ref_frame.begin(), target_items_ids_with_distance_to_ref_frame.end(), [this](std::pair<uint16_t, double> &item1_with_distance, std::pair<uint16_t, double> &item2_with_distance) {
            return item1_with_distance.second < item2_with_distance.second;
        });
    if (target_items_ids_with_distance_to_ref_frame.size() == 0)
        return false;
    else
    {
        RCLCPP_INFO(rclcpp::get_logger(_logger_name), "Item list with distances");
        for (auto &target_item_ids_with_distance_to_ref_frame : target_items_ids_with_distance_to_ref_frame)
        {
            RCLCPP_INFO(rclcpp::get_logger(_logger_name), "Item ID :  %hu -----  Distance : %f", target_item_ids_with_distance_to_ref_frame.first, target_item_ids_with_distance_to_ref_frame.second);
        }
        return true;
    }
}
