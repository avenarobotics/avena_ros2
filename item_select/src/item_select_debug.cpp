#include "item_select_debug.hpp"

namespace ros2mysql
{

  ItemSelect::ItemSelect(const rclcpp::NodeOptions &options)
      : Node("item_select_debug", options)
  {
    // Declare parameters.
    this->initialize_parameters();

    this->configure();

    using namespace std::chrono_literals;

    try
    {
      this->db_ = std::make_unique<MysqlConnector>(this->host_, this->port_, this->db_name_, this->username_, this->password_, this->debug_);
    }
    catch (const network_exception &e)
    {
      RCLCPP_ERROR(this->get_logger(), e.what());
    }

    auto callback =
        [this](const typename custom_interfaces::msg::SelectedItemsIds::SharedPtr msg) -> void {
      RCLCPP_INFO(this->get_logger(), "Item Select debug: received data");
      left_grasp_t left_grasp_data; // right_grasp_t
      if (msg->selected_items_ids.size() != 0)
      {
        left_grasp_data.item_id = msg->selected_items_ids[0]; // right_grasp_data
        RCLCPP_INFO(this->get_logger(), "left_grasp_data.item_id %d",left_grasp_data.item_id );
      }
      try
      {
        db_->clearTable("left_grasp");
        db_->setLeftGrasp(&left_grasp_data); // setRightGrasp
      }
      catch (const network_exception &e)
      {
        RCLCPP_ERROR(this->get_logger(), e.what());
      }
      catch (const query_exception &e)
      {
        RCLCPP_ERROR(this->get_logger(), e.what());
      }

      RCLCPP_INFO(this->get_logger(), "Item Select debug: saved");
    };

    rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
    sub_ = create_subscription<custom_interfaces::msg::SelectedItemsIds>("item_select", latching_qos, callback);
  }

  void
  ItemSelect::configure()
  {
    this->get_parameter<std::string>("host", host_);
    this->get_parameter<std::string>("port", port_);
    this->get_parameter<std::string>("db_name", db_name_);
    this->get_parameter<std::string>("username", username_);
    this->get_parameter<std::string>("password", password_);
    this->get_parameter<bool>("debug", debug_);
  }

  void ItemSelect::initialize_parameters()
  {
    rcl_interfaces::msg::ParameterDescriptor host_descriptor;
    host_descriptor.name = "host";
    host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("host", "", host_descriptor);

    rcl_interfaces::msg::ParameterDescriptor port_descriptor;
    port_descriptor.name = "port";
    port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("port", "", port_descriptor);

    rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
    db_name_descriptor.name = "db_name";
    db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("db_name", "", db_name_descriptor);

    rcl_interfaces::msg::ParameterDescriptor username_descriptor;
    username_descriptor.name = "username";
    username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("username", "", username_descriptor);

    rcl_interfaces::msg::ParameterDescriptor password_descriptor;
    password_descriptor.name = "password";
    password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
    this->declare_parameter("password", "", password_descriptor);

    rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
    debug_descriptor.name = "debug";
    debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
    this->declare_parameter("debug", false, debug_descriptor);
  }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::ItemSelect)
