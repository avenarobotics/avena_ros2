#include "item_select.hpp"
namespace item_select
{
    ItemSelectServer::ItemSelectServer(const rclcpp::NodeOptions &options)
        : Node("item_select_server", options),
          _selected_area_operator(""),
          _area_height(0.0),
          _leaf_size(0.0),
          _min_inclusion_ratio(0.0),
          _selected_policy(""),
          _visualize(false),
          _finger_length(0.0),
          _finger_depth(0.0)
    {
        RCLCPP_INFO(this->get_logger(), "Initialization of item select server.");
        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local().reliable();
        this->_item_select_server = rclcpp_action::create_server<ItemSelectAction>(
            this->get_node_base_interface(),
            this->get_node_clock_interface(),
            this->get_node_logging_interface(),
            this->get_node_waitables_interface(),
            "item_select",
            std::bind(&ItemSelectServer::handle_goal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&ItemSelectServer::handle_cancel, this, std::placeholders::_1),
            std::bind(&ItemSelectServer::handle_accepted, this, std::placeholders::_1));
        // _occupancy_grid_sub = create_subscription<custom_interfaces::msg::OccupancyGrid>("occupancy_grid", qos_settings, std::bind(&ItemSelectServer::_occupancyGridCallback, this, std::placeholders::_1));
        _sub_manager = std::make_shared<helpers::SubscriptionsManager>(get_node_topics_interface());
        _sub_manager->createSubscription("merged_items", "custom_interfaces/Items", qos_settings);
        _sub_manager->createSubscription("merged_ptcld_filtered", "custom_interfaces/MergedPtcldFiltered", qos_settings);

        qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        _item_select_pub = create_publisher<custom_interfaces::msg::SelectedItemsIds>("item_select", qos_settings);
        _getParameters();

        // TODO: Reference frame should probably be passed in launch or in runtime
        _policy_frame = "left_base_link";
        _ref_frame_position[_policy_frame] = _getReferenceFramePosition(_policy_frame);
        RCLCPP_INFO_STREAM(this->get_logger(), "Reference frame \"" << _policy_frame << "\" read from TF.");

        this->declare_parameter("min_inclusion_ratio", 0.0);
        this->declare_parameter("area_height", 0.0);
        this->declare_parameter("occlusion_offset", 0.0);
        this->declare_parameter("max_occlusion_ratio", 0.0);

        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        status = custom_interfaces::msg::Heartbeat::STOPPED;

        RCLCPP_INFO(this->get_logger(), "...item select init done");
    }

    rclcpp_action::GoalResponse ItemSelectServer::handle_goal(
        const rclcpp_action::GoalUUID &uuid,
        std::shared_ptr<const ItemSelectAction::Goal> goal)
    {
        RCLCPP_INFO(this->get_logger(), "Received goal request with order ");
        _selected_item_label = goal->selected_item_label;
        _selected_area_label = goal->selected_area_label;
        _selected_area_operator = goal->selected_area_operator;
        _selected_policy = goal->selected_policy;
        _distance_parameter_min = goal->distance_parameter_min;
        _policy_values["min_distance"] = goal->distance_parameter_min;
        _distance_parameter_max = goal->distance_parameter_max;
        _policy_values["max_distance"] = goal->distance_parameter_max;
        RCLCPP_INFO(this->get_logger(), "selected item label :  %s", _selected_item_label.c_str());
        RCLCPP_INFO(this->get_logger(), "selected area labels : ");
        for (auto area_label : _selected_area_label)
        {
            RCLCPP_INFO(this->get_logger(), "selected area: %s ", area_label.c_str());
        }
        RCLCPP_INFO(this->get_logger(), "selected area operator: %s", _selected_area_operator.c_str());
        RCLCPP_INFO(this->get_logger(), "selected policy: %s", _selected_policy.c_str());
        RCLCPP_INFO(this->get_logger(), "distance parameter min: %s", _distance_parameter_min.c_str());
        RCLCPP_INFO(this->get_logger(), "distance parameter max: %s", _distance_parameter_max.c_str());
        (void)uuid;
        (void)goal;
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }
    void ItemSelectServer::handle_accepted(const std::shared_ptr<GoalHandleItemSelectAction> goal_handle)
    {
        using namespace std::placeholders;
        // this needs to return quickly to avoid blocking the executor, so spin up a new thread
        std::thread{std::bind(&ItemSelectServer::execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    rclcpp_action::CancelResponse ItemSelectServer::handle_cancel(
        const std::shared_ptr<GoalHandleItemSelectAction> goal_handle)
    {
        RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
        (void)goal_handle;
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    bool ItemSelectServer::_selectItemsInAreaByLabelAndPolicy(const std::string &target_item_label, const std::vector<std::string> &target_area_label, const std::string &selection_policy, const std::string &policy_frame, std::vector<uint16_t> &out_selected_items_ids)
    {
        std::vector<std::vector<pcl::PointXYZ>> target_areas_coords;
        std::vector<uint16_t> target_items_ids_in_target_area;
        this->get_parameter("occlusion_offset", _occlusion_offset);
        this->get_parameter("max_occlusion_ratio", _max_occlusion_ratio);
        std::map<std::string, double> policy_parameters;
        policy_parameters["ref_frame_x"] = _ref_frame_position.at(policy_frame).x();
        policy_parameters["ref_frame_y"] = _ref_frame_position.at(policy_frame).y();
        policy_parameters["ref_frame_z"] = _ref_frame_position.at(policy_frame).z();
        policy_parameters["finger_depth"] = _finger_depth;
        policy_parameters["occlusion_offset"] = _occlusion_offset;
        policy_parameters["max_occlusion_ratio"] = _max_occlusion_ratio;
        policy_parameters["occ_grid_leaf_size"] = _leaf_size;
        policy::PolicyInputData policy_input;
        policy_input.items_msg = *_items_msg;
        policy_input.merged_ptcld_filtered_msg = *_merged_ptcld_filtered_msg;
        // container selection

        for (auto area : target_area_label)
        {
            if ((area.find("bowl") != area.npos || area.find("plate") != area.npos || area.find("cutting_board") != area.npos))
            {
                if (_checkContainers(area, target_item_label, policy_input, target_items_ids_in_target_area))
                {
                    RCLCPP_INFO(this->get_logger(), "Container is probably empty");
                    return false;
                }
                {
                    auto policy = PolicyFactory::PolicySelector(selection_policy, _policy_values, policy_parameters, _visualize);
                    // Skip unsupported policy
                    if (policy == nullptr)
                    {
                        RCLCPP_INFO(this->get_logger(), "Unsupported policy chosed:");
                        return false;
                    }

                    if (policy->filter(target_items_ids_in_target_area, policy_input, out_selected_items_ids))
                        return true;
                    else
                        return false;
                }
            }
        }
        // area selection
        if (_computeTargetAreasCoords(target_area_label, _selected_area_operator, _areas, target_areas_coords))
        {
            // item selection by label in specified area
            if (_getTargetItemsinTargetAreas(target_item_label, target_areas_coords, *_items_msg, target_items_ids_in_target_area))
            {
                // call policy
                auto policy = PolicyFactory::PolicySelector(selection_policy, _policy_values, policy_parameters, _visualize);
                // Skip unsupported policy
                if (policy == nullptr)
                {
                    RCLCPP_INFO(this->get_logger(), "Unsupported policy chosed:");
                    return false;
                }
                if (policy->filter(target_items_ids_in_target_area, policy_input, out_selected_items_ids))
                    return true;
                else
                    return false;
            }
        }
        // else
        return false;
    }
    int ItemSelectServer::_checkContainers(std::string area_name, std::string target_item_label, policy::PolicyInputData &policy_input, std::vector<uint16_t> &target_items_ids_in_target_area)
    {
        int container_id = 0;
        if (_getBrainId(area_name, container_id))
        {
            RCLCPP_ERROR(this->get_logger(), "Container has no ID");
            return 1;
        }
        // std::cout << "container_id " << container_id << std::endl;
        Eigen::Vector3f position;
        // std::cout << "position " << std::endl;
        for (auto item : policy_input.items_msg.items)
        {
            // std::cout << "processed id " << item.id << std::endl;
            if (item.id == container_id)
            { //position of the container
                RCLCPP_INFO(this->get_logger(), "Container has been found");
                if (item.label == "bowl" || item.label == "plate")
                {
                    position = Eigen::Vector3f(item.pose.position.x, item.pose.position.y, item.pose.position.z);
                    json part_description = json::parse(item.item_elements[0].parts_description[0]);
                    float radius = part_description["bigger_circle"]["dims"]["radius"].get<float>();
                    // std::cout << "radius" << radius << std::endl;
                    for (auto item : policy_input.items_msg.items)
                    {
                        if (item.label == target_item_label)
                        {
                            Eigen::Vector3f selected_item_position(item.pose.position.x, item.pose.position.y, item.pose.position.z);
                            float distance = (selected_item_position - position).norm();
                            // std::cout << "Item with label " << item.label << std::endl;
                            // std::cout << "Id inside " << item.id << std::endl;
                            if (distance < radius)
                            {
                                target_items_ids_in_target_area.push_back(item.id);
                                // std::cout << "Item with label inside container  " << item.label << std::endl;
                                // std::cout << "Item Id inside container" << item.id << std::endl;
                            }
                        }
                    }
                } // to one function
                if (item.label == "cutting_board")
                {
                    Eigen::Vector3f position;
                    Eigen::Vector3f cutting_board_dims;
                    Eigen::Quaternionf cutting_board_orientation;
                    std::string container_name;
                    try
                    {
                        // Get pose of cutting board
                        Eigen::Affine3f item_pose;
                        helpers::converters::geometryToEigenAffine(item.pose, item_pose);
                        position = Eigen::Vector3f(item_pose.translation());
                        cutting_board_orientation = Eigen::Quaternionf(item_pose.rotation());

                        // Get board part of cutting board
                        json board_part_description = json::parse(item.item_elements[0].parts_description[0]);
                        if (board_part_description["part_name"] != "board")
                            board_part_description = json::parse(item.item_elements[0].parts_description[1]);
                        cutting_board_dims = Eigen::Vector3f(board_part_description["dims"]["x"], board_part_description["dims"]["y"], board_part_description["dims"]["z"]);

                        container_name = item.label;
                    }
                    catch (const std::out_of_range &e)
                    {
                        std::cout << e.what() << std::endl;
                        return 1;
                    }
                    for (auto item : policy_input.items_msg.items)
                    {
                        if (item.label == target_item_label)
                        {
                            Eigen::Affine3f selected_item_pose;
                            helpers::converters::geometryToEigenAffine(item.pose, selected_item_pose);
                            Eigen::Vector3f selected_item_position(selected_item_pose.translation());
                            if (_checkArea(position, cutting_board_dims, cutting_board_orientation, selected_item_position))
                            {
                                target_items_ids_in_target_area.push_back(item.id);
                            }
                        }
                    }
                }
                if (target_items_ids_in_target_area.size() > 0)
                {
                    return 0;
                }
                else
                {
                    RCLCPP_INFO(this->get_logger(), "Selected area is empty");
                    return 1;
                }
            }
        }
        {
            RCLCPP_ERROR_STREAM(this->get_logger(), "Container with selected ID (" << container_id << ") does not exist");
            return 1;
        }
    }
    bool ItemSelectServer::_computeTargetAreasCoords(const std::vector<std::string> &target_area_label, const std::string &selected_area_operator, std::map<std::string, std::vector<pcl::PointXYZ>> &areas, std::vector<std::vector<pcl::PointXYZ>> &out_target_areas_coords)
    {
        // area selection
        if (selected_area_operator == "inside")
        {
            if (target_area_label[0] == "whole") // min_inter
                out_target_areas_coords.push_back({areas["security_area"][2], areas["security_area"][3]});
            else
                for (auto area : target_area_label)
                    out_target_areas_coords.push_back(areas[area]);
        }
        else if (selected_area_operator == "outside")
        { // iterate over avaiable areas
            std::map<std::string, std::vector<pcl::PointXYZ>> local_areas = _areas;
            for (auto &area_label : target_area_label)
            {
                local_areas.erase(area_label);
            }
            local_areas.erase("security_area");

            if (local_areas.size() == 0)
            {
                RCLCPP_INFO(this->get_logger(), "Area vector is empty(You probably excluded all areas), Goal Failed");
                return false;
            }
            for (auto &area : local_areas)
            {
                out_target_areas_coords.push_back(areas[area.first]);
            }
            // std::transform(local_areas.begin(), local_areas.end(), std::back_inserter(out_target_areas_coords), [areas](auto const area) {
            //   return areas[area.first];
            // });
        }
        else
        {
            RCLCPP_INFO(this->get_logger(), "Wrong area operator: available ones are: include & exclude");
            RCLCPP_INFO(this->get_logger(), "Setting default: include and whole table ");
            out_target_areas_coords.push_back({areas["security_area"][2], areas["security_area"][3]});
        }
        if (out_target_areas_coords.size() != 0)
            return true;
        else
            return false;
    }
    bool ItemSelectServer::_getTargetItemsinTargetAreas(const std::string &target_item_label, std::vector<std::vector<pcl::PointXYZ>> &target_areas_coords, const custom_interfaces::msg::Items &data_msg, std::vector<uint16_t> &out_target_items_ids)
    {
        for (auto &item : data_msg.items)
        {
            if (item.label == target_item_label)
            {
                for (auto &target_area_single : target_areas_coords)
                {
                    if (_findItemInTargetArea(target_area_single, item))
                        out_target_items_ids.push_back(item.id);
                }
            }
        }

        if (out_target_items_ids.size() != 0)
            return true;
        else
            return false;
    }

    void ItemSelectServer::_getParameters()
    {
        try
        {
            // json table_points_json = json::parse(areas_str);
            while (true)
            {
                json robot_parameters = helpers::commons::getParameter("robot");
                if (robot_parameters.empty())
                    continue;

                const std::string working_side = robot_parameters["working_side"];
                auto robot_info = helpers::commons::getRobotInfo(working_side);
                _policy_frame = robot_info.link_names[0];

                json labels_params = helpers::commons::getParameter("labels");
                if (labels_params.empty())
                    continue;

                _labels.clear();
                for (const auto &label : labels_params)
                {
                    json label_data = label;
                    if (label_data.contains("label"))
                    {
                        _labels.push_back(label_data["label"].get<std::string>());
                    }
                }
                json panda_params = helpers::commons::getParameter("gripper_parameters_franka");
                if (panda_params.empty())
                    continue;

                _finger_length = panda_params["finger_length"];
                _finger_depth = panda_params["finger_depth"];

                json areas = helpers::commons::getParameter("areas");
                if (areas.empty())
                    continue;
                json grid_size = helpers::commons::getParameter("get_occupancy_grid");
                if (grid_size.empty())
                    continue;

                _leaf_size = std::stof(grid_size["occ_grid_leaf_size"].dump());

                // Extract coordinates of areas except security because it is described as rectangle around table edge
                _areas.clear();
                for (const auto &area : areas.items())
                {
                    const json area_name = area.key();
                    const json area_data = area.value();
                    if (area_data.contains("min") && area_data.contains("max"))
                    {
                        std::vector<pcl::PointXYZ> area_coords;
                        area_coords.push_back(_jsonToPointXYZ(area_data["min"]));
                        area_coords.push_back(_jsonToPointXYZ(area_data["max"]));
                        _areas.insert({area_name, area_coords});
                    }
                }

                // Security area
                json security_area = helpers::commons::getParameter("security_area");
                if (security_area.empty())
                    continue;

                std::vector<pcl::PointXYZ> security_area_coords;
                security_area_coords.push_back(_jsonToPointXYZ(security_area["min_exter"]));
                security_area_coords.push_back(_jsonToPointXYZ(security_area["max_exter"]));
                security_area_coords.push_back(_jsonToPointXYZ(security_area["min_inter"]));
                security_area_coords.push_back(_jsonToPointXYZ(security_area["max_inter"]));
                _areas.insert({"security_area", security_area_coords});

                // Whole table
                std::vector<pcl::PointXYZ> table_coords;
                table_coords.push_back(_jsonToPointXYZ(areas["table_area"]["min"]));
                table_coords.push_back(_jsonToPointXYZ(areas["table_area"]["max"]));
                _areas.insert({"whole", table_coords});
                break;
            }
        }
        catch (const json::exception &e)
        {
            RCLCPP_FATAL_STREAM(get_logger(), "Exception with ID: " << e.id << "; message: " << e.what());
            rclcpp::shutdown();
        }
    }

    bool ItemSelectServer::_findItemInTargetArea(const std::vector<pcl::PointXYZ> &target_area_coords, const custom_interfaces::msg::Item &target_item_by_label)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_element_ptcld(new pcl::PointCloud<pcl::PointXYZ>);
        for (auto item_element : target_item_by_label.item_elements)
        {
            if (item_element.merged_ptcld.data.size() != 0)
            {
                helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(item_element.merged_ptcld, item_element_ptcld);
                *item_ptcld += *item_element_ptcld;
                item_element_ptcld->clear();
            }
        }
        helpers::vision::passThroughFilter(item_ptcld, "z", -0.005, 0.005, true);

        this->get_parameter("area_height", _area_height);
        Eigen::Vector4f areaMinPoint;
        areaMinPoint[0] = target_area_coords[0].x; // define minimum point x
        areaMinPoint[1] = target_area_coords[0].y; // define minimum point y
        areaMinPoint[2] = 0.0;                     // define minimum point z
        Eigen::Vector4f areaMaxPoint;
        areaMaxPoint[0] = target_area_coords[1].x; // define max point x
        areaMaxPoint[1] = target_area_coords[1].y; // define max point y
        areaMaxPoint[2] = _area_height;            // define max point z
        pcl::PointCloud<pcl::PointXYZ>::Ptr item_ptcld_in_area(new pcl::PointCloud<pcl::PointXYZ>);

        pcl::CropBox<pcl::PointXYZ> cropFilter;
        cropFilter.setInputCloud(item_ptcld);
        cropFilter.setMin(areaMinPoint);
        cropFilter.setMax(areaMaxPoint);
        cropFilter.filter(*item_ptcld_in_area);
        if (_visualize)
        {
            pcl::visualization::PCLVisualizer viewer;
            viewer.addPointCloud(item_ptcld_in_area, "ItemInArea");
            viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 1.0, 1.0, 0.0, "ItemInArea");
            viewer.addCube(target_area_coords[0].x, target_area_coords[1].x, target_area_coords[0].y, target_area_coords[1].y, -0.03f, -0.02f, 0.0, 1.0, 1.0, "Area", 0);
            viewer.addCoordinateSystem(1.0);
            viewer.spin();
        }
        this->get_parameter("min_inclusion_ratio", _min_inclusion_ratio);
        if (item_ptcld_in_area->points.size() >= floor(_min_inclusion_ratio * item_ptcld->points.size()))
            return true;
        else
            return false;
    }
    pcl::PointXYZ ItemSelectServer::_jsonToPointXYZ(const json &data)
    {
        auto get_coordinate = [this](const json &point_coords, const std::string &coord_name) -> float
        {
            if (point_coords.contains(coord_name))
                return point_coords[coord_name];
            else
                return 0;
        };
        pcl::PointXYZ point;
        point.x = get_coordinate(data, "x");
        point.y = get_coordinate(data, "y");
        point.z = get_coordinate(data, "z");
        return point;
    }

    int ItemSelectServer::_validateInput()
    {
        if (!_items_msg || !_merged_ptcld_filtered_msg)
        {
            RCLCPP_WARN(get_logger(), "Module has not received data yet.");
            return 1;
        }
        if (_items_msg->header.stamp == builtin_interfaces::msg::Time() ||
            _merged_ptcld_filtered_msg->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_WARN(get_logger(), "Invalid input message header.");
            return 1;
        }
        return 0;
    }

    void ItemSelectServer::execute(const std::shared_ptr<GoalHandleItemSelectAction> goal_handle)
    {
        auto result = std::make_shared<ItemSelectAction::Result>();
        if (status != custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN(this->get_logger(), "Node is not in running state");
            goal_handle->abort(result);
            return;
        }
        RCLCPP_INFO(this->get_logger(), "Item Select called");
        _items_msg = _sub_manager->getData<custom_interfaces::msg::Items>("merged_items");
        _merged_ptcld_filtered_msg = _sub_manager->getData<custom_interfaces::msg::MergedPtcldFiltered>("merged_ptcld_filtered");
        auto feedback = std::make_shared<ItemSelectAction::Feedback>();
        if (_validateInput())
        {
            RCLCPP_ERROR(get_logger(), "Invalid input data. Goal failed");
            auto result = std::make_shared<ItemSelectAction::Result>();
            result->selected_items_ids = std::vector<int32_t>();
            _item_select_pub->publish(custom_interfaces::msg::SelectedItemsIds());
            goal_handle->abort(result);
            return;
        }
        // check input item label
        if (!_checkTargetItemLabelArg(_labels, _selected_item_label))
        {
            auto result = std::make_shared<ItemSelectAction::Result>();
            result->selected_items_ids = std::vector<int32_t>();
            _item_select_pub->publish(custom_interfaces::msg::SelectedItemsIds());
            goal_handle->abort(result);
            return;
        }
        // check input area label
        if (!_checkTargetAreasLabelsArg(_areas, _selected_area_label))
        {
            auto result = std::make_shared<ItemSelectAction::Result>();
            result->selected_items_ids = std::vector<int32_t>();
            _item_select_pub->publish(custom_interfaces::msg::SelectedItemsIds());
            goal_handle->abort(result);
            return;
        }
        auto item_iter = std::find_if(_items_msg->items.begin(), _items_msg->items.end(),
                                      [this](custom_interfaces::msg::Item item)
                                      {
                                          return item.label == _selected_item_label;
                                      });

        if (item_iter != _items_msg->items.end()) // item in the scene
        {

            RCLCPP_INFO(this->get_logger(), "Executing goal");
            const auto goal = goal_handle->get_goal();
            std::vector<uint16_t> selected_items_ids;
            if (!_selectItemsInAreaByLabelAndPolicy(_selected_item_label, _selected_area_label, _selected_policy, _policy_frame, selected_items_ids))
            {
                RCLCPP_WARN(this->get_logger(), "Did not find the item with label and policy");
                auto result = std::make_shared<ItemSelectAction::Result>();
                result->selected_items_ids = std::vector<int32_t>();
                _item_select_pub->publish(custom_interfaces::msg::SelectedItemsIds());
                goal_handle->abort(result);
                RCLCPP_ERROR(this->get_logger(), "Goal Failed");
                return;
            }
            else // item in selected area
            {
                RCLCPP_INFO(this->get_logger(), "Found the item with specified label and policy inside the target area");
                custom_interfaces::msg::SelectedItemsIds item_select_msg;
                if (selected_items_ids.size() != 0)
                {
                    std::vector<int> selected_items_ids_casted(selected_items_ids.begin(), selected_items_ids.end());
                    item_select_msg.selected_items_ids = selected_items_ids_casted;
                }
                item_select_msg.header.frame_id = "world";
                item_select_msg.header.stamp.sec = this->now().seconds();
                item_select_msg.header.stamp.nanosec = this->now().nanoseconds();
                // _fillItemSelectMsgByOccGridMsg(*_occupancy_grid_data, item_select_msg);
                // Check if goal is done
                if (rclcpp::ok())
                {
                    auto result = std::make_shared<ItemSelectAction::Result>();
                    result->selected_items_ids = item_select_msg.selected_items_ids;
                    _item_select_pub->publish(item_select_msg);
                    goal_handle->succeed(result);
                    RCLCPP_INFO(this->get_logger(), "Goal succeeded");
                    return;
                }
            }
        }
        else // item not in the scene
        {
            RCLCPP_WARN(this->get_logger(), " Selected Item '%s' is not in the scene", _selected_item_label.c_str());
            if (rclcpp::ok())
            {
                auto result = std::make_shared<ItemSelectAction::Result>();
                result->selected_items_ids = std::vector<int32_t>();
                _item_select_pub->publish(custom_interfaces::msg::SelectedItemsIds());
                goal_handle->abort(result);
                RCLCPP_ERROR(this->get_logger(), "Goal Failed");
                return;
            }
        }
    }

    // void ItemSelectServer::_occupancyGridCallback(const custom_interfaces::msg::OccupancyGrid::SharedPtr occupancy_grid_msg)
    // {
    //   RCLCPP_INFO(this->get_logger(), "Estimate shape msg recived!");
    //   _occupancy_grid_data = occupancy_grid_msg;
    // }
    // void ItemSelectServer::_fillItemSelectMsgByOccGridMsg(custom_interfaces::msg::OccupancyGrid &occupancy_grid_data, custom_interfaces::msg::ItemSelect &out_item_select_msg)
    // {
    //   out_item_select_msg.header.stamp.sec = this->now().seconds();
    //   out_item_select_msg.header.stamp.nanosec = this->now().nanoseconds();
    //   out_item_select_msg.cam1_rgb = occupancy_grid_data.cam1_rgb;
    //   out_item_select_msg.cam2_rgb = occupancy_grid_data.cam2_rgb;
    //   out_item_select_msg.cam1_depth = occupancy_grid_data.cam1_depth;
    //   out_item_select_msg.cam2_depth = occupancy_grid_data.cam2_depth;
    //   out_item_select_msg.merged_ptcld_filtered = occupancy_grid_data.merged_ptcld_filtered;
    //   out_item_select_msg.cam1_masks = occupancy_grid_data.cam1_masks;
    //   out_item_select_msg.cam1_labels = occupancy_grid_data.cam1_labels;
    //   out_item_select_msg.cam2_masks = occupancy_grid_data.cam2_masks;
    //   out_item_select_msg.cam2_labels = occupancy_grid_data.cam2_labels;
    //   out_item_select_msg.items = occupancy_grid_data.items;
    //   out_item_select_msg.occupancy_grid = occupancy_grid_data.occupancy_grid;
    // }
    bool ItemSelectServer::_checkTargetItemLabelArg(std::vector<std::string> &labels, std::string &target_item_label)
    {

        if (std::find(labels.begin(), labels.end(), target_item_label) != labels.end())
        {
            RCLCPP_INFO(this->get_logger(), "Selected item label is %s", target_item_label.c_str());
            return true;
        }
        else
        {
            RCLCPP_WARN(this->get_logger(), " Wrong item label %s, Available labels are: ", target_item_label.c_str());
            for (auto &label : labels)
                RCLCPP_WARN(this->get_logger(), " ,", label.c_str());
            RCLCPP_ERROR(this->get_logger(), "Wrong item label, Goal Failed");
            return false;
        }
    }
    bool ItemSelectServer::_checkTargetAreasLabelsArg(std::map<std::string, std::vector<pcl::PointXYZ>> &areas, std::vector<std::string> &target_areas_labels)
    {

        for (auto &area_selected : target_areas_labels)
        {
            if ((area_selected.find("bowl") != area_selected.npos || area_selected.find("plate") != area_selected.npos || area_selected.find("cutting_board") != area_selected.npos))
            {
                RCLCPP_INFO(this->get_logger(), " Selected container label is %s", area_selected.c_str());
                return true;
            }
            if (areas.find(area_selected) != areas.end())
                RCLCPP_INFO(this->get_logger(), " Selected area label is %s", area_selected.c_str());
            else
            {
                RCLCPP_WARN(this->get_logger(), " Wrong area label %s, Available labels are: ", area_selected.c_str());
                RCLCPP_ERROR(this->get_logger(), "Goal Failed");
                return false;
            }
        }
        return true;
    }
    int ItemSelectServer::_getBrainId(std::string container_brain_name, int &container_id)
    {
        std::vector<size_t> positions;
        size_t pos = container_brain_name.find("_");
        container_id = 0;
        while (pos != std::string::npos)
        {
            positions.push_back(pos);
            pos = container_brain_name.find("_", pos + 1);
        }

        if (pos == 0)
        {
            container_id = 0;
            RCLCPP_INFO(this->get_logger(), "Container with name %s does not exist in coppelia brain", container_brain_name);
            return 1;
        }
        else
        {
            for (auto position : positions)
            {
                std::string item_id = container_brain_name.substr(position + 1);
                std::istringstream(item_id) >> container_id;
                if (container_id == 0)
                    continue;
                return 0;
            }
        }
        RCLCPP_INFO(this->get_logger(), "Container name contains invalid or no id ");
        return 1;
    }

    int ItemSelectServer::_checkArea(Eigen::Vector3f position, Eigen::Vector3f dimensions, Eigen::Quaternionf orientation, Eigen::Vector3f item_position)
    {
        // pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_item_position_cloud(new pcl::PointCloud<pcl::PointXYZ>);
        // pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_desk_cloud(new pcl::PointCloud<pcl::PointXYZ>);
        // pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_item_returned(new pcl::PointCloud<pcl::PointXYZ>);
        Eigen::Vector4f min(-dimensions.z() / 2, -dimensions.y() / 2, 0, 1); // TODO:fix height
        Eigen::Vector4f max(dimensions.z() / 2, dimensions.y() / 2, 1, 1);
        // std::cout << "item position global  " << item_position << std::endl;
        Eigen::Matrix3f rotation_matrix;
        orientation.inverse();
        rotation_matrix = orientation.toRotationMatrix();
        Eigen::Vector3f item_position_local = item_position - position;
        item_position_local = rotation_matrix * item_position_local;

        // std::cout << "item position local " << item_position_local << std::endl;
        if (item_position_local.x() > min.x() && item_position_local.x() < max.x() && item_position_local.y() > min.y() && item_position_local.y() < max.y())
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    Eigen::Vector3f ItemSelectServer::_getReferenceFramePosition(const std::string &reference_frame_name)
    {
        std::optional<Eigen::Affine3f> reference_frame;
        while (true)
        {
            reference_frame = helpers::vision::getTransformAffine("world", reference_frame_name);
            if (reference_frame)
                break;
            RCLCPP_WARN_STREAM_THROTTLE(get_logger(), *get_clock(), 1000, "Cannot obtain transform to \"" + reference_frame_name + "\" trying again...");
        }
        Eigen::Affine3f reference_frame_aff = *reference_frame;
        return Eigen::Vector3f(reference_frame_aff.translation());
    }

} // namespace item_select
#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(item_select::ItemSelectServer)