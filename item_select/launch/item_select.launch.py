import launch
import sys
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode


def generate_launch_description():
    container = ComposableNodeContainer(
        name='item_select_container',
        namespace='',
        package='rclcpp_components',
        executable='component_container',
        composable_node_descriptions=[
                ComposableNode(
                    package='item_select',
                    plugin='item_select::ItemSelectServer',
                    name='item_select',
                    parameters=[
                         {"min_inclusion_ratio": 0.5},  # percent
                         {"area_height": 1.0},  # m
                         {"occlusion_offset": 0.02},  # m,
                         {"max_occlusion_ratio": 0.6}  # percent
                    ]
                )
        ],
        output='screen',
        # prefix=['xterm -e gdb -ex run --args'],

    )

    return launch.LaunchDescription([container])
