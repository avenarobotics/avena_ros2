#include "save_to_bag/save_to_bag.hpp"

namespace save_to_bag
{
    SaveToBag::SaveToBag(const rclcpp::NodeOptions &options)
        : Node("save_to_bag_node", options)
    {

        _camera_info_rgb_1_topic = "/camera_1/depth_to_rgb/camera_info";
        _camera_info_rgb_2_topic = "/camera_2/depth_to_rgb/camera_info";
        _cameras_data_topic = "/cameras_data";

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        _subscriptions_manager = std::make_shared<helpers::SubscriptionsManager>(get_node_topics_interface());
        _subscriptions_manager->createSubscription(_camera_info_rgb_1_topic, "sensor_msgs/CameraInfo", qos_settings);
        _subscriptions_manager->createSubscription(_camera_info_rgb_2_topic, "sensor_msgs/CameraInfo", qos_settings);

        _subscriptions_manager->createSubscription(_cameras_data_topic, "custom_interfaces/msg/CamerasData", qos_settings);

        this->_action_server = rclcpp_action::create_server<SaveToBagAction>(
            this,
            "save_to_bag",
            std::bind(&SaveToBag::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&SaveToBag::_handleCancel, this, std::placeholders::_1),
            std::bind(&SaveToBag::_handleAccepted, this, std::placeholders::_1));
    }

    rclcpp_action::GoalResponse SaveToBag::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, std::shared_ptr<const SaveToBagAction::Goal> /*goal*/)
    {
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse SaveToBag::_handleCancel(const std::shared_ptr<GoalHandleSaveToBag> /*goal_handle*/)
    {
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void SaveToBag::_handleAccepted(const std::shared_ptr<GoalHandleSaveToBag> goal_handle)
    {
        std::thread{std::bind(&SaveToBag::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    void SaveToBag::_execute(const std::shared_ptr<GoalHandleSaveToBag> goal_handle)
    {
        RCLCPP_INFO(this->get_logger(), "Executing goal");
        auto result = std::make_shared<SaveToBagAction::Result>();
        
        
        rcutils_time_point_value_t time = now().nanoseconds();
        auto save_data = [time](std::unique_ptr<rosbag2_cpp::Writer> &writer, const void *ros_message, std::string topic_name, std::string topic_type) {
            rclcpp::SerializedMessage serialized_msg;
            if (topic_type == "sensor_msgs/msg/Image")
            {
                rclcpp::Serialization<sensor_msgs::msg::Image> serialization;
                serialization.serialize_message(ros_message, &serialized_msg);
            }
            else if (topic_type == "sensor_msgs/msg/PointCloud2")
            {
                rclcpp::Serialization<sensor_msgs::msg::PointCloud2> serialization;
                serialization.serialize_message(ros_message, &serialized_msg);
            }
            else if (topic_type == "sensor_msgs/msg/CameraInfo")
            {
                rclcpp::Serialization<sensor_msgs::msg::CameraInfo> serialization;
                serialization.serialize_message(ros_message, &serialized_msg);
            }
            else if (topic_type == "tf2_msgs/msg/TFMessage")
            {
                rclcpp::Serialization<tf2_msgs::msg::TFMessage> serialization;
                serialization.serialize_message(ros_message, &serialized_msg);
            }
            else if (topic_type == "custom_interfaces/msg/CamerasData")
            {
                rclcpp::Serialization<custom_interfaces::msg::CamerasData> serialization;
                serialization.serialize_message(ros_message, &serialized_msg);
            }

            auto bag_message = std::make_shared<rosbag2_storage::SerializedBagMessage>();
            rosbag2_storage::TopicMetadata tm;
            tm.name = topic_name;
            tm.type = topic_type;
            bag_message->topic_name = tm.name;
            bag_message->time_stamp = time;
            bag_message->serialized_data = std::shared_ptr<rcutils_uint8_array_t>(
                &serialized_msg.get_rcl_serialized_message(), [](rcutils_uint8_array_t * /* data */) {});
            tm.serialization_format = "cdr";
            tm.offered_qos_profiles = "- history: 1\n  depth: 1\n  reliability: 1\n  durability: 2\n  avoid_ros_namespace_conventions: false";

            writer->create_topic(tm);
            writer->write(bag_message);
        };

        _camera_info_rgb_1 = _subscriptions_manager->getData<sensor_msgs::msg::CameraInfo>(_camera_info_rgb_1_topic);
        _camera_info_rgb_2 = _subscriptions_manager->getData<sensor_msgs::msg::CameraInfo>(_camera_info_rgb_2_topic);
        _cam_data = _subscriptions_manager->getData<custom_interfaces::msg::CamerasData>(_cameras_data_topic);


        std::unique_ptr<rosbag2_storage::StorageFactoryInterface> storage_factory =
            std::make_unique<rosbag2_storage::StorageFactory>();
        std::shared_ptr<rosbag2_cpp::SerializationFormatConverterFactoryInterface> converter_factory =
            std::make_shared<rosbag2_cpp::SerializationFormatConverterFactory>();
        std::unique_ptr<rosbag2_storage::MetadataIo> metadata_io =
            std::make_unique<rosbag2_storage::MetadataIo>();
        rosbag2_cpp::StorageOptions storage_options = rosbag2_cpp::StorageOptions{};
        storage_options.storage_id = "sqlite3";


        auto rosbag_directory = rcpputils::fs::path(goal_handle->get_goal()->filename);
        auto status = rcpputils::fs::create_directories(rosbag_directory);
        storage_options.uri = rosbag_directory.string();

        storage_factory->open_read_write(goal_handle->get_goal()->filename + std::to_string(time), storage_options.storage_id);

        auto sequential_writer = std::make_unique<rosbag2_cpp::writers::SequentialWriter>(
            std::move(storage_factory), converter_factory, std::move(metadata_io));

        std::unique_ptr<rosbag2_cpp::Writer> writer = std::make_unique<rosbag2_cpp::Writer>(std::move(sequential_writer));

        std::string rmw_format = "rmw_format";

        writer->open(storage_options, {rmw_format, rmw_format});


   
        if (_camera_info_rgb_1)
                save_data(writer, &(*_camera_info_rgb_1), _camera_info_rgb_1_topic, "sensor_msgs/msg/CameraInfo");
        if (_camera_info_rgb_2)
                save_data(writer, &(*_camera_info_rgb_2), _camera_info_rgb_2_topic, "sensor_msgs/msg/CameraInfo");
        if (_cam_data)
                save_data(writer, &(*_cam_data), "cameras_data", "custom_interfaces/msg/CamerasData");

 


        auto remove_file_dir = rcpputils::fs::path(goal_handle->get_goal()->filename + std::to_string(time) + ".db3");
        rcpputils::fs::remove(remove_file_dir);

        if (rclcpp::ok())
        {
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
        }
    }

    std::string format_storage_uri(const std::string &base_folder, uint64_t storage_count)
    {
        // Right now `base_folder_` is always just the folder name for where to install the bagfile.
        // The name of the folder needs to be queried in case
        // SequentialWriter is opened with a relative path.
        // rcpputils::fs::remove()

        std::stringstream storage_file_name;
        storage_file_name << rcpputils::fs::path(base_folder).filename().string() << "_" << storage_count;

        return (rcpputils::fs::path(base_folder) / storage_file_name.str()).string();
    }



    std::unique_ptr<rosbag2_cpp::Writer> SaveToBag::_get_writer(rcutils_time_point_value_t time)
    {
        std::unique_ptr<rosbag2_storage::StorageFactoryInterface> storage_factory =
            std::make_unique<rosbag2_storage::StorageFactory>();
        std::shared_ptr<rosbag2_cpp::SerializationFormatConverterFactoryInterface> converter_factory =
            std::make_shared<rosbag2_cpp::SerializationFormatConverterFactory>();
        std::unique_ptr<rosbag2_storage::MetadataIo> metadata_io =
            std::make_unique<rosbag2_storage::MetadataIo>();
        rosbag2_cpp::StorageOptions storage_options = rosbag2_cpp::StorageOptions{};
        storage_options.storage_id = "sqlite3";
        storage_options.uri = "/bag";

        storage_factory->open_read_write(storage_options.uri, storage_options.storage_id);
        auto sequential_writer = std::make_unique<rosbag2_cpp::writers::SequentialWriter>(
            std::move(storage_factory), converter_factory, std::move(metadata_io));

        std::unique_ptr<rosbag2_cpp::Writer> writer = std::make_unique<rosbag2_cpp::Writer>(std::move(sequential_writer));

        std::string rmw_format = "rmw_format";
        writer->open(storage_options, {rmw_format, rmw_format});
        return writer;
    }

} // namespace

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(save_to_bag::SaveToBag)
