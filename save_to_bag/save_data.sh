#!/bin/bash

FILE_NAME=$1


arrIN=(${FILE_NAME//./ })
echo ${arrIN[0]}  
TIMESTAMP=$(date '+%Y%m%d_%H%M%S')



ros2 action send_goal /save_to_bag custom_interfaces/action/SaveBag "{filename: $(pwd)/${arrIN[0]}}"
mv $(pwd)/$FILE_NAME $(pwd)/${arrIN[0]}/$FILE_NAME
tar -cvf ${arrIN[0]}_$TIMESTAMP.tgz ${arrIN[0]}/ 
rm -rf ${arrIN[0]}/
rm $FILE_NAME
