#ifndef SAVE_TO_BAG_COMPONENT_HPP
#define SAVE_TO_BAG_COMPONENT_HPP

// ___CPP___
#include <functional>
#include <memory>
#include <string>
#include <tuple>

// __ROS__
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include <rclcpp/parameter.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include <tf2_msgs/msg/tf_message.hpp>
// __ROSBAG__


#include <rosbag2_cpp/writers/sequential_writer.hpp>
#include <rosbag2_cpp/writer.hpp>
#include <rosbag2_storage/bag_metadata.hpp>
#include <rosbag2_storage/topic_metadata.hpp>
#include <rcpputils/filesystem_helper.hpp>
#include <rosbag2_cpp/writers/sequential_writer.hpp>
#include <rosbag2_cpp/converter.hpp>
#include "rosbag2_storage/metadata_io.hpp"
#include "rosbag2_storage/storage_factory.hpp"
#include "rosbag2_storage/storage_factory_interface.hpp"
#include "rosbag2_storage/storage_interfaces/read_write_interface.hpp"
#include "rclcpp/serialization.hpp"
#include "rclcpp/serialized_message.hpp"


// ___AVENA___
#include "save_to_bag/visibility_control.h"
#include "custom_interfaces/action/simple_action.hpp"
#include "custom_interfaces/action/save_bag.hpp"
#include "custom_interfaces/msg/change_detect.hpp"
#include "custom_interfaces/msg/rgb_images.hpp"
#include "custom_interfaces/msg/depth_images.hpp"
#include "custom_interfaces/msg/merged_ptcld_filtered.hpp"
#include "custom_interfaces/msg/scene_debug_data.hpp"
#include "custom_interfaces/msg/cameras_data.hpp"
#include "helpers_commons/helpers_commons.hpp"


namespace save_to_bag
{
  // using namespace custom_interfaces::msg; // usage only in this namespace, so not a big problem

  class SaveToBag : public rclcpp::Node
  {
  public:
    explicit SaveToBag(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    using SaveToBagAction = custom_interfaces::action::SaveBag;
    using GoalHandleSaveToBag = rclcpp_action::ServerGoalHandle<SaveToBagAction>;

  private:
    helpers::Watchdog::SharedPtr _watchdog;


    //ROS
    rclcpp_action::Server<SaveToBagAction>::SharedPtr _action_server;
    rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, std::shared_ptr<const SaveToBagAction::Goal> goal);
    rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandleSaveToBag> goal_handle);
    void _handleAccepted(const std::shared_ptr<GoalHandleSaveToBag> goal_handle);
    void _execute(const std::shared_ptr<GoalHandleSaveToBag> goal_handle);

    std::unique_ptr<rosbag2_cpp::Writer> _get_writer(rcutils_time_point_value_t time);


    helpers::SubscriptionsManager::SharedPtr _subscriptions_manager;
    sensor_msgs::msg::CameraInfo::SharedPtr _camera_info_rgb_1;
    sensor_msgs::msg::CameraInfo::SharedPtr _camera_info_rgb_2;
    custom_interfaces::msg::CamerasData::SharedPtr _cam_data;



    std::string _camera_info_rgb_1_topic;
    std::string _camera_info_rgb_2_topic;

    std::string _cameras_data_topic;
  };

} // namespace 

#endif

