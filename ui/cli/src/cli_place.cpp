#include "cli/cli_place.hpp"

namespace cli
{

    CliPlaceClient::CliPlaceClient(std::string action_name, const rclcpp::NodeOptions &options)
        : Node("cli_place", options), _action_name(action_name)
    {
        _execution_timer = std::make_shared<Timer>("Action client - full time");
        RCLCPP_INFO(this->get_logger(), "Starting CLI action client.");
        // _cli_options =options.arguments();
    }

    int CliPlaceClient::sendGoal(std::vector<std::string> arguments)
    {
        _action_client = rclcpp_action::create_client<PlaceAction>(this, _action_name);
        std::cout << "item_select arguments passed:   " << arguments.size() << std::endl;
        if ((arguments.size() < 4) || (arguments[0] == "-h") || (arguments[0] == "--help"))
        {
            std::cout << "Insufficient list of parameters passed :\n";
            std::cout << "To use Place module you need to pass:\n";
            std::cout << "Search_shift   Selected_area   Area_operator  Place_positions_required   \n";
            std::cout << "Areas          : operating_area,item_area,tools_area \n";
            std::cout << "Area_operator  : include, exclude \n";
            std::cout << "Place_positions_required specify the number of required place poses from Coppelia IK for targeted area \n";
            std::cout << "you can pass multiple areas separated by comma\n";
            return 0;
        }
        for (auto argument : arguments)
        {
            std::cout << "item_select option:   " << argument << std::endl;
        }
        RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
        Timer timer("Action execution");

        if (_waitForServer())
            return 1;

        RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _action_name << "\" action.");

        auto goal_msg = PlaceAction::Goal();
        goal_msg.search_shift = arguments[0];
        //passing area array
        std::stringstream ss(arguments[1]);
        if (arguments[1].find(","))
        {
            std::stringstream ss(arguments[1]);
            while (ss.good())
            {
                std::string substr;
                getline(ss, substr, ',');
                
                goal_msg.selected_area_label.push_back(substr);
            }
        }
        goal_msg.selected_area_operator = arguments[2];
        goal_msg.place_positions_required = arguments[3];
        auto goal_future = _action_client->async_send_goal(goal_msg);

        rclcpp::FutureReturnCode goal_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), goal_future, 10s);
        if (goal_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            RCLCPP_INFO(this->get_logger(), "Sending goal successfully");
            if (_getActionResult(goal_future))
                return 1;
        }
        else if (goal_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal interrupted");
            return 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal timeout");
            return 1;
        }

        return 0;
    }

    int CliPlaceClient::_waitForServer()
    {
        size_t wait_time = 5;
        size_t seconds_waited = 0;
        while (seconds_waited < wait_time)
        {
            if (!_action_client->wait_for_action_server(1s))
            {
                RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available after waiting for " << ++seconds_waited << " seconds.");
                if (!rclcpp::ok())
                {
                    RCLCPP_ERROR(this->get_logger(), "Problem with ROS. Exiting...");
                    throw std::runtime_error("Problem with ROS. Exiting...");
                }
            }
            else
            {
                RCLCPP_INFO(this->get_logger(), "Action server avaiable.");
                break;
            }
        }
        if (seconds_waited == wait_time)
        {
            RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available. Exiting...");
            return 1;
        }
        return 0;
    }

    int CliPlaceClient::_getActionResult(std::shared_future<std::shared_ptr<GoalHandleItemSelectAction>> goal_future)
    {
        int exit_code = 0;
        auto result_future = _action_client->async_get_result(goal_future.get());
        rclcpp::FutureReturnCode result_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), result_future, 10s);
        if (result_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            switch (result_future.get().code)
            {
            case rclcpp_action::ResultCode::SUCCEEDED:
                RCLCPP_INFO(this->get_logger(), "Goal succeeded.");
                break;
            case rclcpp_action::ResultCode::ABORTED:
                RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::CANCELED:
                RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::UNKNOWN:
                RCLCPP_ERROR(this->get_logger(), "Unknown result code");
                exit_code = 1;
                break;
            default:
                RCLCPP_ERROR(this->get_logger(), "Other result code");
                exit_code = 1;
                break;
            }
        }
        else if (result_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Result interrupted");
            exit_code = 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Result timeout");
            exit_code = 1;
        }
        return exit_code;
    }

} // namespace cli

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::vector<std::string> arguments(argv + 1, argv + argc);
    int exit_code = 0;
    try
    {
        auto item_select_client = std::make_shared<cli::CliPlaceClient>("place");
        exit_code = item_select_client->sendGoal(arguments);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        rclcpp::shutdown();
        return 1;
    }

    return exit_code;
}