#include "cli/cli_grasp_client.hpp"
namespace cli
{

    GraspActionClient::GraspActionClient(std::string action_name, const rclcpp::NodeOptions &options)
        : Node("grasp_client", options), _action_name(action_name)
    {
        _execution_timer = std::make_shared<Timer>("Action client - full time");
        RCLCPP_INFO(this->get_logger(), "Starting CLI Grasp action client.");
        // _cli_options =options.arguments();
    }
    int GraspActionClient::sendGoal(int32_t selected_item_id)
    {   
        _action_client = rclcpp_action::create_client<GraspAction>(this, _action_name);
        RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
        Timer timer("Action execution");
        if (_waitForServer())
            return 1;
        RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _action_name << "\" action.");
        auto goal_msg = GraspAction::Goal();
        goal_msg.selected_items_id=selected_item_id;
        auto goal_future = _action_client->async_send_goal(goal_msg);
        rclcpp::FutureReturnCode goal_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), goal_future, 10s);
        if (goal_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            RCLCPP_INFO(this->get_logger(), "Sending goal successfully");
            if (_getActionResult(goal_future))
                return 1;
        }
        else if (goal_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal interrupted");
            return 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal timeout");
            return 1;
        }

        return 0;
    }

    int GraspActionClient::_waitForServer()
    {
        size_t wait_time = 5;
        size_t seconds_waited = 0;
        while (seconds_waited < wait_time)
        {
            if (!_action_client->wait_for_action_server(1s))
            {
                RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available after waiting for " << ++seconds_waited << " seconds.");
                if (!rclcpp::ok())
                {
                    RCLCPP_ERROR(this->get_logger(), "Problem with ROS. Exiting...");
                    throw std::runtime_error("Problem with ROS. Exiting...");
                }
            }
            else
            {
                RCLCPP_INFO(this->get_logger(), "Action server avaiable.");
                break;
            }
        }
        if (seconds_waited == wait_time)
        {
            RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available. Exiting...");
            return 1;
        }
        return 0;
    }

    int GraspActionClient::_getActionResult(std::shared_future<std::shared_ptr<GoalHandleGraspAction>> goal_future)
    {
        int exit_code = 0;
        auto result_future = _action_client->async_get_result(goal_future.get());
        rclcpp::FutureReturnCode result_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), result_future, 10s);
        if (result_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            switch (result_future.get().code)
            {
            case rclcpp_action::ResultCode::SUCCEEDED:
            {

                // std::vector<int32_t> selected_item_ids = result_future.get().result->selected_items_ids;
                // for (auto selected_item_id : selected_item_ids)
                //     std::cout << "selected_item_id: " << selected_item_id << std::endl;
                RCLCPP_INFO(this->get_logger(), "Goal succeeded.");
                break;
            }
            case rclcpp_action::ResultCode::ABORTED:
                RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::CANCELED:
                RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::UNKNOWN:
                RCLCPP_ERROR(this->get_logger(), "Unknown result code");
                exit_code = 1;
                break;
            default:
                RCLCPP_ERROR(this->get_logger(), "Other result code");
                exit_code = 1;
                break;
            }
        }
        else if (result_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Result interrupted");
            exit_code = 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Result timeout");
            exit_code = 1;
        }
        return exit_code;
    }

} // namespace cli
    int main(int argc, char **argv)
    {
        int exit_code = 0;
        int32_t selected_item_id
        try
        {
            auto grasp_action_client = std::make_shared<cli::GraspActionClient>("grasp_client");
            exit_code = grasp_action_client->sendGoal(selected_item_id);
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
            rclcpp::shutdown();
            return 1;
        }

        return exit_code;
    }