#include "cli/cli_validate_estimate_shape.hpp"

namespace cli
{
    CliValidateEstiamteShape::CliValidateEstiamteShape(std::string action_name, const rclcpp::NodeOptions &options)
        : Node("cli_validate_estimate_shape", options), _action_name(action_name)
    {
        _execution_timer = std::make_shared<Timer>("Action client - full time");
        RCLCPP_INFO(this->get_logger(), "Starting CLI action client.");
    }

    int CliValidateEstiamteShape::sendGoal(float position, float orientation, float size, std::string real_scene_decscription_path)
    {
        _action_client = rclcpp_action::create_client<ValidateAction>(this, _action_name);
        
        std::cout << "Max position error: " << position << "\n";
        std::cout << "Max orientation error: " << orientation << "\n";
        std::cout << "Max size error: " << size << "\n";
        std::cout << "Scene description: " << real_scene_decscription_path << "\n";

        RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
        Timer timer("Action execution");

        if (_waitForServer())
            return 1;

        RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _action_name << "\" action.");
        auto goal_msg = ValidateAction::Goal();
        
        goal_msg.position = position;
        goal_msg.orientation = orientation;
        goal_msg.size = size;
        goal_msg.real_scene_decscription_path = real_scene_decscription_path;

        auto goal_future = _action_client->async_send_goal(goal_msg);

        rclcpp::FutureReturnCode goal_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), goal_future, 10s);
        if (goal_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            RCLCPP_INFO(this->get_logger(), "Sending goal successfully");
            if (_getActionResult(goal_future))
                return 1;
        }
        else if (goal_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal interrupted");
            return 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal timeout");
            return 1;
        }

        return 0;
    }

    int CliValidateEstiamteShape::_waitForServer()
    {
        size_t wait_time = 5;
        size_t seconds_waited = 0;
        while (seconds_waited < wait_time)
        {
            if (!_action_client->wait_for_action_server(1s))
            {
                RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available after waiting for " << ++seconds_waited << " seconds.");
                if (!rclcpp::ok())
                {
                    RCLCPP_ERROR(this->get_logger(), "Problem with ROS. Exiting...");
                    throw std::runtime_error("Problem with ROS. Exiting...");
                }
            }
            else
            {
                RCLCPP_INFO(this->get_logger(), "Action server avaiable.");
                break;
            }
        }
        if (seconds_waited == wait_time)
        {
            RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available. Exiting...");
            return 1;
        }
        return 0;
    }

    int CliValidateEstiamteShape::_getActionResult(std::shared_future<std::shared_ptr<GoalHandleItemSelectAction>> goal_future)
    {
        int exit_code = 0;
        auto result_future = _action_client->async_get_result(goal_future.get());
        rclcpp::FutureReturnCode result_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), result_future, 10s);
        if (result_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            switch (result_future.get().code)
            {
            case rclcpp_action::ResultCode::SUCCEEDED:
                RCLCPP_INFO(this->get_logger(), "Goal succeeded.");
                break;
            case rclcpp_action::ResultCode::ABORTED:
                RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::CANCELED:
                RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::UNKNOWN:
                RCLCPP_ERROR(this->get_logger(), "Unknown result code");
                exit_code = 1;
                break;
            default:
                RCLCPP_ERROR(this->get_logger(), "Other result code");
                exit_code = 1;
                break;
            }
        }
        else if (result_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Result interrupted");
            exit_code = 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Result timeout");
            exit_code = 1;
        }
        return exit_code;
    }

} // namespace cli

char* getCmdOption(char **begin, char **end, const std::string &option)
{
    char **itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return nullptr;
}

bool cmdOptionExists(char **begin, char **end, const std::string &option)
{
    return std::find(begin, end, option) != end;
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    int exit_code = 0;

    char* cmd_option;
    float position, orientation, size;
    std::string scene_description;

    if(cmdOptionExists(argv, argv+argc, "position"))
    {
        cmd_option = getCmdOption(argv, argv+argc, "position");
        if(cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("cli_validate_estimate_shape"), "Result interrupted");
            return EXIT_FAILURE;
        }
        position = std::atof(cmd_option);
    }
    else
    {
        position = 0;
    }

    if(cmdOptionExists(argv, argv+argc, "orientation"))
    {
        cmd_option = getCmdOption(argv, argv+argc, "orientation");
        if(cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("cli_validate_estimate_shape"), "Result interrupted");
            return EXIT_FAILURE;
        }
        orientation = std::atof(cmd_option);
    }
    else
    {
        orientation = 0;
    }
        

    if(cmdOptionExists(argv, argv+argc, "size"))
    {
        cmd_option = getCmdOption(argv, argv+argc, "size");
        if(cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("cli_validate_estimate_shape"), "Result interrupted");
            return EXIT_FAILURE;
        }    
        size = std::atof(cmd_option);
    }
    else
    {
        size = 0;
    }

    if(cmdOptionExists(argv, argv+argc, "scene_description"))
    {
        cmd_option = getCmdOption(argv, argv+argc, "scene_description");
        if(cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("cli_validate_estimate_shape"), "Result interrupted");
            return EXIT_FAILURE;
        }
        scene_description = std::string(cmd_option);
    }
    else
    {
        scene_description = ""; 
    }
        
    try
    {
        auto item_select_client = std::make_shared<cli::CliValidateEstiamteShape>("validate_estimate_shape");
        exit_code = item_select_client->sendGoal(position, orientation, size, scene_description);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        rclcpp::shutdown();
        return EXIT_FAILURE;
    }

    return exit_code;
}