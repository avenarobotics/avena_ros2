#include "cli/cli_manager_prepare_pick_and_place_data.hpp"
namespace cli
{

    CliManagerPreparePickAndPlaceData::CliManagerPreparePickAndPlaceData(std::string action_name, const rclcpp::NodeOptions &options)
        : Node("cli_manager_prepare_pick_and_place_data", options), _action_name(action_name)
    {
        _execution_timer = std::make_shared<Timer>("Action client - full time");
        RCLCPP_INFO(this->get_logger(), "Starting Cli manager_prepare_pick_and_place_data action client.");
    }

    int CliManagerPreparePickAndPlaceData::_waitForServer()
    {
        size_t wait_time = 5;
        size_t seconds_waited = 0;
        while (seconds_waited < wait_time)
        {
            if (!_action_client->wait_for_action_server(1s))
            {
                RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available after waiting for " << ++seconds_waited << " seconds.");
                if (!rclcpp::ok())
                {
                    RCLCPP_ERROR(this->get_logger(), "Problem with ROS. Exiting...");
                    throw std::runtime_error("Problem with ROS. Exiting...");
                }
            }
            else
            {
                RCLCPP_INFO(this->get_logger(), "Action server avaiable.");
                break;
            }
        }
        if (seconds_waited == wait_time)
        {
            RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available. Exiting...");
            return 1;
        }
        return 0;
    }
    int CliManagerPreparePickAndPlaceData::_sendGoal(std::string label,
                              std::string areas,
                              std::string area_operator,
                              std::string policy,
                              std::string distance_min,
                              std::string distance_max,
                              std::string selected_area_label_place,
                              std::string selected_area_operator_place,
                              std::string search_shift,
                              std::string place_positions_required,
                              float constrain_value,
                              int ik_trials_number,
                              int max_final_states,
                              int ompl_compare_trials,
                              int min_path_points,
                              int max_time,
                              int max_simplification_time)
    {

        _action_client = rclcpp_action::create_client<custom_interfaces::action::ManagerPreparePickAndPlaceDataAction>(this, "manager_prepare_pick_and_place_data");
        RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
        Timer timer("Action execution");
        if (_waitForServer())
            return 1;
        RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _action_name << "\" action.");

        auto goal_msg = custom_interfaces::action::ManagerPreparePickAndPlaceDataAction::Goal();
        goal_msg.selected_item_label = label;
        goal_msg.selected_area_label = areas;
        goal_msg.selected_area_operator = area_operator;
        goal_msg.selected_policy = policy;
        goal_msg.distance_parameter_min = distance_min;
        goal_msg.distance_parameter_max = distance_max;
        goal_msg.selected_area_label_place = selected_area_label_place;
        goal_msg.selected_area_operator_place = selected_area_operator_place;
        goal_msg.search_shift = search_shift;
        goal_msg.place_positions_required = place_positions_required;
        goal_msg.constrain_value = constrain_value;
        goal_msg.ik_trials_number = ik_trials_number;
        goal_msg.max_final_states = max_final_states;
        goal_msg.ompl_compare_trials = ompl_compare_trials;
        goal_msg.min_path_points = min_path_points;
        goal_msg.max_time = max_time;
        goal_msg.max_simplification_time = max_simplification_time;
        
        auto goal_future = _action_client->async_send_goal(goal_msg);
        rclcpp::FutureReturnCode goal_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), goal_future, 60s);
        if (goal_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            RCLCPP_INFO(this->get_logger(), "Sending goal successfully");
            if (_getActionResult(goal_future))
                return 1;
        }
        else if (goal_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal interrupted");
            return 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal timeout");
            return 1;
        }
        return 0;
    }

    int CliManagerPreparePickAndPlaceData::_getActionResult(std::shared_future<std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction>> goal_future)
    {
        int exit_code = 0;
        auto result_future = _action_client->async_get_result(goal_future.get());
        rclcpp::FutureReturnCode result_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), result_future, 150s);
        if (result_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            switch (result_future.get().code)
            {
            case rclcpp_action::ResultCode::SUCCEEDED:
            {
                RCLCPP_INFO(this->get_logger(), "Goal succeeded.");
                break;
            }
            case rclcpp_action::ResultCode::ABORTED:
                RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::CANCELED:
                RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::UNKNOWN:
                RCLCPP_ERROR(this->get_logger(), "Unknown result code");
                exit_code = 1;
                break;
            default:
                RCLCPP_ERROR(this->get_logger(), "Other result code");
                exit_code = 1;
                break;
            }
        }
        else if (result_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Result interrupted");
            exit_code = 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Result timeout");
            auto result_future_cancell = _action_client->async_cancel_all_goals();
            if (rclcpp::spin_until_future_complete(this->get_node_base_interface(), result_future_cancell, 20s) !=
                rclcpp::FutureReturnCode::SUCCESS)
            {
                RCLCPP_ERROR(this->get_logger(), "failed to cancel goal");
                return 1;
            }
            RCLCPP_INFO(this->get_logger(), "goal is being canceled");
            exit_code = 1;
        }
        return exit_code;
    }
      void _printHelp()
    {
        std::cout<< "To use CLI manager you need to pass : \n";
        std::cout<< "ITEM SELECT : \n";
        std::cout<< "select_sort_policy value <-  \n";
        std::cout<< "label value  <-selected item label\n";
        std::cout<< "select_area_policy value  <-inside/outside of selected area\n";
        std::cout<< "select_area value  <- selected areas you can pass multiple areas separated with comma\n ";
        std::cout<< "PLACE : \n";
        std::cout<< "place_area_policy value  <-inside/outside of selected area\n";
        std::cout<< "place_area value  <-selected areas you can pass multiple areas separated with comma\n";
        std::cout<< "For some policies you need to pass additional parameters : \n";
        std::cout<< "ITEM SELECT : \n";
        std::cout<< "Avaiable policies : nearest , farthest, bigger_than, lower_than , in_range , least_occluded \n";
        std::cout<< "distance_max value  <-distance operator for item select   \n";
        std::cout<< "distance_min value  <-distance operator for item select   \n";
        std::cout<< "PLACE : \n ";
        std::cout<< "search_shift value  <-search shift value for placing algorithm \n";
        std::cout<< "nr_poses value  <-set the treshold of valid place positions in one place \n";
        std::cout<< "Additionaly you can pass values for GeneratePath module \n";
        std::cout<< "GENERATE PATH : \n ";
        std::cout<< "constrain_value value  <-Value of the constrain for generating the path \n";
        std::cout<< "ik_trials_number value  <-Value of the trails ik should perform \n";
        std::cout<< "max_final_states value  <-Value of the max final states robot can achieve \n";
        std::cout<< "ompl_compare_trials value  <- Value of the trails ompl should compare \n";
        std::cout<< "min_path_points value  <-Minimal number of points on the generated path \n";
        std::cout<< "max_time value  <- Maximum time ompl can calculate path \n";
        std::cout<< "max_simplification_time value  <-Maximum time ompl can simplify generated path \n";
    }

} // namespace cli

char *getCmdOption(char **begin, char **end, const std::string &option)
{
    char **itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return nullptr;
}

bool cmdOptionExists(char **begin, char **end, const std::string &option)
{
    return std::find(begin, end, option) != end;
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    int exit_code = 0;

    char *cmd_option;
    std::string label;
    std::string areas;
    std::string area_operator;
    std::string policy;
    std::string distance_min;
    std::string distance_max;
    std::string selected_area_label_place;
    std::string selected_area_operator_place;
    std::string search_shift;
    std::string place_positions_required;
    // initialized with default values
    float constrain_value=15.0f;
    int ik_trials_number=150;
    int max_final_states=4;
    int ompl_compare_trials=2;
    int min_path_points=200;
    int max_time=60;
    int max_simplification_time=20;
    if (cmdOptionExists(argv, argv + argc, "-h"))
    {
        cli::_printHelp();
        return 0;

    }
    // Mandatory parameters
    // label
    if (!cmdOptionExists(argv, argv + argc, "label"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Label parameter is requred");
        return EXIT_FAILURE;
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "label");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Label parameter is requred");
            return EXIT_FAILURE;
        }
        label = std::string(cmd_option);
    }
    // policy
    if (!cmdOptionExists(argv, argv + argc, "select_sort_policy"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "select_sort_policy parameter is requred");
        return EXIT_FAILURE;
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "select_sort_policy");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "select_sort_policy parameter is requred");
            return EXIT_FAILURE;
        }
        policy = std::string(cmd_option);
    }
    // select_area_policy
    if (!cmdOptionExists(argv, argv + argc, "select_area_policy"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "select_area_policy parameter is requred");
        return EXIT_FAILURE;
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "select_area_policy");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "select_area_policy parameter is requred");
            return EXIT_FAILURE;
        }
        area_operator = std::string(cmd_option);
    }
    // select_area
    if (!cmdOptionExists(argv, argv + argc, "select_area"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "select_area parameter is requred");
        return EXIT_FAILURE;
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "select_area");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "select_area parameter is requred");
            return EXIT_FAILURE;
        }
        areas = std::string(cmd_option);
    }
    // place_area_policy
    if (!cmdOptionExists(argv, argv + argc, "place_area_policy"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "place_area_policy parameter is requred");
        return EXIT_FAILURE;
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "place_area_policy");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "place_area_policy parameter is requred");
            return EXIT_FAILURE;
        }
        selected_area_operator_place = std::string(cmd_option);
    }
    // place_area
    if (!cmdOptionExists(argv, argv + argc, "place_area"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "place_area parameter is requred");
        return EXIT_FAILURE;
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "place_area");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "place_area parameter is requred");
            return EXIT_FAILURE;
        }
        selected_area_label_place = std::string(cmd_option);
    }

    if (cmdOptionExists(argv, argv + argc, "distance_min"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "distance_min");
        if (cmd_option == nullptr)
        {
            RCLCPP_WARN(rclcpp::get_logger("Item_select"), "distance_min parameter is not passed");
            return EXIT_FAILURE;
        }
        distance_min = std::string(cmd_option);
    }
    if (cmdOptionExists(argv, argv + argc, "distance_max"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "distance_max");
        if (cmd_option == nullptr)
        {
            RCLCPP_WARN(rclcpp::get_logger("Item_select"), "distance_max parameter is not passed");
            return EXIT_FAILURE;
        }
        distance_max = std::string(cmd_option);
    }
    if (cmdOptionExists(argv, argv + argc, "search_shift"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "search_shift");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Place"), "search_shift parameter is requred");
            return EXIT_FAILURE;
        }
        search_shift = std::string(cmd_option);
    }
    if (cmdOptionExists(argv, argv + argc, "nr_poses"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "nr_poses");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Place"), "number of valid poses parameter is requred");
            throw(std::runtime_error(std::string("number of valid poses parameter is requred")));
        }
        place_positions_required = std::string(cmd_option);
    }
    if (cmdOptionExists(argv, argv + argc, "constrain_value"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "constrain_value");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Generate_path"), "constrain_value parameter is not passed, no contstrains is set");
            return EXIT_FAILURE;
        }
        constrain_value = std::stof(cmd_option);
    }
    else
    {
        constrain_value = -1;
    }
    if (cmdOptionExists(argv, argv + argc, "ik_trials_number"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "ik_trials_number");
        if (cmd_option == nullptr)
        {
            RCLCPP_INFO(rclcpp::get_logger("Generate_path"), "ik_trials_number parameter is not passed ");
            return EXIT_FAILURE;
        }
        ik_trials_number = std::stoi(cmd_option);
    }
    if (cmdOptionExists(argv, argv + argc, "max_final_states"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "max_final_states");
        if (cmd_option == nullptr)
        {
            RCLCPP_INFO(rclcpp::get_logger("Generate_path"), "max_final_states parameter is not passed ");
            return EXIT_FAILURE;
        }
        max_final_states = std::stoi(cmd_option);
    }

    if (cmdOptionExists(argv, argv + argc, "ompl_compare_trials"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "ompl_compare_trials");
        if (cmd_option == nullptr)
        {
            RCLCPP_INFO(rclcpp::get_logger("Generate_path"), "ompl_compare_trials parameter is not passed ");
            return EXIT_FAILURE;
        }
        ompl_compare_trials = std::stoi(cmd_option);
    }

    if (cmdOptionExists(argv, argv + argc, "min_path_points"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "min_path_points");
        if (cmd_option == nullptr)
        {
            RCLCPP_INFO(rclcpp::get_logger("Generate_path"), "min_path_points parameter is not passed ");
            return EXIT_FAILURE;
        }
        min_path_points = std::stoi(cmd_option);
    }
    if (cmdOptionExists(argv, argv + argc, "max_time"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "max_time");
        if (cmd_option == nullptr)
        {
            RCLCPP_INFO(rclcpp::get_logger("Generate_path"), "max_time parameter is not passed ");
            return EXIT_FAILURE;
        }
        max_time = std::stoi(cmd_option);
    }
    if (cmdOptionExists(argv, argv + argc, "max_simplification_time"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "max_simplification_time");
        if (cmd_option == nullptr)
        {
            RCLCPP_INFO(rclcpp::get_logger("Generate_path"), "max_simplification_time parameter is not passed ");
            return EXIT_FAILURE;
        }
        max_simplification_time = std::stoi(cmd_option);
    }
    if (label.empty())
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Missing label parameter ");
        cli::_printHelp();
        return 1;
    }
    if (policy.empty())
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Missing select_sort_policy parameter");
        cli::_printHelp();
        return 1;
    }
    if (area_operator.empty())
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Missing select_area_policy parameter ");
        cli::_printHelp();
        return 1;
    }
    if (areas.empty())
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Missing select_area parameter ");
        cli::_printHelp();
        return 1;
    }
    if (selected_area_operator_place.empty())
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Missing place_area_policy parameter ");
        cli::_printHelp();
        return 1;
    }
        if (selected_area_label_place.empty())
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Missing place_area parameter ");
        cli::_printHelp();
        return 1;
    }
    try
    {
        auto cli_manager_client = std::make_shared<cli::CliManagerPreparePickAndPlaceData>("cli_manager_prepare_pick_and_place_data");
        exit_code = cli_manager_client->_sendGoal(label, areas, area_operator, policy, distance_min, distance_max,
                                                  selected_area_label_place, selected_area_operator_place, search_shift, place_positions_required,
                                                  constrain_value, ik_trials_number, max_final_states, ompl_compare_trials, min_path_points, max_time, max_simplification_time);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        rclcpp::shutdown();
        return 1;
    }

    // std::cout << "error code cli" << exit_code << std::endl;

    return exit_code;
}
