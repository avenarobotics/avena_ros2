#include "cli/cli_generate_objects.hpp"

namespace cli
{

    CliGenerateObjects::CliGenerateObjects(const rclcpp::NodeOptions &options)
        : Node("cli_generate_objects", options)
    {
        // helpers::commons::setLoggerLevel(get_logger(), "info");
        RCLCPP_INFO(get_logger(), "Starting CLI generate objects action clients.");
        _execution_timer = std::make_shared<Timer>("Generate objects actions - full time");
    }

    int CliGenerateObjects::createActionClients()
    {
        RCLCPP_INFO_STREAM(get_logger(), "Creating action clients.");

        for (auto &action_client_name : _action_clients_names)
        {
            _action_clients[action_client_name] = rclcpp_action::create_client<SimpleAction>(this, action_client_name);
            if (_waitForServer(_action_clients[action_client_name]))
                return 1;
            RCLCPP_INFO_STREAM(get_logger(), "Action \"" << action_client_name << "\" is avaiable.");
        }

        return 0;
    }

    int CliGenerateObjects::_waitForServer(const rclcpp_action::Client<SimpleAction>::SharedPtr &action_client)
    {
        size_t wait_time = 5;
        size_t seconds_waited = 0;
        while (seconds_waited < wait_time)
        {
            if (!action_client->wait_for_action_server(1s))
            {
                RCLCPP_ERROR_STREAM(get_logger(), "Action server not available after waiting for " << ++seconds_waited << " seconds.");
                if (!rclcpp::ok())
                {
                    RCLCPP_ERROR(get_logger(), "Problem with ROS. Exiting...");
                    throw std::runtime_error("Problem with ROS. Exiting...");
                }
            }
            else
                break;
        }
        if (seconds_waited == wait_time)
        {
            RCLCPP_ERROR_STREAM(get_logger(), "Action server not available. Exiting...");
            return 1;
        }
        return 0;
    }

    int CliGenerateObjects::generateObjects()
    {
        Timer timer("Generate objects actions - executions time");
        RCLCPP_INFO(get_logger(), "Calling actions to generate objects.");

        for (auto &action_client_name : _action_clients_names)
        {
            // std::this_thread::sleep_for(std::chrono::milliseconds(100));

            Timer timer_single_action(action_client_name);

            RCLCPP_INFO_STREAM(get_logger(), "Sending goal to \"" << action_client_name << "\" action server.");
            if (_sendGoal(_action_clients[action_client_name]))
            {
                RCLCPP_ERROR_STREAM(get_logger(), "Error occured in \"" << action_client_name << "\" action. Exiting...");
                return 1;
            }
            RCLCPP_INFO_STREAM(get_logger(), "Action \"" << action_client_name << "\" finished successfully.");
        }

        return 0;
    }

    int CliGenerateObjects::_sendGoal(const rclcpp_action::Client<SimpleAction>::SharedPtr &action_client)
    {
        auto goal_msg = SimpleAction::Goal();
        auto goal_future = action_client->async_send_goal(goal_msg);

        if (_getSendGoalResult(goal_future))
        {
            RCLCPP_WARN_STREAM(get_logger(), "Problem occured when getting sending goal result.");
            return 1;
        }

        if (_getActionResult(action_client, goal_future.get()))
        {
            RCLCPP_WARN_STREAM(get_logger(), "Problem occured when getting action result.");
            return 1;
        }
        return 0;
    }

    int CliGenerateObjects::_getSendGoalResult(std::shared_future<std::shared_ptr<GoalHandleSimpleAction>> goal_future)
    {
        rclcpp::FutureReturnCode goal_future_result = rclcpp::spin_until_future_complete(get_node_base_interface(), goal_future, 10s);
        if (goal_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            RCLCPP_INFO(get_logger(), "Action server received goal successfully");
            return 0;
        }
        else if (goal_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(get_logger(), "Sending goal was interrupted");
            return 1;
        }
        else
        {
            RCLCPP_ERROR(get_logger(), "Timeout occured while sending goal request");
            return 1;
        }
        return 0;
    }

    int CliGenerateObjects::_getActionResult(const rclcpp_action::Client<SimpleAction>::SharedPtr &action_client, std::shared_ptr<GoalHandleSimpleAction> goal_handle)
    {
        int exit_code = 0;
        auto result_future = action_client->async_get_result(goal_handle);
        rclcpp::FutureReturnCode result_future_result = rclcpp::spin_until_future_complete(get_node_base_interface(), result_future, 60s);
        if (result_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            switch (result_future.get().code)
            {
            case rclcpp_action::ResultCode::SUCCEEDED:
                break;
            case rclcpp_action::ResultCode::ABORTED:
                RCLCPP_ERROR(get_logger(), "Goal was aborted");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::CANCELED:
                RCLCPP_ERROR(get_logger(), "Goal was canceled");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::UNKNOWN:
                RCLCPP_ERROR(get_logger(), "Unknown result code");
                exit_code = 1;
                break;
            default:
                RCLCPP_ERROR(get_logger(), "Other result code");
                exit_code = 1;
                break;
            }
        }
        else if (result_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(get_logger(), "Result interrupted");
            exit_code = 1;
        }
        else
        {
            RCLCPP_ERROR(get_logger(), "Result timeout");
            exit_code = 1;
        }
        return exit_code;
    }

} // namespace cli

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<cli::CliGenerateObjects> generate_objects_clients;
    try
    {
        generate_objects_clients = std::make_shared<cli::CliGenerateObjects>();
        if (generate_objects_clients->createActionClients())
            throw std::runtime_error("Error occured while creating action clients.");

        if (generate_objects_clients->generateObjects())
            throw std::runtime_error("Error occured while generating objects.");
    }
    catch (const std::exception &e)
    {
        RCLCPP_ERROR(rclcpp::get_logger("cli_generate_objects"), e.what());
        generate_objects_clients.reset();
        rclcpp::shutdown();
        return 1;
    }

    return 0;
}
