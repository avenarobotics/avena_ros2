#include "cli/cli_item_select.hpp"
namespace cli
{

    CliItemSelectActionClient::CliItemSelectActionClient(std::string action_name, const rclcpp::NodeOptions &options)
        : Node("cli_item_select", options), _action_name(action_name)
    {
        _execution_timer = std::make_shared<Timer>("Action client - full time");
        RCLCPP_INFO(this->get_logger(), "Starting CLI Item_select action client.");
        _grasp_node = rclcpp::Node::make_shared("grasp_client_node");
        // auto grasp_client = std::make_shared<cli::GraspActionClient>("grasp_client", _grasp_node);
    }
    int CliItemSelectActionClient::sendGoal(std::string label,
                                            std::string areas,
                                            std::string area_operator,
                                            std::string policy,
                                            std::string distance_min,
                                            std::string distance_max)
    {
        _action_client = rclcpp_action::create_client<ItemSelectAction>(this, _action_name);
        RCLCPP_INFO_STREAM(this->get_logger(), "Waiting for action server...");
        Timer timer("Action execution");
        if (_waitForServer())
            return 1;
        RCLCPP_INFO_STREAM(this->get_logger(), "Send goal to \"" << _action_name << "\" action.");
        auto goal_msg = ItemSelectAction::Goal();
        //Assign values;
        std::vector<std::string> areas_vector;
        std::stringstream ss(areas);
        if (areas.find(","))
        {
            std::stringstream ss(areas);
            while (ss.good())
            {
                std::string substr;
                getline(ss, substr, ',');
                areas_vector.push_back(substr);
            }
        }
        else
        {
            areas_vector.push_back(areas);
        }

        goal_msg.selected_item_label = label;
        goal_msg.selected_area_label = areas_vector;
        goal_msg.selected_area_operator = area_operator;
        goal_msg.selected_policy = policy;
        goal_msg.distance_parameter_min = distance_min;
        goal_msg.distance_parameter_max = distance_max;
        std::cout << "Goal Request" << std::endl;
        std::cout << "selected_item_label: " << goal_msg.selected_item_label << std::endl;
        for (auto area : goal_msg.selected_area_label)
            std::cout << "selected_area_label: " << area << std::endl;
        std::cout << "selected_area_operator: " << goal_msg.selected_area_operator << std::endl;
        std::cout << "selected_policy: " << goal_msg.selected_policy << std::endl;
        std::cout << "distance_parameter_min: " << goal_msg.distance_parameter_min << std::endl;
        std::cout << "distance_parameter_max: " << goal_msg.distance_parameter_max << std::endl;

        auto goal_future = _action_client->async_send_goal(goal_msg);
        rclcpp::FutureReturnCode goal_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), goal_future, 10s);
        if (goal_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            RCLCPP_INFO(this->get_logger(), "Sending goal successfully");
            if (_getActionResult(goal_future))
                return 1;
        }
        else if (goal_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal interrupted");
            return 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Sending goal timeout");
            return 1;
        }

        return 0;
    }

    int CliItemSelectActionClient::_waitForServer()
    {
        size_t wait_time = 5;
        size_t seconds_waited = 0;
        while (seconds_waited < wait_time)
        {
            if (!_action_client->wait_for_action_server(1s))
            {
                RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available after waiting for " << ++seconds_waited << " seconds.");
                if (!rclcpp::ok())
                {
                    RCLCPP_ERROR(this->get_logger(), "Problem with ROS. Exiting...");
                    throw std::runtime_error("Problem with ROS. Exiting...");
                }
            }
            else
            {
                RCLCPP_INFO(this->get_logger(), "Action server avaiable.");
                break;
            }
        }
        if (seconds_waited == wait_time)
        {
            RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available. Exiting...");
            return 1;
        }
        return 0;
    }

    // template <typename ActionT>
    // int Dupa::_waitForServer(const rclcpp_action::Client<ActionT>::SharedPtr action_client)
    // {
    //     size_t wait_time = 5;
    //     size_t seconds_waited = 0;
    //     while (seconds_waited < wait_time)
    //     {
    //         if (!action_client->wait_for_action_server(1s))
    //         {
    //             RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available after waiting for " << ++seconds_waited << " seconds.");
    //             if (!rclcpp::ok())
    //             {
    //                 RCLCPP_ERROR(this->get_logger(), "Problem with ROS. Exiting...");
    //                 throw std::runtime_error("Problem with ROS. Exiting...");
    //             }
    //         }
    //         else
    //         {
    //             RCLCPP_INFO(this->get_logger(), "Action server avaiable.");
    //             break;
    //         }
    //     }
    //     if (seconds_waited == wait_time)
    //     {
    //         RCLCPP_ERROR_STREAM(this->get_logger(), "Action server not available. Exiting...");
    //         return 1;
    //     }
    //     return 0;
    // }

    int CliItemSelectActionClient::_getActionResult(std::shared_future<std::shared_ptr<GoalHandleItemSelectAction>> goal_future)
    {
        int exit_code = 0;
        auto result_future = _action_client->async_get_result(goal_future.get());
        rclcpp::FutureReturnCode result_future_result = rclcpp::spin_until_future_complete(this->get_node_base_interface(), result_future, 10s);
        if (result_future_result == rclcpp::FutureReturnCode::SUCCESS)
        {
            switch (result_future.get().code)
            {
            case rclcpp_action::ResultCode::SUCCEEDED:
            {
                std::vector<int32_t> selected_item_ids = result_future.get().result->selected_items_ids;
                for (auto selected_item_id : selected_item_ids)
                    std::cout << "selected_item_id: " << selected_item_id << std::endl;
                // // try
                // {
                //     // auto grasp_client = std::make_shared<cli::GraspActionClient>("grasp_client",_grasp_node);
                //     // rclcpp::spin_some(_grasp_node);
                //     // RCLCPP_INFO(this->get_logger(), "Sending request to grasp");
                //     // exit_code = grasp_client->sendGoal(selected_item_ids[0]);
                // }
                // catch (const std::exception &e)
                // {
                //     std::cerr << e.what() << '\n';
                //     rclcpp::shutdown();
                //     return 1;
                // }
                RCLCPP_INFO(this->get_logger(), "Goal succeeded.");
                break;
            }
            case rclcpp_action::ResultCode::ABORTED:
                RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::CANCELED:
                RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
                exit_code = 1;
                break;
            case rclcpp_action::ResultCode::UNKNOWN:
                RCLCPP_ERROR(this->get_logger(), "Unknown result code");
                exit_code = 1;
                break;
            default:
                RCLCPP_ERROR(this->get_logger(), "Other result code");
                exit_code = 1;
                break;
            }
        }
        else if (result_future_result == rclcpp::FutureReturnCode::INTERRUPTED)
        {
            RCLCPP_ERROR(this->get_logger(), "Result interrupted");
            exit_code = 1;
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Result timeout");
            exit_code = 1;
        }
        return exit_code;
    }
} // namespace cli

char *getCmdOption(char **begin, char **end, const std::string &option)
{
    char **itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return nullptr;
}

bool cmdOptionExists(char **begin, char **end, const std::string &option)
{
    return std::find(begin, end, option) != end;
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    int exit_code = 0;

    char *cmd_option;
    std::string label;
    std::string areas;
    std::string area_operator;
    std::string policy;
    std::string distance_min;
    std::string distance_max;

    if (!cmdOptionExists(argv, argv + argc, "label"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Label parameter is requred");
        throw(std::runtime_error(std::string("Label parameter is requred")));
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "label");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "Label parameter is requred");
            throw(std::runtime_error(std::string("Label parameter is requred")));
        }
        label = std::string(cmd_option);
    }

    if (!cmdOptionExists(argv, argv + argc, "area"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "area parameter is requred");
        throw(std::runtime_error(std::string("area parameter is requred")));
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "area");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "area parameter is requred");
            throw(std::runtime_error(std::string("area parameter is requred")));
        }
        areas = std::string(cmd_option);
    }

    if (!cmdOptionExists(argv, argv + argc, "area_operator"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "area_operator parameter is requred");
        throw(std::runtime_error(std::string("area_operator parameter is requred")));
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "area_operator");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "area_operator parameter is requred");
            throw(std::runtime_error(std::string("area_operator parameter is requred")));
        }
        area_operator = std::string(cmd_option);
    }

    if (!cmdOptionExists(argv, argv + argc, "policy"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "policy parameter is requred");
        throw(std::runtime_error(std::string("policy parameter is requred")));
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "policy");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "policy parameter is requred");
            throw(std::runtime_error(std::string("policy parameter is requred")));
        }
        policy = std::string(cmd_option);
    }

    if (cmdOptionExists(argv, argv + argc, "distance_min"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "distance_min");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "policy parameter is requred");
            return EXIT_FAILURE;
        }
        distance_min = std::string(cmd_option);
    }
    else
    {
        distance_min = "0";
    }

    if (cmdOptionExists(argv, argv + argc, "distance_max"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "distance_max");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("Item_select"), "distance_max parameter is requred");
            return EXIT_FAILURE;
        }
        distance_max = std::string(cmd_option);
    }
    else
    {
        distance_max = "0";
    }

    try
    {
        auto item_select_client = std::make_shared<cli::CliItemSelectActionClient>("item_select");
        exit_code = item_select_client->sendGoal(label, areas, area_operator, policy, distance_min, distance_max);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        rclcpp::shutdown();
        return 1;
    }

    return exit_code;
}
