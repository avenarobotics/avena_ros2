#ifndef CLI__CLI_MANAGER_HPP_
#define CLI__CLI_MANAGER_HPP_

// ___CPP___
#include <chrono>
#include <string>
#include <algorithm>
// ___ROS2___
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

// #include "rclcpp_components/register_node_macro.hpp"

// ___Avena___
// #include "custom_interfaces/action/item_select.hpp"
#include "custom_interfaces/action/manager_prepare_pick_and_place_data_action.hpp"
// #include "cli/cli_grasp_client.hpp"
#include "cli/visibility_control.h"
#include "cli/util.hpp"

namespace cli
{
  using namespace std::chrono_literals;

  class CliManagerPreparePickAndPlaceData : public rclcpp::Node
  {
  public:
    using ManagerPreparePickAndPlaceDataAction = custom_interfaces::action::ManagerPreparePickAndPlaceDataAction;
    using GoalHandleManagerPreparePickAndPlaceDataAction = rclcpp_action::ClientGoalHandle<ManagerPreparePickAndPlaceDataAction>;

    CLI_PUBLIC
    explicit CliManagerPreparePickAndPlaceData(std::string action_name, const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    CLI_PUBLIC
    virtual ~CliManagerPreparePickAndPlaceData(){};

    CLI_PUBLIC
    int _sendGoal(std::string label,
                  std::string areas,
                  std::string area_operator,
                  std::string policy,
                  std::string distance_min,
                  std::string distance_max,
                  std::string selected_area_label_place,
                  std::string selected_area_operator_place,
                  std::string search_shift,
                  std::string place_positions_required,
                  float constrain_value,
                  int ik_trials_number,
                  int max_final_states,
                  int ompl_compare_trials,
                  int min_path_points,
                  int max_time,
                  int max_simplification_time);
    CLI_PUBLIC
    int _split_string(std::string input, std::vector<std::string> output);

  private:
    // ___Member functions___
    CLI_LOCAL
    int _waitForServer();

    CLI_LOCAL
    int _getActionResult(std::shared_future<std::shared_ptr<GoalHandleManagerPreparePickAndPlaceDataAction>> goal_future);
    CLI_LOCAL
    void _printHelp();
    // ___Attributes___
    rclcpp_action::Client<ManagerPreparePickAndPlaceDataAction>::SharedPtr _action_client;
    std::string _action_name;
    Timer::SharedPtr _execution_timer;
    // cli::GraspActionClient::Node::SharedPtr _grasp_node;
    // std::vector<std::string> _cli_options;
  };

} // namespace cli

#endif // _CLI_MANAGER
