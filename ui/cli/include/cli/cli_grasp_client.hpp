#ifndef CLI__CLI_GRASP_CLIENT_HPP_
#define CLI__CLI_GRASP_CLIENT_HPP_


#include <chrono>
#include <string>
#include <algorithm>
// ___ROS2___
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

// #include "rclcpp_components/register_node_macro.hpp"

// ___Avena___
// #include "custom_interfaces/action/item_select.hpp"
#include "cli/visibility_control.h"
#include "cli/util.hpp"
#include "custom_interfaces/action/grasp_action.hpp"
namespace cli
{
  using namespace std::chrono_literals;

  class GraspActionClient : public rclcpp::Node
{
  public:
    using GraspAction =custom_interfaces::action::GraspAction;
    using GoalHandleGraspAction = rclcpp_action::ClientGoalHandle<GraspAction>;

    CLI_PUBLIC
    explicit GraspActionClient(std::string action_name, const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    CLI_PUBLIC
    virtual ~GraspActionClient(){};

    CLI_PUBLIC
    int sendGoal(int32_t selected_item_ids);
    CLI_PUBLIC
    int _execute_grasp(int32_t selected_item_id);
  private:
    // ___Member functions___
    CLI_LOCAL
    int _waitForServer();

    CLI_LOCAL
    int _getActionResult(std::shared_future<std::shared_ptr<GoalHandleGraspAction>> goal_future);

    // ___Attributes___
    rclcpp_action::Client<GraspAction>::SharedPtr _action_client;
    std::string _action_name;
    Timer::SharedPtr _execution_timer;
    // std::vector<std::string> _cli_options;
  };
}//namespace cli
#endif // _CLI_GRASP_CLIENT