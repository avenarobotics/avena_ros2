#ifndef CLI__CLI_GENERATE_OBJECTS_HPP_
#define CLI__CLI_GENERATE_OBJECTS_HPP_

// ___CPP___
#include <chrono>

// ___ROS2___
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

// ___Avena___
#include "custom_interfaces/action/simple_action.hpp"
#include "cli/visibility_control.h"
#include "cli/util.hpp"
// #include "helpers.hpp"

namespace cli
{
  using namespace std::chrono_literals;

  class CliGenerateObjects : public rclcpp::Node
  {
  public:
    using SimpleAction = custom_interfaces::action::SimpleAction;
    using GoalHandleSimpleAction = rclcpp_action::ClientGoalHandle<SimpleAction>;

    CLI_PUBLIC
    explicit CliGenerateObjects(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    CLI_PUBLIC
    virtual ~CliGenerateObjects(){};

    CLI_PUBLIC
    int generateObjects();

    CLI_PUBLIC
    int createActionClients();

  private:
    // ___Member functions___
    CLI_LOCAL
    int _sendGoal(const rclcpp_action::Client<SimpleAction>::SharedPtr &action_client);

    CLI_LOCAL
    int _waitForServer(const rclcpp_action::Client<SimpleAction>::SharedPtr &action_client);

    CLI_LOCAL
    int _getSendGoalResult(std::shared_future<std::shared_ptr<GoalHandleSimpleAction>> goal_future);

    CLI_LOCAL
    int _getActionResult(const rclcpp_action::Client<SimpleAction>::SharedPtr &action_client, std::shared_ptr<GoalHandleSimpleAction> goal_handle);

    // ___Attributes___
    std::map<std::string, rclcpp_action::Client<SimpleAction>::SharedPtr> _action_clients;
    Timer::SharedPtr _execution_timer;
    const std::vector<std::string> _action_clients_names = {
        "scene_publisher",
        "detect",
        "filter_detections",
        "select_new_masks",
        "compose_items",
        "estimate_shape",
        "merge_items",
        "octomap_filter",
        "spawn_collision_items",
    };
  };

} // namespace cli

#endif // CLI__CLI_LIB_HPP_