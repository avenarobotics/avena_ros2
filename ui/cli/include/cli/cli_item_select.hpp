#ifndef CLI__CLI_ITEM_SELECT_HPP_
#define CLI__CLI_ITEM_SELECT_HPP_

// ___CPP___
#include <chrono>
#include <string>
#include <algorithm>
// ___ROS2___
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

// #include "rclcpp_components/register_node_macro.hpp"

// ___Avena___
#include "custom_interfaces/action/item_select.hpp"
#include "cli/cli_grasp_client.hpp"
#include "cli/visibility_control.h"
#include "cli/util.hpp"

namespace cli
{
  using namespace std::chrono_literals;

  class CliItemSelectActionClient : public rclcpp::Node
  {
  public:
    using ItemSelectAction = custom_interfaces::action::ItemSelect;
    using GoalHandleItemSelectAction = rclcpp_action::ClientGoalHandle<ItemSelectAction>;

    CLI_PUBLIC
    explicit CliItemSelectActionClient(std::string action_name, const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    CLI_PUBLIC
    virtual ~CliItemSelectActionClient(){};

    CLI_PUBLIC
    int sendGoal(std::string label,
                 std::string areas,
                 std::string area_operator,
                 std::string policy,
                 std::string distance_min,
                 std::string distance_max);
    CLI_PUBLIC
    int _split_string(std::string input, std::vector<std::string> output);

  private:
    // ___Member functions___
    CLI_LOCAL
    int _waitForServer();

    CLI_LOCAL
    int _getActionResult(std::shared_future<std::shared_ptr<GoalHandleItemSelectAction>> goal_future);

    // ___Attributes___
    rclcpp_action::Client<ItemSelectAction>::SharedPtr _action_client;
    std::string _action_name;
    Timer::SharedPtr _execution_timer;
    cli::GraspActionClient::Node::SharedPtr _grasp_node;
    // std::vector<std::string> _cli_options;
  };

} // namespace cli

#endif // _CLI_ITEM_SELECT
