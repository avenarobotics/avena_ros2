#ifndef CLI__CLI_ITEM_SELECT_HPP_
#define CLI__CLI_ITEM_SELECT_HPP_

// ___CPP___
#include <chrono>

// ___ROS2___
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

// #include "rclcpp_components/register_node_macro.hpp"

// ___Avena___
#include "custom_interfaces/action/place_action.hpp"
#include "cli/visibility_control.h"
#include "cli/util.hpp"


namespace cli
{
  using namespace std::chrono_literals;

  class CliPlaceClient : public rclcpp::Node
  {
  public:
    using PlaceAction = custom_interfaces::action::PlaceAction;
    using GoalHandleItemSelectAction = rclcpp_action::ClientGoalHandle<PlaceAction>;

    CLI_PUBLIC
    explicit CliPlaceClient(std::string action_name, const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    CLI_PUBLIC
    virtual ~CliPlaceClient(){};

    CLI_PUBLIC
    int sendGoal(std::vector<std::string> arguments);

  private:
    // ___Member functions___
    CLI_LOCAL
    int _waitForServer();

    CLI_LOCAL
    int _getActionResult(std::shared_future<std::shared_ptr<GoalHandleItemSelectAction>> goal_future);

    // ___Attributes___
    rclcpp_action::Client<PlaceAction>::SharedPtr _action_client;
    std::string _action_name;
    Timer::SharedPtr _execution_timer;
    std::vector<std::string> _cli_options;
  };

} // namespace cli

#endif // _CLI_ITEM_SELECT
