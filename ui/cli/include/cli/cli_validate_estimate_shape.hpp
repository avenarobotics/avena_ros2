#pragma once

// ___CPP___
#include <chrono>

// ___ROS2___
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

// #include "rclcpp_components/register_node_macro.hpp"

// ___Avena___
#include "custom_interfaces/action/validate_estimate_shape.hpp"
#include "cli/visibility_control.h"
#include "cli/util.hpp"


namespace cli
{
  using namespace std::chrono_literals;

  class CliValidateEstiamteShape : public rclcpp::Node
  {
  public:
    using ValidateAction = custom_interfaces::action::ValidateEstimateShape;
    using GoalHandleItemSelectAction = rclcpp_action::ClientGoalHandle<ValidateAction>;

    CLI_PUBLIC
    explicit CliValidateEstiamteShape(std::string action_name, const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    CLI_PUBLIC
    virtual ~CliValidateEstiamteShape(){};

    CLI_PUBLIC
    int sendGoal(float position, float orientation, float size, std::string real_scene_decscription_path = "");

  private:
    // ___Member functions___
    CLI_LOCAL
    int _waitForServer();

    CLI_LOCAL
    int _getActionResult(std::shared_future<std::shared_ptr<GoalHandleItemSelectAction>> goal_future);

    // ___Attributes___
    rclcpp_action::Client<ValidateAction>::SharedPtr _action_client;
    std::string _action_name;
    Timer::SharedPtr _execution_timer;
    std::vector<std::string> _cli_options;
  };

} // namespace cli