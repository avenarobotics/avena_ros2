#ifndef CLI__CLI_PATH_MANAGER_HPP_
#define CLI__CLI_PATH_MANAGER_HPP_

// ___CPP___
#include <chrono>

// ___ROS2___
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
// #include "rclcpp_components/register_node_macro.hpp"

// ___Avena___
#include "custom_interfaces/action/path_buffer.hpp"
#include "cli/visibility_control.h"
#include "cli/util.hpp"

namespace cli
{
  using namespace std::chrono_literals;

  class CliPathBufferClient : public rclcpp::Node
  {
  public:
    using PathBufferAction = custom_interfaces::action::PathBuffer;
    using GoalHandlePathBufferAction = rclcpp_action::ClientGoalHandle<PathBufferAction>;

    CLI_PUBLIC
    explicit CliPathBufferClient(std::string action_name, const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    CLI_PUBLIC
    virtual ~CliPathBufferClient(){};

    CLI_PUBLIC
    int sendGoal(std::string path_name);

  private:
    // ___Member functions___
    CLI_LOCAL
    int _waitForServer();

    CLI_LOCAL
    int _getActionResult(std::shared_future<std::shared_ptr<GoalHandlePathBufferAction>> goal_future);

    // ___Attributes___
    rclcpp_action::Client<PathBufferAction>::SharedPtr _action_client;
    std::string _action_name;
    Timer::SharedPtr _execution_timer;
  };

} // namespace cli

#endif // CLI__CLI_PATH_MANAGER_HPP_
