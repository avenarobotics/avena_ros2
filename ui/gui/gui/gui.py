# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Gui.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(266, 620)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(136, 138, 133))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(204, 207, 200))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Light, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 172, 166))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Midlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(68, 69, 66))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Dark, brush)
        brush = QtGui.QBrush(QtGui.QColor(91, 92, 89))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Mid, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.BrightText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(136, 138, 133))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Shadow, brush)
        brush = QtGui.QBrush(QtGui.QColor(195, 196, 194))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.AlternateBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 220))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ToolTipBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(136, 138, 133))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(204, 207, 200))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Light, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 172, 166))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Midlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(68, 69, 66))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Dark, brush)
        brush = QtGui.QBrush(QtGui.QColor(91, 92, 89))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Mid, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.BrightText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(136, 138, 133))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Shadow, brush)
        brush = QtGui.QBrush(QtGui.QColor(195, 196, 194))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.AlternateBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 220))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ToolTipBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(68, 69, 66))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(136, 138, 133))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(204, 207, 200))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Light, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 172, 166))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Midlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(68, 69, 66))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Dark, brush)
        brush = QtGui.QBrush(QtGui.QColor(91, 92, 89))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Mid, brush)
        brush = QtGui.QBrush(QtGui.QColor(68, 69, 66))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.BrightText, brush)
        brush = QtGui.QBrush(QtGui.QColor(68, 69, 66))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(136, 138, 133))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(136, 138, 133))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Shadow, brush)
        brush = QtGui.QBrush(QtGui.QColor(136, 138, 133))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.AlternateBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 220))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ToolTipBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ToolTipText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        MainWindow.setPalette(palette)
        MainWindow.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.initialize_btn = QtWidgets.QPushButton(self.centralwidget)
        self.initialize_btn.setEnabled(True)
        self.initialize_btn.setGeometry(QtCore.QRect(30, 20, 211, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.initialize_btn.setFont(font)
        self.initialize_btn.setStyleSheet("QPushButton {\n"
"    border-radius: 15px;\n"
"    background-color: rgb(186, 189, 182);\n"
"    color: rgb(46, 52, 54);\n"
"}\n"
"QPushButton:press {\n"
"    border-radius: 25px;\n"
"    background-color: rgb(46, 52, 54);\n"
"    color: rgb(191, 162, 64);\n"
"}\n"
"QPushButton:hover {\n"
"    background-color: rgba(64, 191, 64, 165);\n"
"}")
        self.initialize_btn.setObjectName("initialize_btn")
        self.exit_btn = QtWidgets.QPushButton(self.centralwidget)
        self.exit_btn.setEnabled(True)
        self.exit_btn.setGeometry(QtCore.QRect(30, 540, 211, 31))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.exit_btn.setFont(font)
        self.exit_btn.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.exit_btn.setStyleSheet("QPushButton {\n"
"    border-radius: 15px;\n"
"    background-color: rgb(186, 189, 182);\n"
"    color: rgb(46, 52, 54);\n"
"}\n"
"QPushButton:press {\n"
"    border-radius: 25px;\n"
"    background-color: rgb(46, 52, 54);\n"
"    color: rgb(191, 162, 64);\n"
"}\n"
"QPushButton:hover {\n"
"    background-color: rgba(191, 64, 64, 230);\n"
"}")
        self.exit_btn.setAutoDefault(False)
        self.exit_btn.setObjectName("exit_btn")
        self.data = QtWidgets.QTabWidget(self.centralwidget)
        self.data.setEnabled(False)
        self.data.setGeometry(QtCore.QRect(30, 80, 201, 271))
        self.data.setStyleSheet("")
        self.data.setObjectName("data")
        self.spawn = QtWidgets.QWidget()
        self.spawn.setObjectName("spawn")
        self.qty_box = QtWidgets.QSpinBox(self.spawn)
        self.qty_box.setGeometry(QtCore.QRect(20, 90, 51, 31))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.qty_box.setFont(font)
        self.qty_box.setStyleSheet("QSpinBox\n"
"{\n"
"    color: rgb(211, 215, 207);\n"
"    background-color: rgb(46, 52, 54);\n"
"    selection-background-color: rgb(46, 52, 54);\n"
"}")
        self.qty_box.setMaximum(10)
        self.qty_box.setObjectName("qty_box")
        self.spawn_btn = QtWidgets.QPushButton(self.spawn)
        self.spawn_btn.setEnabled(False)
        self.spawn_btn.setGeometry(QtCore.QRect(10, 190, 181, 31))
        self.spawn_btn.setStyleSheet("QPushButton {\n"
"    border-radius: 10px;\n"
"    background-color: rgb(186, 189, 182);\n"
"    color: rgb(46, 52, 54);\n"
"}\n"
"QPushButton:press {\n"
"    border-radius: 25px;\n"
"    background-color: rgb(46, 52, 54);\n"
"    color: rgb(191, 162, 64);\n"
"}\n"
"QPushButton:hover {\n"
"    background-color: rgba(64, 191, 64, 165);\n"
"}")
        self.spawn_btn.setObjectName("spawn_btn")
        self.label = QtWidgets.QLabel(self.spawn)
        self.label.setGeometry(QtCore.QRect(10, 10, 201, 17))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.spawn)
        self.label_2.setGeometry(QtCore.QRect(10, 70, 191, 17))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.spawn)
        self.label_3.setGeometry(QtCore.QRect(10, 130, 121, 17))
        self.label_3.setObjectName("label_3")
        self.item_drop = QtWidgets.QComboBox(self.spawn)
        self.item_drop.setEnabled(False)
        self.item_drop.setGeometry(QtCore.QRect(10, 30, 181, 21))
        self.item_drop.setStyleSheet("QComboBox\n"
"{\n"
"    color: rgba(0, 0, 0, 227);\n"
"    background-color: rgb(136, 138, 133);\n"
"    alternate-background-color: rgb(85, 87, 83);\n"
"    selection-background-color: rgb(85, 87, 83);\n"
"    selection-color: rgb(211, 215, 207);\n"
"}")
        self.item_drop.setObjectName("item_drop")
        self.progressBar = QtWidgets.QProgressBar(self.spawn)
        self.progressBar.setGeometry(QtCore.QRect(10, 280, 181, 23))
        self.progressBar.setStyleSheet("QProgressBar {\n"
"    background-color: rgb(243, 243, 243);\n"
"    color: rgb(46, 52, 54);\n"
"    border-style: solid;\n"
"    border-radius: 10px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    border-radius: 10px;\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(206, 92, 0, 255), stop:1 rgba(255, 255, 255, 255));\n"
"}")
        self.progressBar.setMinimum(0)
        self.progressBar.setProperty("value", 14)
        self.progressBar.setObjectName("progressBar")
        self.area_drop = QtWidgets.QComboBox(self.spawn)
        self.area_drop.setGeometry(QtCore.QRect(10, 150, 181, 21))
        self.area_drop.setStyleSheet("QComboBox\n"
"{\n"
"    color: rgba(0, 0, 0, 227);\n"
"    background-color: rgb(136, 138, 133);\n"
"    alternate-background-color: rgb(85, 87, 83);\n"
"    selection-background-color: rgb(85, 87, 83);\n"
"    selection-color: rgb(211, 215, 207);\n"
"}")
        self.area_drop.setObjectName("area_drop")
        self.data.addTab(self.spawn, "")
        self.move = QtWidgets.QWidget()
        self.move.setObjectName("move")
        self.label_4 = QtWidgets.QLabel(self.move)
        self.label_4.setGeometry(QtCore.QRect(10, 130, 201, 17))
        self.label_4.setObjectName("label_4")
        self.item_drop_2 = QtWidgets.QComboBox(self.move)
        self.item_drop_2.setGeometry(QtCore.QRect(10, 30, 171, 21))
        self.item_drop_2.setStyleSheet("QComboBox\n"
"{\n"
"    color: rgba(0, 0, 0, 227);\n"
"    background-color: rgb(136, 138, 133);\n"
"    alternate-background-color: rgb(85, 87, 83);\n"
"    selection-background-color: rgb(85, 87, 83);\n"
"    selection-color: rgb(211, 215, 207);\n"
"}")
        self.item_drop_2.setObjectName("item_drop_2")
        self.label_5 = QtWidgets.QLabel(self.move)
        self.label_5.setGeometry(QtCore.QRect(10, 70, 201, 17))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.move)
        self.label_6.setGeometry(QtCore.QRect(10, 10, 201, 17))
        self.label_6.setObjectName("label_6")
        self.area_drop_2 = QtWidgets.QComboBox(self.move)
        self.area_drop_2.setGeometry(QtCore.QRect(10, 90, 171, 21))
        self.area_drop_2.setStyleSheet("QComboBox\n"
"{\n"
"    color: rgba(0, 0, 0, 227);\n"
"    background-color: rgb(136, 138, 133);\n"
"    alternate-background-color: rgb(85, 87, 83);\n"
"    selection-background-color: rgb(85, 87, 83);\n"
"    selection-color: rgb(211, 215, 207);\n"
"}")
        self.area_drop_2.setObjectName("area_drop_2")
        self.area_drop_3 = QtWidgets.QComboBox(self.move)
        self.area_drop_3.setGeometry(QtCore.QRect(10, 150, 171, 21))
        self.area_drop_3.setStyleSheet("QComboBox\n"
"{\n"
"    color: rgba(0, 0, 0, 227);\n"
"    background-color: rgb(136, 138, 133);\n"
"    alternate-background-color: rgb(85, 87, 83);\n"
"    selection-background-color: rgb(85, 87, 83);\n"
"    selection-color: rgb(211, 215, 207);\n"
"}")
        self.area_drop_3.setObjectName("area_drop_3")
        self.move_btn = QtWidgets.QPushButton(self.move)
        self.move_btn.setGeometry(QtCore.QRect(10, 190, 181, 31))
        self.move_btn.setStyleSheet("QPushButton {\n"
"    border-radius: 10px;\n"
"    background-color: rgb(186, 189, 182);\n"
"    color: rgb(46, 52, 54);\n"
"}\n"
"QPushButton:press {\n"
"    border-radius: 25px;\n"
"    background-color: rgb(46, 52, 54);\n"
"    color: rgb(191, 162, 64);\n"
"}\n"
"QPushButton:hover {\n"
"    background-color: rgba(64, 191, 64, 165);\n"
"}")
        self.move_btn.setObjectName("move_btn")
        self.data.addTab(self.move, "")
        self.shutdown_btn = QtWidgets.QPushButton(self.centralwidget)
        self.shutdown_btn.setEnabled(False)
        self.shutdown_btn.setGeometry(QtCore.QRect(30, 490, 211, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.shutdown_btn.setFont(font)
        self.shutdown_btn.setStyleSheet("QPushButton {\n"
"    border-radius: 15px;\n"
"    background-color: rgb(186, 189, 182);\n"
"    color: rgb(46, 52, 54);\n"
"}\n"
"QPushButton:press {\n"
"    border-radius: 25px;\n"
"    background-color: rgb(46, 52, 54);\n"
"    color: rgb(191, 162, 64);\n"
"}\n"
"QPushButton:hover {\n"
"    background-color: rgba(191, 64, 64, 230);\n"
"}")
        self.shutdown_btn.setObjectName("shutdown_btn")
        self.clear_btn = QtWidgets.QPushButton(self.centralwidget)
        self.clear_btn.setEnabled(False)
        self.clear_btn.setGeometry(QtCore.QRect(30, 400, 211, 51))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.clear_btn.setFont(font)
        self.clear_btn.setStyleSheet("QPushButton {\n"
"    border-radius: 15px;\n"
"    background-color: rgb(186, 189, 182);\n"
"    color: rgb(46, 52, 54);\n"
"}\n"
"QPushButton:press {\n"
"    border-radius: 25px;\n"
"    background-color: rgb(46, 52, 54);\n"
"    color: rgb(191, 162, 64);\n"
"}\n"
"QPushButton:hover {\n"
"    background-color: rgba(191, 140, 64, 220);\n"
"}")
        self.clear_btn.setObjectName("clear_btn")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 266, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.data.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.initialize_btn.setText(_translate("MainWindow", "initialize system"))
        self.exit_btn.setText(_translate("MainWindow", "exit"))
        self.spawn_btn.setText(_translate("MainWindow", "spawn"))
        self.label.setText(_translate("MainWindow", "Choose a type of an object:"))
        self.label_2.setText(_translate("MainWindow", "Choose an object quantity:"))
        self.label_3.setText(_translate("MainWindow", "Choose an area:"))
        self.data.setTabText(self.data.indexOf(self.spawn), _translate("MainWindow", "spawn"))
        self.label_4.setText(_translate("MainWindow", "Choose an end area:"))
        self.label_5.setText(_translate("MainWindow", "Choose a start area:"))
        self.label_6.setText(_translate("MainWindow", "Choose an object:"))
        self.move_btn.setText(_translate("MainWindow", "move"))
        self.data.setTabText(self.data.indexOf(self.move), _translate("MainWindow", "move"))
        self.shutdown_btn.setText(_translate("MainWindow", "shutdown system"))
        self.clear_btn.setText(_translate("MainWindow", "clear scene"))
