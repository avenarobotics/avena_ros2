from rcl_interfaces.srv import GetParameters

import json
import rclpy

class GuiGetParams:

    def __del__(self):
        self.node.destroy_node()

    def __init__(self):

        rclpy.init(args=None)
        self.node = rclpy.create_node('minimal_client_async')
        self.gui_param = self.node.create_client(GetParameters, '/parameters_server/get_parameters')

        self.item_list=None
        self.area_list=None

    def gui_get_item(self):
        # print('\ngui_get_item @async_param.py (gui_get_item)\n', type(self.item_list), '\n', self.item_list)
        return self.item_list

    def gui_get_area(self):
        # print('\ngui_get_area @async_param.py (gui_get_area)\n', type(self.area_list), '\n', self.area_list)
        return self.area_list

    def gui_get_param(self):
        req = GetParameters.Request()
        req.names = ['labels', 'areas']
        while not self.gui_param.wait_for_service(timeout_sec=1.0):
            self.node.get_logger().info('service not available, waiting again...')

        future = self.gui_param.call_async(req)

        while rclpy.ok():
            rclpy.spin_once(self.node)
            if future.done():
                try:
                    result = future.result()
                except Exception as e:
                    self.node.get_logger().info('Service call failed %r' % (e,))
                else:
                    labels=result.values[0].string_value
                    areas=result.values[1].string_value

                    json_labels=json.loads(labels)
                    json_area=json.loads(areas)
                    
                    # creating list of items
                    my_list=[]
                    for item in json_labels:
                        if item['item']:
                            get_label=item['label']
                            my_list.append(get_label)

                    # getting rid of dublicates in my_list
                    self.item_list = my_list
                    self.area_list=list(json_area.keys())

                    # print('\nitem_list @async_param.py (gui_get_param)\n', type(self.item_list), '\n' , self.item_list)
                    # print('\narea_list @async_param.py (gui_get_param)\n', type(self.area_list), '\n', self.area_list, '\n')

                    break

if __name__ == '__main__':
    GuiGetParams().gui_get_param()

    rclpy.shutdown()