import sys
import subprocess
import json

from async_param import GuiGetParams

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from gui import Ui_MainWindow

class MainWindow:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.main_win=QMainWindow()
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self.main_win)

        self.ui.initialize_btn.clicked.connect(self.script)
        self.ui.exit_btn.clicked.connect(self.exit)
        self.ui.spawn_btn.clicked.connect(self.spawn)
        self.ui.clear_btn.clicked.connect(self.clear)
        self.ui.move_btn.clicked.connect(self.move)

        cmd_1='mate-terminal -- /bin/bash -c "ros2 launch avena_bringup main_intra_with_debug.launch.py"'
        p1=subprocess.Popen(cmd_1, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

        self.ros_param_server=GuiGetParams()

        # getting parameters item & area from async_param.py
        self.ros_param_server.gui_get_param()
        self.value_item_pick=self.ros_param_server.gui_get_item()
        self.value_area_pick=self.ros_param_server.gui_get_area()

        # adding items to dropdown menu
        for i in self.value_item_pick:
            self.ui.item_drop.addItem(i)
            self.ui.item_drop_2.addItem(i)

        # adding areas to dropdown menu
        for i in self.value_area_pick:
            self.ui.area_drop.addItem(i)
            self.ui.area_drop_2.addItem(i)
            self.ui.area_drop_3.addItem(i)

    def show(self):
        self.main_win.show()

    def script(self):
            print("initialize test")

            # widget update
            self.ui.data.setEnabled(True)
            self.ui.shutdown_btn.setEnabled(True)
            self.ui.spawn_btn.setEnabled(True)
            self.ui.item_drop.setEnabled(True)

            cmd_2='mate-terminal -- /opt/avena/coppelia_brain/coppeliaSim.sh ' \
                '/root/ros2_ws/src/avena_ros2/scenes/scenes/ava01_scene_coppelia_brain.ttt -s' 
            p2=subprocess.Popen(cmd_2, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

            cmd_3='mate-terminal -- /opt/avena/coppelia_camera/coppeliaSim.sh ' \
                '/root/ros2_ws/src/avena_ros2/scenes/scenes/ava01_scene_FrankaArm.ttt -s'
            p3=subprocess.Popen(cmd_3, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

            cmd_4='mate-terminal -- /bin/bash -c "ros2 run behaviour_trees main -t -f 10"'
            p4=subprocess.Popen(cmd_4, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

    def exit(self):
            exit()

    def spawn(self):

        # widget update
        self.ui.clear_btn.setEnabled(True)

        try:
            print('\n---------------------------------------------------------------------------------------')
            print('Item', \
                self.ui.item_drop.currentText(), 'in quantity of', \
                self.ui.qty_box.value(), 'to be spawned in', 
                self.ui.area_drop.currentText(), '!')
            print('---------------------------------------------------------------------------------------\n')
        except AttributeError:
            print('Please choose proper parameters for spawn.')
            return

        spawn_cmd='ros2 service call /control_coppelia_camera/spawn_items custom_interfaces/srv/SpawnItem ' \
            '"{type: {data: ' + self.ui.item_drop.currentText() + '}, random_type: false, random_position: true, '\
            'random_orientation: true, amount: {data: ' + str(self.ui.qty_box.value()) + '}, ' \
            'area_name: {data: ' + self.ui.area_drop.currentText() + '}}"'

        subprocess.run(spawn_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

    def clear(self):
        print('\n...clearing coppelia...\n....please wait...')
        cmd_clear=['ros2', 'service', 'call', '/control_coppelia_camera/clear_coppelia', 'custom_interfaces/srv/ClearCoppelia']
        clear=subprocess.run(cmd_clear, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
        print('\n' + clear.stdout.decode('UTF-8'))
        try:
            subprocess.run('action_home_position.sh', stdout=subprocess.PIPE, stderr=subprocess.STDOUT, \
                shell=False, check=True, timeout=10)
        except subprocess.CalledProcessError:
            print('Clearing coppelia failed.')
        except subprocess.TimeoutExpired:
            pass
        return

    def move(self):
        
        # print('moving test')
        try:
            print('\n---------------------------------------------------------------------------------------')
            print('Item', \
                self.ui.item_drop_2.currentText(), 'to be moved from', \
                self.ui.area_drop_2.currentText(), 'to', \
                self.ui.area_drop_3.currentText(), '.')
            print('---------------------------------------------------------------------------------------\n')
        # needed when parameter fields (eg. dropdown) empty as default    
        except AttributeError:
            print('Please choose proper parameters for move.')
            return
        except TypeError:
            print('Please choose proper parameters for move.')
            return     

        pick_pre1='ros2 run cli get_occupancy_grid'
        print(pick_pre1, 'in progress \nplease wait...\n')
        try:
            pre1=subprocess.run(pick_pre1, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, \
                shell=True, check=True, timeout=30)
            decode_pre1 = pre1.stdout.decode('UTF-8')
            print(decode_pre1)
        except subprocess.CalledProcessError:
            print('Getting occupancy grid failed!')
        except subprocess.TimeoutExpired:
            pass
            return

        pick_pre2='ros2 run cli manager_prepare_pick_and_place_data select_sort_policy nearest label ' \
                + self.ui.item_drop_2.currentText() + ' select_area_policy inside select_area ' \
                + self.ui.area_drop_2.currentText() + ' place_area_policy inside place_area ' \
                + self.ui.area_drop_3.currentText() + ' search_shift 0.1 nr_poses 2 constrain_value 15 ' \
                'ik_trials_number 150 max_final_states 4 ompl_compare_trials 2 min_path_points 200 ' \
                'max_time 6 max_simplification_time 20'
        print(pick_pre2 + '\n...in progress...' + '\n...please wait...\n')

        try:
            pre2=subprocess.run(pick_pre2, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, check=True)
            decode_pre2 = pre2.stdout.decode('UTF-8')
            print(decode_pre2)
        except subprocess.SubprocessError:
            print('Pick and place data process failed!')
            return
        
        p_and_p='action_pick_and_place.sh'
        print(p_and_p, '\n...in progress...\n...please wait...\n')

        try:
            temp=subprocess.run(p_and_p, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, check=True, timeout=60)
            print(p_and_p + ' succeded...\n')
            decode_temp = temp.stdout.decode('UTF-8')
            print(decode_temp)
        except subprocess.CalledProcessError:
            print('Script ' + p_and_p + ' failed')
            return
        except subprocess.TimeoutExpired:
            print(p_and_p + ' timeout reached and failed!')
            return

        home='action_home_position.sh'
        print(home + ' in progress' + '\nplease wait...\n')
        
        try:
            temp=subprocess.run(home, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, check=True, timeout=60)
            print(home + ' succeded...\n')
            decode_temp = temp.stdout.decode('UTF-8')
            print(decode_temp)
        except subprocess.CalledProcessError:
            print('Script ' + home + ' failed')
            return
        except subprocess.TimeoutExpired:
            print(home + ' timeout reached and failed!')
            return

if __name__=='__main__':
    app=QApplication(sys.argv)
    main_win=MainWindow()
    main_win.show()
    sys.exit(app.exec_())