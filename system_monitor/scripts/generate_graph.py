#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from file_read_backwards import FileReadBackwards

# import matplotlib.pyplot as plt
# from matplotlib.animation import FuncAnimation
import numpy as np

import sys
import os.path
import glob

class ModuleStatistics:
    def __init__(self, module_name, file):
        self.module_name = module_name
        self.file = file
        self.__last_lines = []
    
    def grabLastLines(self, number_of_lines):
        self.__last_lines = []
        with FileReadBackwards(self.file, encoding="ascii") as frb:
            for x in range(number_of_lines):
                self.__last_lines.append(frb.readline().split(','))
        frb.close()

    def getActionTimeinMilliseconds(self):
        return int(self.__last_lines[0][10]) / 1000000

    def getActionTimeInPercents(self):
        return (self.getActionTimeinMilliseconds() / 100) * 100

class SystemMonitorStatisticsNode(Node):
    def __init__(self):
        super().__init__('system_monitor_generate_graph')
        self.declare_parameter('dmp_dir')
        self.declare_parameter('modules', [])

        self.dmp_dir = self.get_parameter('dmp_dir').get_parameter_value().string_value
        print(self.dmp_dir)

        if not os.path.isdir(self.dmp_dir):
            raise Exception('DMP directory doesn\'t exists!')
            pass

        self.param_modules = self.get_parameter('modules')

        self.__modules = {}

        self.__loadModules()

        # self.get_logger().info("dir: %s, modules[]: %s" % (str(self.dmp_dir), str(self.__modules),))

        timer_period = 1  # seconds
        self.timer = self.create_timer(timer_period, self.__timer_callback)

    def __timer_callback(self):
        self.get_logger().info("Realoading modules data files")
        for module_name, module in self.__modules.items():
            module.grabLastLines(150) # 60s
        # generate stdout
        for module_name, module in self.__modules.items():
            self.get_logger().info("module: %s %.2f" % (module_name, module.getActionTimeInPercents()))

    def __isModulesOnList(self, module_name):
        if len(self.param_modules.value) == 0:
            return True

        if module_name in self.param_modules.value:
            return True
        
        return False

    def __loadModules(self):
        _modules = glob.glob(self.dmp_dir + '/*.dat')
        # print(_modules)
        for _module in _modules:
            (module_name, extension) = os.path.splitext(os.path.basename(_module))
            if self.__isModulesOnList(module_name):
                self.__modules[module_name] = ModuleStatistics(module_name, _module)
                print("Enabling module:" + module_name)
            else:
                pass
        pass


def main(args=None):
    # x = [1,2,3]
    # y = [4,6,5]
    # plt.plot(x,y)
    # plt.show()

    rclpy.init(args=args)

    node = SystemMonitorStatisticsNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

    # program_name = sys.argv[0]
    arguments = sys.argv[1:]
    for x in sys.argv:
        print("Argument: ", x)
    try:
        directory_name=sys.argv[1]
        print(directory_name)
        if os.path.isdir(directory_name):
            # directory exists
            pass
        else:
            print("Brak katalogu")
            raise Exception('spam', 'eggs')
        modules = glob.glob(directory_name + '/*.dat')
        # modules = os.listdir(directory_name)
        # print(modules)
        for module in modules:
            file_infos = os.path.splitext(os.path.basename(filepath_tar_gz))
            print(file_infos)

    except:
        print('Please pass directory_name')
    # main()

if __name__ == "__main__":
    main()
