# Copyright 2019 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch a talker and a listener in a component container."""
import os
import launch
import yaml
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    """Generate launch description with multiple components."""

    config_dir = os.path.join(get_package_share_directory('system_monitor'), 'config')   
    param_config = os.path.join(config_dir, "system_monitor.yaml")

    # with open(param_config, 'r') as f:
    #     modules = yaml.safe_load(f)

    container = ComposableNodeContainer(
            name='system_monitor_container',
            namespace='',
            package='rclcpp_components',
            executable='component_container',
            composable_node_descriptions=[
                ComposableNode(package='system_monitor', plugin='system_monitor::SystemMonitor', name='system_monitor', parameters=[])
            ],
            output='screen',
    )

    return launch.LaunchDescription([container])
