#ifndef SYSTEM_MONITOR__SYSTEM_MONITOR_HPP
#define SYSTEM_MONITOR__SYSTEM_MONITOR_HPP

// ___CPP___
#include <functional>
#include <memory>
#include <chrono>
#include <fstream>      // std::ofstream
#include <filesystem>

// __ROS__
#include <rclcpp/rclcpp.hpp>

// ___Avena___
#include "system_monitor/visibility_control.h"
#include "custom_interfaces/msg/watchdog.hpp"
#include "helpers_commons/watchdog.hpp"

namespace system_monitor
{
    // using namespace helpers;
    // using namespace std::placeholders;

    // struct AvenaMesh
    // {
    //     AvenaMesh()
    //         : cloud(new pcl::PointCloud<pcl::PointXYZ>)
    //     {
    //     }
    //     pcl::PointXYZ min_pt;
    //     pcl::PointXYZ max_pt;

    //     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;
    //     pcl::PolygonMesh mesh;
    // };

    class SystemMonitor : public rclcpp::Node
    {
    public:
        COMPOSITION_PUBLIC
        explicit SystemMonitor(const rclcpp::NodeOptions &options);

    private:
        void _statisticsCallback(const custom_interfaces::msg::Statistics::SharedPtr msg) const;
        void _heartbeatCallback(const custom_interfaces::msg::Heartbeat::SharedPtr msg) const;
        void _broadcastWatchdog();
        void _systemCheckTimerCallback();
        // void _startWatchdog();
        // void _broadcastHeartbeat();
        // void _watchdogTimerCallback();
        // void _watchdogStateSummary();
        // void _watchdogTopicCallback(const std_msgs::msg::String::SharedPtr msg) const


        // helpers::Watchdog::SharedPtr _watchdog;
        // rclcpp::Subscription<custom_interfaces::msg::Watchdog>::SharedPtr _sub_watchdog;
        rclcpp::Subscription<custom_interfaces::msg::Heartbeat>::SharedPtr  _sub_heartbeat;
        rclcpp::Subscription<custom_interfaces::msg::Statistics>::SharedPtr _sub_statistics;

        rclcpp::Publisher<custom_interfaces::msg::Watchdog>::SharedPtr _pub_watchdog;

        std::chrono::duration<double> _pub_watchdog_duration;
        std::chrono::duration<double> _system_check_duration;

        rclcpp::TimerBase::SharedPtr _pub_watchdog_timer;
        rclcpp::TimerBase::SharedPtr _system_check_timer;
        // rclcpp::TimerBase::SharedPtr _system_check_debug;
        // rclcpp::TimerBase::SharedPtr _starting_watchdog;
        // std::map<std::string, AvenaMesh> _meshes;

        std::time_t _result;
        bool _debug;
    };

} // namespace system_monitor

#endif //SYSTEM_MONITOR__SYSTEM_MONITOR_HPP
