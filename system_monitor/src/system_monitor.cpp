
#include "system_monitor/system_monitor.hpp"

namespace system_monitor
{
    using namespace std::chrono_literals;

    SystemMonitor::SystemMonitor(const rclcpp::NodeOptions &options)
        : Node("system_monitor", options),
          _pub_watchdog_duration(1.0 / 10.0),
          _system_check_duration(1.0 / 5.0),
          _debug(false)
    {
        // _watchdog = std::make_shared<helpers::Watchdog>(this, "system_monitor");
        auto qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));

        _sub_statistics = rclcpp::create_subscription<custom_interfaces::msg::Statistics>(this, "system_monitor/statistics", qos_settings, std::bind(&SystemMonitor::_statisticsCallback, this, std::placeholders::_1));
        _sub_heartbeat  = rclcpp::create_subscription<custom_interfaces::msg::Heartbeat>(this, "system_monitor/heartbeat", qos_settings, std::bind(&SystemMonitor::_heartbeatCallback, this, std::placeholders::_1));

        _pub_watchdog  = this->create_publisher<custom_interfaces::msg::Watchdog>("system_monitor/watchdog", qos_settings);

        _pub_watchdog_timer = this->create_wall_timer(_pub_watchdog_duration, std::bind(&SystemMonitor::_broadcastWatchdog, this));

        // System check
        _system_check_timer = this->create_wall_timer(_system_check_duration, std::bind(&SystemMonitor::_systemCheckTimerCallback, this));

        _result = std::time(nullptr); // needed for storing DMP files directory name
    }

    void SystemMonitor::_broadcastWatchdog()
    {
        // ProcStat * ps = get_proc_stat_info();
        custom_interfaces::msg::Watchdog::SharedPtr msg(new custom_interfaces::msg::Watchdog);
        msg->header.frame_id = "world";
        msg->header.stamp = this->now();
        this->_pub_watchdog->publish(*msg);
    }

    void SystemMonitor::_statisticsCallback(const custom_interfaces::msg::Statistics::SharedPtr msg) const 
    {
        std::filesystem::create_directories(std::to_string(_result));
        
        std::ofstream dat_file (std::to_string(_result) + "/" + msg->module_name + ".dat", std::ofstream::out | std::ofstream::app);
        dat_file  << std::to_string(msg->header.stamp.sec) << "," 
                  << std::to_string(msg->header.stamp.nanosec) << "," 
                  << std::to_string(CLOCKS_PER_SEC) << "," 
                  << std::to_string(msg->utime) << "," 
                  << std::to_string(msg->utime) << "," 
                  << std::to_string(msg->stime) << "," 
                  << std::to_string(msg->cutime) << "," 
                  << std::to_string(msg->cstime) << "," 
                  << std::to_string(msg->rss) << "," 
                  << std::to_string(msg->vsize) << ","
                  << std::to_string(msg->action_time) << std::endl;
        dat_file.close();

    }

    void SystemMonitor::_heartbeatCallback(const custom_interfaces::msg::Heartbeat::SharedPtr msg) const 
    {
        
    }

    void SystemMonitor::_systemCheckTimerCallback()
    {
        // RCLCPP_ERROR(_node_handler->get_logger(), "WATCHDOG");
        // this->_working = false;
    }

} // namespace system_monitor

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(system_monitor::SystemMonitor)
