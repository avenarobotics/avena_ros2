#include "rclcpp/rclcpp.hpp"
#include "virtualscene/functionalities.hpp"
#include "custom_interfaces/srv/clear_coppelia.hpp"
#include "virtualscene/param_parser.hpp"

void executeClear(std::shared_ptr<rclcpp::Node> node, std::string service_name);

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("virtualscene");

    try
    {
        std::string service_name;

        if(cmdOptionExists(argv, argv+argc, "coppelia_brain"))
        {
            service_name = "/control_coppelia_brain/clear_coppelia";
        }
        else 
        {
            service_name = "/control_coppelia_camera/clear_coppelia";
        }

        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Execution time: %.0f[ms]", funcTime(executeClear, node, service_name));
    }
    catch(const std::exception& e)
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), e.what());
    }
}

void executeClear(std::shared_ptr<rclcpp::Node> node, std::string service_name)
{
    rclcpp::Client<custom_interfaces::srv::ClearCoppelia>::SharedPtr client = 
        node->create_client<custom_interfaces::srv::ClearCoppelia>(service_name);

    auto request = std::make_shared<custom_interfaces::srv::ClearCoppelia::Request>();
    while(!client->wait_for_service(1s))
    {
        if (!rclcpp::ok()) {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
            throw std::runtime_error(std::string("Interrupted while waiting for the service. Exiting."));
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
    }

    auto result = client->async_send_request(request);

    if(rclcpp::spin_until_future_complete(node, result) == rclcpp::FutureReturnCode::SUCCESS)
    {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service returned: %s", (result.get()->success)?"SUCCESS":"FAILURE");
    }
}