#include "rclcpp/rclcpp.hpp"
#include "virtualscene/functionalities.hpp"
#include "virtualscene/param_parser.hpp"
#include "custom_interfaces/srv/set_simulation_status.hpp"

void executeSetSimulationStatus(std::shared_ptr<rclcpp::Node> node, std::string service_name, std::string operation);

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("virtualscene");

    try
    {
        std::string operation;
        if (cmdOptionExists(argv, argv+argc, "start"))
            operation = "start";
        else if (cmdOptionExists(argv, argv+argc, "stop"))
            operation = "stop";
        else if (cmdOptionExists(argv, argv+argc, "pause"))
            operation = "pause";
        else
            throw(std::runtime_error(std::string("unrecognized operation")));

        std::string service_name;

        if (cmdOptionExists(argv, argv + argc, "coppelia_brain"))
        {
            service_name = "/control_coppelia_brain/set_simulation_status";
        }
        else
        {
            service_name = "/control_coppelia_camera/set_simulation_status";
        }

        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Execution time: %.0f[ms]", funcTime(executeSetSimulationStatus, node, service_name, operation));
    }
    catch (const std::exception &e)
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), e.what());
    }
}

void executeSetSimulationStatus(std::shared_ptr<rclcpp::Node> node, std::string service_name, std::string operation)
{
    rclcpp::Client<custom_interfaces::srv::SetSimulationStatus>::SharedPtr client =
        node->create_client<custom_interfaces::srv::SetSimulationStatus>(service_name);

    auto request = std::make_shared<custom_interfaces::srv::SetSimulationStatus::Request>();
    request->status.data = operation;
    while (!client->wait_for_service(1s))
    {
        if (!rclcpp::ok())
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
            throw std::runtime_error(std::string("Interrupted while waiting for the service. Exiting."));
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
    }

    auto result = client->async_send_request(request);
}