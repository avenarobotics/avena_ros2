#include "virtualscene/functionalities.hpp"
#include <cmath>

geometry_msgs::msg::Quaternion toQuaternion(geometry_msgs::msg::Vector3 euler_angles)
{
    auto cy = static_cast<float>(cos(euler_angles.x * 0.5));
    auto sy = static_cast<float>(sin(euler_angles.x * 0.5));
    auto cp = static_cast<float>(cos(euler_angles.y * 0.5));
    auto sp = static_cast<float>(sin(euler_angles.y * 0.5));
    auto cr = static_cast<float>(cos(euler_angles.z * 0.5));
    auto sr = static_cast<float>(sin(euler_angles.z * 0.5));

    geometry_msgs::msg::Quaternion q;
    q.w = cr * cp * cy + sr * sp * sy;
    q.x = sr * cp * cy - cr * sp * sy;
    q.y = cr * sp * cy + sr * cp * sy;
    q.z = cr * cp * sy - sr * sp * cy;

    return q;
}

