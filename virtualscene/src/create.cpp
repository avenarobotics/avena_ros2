#include "custom_interfaces/srv/spawn_item.hpp"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"
#include "nlohmann/json.hpp"
#include "rclcpp/rclcpp.hpp"
#include "virtualscene/functionalities.hpp"
#include "virtualscene/param_parser.hpp"
#include <iomanip>
#include <iostream>

struct SpawnParams
{
    int amount;
    std::string type;
    geometry_msgs::msg::Point::SharedPtr position;
    geometry_msgs::msg::Vector3::SharedPtr euler_angles;
    std::string service_name;
    std::string point;
    std::string area;

    SpawnParams()
    {
        position = std::make_shared<geometry_msgs::msg::Point>();
        euler_angles = std::make_shared<geometry_msgs::msg::Vector3>();
        point = "";
        area = "";
        amount = 1;
    }

    friend std::ostream &operator<<(std::ostream &os, const SpawnParams &sp);
};

std::ostream &operator<<(std::ostream &os, const SpawnParams &sp)
{
    os << "Type: " << sp.type << '\n';
    os << "Amount: " << sp.amount << '\n';
    os << "Position: " << ((sp.position == nullptr) ? "NULL" : "USER_DEFINED") << '\n';
    os << "EulerAngles: " << ((sp.euler_angles == nullptr) ? "NULL" : "USER_DEFINED") << '\n';
    os << "Point: " << ((sp.point == "") ? "NULL" : sp.point) << '\n';
    os << "Area: " << ((sp.area == "") ? "NULL" : sp.area) << '\n';

    return os;
}

void extractPositionFromParameter(char *param, geometry_msgs::msg::Point::SharedPtr position, geometry_msgs::msg::Vector3::SharedPtr euler_angles);

void executeCreateItem(std::shared_ptr<rclcpp::Node> node, int argc, char **argv, std::string service_name);

void runCreateItemClient(std::shared_ptr<rclcpp::Node> node, std::string service_name, const SpawnParams &params);

int getAreasInfo(nlohmann::json &out_areas_info);

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);

    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("virtualscene");

    try
    {
        std::string service_name;

        if (cmdOptionExists(argv, argv + argc, "coppelia_brain"))
        {
            service_name = "/control_coppelia_brain/spawn_items";
        }
        else
        {
            service_name = "/control_coppelia_camera/spawn_items";
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Execution time: %.0f[ms]", funcTime(executeCreateItem, node, argc, argv, service_name));
    }
    catch (const std::exception &e)
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), e.what());
    }
}

void extractPositionFromParameter(char *param, geometry_msgs::msg::Point::SharedPtr position)
{
    double pos_array[3];
    char *token = strtok(param, ",");
    size_t i = 0;
    while (token != nullptr)
    {
        pos_array[i] = strtod(token, nullptr);
        token = strtok(nullptr, ",");
        i++;
    }
    if (i != 3)
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Wrong amount of position numbers: " + std::to_string(i) + " (expected 3)");
        throw(std::runtime_error(std::string("Wrong amount of position numbers: " + std::to_string(i) + " (expected 3)")));
    }
    position->x = pos_array[0];
    position->y = pos_array[1];
    position->z = pos_array[2];
}

void extractOrientationFromParameter(char *param, geometry_msgs::msg::Vector3::SharedPtr euler_angles)
{
    if (strcmp(param, "random") == 0)
    {
        std::cout << "Orientation shuld be random\n";
        euler_angles = nullptr;
        return;
    }

    double pos_array[3];
    char *token = strtok(param, ",");
    size_t i = 0;
    while (token != nullptr)
    {
        pos_array[i] = strtod(token, nullptr);
        token = strtok(nullptr, ",");
        i++;
    }
    if (i != 3)
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Wrong amount of oriantation numbers: " + std::to_string(i) + " (expected 3)");
        throw(std::runtime_error(std::string("Wrong amount of oriantation numbers: " + std::to_string(i) + " (expected 3)")));
    }
    euler_angles->x = pos_array[0];
    euler_angles->y = pos_array[1];
    euler_angles->z = pos_array[2];
}

int parseSceneDescription(std::shared_ptr<rclcpp::Node> node, std::string service_name, std::string scene_descritpion_path)
{
    if (scene_descritpion_path != "")
    {
        std::ifstream ifs(scene_descritpion_path);
        if (!ifs.good())
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), scene_descritpion_path + " doesn't exist!\n");
            return 1;
        }
        else
        {
            nlohmann::json real_scene_description = nlohmann::json::parse(ifs);
            for (auto item : real_scene_description["items"])
            {
                if (item.contains("label") && item.contains("grid_point_id") && item.contains("orientation"))
                {
                    SpawnParams spawn_params;
                    spawn_params.type = item["label"].get<std::string>();
                    spawn_params.point = item["grid_point_id"].get<std::string>();
                    Eigen::Vector3f euler_angles_eigen = helpers::vision::assignQuaternionFromJson(item["orientation"]).toRotationMatrix().eulerAngles(0, 1, 2);
                    spawn_params.euler_angles->x = euler_angles_eigen.x();
                    spawn_params.euler_angles->y = euler_angles_eigen.y();
                    spawn_params.euler_angles->z = euler_angles_eigen.z();
                    spawn_params.amount = 1;
                    spawn_params.position = nullptr;
                    spawn_params.service_name = service_name;
                    runCreateItemClient(node, service_name, spawn_params);
                }
                else
                {
                    RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), " Item description does not contain all required information to spawn item( you need to pass label,grid_point_id,orientation)");
                }
            }
        }
        ifs.close();
    }
    else
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Path is empty");
        return 1;
    }
    return 0;
}
void executeCreateItem(std::shared_ptr<rclcpp::Node> node, int argc, char **argv, std::string service_name)
{
    nlohmann::json areas_info;
    if (getAreasInfo(areas_info))
    {
        // RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Not able to read areas info from parameters server");
        throw(std::runtime_error(std::string("Not able to read areas info from parameters server")));
    }
    // RCLCPP_INFO_STREAM(rclcpp::get_logger("rclcpp"), areas_info);

    SpawnParams spawn_params;

    char *cmd_option;

    if (cmdOptionExists(argv, argv + argc, "scene_description"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "scene_description");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "You need to pass path to json file with scene description");
            throw(std::runtime_error(std::string("Specified path is empty")));
        }
        if (parseSceneDescription(node, service_name, std::string(cmd_option)))
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Cant spawn item from current file description");
            return;
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Succesfully spawned items from scene description");
        return;
    }

    if (!cmdOptionExists(argv, argv + argc, "label"))
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Label parameter is requred");
        throw(std::runtime_error(std::string("Label parameter is requred")));
    }
    else
    {
        cmd_option = getCmdOption(argv, argv + argc, "label");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Label parameter is requred");
            throw(std::runtime_error(std::string("Label parameter is requred")));
        }
        spawn_params.type = std::string(cmd_option);
    }

    if (cmdOptionExists(argv, argv + argc, "amount"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "amount");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Specify amount integer after amount flag");
            throw(std::runtime_error(std::string("Specify amount integer after amount flag")));
        }
        spawn_params.amount = std::stoi(std::string(cmd_option));
    }

    if (cmdOptionExists(argv, argv + argc, "position"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "position");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Specify position string after position flag");
            throw(std::runtime_error(std::string("Specify amount string after position flag")));
        }

        if (strcmp(cmd_option, "random") == 0)
            spawn_params.position = nullptr;
        else
            extractPositionFromParameter(cmd_option, spawn_params.position);
    }
    else
    {
        spawn_params.position = nullptr;
    }

    if (cmdOptionExists(argv, argv + argc, "orientation"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "orientation");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Specify orientation string after oriantation flag");
            throw(std::runtime_error(std::string("Specify orientation string after oriantation flag")));
        }

        if (strcmp(cmd_option, "random") == 0)
            spawn_params.euler_angles = nullptr;
        else
            extractOrientationFromParameter(cmd_option, spawn_params.euler_angles);
    }
    else
    {
        spawn_params.euler_angles = nullptr;
    }

    if (cmdOptionExists(argv, argv + argc, "area"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "area");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Specify area name after area flag");
            throw(std::runtime_error(std::string("Specify area string after area flag")));
        }
        spawn_params.area = std::string(cmd_option);
    }

    if (cmdOptionExists(argv, argv + argc, "point"))
    {
        cmd_option = getCmdOption(argv, argv + argc, "point");
        if (cmd_option == nullptr)
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Specify area name after point flag");
            throw(std::runtime_error(std::string("Specify point string after point flag")));
        }
        spawn_params.point = std::string(cmd_option);
    }

    if (spawn_params.area != "" && spawn_params.point != "")
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Providing point flag and area flag in the same time is illegal!");
        throw(std::runtime_error(std::string("Providing point flag and area flag in the same time is illegal!")));
    }

    //cout spawn params
    std::cerr << spawn_params << "\n";

    runCreateItemClient(node, service_name, spawn_params);
}

void runCreateItemClient(std::shared_ptr<rclcpp::Node> node, std::string service_name, const SpawnParams &params)
{

    rclcpp::Client<custom_interfaces::srv::SpawnItem>::SharedPtr client =
        node->create_client<custom_interfaces::srv::SpawnItem>(service_name);
    auto request = std::make_shared<custom_interfaces::srv::SpawnItem::Request>();
    request->amount.data = params.amount;
    if (params.type == "random")
        request->random_type = true;
    else
    {
        request->random_type = false;
        request->type.data = params.type;
    }
    if (params.position == nullptr)
        request->random_position = true;
    else
    {
        request->random_position = false;
        request->pose.position = *(params.position);
    }
    if (params.euler_angles == nullptr)
        request->random_orientation = true;
    else
    {
        request->random_orientation = false;
        request->pose.orientation = toQuaternion(*params.euler_angles);
    }
    request->area_name.data = params.area;
    request->point_name.data = params.point;
    while (!client->wait_for_service(1s))
    {
        if (!rclcpp::ok())
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
            throw std::runtime_error(std::string("Interrupted while waiting for the service. Exiting."));
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
    }
    auto result = client->async_send_request(request);
    if (rclcpp::spin_until_future_complete(node, result, std::chrono::seconds(20)) == rclcpp::FutureReturnCode::SUCCESS)
    {

        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service returned: %s", (result.get()->success) ? "SUCCESS" : "FAILURE");
    }
    else
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Service timeouted");
        return;
    }
}

int getAreasInfo(nlohmann::json &out_areas_info)
{
    out_areas_info = helpers::commons::getParameter("areas", std::chrono::seconds(1));
    if (out_areas_info.empty())
        return 1;

    // RCLCPP_WARN_STREAM(rclcpp::get_logger("rclcpp"), _areas_info);
    // RCLCPP_WARN(rclcpp::get_logger("rclcpp"), "DZIALA");
    return 0;
}
