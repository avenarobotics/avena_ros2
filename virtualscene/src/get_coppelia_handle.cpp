#include "rclcpp/rclcpp.hpp"
#include "virtualscene/functionalities.hpp"
#include "virtualscene/param_parser.hpp"
#include "custom_interfaces/srv/get_coppelia_handle.hpp"

void executeGetCoppeliaHandle(std::shared_ptr<rclcpp::Node> node, std::string rcl_client_get_service_name, int item_id);

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("virtualscene");

    try
    {
        std::string service_name = "/control_coppelia_brain/get_coppelia_handle";

        if(!cmdOptionExists(argv, argv+argc, "item_id"))
        {
            throw(std::runtime_error(std::string("item_id undefined")));
        }
        
        int item_id = std::stoi(getCmdOption(argv,argv+argc, "item_id"));
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Getting coppelia handle for: %d", item_id);
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Execution time: %.0f[ms]", funcTime(executeGetCoppeliaHandle, node, service_name, item_id));
    }
    catch (const std::exception &e)
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), e.what());
    }
}

void executeGetCoppeliaHandle(std::shared_ptr<rclcpp::Node> node, std::string service_name, int item_id)
{
    rclcpp::Client<custom_interfaces::srv::GetCoppeliaHandle>::SharedPtr client =
        node->create_client<custom_interfaces::srv::GetCoppeliaHandle>(service_name);

    auto request = std::make_shared<custom_interfaces::srv::GetCoppeliaHandle::Request>();
    request->item_id = item_id;
    while (!client->wait_for_service(1s))
    {
        if (!rclcpp::ok())
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
            throw std::runtime_error(std::string("Interrupted while waiting for the service. Exiting."));
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
    }

    auto result = client->async_send_request(request);

    if (rclcpp::spin_until_future_complete(node, result) == rclcpp::FutureReturnCode::SUCCESS)
    {
        std::cout << result.get()->coppelia_handle << '\n';
        std::cout << result.get()->coppelia_name.data << '\n';
    }
}