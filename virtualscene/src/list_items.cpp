#include "rclcpp/rclcpp.hpp"
#include "virtualscene/functionalities.hpp"
#include "virtualscene/param_parser.hpp"
#include "custom_interfaces/srv/list_spawned_items.hpp"

void executeListItems(std::shared_ptr<rclcpp::Node> node, std::string service_name);

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("virtualscene");

    try
    {
        std::string service_name;

        if (cmdOptionExists(argv, argv + argc, "coppelia_brain"))
        {
            service_name = "/control_coppelia_brain/list_spawned_items";
        }
        else
        {
            service_name = "/control_coppelia_camera/list_spawned_items";
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Execution time: %.0f[ms]", funcTime(executeListItems, node, service_name));
    }
    catch (const std::exception &e)
    {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), e.what());
    }
}

void executeListItems(std::shared_ptr<rclcpp::Node> node, std::string service_name)
{
    rclcpp::Client<custom_interfaces::srv::ListSpawnedItems>::SharedPtr client =
        node->create_client<custom_interfaces::srv::ListSpawnedItems>(service_name);

    auto request = std::make_shared<custom_interfaces::srv::ListSpawnedItems::Request>();
    while (!client->wait_for_service(1s))
    {
        if (!rclcpp::ok())
        {
            RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
            throw std::runtime_error(std::string("Interrupted while waiting for the service. Exiting."));
        }
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
    }

    auto result = client->async_send_request(request);

    if (rclcpp::spin_until_future_complete(node, result) == rclcpp::FutureReturnCode::SUCCESS)
    {
        std::for_each(
            result.get()->items.begin(),
            result.get()->items.end(),
            [](std_msgs::msg::String single_item) {
                std::cout << single_item.data << '\n';
            });
    }
}