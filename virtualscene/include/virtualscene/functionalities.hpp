#pragma once
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/quaternion.hpp"
#include "geometry_msgs/msg/vector3.hpp"
#include "geometry_msgs/msg/point.hpp"
#include "custom_interfaces/srv/spawn_item.hpp"
#include <chrono>

using namespace std::chrono_literals;

using TimeVar = std::chrono::high_resolution_clock::time_point;

#define duration(a) std::chrono::duration_cast<std::chrono::milliseconds>(a).count()
#define timeNow() std::chrono::high_resolution_clock::now()

/**
 *
 * @tparam F -> template function type
 * @tparam Args -> template arguments type
 * @param func -> pointer to function that will be measured
 * @param args -> arguments that will be passed to the called function
 * @return high resolution timestamp from start to end of execution
 */
template<typename F, typename... Args>
double funcTime(F func, Args&&... args){
    TimeVar t1=timeNow();
    func(std::forward<Args>(args)...);
    return duration(timeNow()-t1);
}


/**
 *
 * @param yaw
 * @param pitch
 * @param roll
 * @return quaternion based on parameters values
 */
geometry_msgs::msg::Quaternion toQuaternion(geometry_msgs::msg::Vector3 euler_angles);
