#pragma once

#include <string>
#include <algorithm>

/**
 *
 * @param begin -> pointer on first cmd argument
 * @param end -> pointer on last cmd argument
 * @param option -> searched option
 * @return -> pointer on next argument after the searched one or nullptr if cannot find
 */
char *getCmdOption(char **begin, char **end, const std::string &option);

/**
 *
 * @param begin -> pointer on first cmd argument
 * @param end -> pointer on last cmd argument
 * @param option -> searched option
 * @return true if option param exist, false otherwise
 */
bool cmdOptionExists(char **begin, char **end, const std::string &option);