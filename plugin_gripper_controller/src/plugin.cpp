#include "plugin_gripper.h"
#include "simPlusPlus/Plugin.h"

GripperController::GripperController()
{
}

void GripperController::onStart()
{
    if (!registerScriptStuff())
        throw std::runtime_error("script stuff initialization failed");

    _node_gripper = rclcpp::Node::make_shared("gripper_state");

    setExtVersion("Plugin for publishing gripper states");
    setBuildDate(BUILD_DATE);
    rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
    _pub_gripper_state = _node_gripper->create_publisher<sensor_msgs::msg::JointState>("gripper_joint_states", latching_qos);
    _timer = _node_gripper->create_wall_timer(10ms, std::bind(&GripperController::broadcastGripperState, this));
    _subscribe_gripper_status = _node_gripper->create_subscription<sensor_msgs::msg::JointState>("/gripper_control_status", latching_qos, std::bind(&GripperController::_setGripperStatusCallback, this, std::placeholders::_1));
    // _watchdog = std::make_shared<helpers::Watchdog>(_node_gripper.get(), "system_monitor");
}

void GripperController::onInstancePass(const sim::InstancePassFlags &flags, bool first)
{
    rclcpp::spin_some(_node_gripper);
}

void GripperController::_setGripperStatusCallback(const sensor_msgs::msg::JointState::SharedPtr gripper_set_status_msg)
{

    _gripper_set_status = gripper_set_status_msg;
    _setGripperPositionByVelocity();
}

void GripperController::broadcastGripperState()
{
    sensor_msgs::msg::JointState::SharedPtr gripper_state_msg(new sensor_msgs::msg::JointState);
    std::vector<float> positions;
    std::vector<float> velocities;
    std::vector<int> force_sensor;
    std::vector<int> gripper_handles;
    std::vector<int> collision_handles;

    gripper_handles.resize(3);
    collision_handles.resize(2);
    force_sensor.resize(2);

    gripper_handles[0] = simGetObjectHandle("Panda_gripper");
    gripper_handles[1] = simGetObjectHandle("Panda_gripper_joint1");
    gripper_handles[2] = simGetObjectHandle("Panda_gripper_joint2");

    collision_handles[0] = simGetCollisionHandle("left_finger");
    collision_handles[1] = simGetCollisionHandle("right_finger");

    //read position and velocity from robot gripper
    for (size_t i = 0; i < gripper_handles.size(); i++)
    {
        float position;
        float velocity;
        simGetJointPosition(gripper_handles[i], &position);
        simGetJointTargetVelocity(gripper_handles[i], &velocity);
        positions.push_back(position);
        velocities.push_back(velocity);
    }
    //check if collision exist
    for (size_t i = 0; i < 2; i++)
    {
        int effort = simReadCollision(collision_handles[i]);
        if (effort == -1)
            force_sensor[i] = 0;
        else
            force_sensor[i] = effort;
    }

    // create message for gripper state publisher, effort - simulation check collision
    gripper_state_msg->name = {"Panda_gripper", "Panda_gripper_joint1", "Panda_gripper_joint2"};
    gripper_state_msg->position = {positions[0], positions[1], positions[2]};
    gripper_state_msg->velocity = {velocities[0], velocities[1], velocities[2]};
    gripper_state_msg->effort = {0, static_cast<float>(force_sensor[0]), static_cast<float>(force_sensor[1])};
    gripper_state_msg->header.stamp = _node_gripper->now();
    _pub_gripper_state->publish(*gripper_state_msg);
}

void GripperController::_setGripperPositionByVelocity()
{
    std::vector<int> gripper_handles;
    gripper_handles.resize(3);
    gripper_handles[0] = simGetObjectHandle("Panda_gripper");
    gripper_handles[1] = simGetObjectHandle("Panda_gripper_joint1");
    gripper_handles[2] = simGetObjectHandle("Panda_gripper_joint2");

    if (!_gripper_set_status || _gripper_set_status->header.stamp == builtin_interfaces::msg::Time())
    {
        RCLCPP_INFO(_node_gripper->get_logger(), "Recieved incorrect values for gripper");
        for (size_t i = 1; i < _gripper_set_status->position.size(); i++)
            simSetJointTargetVelocity(gripper_handles[i], 0);
    }
    else
    { //set gripper velocity calculated in controller
        for (size_t i = 1; i < _gripper_set_status->position.size(); i++)
            simSetJointTargetVelocity(gripper_handles[i], _gripper_set_status->velocity[i]);
    }
}

SIM_PLUGIN(PLUGIN_NAME, 1, GripperController)
#include "stubsPlusPlus.cpp"
