#ifndef SCENE_PUBLISHER_COMPONENT_HPP
#define SCENE_PUBLISHER_COMPONENT_HPP

// ___CPP___
#include <functional>
#include <memory>
#include <string>
#include <tuple>

// __ROS__
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include <rclcpp/parameter.hpp>

// ___AVENA___
#include "scene_publisher/visibility_control.h"
#include "custom_interfaces/action/simple_action.hpp"
#include "custom_interfaces/msg/change_detect.hpp"
#include "custom_interfaces/msg/rgb_images.hpp"
#include "custom_interfaces/msg/depth_images.hpp"
#include "custom_interfaces/msg/merged_ptcld_filtered.hpp"
#include "custom_interfaces/msg/scene_debug_data.hpp"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"

namespace scene_publisher
{
  using namespace custom_interfaces::msg; // usage only in this namespace, so not a big problem

  class ScenePublisher : public rclcpp::Node, public helpers::WatchdogInterface
  {
  public:
    explicit ScenePublisher(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    using Action = custom_interfaces::action::SimpleAction;
    using GoalHandle = rclcpp_action::ServerGoalHandle<Action>;

    void initNode() override;
    void shutDownNode() override;

  private:
    helpers::Watchdog::SharedPtr _watchdog;

    std::tuple<RgbImages::UniquePtr,
               DepthImages::UniquePtr,
               MergedPtcldFiltered::UniquePtr,
               SceneDebugData::UniquePtr,
               sensor_msgs::msg::PointCloud2::UniquePtr>
    _prepareOutputMessages(const custom_interfaces::msg::ChangeDetect::SharedPtr &change_detect_data);

    //ROS
    rclcpp_action::Server<Action>::SharedPtr _action_server;
    rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, Action::Goal::ConstSharedPtr goal);
    rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandle> goal_handle);
    void _handleAccepted(const std::shared_ptr<GoalHandle> goal_handle);
    void _execute(const std::shared_ptr<GoalHandle> goal_handle);
    void _initializeServers();

    rclcpp::Subscription<ChangeDetect>::SharedPtr _change_detect_sub;
    ChangeDetect::SharedPtr _change_detect_data;

    rclcpp::Publisher<RgbImages>::SharedPtr _rgb_images_publisher;
    rclcpp::Publisher<DepthImages>::SharedPtr _depth_images_publisher;
    rclcpp::Publisher<MergedPtcldFiltered>::SharedPtr _merged_ptcld_filtered_publisher;
    rclcpp::Publisher<SceneDebugData>::SharedPtr _scene_debug_data_publisher;
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr _diff_ptcld_publisher;
    bool _cant_touch_this;








  };

} // namespace scene_publisher

#endif
