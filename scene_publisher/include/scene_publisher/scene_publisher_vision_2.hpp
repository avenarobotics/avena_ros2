#ifndef SCENE_PUBLISHER_COMPONENT_HPP
#define SCENE_PUBLISHER_COMPONENT_HPP

// ___CPP___
#include <functional>
#include <memory>
#include <string>
#include <tuple>

// __ROS__
#include <rclcpp/rclcpp.hpp>
#include <rclcpp_action/rclcpp_action.hpp>
#include <rclcpp/parameter.hpp>

// ___AVENA___
#include "custom_interfaces/action/scene_publisher.hpp"
#include "custom_interfaces/msg/change_detect.hpp"
#include "custom_interfaces/msg/rgb_images.hpp"
#include "custom_interfaces/msg/depth_images.hpp"
#include "custom_interfaces/msg/ptclds.hpp"
#include "custom_interfaces/msg/scene_debug_data.hpp"
#include "helpers_commons/helpers_commons.hpp"
#include "helpers_vision/helpers_vision.hpp"

// ___Package___
#include "scene_publisher/visibility_control.h"
#include "scene_publisher/buffer.hpp"

namespace scene_publisher
{
  using namespace custom_interfaces::msg; // usage only in this namespace, so not a big problem
  using json = nlohmann::json;

  class ScenePublisher : public rclcpp::Node
  {
  public:
    explicit ScenePublisher(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());

    using Action = custom_interfaces::action::ScenePublisher;
    using GoalHandle = rclcpp_action::ServerGoalHandle<Action>;

  private:
    // ___Private functions___

    /**
     * @brief Prepare output messages taking data from internal buffers with requested timestamp
     * 
     * @param request_timestamp timestamp which high logic requested for output messages
     * @return std::tuple<RgbImages::UniquePtr,
     * DepthImages::UniquePtr,
     * Ptclds::UniquePtr> 
     */
    std::tuple<RgbImages::UniquePtr,
               DepthImages::UniquePtr,
               Ptclds::UniquePtr>
    _prepareOutputMessages(const builtin_interfaces::msg::Time &request_timestamp);

    /**
     * @brief Handling of incoming goal for action
     * 
     * @param uuid 
     * @param goal 
     * @return rclcpp_action::GoalResponse 
     */
    rclcpp_action::GoalResponse _handleGoal(const rclcpp_action::GoalUUID &uuid, Action::Goal::ConstSharedPtr goal);

    /**
     * @brief Handling of cancel request for action
     * 
     * @param goal_handle 
     * @return rclcpp_action::CancelResponse 
     */
    rclcpp_action::CancelResponse _handleCancel(const std::shared_ptr<GoalHandle> goal_handle);

    /**
     * @brief Handle goal which was accepted in _handleGoal function
     * 
     * @param goal_handle 
     */
    void _handleAccepted(const std::shared_ptr<GoalHandle> goal_handle);

    /**
     * @brief Main logic function which handles all processing
     * 
     * @param goal_handle 
     */
    void _execute(const std::shared_ptr<GoalHandle> goal_handle);

    /**
     * @brief Initialize all ROS services and action servers
     * 
     */
    void _initializeServers();

    /**
     * @brief Initialize all publishers with correct topic names, types and QoS values
     * 
     */
    void _initializePublishers(const rclcpp::QoS &qos_settings);

    /**
     * @brief Initialize all subscribers with correct topic names, types and QoS values and callbacks
     * 
     */
    void _initializeSubscribers(const rclcpp::QoS &qos_settings);

    /**
     * @brief Get cutoff time in seconds for data buffers
     * 
     * @return std::chrono::duration<double> 
     */
    std::chrono::duration<double> _getCutoffTime();

    // ___Memeber attributes___
    helpers::Watchdog::SharedPtr _watchdog;
    rclcpp_action::Server<Action>::SharedPtr _action_server;

    rclcpp::Subscription<RgbImages>::SharedPtr _rgb_images_sub;
    rclcpp::Subscription<DepthImages>::SharedPtr _depth_images_sub;
    rclcpp::Subscription<Ptclds>::SharedPtr _ptclds_sub;

    rclcpp::Publisher<RgbImages>::SharedPtr _rgb_images_pub;
    rclcpp::Publisher<DepthImages>::SharedPtr _depth_images_pub;
    rclcpp::Publisher<Ptclds>::SharedPtr _ptclds_pub;

    // Buffers
    Buffer<RgbImages> _rgb_images_buffer;
    Buffer<DepthImages> _depth_images_buffer;
    Buffer<Ptclds> _ptclds_buffer;
  };

} // namespace scene_publisher

#endif
