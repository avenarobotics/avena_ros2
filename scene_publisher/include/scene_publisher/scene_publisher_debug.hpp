#ifndef SCENE_PUBLISHER_DEBUG__COMPONENT_HPP_
#define SCENE_PUBLISHER_DEBUG__COMPONENT_HPP_

// ___CPP__
#include <memory>
#include <assert.h>
#include <sstream>

// ___ROS2___
#include <rclcpp/rclcpp.hpp>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
// #include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
// #include <message_filters/message_traits.h>

// ___Avena packages___
#include "mysql_connector.h"
#include "scene_publisher/visibility_control.h"
#include "custom_interfaces/msg/rgb_images.hpp"
#include "custom_interfaces/msg/depth_images.hpp"
#include "custom_interfaces/msg/merged_ptcld_filtered.hpp"
#include "custom_interfaces/msg/scene_debug_data.hpp"
#include "helpers_vision/helpers_vision.hpp"

namespace ros2mysql
{

  class ScenePublisher : public rclcpp::Node
  {
  public:
    COMPOSITION_PUBLIC
    explicit ScenePublisher(const rclcpp::NodeOptions &options);

    using Synchronizer = message_filters::TimeSynchronizer<custom_interfaces::msg::RgbImages,
                                                           custom_interfaces::msg::DepthImages,
                                                           custom_interfaces::msg::MergedPtcldFiltered,
                                                           custom_interfaces::msg::SceneDebugData>;

  private:
    /**;
     * Configures component.
     */
    COMPOSITION_LOCAL
    void _configure();

    /**;
     * Declares the parameter using rcl_interfaces.
     */
    COMPOSITION_LOCAL
    void _initializeParameters();

    COMPOSITION_LOCAL
    void _initializeSyncSubscriber();

    COMPOSITION_LOCAL
    void _syncSceneTopics(const custom_interfaces::msg::RgbImages::ConstSharedPtr &rgb_images_msg,
                          const custom_interfaces::msg::DepthImages::ConstSharedPtr &depth_images_msg,
                          const custom_interfaces::msg::MergedPtcldFiltered::ConstSharedPtr &merged_ptcld_filtered_msg,
                          const custom_interfaces::msg::SceneDebugData::ConstSharedPtr &scene_debug_data_msg);

    std::unique_ptr<MysqlConnector> _db;
    std::string _host;
    std::string _port;
    std::string _db_name;
    std::string _username;
    std::string _password;
    bool _debug;

    message_filters::Subscriber<custom_interfaces::msg::RgbImages> _rgb_images_sub;
    message_filters::Subscriber<custom_interfaces::msg::DepthImages> _depth_images_sub;
    message_filters::Subscriber<custom_interfaces::msg::MergedPtcldFiltered> _merged_ptcld_filtered_sub;
    message_filters::Subscriber<custom_interfaces::msg::SceneDebugData> _scene_debug_data_sub;

    std::shared_ptr<Synchronizer> _syncApproximate;
  };

} // namespace ros2mysql

#endif // SCENE_PUBLISHER_DEBUG__COMPONENT_HPP_
