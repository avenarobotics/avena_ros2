#include "scene_publisher/scene_publisher_debug.hpp"

#include <iostream>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include <rclcpp/parameter.hpp>

namespace ros2mysql
{
    using json = nlohmann::json;
    
    ScenePublisher::ScenePublisher(const rclcpp::NodeOptions &options)
        : Node("scene_publisher_debug", options)
    {
        RCLCPP_INFO(get_logger(), "Scene publisher debug initialization...");

        _initializeParameters();
        _configure();
        _initializeSyncSubscriber();

        try
        {
            this->_db = std::make_unique<MysqlConnector>(this->_host, this->_port, this->_db_name, this->_username, this->_password, this->_debug);
        }
        catch (const network_exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
        }
    }

    void ScenePublisher::_initializeSyncSubscriber()
    {
        RCLCPP_INFO(get_logger(), "Initializing synchronized subscriber.");

        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local();

        _rgb_images_sub.subscribe(this, "rgb_images", qos_settings.get_rmw_qos_profile());
        _depth_images_sub.subscribe(this, "depth_images", qos_settings.get_rmw_qos_profile());
        _merged_ptcld_filtered_sub.subscribe(this, "merged_ptcld_filtered", qos_settings.get_rmw_qos_profile());
        _scene_debug_data_sub.subscribe(this, "scene_debug_data", qos_settings.get_rmw_qos_profile());

        _syncApproximate = std::make_shared<Synchronizer>(_rgb_images_sub, _depth_images_sub, _merged_ptcld_filtered_sub, _scene_debug_data_sub, 10);
        _syncApproximate->registerCallback(std::bind(&ScenePublisher::_syncSceneTopics, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
    }

    void ScenePublisher::_syncSceneTopics(const custom_interfaces::msg::RgbImages::ConstSharedPtr &rgb_images_msg,
                                          const custom_interfaces::msg::DepthImages::ConstSharedPtr &depth_images_msg,
                                          const custom_interfaces::msg::MergedPtcldFiltered::ConstSharedPtr &merged_ptcld_filtered_msg,
                                          const custom_interfaces::msg::SceneDebugData::ConstSharedPtr &scene_debug_data_msg)
    {
        RCLCPP_INFO(get_logger(), "Received messages from security controller topics: \"rgb_images\", \"depth_images\", \"merged_ptcld_filtered\", \"scene_debug_data\"");
        auto validateHeader = [this](const std_msgs::msg::Header &header) -> bool {
            return header.stamp == builtin_interfaces::msg::Time();
        };

        try
        {
            // Validate input messages headers
            if (validateHeader(rgb_images_msg->header) ||
                validateHeader(depth_images_msg->header) ||
                validateHeader(merged_ptcld_filtered_msg->header) ||
                validateHeader(scene_debug_data_msg->header))
            {
                RCLCPP_WARN(get_logger(), "Invalid header in one of the incoming messages.");
                return;
            }

            // Unpack msg
            // Diff info
            float last_change_timestamp = scene_debug_data_msg->timestamp.sec;
            float scene_change_percentage = scene_debug_data_msg->scene_change_percentage;

            auto convertRosCloudToPcl = [this](const sensor_msgs::msg::PointCloud2 &cloud) -> pcl::PointCloud<pcl::PointXYZ>::Ptr {
                pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_cloud(new pcl::PointCloud<pcl::PointXYZ>);
                helpers::converters::rosPtcldtoPcl<pcl::PointXYZ>(cloud, pcl_cloud);
                return pcl_cloud;
            };

            auto cam1_ptcld = convertRosCloudToPcl(scene_debug_data_msg->cam1_ptcld);
            auto cam1_ptcld_trans = convertRosCloudToPcl(scene_debug_data_msg->cam1_ptcld_trans);
            auto cam2_ptcld = convertRosCloudToPcl(scene_debug_data_msg->cam2_ptcld);
            auto cam2_ptcld_trans = convertRosCloudToPcl(scene_debug_data_msg->cam2_ptcld_trans);
            auto merged_ptcld = convertRosCloudToPcl(scene_debug_data_msg->merged_ptcld);
            auto merged_ptcld_filtered = convertRosCloudToPcl(merged_ptcld_filtered_msg->merged_ptcld_filtered);
            auto diff_ptcld = convertRosCloudToPcl(scene_debug_data_msg->diff_ptcld);

            cv::Mat cam1_rgb;
            cv::Mat cam1_depth;
            cv::Mat cam2_rgb;
            cv::Mat cam2_depth;

            helpers::converters::rosImageToCV(rgb_images_msg->cam1_rgb, cam1_rgb);
            helpers::converters::rosImageToCV(depth_images_msg->cam1_depth, cam1_depth);
            helpers::converters::rosImageToCV(rgb_images_msg->cam2_rgb, cam2_rgb);
            helpers::converters::rosImageToCV(depth_images_msg->cam2_depth, cam2_depth);

            json scene_diff_info;
            scene_diff_info["last_change_timestamp"] = last_change_timestamp;
            scene_diff_info["scene_change_percentage"] = scene_change_percentage;
            scene_diff_info["areas_change"] = json::parse(scene_debug_data_msg->areas_change);

            scene_t scene_data;
            scene_data.scene_diff_info = scene_diff_info;
            helpers::converters::imageToStream(cam1_rgb, scene_data.camera_1_rgb);
            helpers::converters::imageToStream(cam1_depth, scene_data.camera_1_depth);
            helpers::converters::imageToStream(cam2_rgb, scene_data.camera_2_rgb);
            helpers::converters::imageToStream(cam2_depth, scene_data.camera_2_depth);
            helpers::converters::pointcloudToStream(*(cam1_ptcld), scene_data.pcl_camera_1);
            helpers::converters::pointcloudToStream(*(cam2_ptcld), scene_data.pcl_camera_2);
            helpers::converters::pointcloudToStream(*(merged_ptcld), scene_data.merged_pcl);
            helpers::converters::pointcloudToStream(*(diff_ptcld), scene_data.pcl_scene_diff);
            helpers::converters::pointcloudToStream(*(merged_ptcld_filtered), scene_data.pcl_self_filtered);

            // SAVE DATA TO DATABASE ----------------------
            // clear database
            _db->clearTable("scene");

            // save data
            _db->setScene(&scene_data);
        }
        catch (const network_exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
        }
        catch (const query_exception &e)
        {
            RCLCPP_ERROR(this->get_logger(), e.what());
        }
    }

    void ScenePublisher::_configure()
    {
        this->get_parameter<std::string>("host", _host);
        this->get_parameter<std::string>("port", _port);
        this->get_parameter<std::string>("db_name", _db_name);
        this->get_parameter<std::string>("username", _username);
        this->get_parameter<std::string>("password", _password);
        this->get_parameter<bool>("debug", _debug);
    }

    void ScenePublisher::_initializeParameters()
    {
        rcl_interfaces::msg::ParameterDescriptor host_descriptor;
        host_descriptor.name = "host";
        host_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("host", "", host_descriptor);

        rcl_interfaces::msg::ParameterDescriptor port_descriptor;
        port_descriptor.name = "port";
        port_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("port", "", port_descriptor);

        rcl_interfaces::msg::ParameterDescriptor db_name_descriptor;
        db_name_descriptor.name = "db_name";
        db_name_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("db_name", "", db_name_descriptor);

        rcl_interfaces::msg::ParameterDescriptor username_descriptor;
        username_descriptor.name = "username";
        username_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("username", "", username_descriptor);

        rcl_interfaces::msg::ParameterDescriptor password_descriptor;
        password_descriptor.name = "password";
        password_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_STRING;
        this->declare_parameter("password", "", password_descriptor);

        rcl_interfaces::msg::ParameterDescriptor debug_descriptor;
        debug_descriptor.name = "debug";
        debug_descriptor.type = rcl_interfaces::msg::ParameterType::PARAMETER_BOOL;
        this->declare_parameter("debug", false, debug_descriptor);
    }

} // namespace ros2mysql

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(ros2mysql::ScenePublisher)
