#include "scene_publisher/scene_publisher.hpp"


namespace scene_publisher
{
    ScenePublisher::ScenePublisher(const rclcpp::NodeOptions &options)
        : Node("scene_publisher", options)
    {
        helpers::commons::setLoggerLevelFromParameter(this);
        _cant_touch_this = false;
        rclcpp::QoS qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        _change_detect_sub = create_subscription<custom_interfaces::msg::ChangeDetect>("change_detect", qos_settings,
                                                                                       [this](const custom_interfaces::msg::ChangeDetect::SharedPtr change_detect_msg) {
                                                                                           if(_cant_touch_this)
                                                                                             return;
                                                                                           _change_detect_data = change_detect_msg;
                                                                                            helpers::TicTok::GetInstance()->stop();
                                                                                       });

        qos_settings = rclcpp::QoS(rclcpp::KeepLast(1)); //.transient_local();
        _rgb_images_publisher = create_publisher<custom_interfaces::msg::RgbImages>("rgb_images", qos_settings);
        _depth_images_publisher = create_publisher<custom_interfaces::msg::DepthImages>("depth_images", qos_settings);
        _merged_ptcld_filtered_publisher = create_publisher<custom_interfaces::msg::MergedPtcldFiltered>("merged_ptcld_filtered", qos_settings);
        _scene_debug_data_publisher = create_publisher<custom_interfaces::msg::SceneDebugData>("scene_debug_data", qos_settings);
        _diff_ptcld_publisher = create_publisher<sensor_msgs::msg::PointCloud2>("diff_ptcld", qos_settings);
 







        _initializeServers();

        _watchdog = std::make_shared<helpers::Watchdog>(this, this, "system_monitor");
        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void ScenePublisher::initNode()
    {
        status = custom_interfaces::msg::Heartbeat::STARTING;

        status = custom_interfaces::msg::Heartbeat::RUNNING;
    }

    void ScenePublisher::shutDownNode()
    {
        status = custom_interfaces::msg::Heartbeat::STOPPING;

        status = custom_interfaces::msg::Heartbeat::STOPPED;
    }

    void ScenePublisher::_initializeServers()
    {
        _action_server = rclcpp_action::create_server<Action>(
            this,
            "scene_publisher",
            std::bind(&ScenePublisher::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&ScenePublisher::_handleCancel, this, std::placeholders::_1),
            std::bind(&ScenePublisher::_handleAccepted, this, std::placeholders::_1));
    }

    rclcpp_action::GoalResponse ScenePublisher::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, Action::Goal::ConstSharedPtr /*goal*/)
    {
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse ScenePublisher::_handleCancel(const std::shared_ptr<GoalHandle> /*goal_handle*/)
    {
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void ScenePublisher::_handleAccepted(const std::shared_ptr<GoalHandle> goal_handle)
    {
        std::thread{std::bind(&ScenePublisher::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    void ScenePublisher::_execute(const std::shared_ptr<GoalHandle> goal_handle)
    {
        helpers::Timer timer("Scene publisher action", get_logger());
        auto result = std::make_shared<Action::Result>();
        if (status != custom_interfaces::msg::Heartbeat::RUNNING)
        {
            RCLCPP_WARN_ONCE(this->get_logger(), "Node is not in running state");
            goal_handle->abort(result);
            return;
        }
        // auto request_timestamp = goal_handle->get_goal()->timestamp;
        auto abortReturn = [this, &goal_handle, &result]()
        {
            _rgb_images_publisher->publish(RgbImages());
            _depth_images_publisher->publish(DepthImages());
            _merged_ptcld_filtered_publisher->publish(MergedPtcldFiltered());
            _scene_debug_data_publisher->publish(SceneDebugData());
            _diff_ptcld_publisher->publish(sensor_msgs::msg::PointCloud2());
            goal_handle->abort(result);
        };

        // Validate input message
        if (!_change_detect_data || _change_detect_data->header.stamp == builtin_interfaces::msg::Time())
        {
            RCLCPP_ERROR(this->get_logger(), "Invalid input message. Goal failed.");
            abortReturn();
            return;
        }

        RCLCPP_INFO(this->get_logger(), "Publishing scene data");
        auto [rgb_images, depth_images, merged_ptcld_filtered, debug_scene_data, diff_ptcld] = _prepareOutputMessages(_change_detect_data);
        _rgb_images_publisher->publish(std::move(rgb_images));
        _depth_images_publisher->publish(std::move(depth_images));
        _merged_ptcld_filtered_publisher->publish(std::move(merged_ptcld_filtered));
        _scene_debug_data_publisher->publish(std::move(debug_scene_data));

        _diff_ptcld_publisher->publish(std::move(diff_ptcld));

        if (rclcpp::ok())
        {
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
        }

    }

    std::tuple<RgbImages::UniquePtr,
               DepthImages::UniquePtr,
               MergedPtcldFiltered::UniquePtr,
               SceneDebugData::UniquePtr,
               sensor_msgs::msg::PointCloud2::UniquePtr>
    ScenePublisher::_prepareOutputMessages(const custom_interfaces::msg::ChangeDetect::SharedPtr &change_detect_data)
    {
        std_msgs::msg::Header header;
        header.stamp = now();

        RgbImages::UniquePtr rgb_images = std::make_unique<RgbImages>();
        rgb_images->header = header;
        rgb_images->cam1_rgb = change_detect_data->cam1_rgb;
        rgb_images->cam2_rgb = change_detect_data->cam2_rgb;

        DepthImages::UniquePtr depth_images = std::make_unique<DepthImages>();
        depth_images->header = header;
        depth_images->cam1_depth = change_detect_data->cam1_depth;
        depth_images->cam2_depth = change_detect_data->cam2_depth;

        MergedPtcldFiltered::UniquePtr merged_ptcld_filtered = std::make_unique<MergedPtcldFiltered>();
        merged_ptcld_filtered->header = header;
        merged_ptcld_filtered->merged_ptcld_filtered = change_detect_data->merged_ptcld_filtered;

        sensor_msgs::msg::PointCloud2::UniquePtr diff_ptcld = std::make_unique<sensor_msgs::msg::PointCloud2>();
        *diff_ptcld = change_detect_data->diff_ptcld;
        diff_ptcld->header = header;

        SceneDebugData::UniquePtr debug_scene_data = std::make_unique<SceneDebugData>();
        debug_scene_data->header = header;
        debug_scene_data->cam1_ptcld = change_detect_data->cam1_ptcld;
        debug_scene_data->cam2_ptcld = change_detect_data->cam2_ptcld;
        debug_scene_data->cam1_ptcld_trans = change_detect_data->cam1_ptcld_trans;
        debug_scene_data->cam2_ptcld_trans = change_detect_data->cam2_ptcld_trans;
        debug_scene_data->merged_ptcld = change_detect_data->merged_ptcld;
        debug_scene_data->timestamp = change_detect_data->timestamp;
        debug_scene_data->scene_change_percentage = change_detect_data->scene_change_percentage;
        debug_scene_data->areas_change = change_detect_data->areas_change;
        debug_scene_data->diff_ptcld = change_detect_data->diff_ptcld;

        return std::make_tuple(std::move(rgb_images), std::move(depth_images), std::move(merged_ptcld_filtered), std::move(debug_scene_data), std::move(diff_ptcld));
    }

} // namespace scene_publisher

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(scene_publisher::ScenePublisher)
