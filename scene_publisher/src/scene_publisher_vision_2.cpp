#include "scene_publisher/scene_publisher.hpp"

namespace scene_publisher
{
    ScenePublisher::ScenePublisher(const rclcpp::NodeOptions &options)
        : Node("scene_publisher", options)
    {
        helpers::commons::setLoggerLevelFromParameter(this);

        auto cutoff_time = _getCutoffTime();
        _rgb_images_buffer = Buffer<RgbImages>(cutoff_time);
        _depth_images_buffer = Buffer<DepthImages>(cutoff_time);
        _ptclds_buffer = Buffer<Ptclds>(cutoff_time);

        auto qos_settings = rclcpp::QoS(rclcpp::KeepLast(1));
        _initializeServers();
        _initializePublishers(qos_settings);
        _initializeSubscribers(qos_settings);

        _watchdog = std::make_shared<helpers::Watchdog>(this, "system_monitor");
    }

    void ScenePublisher::_initializeServers()
    {
        _action_server = rclcpp_action::create_server<Action>(
            this,
            "scene_publisher",
            std::bind(&ScenePublisher::_handleGoal, this, std::placeholders::_1, std::placeholders::_2),
            std::bind(&ScenePublisher::_handleCancel, this, std::placeholders::_1),
            std::bind(&ScenePublisher::_handleAccepted, this, std::placeholders::_1));
    }

    void ScenePublisher::_initializePublishers(const rclcpp::QoS &qos_settings)
    {
        _rgb_images_pub = create_publisher<RgbImages>("rgb_images", qos_settings);
        _depth_images_pub = create_publisher<DepthImages>("depth_images", qos_settings);
        _ptclds_pub = create_publisher<Ptclds>("ptclds", qos_settings);
    }

    void ScenePublisher::_initializeSubscribers(const rclcpp::QoS &qos_settings)
    {
        auto rgb_images_callback = [this](const RgbImages::SharedPtr rgb_images)
        {
            // helpers::Timer timer("RGBS", get_logger());
            _rgb_images_buffer.push(*rgb_images);
        };
        auto depth_images_callback = [this](const DepthImages::SharedPtr depth_images)
        {
            // helpers::Timer timer("DEPTHS", get_logger());
            _depth_images_buffer.push(*depth_images);
        };
        auto ptclds_callback = [this](const Ptclds::SharedPtr ptclds)
        {
            // helpers::Timer timer("PTCLDS", get_logger());
            _ptclds_buffer.push(*ptclds);
        };

        _rgb_images_sub = create_subscription<custom_interfaces::msg::RgbImages>("rgb_images_stream", qos_settings, rgb_images_callback);
        _depth_images_sub = create_subscription<custom_interfaces::msg::DepthImages>("depth_images_stream", qos_settings, depth_images_callback);
        _ptclds_sub = create_subscription<custom_interfaces::msg::Ptclds>("ptclds_stream", qos_settings, ptclds_callback);
    }

    rclcpp_action::GoalResponse ScenePublisher::_handleGoal(const rclcpp_action::GoalUUID & /*uuid*/, Action::Goal::ConstSharedPtr /*goal*/)
    {
        return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse ScenePublisher::_handleCancel(const std::shared_ptr<GoalHandle> /*goal_handle*/)
    {
        return rclcpp_action::CancelResponse::ACCEPT;
    }

    void ScenePublisher::_handleAccepted(const std::shared_ptr<GoalHandle> goal_handle)
    {
        std::thread{std::bind(&ScenePublisher::_execute, this, std::placeholders::_1), goal_handle}.detach();
    }

    void ScenePublisher::_execute(const std::shared_ptr<GoalHandle> goal_handle)
    {
        helpers::Timer timer("Scene publisher action", get_logger());
        auto request_timestamp = goal_handle->get_goal()->timestamp;
        auto result = std::make_shared<Action::Result>();
        auto abortReturn = [this, &goal_handle, &result]()
        {
            _rgb_images_pub->publish(RgbImages());
            _depth_images_pub->publish(DepthImages());
            _ptclds_pub->publish(Ptclds());
            goal_handle->abort(result);
        };

        auto [rgb_images, depth_images, ptclds] = _prepareOutputMessages(request_timestamp);
        if (!rgb_images || !depth_images || !ptclds)
        {
            abortReturn();
            return;
        }

        RCLCPP_INFO(this->get_logger(), "Publishing scene data");
        _rgb_images_pub->publish(std::move(rgb_images));
        _depth_images_pub->publish(std::move(depth_images));
        _ptclds_pub->publish(std::move(ptclds));

        if (rclcpp::ok())
        {
            goal_handle->succeed(result);
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
        }
    }

    std::tuple<RgbImages::UniquePtr,
               DepthImages::UniquePtr,
               Ptclds::UniquePtr>
    ScenePublisher::_prepareOutputMessages(const builtin_interfaces::msg::Time &request_timestamp)
    {
        // Get data from buffers
        auto request_timestamp_time = std::chrono::duration<double>(request_timestamp.sec + request_timestamp.nanosec / 1e9);
        RgbImages::UniquePtr rgb_images;
        DepthImages::UniquePtr depth_images;
        Ptclds::UniquePtr ptclds;

        try
        {
            auto buffered_rgb_images = _rgb_images_buffer.getData(request_timestamp_time);
            auto buffered_depth_images = _depth_images_buffer.getData(request_timestamp_time);
            auto buffered_ptclds = _ptclds_buffer.getData(request_timestamp_time);

            std_msgs::msg::Header header;
            header.stamp = now();

            rgb_images = std::make_unique<RgbImages>();
            rgb_images->header = header;
            rgb_images->cam1_rgb = buffered_rgb_images.cam1_rgb;
            rgb_images->cam2_rgb = buffered_rgb_images.cam2_rgb;

            depth_images = std::make_unique<DepthImages>();
            depth_images->header = header;
            depth_images->cam1_depth = buffered_depth_images.cam1_depth;
            depth_images->cam2_depth = buffered_depth_images.cam2_depth;

            ptclds = std::make_unique<Ptclds>();
            ptclds->header = header;
            ptclds->cam1_ptcld = buffered_ptclds.cam1_ptcld;
            ptclds->cam2_ptcld = buffered_ptclds.cam2_ptcld;
        }
        catch (const std::exception &e)
        {
            RCLCPP_ERROR(get_logger(), e.what());
        }

        return std::make_tuple(std::move(rgb_images), std::move(depth_images), std::move(ptclds));
    }

    std::chrono::duration<double> ScenePublisher::_getCutoffTime()
    {
        std::chrono::duration<double> cutoff_time = std::chrono::duration<double>::zero();
        try
        {
            while (rclcpp::ok())
            {
                auto parameter = helpers::commons::getParameter("scene_publisher");
                if (parameter.empty())
                    continue;

                auto cutoff_time_param = parameter["cutoff_time"].get<double>();
                cutoff_time = std::chrono::duration<double>(cutoff_time_param);
                break;
            }
        }
        catch (const json::exception &e)
        {
            std::string error_message = "Error occured while parsing parameter from the server. Message: " + std::string(e.what()) + ". Exiting...";
            RCLCPP_FATAL(get_logger(), error_message);
            rclcpp::shutdown();
            throw std::runtime_error(error_message);
        }
        return cutoff_time;
    }

} // namespace scene_publisher

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(scene_publisher::ScenePublisher)
