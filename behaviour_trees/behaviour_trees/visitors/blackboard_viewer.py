import py_trees


class BlackboardVisitor(py_trees.visitors.VisitorBase):
    """
    TODO
    """

    def __init__(self, full=False):
        super(BlackboardVisitor, self).__init__(full=full)

    def finalise(self):
        """
        Override this method if any work needs to be
        performed after ticks (i.e. showing data).
        """
        all_keys = py_trees.blackboard.Blackboard.keys()
        blackboard_storage = {}
        for key in all_keys:
            try:
                blackboard_storage[key] = py_trees.blackboard.Blackboard.storage[key]
            except KeyError:
                blackboard_storage[key] = "-"

        width = len(max(blackboard_storage.keys(), key=len)) + 4
        all_info = py_trees.console.bold_green + 'Blackboard:' + py_trees.console.reset + '\n'
        for k, v in blackboard_storage.items():
            display_info = '  '
            display_info += f'{k}: '
            display_info = display_info.ljust(width)

            if type(v) not in [str, int, float, bool, py_trees.common.Status]:
                if hasattr(v, '__len__'):
                    display_info += f'[ <type={type(v).__name__}> <size={len(v)}> '
                    if len(v) > 0:
                        display_info += f'<element type={type(v[0]).__name__}> '
                    display_info += ']'
                else:
                    display_info += f'<type={type(v).__name__}> '
            else:
                display_info += f'{v} '
            display_info += '\n'
            all_info += display_info
        print(all_info)
