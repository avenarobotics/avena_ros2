import py_trees
import py_trees_ros

from .behaviours.pick_and_place_params_to_blackboard import PickAndPlaceParamsToBlackboard
from std_msgs.msg import Float32


def create_data_gathering_subtree():
    # Data gathering
    data_to_blackboard = py_trees.composites.Sequence(
        name='Data to blackboard')
    scene_change_to_blackboard = py_trees_ros.subscribers.ToBlackboard(
        name='Scene change value to blackboard',
        topic_name='scene_change_value',
        topic_type=Float32,
        qos_profile=py_trees_ros.utilities.qos_profile_unlatched(),
        blackboard_variables={'scene_change_value': 'data'},
        clearing_policy=py_trees.common.ClearingPolicy.NEVER,
    )
    pick_and_place_to_blackboard = PickAndPlaceParamsToBlackboard()
    data_to_blackboard.add_child(scene_change_to_blackboard)
    data_to_blackboard.add_child(pick_and_place_to_blackboard)
    return data_to_blackboard
