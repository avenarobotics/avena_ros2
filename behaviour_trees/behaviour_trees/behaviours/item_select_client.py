import typing
from .action_client import ActionClient
from custom_interfaces.action import ItemSelect


class ItemSelectClient(ActionClient):
    def __init__(self,
                 name: str = 'Item select',
                 generate_feedback_message: typing.Callable[[typing.Any], str] = None,
                 wait_for_server_timeout_sec: float = -3.0):
        super().__init__(action_type=ItemSelect,
                         action_name='item_select',
                         name=name,
                         generate_feedback_message=generate_feedback_message,
                         wait_for_server_timeout_sec=wait_for_server_timeout_sec)

    def _register_blackboard_keys(self):
        self.register_read_key('selected_item_label')
        self.register_read_key('selected_area_label')
        self.register_read_key('selected_area_operator')
        self.register_read_key('selected_policy')
        self.register_read_key('distance_parameter_min')
        self.register_read_key('distance_parameter_max')
        self.register_write_key('selected_items_ids')
        self.register_write_key('selected_item_id')

    def _get_goal_from_blackboard(self):
        goal = ItemSelect.Goal()
        goal.selected_item_label = self.blackboard.get('selected_item_label')
        selected_area_label_str: str = self.blackboard.get('selected_area_label')
        goal.selected_area_label = selected_area_label_str.split(',')
        goal.selected_area_operator = self.blackboard.get('selected_area_operator')
        goal.selected_policy = self.blackboard.get('selected_policy')
        goal.distance_parameter_min = self.blackboard.get('distance_parameter_min')
        goal.distance_parameter_max = self.blackboard.get('distance_parameter_max')
        return goal

    def _save_result_to_blackboard(self):
        print(f'Item select: self.result_message: {self.result_message}')
        print(f'Item select: self.result.selected_items_ids: {self.result.selected_items_ids}')
        self.blackboard.set('selected_items_ids', self.result.selected_items_ids)
        print(f'type: {type(self.result.selected_items_ids)}')
        self.blackboard.set('selected_item_id', self.result.selected_items_ids[0])
