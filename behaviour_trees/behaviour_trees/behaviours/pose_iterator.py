#!/usr/bin/env python

import py_trees


class PoseIterator(py_trees.behaviour.Behaviour):
    """
    TODO
    """

    def __init__(self,
                 poses: str,
                 out_pose: str,
                 name: str = py_trees.common.Name.AUTO_GENERATED):
        super().__init__(name)
        self.poses: str = poses
        self.out_pose: str = out_pose
        self.blackboard = self.attach_blackboard_client(name=self.name)
        self.blackboard.register_key(key=self.poses, access=py_trees.common.Access.READ,
                                     remap_to=py_trees.blackboard.Blackboard.absolute_name("/", self.poses))
        self.blackboard.register_key(key=self.poses, access=py_trees.common.Access.WRITE,
                                     remap_to=py_trees.blackboard.Blackboard.absolute_name("/", self.poses))
        self.blackboard.register_key(key=self.out_pose, access=py_trees.common.Access.WRITE,
                                     remap_to=py_trees.blackboard.Blackboard.absolute_name("/", self.out_pose))

    def update(self):
        """
        TODO
        """
        poses: list = self.blackboard.get(self.poses)
        if not poses and len(poses) == 0:
            self.feedback_message = f'there are no poses: length of poses: {len(poses)}'
            return py_trees.common.Status.FAILURE
        current_pose = poses.pop(0)
        self.blackboard.set(self.out_pose, current_pose)
        self.blackboard.set(self.poses, poses)
        return py_trees.common.Status.SUCCESS
