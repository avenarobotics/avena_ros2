import py_trees


class ClearPickAndPlaceBlackboard(py_trees.behaviour.Behaviour):
    """
    TODO
    """

    def __init__(self,
                 name: str = 'Clear pick and place blackboard'):
        super().__init__(name=name)
        self.blackboard = self.attach_blackboard_client(name=self.name)
        self.keys = ['selected_items_ids', 'selected_item_id', 'grasp_poses',
                     'grasp_pose', 'place_poses', 'place_pose',
                     'path_to_pick', 'path_pick_to_place', 'do_pick_and_place']
        self._register_pick_and_place_keys()

    def _register_pick_and_place_keys(self):
        for key in self.keys:
            self._register_write_key(key)

    def _register_write_key(self, key: str):
        self.blackboard.register_key(key=key, access=py_trees.common.Access.WRITE,
                                     remap_to=py_trees.blackboard.Blackboard.absolute_name("/", key))

    def update(self) -> py_trees.common.Status:
        print('Clearing pick and place data')
        for key in self.keys:
            self.blackboard.set(key, None)
        return py_trees.common.Status.SUCCESS
