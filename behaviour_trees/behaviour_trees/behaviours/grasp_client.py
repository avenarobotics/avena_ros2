import typing
from .action_client import ActionClient
from custom_interfaces.action import GraspAction


class GraspClient(ActionClient):
    def __init__(self,
                 name: str = 'Grasp',
                 generate_feedback_message: typing.Callable[[typing.Any], str] = None,
                 wait_for_server_timeout_sec: float = -3.0):
        super().__init__(action_type=GraspAction,
                         action_name='grasp',
                         name=name,
                         generate_feedback_message=generate_feedback_message,
                         wait_for_server_timeout_sec=wait_for_server_timeout_sec)

    def _register_blackboard_keys(self):
        self.register_read_key('selected_item_id')
        self.register_write_key('grasp_poses')

    def _get_goal_from_blackboard(self):
        goal = GraspAction.Goal()
        goal.selected_item_id = self.blackboard.get('selected_item_id')
        return goal

    def _save_result_to_blackboard(self):
        print(f'grasp: self.result_message: {self.result_message}\n')
        print(f'grasp: self.result.grasp_poses: {list(self.result.grasp_poses)}\n')
        print(f'grasp: self.result.grasp_poses: {type(self.result.grasp_poses)}\n')
        self.blackboard.set('grasp_poses', self.result.grasp_poses)
