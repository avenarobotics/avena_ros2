import typing
from .action_client import ActionClient
from custom_interfaces.action import PlaceAction


class PlaceClient(ActionClient):
    def __init__(self,
                 name: str = 'Place',
                 generate_feedback_message: typing.Callable[[typing.Any], str] = None,
                 wait_for_server_timeout_sec: float = -3.0):
        super().__init__(action_type=PlaceAction,
                         action_name='place',
                         name=name,
                         generate_feedback_message=generate_feedback_message,
                         wait_for_server_timeout_sec=wait_for_server_timeout_sec)

    def _register_blackboard_keys(self):
        self.register_read_key('grasp_pose')
        self.register_read_key('selected_item_id')
        self.register_read_key('selected_area_label_place')
        self.register_read_key('selected_area_operator_place')
        self.register_read_key('search_shift')
        self.register_read_key('place_positions_required')
        self.register_write_key('place_poses')

    def _get_goal_from_blackboard(self):
        goal = PlaceAction.Goal()
        goal.grasp_pose = self.blackboard.get('grasp_pose')
        goal.selected_item_id = self.blackboard.get('selected_item_id')
        selected_area_label_place_str: str = self.blackboard.get('selected_area_label_place')
        goal.selected_area_label = selected_area_label_place_str.split(',')
        goal.selected_area_operator = self.blackboard.get('selected_area_operator_place')
        goal.search_shift = self.blackboard.get('search_shift')
        goal.place_positions_required = self.blackboard.get('place_positions_required')
        return goal

    def _save_result_to_blackboard(self):
        self.blackboard.set('place_poses', self.result.place_poses)
