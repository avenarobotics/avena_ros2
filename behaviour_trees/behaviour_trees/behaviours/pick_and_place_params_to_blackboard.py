import copy

import py_trees
import rclpy
import threading

from custom_interfaces.srv import SetPickAndPlaceData


class PickAndPlaceParamsToBlackboard(py_trees.behaviour.Behaviour):
    """
    TODO
    """

    def __init__(self,
                 name: str = 'Pick and place parameters to blackboard'):
        super().__init__(name)
        self.srv = None
        self.node = None
        self.data_guard = threading.Lock()
        self.request_data: SetPickAndPlaceData.Request = SetPickAndPlaceData.Request()
        self.do_pick_and_place = False
        self.blackboard = self.attach_blackboard_client(name=self.name)
        self.param_keys = ['selected_item_label', 'selected_area_label', 'selected_area_operator', 'selected_policy',
                           'distance_parameter_min', 'distance_parameter_max', 'selected_area_label_place',
                           'selected_area_operator_place', 'search_shift', 'place_positions_required',
                           'constrain_value',
                           'ik_trials_number', 'max_final_states', 'ompl_compare_trials', 'min_path_points',
                           'max_time',
                           'max_simplification_time']
        # self.pick_and_place_params = ['selected_items_ids', 'selected_item_id', 'grasp_poses',
        #                               'grasp_pose', 'place_poses', 'place_pose',
        #                               'path_to_pick', 'path_pick_to_place', 'do_pick_and_place']
        for key in self.param_keys:
            self.blackboard.register_key(key=key, access=py_trees.common.Access.WRITE)
            self.blackboard.set(key, None)
        # for key in self.pick_and_place_params:
        #     self.blackboard.register_key(key=key, access=py_trees.common.Access.WRITE)
        #     self.blackboard.set(key, None)
        self.blackboard.register_key(key='do_pick_and_place', access=py_trees.common.Access.WRITE)
        self.blackboard.register_key(key='do_pick_and_place', access=py_trees.common.Access.READ)
        self.blackboard.set('do_pick_and_place', False)

    def setup(self, **kwargs):
        try:
            self.node: rclpy.Node = kwargs['node']
        except KeyError as e:
            error_message = "didn't find 'node' in setup's kwargs [{}][{}]".format(self.name, self.__class__.__name__)
            raise KeyError(error_message) from e  # 'direct cause' traceability
        self.srv = self.node.create_service(
            srv_type=SetPickAndPlaceData,
            srv_name='set_pick_and_place_data',
            callback=self._save_pick_and_place_data
        )

    def update(self) -> py_trees.common.Status:
        if self.do_pick_and_place:
            print('do pick and place')
            # print(f'self.request_data: {self.request_data}')
            with self.data_guard:
                for key in self.param_keys:
                    param_value = getattr(self.request_data, key, 'NULL')
                    print(f'{key}: {param_value}')
                    self.blackboard.set(key, param_value)
                # self.blackboard.set(key, param_value)
                self.blackboard.set('do_pick_and_place', True)
                self.request_data = SetPickAndPlaceData.Request()
            print('***')
        self.do_pick_and_place = False
        return py_trees.common.Status.SUCCESS

    def _save_pick_and_place_data(self, request: SetPickAndPlaceData.Request, response: SetPickAndPlaceData.Response):
        with self.data_guard:
            self.request_data = copy.deepcopy(request)
        self.do_pick_and_place = True
        response.success = True
        return response
