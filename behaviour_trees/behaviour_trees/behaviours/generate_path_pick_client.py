import typing
from .action_client import ActionClient
from custom_interfaces.action import GeneratePathPickAction


class GeneratePathPickClient(ActionClient):
    def __init__(self,
                 name: str = 'Generate path pick',
                 generate_feedback_message: typing.Callable[[typing.Any], str] = None,
                 wait_for_server_timeout_sec: float = -3.0):
        super().__init__(action_type=GeneratePathPickAction,
                         action_name='generate_path_pick',
                         name=name,
                         generate_feedback_message=generate_feedback_message,
                         wait_for_server_timeout_sec=wait_for_server_timeout_sec)

    def _register_blackboard_keys(self):
        self.register_read_key('grasp_pose')
        self.register_read_key('constrain_value')
        self.register_read_key('ik_trials_number')
        self.register_read_key('max_final_states')
        self.register_read_key('ompl_compare_trials')
        self.register_read_key('min_path_points')
        self.register_read_key('max_time')
        self.register_read_key('max_simplification_time')
        self.register_write_key('path_to_pick')

    def _get_goal_from_blackboard(self):
        print('PATH PICK')
        goal = GeneratePathPickAction.Goal()
        goal.grasp_pose = self.blackboard.get('grasp_pose')
        print(type(self.blackboard.get('constrain_value')))
        goal.constrain_value = self.blackboard.get('constrain_value')
        goal.ik_trials_number = self.blackboard.get('ik_trials_number')
        goal.max_final_states = self.blackboard.get('max_final_states')
        goal.ompl_compare_trials = self.blackboard.get('ompl_compare_trials')
        goal.min_path_points = self.blackboard.get('min_path_points')
        goal.max_time = self.blackboard.get('max_time')
        goal.max_simplification_time = self.blackboard.get('max_simplification_time')
        return goal

    def _save_result_to_blackboard(self):
        print('PATH PICK result')
        self.blackboard.set('path_to_pick', self.result.path_to_pick)
