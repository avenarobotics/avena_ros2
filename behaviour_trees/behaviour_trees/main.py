import sys
import argparse

import rclpy
import py_trees
import py_trees_ros
import py_trees.console as console

from . import generate_objects
from . import pick_and_place
from . import data_gathering

from .visitors.blackboard_viewer import BlackboardVisitor


def create_system_tree() -> py_trees.behaviour.Behaviour:
    root = py_trees.composites.Parallel(
        name='Avena Pipeline',
        policy=py_trees.common.ParallelPolicy.SuccessOnAll(synchronise=False),
    )
    # Data gathering subtree
    root.add_child(data_gathering.create_data_gathering_subtree())

    # Generate collision items subtree
    root.add_child(generate_objects.create_generate_objects_subtree())

    # Pick and place subtree
    root.add_child(pick_and_place.create_pick_and_place_subtree())

    # # Idle
    # idle = py_trees.behaviours.Running(name="Idle")
    # root.add_child(idle)
    return root


def command_line_argument_parser():
    parser = argparse.ArgumentParser(description='Avena pipeline behaviour tree',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-t', '--tree', action='store_true',
                        help='display tree state')
    parser.add_argument('-b', '--blackboard', action='store_true',
                        help='display blackboard')
    parser.add_argument('-l', '--log', action='store_true',
                        help='log DEBUG to console')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-s', '--step', action='store_true',
                       help='run behaviour tree step by step')
    group.add_argument('-f', '--freq', type=float, default=100.0,
                       help='frequency in which the tree should be ticked')
    return parser


def main():
    """
    Entry point for the script.
    """
    args = command_line_argument_parser().parse_args()
    rclpy.init(args=None)

    root = create_system_tree()
    tree = py_trees_ros.trees.BehaviourTree(
        root=root,
        unicode_tree_debug=False,
    )

    if args.log:
        py_trees.logging.level = py_trees.logging.Level.DEBUG

    if args.tree:
        tree.add_pre_tick_handler(lambda behaviour_tree: print(f'\n--- Run {behaviour_tree.count} ---'))
        py_trees.blackboard.Blackboard.enable_activity_stream()
        tree.add_visitor(py_trees.visitors.DebugVisitor())
        tree.add_visitor(py_trees.visitors.DisplaySnapshotVisitor(
            display_only_visited_behaviours=False,
            display_blackboard=False,
            display_activity_stream=False,
        ))

    if args.blackboard:
        tree.visitors.append(BlackboardVisitor())

    try:
        tree.setup(timeout=15)
    except py_trees_ros.exceptions.TimedOutError as e:
        console.logerror(console.red + "failed to setup the tree, aborting [{}]".format(str(e)) + console.reset)
        tree.shutdown()
        rclpy.shutdown()
        sys.exit(1)
    except KeyboardInterrupt:
        # not a warning, nor error, usually a user-initiated shutdown
        console.logerror("tree setup interrupted")
        tree.shutdown()
        rclpy.shutdown()
        sys.exit(1)

    if args.step:
        while True:
            try:
                rclpy.spin_once(tree.node)
                tree.tick()
                print('Press "q" to quit\n---')
                if py_trees.console.read_single_keypress() == 'q':
                    break
            except KeyboardInterrupt:
                pass
    else:
        period_ms = (1.0 / args.freq) * 1000.0
        tree.tick_tock(period_ms=period_ms)
        try:
            rclpy.spin(tree.node)
        except KeyboardInterrupt:
            pass

    tree.shutdown()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
