import rclpy
from rclpy.action import ActionServer
from rclpy.node import Node
from rclpy.executors import SingleThreadedExecutor
from std_srvs.srv import Trigger

from custom_interfaces.action import SimpleAction
from custom_interfaces.action import SceneChangeStatus


class ScenePublisher(Node):
    def __init__(self):
        super().__init__('scene_publisher')
        self.get_logger().info('Initializing "scene_publisher" server')
        self._action_server = ActionServer(
            node=self,
            action_type=SceneChangeStatus,
            action_name='scene_publisher',
            execute_callback=self.execute_callback)
        self._service = self.create_service(
            srv_type=Trigger,
            srv_name='trigger_scene_changed',
            callback=self.trigger_scene_changed)
        self._trigger_scene_change = False

    def execute_callback(self, goal_handle: rclpy.action.server.ServerGoalHandle):
        self.get_logger().info('Executing goal...')
        result = SceneChangeStatus.Result()
        self.get_logger().info(f'Scene change status: {self._trigger_scene_change}')
        if self._trigger_scene_change:
            result.scene_changed = True
            goal_handle.succeed()
            self._trigger_scene_change = False
        else:
            result.scene_changed = False
            goal_handle.abort()
        return result

    def trigger_scene_changed(self, request: Trigger.Request, response: Trigger.Response):
        self.get_logger().info('Next call to "scene_publisher" scene change will occur')
        self._trigger_scene_change = True
        response.message = 'Next call to "scene_publisher" scene change will occur'
        response.success = True
        return response


class SimpleActionServer(Node):
    def __init__(self, name: str):
        super().__init__(name)
        self.get_logger().info(f'Initializing "{name}" server')
        self._action_server = ActionServer(
            node=self,
            action_type=SimpleAction,
            action_name=name,
            execute_callback=self.execute_callback)

    def execute_callback(self, goal_handle: rclpy.action.server.ServerGoalHandle):
        self.get_logger().info('Executing goal...')
        result = SimpleAction.Result()
        goal_handle.succeed()
        return result


def main():
    rclpy.init(args=None)

    try:
        executor = SingleThreadedExecutor()

        scene_publisher_server = ScenePublisher()
        simple_action_names = ["detect", "filter_detections", "select_new_masks", "compose_items",
                               "estimate_shape", "merge_items", "octomap_filter", "spawn_collision_items"]
        action_servers = [SimpleActionServer(
            action_name) for action_name in simple_action_names]
        action_servers.append(scene_publisher_server)

        for action_server in action_servers:
            executor.add_node(action_server)

        executor.spin()
    except Exception as e:
        print(f'ERROR: {e}')
        pass
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
