import operator
import typing

import rclpy
import rclpy.qos
import py_trees
import py_trees_ros

from trajectory_msgs.msg import JointTrajectory
from .behaviours.simple_action_client import SimpleActionClient
from .behaviours.item_select_client import ItemSelectClient
from .behaviours.grasp_client import GraspClient
from .behaviours.place_client import PlaceClient
from .behaviours.generate_path_pick_client import GeneratePathPickClient
from .behaviours.generate_path_place_client import GeneratePathPlaceClient
from .behaviours.pose_iterator import PoseIterator
from .behaviours.clear_pick_and_place_blackboard import ClearPickAndPlaceBlackboard


def create_execute_move_subtree() -> py_trees.behaviour.Behaviour:
    sequence = py_trees.composites.Sequence(name='Execute move')

    sequence.add_child(SimpleActionClient(
        action_name='gripper_open', name='Open gripper'))
    publish_path_to_pick = py_trees_ros.publishers.FromBlackboard(
        name='Get path to pick',
        blackboard_variable='path_to_pick',
        topic_name='generated_path',
        topic_type=JointTrajectory,
        qos_profile=rclpy.qos.QoSProfile(
            depth=1,
            reliability=rclpy.qos.QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_RELIABLE,
            durability=rclpy.qos.QoSDurabilityPolicy.RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL
        )
    )
    sequence.add_child(publish_path_to_pick)
    sequence.add_child(SimpleActionClient(
        action_name='execute_move', name='Execute move'))
    sequence.add_child(SimpleActionClient(
        action_name='gripper_close', name='Close gripper'))
    publish_path_to_place = py_trees_ros.publishers.FromBlackboard(
        name='Get path to place',
        blackboard_variable='path_pick_to_place',
        topic_name='generated_path',
        topic_type=JointTrajectory,
        qos_profile=rclpy.qos.QoSProfile(
            depth=1,
            reliability=rclpy.qos.QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_RELIABLE,
            durability=rclpy.qos.QoSDurabilityPolicy.RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL
        )
    )
    sequence.add_child(publish_path_to_place)
    sequence.add_child(SimpleActionClient(
        action_name='execute_move', name='Execute move'))
    sequence.add_child(SimpleActionClient(
        action_name='gripper_open', name='Open gripper'))
    return sequence


def create_prepare_pick_and_place_data_subtree() -> py_trees.behaviour.Behaviour:
    prepare_pick_and_place_data = py_trees.composites.Sequence(name='Prepare pick and place data')

    def length_equal_check(values, threshold) -> bool:
        if values and threshold and type(values) is list:
            return len(values) == threshold
        return False

    def length_greater_check(values, threshold) -> bool:
        if values and threshold is not None and type(values) is list:
            return len(values) > threshold
        return False

    # Item select subtree
    item_select_subtree = py_trees.composites.Selector(name='Item select')
    item_select_condition = py_trees.behaviours.CheckBlackboardVariableValue(
        name='Are selected items IDs available?',
        check=py_trees.common.ComparisonExpression(
            variable='selected_items_ids',
            value=0,
            operator=length_greater_check,
        )
    )

    item_select_client = ItemSelectClient()
    item_select_subtree.add_child(item_select_condition)
    item_select_subtree.add_child(item_select_client)
    prepare_pick_and_place_data.add_child(item_select_subtree)

    # Grasp subtree
    grasp_subtree = py_trees.composites.Selector(name='Grasp')
    grasp_condition = py_trees.behaviours.CheckBlackboardVariableValue(
        name='Are grasp poses available?',
        check=py_trees.common.ComparisonExpression(
            variable='grasp_poses',
            value=0,
            operator=length_greater_check,
        )
    )

    grasp_client = GraspClient()
    grasp_subtree.add_child(grasp_condition)
    grasp_subtree.add_child(grasp_client)
    prepare_pick_and_place_data.add_child(grasp_subtree)

    # Generate path pick and place sequence (depending on grasp result)
    generate_path_pick_and_place = py_trees.composites.Sequence()
    next_grasp_pose = PoseIterator(poses='grasp_poses', out_pose='grasp_pose', name='Next grasp pose')
    next_grasp_pose_status_to_bb = py_trees.decorators.StatusToBlackboard(
        child=next_grasp_pose,
        variable_name='next_grasp_pose_status',
    )
    generate_path_pick_client = GeneratePathPickClient()
    place_client = PlaceClient()
    generate_path_pick_and_place.add_children([next_grasp_pose_status_to_bb, generate_path_pick_client, place_client])
    failure_is_running_generate_path_pick_and_place = py_trees.decorators.FailureIsRunning(
        child=generate_path_pick_and_place)

    # Guard
    def guard0(blackboard: py_trees.blackboard.Blackboard) -> typing.Union[bool, py_trees.common.Status]:
        if blackboard.exists('next_grasp_pose_status'):
            next_grasp_pose_status = blackboard.get('next_grasp_pose_status')
            print('next_grasp_pose_status', next_grasp_pose_status)
            if next_grasp_pose_status != py_trees.common.Status.SUCCESS:
                return False
        return True

    eternal_guard0 = py_trees.decorators.EternalGuard(
        child=failure_is_running_generate_path_pick_and_place,
        condition=guard0,
        blackboard_keys={'next_grasp_pose_status'},
        name='Guard generate path pick and place',
    )
    prepare_pick_and_place_data.add_child(eternal_guard0)

    # Place path subtree
    generate_path_place = py_trees.composites.Sequence()
    next_place_pose = PoseIterator(poses='place_poses', out_pose='place_pose', name='Next place pose')
    next_place_pose_status_to_bb = py_trees.decorators.StatusToBlackboard(
        child=next_place_pose,
        variable_name='next_place_pose_status',
    )
    generate_path_place_client = GeneratePathPlaceClient()
    generate_path_place.add_children([next_place_pose_status_to_bb, generate_path_place_client])
    failure_is_running_place_path = py_trees.decorators.FailureIsRunning(child=generate_path_place)

    # Guard
    def guard1(blackboard: py_trees.blackboard.Blackboard) -> typing.Union[bool, py_trees.common.Status]:
        if blackboard.exists('next_place_pose_status'):
            next_place_pose_status = blackboard.get('next_place_pose_status')
            print('next_place_pose_status', next_place_pose_status)
            if next_place_pose_status != py_trees.common.Status.SUCCESS:
                return False
        return True

    eternal_guard1 = py_trees.decorators.EternalGuard(
        child=failure_is_running_place_path,
        condition=guard1,
        blackboard_keys={'next_place_pose_status'},
        name='Guard generate path place',
    )
    prepare_pick_and_place_data.add_child(eternal_guard1)

    return prepare_pick_and_place_data


def create_pick_and_place_subtree() -> py_trees.behaviour.Behaviour:
    """Create root for behaviour tree for pick and place"""
    root = py_trees.composites.Selector(name='Pick and place')

    movement_subtree = py_trees.composites.Sequence()
    root.add_child(movement_subtree)

    # Check whether pick and place should run
    is_pick_and_place_requested = py_trees.behaviours.CheckBlackboardVariableValue(
        name='Do pick and place?',
        check=py_trees.common.ComparisonExpression(
            variable='do_pick_and_place',
            value=True,
            operator=operator.eq
        )
    )
    movement_subtree.add_child(is_pick_and_place_requested)

    # Clear blackboard after successful processing
    clear_pick_and_place_blackboard = ClearPickAndPlaceBlackboard()
    movement_subtree.add_child(clear_pick_and_place_blackboard)

    # Get occupancy grid
    get_occupancy_grid = SimpleActionClient(
        action_name='get_occupancy_grid',
        name='Get occupancy grid',
    )
    movement_subtree.add_child(get_occupancy_grid)

    # Prepare pick and place data
    prepare_pick_and_place = create_prepare_pick_and_place_data_subtree()
    timeout = py_trees.decorators.Timeout(child=prepare_pick_and_place, duration=1000.0)
    movement_subtree.add_child(timeout)

    # Execute move subtree
    execute_move_subtree = create_execute_move_subtree()
    movement_subtree.add_child(execute_move_subtree)

    # Idle
    idle = py_trees.behaviours.Running(name="Idle")
    root.add_child(idle)

    return root
