import rclpy
import sys
import operator
import py_trees
import py_trees_ros
import py_trees.console as console

from . import data_gathering
from .behaviours.simple_action_client import SimpleActionClient


def create_simple_action_client(name: str) -> SimpleActionClient:
    simple_action_client = SimpleActionClient(
        action_name=name,
        name=name,
    )
    return simple_action_client


def create_generate_objects_action_clients():
    simple_action_names = ["scene_publisher", "detect", "filter_detections", "select_new_masks", "compose_items",
                           "estimate_shape", "merge_items", "octomap_filter", "spawn_collision_items"]
    simple_action_clients = [create_simple_action_client(
        action_name) for action_name in simple_action_names]

    return simple_action_clients


def create_generate_objects_subtree() -> py_trees.behaviour.Behaviour:
    """Create root for behaviour tree for generating objects"""
    # Worker Tasks
    tasks = py_trees.composites.Selector(
        name='Generate collision objects in Coppelia Brain')
    generate_objects = py_trees.composites.Sequence(name="Generate objects")
    is_generate_objects_requested = py_trees.behaviours.CheckBlackboardVariableValue(
        name="Generate objects?",
        check=py_trees.common.ComparisonExpression(
            variable="scene_change_value",
            value=0.01,
            operator=operator.gt
        )
    )

    # Idle
    idle = py_trees.behaviours.Running(name="Idle")

    # Connecting behaviours
    # root.add_child(tasks)
    tasks.add_child(generate_objects)
    generate_objects.add_child(is_generate_objects_requested)
    generate_objects.add_children(create_generate_objects_action_clients())
    tasks.add_child(idle)

    return tasks


def main():
    """
    Entry point for the script.
    """
    rclpy.init(args=None)

    # py_trees.logging.level = py_trees.logging.Level.DEBUG

    root = py_trees.composites.Parallel(
        name="Avena - generate objects",
        policy=py_trees.common.ParallelPolicy.SuccessOnAll(synchronise=False)
    )
    root.add_child(data_gathering.create_data_gathering_subtree())
    root.add_child(create_generate_objects_subtree())

    tree = py_trees_ros.trees.BehaviourTree(
        root=root,
        unicode_tree_debug=True
    )
    try:
        tree.setup(timeout=15)
    except py_trees_ros.exceptions.TimedOutError as e:
        console.logerror(
            console.red + "failed to setup the tree, aborting [{}]".format(str(e)) + console.reset)
        tree.shutdown()
        rclpy.shutdown()
        sys.exit(1)
    except KeyboardInterrupt:
        # not a warning, nor error, usually a user-initiated shutdown
        console.logerror("tree setup interrupted")
        tree.shutdown()
        rclpy.shutdown()
        sys.exit(1)

    tree.tick_tock(period_ms=50.0)

    try:
        rclpy.spin(tree.node)
    except KeyboardInterrupt:
        pass

    tree.shutdown()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
