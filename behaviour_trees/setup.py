import os
from glob import glob
from setuptools import setup, find_packages

package_name = 'behaviour_trees'

setup(
    name=package_name,
    version='0.0.1',
    packages=find_packages(),
    data_files=[
        ('share/ament_index/resource_index/packages',
         ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.launch.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Bartlomiej Styczen',
    maintainer_email='bartlomiej.styczen@pomagier.info',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'main = behaviour_trees.main:main',
            'generate_objects = behaviour_trees.generate_objects:main',
            'mock_action_servers = behaviour_trees.mock.mock_action_servers:main',
        ],
    },
)
