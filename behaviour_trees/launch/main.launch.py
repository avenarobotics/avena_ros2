import launch
import launch.actions
import launch.substitutions
import launch_ros.actions


def generate_launch_description():
    main_bt = launch_ros.actions.Node(
        package='behaviour_trees',
        executable='main',
        output='screen',
    )
    return launch.LaunchDescription([main_bt])
