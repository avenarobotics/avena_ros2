import launch
import launch.actions
import launch.substitutions
import launch_ros.actions


def generate_launch_description():
    generate_objects_bt = launch_ros.actions.Node(
        package='behaviour_trees',
        executable='generate_objects',
        output='screen',
    )
    return launch.LaunchDescription([generate_objects_bt])
