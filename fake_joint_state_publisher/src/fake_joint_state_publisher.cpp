#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_srvs/srv/trigger.hpp>
#include <trajectory_msgs/msg/joint_trajectory.hpp>

using namespace std::chrono_literals;

class FakeJointStatePublisher : public rclcpp::Node
{

public:
    FakeJointStatePublisher()
        : Node("fake_joint_state_publisher")
    {
        RCLCPP_INFO(get_logger(), "Initializing fake joint state publisher");
        _prepareInitialArmJointStates();
        rclcpp::QoS latching_qos = rclcpp::QoS(rclcpp::KeepLast(1)).transient_local().reliable();
        _pub_arm_joint_state = this->create_publisher<sensor_msgs::msg::JointState>("arm_joint_states", 10);
        _pub_gripper_joint_state = this->create_publisher<sensor_msgs::msg::JointState>("gripper_joint_states", 10);
        _generated_path_sub = this->create_subscription<trajectory_msgs::msg::JointTrajectory>("generated_path", latching_qos,
                                                                                               [this](const trajectory_msgs::msg::JointTrajectory::SharedPtr generated_path)
                                                                                               {
                                                                                                   RCLCPP_INFO(get_logger(), "Received generated path");
                                                                                                   _generated_path = generated_path;
                                                                                               });
        _update_arm_joint_stated_srv = this->create_service<std_srvs::srv::Trigger>("update_arm_joint_states", std::bind(&FakeJointStatePublisher::_updateJointStatesToPublish, this, std::placeholders::_1, std::placeholders::_2));
        _arm_joints_timer = this->create_wall_timer(50ms, std::bind(&FakeJointStatePublisher::_broadcastArmJointState, this));
        _gripper_joints_timer = this->create_wall_timer(50ms, std::bind(&FakeJointStatePublisher::_broadcastGripperJointState, this));
        RCLCPP_INFO(get_logger(), "...done");
    }

private:
    void _broadcastArmJointState()
    {
        _arm_joint_states->header.stamp = now();
        _pub_arm_joint_state->publish(*_arm_joint_states);
    }

    void _broadcastGripperJointState()
    {
        sensor_msgs::msg::JointState joint_state_msg;
        std::vector<std::string> joint_names{_robot_prefix + "_gripper_joint_1", _robot_prefix + "_gripper_joint_2"};

        const size_t num_joints = joint_names.size();
        joint_state_msg.name = joint_names;
        joint_state_msg.position.resize(num_joints, 0);

        const float position = 0.04;
        joint_state_msg.position[0] = position;
        joint_state_msg.position[1] = position;

        rclcpp::Time now = this->now();
        joint_state_msg.header.stamp = now;
        _pub_gripper_joint_state->publish(joint_state_msg);
    }

    void _prepareInitialArmJointStates()
    {
        _arm_joint_states = std::make_shared<sensor_msgs::msg::JointState>();
        std::vector<std::string> joint_names;
        for (int i = 1; i <= 7; i++)
            _arm_joint_states->name.push_back(_robot_prefix + "_joint_" + std::to_string(i));

        _arm_joint_states->position.resize(_arm_joint_states->name.size(), 0);

        _arm_joint_states->position[0] = 0;
        _arm_joint_states->position[1] = 0;
        _arm_joint_states->position[2] = 0;
        _arm_joint_states->position[3] = -0.5;
        _arm_joint_states->position[4] = 0;
        _arm_joint_states->position[5] = 1.57;
        _arm_joint_states->position[6] = -0.78539830446243;
    }

    void _updateJointStatesToPublish(const std::shared_ptr<std_srvs::srv::Trigger::Request> /*request*/, std::shared_ptr<std_srvs::srv::Trigger::Response> response)
    {
        RCLCPP_INFO(get_logger(), "Update arm joint states from last point of generated path");
        if (!_generated_path)
        {
            response->success = false;
            response->message = "There is no generated path available";
            return;
        }
        _arm_joint_states->name = _generated_path->joint_names;
        const auto last_point = _generated_path->points.back();
        _arm_joint_states->position = last_point.positions;
        response->success = true;
        response->message = "Arm joint state updated successfully";
    }

    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_arm_joint_state;
    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr _pub_gripper_joint_state;
    rclcpp::Subscription<trajectory_msgs::msg::JointTrajectory>::SharedPtr _generated_path_sub;
    rclcpp::Service<std_srvs::srv::Trigger>::SharedPtr _update_arm_joint_stated_srv;
    trajectory_msgs::msg::JointTrajectory::SharedPtr _generated_path;
    sensor_msgs::msg::JointState::SharedPtr _arm_joint_states;
    rclcpp::TimerBase::SharedPtr _arm_joints_timer;
    rclcpp::TimerBase::SharedPtr _gripper_joints_timer;
    const std::string _robot_prefix = "right_franka";
};

int main(int argc, char *argv[])
{

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<FakeJointStatePublisher>());
    rclcpp::shutdown();

    return 0;
}
