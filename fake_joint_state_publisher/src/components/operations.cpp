#include "trajectory_generator/operations.h"

const Path _convertTrajMsgToPath(trajectory_msgs::msg::JointTrajectory::SharedPtr path_data)
{

    if (path_data != nullptr)
    {
        std::list<Eigen::VectorXd> waypoints;
        Eigen::VectorXd waypoint(JOINTS_NUMBER);

        for (size_t i = 0; i < path_data->points.size(); i++)
        {
            for (size_t j = 0; j < JOINTS_NUMBER; j++)
                waypoint[j] = path_data->points[i].positions[j];

            waypoints.push_back(waypoint);
        }

        Path path(waypoints, 0.01);

        int len = path.getLength();

        if (_debug)
            RCLCPP_INFO(this->get_logger(), "_convertTrajMsgToPath -> in path size %d", len);

        return path;
    }
    else
        RCLCPP_INFO(get_logger(), "path_data is empty.");
}