#include "trajectory_generator/trajectory_generator.h"
#include <vector>

Trajectory TrajectoryGenerator::generateTrajecotryFromPath(const Path path)
{
    Eigen::VectorXd maxAcceleration(JOINTS_NUMBER);
    Eigen::VectorXd maxVelocity(JOINTS_NUMBER);

    maxVelocity << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;
    maxAcceleration << 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0;

    std::cout << "Generating trajectory with : \n";
    for (size_t i = 0; i < 2; i++)
    {
        if (i == 0)
            std::cout << "max velocity:     | ";
        else
            std::cout << "max acceleration: | ";

        for (size_t j = 0; j < JOINTS_NUMBER; j++)
        {
            if (i == 0)
                std::cout << maxVelocity[j] << " | ";
            else
                std::cout << maxAcceleration[j] << " | ";
        }
        std::cout << "\n";
    }

    Trajectory trajectory(path, maxVelocity, maxAcceleration);

    generateAcceleration(maxAcceleration, trajectory);

    if (trajectory.isValid())
    {
        std::cout << "Trajectory generated."
                  << "\n";
        return trajectory;
    }
    else
    {
        std::cout << "Trajectory generation failed."
                  << "\n";
        exit(-1);
    }
}

void TrajectoryGenerator::generateAcceleration(Eigen::VectorXd maxAcceleration, Trajectory &trajectory)
{

    std::vector<std::array<double, JOINTS_NUMBER>> acceleration;
    // std::array<double, JOINT_NUMBER> accel_values;

    double fulltime = trajectory.getDuration() / DELTA_T;
    int time_check = trajectory.getDuration() / DELTA_T;

    if (1 > fulltime - time_check > 0)
        time_check += 1;

    double time = 0;

    for (int j = 0; j <= time_check; j++)
    {
        std::array<double, JOINTS_NUMBER> accel_values;

        for (size_t i = 0; i < JOINTS_NUMBER; i++)
        {

            if (time < trajectory.getDuration())

                accel_values[i] = (trajectory.getVelocity(time + DELTA_T)[i] - trajectory.getVelocity(time)[i]) / DELTA_T;
            else
                accel_values[i] = 0.0;
        }

        time += DELTA_T;

        acceleration.push_back(accel_values);
    }

    trajectory.setAcceleration = acceleration;
}
